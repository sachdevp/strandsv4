((nil . ((projectile-project-compilation-dir . "./build")
         (compile-command . "make -j3 Strands Tests")
         (projectile-enable-caching . t)
         (projectile-project-compilation-cmd . "cmake --build . --target install -- -j4")))

;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")
 
 ("c++-mode" .
  ((eval . (progn
             (require 'projectile)
             (setq company-clang-arguments
                   (delete-dups
                    (append company-clang-arguments
                            (list (concat "-I" (projectile-project-root))
                                  ("-I/usr/autodesk/maya/include/"))))
                   company-c-headers-path-system
                   (delete-dups
                    (append company-c-headers-path-system
                            (list (concat "-I" (projectile-project-root))
                                  ("-I/usr/autodesk/maya/include/"))))))))))
