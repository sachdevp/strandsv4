/**
   DoFFull.hpp
   @author: Prashant Sachdeva
   @desc:   DoF fixed to a rigid body or the world.
*/
#pragma once
#include <DoF/DoF.hpp>

class Object;
class DoFFull: public DoF {
   // No private variables
public:
   DoFFull(SP<DynObj> _target, Eigen::Vector3t pos);
   void computeJacobian();
   Eigen::VectorXt computeTargetCoordinates();
   RET setTargetVelocity();
   RET Init();
   RET getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc);
};
