#include <DoF/DoFFull.hpp>
#include <DynObj/DynObj.hpp>

using namespace Eigen;

DoFFull::DoFFull(SP<DynObj>_target, V3 pos):
   DoF(nullptr, _target){
   this->Gamma.setZero(0,0);
   this->Omega.setIdentity(3, 3);
   this->v.setZero(3);
   this->x = pos;
}

// Nothing to be done since the Jacobian of a full DoF is not dependent on the
// state of the system.
void DoFFull::computeJacobian(){return;}

VectorXt DoFFull::computeTargetCoordinates(){return this->x;}

RET DoFFull::setTargetVelocity(){
   this->target->setv(this->v);
   return R::Success;
}

RET DoFFull::Init(){return DoF::Init();}

RET DoFFull::getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc){
   Gr = Gc = -1;
   Or = this->target->getIndex();
   Oc = this->matIndex;
   nOr = nOc = 3;
   return R::Success;
}
