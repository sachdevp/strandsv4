/**
   DoFCurve.hpp
   @author: Prashant Sachdeva
   @desc:   DoF Frame with full 6 DoFs
*/
#pragma once
#include <DoF/DoF.hpp>

class Object;
class DynObj;
class DoFFrame: public DoF {
private:
   KeysMap<V3> mScVelMap;

public:
   DoFFrame(SP<DynObj> _source, SP<DynObj> _target);
   virtual void computeJacobian() override final;
   virtual Eigen::VectorXt computeTargetCoordinates() override final;
   M4  getE();
   RET Init();
   virtual RET getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc) override final;
   RET setx(Eigen::VectorXt _x);
   RET setv(Eigen::VectorXt _v);
   ~DoFFrame(){};
   RET clearForStep(ftype t);
   RET setScVelMap(KeysMap<V3> _vMap);
   RET clearScVelMap();
};

