#pragma once

#include "Surface.hpp"

// rx along the cylinder. ry.rx=0 to be checked.
class SurfaceCylinder : public Surface{
public:
   // NOTE Check rx and ry to be perpendicular
   SurfaceCylinder(ftype _radius, M3 _axes,
                   int _gid, std::string _name);
   ~SurfaceCylinder(){};
   void setBoundary(Eigen::Vector2t _umin, Eigen::Vector2t _umax);
   void update();
   ftype getLength() const;
   ftype getDistanceU0U1(V2 u0, V2 u1);
   RET computeClosest(V2& u, V3 x);
   Eigen::MatrixXt computeOmega(M3 R, V2 u);
   V3 getx(V2 _u);
   M3 getR();

private:
   void clear();
   // NOTE Length is irrelevant. Cylinder is considered infinite.
   ftype radius;
   // rx and ry for the circular cross-section and rz for the cylinder axis
   M3 axes;
};
