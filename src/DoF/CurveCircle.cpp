#include <DoF/CurveCircle.hpp>
using namespace Eigen;
using namespace std;

CurveCircle::CurveCircle(ftype _radius, Vector3t _rx, Vector3t _ry,
                         int _gid, std::string _name):
   Curve(_gid,_name), radius(_radius), rx(_rx), ry(_ry){
   clear();
   // Check that the axes are unit norm, i.e., only unit axes vectors. Prevents
   // redundant information and the possibility of use for ellipse, for which
   // this class is not intended.
   assert(EQUALS(rx.norm() , 1));
   assert(EQUALS(ry.norm() , 1));
   this->rz = rx.cross(ry);
   this->radius = _radius;
}

CurveCircle::~CurveCircle(){}

void CurveCircle::clear(){
   this->radius = 0.001f;
   this->umin = -FLT_MAX;
   this->umax = FLT_MAX;
}

void CurveCircle::setBoundary(ftype _umin, ftype _umax){
   if (_umax >= _umin){
      this->umax = _umax;
      this->umin = _umin;
   } else{
      this->umax = _umax + 2*M_PI;
      this->umin = _umin;
   }
}

ftype CurveCircle::getLength() const{
   // FIXME Use umax and umin here
   return 2.0f*M_PI*radius;
}

ftype CurveCircle::getDistanceU0U1(ftype u0, ftype u1) const{
   // CHECK if this needs to be updated for angles beyond 0-2*PI
   return M_PI*radius*std::abs(u1-u0);
}

RET CurveCircle::computeClosest(ftype &u, Vector3t x){
   // TODO Implement this functions
   return R::Failure;
}

Vector3t CurveCircle::getx(ftype _u) {
   return radius*(rx * cos(_u) + ry * sin(_u));
};

M3 CurveCircle::getR(){
   M3 retVal;
   retVal.col(X) = rx;
   retVal.col(Y) = ry;
   retVal.col(Z) = rz;
   return retVal;
}

Eigen::MatrixXt CurveCircle::computeOmega(Eigen::Matrix3t R, ftype u){
   Vector3t rhat = ry*cos(u) - rx*sin(u);
   Vector3t r = rhat*radius;
   return R*r;
}
