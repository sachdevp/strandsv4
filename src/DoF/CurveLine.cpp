#include <DoF/CurveLine.hpp>
using namespace Eigen;
using namespace std;

// Constructor: Just sets the required values
CurveLine::CurveLine(M3 _axes, int _gid, string _name):
   Curve(_gid, _name){
   axes = _axes;
   dir  = _axes.col(Z);
   dir.normalize();
   log_random("Creating line curve (dir:%.2f,%.2f,%.2f)", dir[0], dir[1], dir[2]);
   clear();
}

// Line equation
V3 CurveLine::getx(ftype _u){return _u*dir;}

// Computing Jacobian matrix
ftype CurveLine::getDistanceU0U1(ftype _u0, ftype _u1) const{
   return abs(_u0 - _u1);
}

ftype CurveLine::getLength() const{
   return umax - umin;
}

// The closest point on a line is computed by projecting (x-p) to d. The (x-p)
// step should be done before calling this function.
RET CurveLine::computeClosest(ftype &u, V3 pt){
   u = pt.dot(dir);
   return R::Success;
}

MatrixXt CurveLine::computeOmega(M3 R, ftype u){
   return R*dir;
}
