/**
 * DoFCurve.hpp
 * @author: Prashant Sachdeva
 * @desc:   DoF lying on a curve
 **/
#pragma once
#include <DoF/DoF.hpp>

class Object;
class Curve;
class DoFCurve: public DoF {
private:
   Curve* curve;
   // Local position of the center of the curve with respect to the source
   // Note: Refer to thesis proposal for maths
   Eigen::Vector3t l0, l;
public:
   DoFCurve(SP<DynObj> _source, SP<DynObj> _target,
            Curve* _curve, ftype _theta, V3 _curveCenter,
            bool _useLocalPos = true);
   void computeJacobian() final;
   RET Init();
   Eigen::VectorXt computeTargetCoordinates() final;
   RET getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc) final;
   M4 getCenterE() final;
   M4 getCenterEExt() final;
};
