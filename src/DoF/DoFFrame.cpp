#include <SE3.hpp>
#include <DoF/DoFFrame.hpp>
#include <DynObj/DynObj.hpp>

using namespace std;
using namespace Eigen;

DoFFrame::DoFFrame(SP<DynObj> _source, SP<DynObj> _target):
   DoF(_source, _target){
   log_random("DoFFrame for target (%d) from source (%d)",
              this->target->getId(), this->source->getId());
   this->x.setZero(6);
   this->v.setZero(6);
   this->Gamma.setZero(6,6);
   this->Omega.setIdentity(6,6);
};

void DoFFrame::computeJacobian(){
   assert(IS_WORLD(this->source));
   return;
}

RET DoFFrame::setx(Eigen::VectorXt _x){
   if(_x.size()!=x.size()) return R::Failure;
   this->x = _x;
   return R::Success;
};

RET DoFFrame::setv(Eigen::VectorXt _v){
   if(_v.size()!=v.size()) return R::Failure;
   this->v = _v;
   return R::Success;
};

RET DoFFrame::Init(){return DoF::Init();}

Matrix4t DoFFrame::getE(){return SE3::computeTransform(this->x);}

VectorXt DoFFrame::computeTargetCoordinates(){
   return this->x;
};

// G for Gamma and O for Omega. Refer to mathematics documentation.
RET DoFFrame::getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc){
   Gr = -1;
   Gc = -1;
   Or = target->getIndex();
   Oc = this->matIndex;
   nOr = 6;
   nOc = 6;
   return R::Success;
}

RET DoFFrame::clearForStep(ftype t){
   if(!isScripted()) return R::Success;
   for(pair<double, V3> scVelPair: mScVelMap){
      auto tKey = scVelPair.first;
      if(tKey>tScVelSet && t>tKey){
         this->v_sc << 0, 0, 0, scVelPair.second;
         tScVelSet = tKey;
         break;
      }}
   // log_warn("Updating scripted vel: %3.2f, %s", t, MS_T(this->v_sc));
   return R::Success;
}

RET DoFFrame::setScVelMap(KeysMap<V3> _vMap){
   mScVelMap = _vMap;
   bScripted = true;
   v_sc = VectorXt::Zero(this->x.size());
   return R::Success;
}

RET DoFFrame::clearScVelMap(){
   mScVelMap.clear();
   return R::Failure;
}
