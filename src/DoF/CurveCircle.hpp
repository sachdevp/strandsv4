#pragma once

#include <DoF/Curve.hpp>

//The circle is defined by the plane given by rx and ry.
class CurveCircle : public Curve{
public:
   CurveCircle(ftype _radius, V3 _rx, V3 _ry, int _gid, std::string _name);
   ~CurveCircle();
   void setBoundary(ftype _umin, ftype _umax);
   ftype getLength() const;
   ftype getDistanceU0U1(ftype u0, ftype u1) const;
   RET computeClosest(ftype &u, V3 x);
   // TODO Implement this function
   Eigen::MatrixXt computeOmega(M3 R, ftype u);
   V3 getx(ftype _u);
   M3 getR();

private:
   void clear();
   // Unit vectors to define the circle
   // Easy to reason about theta etc.
   ftype radius;      //radius of the circle
   V3 rx, ry, rz;
};
