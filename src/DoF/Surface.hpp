/** Surface.hpp
    @author: Prashant Sachdeva
    @desc:   Abstract class to define the basic properties of a surface.
*/

#pragma once
#include <include_common.hpp>
#include <Object.hpp>

class Surface: public Object{
public:
   // TODO Figure out constructors for Surface
   Surface(int _gid, std::string _name): Object(_gid, _name){};
   virtual ~Surface(){};
   // NOTE Not using boundary for the surface yet
   virtual void setBoundary(V2 umin, V2 umax) = 0;
   virtual ftype getDistanceU0U1(V2 u0, V2 u1) = 0;
   virtual RET computeClosest(V2& u, V3 x) = 0;
   virtual Eigen::MatrixXt computeOmega(M3 R, V2 u) = 0;
   virtual V3 getx(V2 _u) = 0;
   virtual M3 getR() = 0;

protected:
   /////////////////////
   // Model variables //
   /////////////////////
   Eigen::Vector2t umax, umin;
};
