#include <DoF/DoFSurface.hpp>
#include <DynObj/DynObj.hpp>
#include <SE3.hpp>
#include <DoF/Surface.hpp>

using namespace std;
using namespace Eigen;

DoFSurface::DoFSurface
(SP<DynObj> _source, SP<DynObj> _target,
 Surface* _surface, V3 _surfaceCenter, Vector2t _u,
 bool _useLocalPos):
   DoF(_source, _target), surface(_surface){
   // Just one degree of freedom
   x.resize(2);
   x = _u;
   this->Gamma = MatrixXt::Zero(3, 6);
   this->Omega = MatrixXt::Zero(3, 2);
   this->v.setZero(2);
   if(!_useLocalPos){
      M4 E = _source->getTransform();
      M3 R = SE3::getRotation(E);
      V3 p = SE3::getTranslation(E);
      // Set offset of the surface depending on @_useLocalPos
      l0 = R.partialPivLu().solve(_surfaceCenter - p);
   } else l0 = _surfaceCenter;
};

VectorXt DoFSurface::computeTargetCoordinates(){
   M3 R = SE3::getRotation(source->getTransform());
   V3 p = SE3::getTranslation(source->getTransform());
   // Position in surface frame
   V3 surface_x = surface->getx(x);
   // Position in source frame
   V3 source_x = l0 + surface_x;
   V3 global_x = R*source_x + p;
   this->l = global_x - p;
   return global_x;
}

// Converts parametric velocity into world velocity
void DoFSurface::computeJacobian(){
   // FIXME Implement dirtyJR properly to avoid double computation
   // if(!this->dirtyJR) log_info(2, "Jacobian already computed");
   M3 R = SE3::getRotation(source->getTransform());
   this->Gamma = SE3::computeGamma(R, this->l);
   // Omega requires the rotation matrix and the local position.
   this->Omega = surface->computeOmega(R, this->x);
   this->dirtyJR = false;
}

RET DoFSurface::Init(){return DoF::Init();}

// NOTE Still testing
// G for Gamma and O for Omega. Refer to mathematics documentation.
RET DoFSurface::getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc){
   if(IS_WORLD(this->source)){
      Gr = -1;
      Gc = -1;
   } else{
      Gr = target->getIndex();
      Gc = source->getIndex();
   }
   nOr = 3;
   nOc = 2;
   Or = target->getIndex();
   Oc = this->matIndex;
   return R::Success;
}

M4 DoFSurface::getCenterE(){
   M4 retVal;
   retVal.setIdentity();
   retVal.block<3,3>(0,0) = surface->getR();
   retVal.block<3,1>(0,3) = l0;
   return source->getTransform()*retVal;
}
