#include <DoF/DoFCurve.hpp>
#include <DynObj/DynObj.hpp>
#include <SE3.hpp>
#include <DoF/Curve.hpp>

using namespace std;
using namespace Eigen;

DoFCurve::DoFCurve(SP<DynObj> _source, SP<DynObj> _target, Curve* _curve, ftype _theta,
                   V3 _curveCenter, bool _useLocalPos):
   DoF(_source, _target), curve(_curve){
   // Just one degree of freedom
   x.resize(1);
   x(0) = _theta;
   this->Gamma = MatrixXt::Zero(3, 6);
   this->Omega = MatrixXt::Zero(3, 1);
   this->v.setZero(1);
   if(!_useLocalPos){
      M4 E = _source->getTransform();
      M3 R = SE3::getRotation(E);
      V3 p = SE3::getTranslation(E);
      // Set offset of the curve depending on @_useLocalPos
      l0 = R.partialPivLu().solve(_curveCenter - p);
   } else l0 = _curveCenter;
   l = l0;
};

VectorXt DoFCurve::computeTargetCoordinates(){
   M4 E = source->getTransform();
   M3 R = SE3::getRotation(E);
   V3 p = SE3::getTranslation(E);
   // Position in curve frame
   V3 curve_x  = curve->getx(x(0));
   // Position in source frame
   V3 source_x = l0 + curve_x;
   V3 global_x = R*source_x + p;
   this->l = source_x;
   // this->l = global_x - p;
   return global_x;
}

// Converts parametric velocity into world velocity
void DoFCurve::computeJacobian(){
   // FIXME Implement dirtyJR properly to avoid double computation
   // if(!this->dirtyJR) log_info(2, "Jacobian already computed");
   M3 R = SE3::getRotation(source->getTransform());
   this->Gamma = SE3::computeGamma(R, this->l);
   // Omega requires the rotation matrix and the local position.
   this->Omega = curve->computeOmega(R, this->x(0));
   // this->dirtyJR = false;
}

RET DoFCurve::Init(){return DoF::Init();}

// NOTE Still testing
// G for Gamma and O for Omega. Refer to mathematics documentation.
RET DoFCurve::getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc){
   if(IS_WORLD(source)){
      Gr = -1;
      Gc = -1;
   } else{
      Gr = target->getIndex();
      Gc = source->getIndex();
   }
   nOr = 3;
   nOc = 1;
   Or = target->getIndex();
   Oc = matIndex;
   return R::Success;
}

M4 DoFCurve::getCenterE(){
   M4 retE;
   retE.setIdentity();
   retE.block<3,3>(0,0) = curve->getR();
   retE.block<3,1>(0,3) = l0;
   M4 transRetE = source->getTransform()*retE;
   return transRetE;
}

M4 DoFCurve::getCenterEExt(){
   M4 retE;
   retE.setIdentity();
   retE.block<3,3>(0,0) = curve->getR();
   retE.block<3,1>(0,3) = l0;
   M4 transRetE = source->getTransform()*retE;
   return transRetE;
}
