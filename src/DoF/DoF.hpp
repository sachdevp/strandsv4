/**
   DoF.hpp
   @author: Prashant Sachdeva
   @desc:   DoF is an abstract class which specifies the interface for degrees of freedom.
            Any abstract or physical DoF will derive from this class. It will provide methods to
            get the Jacobian for the reduced DoF to physical DoF. The number of associated degrees
            of freedom is given by the source object + reduced degrees of freedom.
*/

#pragma once
#include <include_common.hpp>
#include <BaseObject.hpp>

class DynObj;
class Object;
class DoF: public BaseObject {
protected:
   bool dirtyJR;              // Is the Jacobian computed? (Not currently a used feature)
   Eigen::VectorXt x, x_sc;   // Location of the dof and intergrated location from scripted velocity
   Eigen::VectorXt v, v_sc;   // Reduced velocity degrees of freedom and scripted reduced velocity
   Eigen::VectorXt dx, dx_stab;

   /** Jacobian blocks **/
   // Gamma is dependent on the source
   Eigen::MatrixXt Gamma;
   // Omega is based on the extra reduced DoFs (given in v)
   Eigen::MatrixXt Omega;

   // Base objects which helps locate the source and target objects
   SP<DynObj> source;
   // Target object whose physical position is specified by class instance
   SP<DynObj> target;
   // Length of dof + dofs of source
   uint nDoF;
   bool bAddedToScene;
   bool bScripted;
   bool bDebug;
   int matIndex;
   ftype tScVelSet;

public:
   // Constructors
   DoF(SP<DynObj> _source, SP<DynObj> _target);
   Eigen::VectorXt getx(){return x;};
   Eigen::VectorXt getv(){return v;};
   Eigen::MatrixXt getOmega();
   Eigen::MatrixXt getGamma();
   RET fillGamma(Eigen::BlockMXt M);
   RET fillOmega(Eigen::BlockMXt M);
   // Number of reduced degrees of freedom (size of x)
   uint getnReduced();
   Eigen::VectorXt getScriptedVel(){return v_sc;}
   virtual RET setTargetCoordinates();
   RET setTargetVelocity();
   RET setInitVelocity(V6 v0){
      this->v = v0;
      setTargetVelocity();
      return R::Success;
   }
   RET setIndex(int ind);
   RET setdx(Eigen::VectorXt);
   RET setdx_stab(Eigen::VectorXt);
   RET setx(Eigen::VectorXt);
   RET setv(Eigen::VectorXt);
   RET computeScriptDrift(Eigen::BlockVXt& drift);
   RET setScVelMap(KeysMap<V3>);
   RET setScVel(ftype _t, Eigen::VectorXt _sc);

   // Initialize the matrices' sizes etc.
   RET Init();
   RET addedToScene();

   RET scriptUpdate(ftype dt);
   virtual Eigen::VectorXt computeTargetCoordinates() = 0;
   virtual void computeJacobian() = 0;
   virtual RET getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc) = 0;
   // Introduced to allow reduced coordinate joints
   inline int getMatIndex(){return matIndex;}
   virtual M4 getCenterE(){return EYE4;};
   virtual M4 getCenterEExt(){return getCenterE();};
   virtual bool debugOutput(std::string &s);
   virtual ~DoF(){};

   RET fillJacobian(Eigen::BlockMXt& J);
   bool isScripted(){return this->bScripted;}
   // Allow probing of source for segment purposes.
   auto getSource(){return source;}
   std::string getTargetName();
   // Eigen::VectorXt getForce();
   // Function to get center DoF object.
   // Center of circle/cylinder, or reference point of plain etc.
   // virtual RET setDebugOutput(){this->bDebug = true; return R::Success;};
};
