/** @auth: Prashant Sachdeva
    @desc: Line curve definition for DoF1 node confined to a line
    Line equation is x = p + u*d with normalized direction, d
 */

#pragma once

#include <DoF/Curve.hpp>

class CurveLine : public Curve{
public:
   CurveLine(M3 _axes, int _gid, std::string _name);
   ~CurveLine(){};
   // NOTE Not yet respected
   void setBoundary(ftype _umin, ftype _umax){umin = _umin; umax = _umax;};
   ftype getDistanceU0U1(ftype u0, ftype u1) const;
   RET computeClosest(ftype &u, V3 x);
   Eigen::MatrixXt computeOmega(M3 R, ftype u);
   V3 getx(ftype _u);
   M3 getR(){return axes;}; // TODO Fix this
   void update(){};
   ftype getLength() const;

private:
   void clear(){};

public:
   V3 dir;
   M3 axes;
};
