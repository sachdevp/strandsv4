/** Curve.hpp
    @author: sachdevp
    @desc:   Abstract class to define the basic properties of a curve.
*/

#pragma once
#include <include_common.hpp>
#include <Object.hpp>

class Curve : public Object{
public:
   // TODO Figure out constructors for Curve
   Curve(int _gid, std::string _name): Object(_gid, _name){};
   virtual ~Curve(){};
   virtual void setBoundary(ftype umin, ftype umax) = 0;
   virtual ftype getLength() const = 0;
   virtual ftype getDistanceU0U1(ftype u0, ftype u1) const = 0;
   virtual RET computeClosest(ftype &u, V3 x) = 0;
   // R and l represent the rotation and translation of the dynamic object which
   // forms the source of the circle
   virtual Eigen::MatrixXt computeOmega(M3 R, ftype u) = 0;
   virtual V3 getx(ftype _u) = 0;
   virtual M3 getR() = 0;

public:
   /////////////////////
   // Model variables //
   /////////////////////
   ftype umax, umin;
};
