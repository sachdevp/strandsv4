// @auth sachdevp@cs.ubc.ca
// @desc Definitions for abstract DoF.

#include <DoF/DoF.hpp>
#include <DynObj/DynObj.hpp>
#include <sstream>
using namespace std;
using namespace Eigen;

DoF::DoF(SP<DynObj> _source, SP<DynObj> _target):
   source(_source), target(_target){
   this->bScripted = false;
   this->bAddedToScene = false;
   this->tScVelSet = -1;
};

RET DoF::Init(){
   this->x_sc = this->x;
   this->computeJacobian();
   this->setTargetCoordinates();
   this->setTargetVelocity();
   return R::Success;
};

RET DoF::addedToScene(){
   bAddedToScene = true;
   return R::Success;
};

MatrixXt DoF::getGamma(){
   return Gamma;
}

MatrixXt DoF::getOmega(){
   return Omega;
}

RET DoF::fillGamma(BlockMXt M){
   M = Gamma;
   return R::Success;
}

RET DoF::fillOmega(BlockMXt M){
   M = Omega;
   return R::Success;
}

RET DoF::scriptUpdate(ftype dt){
   // x += v*dt;
   if(isScripted()) x_sc += v_sc*dt;
   return R::Success;
}

RET DoF::setdx(VectorXt _dx){
   if(_dx.size()!=x.size()) return R::Failure;
   this->dx = _dx;
   return R::Success;
};

RET DoF::setdx_stab(VectorXt _dx_stab){
   if(_dx_stab.size()!=x.size()) return R::Failure;
   this->dx_stab = _dx_stab;
   return R::Success;
};

RET DoF::setx(VectorXt _x){
   if(_x.size()!=x.size()) return R::Failure;
   this->x = _x;
   return R::Success;
};

RET DoF::setv(VectorXt _v){
   if(_v.size()!=v.size()) return R::Failure;
   this->v = _v;
   return R::Success;
};

RET DoF::setTargetVelocity(){
   // log_warn("Setting target <%s> from source <%s>", ObjName(target),ObjName(source));

   if(isScripted()) return R::Success;
   VectorXt vt = this->Omega * this->v;
   if(source) vt = vt + Gamma * source->getv();

   target->setv(vt);
   return RET::Success;
};

RET DoF::setTargetCoordinates(){
   VectorXt x = computeTargetCoordinates();
   target->setx(x);
   // target->setdx(dx);
   // target->setdx_stab(dx_stab);
   return RET::Success;
}

RET DoF::setIndex(int ind){
   this->matIndex = ind;
   return R::Success;
}

uint DoF::getnReduced(){
   return (uint)x.rows();
}

RET DoF::setScVel(ftype _t, VectorXt _sc){
   RCHECKERROR_B(!isScripted(), "Setting script for unscripted DoF.");
   RCHECKERROR_B(_sc.rows() != v.rows(), "Incorrect length of velocity vector.");
   v_sc      = _sc;
   tScVelSet = _t ;
   return R::Success;
}

RET DoF::computeScriptDrift (BlockVXt& drift){
   if(!isScripted()) return R::Failure;
   drift = this->x - this->x_sc;
   return R::Success;
}

bool DoF::debugOutput(string &s){
   if(!bDebug) return false;
   s = "";
   s += target->getName() + "-DoF x: " + MS_T(this->x) + "\n";
   s += target->getName() + "-DoF v: " + MS_T(this->v) + "\n";
   // s += target->getName() + "-DoF f: " + MS_T(this->getForce()) + "\n";
   return true;
}

// VectorXt DoF::getForce(){
//    VectorXt force = this->Omega * this->force;
//    if(source) vt = vt + Gamma * source->getForce();
// }

std::string DoF::getTargetName(){
   return target->getName();
}
