#include "SurfaceCylinder.hpp"

using namespace Eigen;
using namespace std;

SurfaceCylinder::SurfaceCylinder
(ftype _radius, M3 _axes, int _gid, std::string _name):
   Surface(_gid, _name), radius(_radius), axes(_axes){
   if(!EQUALS_DET(axes, 1)){
      log_err("Axes matrix not normalized.");
   }
   log_verbose("Surface cylinder axes: %s", MS_T(axes));
}

void SurfaceCylinder::setBoundary(Vector2t _umin, Vector2t _umax){
   umin = _umin;
   umax = _umax;
}

ftype SurfaceCylinder::getDistanceU0U1(Vector2t u0, Vector2t u1){
   // TODO Correct this
   ftype x = M_PI*radius*std::abs(u1[0]-u0[0]);
   ftype y = std::abs( u1[1] - u0[1] );
   return sqrt(x*x + y*y);
}

MatrixXt SurfaceCylinder::computeOmega(Matrix3t R, Vector2t u){
   MatrixXt Omega;
   Omega.resize(3, 2);
   ftype theta = u[0];
   Vector3t tangent = radius * (axes.col(Y)*cos(theta) - axes.col(X)*sin(theta));
   Vector3t normal = axes.col(Z);
   Omega.block<3, 1>(0, 0) = R * tangent;
   Omega.block<3, 1>(0, 1) = R * normal;
   return Omega;
}

Vector3t SurfaceCylinder::getx(Vector2t u){
   return radius*(axes * V3(cos(u[0]),sin(u[0]),u[1]));
}

M3 SurfaceCylinder::getR(){
   return axes;
}

RET SurfaceCylinder::computeClosest(Vector2t &u, Vector3t x){
   return R::Failure;
}
