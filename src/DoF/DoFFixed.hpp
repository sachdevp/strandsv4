/**
   DoFFixed.hpp
   @author: Prashant Sachdeva
   @desc:   DoF fixed to a rigid body or the world.
*/
#pragma once
#include <DoF/DoF.hpp>

class Object;
class DoFFixed: public DoF {
private:
   // Local position of the fixed DoF w.r.t. the frame of reference
   V3 l;

public:
   DoFFixed(SP<DynObj> _source, SP<DynObj> _target,
            V3 _pos, bool useLocalPos = true);
   void computeJacobian();
   Eigen::MatrixXt getJacobian();
   Eigen::VectorXt computeTargetCoordinates();
   RET setTargetVelocity(); // Overriding the base funtion
   RET Init();
   RET getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc);
   M4 getCenterE();
        // Global pos assumed for this function.
   RET updateOffset(V3 _pos, bool useLocalPos = false);
};
