#include <SE3.hpp>
#include <DoF/DoFFixed.hpp>
#include <DynObj/DynObj.hpp>

DoFFixed::DoFFixed(SP<DynObj> _source, SP<DynObj> _target, V3 _pos, bool useLocalPos):
   DoF(_source, _target){

   // NOTE R should not scale and should only rotate.
   assert(_source!=nullptr);

   updateOffset(_pos, useLocalPos);

   this->Gamma = MatrixXt::Zero(3,6);
   this->Omega = MatrixXt::Zero(3,0);
};

RET DoFFixed::updateOffset(V3 pos, bool useLocalPos){
   Eigen::Matrix4t E = source->getTransform();
   M3 rotE = SE3::getRotation(E);
   V3 p = SE3::getTranslation(E);
   this->nDoF = this->source->getnDoF();
   this->x = VectorXt::Zero(0);
   this->v = VectorXt::Zero(0);

   if(!useLocalPos) this->l = LinSolver(rotE).solve(pos - p);
   else this->l = pos;

   return R::Success;
}

void DoFFixed::computeJacobian(){
   if(!IS_WORLD(this->source)){
      M3 R = SE3::getRotation(this->source->getTransform());
      this->Gamma = SE3::computeGamma(R, this->l);
   }
}

MatrixXt DoFFixed::getJacobian(){
   return this->Gamma;
}

VectorXt DoFFixed::computeTargetCoordinates(){
   V3 pos, p;
   M4 E = source->getTransform();
   M3 rotE;
   SE3::getRp(rotE, p, E);
   pos = rotE*this->l + p;

   return pos;
}

RET DoFFixed::Init(){
   log_random("Init called on <%s>", ObjName(target));
   return DoF::Init();
}

RET DoFFixed::setTargetVelocity(){
   if(IS_WORLD(source)){
      target->setv(VectorXt::Zero(3));
   }
   return DoF::setTargetVelocity();
}

// G for Gamma and O for Omega. Refer to mathematics documentation.
RET DoFFixed::getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc){
   if(IS_WORLD(this->source)){
      Gr = -1;
      Gc = -1;
   } else{
      Gr = target->getIndex();
      Gc = source->getIndex();
   }
   Or = -1;
   Oc = -1;
   nOr = 3;
   nOc = 0;
   return R::Success;
}
M4 DoFFixed::getCenterE(){
   M4 E = source->getTransform();
   E.block<3, 1>(0, 3) = this->computeTargetCoordinates();
   return EYE4;
};
