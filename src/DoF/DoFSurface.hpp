/**
 * DoFSurface.hpp
 * @author: Prashant Sachdeva
 * @desc:   DoF lying on a surface
 *
 **/
#pragma once
#include <DoF/DoF.hpp>

class Object;
class Surface;
class DoFSurface: public DoF {
private:
   Surface* surface;
   // Local position of the center of the surface with respect to the source
   // Note: Refer to thesis proposal for maths
   V3 l0, l;
public:
   DoFSurface(SP<DynObj> _source, SP<DynObj> _target, Surface* _surface,
              V3 _surfaceCenter, V2 _u, bool _useLocalPos = true);
   void computeJacobian();
   RET Init();
   Eigen::VectorXt computeTargetCoordinates();
   RET getIndex(int &Gr, int &Gc, int &Or, int &Oc, int &nOr, int &nOc);
   M4 getCenterE();
};
