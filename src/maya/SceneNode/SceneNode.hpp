#pragma once

// Maya Includes
#include <MayaHelper.hpp>

// Strands Includes
#include <DynObj/Strand.hpp>
#include <DynObj/PointL.hpp>
#include <Scene/Scene.hpp>
#include <include_common.hpp>
#include <SceneConstants.hpp>
#include <StrandsCmd/SimParams.hpp>
#include <Recorder/SimOutput.hpp>
#include <Serializer/InputData.hpp>

class DoFFrame;

/**
   Extends MPxNode to create node for scene parameters
 */

// Maps strings to MObject
using MOBJMAP = std::map<std::string, MObject>;

class SceneNode: public MPxNode{
public:
   // 
   static STRMAP  typeIdString;
   static STRMAP  typeSceneArrString;
   static STRMAP  typeSceneArrShortString;
   static STRMAP  typeSceneCountString;
   static STRMAP  typeSceneCountShortString;
   static MOBJMAP typeSceneCountAttr;
   static MOBJMAP typeSceneArrAttr;
   template<class T> using UpdateFn = std::function<T (SP<SimFrame> frame)> ;
   template<class T> using AttrTuple = std::tuple<MPlug, UpdateFn<T>>;
   // typedef MStatus (*checkFn)(MObject);
   // typedef MStatus (*outFn)(MObject, ObjOutput);
   // static std::map<std::string, checkFn> checkTypeFns;
   // static std::map<std::string, outFn>   outputTypeDataFns;

   friend class StrandsCmd;

   static MTypeId id;
   static void*   creator();
   static MStatus initialize();

   /** Attribute initialization **/
   static MStatus initCountAttr();
   static MStatus initSceneMsgAttr(MObject &attrObj, const char* type);
   static MStatus initCommonAttrs();

   /** StrandsCmd Helper functions **/
   // StWork done by SceneNode for various commands defined with StrandsCmd plugin
   MStatus addStrand(int &sid, int &oid, MObject &strandObj, MObject &outStrandObj);
   MStatus addRigid(int &rid, int &oid, MObject &meshObj, MObject &outMeshObj,
                    MObject &transObj, MObject &outTransObj);
   MStatus addJoint(int &jid, int &oid, MObject &jointObj, int nObjects,
                    MObject rigidTransformObj[2], MString mOptJointType);
   MStatus addPoint(int &pid, int &oid, MObject _locatorObj, bool _LorQ);
   MStatus addForce(int &fid, int &oid, MObject forceObj);
   MStatus addConstraint(int &cid, int &oid, MObject constObj, MObject dynObj);
   MStatus attachForce(MObject forceObj, MObject obj);
   MStatus assignToRigid(MObject pointObj, MObject rigidObj);

   /** Strands Library functions**/
   // Strands::Scene creation function
   MStatus createStrandsScene(Strands::Scene* &, SimOutput* &, MAPSTRV3 offsetMap, MString labelStr);
   SP<Strands::Joint>
   createJoint(Strands::Scene* &scene, MObject jointObj, int &jid, std::string &jointName, MStatus &ms);
   SP<PointL>
   createPoint(Strands::Scene* &scene, MObject pointObj, int &pid, std::string &pointName, MStatus &ms,
               // Optional
               V3 offset = V3(0,0,0));
   SP<Rigid>
   createRigid(Strands::Scene* &scene, MObject rigidObj, int &rid, std::string &rigidName, MStatus &ms);
   SP<Strands::Force>
   createForce(Strands::Scene* &scene, MObject forceObj, int &fid, std::string &forceName, MStatus &ms);

   MStatus exportStrandsScene(MString labelStr, MString filename);
   std::optional<JointInput*>  createJointInput (MObject jointObj);
   std::optional<PointInput*>  createPointInput (MObject pointObj);
   std::optional<StrandInput*> createStrandInput(MObject strandObj);
   std::optional<ForceInput*>  createForceInput (MObject forceObj);
   std::optional<RigidInput*>  createRigidInput (MObject transRigidObj);
   std::optional<SceneInput> createSceneInput();
   SP<Strands::Strand> exportStrand
   (Strands::Scene* &scene, MObject strandObj, MAP<PointL>, const MAP<Strands::Joint>,
    int &sid, std::string &strandName, MStatus &ms);

   // MStatus combineMap(ObjMap &objMap, const NameMap nameMap, ObjType objType);
   MStatus exportStrands(SceneInput &sceneIn, MPlug strandsPlug, NameMap &nameMap);
   MStatus exportRigids(SceneInput &sceneIn, MPlug rigidsPlug, NameMap &nameMap);
   MStatus exportForces(SceneInput &sceneIn, MPlug forcesPlug, NameMap &nameMap);
   MStatus exportJoints(SceneInput &sceneIn, MPlug jointsPlug, NameMap &nameMap);
   MStatus exportPoints(SceneInput &sceneIn, MPlug pointsPlug, NameMap &nameMap);
   MStatus exportIndicators(SceneInput &sceneIn, NameMap rigidsNameMap, NameMap &nameMap);


   // Creating strands requires list of all points, and joints for
   // segment type and activity selection.
   SP<Strands::Strand> createStrand
   (Strands::Scene* &scene, MObject strandObj, int &sid, std::string &strandName, MStatus &ms);

   // MStatus combineMap(ObjMap &objMap, const NameMap nameMap, ObjType objType);
   MStatus createStrands
   (Strands::Scene* &scene, MPlug strandsPlug, MAP<Strands::Strand>&, IdObjMap &strandMObjs, NameMap &nameMap);
   MStatus createRigids(Strands::Scene* &scene, MPlug rigidsPlug, MAP<Rigid>&,
                        IdObjMap &rigidMObjs, NameMap &nameMap);
   MStatus createForces(Strands::Scene* &scene, MPlug forcesPlug,
                        MAP<Strands::Force>&, IdObjMap &forceMObjs, NameMap &nameMap);
   MStatus createJoints(Strands::Scene* &scene, MPlug jointsPlug,
                        MAP<Strands::Joint>&, IdObjMap &jointMObjs, NameMap &nameMap);
   MStatus createPoints(Strands::Scene* &scene, MPlug pointsPlug,
                        MAP<PointL>&, IdObjMap &pointMObjs, NameMap &nameMap, MAPSTRV3 offsetMap);
   MStatus createIndicators(SimOutput* simOutput, MAP<Rigid>, NameMap rigidsNameMap, NameMap &nameMap);

   // FIXME Remove MSTATUS and change to optional
   template<class T>
   static MStatus getAttrKeysMap(KeysMap<T> &attrValsMap, MPlug plug, ftype timeScale){
      attrValsMap.clear();
      MStatus ms;
      // Scale frame values by dtx.
      MPlugArray connections;
      plug.connectedTo(connections, true, false, 0);
      MDGModifier dgMod;
      MCHECKERROR_B(connections.length()>1, "Too many connections to plug.");
      if(connections.length()==1){
         MObject conn = connections[0].node();
         MFnAnimCurve animCurve(conn);
         int nKeys = animCurve.numKeys();
         for(int iKey=0; iKey<nKeys; iKey++){
            ftype time = (animCurve.time(iKey).value()-1)*timeScale;
            // cout<<time<<endl;
            ftype val = animCurve.value(iKey);
            attrValsMap[time] = val;
         }
      } else{
         ftype val;
         CHECK_MSTATUS_AND_RETURN_IT(plug.getValue(val));
         attrValsMap[-EPSILON] = val;
      }
      return MS::kSuccess;
   }

   template<class T>
   static MStatus getAttrKeyframes(std::map<int, T> &attrValsMap, MPlug plug){
      attrValsMap.clear();
      MStatus ms;
      MPlugArray connections;
      plug.connectedTo(connections, true, false, 0);
      MDGModifier dgMod;
      MCHECKERROR_B(connections.length()>1, "Too many connections to plug.");
      if(connections.length()==1){
         MObject conn = connections[0].node();
         MFnAnimCurve animCurve(conn);
         int nKeys = animCurve.numKeys();
         for(int iKey=0; iKey<nKeys; iKey++){
            ftype val = animCurve.value(iKey);
            int iFrame = animCurve.time(iKey).value()-1;
            attrValsMap[iFrame] = val;
         }
      } else{
         ftype val;
         CHECK_MSTATUS_AND_RETURN_IT(plug.getValue(val));
         attrValsMap[0] = val;
      }
      return MS::kSuccess;
   }

   template<class T>
   static MStatus getAttrKeyframes(std::map<int, T> &attrValsMap, MObject obj, std::string plugString){
      return getAttrKeyframes(attrValsMap, MFnDependencyNode(obj).findPlug(plugString.c_str()));
   }

   template<class T>
   static MStatus getAttrKeysMap(KeysMap<T> &attrValsMap, MObject obj, std::string plugString, ftype timeScale){
      return getAttrKeysMap(attrValsMap, MFnDependencyNode(obj).findPlug(plugString.c_str()), timeScale);
   }
   // The activation is active once time is greater than key value.
   MStatus getAllStrandActKeyframes(ActKeyframes &actKeyframes, IdObjMap strandObj);
   MStatus getStrandActKeyframes   (Keyframes &keyframes      , MObject strandObj);
   // TODO Make static.
   MStatus getScriptedVelMap       (KeysMap<V3> &velMap       , MObject rigidTransObj);
   MStatus getWrenchMap            (KeysMap<V6> &wrenchMap    , MObject forceObj     , ftype scale);

   /**
      @return kSuccess if constraint is found (with rigidObj) or not found (with NullObj)
              kFailure if constraint finding fails due to unforseen error
   */
   MStatus findPointConstraint(MObject &rigidObj, MObject &constObj, MObject pointObj);
   // MStatus setConstTransform(MObject &pointObj, M4 E);
   MStatus clearCount();
   // Return number of enabled strands in @nStrands
   MStatus getnEnabledStrands(int &nStrands);
   // Return number of joints
   MStatus getnJoints(int &nJoints);
   // Get all maps for various scene objects
   MStatus getMaps(MAP<PointL> *&, MAP<Rigid> *&, MAP<Strands::Force> *&,
                   MAP<Strands::Joint> *&, MAP<Strands::Strand> *&);

   // Prevent warning by overriding function.
   using MPxNode::dependsOn;
   // Required for MPxNode class derivatives
   MStatus dependsOn(const MPlug, const MPlug, bool) const{return MS::kSuccess;};
   MStatus compute(const MPlug&, MDataBlock&);

   /** Input MObjects **/
   // Object ID counts
   // NOTE These are not the actual counts. These numbers are used for IDs.
   static MObject anConstraints;
   static MObject anForces;
   static MObject anJoints;
   static MObject anObjects;
   static MObject anPoints;
   static MObject anRigids;
   static MObject anStrands;

   // Strands input data
   static MObject aConstraints;
   static MObject aForces;
   static MObject aJoints;
   static MObject aPoints;
   static MObject aRigids;
   static MObject aStrands;
   // Custom defined locations which may be used in state output.
   static MObject aIndicatorsFile;

   // Debug related attrs
   static MObject aDebug;
   static MObject aDebugFile;
   static MObject aReplaceLog;

   // Scene common attributes
   static MObject aGravity;
   static MObject aTimestep;
   static MObject aDamping;
   static MObject aStab;
   static MObject aScale;
   static MObject aSolver;
   // Output data only after so many dt intervals.
   // Useful in having faster outputs
   static MObject aTimestepMultiplier;

   static MObject anResults;

private:
   // Modeling scale. Applies to only lengths
   float mScale;

   // Maps from object-id to object-pointers
   MAP<PointL> points;
   MAP<Rigid> rigids;
   MAP<Strands::Force> forces;
   MAP<Strands::Joint> joints;
   MAP<Strands::Strand> strands;

   // Creator helper functions
   static MStatus processInertiaArray(V3 &com, double &mass, V3 &MI, M3 &axes, MDoubleArray inertiaArray);
   static std::optional<MObject> findRigidConstraint(MObject tRigidObj);
   static std::optional<std::tuple<std::array<float, 4>, M4, M4>>
   compRigidMassTransform(MObject transObj, MObject meshObj);
   std::optional<KeysMap<V3>> getScriptedKeymap(MObject mObj);
   // MStatus setRigidMassTransform(SP<Rigid>& rigid, MObject transRigidObj, MObject meshObj);
   MStatus getConstraintAttrs(M4 &constE, int &constType, MObject constObj);
   // MStatus setDebugIfNeeded(SP<Object> sObj, MObject mObj);
   // MStatus setScriptedIfNeeded(SP<Rigid> sRigid, DoFFrame* frame, MObject mObj);
   MStatus getRigidId(int &rid, MObject rigidObj);
   MStatus getPointAttrs(int &oid, int &pid, V3 &pointPos, MObject pointObj);
   MStatus getConstraintAttrs(V3 &constPos, int &constType, MObject constObj);
   MStatus connectToScene(int &tid, int &oid, MObject obj, const char* type);

   // Function to clear counts of objects safely. Either all counts are cleared or none are.
   static MStatus createCountAttr(MObject &obj, const char* longName, const char* shortName);
   MStatus addTypeAttr(MObject &typeAttr, MObject inObj, ObjType type);

   /** Output related functions and attributes**/
private:
   ftype mTimeScale;
   int getnType(MObject obj, const char *attrName, const char *nAttrName, MStatus *ms);

   static MStatus checkPoint (MObject);
   static MStatus checkRigid (MObject);
   static MStatus checkJoint (MObject);
   static MStatus checkStrand(MObject);

   // Output data for a particular point
   MStatus outputPointsData (SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap pointObjs);
   MStatus outputRigidsData (SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap rigidObjs);
   MStatus outputJointsData (SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap jointObjs);
   MStatus outputStrandsData(SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap strandObjs,
                             SimParams*);

   // Select output result number to be output to model
   MStatus outputResult(int);
   MStatus setStrandExtraSize(int iSim);
   MStatus deleteOutput(int);
   std::vector<SimParams*> mSimParams;
   inline auto getnSims(){return mSimParams.size();}
   inline auto pushSim(SimParams* simp){mSimParams.push_back(simp); return getnSims()-1;}
   int getId(const char* type);
   RET setJointLimits(SP<Strands::Joint> joint, V6 l6, V6 u6, JointLimitType limitType);

   template<class T> MStatus updateAttrs
   (std::vector<AttrTuple<T>> attrTuples, SPVEC<SimFrame> frames, MTimeArray *tArr){
      MStatus ms;
      std::vector<double> vVals(tArr->length());

      // Assign to plugs
      for(auto attrTuple: attrTuples){
         MPlug plug     = std::get<0>(attrTuple);
         UpdateFn<T> fn = std::get<1>(attrTuple);
         std::transform(frames.begin(), frames.end(), vVals.begin(),
                        [fn](auto frame) {return fn(frame);});
         MCHECKERROR(resetAnimCurve(plug, vVals, tArr), "Could not clear plug for %s", DNName(plug));
      }
      return MS::kSuccess;
   }

   static MStatus collectChildPlugs(std::vector<MPlug>&, MPlug, int nC);
   static MStatus collectArrayElementPlugs(std::vector<MPlug>&, MPlug, int nE);

   template<class T>
   MStatus collectSubPlugs(std::vector<AttrTuple<T>>& vAttrTuples, int nAttrs,
                           MObject vObjs[], std::string plugStrings[], bool vCheck[], int vSize[],
                           std::function<T (SP<SimFrame> frame, int iC)> vFns[],
                           std::function<MStatus (std::vector<MPlug>&, MPlug, int)> collectFn){
      std::vector<MPlug> plugs;
      MStatus ms;
      for(int i=0;i<nAttrs;i++){
         MFnDependencyNode DN(vObjs[i]);
         MPlug MPLUG_RET(plug, DN, plugStrings[i].c_str());
         if(!vCheck[i]) continue;
         MCHECKERROR(collectFn(plugs, plug, vSize[i]), "Could not get children for plug %s", plugStrings[i].c_str());
         int iC = 0;
         for(auto p: plugs){
            auto fn = vFns[i];
            vAttrTuples.push_back(std::make_tuple(p, [iC, fn] (SP<SimFrame> frame) {return fn(frame, iC);}));
            iC++;
         }
      }
      return MS::kSuccess;

   }
};
