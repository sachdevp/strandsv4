#include<SceneNode/SceneNode.hpp>
#include <Strand/StrandMayaShape.h>
#include <MayaHelper.hpp>

using namespace std;

optional<SceneInput> SceneNode::createSceneInput(){
   MPlug solverPlug  (thisMObject(), aSolver);
   MPlug dampPlug    (thisMObject(), aDamping);
   MPlug stabPlug    (thisMObject(), aStab);
   MPlug timestepPlug(thisMObject(), aTimestep);
   MPlug multPlug    (thisMObject(), aTimestepMultiplier);

   // Clear these maps. And fill them up again.
   MPlug rigidsPlug (thisMObject (), aRigids);
   MPlug pointsPlug (thisMObject (), aPoints);
   MPlug forcesPlug (thisMObject (), aForces);
   MPlug jointsPlug (thisMObject (), aJoints);
   MPlug strandsPlug(thisMObject (), aStrands);
   MPlug gravityPlug(thisMObject (), aGravity);

   SceneInput sceneIn;
   MStatus ms;

   // Set SimOutput dt to larger timestep
   GET_VAL2_ELSE_NULLOPT(sceneIn.dtx, multPlug    , Int);
   GET_VAL2_ELSE_NULLOPT(sceneIn.dt , timestepPlug, Double);

   // Create rigid bodies
   NameMap rigidsNameMap, pointsNameMap, strandsNameMap, forcesNameMap, jointsNameMap, indicatorsNameMap;
   MCHECKERROR_RNULLOPT(exportRigids    (sceneIn, rigidsPlug   , rigidsNameMap ), "Could not export rigids");
   MCHECKERROR_RNULLOPT(exportPoints    (sceneIn, pointsPlug   , pointsNameMap ), "Could not export points.");
   MCHECKWARN(          exportForces    (sceneIn, forcesPlug   , forcesNameMap ), "Could not create all forces.");
   MCHECKERROR_RNULLOPT(exportJoints    (sceneIn, jointsPlug   , jointsNameMap ), "Could not export joints.");
   MCHECKERROR_RNULLOPT(exportStrands   (sceneIn, strandsPlug  , strandsNameMap), "Could not export strands.");
   MCHECKERROR_RNULLOPT(exportIndicators(sceneIn, rigidsNameMap, indicatorsNameMap),
                        "Could not export indicators");

   // Do not use linear solver.
   sceneIn.sol = Solver::QPMOSEK;
   CHECK_MSTATUS_AND_RETURN(dampPlug.getValue(sceneIn.damp), nullopt);
   CHECK_MSTATUS_AND_RETURN(stabPlug.getValue(sceneIn.stab), nullopt);

   GET_POINT_RET(sceneIn.grav, gravityPlug, nullopt);
   return sceneIn;
}

// Create a strand. This function creates a strand and returns it. It uses
// @strandTransObj to get updated data from Maya. This data is then used to
// create the strand.
optional<StrandInput*> SceneNode::createStrandInput(MObject strandTransObj){
   MStatus ms;
   SP<Strands::Strand> strand;
   StrandMaya  *strandMaya = nullptr;
   StrandInput *sin = new StrandInput();

   MDagPath strandShapeDP, strandTransDP;
   // Get TransformDP
   MCHECKRET_NULLOPT(MDagPath::getAPathTo(strandTransObj, strandTransDP));

   // Get ShapeDP
   MCHECKRET_NULLOPT(MDagPath::getAPathTo(strandTransObj, strandShapeDP));
   MCHECKRET_NULLOPT(strandShapeDP.extendToShapeDirectlyBelow(0));

   // Get DNs
   MObject strandShapeObj = strandShapeDP.node();
   MFnDependencyNode strandTransDN(strandTransDP.node());
   MFnDependencyNode strandShapeDN(strandShapeObj);
   MPxNode* node = strandShapeDN.userNode();
   strandMaya = dynamic_cast<StrandMaya*>(node);
   MCHECKRET_NULLOPT(!strandMaya);

   sin->name = DNName(strandTransDN);
   strandMaya->getPointIds(sin->pids);

   // Get all the values
   GET_VAL_ELSE_NULLOPT(sin->sid, strandTransDN, kAttrStrandIDLong, Int);
   GET_VAL_ELSE_NULLOPT(sin->oid, strandTransDN, kAttrObjectIDLong, Int);
   int matType;
   GET_VAL_ELSE_NULLOPT(matType, strandShapeDN, kAttrMaterialLong, Int);
   sin->matType = MatType(matType);
   GET_VAL_ELSE_NULLOPT(sin->inextensible, strandShapeDN, kAttrInextensibleLong, Bool);
   GET_VAL_ELSE_NULLOPT(sin->debug       , strandTransDN, kAttrDebugLong       , Bool);
   GET_VAL_ELSE_NULLOPT(sin->simStrand   , strandTransDN, kAttrSimpleStrandLong, Bool);
   GET_VAL_ELSE_NULLOPT(sin->r           , strandShapeDN, kAttrRadiusLong      , Double);
   GET_VAL_ELSE_NULLOPT(sin->rho           , strandShapeDN, kAttrDensityLong     , Double);
   GET_VAL_ELSE_NULLOPT(sin->k           , strandShapeDN, kAttrStiffnessLong   , Double);
   GET_VAL_ELSE_NULLOPT(sin->dl_act      , strandShapeDN, kAttrActParamLong    , Double);
   GET_VAL_ELSE_NULLOPT(sin->initStrain  , strandShapeDN, kAttrStrainLong      , Double);

   return sin;
}

// Creates a point from plug data and adds the point to the scene.
optional<PointInput*> SceneNode::createPointInput(MObject pointObj){
   MPlug outPlug, inPlug;
   V3 pointPos = V3::Zero(), constPos = V3::Zero();
   MFnDependencyNode pointDN(pointObj), constDN;
   MObject rigidObj, constObj;
   PointInput* pin = new PointInput();
   MStatus ms;

   int rid = -1, constType = kCTFreeConstraintInt;

   MCHECKERROR_RNULLOPT(getPointAttrs(pin->oid, pin->pid, pin->x, pointObj),
                     "Point attributes failed");
   log_random("Creating point %d", pin->pid);

   if(!MFAIL(findPointConstraint(rigidObj, constObj, pointObj))){
      PointConstInput pointConst;
      // MObject constTransObj;
      // constDN.setObject(constObj);
      // MFnDependencyNode constTransDN(constTransObj);
      // getTransform(constTransObj, constObj);
      // Get constraint axes related info
      // getxform(pointConst.constE, constTransObj, mScale);
      // Get constraint and point positions
      MCHECKERROR_RNULLOPT(getConstraintAttrs(pointConst.constE, pointConst.constType, constObj),
                           "Could not get constraint position");
      MCHECKERROR_RNULLOPT(getRigidId(pointConst.rid, rigidObj), "Could not get rigid ID");

      log_verbose("Constraint (type %d) found for point %d", pointConst.constType, pin->pid);
      pin->optConst = pointConst;
   }

   GET_VAL_ELSE_NULLOPT(pin->debug, pointDN, kAttrDebugLong, Bool);
   pin->name = DNName(pointDN);
   return pin;
}

optional<JointInput*> SceneNode::createJointInput(MObject jointObj){
   RET r = R::Success;
   MFnDependencyNode jointDN(jointObj);
   JointInput* jin = new JointInput();
   MStatus ms;
   GET_VAL_ELSE_NULLOPT(jin->jid, jointDN, kAttrJointIDLong, Int);
   GET_VAL_ELSE_NULLOPT(jin->oid, jointDN, kAttrObjectIDLong, Int);
   jin->name = DNName(jointDN);
   GET_VAL_ELSE_NULLOPT(jin->parentId, jointDN, kAttrJointParentIDLong, Int);
   GET_VAL_ELSE_NULLOPT(jin->childId, jointDN, kAttrJointChildIDLong, Int);
   int lim;
   GET_VAL_ELSE_NULLOPT(lim, jointDN, kAttrLimitTypeLong, Int);
   jin->limitType = static_cast<JointLimitType>(lim);

   GET_VAL_ELSE_NULLOPT(jin->debug, jointDN, kAttrDebugLong, Bool);

   GET_VAL_ELSE_NULLOPT(jin->rad , jointDN, kAttrJointRadLong   , Double);
   GET_VAL_ELSE_NULLOPT(jin->ang0, jointDN, kAttrJNeutralAngLong, Double);
   GET_VAL_ELSE_NULLOPT(jin->k   , jointDN, kAttrJStiffnessLong , Double);
   GET_VAL_ELSE_NULLOPT(jin->damp, jointDN, kAttrJDampingLong   , Double);
   V3 l3, u3;
   getxform(jin->E, jointObj);
   
   char c='l';
   string fname = string(DATA_DIR) + jin->name+".in";
   ifstream fin(fname);
   if(fin.good()){
      log_reg("exponential stiffness from %s", fname.c_str());
      fin>>c;
      if(c=='e'){
         log_reg("input 6 params for %s", jin->name.c_str());
         ftype a, b, c, d, e, f;
         for(int i=0; i<6; i++) {fin>>jin->params[i];}
      } else{
         log_err("malformed joint stiffness parameter files.");
      }
   }
   log_warn("Shape type for %s: %c", jin->name.c_str(), c);
   jin->stiffnessType = c;
   GET_POINT_RET_NULLOPT(l3, jointDN, kAttrLowerLong, 1.0);
   GET_POINT_RET_NULLOPT(u3, jointDN, kAttrUpperLong, 1.0);
   jin->l6.head<3>() = l3;
   jin->u6.head<3>() = u3;
   log_warn("Joint type not yet implemented. Using hinge joint.");
   jin->bConst = ZROT;
   return jin;
}

optional<ForceInput*> SceneNode::createForceInput(MObject forceObj){
   MFnDependencyNode forceDN(forceObj);
   MPlug outPlug, inPlug;
   // MPoint pos; M4 E;
   ForceInput* fin = new ForceInput;
   MStatus ms;

   fin->name = DNName(forceDN);
   MCHECKRET_NULLOPT(getxform(fin->E, forceObj));
   GET_VAL_ELSE_NULLOPT(fin->fid   , forceDN, kAttrForceIDLong       , Int);
   GET_VAL_ELSE_NULLOPT(fin->mag   , forceDN, kAttrForceMagnitudeLong, Double);
   GET_VAL_ELSE_NULLOPT(fin->oid   , forceDN, kAttrObjectIDLong      , Int);
   GET_VAL_ELSE_NULLOPT(fin->dobjId, forceDN, kAttrForceDynObjIDLong , Int);
   GET_VAL_ELSE_NULLOPT(fin->fType , forceDN, kAttrForceTypeLong     , Int);
   GET_VAL_ELSE_NULLOPT(fin->debug , forceDN, kAttrDebugLong, Bool);

   KeysMap<V6> wrenchMap;
   MCHECKRET_NULLOPT(getWrenchMap(fin->wrenchMap, forceObj, mTimeScale));
   return fin;
}
