#include <SceneNode/SceneNode.hpp>

// Strands includes
#include <include_common.hpp>
#include <Creator/Creator.hpp>
#include <DynObj/Rigid.hpp>
#include <Joint.hpp>
#include <Force.hpp>
#include <SE3.hpp>
// #include <MaterialSpring.hpp>
// #include <MaterialLinearMuscle.hpp>
// #include <MaterialHillMuscle.hpp>

#include <maya/MDGModifier.h>
#include <maya/MGlobal.h>

// Maya special structures
#include <maya/MPlugArray.h>
#include <maya/MDagPath.h>
#include <maya/MFnDagNode.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnAnimCurve.h>

// Maya data structures
#include <maya/MDoubleArray.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>

// Maya Strands includes
#include <MayaAttr.hpp>
#include <MayaStrings.h>
#include <Strand/StrandMayaShape.h>
#include <MayaHelper.hpp>
#include <StrandsHelper.hpp>

#include<algorithm>

using namespace std;
using namespace Eigen;
SP<Strands::Joint> SceneNode::createJoint
(Strands::Scene* &scene, MObject jointObj, int &jid, string &jointName, MStatus& ms){
   RET r = R::Success;
   MStatus tmpMS;
   auto optJin = createJointInput(jointObj);
   MCHECKERROR_B_RNULL(!optJin.has_value(), "Could not get joint input.");
   auto jin = optJin.value();
   jointName = jin->name;
   jid = jin->jid;
   auto joint = Creator::create_joint(scene, jin);

   return joint;
}

SP<Strands::Force> SceneNode::createForce
(Strands::Scene* &scene, MObject forceObj, int &fid, string &fName, MStatus &ms){
   auto optFin = createForceInput(forceObj);
   MCHECKERROR_B_RNULL(!optFin.has_value(), "Could not create force input.");
   auto fin = optFin.value();
   fName = fin->name;
   fid = fin->fid;

   return Creator::create_force(scene, fin);
}

MStatus SceneNode::getConstraintAttrs(M4 &constE, int &constType, MObject constObj){
   MStatus ms;
   MFnDependencyNode constDN(constObj);
   getxform(constE, constObj);
   GET_VAL2(constType, constDN, kAttrConstTypeLong, Int);
   return ms;
}

MStatus SceneNode::getRigidId(int &rid, MObject rigidObj){
   MFnDependencyNode rigidDN(rigidObj);
   MStatus ms;
   if(rigidObj.isNull()) rid = (uint)RigidBodies::World;
   else GET_VAL2(rid, rigidDN, kAttrRigidIDLong, Int);
   return ms;
}

MStatus SceneNode::getPointAttrs(int &oid, int &pid, V3 &pointPos, MObject pointObj){
   MFnDependencyNode pointDN(pointObj);
   MStatus ms;
   GET_VAL2(pid, pointDN, kAttrPointIDLong,  Int);
   GET_VAL2(oid, pointDN, kAttrObjectIDLong, Int);
   GET_POINT(pointPos, pointDN, kAttrTransLong, 1.0);
   return MS::kSuccess;
}

// Creates a point from plug data and adds the point to the scene.
SP<PointL> SceneNode::createPoint
(Strands::Scene* &scene, MObject pointObj, int &pid, string &pName, MStatus &ms, V3 offset){
   if(offset!=V3::Zero()) log_err("Offset ignored.");
   auto optPin = createPointInput(pointObj);
   MCHECKERROR_B_RNULL(!optPin.has_value(), "Could not create force input.");
   auto pin = optPin.value();
   V3 pointPos = V3::Zero(), constPos;
   int rid = -1, constType = kCTFreeConstraintInt;
   M3 constAxes = EYE3;
   pid   = pin->pid;
   pName = pin->name;
   auto point = Creator::create_point(scene, pin);

   ms = MS::kSuccess;
   return point;
}

MStatus SceneNode::getStrandActKeyframes(Keyframes &actsMap, MObject strandShapeObj){
   return getAttrKeyframes(actsMap, strandShapeObj, kAttrActivationLong);
}

MStatus SceneNode::getScriptedVelMap(KeysMap<V3> &velMap, MObject rigidTransObj){
   KeysMap<double> velKeys[3];
   MFnDependencyNode rigidTransDN;
   MStatus ms;
   MCHECKRET(rigidTransDN.setObject(rigidTransObj));

   MPlug plug = rigidTransDN.findPlug(kAttrScriptedVelocityLong, &ms);
   MCHECKRET(ms);

   for(int i=0; i<3; i++) MCHECKRET(getAttrKeysMap(velKeys[i], plug.child(i), mTimeScale));

   for(auto velKey: velKeys[0]){
      velMap[velKey.first] << velKey.second,
         velKeys[1][velKey.first],
         velKeys[2][velKey.first];
   }

   return MS::kSuccess;
}

// Create a strand. This function creates a strand and returns it. It uses
// @strandTransObj to get updated data from Maya. This data is then used to
// create the strand.
SP<Strands::Strand> SceneNode::createStrand
(Strands::Scene* &scene, MObject strandTransObj, int &sid, string &strandName, MStatus &ms){
   ms = MS::kSuccess;
   SP<Strands::Strand> strand;

   // Check material type
   auto optSin = createStrandInput(strandTransObj);
   MCHECKERROR_B_RNULL(!optSin.has_value(), "Strand input could not be created.");
   StrandInput* sin = optSin.value();
   sid = sin->sid;
   strandName = sin->name;

   return Creator::create_strand(scene, sin);
}

MStatus SceneNode::getnEnabledStrands(int &nStrands){
   MPlug strandPlug;
   MObject strandObj;
   MStatus ms;
   nStrands = 0;
   MPlug strandsPlug(thisMObject(), aStrands);
   for(uint iStrandPlug=0; iStrandPlug < strandsPlug.numElements(); iStrandPlug++){
      MCHECKCONT(getObjectFromPlugArray(strandObj, strandsPlug, iStrandPlug),
                 "Failed to find strand at %d. May have been deleted.",
                 iStrandPlug);
      MFnDependencyNode strandDN(strandObj);
      int GET_VAL2(enabled, strandDN, kAttrEnabledLong, Int);
      // If strand is not enabled, do not model it.
      if(enabled) nStrands++;
   }
   return ms;
}

MStatus SceneNode::getnJoints(int &nJoints){
   MStatus ms;
   nJoints = 0;
   MPlug jointsPlug(thisMObject(), aJoints);
   nJoints = jointsPlug.numElements();
   return MS::kSuccess;
}

MStatus SceneNode::createRigids
(Strands::Scene* &scene, MPlug rigidsPlug, MAP<Rigid> &rigids, IdObjMap &rigidMObjs, NameMap &nameMap){
   MObject rigidObj;
   MStatus ms;
   log_random("Number of rigid plugs: %d", rigidsPlug.numElements());
   int rid;
   SP<Rigid> rigid;
   for(uint iRigidPlug=0; iRigidPlug < rigidsPlug.numElements(); iRigidPlug++){
      string rigidName;
      MCHECKCONT(getObjectFromPlugArray(rigidObj, rigidsPlug, iRigidPlug),
                 "Failed to find rigid body at %d. May have been deleted.",
                 iRigidPlug);
      rigid = createRigid(scene, rigidObj, rid, rigidName, ms);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
      rigids[rid] = rigid;
      rigidMObjs[rid] = rigidObj;
      nameMap[rigidName] = rid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::createForces
(Strands::Scene* &scene, MPlug forcesPlug, MAP<Strands::Force> &forces, IdObjMap &forceMObjs, NameMap &nameMap){
   SP<Strands::Force> force;
   MObject forceObj;
   MStatus ms;
   int fid = -1;
   for(uint iForcePlug=0; iForcePlug<forcesPlug.numElements(); iForcePlug++){
      string forceName;
      MCHECKCONT(getObjectFromPlugArray(forceObj, forcesPlug, iForcePlug),
                 "Failed to find force at %d. May have been deleted.",
                 iForcePlug);
      force = createForce(scene, forceObj, fid, forceName, ms);
      MFnDependencyNode forceDN(forceObj);
      MCHECKERROR(ms, "Failed to create force %d", fid);
      MCHECKERROR_B(fid == -1, "FID not set");
      forces[fid] = force;
      forceMObjs[fid] = forceObj;
      nameMap[forceName] = fid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::createJoints
(Strands::Scene* &scene, MPlug jointsPlug, MAP<Strands::Joint> &joints, IdObjMap &jointMObjs, NameMap &nameMap){
   // Create joint
   MObject jointObj;
   SP<Strands::Joint> joint;
   MStatus ms;
   int jid;
   for(uint iJointPlug=0;iJointPlug<jointsPlug.numElements(); iJointPlug++){
      string jointName;
      MCHECKCONT(getObjectFromPlugArray(jointObj, jointsPlug, iJointPlug),
                 "Failed to find joint at %d. May have been deleted.",
                 iJointPlug);
      joint = createJoint(scene, jointObj, jid, jointName, ms);
      MCHECKERROR(ms, "Could not create joint %d", jid);
      joints[jid] = joint;
      jointMObjs[jid] = jointObj;
      nameMap[jointName] = jid;
   }
   return MS::kSuccess;
}

inline V3 getOffset(MAPSTRV3 offsetMap, MObject pointObj){
   MString pointName = MFnDependencyNode(pointObj).name();
   auto pointOffsetIt = offsetMap.find(pointName.asChar());
   V3 offset;
   offset<<0, 0, 0;
   // cout<<pointName<<" "<<MS_T(offset)<<endl;
   if(pointOffsetIt!=offsetMap.end()){offset = pointOffsetIt->second;}
   return offset;
}

MStatus SceneNode::createPoints
(Strands::Scene* &scene, MPlug pointsPlug, MAP<PointL> &points,
 IdObjMap &pointMObjs, NameMap &nameMap, MAPSTRV3 offsetMap){
// Create points
   for(auto offPair: offsetMap){
      cout<<offPair.first<<" "<<MS_T(offPair.second)<<endl;
   }
   MObject pointObj;
   MStatus ms;
   log_verbose("Number of point plugs: %d", pointsPlug.numElements());
   int pid = -1;
   SP<PointL> point = nullptr;
   for(uint iPointPlug=0;iPointPlug<pointsPlug.numElements(); iPointPlug++){
      string pointName;
      MCHECKCONT(getObjectFromPlugArray(pointObj, pointsPlug, iPointPlug),
                 "Skipping index. Could not get object.");
      MCHECKERROR_B(pointObj.isNull(), "Should not reach this point.");
      point = createPoint(scene, pointObj, pid, pointName, ms, getOffset(offsetMap, pointObj));
      MFnDependencyNode pointDN;
      MCHECKERROR(ms, "Failed to create point %d", pid);
      MCHECKERROR_B(pid == -1, "PID not set.");
      points[pid] = point;
      pointMObjs[pid] = pointObj;
      nameMap[pointName] = pid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::createStrands
(Strands::Scene* &scene, MPlug strandsPlug, MAP<Strands::Strand> &strands, IdObjMap &strandMObjs, NameMap &nameMap){
   MPlug strandPlug;
   MObject strandObj;
   SP<Strands::Strand> strand = nullptr;
   int enabled;
   MStatus ms;
   for(uint iStrandPlug=0;iStrandPlug<strandsPlug.numElements(); iStrandPlug++){
      string strandName;
      MCHECKCONT(getObjectFromPlugArray(strandObj, strandsPlug, iStrandPlug),
                 "Failed to find strand at %d. May have been deleted.",
                 iStrandPlug);
      MFnDependencyNode strandDN(strandObj);
      MPlug enabledPlug = strandDN.findPlug(kAttrEnabledLong, &ms);
      MCHECKRET(enabledPlug.getValue(enabled));
      // If strand is not enabled, do not model it.
      if(!enabled){log_reg("Strand %s not enabled", DNName(strandDN)); continue;}
      int sid;
      strand = createStrand(scene, strandObj, sid, strandName, ms);
      MCHECKERROR_B(!strand || MFAIL(ms), "Could not create strand");
      log_verbose("Strand %s added", strand->getCName());
      strands[sid] = strand;
      strandMObjs[sid] = strandObj;
      nameMap[strandName] = sid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::createIndicators
(SimOutput* simOutput, MAP<Rigid> rigids, NameMap rigidsNameMap, NameMap &nameMap){
   MPlug indicatorsFilePlug(thisMObject(), aIndicatorsFile);
   
   MDataHandle iFData;
   indicatorsFilePlug.getValue(iFData);
   MString iFileStr = iFData.asString();

   ifstream indStream(iFileStr.asChar());
   int id=0;

   if(indStream.good()){
      log_reg("indicators file: %s", iFileStr.asChar());
      string indName;           // Name for indicator
      string rigidName;
      char c;                   // Local 'l' or global 'g'
      V3 l;
      ftype x, y, z;
      while(indStream>>indName){
         indStream>>rigidName>>c>>x>>y>>z;
         int rigidId = rigidsNameMap[rigidName];
         auto rigid = rigids[rigidId];
         M4 E = rigid->getTransformExt();
         l = (E.inverse()*V4(x,y,z,1)).head<3>();
         simOutput->indMap[id] = make_shared<Indicator>(id, indName, l, rigidId);
         nameMap[indName] = id;
         id++;
      }
   } else{
      log_warn("Could not open indicators file.");
   }

   return MS::kSuccess;
}

MStatus SceneNode::getAllStrandActKeyframes(ActKeyframes& actKeyframes, IdObjMap strandMObjs){
   MDagPath strandShapeDP;
   MStatus ms;
   // Get TransformDP
   actKeyframes.clear();
   for(auto strandMObjPair: strandMObjs){
      MObject strandMObj = strandMObjPair.second;
      // Get ShapeDP
      MCHECKRET(MDagPath::getAPathTo(strandMObj, strandShapeDP));
      MCHECKRET(strandShapeDP.extendToShapeDirectlyBelow(0));

      // Get DNs
      MObject strandShapeObj = strandShapeDP.node();
      MFnDependencyNode strandShapeDN(strandShapeObj);

      int GET_VAL2(matType, strandShapeDN, kAttrMaterialLong, Int);

      Keyframes keyframes;
      if(matType == (short)MatType::MUSCLELINEAR){
         MCHECKERROR(getStrandActKeyframes(keyframes, strandShapeObj), "Could not get strand activation.");
      }

      MFnDependencyNode strandTransDN(strandMObj);
      string strandName(DNName(strandTransDN));
      for(auto keyPair: keyframes){
         auto iFrame = keyPair.first;
         auto actVal = keyPair.second;
         actKeyframes[iFrame][strandName] = actVal;
      }
   }
   return MS::kSuccess;
}

// Create Strands::Scene object for simulation
// Note that simOutput will be added to rec and will be availble to the scene as part of
MStatus SceneNode::createStrandsScene
(Strands::Scene* &scene, SimOutput* &simOutput, MAPSTRV3 offsetMap, MString labelStr){
   // Empty scene
   scene     = new Strands::Scene();
   simOutput = new SimOutput();
   MCHECKERROR_NOTNULL(scene, "Scene is null");
   MPlug debugPlug      (thisMObject(), aDebug);
   MPlug debugFilePlug  (thisMObject(), aDebugFile);
   MPlug replaceLogPlug (thisMObject(), aReplaceLog);
   MPlug scalePlug      (thisMObject(), aScale);
   MPlug solverPlug     (thisMObject(), aSolver);
   MPlug dampingPlug    (thisMObject(), aDamping);
   MPlug stabPlug       (thisMObject(), aStab);
   MPlug timestepPlug   (thisMObject(), aTimestep);
   MPlug multPlug       (thisMObject(), aTimestepMultiplier);

   KeysMap<bool> debugMap;
   MCHECKERROR(getAttrKeysMap(debugMap, thisMObject(), kAttrDebugLong, mTimeScale),
               "Could not get debug map.");
   MStatus ms;
   bool replace = false;
   MDataHandle dFData;
   bool debug;
   debugPlug.getValue(debug);
   MString dFileStr;
   if(debug){
      debugFilePlug.getValue(dFData);
      dFileStr = dFData.asString();
      replace = replaceLogPlug.asBool();
      if (!MFAIL(dFileStr.substituteFirst("$name", labelStr)))
         log_warn("Replaced $name with label.");
      MCHECKRET_R(scene->setDebug(debugMap, dFileStr.asChar(), replace));
   }
   
   scene->setLabel(labelStr.asChar());
   mScale = scalePlug.asFloat();

   // Clear these maps. And fill them up again.
   points.clear();
   rigids.clear();
   forces.clear();
   joints.clear();
   strands.clear();
   MPlug rigidsPlug(thisMObject(), aRigids);
   MPlug pointsPlug(thisMObject(), aPoints);
   MPlug forcesPlug(thisMObject(), aForces);
   MPlug jointsPlug(thisMObject(), aJoints);
   MPlug strandsPlug(thisMObject(), aStrands);
   MPlug gravityPlug(thisMObject(), aGravity);

   // Set SimOutput dt to larger timestep
   int    GET_VAL(dtx, multPlug    , Int);
   double GET_VAL(dt , timestepPlug, Double);
   double lStep = dtx*dt;
   mTimeScale = dtx*dt;
   // Create rigid bodies
   NameMap rigidsNameMap, pointsNameMap, strandsNameMap, forcesNameMap, jointsNameMap, indicatorsNameMap;
   MCHECKRET(createRigids(scene, rigidsPlug, rigids, simOutput->rigidMObjs, rigidsNameMap));
   MCHECKERROR_R(simOutput->combineMap(rigidsNameMap, ObjType::RIGID), "Could not accumulate map for rigids.");
   MCHECKRET(createPoints(scene, pointsPlug, points, simOutput->pointMObjs, pointsNameMap, offsetMap));
   MCHECKERROR_R(simOutput->combineMap(pointsNameMap, ObjType::POINT), "Could not accumulate map for points.");
   MCHECKWARN(createForces(scene, forcesPlug, forces, simOutput->forceMObjs, forcesNameMap), "Could not create all forces.");
   MCHECKERROR_R(simOutput->combineMap(forcesNameMap, ObjType::FORCE), "Could not accumulate map for forces.");
   MCHECKRET(createJoints(scene, jointsPlug, joints, simOutput->jointMObjs, jointsNameMap));
   MCHECKERROR_R(simOutput->combineMap(jointsNameMap, ObjType::JOINT), "Could not accumulate map for joints.");
   MCHECKRET(createStrands(scene, strandsPlug, strands, simOutput->strandMObjs, strandsNameMap));
   MCHECKERROR_R(simOutput->combineMap(strandsNameMap, ObjType::STRND), "Could not accumulate map for strands.");
   MCHECKRET(createIndicators(simOutput, rigids, rigidsNameMap, indicatorsNameMap));
   MCHECKERROR_R(simOutput->combineMap(indicatorsNameMap, ObjType::INDIC), "Could not accumulate map for indicators.");

   // Do not use linear solver.
   MCHECKRET_R(scene->setSolver(Solver::QPMOSEK));
   MCHECKRET_R(scene->Init());
   Eigen::Vector3t gravity;
   ftype dmp;
   CHECK_MSTATUS_AND_RETURN_IT(dampingPlug.getValue(dmp));
   ftype stab;
   CHECK_MSTATUS_AND_RETURN_IT(stabPlug.getValue(stab));

   GET_POINT2(gravity, gravityPlug);
   scene->setGravity(gravity);
   scene->setDamping(dmp);
   scene->setStab(stab);
   scene->setTimestep(dt);
   return MS::kSuccess;
}
