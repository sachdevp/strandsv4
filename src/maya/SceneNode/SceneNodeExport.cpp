#include <SceneNode/SceneNode.hpp>

// Strands includes
#include <include_common.hpp>
#include <Creator/Creator.hpp>
#include <DynObj/Rigid.hpp>
#include <Joint.hpp>
#include <Force.hpp>
#include <SE3.hpp>
#include <MaterialSpring.hpp>
#include <MaterialLinearMuscle.hpp>
// #include <MaterialHillMuscle.hpp>

#include <maya/MDGModifier.h>
#include <maya/MGlobal.h>

// Maya special structures
#include <maya/MPlugArray.h>
#include <maya/MDagPath.h>
#include <maya/MFnDagNode.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnAnimCurve.h>

// Maya data structures
#include <maya/MDoubleArray.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>

// Maya Strands includes
#include <MayaAttr.hpp>
#include <MayaStrings.h>
#include <Strand/StrandMayaShape.h>
#include <Serializer/XMLExporter.hpp>
#include <Serializer/XMLImporter.hpp>
#include <MayaHelper.hpp>
#include <StrandsHelper.hpp>

#include<algorithm>
#include <config.h>

using namespace std;
using namespace Eigen;
MStatus SceneNode::exportRigids
(SceneInput &sceneIn, MPlug rigidsPlug, NameMap &nameMap){
   MObject rigidObj;
   MStatus ms;
   log_random("Number of rigid plugs: %d", rigidsPlug.numElements());
   for(uint iRigidPlug=0; iRigidPlug < rigidsPlug.numElements(); iRigidPlug++){
      MCHECKCONT(getObjectFromPlugArray(rigidObj, rigidsPlug, iRigidPlug),
                 "Failed to find rigid body at %d. May have been deleted.",
                 iRigidPlug);
      auto optRin(createRigidInput(rigidObj));
      MCHECKERROR_B(!optRin.has_value(), "Could not get rigid input.");
      auto rin = optRin.value();
      sceneIn.appendObjInput(rin);
      nameMap[rin->name] = rin->rid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::exportForces(SceneInput &sceneIn, MPlug forcesPlug, NameMap &nameMap){
   SP<Strands::Force> force;
   MObject forceObj;
   MStatus ms;
   for(uint iForcePlug=0; iForcePlug<forcesPlug.numElements(); iForcePlug++){
      MCHECKCONT(getObjectFromPlugArray(forceObj, forcesPlug, iForcePlug),
                 "Failed to find force at %d. May have been deleted.",
                 iForcePlug);
      auto optFin = createForceInput(forceObj);
      MCHECKERROR_B(!optFin.has_value(),
                    "Could not get force input %d after being able to get plug.",
                    iForcePlug);
      MFnDependencyNode forceDN(forceObj);
      auto fin = optFin.value();
      MCHECKERROR(ms, "Failed to create force input %d", fin->fid);
      MCHECKERROR_B(fin->fid == -1, "FID not set");
      forces[fin->fid] = force;
      sceneIn.appendObjInput(fin);
      nameMap[fin->name] = fin->fid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::exportJoints(SceneInput &sceneIn, MPlug jointsPlug, NameMap &nameMap){
   // Create joint
   MObject jointObj;
   MStatus ms;
   for(uint iJointPlug=0;iJointPlug<jointsPlug.numElements(); iJointPlug++){
      MCHECKCONT(getObjectFromPlugArray(jointObj, jointsPlug, iJointPlug),
                 "Failed to find joint at %d. May have been deleted.",
                 iJointPlug);
      auto optJin = createJointInput(jointObj);
      MCHECKERROR_B(!optJin.has_value(),
                    "Could not get joint input %d after being able to get plug.",
                    iJointPlug);
      auto jin = optJin.value();
      MCHECKERROR(ms, "Could not create joint input %d", jin->jid);
      sceneIn.appendObjInput(jin);
      nameMap[jin->name] = jin->jid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::exportPoints(SceneInput &sceneIn, MPlug pointsPlug, NameMap &nameMap){
   MObject pointObj;
   MStatus ms;
   log_verbose("Number of point plugs: %d", pointsPlug.numElements());
   for(uint iPointPlug=0;iPointPlug<pointsPlug.numElements(); iPointPlug++){
      MCHECKCONT(getObjectFromPlugArray(pointObj, pointsPlug, iPointPlug),
                 "Skipping index. Could not get object.");
      MCHECKERROR_B(pointObj.isNull(), "Should not reach this point.");
      auto optPin = createPointInput(pointObj);
      MCHECKERROR_B(!optPin.has_value(),
                    "Could not get point input %d after being able to get plug.",
                    iPointPlug);
      auto pin = optPin.value();
      MCHECKERROR(ms, "Failed to create point %d", pin->pid);
      MCHECKERROR_B(pin->pid == -1, "PID not set.");
      sceneIn.appendObjInput(pin);
      nameMap[pin->name] = pin->pid;
   }
   return MS::kSuccess;
}

MStatus SceneNode::exportStrands(SceneInput &sceneIn, MPlug strandsPlug, NameMap &nameMap){
   MPlug strandPlug;
   MObject strandObj;
   int enabled;
   MStatus ms;
   for(uint iStrandPlug=0;iStrandPlug<strandsPlug.numElements(); iStrandPlug++){
      MCHECKCONT(getObjectFromPlugArray(strandObj, strandsPlug, iStrandPlug),
                 "Failed to find strand at %d. May have been deleted.",
                 iStrandPlug);
      MFnDependencyNode strandDN(strandObj);
      MPlug enabledPlug = strandDN.findPlug(kAttrEnabledLong, &ms);
      MCHECKRET(enabledPlug.getValue(enabled));
      // If strand is not enabled, do not model it.
      if(!enabled){log_reg("Strand %s not enabled", DNName(strandDN)); continue;}
      auto optSin = createStrandInput(strandObj);
      MCHECKERROR_B(!optSin.has_value() || MFAIL(ms), "Could not create strand");
      auto sin = optSin.value();
      log_verbose("Strand %s added", sin->name.c_str());
      sceneIn.appendObjInput(sin);
      nameMap[sin->name] = sin->sid;
   }

   return MS::kSuccess;
}

MStatus SceneNode::exportIndicators(SceneInput &sceneIn, NameMap rigidsNameMap, NameMap &nameMap){
   MPlug indicatorsFilePlug(thisMObject(), aIndicatorsFile);
   
   MDataHandle iFData;
   indicatorsFilePlug.getValue(iFData);
   MString iFileStr = iFData.asString();

   ifstream indStream(iFileStr.asChar());
   int id=0;

   if(indStream.good()){
      log_reg("indicators file: %s", iFileStr.asChar());
      string indName;           // Name for indicator
      string rigidName;
      char c;                   // Local 'l' or global 'g'
      V3 l;
      ftype x, y, z;
      while(indStream>>indName){
         indStream>>rigidName>>c>>x>>y>>z;
         int rigidId = rigidsNameMap[rigidName];
         auto rigid = rigids[rigidId];
         M4 E = rigid->getTransformExt();
         l = (E.inverse()*V4(x,y,z,1)).head<3>();
         IndicatorInput *iin = new IndicatorInput;
         iin->oid     = INDICATOR_OID;
         iin->name    = indName;
         iin->debug   = false;
         iin->objType = ObjType::INDIC;
         sceneIn.appendObjInput(iin);
         nameMap[indName] = id;
         id++;
      }
   } else{
      log_warn("Could not open indicators file.");
   }

   return MS::kSuccess;
}

// Create Strands::Scene object for simulation
// Note that simOutput will be added to rec and will be availble to the scene as part of
MStatus SceneNode::exportStrandsScene(MString labelStr, MString filename){
   // Debug related attributes should be set independent of XML file.
   // MPlug debugPlug      (thisMObject(), aDebug);
   // MPlug debugFilePlug  (thisMObject(), aDebugFile);
   // MPlug replaceLogPlug (thisMObject(), aReplaceLog);
   // MPlug scalePlug      (thisMObject(), aScale);

   MStatus ms;

   XMLExporter xmlExp(filename.asChar());
   xmlExp.init(labelStr.asChar());

   auto optSceneIn = createSceneInput();
   MCHECKERROR_B(!optSceneIn.has_value(), "Could not get scene input.");
   auto sceneIn = optSceneIn.value();

   MCHECKRET_R(xmlExp.exportScene(sceneIn));
   // XMLImporter xmlImp(filename.asChar());
   // xmlImp.init();
   // auto sceneIn2 = xmlImp.importScene();
   // MCHECKERROR_B(!sceneIn2.has_value(), "Could not read output file."
   //               "Output may have failed, or the importer did not work.");

   // XMLExporter xmlExp2((filename + ".2").asChar());
   // xmlExp2.init(labelStr.asChar());
   // xmlExp2.exportScene(sceneIn2.value());

   return MS::kSuccess;
}
