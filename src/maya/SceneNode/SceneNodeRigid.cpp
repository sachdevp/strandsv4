#include <SceneNode/SceneNode.hpp>

// Strands includes
#include <include_common.hpp>
#include <Creator/Creator.hpp>
#include <DynObj/Rigid.hpp>
#include <Joint.hpp>
#include <SE3.hpp>

// Maya special structures
#include <maya/MFnDependencyNode.h>
#include <maya/MDagPath.h>

// Maya data structures
#include <maya/MDoubleArray.h>
#include <maya/MMatrix.h>
#include <maya/MPoint.h>

// Maya Strands includes
#include <MayaAttr.hpp>
#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <Strand/StrandMayaShape.h>
#include <StrandsHelper.hpp>

using namespace std;

MStatus SceneNode::
processInertiaArray(V3 &com, double &mass, V3 &MI,
                    M3 &axes, MDoubleArray inertiaArray){
   if(inertiaArray.length()!=16) return MS::kFailure;
   // index counter
   uint ind = 0;
   // COM
   getVec3FromMDArr(com, ind, inertiaArray);
   // Mass
   mass = inertiaArray[ind]; ind++;
   // MI
   getVec3FromMDArr(MI, ind, inertiaArray);
   getMat3FromMDArr(axes, ind, inertiaArray);
   log_verbose(HS("com") "\n"
               "mass = %3.2f\n"
               HS("MI") "\n"
               HS("axes") "\n",
               MS(com), mass, MS(MI), MS(axes));
   return MS::kSuccess;
}

// Mass numbers, rigidEcom, and rigidE
optional<tuple<array<float, 4>, M4, M4>>
SceneNode::compRigidMassTransform(MObject transObj, MObject meshObj){
   MStatus ms;

   MFnDependencyNode meshDN(meshObj);
   double mass; V3 MI;
   M4 rigidE, rigidERel, rigidEcom;
   rigidEcom.setIdentity();
   rigidE.setIdentity();

   bool GET_VAL3(compMass, meshDN, kAttrCompMassLong, Bool);
   MCHECKERROR_RNULLOPT(ms, "No checkbox for mass computation found.");

   // Get transform of the body
   getxform(rigidE, transObj);
   Eigen::Transform<ftype, 3, Affine> T(rigidE);
   Eigen::Matrix3t R, S;
   T.computeScalingRotation(&S, &R);
   rigidE.block<3, 3>(0,0) = R;

   if(compMass){
      MDoubleArray inertiaArray; M3 axes; V3 com;
      double rho = 1e3;
      GET_VAL_ELSE_NULLOPT(rho, meshDN, kAttrDensityShort, Double);
      // FIXME Use C++ inertia code or run this from the script.
      REQUIRED_CMD_RESULT_RNULLOPT(MString("Inertia -rho ") + rho + " -scale " +
                                   DEFAULT_SCALE + " " + meshDN.name(), inertiaArray);
      MCHECKERROR_RNULLOPT(processInertiaArray(com, mass, MI, axes, inertiaArray),
                           "Could not process inertia array.");
      SE3::snapMatrix(axes);

      // Using transform from inertia properties: COM and principal axes of rotation
      rigidEcom.block<3, 3>(0, 0) = axes;
      // Make sure matrix does not have spurious values.
      rigidEcom.block<3, 1>(0, 3) = com;
   } else{
      rigidEcom = rigidE;
      GET_POINT_RET_NULLOPT(MI, meshDN, kAttrMILong, 1.0);
      GET_VAL_ELSE_NULLOPT(mass, meshDN, kAttrMassLong, Double);
   }

   /* Set mass matrix */
   array<ftype, 4> dMass;
   for(uint i=0; i<3; i++) dMass[i] = MI[i];
   dMass[3] = mass;

   return make_tuple(dMass, rigidEcom, rigidE);
}


// MStatus SceneNode::setRigidMassTransform
// (SP<Rigid>& rigid, MObject transObj, MObject meshObj){
//    MStatus ms;

//    MFnDependencyNode meshDN(meshObj);
//    double mass; V3 MI;
//    M4 rigidE, rigidERel, rigidEcom;
//    rigidEcom.setIdentity();
//    rigidE.setIdentity();

//    bool GET_VAL3(compMass, meshDN, kAttrCompMassLong, Bool);
//    if(MFAIL(ms)){
//       log_warn("No checkbox for mass computation found. Assume computation? [y/n]");
//       do{
//          char c = getchar();
//          if(c == 'y' || c == 'Y') break;
//          if(c == 'n' || c == 'N') return MS::kFailure;
//       } while(true);
//    }

//    // Get transform of the body
//    getxform(rigidE, transObj, mScale);
//    Eigen::Transform<ftype, 3, Affine> T(rigidE);
//    Eigen::Matrix3t R, S;
//    T.computeScalingRotation(&S, &R);
//    rigidE.block<3, 3>(0,0) = R;

//    if(compMass){
//       MDoubleArray inertiaArray; M3 axes; V3 com;
//       double rho = 1e3;
//       GET_VAL2(rho, meshDN, kAttrDensityShort, Double);
//       REQUIRED_CMD_RESULT(MString("Inertia -rho ") + rho + " -scale " +
//                           mScale + " " + meshDN.name(), inertiaArray);
//       MCHECKRET(processInertiaArray(com, mass, MI, axes, inertiaArray));
//       SE3::snapMatrix(axes);

//       // Using transform from inertia properties: COM and principal axes of rotation
//       rigidEcom.block<3, 3>(0, 0) = axes;
//       // Make sure matrix does not have spurious values.
//       rigidEcom.block<3, 1>(0, 3) = com;
//    } else{
//       rigidEcom = rigidE;
//       GET_POINT(MI, meshDN, kAttrMILong, 1.0);
//       GET_VAL2(mass, meshDN, kAttrMassLong, Double);
//    }
//    rigid->setTransform(rigidEcom, rigidE);
//    /* Set mass matrix */
//    ftype dMass[4];
//    for(uint i=0; i<3; i++) dMass[i] = MI[i];
//    dMass[3] = mass;
//    // for(int i=0; i<4; i++) dMass[i] = mass;
//    rigid->setMassMatrix(dMass);
//    /* Set transform matrix */


//    return MS::kSuccess;
// }

optional<MObject> SceneNode::findRigidConstraint(MObject tRigidObj){
   bool exists = false;
   MObject constObj;
   MStatus ms = findConstraint(constObj, tRigidObj, &exists);
   // No constraint if field does not exist (exists == false)
   MCHECKERROR_RNULLOPT(exists && MFAIL(ms),
               "Field exists but no constraint. This should not happen.");
   log_check_err((ms == MS::kSuccess) && !exists,
                 "Constraint found, but field does not exist. Something is wrong here.");
   // If constraint could not be found return nullopt.
   if(MFAIL(ms)) return nullopt;
   return constObj;
}

// MStatus SceneNode::setDebugIfNeeded(SP<Object> sObj, MObject mObj){
//    bool debug = false;
//    MFnDependencyNode DN(mObj);
//    MStatus ms;
//    GET_VAL2(debug, DN, kAttrDebugLong, Bool);
//    MCHECKERROR(ms, "Debug attr not found for %s", DNName(DN));

//    if(debug) sObj->setDebugOutput();
//    return MS::kSuccess;
// }

optional<KeysMap<V3>> SceneNode::getScriptedKeymap(MObject mObj){
   KeysMap<V3> velMap;
   bool scripted = false;
   MFnDependencyNode DN(mObj);
   MStatus ms;
   GET_VAL_ELSE_NULLOPT(scripted, DN, kAttrScriptedLong, Bool);
   MCHECKERROR_RNULLOPT(ms, "Script attr not found for %s", DNName(DN));

   if(scripted){
      MCHECKERROR_RNULLOPT(getScriptedVelMap(velMap, mObj),
                           "Could not get scripted vel map.");
      log_verbose("Scripting %s", DNName(DN));
      // Value found.
      return velMap;
   }

   // Not scripted or threw an error. Ignore the error for scripted velocity.
   return nullopt;
}

// MStatus SceneNode::setScriptedIfNeeded(SP<Rigid> sRigid, DoFFrame* frame, MObject mObj){
//    bool scripted = false;
//    MFnDependencyNode DN(mObj);
//    MStatus ms;
//    GET_VAL2(scripted, DN, kAttrScriptedLong, Bool);
//    MCHECKERROR(ms, "Script attr not found for %s", DNName(DN));

//    if(scripted){
//       KeysMap<V3> velMap;
//       MCHECKRET(getScriptedVelMap(velMap, mObj));

//       log_verbose("Scripting %s", DNName(DN));
//       frame->setScripted(velMap);
//    }

//    return MS::kSuccess;
// }

// Creates a rigid and adds it to the scene too.
SP<Rigid> SceneNode::createRigid
(Strands::Scene* &scene, MObject transRigidObj, int &rid, string &rName, MStatus &ms){
   ms = MS::kSuccess;
   // If transRigidObj is null, fail.
   MCHECKERROR_B_RNULL(transRigidObj.isNull(), "Null MObject passed.");
   auto optRin = createRigidInput(transRigidObj);
   MCHECKERROR_B_RNULL(!optRin.has_value(), "Could not get rigid input.");
   auto rin = optRin.value();

   rName = rin->name;
   rid   = rin->rid;

   auto rigid = Creator::create_rigid(scene, rin);
   // auto rigid = make_shared<Rigid>();

   // // Use given mass if asked to do so, and calculate if needed.
   // rigid->setTransform(rin->Ecom, rin->E);
   // rigid->setMassMatrix(rin->mass);
   // rigid->setInfo(rin->oid, rin->name);
   // rigid->setRigidId(rid);

   // // Frame DoFs for cube
   // DoFFrame* frame = new DoFFrame(scene->getRigid((int)RigidBodies::World), rigid);
   // rigid->setFrame(frame);

   // if(rin->optScVelMap.has_value())
   //    frame->setScripted(rin->optScVelMap.value());

   // // Add to scene
   // scene->addDoF(frame); scene->addRigid(rigid);

   // /** Constraint **/
   // if(rin->optConstE.has_value()) {
   //    auto joint = Creator::free_axis(scene, rigid, ROT_AXIS_0, rin->optConstE.value());
   //    log_check_err(!joint, "Failed to create joint on rigid body. Null joint returned.");
   // }

   ms = MS::kSuccess; return rigid;
}

optional<RigidInput*> SceneNode::createRigidInput(MObject transRigidObj){
   // If transRigidObj is null, fail.
   MStatus ms;
   MCHECKERROR_RNULLOPT(transRigidObj.isNull(), "Null MObject passed.");
   MDagPath dagP;
   MObject meshObj;
   RigidInput* rin = new RigidInput();

   MCHECKRET_NULLOPT(getShape(meshObj, transRigidObj));
   MFnDependencyNode rigidDN(transRigidObj), meshDN(meshObj);

   GET_VAL_ELSE_NULLOPT(rin->rid, rigidDN, kAttrRigidIDShort , Int);
   GET_VAL_ELSE_NULLOPT(rin->oid, rigidDN, kAttrObjectIDShort, Int);

   rin->name = DNName(rigidDN);
   GET_VAL_ELSE_NULLOPT(rin->debug, rigidDN, kAttrDebugLong, Bool);

   auto optRMassParams = compRigidMassTransform(transRigidObj, meshObj);
   MCHECKERROR_B_R(!optRMassParams.has_value(), nullopt,
                   "Could not get mass for rigid object.");
   tie(rin->mass, rin->Ecom, rin->E) = optRMassParams.value();

   auto optScVelMap = getScriptedKeymap(transRigidObj);
   if(optScVelMap.has_value()){
      rin->bScripted = true;
      rin->scVelMap = optScVelMap.value();
   } else rin->bScripted = false;

   /** Constraint **/
   auto optConstObj = findRigidConstraint(transRigidObj);
   if(optConstObj.has_value()) {
      log_verbose("Found constraint for rigid object: %s", DNName(rigidDN));
      M4 constE;
      MCHECKRET_NULLOPT(getxform(constE, optConstObj.value()));
      rin->optConstE = constE;
   }
   return rin;
}

MStatus SceneNode::getWrenchMap(KeysMap<V6> &wrenchMap, MObject forceObj, ftype timeScale){
   KeysMap<double> wrenchKeys[6];
   MFnDependencyNode forceDN;
   MStatus ms;
   MCHECKRET(forceDN.setObject(forceObj));

   MPlug plug = forceDN.findPlug(kAttrForceWrenchValLong, &ms);
   MCHECKRET(ms);

   for(int i=0; i<6; i++){
      MCHECKRET(getAttrKeysMap(wrenchKeys[i], plug.child(i), timeScale));
   }

   for(auto wrenchKey: wrenchKeys[0]){
      wrenchMap[wrenchKey.first] <<
         wrenchKey.second,
         wrenchKeys[1][wrenchKey.first],
         wrenchKeys[2][wrenchKey.first],
         wrenchKeys[3][wrenchKey.first],
         wrenchKeys[4][wrenchKey.first],
         wrenchKeys[5][wrenchKey.first];
   }

   return MS::kSuccess;
}
