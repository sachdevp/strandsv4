#include "SceneNode.hpp"
#include <MayaStrings.h>
#include <maya/MDGModifier.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MGlobal.h>
#include <maya/MPlugArray.h>
#include <sstream>
#include <include_common.hpp>
#include <MayaHelper.hpp>

using namespace std;

// TODO Lots of repetitive code for message attributes

MStatus SceneNode::createCountAttr(MObject &obj, const char* longName, const char* shortName){
   MFnNumericAttribute nAttr;
   MStatus ms;
   obj = intAttr(longName, shortName, 0);
   nAttr.setWritable(true);
   nAttr.setHidden(true);
   MCHECKRET(addAttribute(obj));
   return ms;
}

// TODO Remove magic strings
MStatus SceneNode::initCountAttr(){
   MCHECKRET(createCountAttr(anPoints, kAttrScenenPointsLong, kAttrScenenPointsShort));
   MCHECKRET(createCountAttr(anObjects, kAttrScenenObjectsLong, kAttrScenenObjectsShort));
   MCHECKRET(createCountAttr(anStrands, kAttrScenenStrandsLong, kAttrScenenStrandsShort));
   MCHECKRET(createCountAttr(anRigids, kAttrScenenRigidsLong, kAttrScenenRigidsShort));
   MCHECKRET(createCountAttr(anJoints, kAttrScenenJointsLong, kAttrScenenJointsShort));
   MCHECKRET(createCountAttr(anForces, kAttrScenenForcesLong, kAttrScenenForcesShort));
   MCHECKRET(createCountAttr(anConstraints, kAttrScenenConstraintsLong, kAttrScenenConstraintsShort));
   return MS::kSuccess;
}

// MStatus SceneNode::initStrandDataAttr(){
//    MStatus ms;
//    MFnNumericAttribute nAttr;
//    MFnCompoundAttribute cAttr;
//    MFnMessageAttribute mAttr;

//    aStrands = mAttr.create(kAttrSceneStrandsLong, kAttrSceneStrandsShort, &ms);

//    mAttr.setWritable(true);
//    mAttr.setArray(true);
//    mAttr.setIndexMatters(true);

//    ms = addAttribute(aStrands);
//    CHECK_MSTATUS_AND_RETURN_IT(ms);

//    return MS::kSuccess;
// }

MStatus SceneNode::initSceneMsgAttr(MObject &attrObj, const char* type){
   MStatus ms;
   MFnMessageAttribute mAttr;

   attrObj = mAttr.create(typeSceneArrString[type].c_str(), typeSceneArrShortString[type].c_str(), &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);

   mAttr.setWritable(true);
   mAttr.setArray(true);
   mAttr.setIndexMatters(true);

   ms = addAttribute(attrObj);
   CHECK_MSTATUS_AND_RETURN_IT(ms);

   return MS::kSuccess;

}

MStatus SceneNode::initCommonAttrs(){
   MFnNumericAttribute nAttr;
   MFnTypedAttribute tAttr;
   MFnStringData sData;
   MStatus ms;
   MFnEnumAttribute enumAttr;

   aDebug = nAttr.create(kAttrDebugLong, kAttrDebugShort, MFnNumericData::kBoolean, 0, &ms);
   MCHECKRET(nAttr.setKeyable(true));
   MCHECKRET(addAttribute(aDebug));

   aDebugFile = tAttr.create(kAttrDebugFileLong, kAttrDebugFileShort, MFnData::kString, sData.create(), &ms);
   MCHECKRET(addAttribute(aDebugFile));
   MFnAttribute attr(aDebugFile);
   CHECK_MSTATUS(attr.setUsedAsFilename(true));

   aIndicatorsFile = tAttr.create(kAttrIndicatorsFileLong, kAttrIndicatorsFileShort, MFnData::kString, sData.create(), &ms);
   MCHECKRET(addAttribute(aIndicatorsFile));
   MFnAttribute attr2(aIndicatorsFile);
   CHECK_MSTATUS(attr2.setUsedAsFilename(true));

   aReplaceLog = nAttr.create(kAttrReplaceLogLong, kAttrReplaceLogShort, MFnNumericData::kBoolean, 0, &ms);
   MCHECKRET(addAttribute(aReplaceLog));

   // Add scale: default 1.0
   aScale = nAttr.create(kAttrScaleLong, kAttrScaleShort, MFnNumericData::kDouble, DEFAULT_SCALE, &ms);
   addAttribute(aScale);

   // aSolver = enumAttr.create(kAttrSolverLong, kAttrSolverShort, 0, &ms);
   // MCHECKRET(addAttribute(aSolver));
   enumAttr.addField(kAttrSolverLinear, (int) Solver::LINEAR);
   enumAttr.addField(kAttrSolverQPMosek,(int) Solver::QPMOSEK);

   return MS::kSuccess;
}

// MStatus SceneNode::initRigidDataAttr(){
//    MStatus ms;
//    MFnMessageAttribute mAttr;

//    aRigids = mAttr.create(kAttrSceneRigidsLong, kAttrSceneRigidsShort, &ms);
//    mAttr.setWritable(true);
//    mAttr.setArray(true);
//    mAttr.setIndexMatters(true);

//    /** Add the attribute to the SceneNode **/
//    CHECK_MSTATUS_AND_RETURN_IT(addAttribute(aRigids));
//    return MS::kSuccess;
// }

// MStatus SceneNode::initPointDataAttr(){
//    MStatus ms;
//    MFnMessageAttribute mAttr;

//    aPoints = mAttr.create(kAttrScenePointsLong, kAttrScenePointsShort, &ms);
//    CHECK_MSTATUS_AND_RETURN_IT(ms);

//    mAttr.setWritable(true);
//    mAttr.setArray(true);
//    mAttr.setIndexMatters(true);

//    CHECK_MSTATUS_AND_RETURN_IT(addAttribute(aPoints));
//    return MS::kSuccess;
// }

// MStatus SceneNode::initJointDataAttr(){
//    MStatus ms;
//    MFnMessageAttribute mAttr;

//    aJoints = mAttr.create("Joints", "jt", &ms);
//    mAttr.setWritable(true);
//    mAttr.setArray(true);
//    mAttr.setIndexMatters(true);

//    CHECK_MSTATUS_AND_RETURN_IT(addAttribute(aJoints));
//    return MS::kSuccess;
// }

// MStatus SceneNode::initForceDataAttr(){
//   MStatus ms;
//   MFnMessageAttribute mAttr;
//   aForces = mAttr.create(kAttr, "f", &ms);
//   mAttr.setWritable(true);
//   mAttr.setArray(true);
//   mAttr.setIndexMatters(true);

//   CHECK_MSTATUS_AND_RETURN_IT(addAttribute(aForces));
//   return MS::kSuccess;
// }

// MStatus SceneNode::initConstDataAttr(){
//    MStatus ms;
//    MFnMessageAttribute mAttr;
//    aConstraints = mAttr.create(kAttrSceneConstraintsLong, kAttrSceneConstraintsShort, &ms);
//    mAttr.setWritable(true);
//    mAttr.setArray(true);
//    mAttr.setIndexMatters(true);

//    CHECK_MSTATUS_AND_RETURN_IT(addAttribute(aConstraints));
//    return MS::kSuccess;
// }
