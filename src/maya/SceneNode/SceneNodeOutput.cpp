#include <SceneNode/SceneNode.hpp>
#include <MayaHelper.hpp>
#include <SE3.hpp>
#include <maya/MDGModifier.h>
#include <maya/MFnAnimCurve.h>
#include <algorithm>
#include <numeric>
#include <tuple>
#include <functional>

 //   MObject vObjs[], std::string plugStrings[], bool vCheck[], int vSize[],
 // std::function<double (SP<SimFrame> frame, int iC)> vFns[],
inline double getPTransVal (SP<SimFrame> frame, int pid, int t){return frame->vPOut[pid].x[t];}
inline double getPVelVal   (SP<SimFrame> frame, int pid, int t){return frame->vPOut[pid].v[t];}
inline double getPDoFPosVal(SP<SimFrame> frame, int pid, int t){return frame->vPOut[pid].dofPos[t];}
inline double getPDoFAngVal(SP<SimFrame> frame, int pid, int t){return frame->vPOut[pid].dofAng[t];}

MStatus SceneNode::outputPointsData(SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap pointObjs){
   MFnDependencyNode sceneDN(thisMObject());
   MStatus ms;
   MPlug MPLUG_RET(pPlugs, sceneDN, kAttrScenePointsLong);
   MObject outPointObj, outPointShapeObj, pointObj;
   MPlug plug;
   MFnDependencyNode pointDN;
   auto nFrames = frames.size();
   for(auto pointObjPair: pointObjs){
      auto pointObj = pointObjPair.second;
      auto pid = pointObjPair.first;
      pointDN.setObject(pointObj);
      // cout<<"Getting point "<<pid<<" "<<pointDN.name()<<endl;
      MCHECKRET(findOutput(outPointObj, pointObj));
      int GET_VAL2(pid2, pointDN, kAttrPointIDLong, Int);
      MCHECKERROR_B(pid!=pid2, "Bad pid.");

      MFnDependencyNode outPointDN(outPointObj), outConstDN;

      // Constraint related code
      MObject constObj, rigidObj, outConstObj, outConstTransObj;
      bool bConst = false;
      if(MFAIL(findPointConstraint(rigidObj, constObj, pointObj))){
         log_verbose("No constraint for point %s", DNName(pointDN));
      } else{
         bConst = true;
         MCHECKWARN(findOutput(outConstObj, constObj),
                    "Constraint output not found for %s", MObjName(constObj));
         MCHECKWARN(getTransform(outConstTransObj, outConstObj), "No transform");
         outConstDN.setObject(outConstObj);
      }

      size_t      nAttrs = 4;
      MObject     vObjs[]       = {outPointObj,    outPointObj,     outConstObj,    outConstObj};
      std::string plugStrings[] = {kAttrTransLong, kAttrPosVelLong, kAttrTransLong, kAttrRotLong};
      // Some output required only if constraint found.
      bool        vCheck[]      = {true,           true,            bConst,         bConst};
      int         vSize[]       = {3,              3,               3,              3};

      std::function<double (SP<SimFrame> frame, int iC)> vFns[] =
         {[pid] (SP<SimFrame> frame, int t) {return getPTransVal (frame, pid, t);},
          [pid] (SP<SimFrame> frame, int t) {return getPVelVal   (frame, pid, t);},
          [pid] (SP<SimFrame> frame, int t) {return getPDoFPosVal(frame, pid, t);},
          [pid] (SP<SimFrame> frame, int t) {return getPDoFAngVal(frame, pid, t);}};
      if(!bConst) nAttrs = 2;
      std::vector<AttrTuple<double>> vAttrTuples;
      MCHECKERROR(collectSubPlugs(vAttrTuples, nAttrs, vObjs, plugStrings, vCheck, vSize, vFns, collectChildPlugs),
                  "Could not collect child plugs for point related attributes");
      MCHECKERROR(updateAttrs(vAttrTuples, frames, tArr), "Could not output point attrs.");
   }

   return MS::kSuccess;
}

inline double getSStrainVal     (SP<SimFrame> frame, int sid){return frame->vSOut[sid].strain;}
inline double getSConstForceVal (SP<SimFrame> frame, int sid){return frame->vSOut[sid].constForce;}
inline int    getSConstActiveVal(SP<SimFrame> frame, int sid){return frame->vSOut[sid].inexActive;}

inline int getSExtraNodeIdxVal(SP<SimFrame> frame, int sid, int idx){
   if(idx<frame->vSOut[sid].extraNodes.size()) return frame->vSOut[sid].extraNodes[idx].first;
   return -1;
}

inline double getSExtraNodePosVal (SP<SimFrame> frame, int sid, int t, int idx){
   if(idx<frame->vSOut[sid].extraNodes.size()) return frame->vSOut[sid].extraNodes[idx].second[t];
   return 0.0;
}

MStatus SceneNode::outputStrandsData
(SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap strandObjs, SimParams* simParams){
   MFnDependencyNode sceneDN(thisMObject()), strandDN;
   MStatus ms;
   MPlug MPLUG_RET(sPlugs, sceneDN, kAttrSceneStrandsLong);
   MObject outStrandObj, outStrandShapeObj, strandObj;
   auto nFrames = frames.size();

   for(auto strandObjPair: strandObjs){
      auto sid = strandObjPair.first;
      auto strandObj = strandObjPair.second;
      strandDN.setObject(strandObj);
      // cout<<"Getting strand "<<sid<<" "<<strandDN.name()<<endl;
      MCHECKRET(findOutput(outStrandObj, strandObj));
      MCHECKRET(getShape(outStrandShapeObj, outStrandObj));
      int GET_VAL2(sid2, strandDN, kAttrStrandIDLong, Int);
      MCHECKERROR_B(sid!=sid2,"Bad sid.");

      MFnDependencyNode outStrandDN(outStrandObj), outStrandShapeDN(outStrandShapeObj);

      MPlug MPLUG_RET(outStrainPlug, outStrandDN, kAttrStrainLong);
      MPlug MPLUG_RET(outConstForcePlug, outStrandDN, kAttrSConstForceLong);
      MPlug MPLUG_RET(outConstActivePlug, outStrandDN, kAttrSInexActiveLong);
      std::vector<AttrTuple<double>> vAttrDblTuples;
      std::vector<AttrTuple<int   >> vAttrIntTuples;
      vAttrDblTuples.push_back(
         std::make_tuple(outStrainPlug,
                         [sid] (SP<SimFrame> frame){return getSStrainVal(frame, sid);}));
      vAttrDblTuples.push_back(
         std::make_tuple(outConstForcePlug,
                         [sid] (SP<SimFrame> frame){return getSConstForceVal(frame, sid);}));
      
      MPlug MPLUG_RET(outExtraNodesPosPlug, outStrandShapeDN, kAttrExtraNodesPosLong);
      MPlug MPLUG_RET(outExtraNodesIdxPlug, outStrandShapeDN, kAttrExtraNodesIdxLong);

      for(int iE = 0; iE<simParams->getMaxStrandNodes(sid); iE++){
         MPlug ePlug  = outExtraNodesPosPlug.elementByLogicalIndex(iE);
         MPlug ePlug2 = outExtraNodesIdxPlug.elementByLogicalIndex(iE);
         for(int iC=0; iC<3; iC++){
            MPlug cPlug = ePlug.child(iC);
            UpdateFn<double> fn = [iC, iE, sid] (SP<SimFrame> frame)
                                     {return getSExtraNodePosVal(frame, sid, iC, iE);};
            vAttrDblTuples.push_back(std::make_tuple(cPlug, fn));
         }
         UpdateFn<int> fn = [iE, sid] (SP<SimFrame> frame)
                               {return getSExtraNodeIdxVal(frame, sid, iE);};
         vAttrIntTuples.push_back(std::make_tuple(ePlug2, fn));
      }
      vAttrIntTuples.push_back(
         std::make_tuple(outConstActivePlug,
                         [sid] (SP<SimFrame> frame){return getSConstActiveVal(frame, sid);}));

      MCHECKERROR(updateAttrs(vAttrDblTuples, frames, tArr), "Could not output strand dbl attrs.");
      MCHECKERROR(updateAttrs(vAttrIntTuples, frames, tArr), "Could not output strand int attrs.");
   }

   return MS::kSuccess;
}
// inline int getSExtraNodePosVal (SP<SimFrame> frame, int sid, int t, int idx){return frame->vSOut[sid].strain;}

inline double getRTransVal (SP<SimFrame> frame, int rid, int t){return frame->vROut[rid].pos[t];}
inline double getRRotVal   (SP<SimFrame> frame, int rid, int t){return frame->vROut[rid].ang[t]*180.0/M_PI;}
inline double getRPosVelVal(SP<SimFrame> frame, int rid, int t){return frame->vROut[rid].v[t+3];}
inline double getRAngVelVal(SP<SimFrame> frame, int rid, int t){return frame->vROut[rid].v[t];}

MStatus SceneNode::outputRigidsData(SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap rigidObjs){
   MFnDependencyNode sceneDN(thisMObject()), rigidDN;
   MStatus ms;
   MPlug MPLUG_RET(rPlugs, sceneDN, kAttrSceneRigidsLong);
   MPlug plug;
   MObject outRigidObj, outRigidShapeObj, rigidObj;
   auto nFrames = frames.size();

   for(auto rigidObjPair: rigidObjs){
      auto rigidObj = rigidObjPair.second;
      auto rid = rigidObjPair.first;
      rigidDN.setObject(rigidObj);
      // cout<<"Getting rigid "<<rid<<" "<<rigidDN.name()<<endl;
      MCHECKRET(findOutput(outRigidObj, rigidObj));
      int GET_VAL2(rid2, rigidDN, kAttrRigidIDLong, Int);
      MCHECKERROR_B(rid!=rid2,"Bad rid.");

      MFnDependencyNode outRigidDN(outRigidObj);

      size_t nAttrs = 4;
      MObject     vObjs[]       = {outRigidObj,    outRigidObj,  outRigidObj,     outRigidObj};
      std::string plugStrings[] = {kAttrTransLong, kAttrRotLong, kAttrPosVelLong, kAttrAngVelLong};
      bool        vCheck[]      = {true,           true,         true,            true};
      int         vSize[]       = {3,              3,            3,               3};
      std::function<double (SP<SimFrame> frame, int iC)> vFns[] =
         {[rid] (SP<SimFrame> frame, int iC) {return getRTransVal(frame, rid, iC);},
          [rid] (SP<SimFrame> frame, int iC) {return getRRotVal(frame, rid, iC);},
          [rid] (SP<SimFrame> frame, int iC) {return getRPosVelVal(frame, rid, iC);},
          [rid] (SP<SimFrame> frame, int iC) {return getRAngVelVal(frame, rid, iC);}};

      std::vector<AttrTuple<double>> vAttrTuples;
      MCHECKERROR(collectSubPlugs(vAttrTuples, nAttrs, vObjs, plugStrings, vCheck, vSize, vFns, collectChildPlugs),
                  "Could not collect child plugs for rigid related attributes");
      MCHECKERROR(updateAttrs(vAttrTuples, frames, tArr), "Could not output rigid attrs.");
   }

   return MS::kSuccess;
}

inline double getJTransVal   (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].pos[t];}
inline double getJRotVal     (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].ang[t]*180.0/M_PI;}
inline double getJAngVal     (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].angles[t];}

inline double getJPosVelVal  (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].v[t+3];}
inline double getJAngVelVal  (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].v[t];}
inline double getJPosDriftVal(SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].drift[t+3];}
inline double getJAngDriftVal(SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].drift[t];}

inline double getJPosAVal    (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].posA[t];}
inline double getJAngAVal    (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].angA[t]*180.0/M_PI;}
inline double getJPosBVal    (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].posB[t];}
inline double getJAngBVal    (SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].angB[t]*180.0/M_PI;}
inline double getJLimActiveVal(SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].exceeded[t];}
inline double getJConstForce(SP<SimFrame> frame, int jid, int t){return frame->vJOut[jid].constForce[t];}

MStatus SceneNode::outputJointsData(SPVEC<SimFrame> frames, MTimeArray *tArr, IdObjMap jointObjs){
   MFnDependencyNode sceneDN(thisMObject()), jointDN;
   MStatus ms;
   MPlug MPLUG_RET(jPlugs, sceneDN, kAttrSceneJointsLong);
   MPlug plug;
   MObject outJointObj, outJointShapeObj, jointObj;
   auto nFrames = frames.size();

   for(auto jointObjPair: jointObjs){
      auto jointObj = jointObjPair.second;
      auto jid = jointObjPair.first;
      jointDN.setObject(jointObj);
      // cout<<"Getting joint "<<jid<<" "<<jointDN.name()<<endl;
      MCHECKRET(findOutput(outJointObj, jointObj));
      int GET_VAL2(jid2, jointDN, kAttrJointIDLong, Int);
      MCHECKERROR_B(jid!=jid2,"Bad jid.");
      MObject debugTransObjA, debugTransObjB;
      MCHECKRET(findMessageDestination(debugTransObjA, kAttrJointDebugALong, outJointObj));
      MCHECKRET(findMessageDestination(debugTransObjB, kAttrJointDebugBLong, outJointObj));

      MFnDependencyNode outJointDN(outJointObj);

      size_t nAttrs = 13;
      MObject vObjs[] = {outJointObj, outJointObj, outJointObj,
                         outJointObj, outJointObj, outJointObj, outJointObj,
                         debugTransObjA, debugTransObjA, debugTransObjB, debugTransObjB,
                         outJointObj, outJointObj};
      std::string plugStrings[] =
         {kAttrTransLong, kAttrRotLong, kAttrJAnglesLong,
          kAttrPosVelLong, kAttrAngVelLong, kAttrDriftPosLong, kAttrDriftAngLong,
          kAttrTransLong, kAttrRotLong, kAttrTransLong, kAttrRotLong,
          kAttrJLimActiveLong, kAttrJConstForceLong};
      bool vCheck[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
      int  vSize[]  = {3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 6};
      std::function<double (SP<SimFrame> frame, int iC)> vFns[] =
         {[jid] (SP<SimFrame> frame, int iC) {return getJTransVal   (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJRotVal     (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJAngVal     (frame, jid, iC);},

          [jid] (SP<SimFrame> frame, int iC) {return getJPosVelVal  (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJAngVelVal  (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJPosDriftVal(frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJAngDriftVal(frame, jid, iC);},

          [jid] (SP<SimFrame> frame, int iC) {return getJPosAVal    (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJAngAVal    (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJPosBVal    (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJAngBVal    (frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJLimActiveVal(frame, jid, iC);},
          [jid] (SP<SimFrame> frame, int iC) {return getJConstForce (frame, jid, iC);},
         };
      AttrTuple<double> attrTuples[nAttrs];
      std::vector<AttrTuple<double>> vAttrTuples;
      MCHECKERROR(collectSubPlugs(vAttrTuples, nAttrs, vObjs, plugStrings, vCheck, vSize, vFns, collectChildPlugs),
                  "Could not collect child plugs for rigid related attributes");
      MCHECKERROR(updateAttrs(vAttrTuples, frames, tArr), "Could not output rigid attrs.");
   }

   return MS::kSuccess;
}
    

MStatus SceneNode::setStrandExtraSize(int iSim){
   MFnDependencyNode sceneDN(thisMObject());
   MStatus ms;
   MObject strandObj;
   MPlug MPLUG_RET(strandsPlug, sceneDN, kAttrSceneStrandsLong);
   MObject outObj, outShapeObj;
   MFnDependencyNode outDN, shapeDN, outShapeDN;

   for(auto iStrand=0; iStrand<strandsPlug.numElements(); iStrand++){
      ms = getObjectFromPlugArray(strandObj, strandsPlug, iStrand);
      if(!MFAIL(ms)){
         if(MFAIL(checkStrand(strandObj))) continue;
         int GET_VAL2(sid, MFnDependencyNode(strandObj), kAttrStrandIDLong, Int);
         ms = findOutput(outObj, strandObj);
         ms = getShape(outShapeObj, outObj);
         outDN.setObject(outObj);
         outShapeDN.setObject(outShapeObj);
         MPlug MPLUG_RET(outENodesPosPlug, outShapeDN, kAttrExtraNodesPosLong);
         MPlug MPLUG_RET(outENodesIdxPlug, outShapeDN, kAttrExtraNodesIdxLong);
         size_t nENodes = outENodesPosPlug.numElements();
         MDGModifier dgMod;
         for(uint iPlug = 0; iPlug < nENodes; iPlug++){
            // REQUIRED_CMD("removeMultiInstance " + outStrandShapeDN.name() + "." kAttrExtraNodesIdxLong "[" + iPlug + "]");
            // REQUIRED_CMD("removeMultiInstance " + outStrandShapeDN.name() + "." kAttrExtraNodesPosLong "[" + iPlug + "]");
            MPlug posPlug = outENodesPosPlug.elementByLogicalIndex(iPlug, &ms);
            MCHECKERROR(ms, "Could not get element.");
            MCHECKERROR(dgMod.removeMultiInstance(posPlug, true), "Could not remove element. Maybe connected.");
            MPlug idxPlug = outENodesIdxPlug.elementByLogicalIndex(iPlug, &ms);
            MCHECKERROR(ms, "Could not get element.");
            MCHECKERROR(dgMod.removeMultiInstance(idxPlug, true), "Could not remove element. Maybe connected.");
            MCHECKERROR(dgMod.doIt(), "Failed to clear child plugs.");
         }
         MPlug MPLUG_RET(idxPlugs, outShapeDN, kAttrExtraNodesIdxLong);
         int maxVal = this->mSimParams[iSim]->getMaxStrandNodes(sid);
         if(maxVal == -1) log_err("Strand not found.");
         for(int iPlug=0; iPlug<maxVal; iPlug++){
            MPlug plug = idxPlugs.elementByLogicalIndex(iPlug, &ms);
            MCHECKERROR(ms, "Could not get plug");
            ms = plug.setValue(-1);
            MCHECKERROR(ms, "Could not set value");
         }
      }               
      else{
         log_err("Could not get Strand object from plug");
      }
   }
   return MS::kSuccess;
}

MStatus SceneNode::outputResult(int iSim){
   // TODO Check for completion state
   MStatus ms;
   int step = 0;
   MCHECKERROR_B(mSimParams.size()<=iSim || mSimParams[iSim]==nullptr,
                 "Could not find the result");
   SimParams* simParams = mSimParams[iSim];
   auto points  = simParams->getPointMObjMap();
   auto rigids  = simParams->getRigidMObjMap();
   auto joints  = simParams->getJointMObjMap();
   auto strands = simParams->getStrandMObjMap();
   auto modString = simParams->getModString();
   MCHECKRET(applyModString(modString));

   // Clear anim curves for strain
   MFnDependencyNode sceneDN(thisMObject());
   MPlug MPLUG_RET(sPlugs, sceneDN, kAttrSceneStrandsLong);
   MObject obj, outStrandObj, outStrandShapeObj;
   int nStrands = 0;
   auto frames(simParams->getFrames());
   auto nFrames = frames.size();

   // Create time range.
   MTimeArray tArr;
   MDoubleArray dArr;
   dArr.setLength(nFrames);

   std::vector<MTime> vTimeVals(nFrames);
   IVec vRange(nFrames);
   std::iota(vRange.begin(), vRange.end(), 1);
   double dt = simParams->getLargeTimestep();
   std::transform(vRange.begin(), vRange.end(), vTimeVals.begin(),
                  [dt](auto i) {return MTime(i/24.0, MTime::Unit::kSeconds);});
   tArr = MTimeArray(vTimeVals.data(), vTimeVals.size());

   int iStrand = 0;
   for(int i=0; i<sPlugs.numElements(); i++){
      if(MFAIL(getObjectFromPlugArray(obj, sPlugs, i))) continue;
      MCHECKRET(findOutput(outStrandObj, obj));
      MCHECKERROR(getShape(outStrandShapeObj, outStrandObj), "Could not get shape for strand transform.");
      MFnDependencyNode outStrandDN(outStrandObj);
      MFnDependencyNode outStrandShapeDN(outStrandShapeObj);
      MPlug MPLUG_RET(strainPlug, outStrandDN, kAttrStrainLong);
      MCHECKWARN(clearAnimCurve(strainPlug), "Could not clear strain for %s", DNName(outStrandDN));
      MPlug MPLUG_RET(extraNodesIdxPlug, outStrandShapeDN, kAttrExtraNodesIdxLong);
      MPlug MPLUG_RET(extraNodesPosPlug, outStrandShapeDN, kAttrExtraNodesPosLong);
      MCHECKWARN(clearAnimCurve(extraNodesIdxPlug),
                 "Could not clear extra nodes idx for %s", DNName(outStrandShapeDN));
      MCHECKWARN(clearAnimCurve(extraNodesPosPlug),
                 "Could not clear extra nodes pos for %s", DNName(outStrandShapeDN));
      iStrand++;
   }

   MPlug MPLUG_RET(jPlugs, sceneDN, kAttrSceneJointsLong);
   MObject outJointObj, outJointShapeObj;

   MCHECKWARN(setStrandExtraSize(iSim),                             "Could not set strands' extra size.");
   MCHECKWARN(outputPointsData(frames, &tArr, points),              "Could not output points data.");
   MCHECKWARN(outputRigidsData(frames, &tArr, rigids),              "Could not output rigids data.");
   MCHECKWARN(outputJointsData(frames, &tArr, joints),              "Could not output joints data.");
   MCHECKWARN(outputStrandsData(frames, &tArr, strands, simParams), "Could not output joints data.");

   log_reg("Output applied.");

   return MS::kSuccess;
}

MStatus SceneNode::deleteOutput(int iSim){
   MStatus ms;
   MFnDependencyNode sceneDN(thisMObject());
   MCHECKERROR_B((iSim<-1) || (iSim>=(int)mSimParams.size()),"Simulation id invalid");

   MPlug nResultsPlug = sceneDN.findPlug(kAttrScenenResultsLong, ms);
   MCHECKRET(ms);
   int r; MCHECKRET(nResultsPlug.getValue(r));
   // Delete all simulations
   if(iSim==-1){
      int i=0;
      for(auto sim: mSimParams){cout<<i++<<endl; delete sim;}
      mSimParams.clear();
      MCHECKRET(nResultsPlug.setValue(0));
      return MS::kSuccess;
   }
   // mSimParams[iSim].reset();
   mSimParams.erase(mSimParams.begin()+iSim);
   MCHECKRET(nResultsPlug.setValue(r-1));
   return MS::kSuccess;
}

#define MCHECK_MOBJECT(OBJ) if(OBJ.isNull()) return MS::kFailure;

MStatus SceneNode::checkStrand(MObject strandObj){
   MStatus ms;
   MCHECK_MOBJECT(strandObj);
   int GET_VAL2(enabled, MFnDependencyNode(strandObj), kAttrEnabledLong, Bool);
   if (!enabled) ms = MS::kFailure;
   return ms;
}

MStatus SceneNode::checkJoint(MObject jointObj){MCHECK_MOBJECT(jointObj); return MS::kSuccess;}
MStatus SceneNode::checkRigid(MObject rigidObj){MCHECK_MOBJECT(rigidObj); return MS::kSuccess;}
MStatus SceneNode::checkPoint(MObject pointObj){MCHECK_MOBJECT(pointObj); return MS::kSuccess;}

int SceneNode::getnType
(MObject obj, const char* attrName, const char* nAttrName, MStatus *retMS){
   MFnDependencyNode DN(obj);
   MStatus& ms = *retMS;

   MPlug MPLUG_RET(attrPlug, DN, attrName);
   int GET_VAL2(n2, DN, nAttrName, Int);
   int n  = attrPlug.numElements();
   MCHECKWARN_B(n==n2, "Num elements(%d) does not match. Assuming %s is correct",
                n, nAttrName);
   return n2;
}

#define getnFn(OBJ, TYPE) getnType(OBJ, kAttrScene##TYPE##Long, kAttrScenen##TYPE##Long, &ms);

MStatus SceneNode::collectChildPlugs(std::vector<MPlug>& vRetPlugs, MPlug inPlug, int nC){
   vRetPlugs.clear();
   for(int i=0; i<nC; i++){
      vRetPlugs.push_back(inPlug.child(i));
   }
   return MS::kSuccess;
}

MStatus SceneNode::collectArrayElementPlugs(std::vector<MPlug>& vRetPlugs, MPlug inPlug, int nE){
   vRetPlugs.clear();
   for(int i=0; i<nE; i++){
      vRetPlugs.push_back(inPlug.elementByLogicalIndex(i));
   }
   return MS::kSuccess;
}
