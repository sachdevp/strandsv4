#include <maya/MDGModifier.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MPlugArray.h>
#include <maya/MPoint.h>
#include <maya/MDagPath.h>
#include <maya/MFnDagNode.h>
#include <include_common.hpp>
#include "SceneNode.hpp"
#include "MayaHelper.hpp"
#include "MayaStrings.h"
#include <Creator/Creator.hpp>
#include <SE3.hpp>

using namespace std;
using namespace Eigen;

MTypeId SceneNode::id(SCENE_NODE_ID);
MObject SceneNode::aStrands;
MObject SceneNode::aPoints;
MObject SceneNode::aRigids;
MObject SceneNode::aJoints;
MObject SceneNode::aForces;
MObject SceneNode::aConstraints;

// Debug related attrs
MObject SceneNode::aDebug;
MObject SceneNode::aDebugFile;
MObject SceneNode::aIndicatorsFile;
MObject SceneNode::aReplaceLog;

MObject SceneNode::aScale;
MObject SceneNode::aGravity;
MObject SceneNode::aTimestep;
MObject SceneNode::aDamping;
MObject SceneNode::aStab;
MObject SceneNode::aTimestepMultiplier;
MObject SceneNode::aSolver;

// Count attrs
MObject SceneNode::anStrands;
MObject SceneNode::anPoints;
MObject SceneNode::anObjects;
MObject SceneNode::anRigids;
MObject SceneNode::anJoints;
MObject SceneNode::anForces;
MObject SceneNode::anConstraints;

MObject SceneNode::anResults;

void* SceneNode::creator(){return new SceneNode;}
MStatus SceneNode::compute(const MPlug& plug, MDataBlock& data){
   if(plug == anResults){
      MDataHandle nresDH = data.outputValue(plug);
      int& nRes = nresDH.asInt();
      nRes = (int)mSimParams.size();
   }
   data.setClean(plug);
   return MS::kSuccess;
}

int SceneNode::getId(const char* type){
   MFnDependencyNode sceneDN(thisMObject());
   MStatus ms;
   MPlug nPlug(thisMObject(), typeSceneCountAttr[type]);
   int GET_VAL(n, nPlug, Int);
   int id = n; n++;
   nPlug.setValue(n);
   return id;
}

MStatus SceneNode::connectToScene(int &tid, int &oid, MObject obj, const char* type){
   MStatus ms;
   MFnMessageAttribute mAttr;
   MDGModifier dgMod;
   MFnDependencyNode DN(obj);
   MObject sceneAttr = mAttr.create(kAttrSceneLong, kAttrSceneShort, &ms);
   MCHECKRET(ms);

   MCHECKRET(dgMod.addAttribute(obj, sceneAttr));
   MCHECKRET(dgMod.doIt());

   tid = getId(type); oid = getId("object");

   MPlug arrPlug(thisMObject(), typeSceneArrAttr[type]);
   MPlug arrIndPlug = arrPlug.elementByLogicalIndex(tid, &ms);
   MObject arrIndAttr = arrIndPlug.asMObject(MDGContext::fsNormal, &ms);
   MPlug scenePlug(obj, sceneAttr);

   MCHECKERROR(DN.findPlug(typeIdString[type].c_str()).setValue(tid),
               "Could not set type id value.");
   MCHECKERROR(DN.findPlug(kAttrObjectIDLong).setValue(oid),
               "Could not set object id value.");

   MCHECKRET(dgMod.connect(scenePlug, arrIndPlug));
   MCHECKRET(dgMod.doIt());
   return MS::kSuccess;
}

// Sets the PointId and ObjectId fields for the point.
// Also sets them as Lagrangian or Quasistatic
MStatus SceneNode::addPoint(int &pid, int &oid, MObject transObj, bool _LorQ){
   MStatus ms;
   MFnDependencyNode transDN, sceneDN;
   MFnEnumAttribute enumAttr;
   string str;
   MString locatorName;

   transDN.setObject(transObj);
   locatorName = transDN.name();

   // Mark it as an input object of type POINT
   MObject typeAttr;
   MCHECKRET(addTypeAttr(typeAttr, transObj, ObjType::POINT));
   MObject velAttr;
   

   MCHECKERROR(connectToScene(pid, oid, transObj, "point"),
               "Could not connect %s to scene", locatorName.asChar());

   MObject LorQ = enumAttr.create( kAttrLorQLong, kAttrLorQShort, 0, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   enumAttr.setObject(LorQ);
   MCHECKRET(transDN.addAttribute(LorQ, MLOCAL));
   MPlug lqPlug(transObj, LorQ);
   enumAttr.addField("Lagrangian" , 1);
   enumAttr.addField("Quasistatic", 0);
   enumAttr.setDefault(_LorQ);

   return ms;
}

MStatus SceneNode::addStrand(int &sid, int &oid, MObject &inStrandObj, MObject &outStrandObj){
   log_verbose("Adding strand to SceneNode");
   MObject typeAttr;
   MCHECKRET(addTypeAttr(typeAttr, inStrandObj, ObjType::STRND));
   MCHECKERROR_B(inStrandObj.isNull() || outStrandObj.isNull()
                 || inStrandObj == outStrandObj, "Bad objects provided.");
   connectToScene(sid, oid, inStrandObj, "strand");
   return MS::kSuccess;
}

MStatus SceneNode::addRigid
(int &rid, int &oid, MObject &inMeshObj, MObject &outMeshObj,
 MObject &inTransObj, MObject &outTransObj){
   log_verbose("Adding rigid to SceneNode");
   MObject typeAttr;
   MCHECKRET(addTypeAttr(typeAttr, inTransObj, ObjType::RIGID));

   MCHECKERROR_B(inMeshObj.isNull() || outMeshObj.isNull()
                 || inMeshObj == outMeshObj,"Bad objects provided");
   MCHECKERROR_B(inTransObj.isNull() || outTransObj.isNull()
                 || inTransObj == outTransObj, "Bad objects provided");

   // Add object and strand id to strand
   return connectToScene(rid, oid, inTransObj, "rigid");
}

MStatus SceneNode::addJoint
(int &jid, int &oid, MObject &jointObj, int nObjects, MObject rigidTransformObj[2], MString mOptJointType){
   log_verbose("Adding joint to SceneNode");
   // Add object and strand id to strand
   MCHECKRET(connectToScene(jid, oid, jointObj, "joint"));
   MFnDependencyNode jointDN(jointObj), rigidDN[2];
   MString jointName = jointDN.name();

   MObject typeAttr;
   MCHECKRET(addTypeAttr(typeAttr, jointObj, ObjType::JOINT));


   rigidDN[0].setObject(rigidTransformObj[0]);
   MString rigid0Name = rigidDN[0].name();

   // Set parent and child of the joint
   // NOTE Some of this might be redundant
   // FIXME Redo this with MDGModifier
   REQUIRED_CMD("connectAttr \"" + rigid0Name + "." kAttrRigidIDShort "\" "
                + jointName + "." kAttrJointChildIDShort);

   if(nObjects == 2){
      rigidDN[1].setObject(rigidTransformObj[1]);
      MString rigid1Name = rigidDN[1].name();
      REQUIRED_CMD("connectAttr \"" + rigid1Name + ".rid\" " + jointName + ".parentId");
   } else{ // If only one object, the connect joint to world
      REQUIRED_CMD("setAttr \"" + jointName + ".parentId\" " + (uint)RigidBodies::World);
   }
   return MS::kSuccess;
}

MStatus SceneNode::addForce
(int &fid, int &oid, MObject forceTransObj){
   MObject typeAttrObj;
   MCHECKRET(addTypeAttr(typeAttrObj, forceTransObj, ObjType::FORCE));
   return connectToScene(fid, oid, forceTransObj, "force");
}

MStatus SceneNode::addConstraint
(int &cid, int &oid, MObject constObj, MObject dynObj){
   MObject typeAttrObj;
   MCHECKRET(addTypeAttr(typeAttrObj, constObj, ObjType::CONST));
   return connectToScene(cid, oid, constObj, "constraint");
}

// Doesn't do anything for now. This is for any possible internal processing if
// ever needed.
MStatus SceneNode::attachForce (MObject forceObj, MObject obj){return MS::kSuccess;}

// Fixing the point to the rigid object
// NOTE This function as of now seems useless
MStatus SceneNode::assignToRigid
(MObject pointObj, MObject rigidObj){
   MFnDependencyNode pointDN(pointObj);
   MStatus ms;
   return MS::kSuccess;
}

MStatus SceneNode::findPointConstraint
(MObject &rigidObj, MObject &constObj, MObject pointObj){
   MStatus ms = MS::kSuccess;
   MFnDependencyNode pointDN(pointObj), constDN, rigidDN;
   rigidObj = MObject::kNullObj;

   bool fieldExists;
   ms = findConstraint(constObj, pointObj, &fieldExists);
   if(MFAIL(ms)){
      if(fieldExists) log_err("Constraint attribute but no constraint");
      return MS::kFailure;
   }

   // Debug message
   constDN.setObject(constObj); log_random("Constraint Object is %s", DNName(constDN));

   // Find rigid object for the point
   MCHECKERROR(findConstraintSource(rigidObj, constObj), "Failed to find constraint source.");

   if(rigidObj.isNull()){
      log_verbose("Found constraint %s fixed to world", DNName(constDN));
   } else{
      rigidDN.setObject(rigidObj);
      log_verbose("Found constraint %s with rigid %s", DNName(constDN), DNName(rigidDN));
   }
   return MS::kSuccess;
}

// MStatus SceneNode::setConstTransform(MObject &pointObj, M4 E){
//    // Find position and angles
//    MStatus ms;
//    MObject constObj, rigidObj, outConstObj, outConstTransObj;
//    if(MFAIL(findPointConstraint(rigidObj, constObj, pointObj))){return MS::kFailure;}

//    MCHECKERROR(findOutput(outConstObj, constObj)          , "Constraint output not found");
//    MCHECKERROR(getTransform(outConstTransObj, outConstObj), "No transform");
//    MCHECKERROR(setTransformToObj(outConstTransObj, E)     , "Could not set transform");

//    return MS::kSuccess;
// }

MStatus SceneNode::clearCount(){
   // Check that it is safe to clear the counts
   // This means you have no objects left.
   MStatus ms;
   MFnDependencyNode sceneDN(thisMObject());
   MString sceneName = sceneDN.name();
   // FIXME Set value with MPlug
   REQUIRED_CMD("setAttr \"" + sceneName + ".nPoints\" 0");
   REQUIRED_CMD("setAttr \"" + sceneName + ".nObjects\" 0");
   REQUIRED_CMD("setAttr \"" + sceneName + ".nStrands\" 0");
   REQUIRED_CMD("setAttr \"" + sceneName + ".nRigids\" 0");
   REQUIRED_CMD("setAttr \"" + sceneName + ".nForces\" 0");
   REQUIRED_CMD("setAttr \"" + sceneName + ".nJoints\" 0");
   return ms;
}

MStatus SceneNode::initialize() {
   MFnNumericAttribute nAttr;
   MStatus ms;

   // Add gravity: default 0, -9.79, 0
   aGravity = nAttr.createPoint("Gravity", "grav");
   nAttr.setDefault(DEFAULT_GRAVITY);
   MCHECKRET(addAttribute(aGravity));

   // Add timestep: default 1e-2
   aTimestep = dblAttr(kAttrSceneDtLong, kAttrSceneDtShort, DEFAULT_TIMESTEP);
   MCHECKRET(addAttribute(aTimestep));

   // Add damping: default 1e-2
   aDamping = dblAttr(kAttrSceneDampingLong, kAttrSceneDampingShort, DEFAULT_DAMPING);
   MCHECKRET(addAttribute(aDamping));

   // Add stabilization: default 1e-2
   aStab = dblAttr(kAttrSceneStabLong, kAttrSceneStabShort, DEFAULT_DAMPING);
   MCHECKRET(addAttribute(aStab));

   aTimestepMultiplier = intAttr(kAttrSceneDtxLong, kAttrSceneDtxShort, DEFAULT_MULTIPLIER);
   MCHECKRET(addAttribute(aTimestepMultiplier));

   anResults = intAttr(kAttrScenenResultsLong, kAttrScenenResultsShort, 0);
   nAttr.setWritable(true);
   nAttr.setHidden(true);
   CHECK_MSTATUS_AND_RETURN_IT(addAttribute(anResults));

   typeIdString["point"]                   = kAttrPointIDLong;
   typeIdString["strand"]                  = kAttrStrandIDLong;
   typeIdString["rigid"]                   = kAttrRigidIDLong;
   typeIdString["joint"]                   = kAttrJointIDLong;
   typeIdString["force"]                   = kAttrForceIDLong;
   typeIdString["constraint"]              = kAttrConstraintIDLong;

   typeSceneArrString["point"]             = kAttrScenePointsLong;
   typeSceneArrString["strand"]            = kAttrSceneStrandsLong;
   typeSceneArrString["rigid"]             = kAttrSceneRigidsLong;
   typeSceneArrString["joint"]             = kAttrSceneJointsLong;
   typeSceneArrString["force"]             = kAttrSceneForcesLong;
   typeSceneArrString["constraint"]        = kAttrSceneConstraintsLong;

   typeSceneArrShortString["point"]        = kAttrScenePointsShort;
   typeSceneArrShortString["strand"]       = kAttrSceneStrandsShort;
   typeSceneArrShortString["rigid"]        = kAttrSceneRigidsShort;
   typeSceneArrShortString["joint"]        = kAttrSceneJointsShort;
   typeSceneArrShortString["force"]        = kAttrSceneForcesShort;
   typeSceneArrShortString["constraint"]   = kAttrSceneConstraintsShort;

   typeSceneCountString["point"]           = kAttrScenenPointsLong;
   typeSceneCountString["strand"]          = kAttrScenenStrandsLong;
   typeSceneCountString["rigid"]           = kAttrScenenRigidsLong;
   typeSceneCountString["joint"]           = kAttrScenenJointsLong;
   typeSceneCountString["force"]           = kAttrScenenForcesLong;
   typeSceneCountString["constraint"]      = kAttrScenenConstraintsLong;

   typeSceneCountShortString["point"]      = kAttrScenenPointsShort;
   typeSceneCountShortString["strand"]     = kAttrScenenStrandsShort;
   typeSceneCountShortString["rigid"]      = kAttrScenenRigidsShort;
   typeSceneCountShortString["joint"]      = kAttrScenenJointsShort;
   typeSceneCountShortString["force"]      = kAttrScenenForcesShort;
   typeSceneCountShortString["constraint"] = kAttrScenenConstraintsShort;

   MCHECKRET(initCountAttr());
   MCHECKRET(initSceneMsgAttr(aPoints     , "point"));
   MCHECKRET(initSceneMsgAttr(aStrands    , "strand"));
   MCHECKRET(initSceneMsgAttr(aRigids     , "rigid"));
   MCHECKRET(initSceneMsgAttr(aJoints     , "joint"));
   MCHECKRET(initSceneMsgAttr(aForces     , "force"));
   MCHECKRET(initSceneMsgAttr(aConstraints, "constraint"));
   MCHECKRET(initCommonAttrs());

   typeSceneCountAttr["object"]     = anObjects;
   typeSceneCountAttr["point"]      = anPoints;
   typeSceneCountAttr["strand"]     = anStrands;
   typeSceneCountAttr["rigid"]      = anRigids;
   typeSceneCountAttr["joint"]      = anJoints;
   typeSceneCountAttr["force"]      = anForces;
   typeSceneCountAttr["constraint"] = anConstraints;

   typeSceneArrAttr["point"]        = aPoints;
   typeSceneArrAttr["strand"]       = aStrands;
   typeSceneArrAttr["rigid"]        = aRigids;
   typeSceneArrAttr["joint"]        = aJoints;
   typeSceneArrAttr["force"]        = aForces;
   typeSceneArrAttr["constraint"]   = aConstraints;

   return MS::kSuccess;
}

MStatus SceneNode::addTypeAttr(MObject &typeAttr, MObject inObj, ObjType type){
   MFnEnumAttribute enumAttr;
   MStatus ms;
   typeAttr = enumAttr.create(kAttrObjectTypeLong, kAttrObjectTypeShort, 0, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MFnDependencyNode DN(inObj);
   MCHECKRET(DN.addAttribute(typeAttr, MLOCAL));

   enumAttr.addField("Constraint", static_cast<int>(ObjType::CONST));
   enumAttr.addField("Force"     , static_cast<int>(ObjType::FORCE));
   enumAttr.addField("Joint"     , static_cast<int>(ObjType::JOINT));
   enumAttr.addField("Point"     , static_cast<int>(ObjType::POINT));
   enumAttr.addField("Rigid"     , static_cast<int>(ObjType::RIGID));
   enumAttr.addField("Strand"    , static_cast<int>(ObjType::STRND));
   enumAttr.setHidden(false);

   MPlug typePlug(inObj, typeAttr);
   typePlug.setValue(static_cast<int>(type));
   return MS::kSuccess;
}

MStatus SceneNode::getMaps(MAP<PointL>          *&points,
                           MAP<Rigid>           *&rigids,
                           MAP<Strands::Force>  *&forces,
                           MAP<Strands::Joint>  *&joints,
                           MAP<Strands::Strand> *&strands){
   points  = &(this->points);
   rigids  = &(this->rigids);
   forces  = &(this->forces);
   joints  = &(this->joints);
   strands = &(this->strands);
   return MS::kSuccess;
}
