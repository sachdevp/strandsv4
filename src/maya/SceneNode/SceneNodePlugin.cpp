#include "SceneNode.hpp"
#include <DynObj/Strand.hpp>
#include <maya/MFnPlugin.h>
#include "MayaStrings.h"

STRMAP SceneNode::typeIdString;
STRMAP SceneNode::typeSceneArrString;
STRMAP SceneNode::typeSceneArrShortString;
STRMAP SceneNode::typeSceneCountString;
STRMAP SceneNode::typeSceneCountShortString;
MOBJMAP SceneNode::typeSceneCountAttr;
MOBJMAP SceneNode::typeSceneArrAttr;

MStatus initializePlugin(MObject obj) {
  MStatus status;
  MFnPlugin plugin(obj, "Prashant Sachdeva", "1.0", "Any");

  status = plugin.registerNode("SceneNode", SceneNode::id, SceneNode::creator, SceneNode::initialize);
  CHECK_MSTATUS_AND_RETURN_IT(status);
  return status;
}

MStatus uninitializePlugin(MObject obj) {
  MStatus status;
  MFnPlugin plugin(obj);

  status = plugin.deregisterNode(SceneNode::id);
  CHECK_MSTATUS_AND_RETURN_IT(status);

  return status;
}
