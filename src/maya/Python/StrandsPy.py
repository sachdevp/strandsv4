# Strands Command Plugin
import sys
import maya.api.OpenMaya as OM
import maya.cmds as cmds
import numpy as np
import xml.dom.minidom as minidom
import xml.etree.ElementTree as ET

def print_scene(scene):
    XMLstring = ET.tostring(scene,'ascii')
    reparsed = minidom.parseString(XMLstring)
    print(reparsed.toprettyxml(indent='   ')) # Prettify the XML
    return

def matrix_to_bare_string(a):
    "Custom function to return string from numpy matrix"
    af = a.flatten()
    string = np.array2string(af, separator=' ',prefix=' ', )
    return string[1:-1:]

# Number of control vertices
# Assumes linear curve
def numCVs(crv):
    numSpans = cmds.getAttr(crv+'.spans')
    degree = cmds.getAttr(crv+'.degree')
    return( numSpans + degree)

# Are the 3 points collinear?
def areCollinear(A, B, C):
    AB = np.array(B) - np.array(A)
    AC = np.array(C) - np.array(A)
    dotp = np.dot(AB, AC);
    nm = np.linalg.norm(AB)*np.linalg.norm(AC)
    if np.allclose(nm, dotp):
        return True;
    if np.allclose(nm, -dotp):
        print('Out of order in checking collinearity');
        return True;
    return False;

# Return the closest CV to the position. Used to select the control vertex after
# creating a new knot
def findCVIndex(crv, pos):
    n = numCVs(crv);
    print('Looking through %d CVsin %s' %( n, crv))
    for i in range(n-1):
        cvi = crv + ('.cv[%d]'%i);
        cvip = crv + ('.cv[%d]'%(i+1));
        cviPos = cmds.pointPosition(cvi)
        cvipPos= cmds.pointPosition(cvip)
        if areCollinear(cviPos, pos, cvipPos):
            return (i+1)
        print('ERROR: Cannot find CV Index')
    return -1;

##########################################################
# Plug-in
##########################################################
class StrandsPyClass( OM.MPxCommand ):
    kPluginCmdName = 'StrandsPy'
    kFlagLongName = ['-command', '-name', '-scene']
    kFlagShortName = ['-c', '-n', '-s']
    kCommandArg = 0;
    kNameArg = 1;
    kLocatorScale = 0.1;
    kDebug = True;
    ## Maya Argument types list
    # OpenMaya.MSyntax.kAngle = 8
    # OpenMaya.MSyntax.kBoolean = 2
    # OpenMaya.MSyntax.kDistance = 7
    # OpenMaya.MSyntax.kDouble = 4
    # OpenMaya.MSyntax.kInvalidArgType = 0
    # OpenMaya.MSyntax.kInvalidObjectFormat = 0
    # OpenMaya.MSyntax.kLastArgType = 11
    # OpenMaya.MSyntax.kLastObjectFormat = 4
    # OpenMaya.MSyntax.kLong = 3
    # OpenMaya.MSyntax.kNoArg = 1
    # OpenMaya.MSyntax.kNone = 1
    # OpenMaya.MSyntax.kSelectionItem = 10
    # OpenMaya.MSyntax.kSelectionList = 3
    # OpenMaya.MSyntax.kString = 5
    # OpenMaya.MSyntax.kStringObjects = 2
    # OpenMaya.MSyntax.kTime = 9
    # OpenMaya.MSyntax.kUnsigned = 6
    #
    kFlagType = [OM.MSyntax.kString, OM.MSyntax.kString, OM.MSyntax.kString]

    def addNodeParentConstraint(self, mesh, node):
        if self.kDebug:
            bObj0 = cmds.objExists(mesh)
            bObj1 = cmds.objExists(node)
            if not bObj0:
                print('Could not find object: %s'%mesh)
                return None
            if not bObj1:
                print('Could not find object: %s'%node)
                return None
            nodeShapes = cmds.listRelatives(node, shapes=True, typ='locator', path=True)
            meshShapes = cmds.listRelatives(mesh, shapes=True, typ='mesh', path=True)
            print('Found relatives: ', meshShapes, nodeShapes);
        if nodeShapes and meshShapes and ((len(nodeShapes) == 1) and (len(meshShapes) == 1)):
            # nodeShape = nodeShapes[0];
            # meshShape = meshShapes[0];
            print('Mesh is %s', mesh);
            print('Node is %s', node);
            constraint = cmds.parentConstraint(mesh, node, mo=True);
            return constraint;
        else:
            return None;

    def FixPoint(self):
        objects =  cmds.ls(sl=True, fl=True)
        if len(objects)!=2:
            print('Select one node and one mesh');
            return None;
        else:
            # Get the corresponding shapes to confirm whether locator or mesh
            constraint1 = self.addNodeParentConstraint(objects[0], objects[1]);
            constraint2 = self.addNodeParentConstraint(objects[1], objects[0]);
            constraint = constraint1 or constraint2
            if constraint == None:
                print('Constraint could not be created (Likely a mesh and a locator are not selected)')
            return constraint

    def patternLocName(self, strandName, cvIndex):
        locName = strandName + ('%dlocator'%cvIndex);
        return locName;

    # Assumes the locator and strand CV exist and connects.
    def connectStrandNode(self, strandName, cvIndex):
        locName = self.patternLocName(strandName, cvIndex);
        cmds.connectAttr(locName+'.translate', strandName+('Shape.controlPoints[%d]'%cvIndex));

    # Assumes the locator and strand CV exist and connects.
    def disconnectStrandNode(self, strandName, cvIndex):
        locName = self.patternLocName(strandName, cvIndex);
        cmds.disconnectAttr(locName+'.translate', strandName+('Shape.controlPoints[%d]'%cvIndex));
        return locName;

    def AttachLocator(self, cvIndex, curveName):
        #  TODO Change color of display handle
        cvName  = curveName + ('.controlPoints[%d]' % cvIndex);
        locName = self.patternLocName(curveName, cvIndex)
        print('Locator name: %s'% locName);
        pos = cmds.pointPosition(cvName)
        # TODO Make sure that it does not already exist
        loc = cmds.spaceLocator(p=pos, relative=True);

        # Make adjustments to get translate correctly
        cmds.setAttr(loc[0]+'.translateX', pos[0])
        cmds.setAttr(loc[0]+'.translateY', pos[1])
        cmds.setAttr(loc[0]+'.translateZ', pos[2])
        cmds.setAttr(loc[0]+'.localPositionX', 0)
        cmds.setAttr(loc[0]+'.localPositionY', 0)
        cmds.setAttr(loc[0]+'.localPositionZ', 0)

        # Rename the locator to easily identify
        newLocName = cmds.rename(loc[0], locName);
        assert(locName == newLocName)
        # Rescale the handles
        cmds.setAttr((locName+'Shape.localScaleX'),self.kLocatorScale);
        cmds.setAttr((locName+'Shape.localScaleY'),self.kLocatorScale);
        cmds.setAttr((locName+'Shape.localScaleZ'),self.kLocatorScale);
        # Put pivot in the right position
        cmds.move(pos[0],pos[1],pos[2], locName+'.scalePivot', locName+'.rotatePivot');
        self.connectStrandNode(curveName, cvIndex);
        return newLocName;

    def ExportStrandsScene(self):
        scene_node = ET.Element("Scene")
        comment = ET.Comment("Exported by Strands script for Maya")
        scene_node.append(comment)
        rigids = [ x[:x.rfind('Shape')] for x in cmds.ls(type='mesh')]
        rigids_node = ET.SubElement(scene_node, "Rigids")
        for rigid in rigids:
            rigid_node = ET.SubElement(rigids_node, "Rigid")
            rigid_node.set('name', rigid)
            print('name set')
            transform = cmds.xform(rigids[0], m=True, query=True)
            print(transform)
            transform_f = matrix_to_bare_string(np.array(transform))
            print(transform_f)
            transform_node = ET.SubElement(rigid_node, "E")
            transform_node.text=transform_f
            print('transform set')
            print('XML created')

        # Export points and map them
        points = [ x[:x.rfind('Shape')] for x in cmds.ls(type='locator')]
        points_node = ET.SubElement(scene_node, "Points")

        for point in points:
            print('Point %s'% point)
            point_node = ET.SubElement(points_node, "Point")
            pointPos = cmds.pointPosition(point);
            point_node.pos = ET.SubElement(point_node, "pos")
            point_node.pos.text = matrix_to_bare_string(np.array(pointPos))

        # Export strands
        strands_node = ET.SubElement(scene_node, "Strands")
        strands = [ x[:x.rfind('Shape')] for x in cmds.ls(type='nurbsCurve')]

        for strand in strands:
            print('Strand %s'% strand)
            strand_node = ET.SubElement(strands_node, "Strand")
            nNodes = numCVs(strand)
            for cvIndex in range(nNodes):
                node_node = ET.SubElement(strand_node, "Node")
                cv = strand+('.cv[%d]'%cvIndex)
                nodePos = cmds.pointPosition(cv);
                node_node.pos = ET.SubElement(node_node, "pos")
                node_node.pos.text = matrix_to_bare_string(np.array(nodePos))

        print_scene(scene_node)

    def ConnectStrandToScene(self,options):
        parentAttr = options[0]+(".StrandsData[%s]" % options[1]);
        print parentAttr+".radius";
        cmds.setAttr(parentAttr+".radius", 0.01);
        cmds.connectAttr(parentAttr+".radius", options[2] + ".radius");
        cmds.connectAttr(parentAttr+".stiffness", options[2] + ".stiffness");
        cmds.connectAttr(parentAttr+".density", options[2] + ".density");
        cmds.connectAttr(parentAttr+".nodes", options[2] + ".nodes");
        return True;

    kStrandCmdDict = {}
    kStrandCmdDict["FixPoint"]     = FixPoint;
    kStrandCmdDict["ExportScene"]  = ExportStrandsScene;
    kStrandCmdDict["ConnectStrandToScene"]    = ConnectStrandToScene;

    def __init__(self):
        ''' Constructor. '''
        OM.MPxCommand.__init__(self)

    def doIt(self, args):
        '''Command parser and caller. Commands listed in kStrandCmdDict are called'''
        ## TODO Try drawing in the current scene location.
        syntax = OM.MSyntax()
        if(len(self.kFlagLongName) != len(self.kFlagShortName)):
            print('Bad syntax specification. Long and short name count should match')
        for ii in range(len(self.kFlagLongName)):
            syntax.addFlag( self.kFlagShortName[ii], self.kFlagLongName[ii], self.kFlagType[ii])
        argData = OM.MArgParser( syntax, args )

        ## Parse name argument
        name = None;
        if argData.isFlagSet( self.kFlagShortName[self.kNameArg] ):
            name = argData.flagArgumentString( self.kFlagShortName[self.kNameArg], 0 )
            print 'name: ' + name

        ## Parse command argument
        if argData.isFlagSet(self.kFlagShortName[self.kCommandArg]):
            # Get command to be executed
            cmdString = argData.flagArgumentString( self.kFlagShortName[self.kCommandArg], 0 )
            command = self.kStrandCmdDict[cmdString];
            if(command==None):
                print('Command not found');
            else:
                print('Calling command %s'% cmdString);
        else:
            print('No command called');
            self.setResult(False);
            return;

        ## Call command
        if cmdString=="CreateStrand":
            if self.CreateStrand(name) == name:
                self.setResult(True);
            else:
                self.setResult(False);

        elif cmdString=="AddNode":
            if self.AddNode():
                self.setResult(True);
            else:
                self.setResult(False);

        elif cmdString=="FixPoint":
            if self.FixPoint()== None:
                self.setResult(False);
            else:
                self.setResult(True);

        elif cmdString=="ConnectStrandToScene":
            if self.ConnectStrandToScene(options):
                self.setResult(True);
            else:
                self.setResult(False);
        elif cmdString=="ExportScene":
            self.ExportStrandsScene();

##########################################################
# Plug-in initialization.
##########################################################

def maya_useNewAPI():
	"""
	The presence of this function tells Maya that the plugin produces, and
	expects to be passed, objects created using the Maya Python API 2.0.
	"""
	pass

def cmdCreator():
    ''' Create an instance of the Strands command. '''
    return StrandsPyClass()

def initializePlugin( mobject ):
    ''' Initialize the plug-in when Maya loads it. '''
    mplugin = OM.MFnPlugin( mobject )
    try:
        mplugin.registerCommand( StrandsPyClass.kPluginCmdName, cmdCreator )
    except:
        sys.stderr.write( 'Failed to register command: ' + StrandsPyClass.kPluginCmdName )

def uninitializePlugin( mobject ):
    ''' Uninitialize the plug-in when Maya un-loads it. '''
    mplugin = OM.MFnPlugin( mobject )
    try:
        mplugin.deregisterCommand( StrandsPyClass.kPluginCmdName )
    except:
        sys.stderr.write( 'Failed to unregister command: ' + StrandsPyClass.kPluginCmdName )

##########################################################
# Sample usage.
##########################################################
'''
# Copy the following lines and run them in Maya's Python Script Editor:

import maya.cmds as cmds
cmds.loadPlugin('sampleCommand.py')
cmds.myCommandName()

'''

# NOTE How to keyframe attributes for playback
# currentTime 25 ;
# setAttr "nurbsCone1.A" 0;
# setKeyframe { "nurbsCone1.A" };
