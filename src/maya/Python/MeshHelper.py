import numpy as np
import numpy.linalg as la
import trimesh
import math

class MeshHelper:
    mesh =None
    meshLoc = None
    meshLoaded = False;
    def __init__(self, meshLoc):
        self.meshLoc = meshLoc;
        self.mesh = trimesh.load_mesh(meshLoc)
        self.meshLoaded = True

    def printMI(self):
        if not self.meshLoaded:
            return False;
        print self.mesh.moment_inertia;

    # Refer to http://www.euclideanspace.com/maths/geometry/rotations/conversions/angleToEuler/
    def angleCalc(self, x, y, z, s, t):
        return math.atan2(y*s - x*z*t, 1 - (y*y+z*z)*t);
    def computePA(self):
        if not self.meshLoaded:
            return False;
        self.mesh.vertices -= self.mesh.center_mass
        MI = self.mesh.moment_inertia
        evals, evecs = la.eig(MI)
        evals = np.array(evals);
        evecs = np.matrix(evecs);
        pa = evecs[0]
        print pa
        # Convert axis-angle representation to Euler representation, which is
        # usable in Autodesk Maya
        x = np.array([1,0,0]);
        c = np.dot(pa, x);
        angles = np.array([0.0,0.0,0.0])
        angle = math.acos( np.dot(pa, x))
        axis = np.cross(pa,x)[0]
        s = math.sin(angle)
        c1 = 1.0-c
        if (axis[0]*axis[1]*c1 + axis[2]*s)>0.9998:
            angles[0] = 2*math.atan2(axis[0]*math.sin(angle/2.0), math.cos(angle/2.0))
            angles[1] = math.pi/2.0
            angles[2] = 0.0;
            return angles;
        if (axis[0]*axis[1]*c1 + axis[2]*s)<-0.9998:
            angles[0] = -2*math.atan2(axis[0]*math.sin(angle/2.0), math.cos(angle/2.0))
            angles[1] = -math.pi/2.0
            angles[2] = 0.0;
            return angles;
        angles[1] = math.asin(axis[0]*axis[1]*c1 + axis[2]*s)
        angles[0] = self.angleCalc(axis[0], axis[1], axis[2], s, c1);
        angles[2] = self.angleCalc(axis[1], axis[0], axis[2], s, c1);
        print angles*180/math.pi
