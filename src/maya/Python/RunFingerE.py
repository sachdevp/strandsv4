# 1. Initialize plugin in Maya by pasting the result of the following command in folder
# src/maya/Python/classes.
#
# $ cat mayahelper.py graphoptions.py scenemod.py plugin.py strandsui.py load_instr.py
#
# You can use xclip as follows:
#
# $ cat mayahelper.py graphoptions.py scenemod.py plugin.py strandsui.py load_instr.py | xclip
#
# 2. Then load scene "300.1" and run the simulation.
# 3. Run this script.
# Email sachdevp@cs.ubc.ca in case of errors.

# NOTE Do not try to run Simulate and Continue commands back to back. That will most definitely
# crash the code. Instead, use get_state_thread after every simulation call, which can be done on a
# thread to keep Maya responsive, but join the thread before calling simulate again.

import maya.cmds as cmds
import time
from threading import Thread

# Try to get the state for @try_for seconds
def get_state_thread(try_for, simNo, state):
    try_again = True
    i_try = 0;
    while try_again and i_try<=try_for:
        # gotState is either a list or false.
        try:
            state.append(cmds.StrandsCmd(cmd='GetState', sim=simNo))
            try_again = False
            print state
            return state
        except:
            print 'Could not get state. Trying again.'
            if state:
                print 'State is %s'%state
            else:
                print 'No state'
            time.sleep(1)
            i_try = i_try+1

simNo = cmds.StrandsCmd(cmd='Simulate', time=.2, label='sim1', act='FlexorMuscle 0.05 ExtMuscle 0')
print simNo
cmds.StrandsCmd(cmd='DefineState', sim=simNo, fmt='MCP.ang PIP.ang DIP.ang MCP.exceeded FingerTip.pos')
state = []
thread = Thread(target=get_state_thread, args=(10, simNo, state))
thread.start()
thread.join()
print state[0]
# cmds.StrandsCmd(cmd='Continue', time=1, sim=simNo, act='FlexorMuscle 0.2 ExtMuscle 0')
#thread.start_new_thread(get_state_thread, (100, simNo, ))
#cmds.StrandsCmd(cmd='Continue', time=10, sim=simNo, act='FlexorMuscle 1.0 ExtMuscle 0')
#thread.start_new_thread(get_state_thread, (100, simNo, ))
