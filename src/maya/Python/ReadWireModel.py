import xml.etree.ElementTree as ET
import collections as col

## Classes to be used for reading muscles data
class Node:
    """
    Each @Node has an @index, with it's parent links and child links. Hence,
    the graph is directed. Nodes by itself contain enough information to
    represent the muscle.
    """
    def __init__(self, index, pLinks, cLinks, pos):
        self.index = index;
        self.pLinks = pLinks;
        self.cLinks = cLinks;
        self.pos = pos;
    def __repr__(self):
        s = "Node(index=%d, pLinks=("%(int( self.index ));
        if(len(self.pLinks)>0):
            s = s+str( self.pLinks[0] );
        for ii in range(1, len(self.pLinks)):
            s=s+"," + str( self.pLinks[ii] );
        s = s + "),cLinks=(";
        if(len(self.cLinks)>0):
            s = s+str( self.cLinks[0] );
        for ii in range(1, len(self.cLinks)):
            s=s+"," + str( self.cLinks[ii] );
        s = s + '),pos=(%.2f, %.2f, %.2f)'%( self.pos[0], self.pos[1], self.pos[2] )
        return s;
    def __str__(self):
        return self.__repr__();

class Link:
    """
    @Link has an @index and the parent node @pNode and child node @cNode that
    it connects. Links by themselves have enough information to represent the
    whole graph.
    """
    def __init__(self, index, pNode, cNode):
        self.index = index;
        self.pNode= pNode;
        self.cNode = cNode;
    def __repr__(self):
        return "Link(index=%d, pNode=%d, cNode=%d)"%(int( self.index ), int( self.pNode ), int( self.cNode ));
    def __str__(self):
        return self.__repr__();

class Muscle:
    """
    @Muscle is a representation of a muscle with some redundancy. The muscle
    has an @index, and @name. @nodes is a dictionary of @Node class instances
    keyed on @Node.index and @links is a dictionary of @Link class instances
    keyed on @Link.index. These dictionaries should allow easy traversal and
    splitting of muscles into linear segments or "strands".
    """
    def __init__(self, index, name, nodes, links):
        self.index = index;
        self.name = name;
        # @Muscle.nodes & @Muscle.links stored in a dict format
        self.nodes = nodes;
        self.links = links;

    # Return: (@node, @strand)
    def getStrand(self,node):
        """
        Returns a list of links as a @Strand class until it reaches a junction. If
        the node has multiple child links then None is returned. The intended
        use is for getting the rest of the strand and attaching the first link
        to it.

        Also returned is the new junction @node, which may be None if the end
        is reached.
        """
        # print "Starting with node %d"% node.index
        strand = Strand([]);
        t=0;
        while 1:
            if len(node.cLinks) > 1:
                # print "Returning node %d"% node.index
                return node, strand;
            if len(node.cLinks) == 0:
                # print "Returning node %d"% node.index
                return None, strand;
            link = self.links[node.cLinks[0]];
            strand.append(link);
            nodeId = link.cNode;
            node = self.nodes[nodeId]
            t=t+1;
            if t>100:
                print "Too many points."
                return None, None;

    def split_muscle(self):
        """
        Return @Strand class instances for different sections of the muscle.
        """
        print 'Splitting muscle %s'%self.name
        strands = [];
        begin_nodes = [];
        # Find all beginning nodes (hopefully only one)
        for node in self.nodes:
            if node.pLinks==[]:
                begin_nodes.append(node);
        # Check for just one beginning node
        if len(begin_nodes)!=1:
            print '%s:Could not find just one beginning node, %d found'%(self.name, len(begin_nodes) );
        # Beginning node found
        root = begin_nodes[0];
        if len(root.cLinks)==0:
            print 'Error: Just one node!'
            return None, None;

        nodes = begin_nodes;
        processed_id = [t.index for t in nodes ]
        while 1:
            if nodes == []:
                break;
            node = nodes.pop();
            for linkId in node.cLinks:
                link = self.links[linkId];
                nodeId = link.cNode;
                inputNode = self.nodes[nodeId];
                node, strand = self.getStrand(inputNode);
                if strand == None:
                    print 'ERROR! Could not split';
                    return None, None;
                strand.prepend(link);
                strands.append(strand);
                # Handle any returned junction nodes
                if node!=None:
                    if node.index not in processed_id:
                        nodes.append(node);
                        processed_id.append(node.index);
        return strands;

class Connection:
    def __init__(self, id0, id1, begEnd0, begEnd1):
        self.strandID[0] = id0;
        self.strandID[1] = id1;
        assert(begEnd0==0 or begEnd0==1);
        assert(begEnd1==0 or begEnd1==1);
        self.begEnd[0] = begEnd0;
        self.begEnd[1] = begEnd1;
    def __repr__(self):
        return "Strand %d at %s connected to strand %d at %s"%              \
            (self.strandID[0], "beginning" if self.begEnd[0]==0 else "end", \
             self.strandID[1], "beginning" if self.begEnd[1]==0 else "end");

class Strand:
    """
    @Strand is a directed linear segment of a muscles. @links is a list of
    @Link instances that represents the strand.
    """
    def __init__(self, links):
        self.links = links
    def append(self, link):
        self.links.append(link);
    def prepend(self, link):
        self.links.insert(0, link);
    def __repr__(self):
        s = "";
        for link in self.links:
            s = s + " (%d: %d, %d)"%(link.index, link.pNode, link.cNode)
        return s;

# Convert the muscle from XML to a graph
def muscle_to_graph(muscleNode):
    nLinks = 0;
    nNodes = 0;
    nodes = []
    links = []
    nodes_dict = {}
    links_dict = {}
    muscleNameNode = muscleNode.find('Name')
    muscleName = muscleNameNode.text
    for child in muscleNode:
        if child.tag == 'WireNode':
            cLinks = []; pLinks = [];
            indexNode = child.find('Index')
            index = int( indexNode.text )
            pointNode = child.find('Point')
            pos = [0,0,0]
            if pointNode!=None:
                posNode = pointNode.find('Position')
                if posNode!=None:
                    pos = [(float(x)) for x in posNode.text.split(' ')]
                else:
                    print 'No position found for node %d in %s'%(index, muscleName)
            else:
                print 'No point found for node %d in %s'%(index, muscleName)
            # pos = [ float(x) for x in (child.find('Point').find('Position').text.split(' ')) ]
            pLinksNode = child.find('ParentLinks')
            cLinksNode = child.find('ChildLinks')
            if pLinksNode!=None and pLinksNode.text!=None:
                for pLink in pLinksNode.text.split(','):
                    pLinks.append(int(pLink));
            if cLinksNode!=None and cLinksNode.text!=None:
                for cLink in cLinksNode.text.split(','):
                    cLinks.append(int(cLink));
            nodes.append(Node(index, pLinks, cLinks, pos[0:3]));
            # print nodes[nNodes]
            nodes_dict[index] = nodes[nNodes]
            nNodes = nNodes + 1;
        if child.tag == 'WireLink':
            index=int ( child.find('Index').text );
            links.append(Link(index,
                         int( child.find('ParentNode').text ),
                         int( child.find('ChildNode').text )))
            # print links[nLinks]
            links_dict[index] = links[nLinks];
            nLinks = nLinks + 1;
    return nodes, links;

# Return all the strands of all the muscles.
def read_muscles(XMLFile):
    tree = ET.parse(XMLFile);
    root = tree.getroot();
    muscles = [];
    for muscle_node in root.find('MuscleSet').iter('Muscle'):
        nodes, links = muscle_to_graph(muscle_node);
        index=-1;
        name='DefaultMuscleName';
        if muscle_node.find('Index')!=None:
            index = muscle_node.find('Index').text
        if muscle_node.find('Name')!=None:
            name = muscle_node.find('Name').text
            print name
        muscle = Muscle(index, name, nodes, links);
        muscles.append(muscle);
    print muscles
    allStrands = []
    allNodes = []
    for muscle in muscles:
        # if muscle.name == "upper_limb__l_flexor_digitorum_profundus":
        muscle.strands = muscle.split_muscle();
        # TODO Write find_junctions
        # junctions = strands.find_junctions();
    return muscles;
