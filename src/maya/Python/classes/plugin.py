import os
from maya import cmds

def GetPrefix():
    return ''

def GetReleaseSuffix():
    return 'Release'

def GetDebugSuffix():
    return 'Debug'

def GetFileSuffix():
    return '.so'

def GetFileLocation():
    return '/home/sachdevp/Software/Autodesk/Plugins/'

def GetReleaseFiles():
    prefix = GetPrefix()
    suffix = GetReleaseSuffix()
    fileNames = [prefix+'StrandMaya'+suffix,
                 prefix+'SceneNode'+suffix,
                 prefix+'ConstraintShapeNode'+suffix,
                 prefix+'ConstraintComputeNode'+suffix,
                 prefix+'StrandsCmd'+suffix]
    return fileNames

def GetDebugFiles():
    prefix = GetPrefix()
    suffix = GetDebugSuffix()
    fileNames = [prefix+'StrandMaya'+suffix,
                 prefix+'SceneNode'+suffix,
                 prefix+'ConstraintShapeNode'+suffix,
                 prefix+'ConstraintComputeNode'+suffix,
                 prefix+'StrandsCmd'+suffix]
    return fileNames

def getPluginFiles():
    '''Select object and ask whether to delete it.'''
    defaultVersion = 'Debug'
    maxTime = -100000
    fSuff = GetFileSuffix()
    fPref = GetFileLocation()
    RelExists = True 
    DebExists = True 
    for fName in GetReleaseFiles():
        print fName
        fLoc = fPref+fName+fSuff
        if os.path.exists(fLoc):
            fTime = os.path.getctime(fLoc)
            if fTime > maxTime:
                maxTime = fTime
                defaultVersion = 'Release'
        else:
            RelExists = False
            print '%s not found'%fLoc
            break
    for fName in GetDebugFiles():
        print fName
        fLoc = fPref+fName+fSuff
        if os.path.exists(fLoc):
            fTime = os.path.getctime(fLoc)
            if fTime > maxTime:
                maxTime = fTime
                defaultVersion = 'Debug'
        else:
            DebExists = False
            print '%s not found'%fLoc
            break
    if DebExists and RelExists:    
        result = cmds.confirmDialog(
            title='Debug or Release',
            message='Which version?',
            button=['Debug', 'Release', 'Cancel'],
            defaultButton=defaultVersion,
            dismissString='Cancel')
        print 'Loading %s version'%result
        if result == 'Release':
            return GetReleaseFiles()
        if result == 'Debug':
            return GetDebugFiles()
        else:
            print 'Canceling.'
    elif DebExists:
        return GetDebugFiles();
    elif RelExists:
        return GetReleaseFiles();
    return []

def LoadPlugins():
    '''Loads the plugins'''
    pluginFiles=getPluginFiles()
    for plugin in pluginFiles:
        cmds.loadPlugin(plugin)
    if pluginFiles != []:
        cmds.loadPlugin('Inertia')
        return True
    return False
