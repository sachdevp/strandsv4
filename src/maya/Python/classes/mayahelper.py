from maya import cmds
from maya import mel
from maya import OpenMayaUI as OMUI
from maya import OpenMaya as OM

class MayaHelper:
    '''Helper functions in separate class'''
    LUMRhoAttr = 'LUMLoad.Density'
    LUMForceAttr = 'LUMLoadForce.ForceWrenchValTY'
    kMass = 'mass'
    kTimestep = 'dt'
    DT = 'dt'
    DTX = 'dtx'
    def getMainWindow(self):
        ''' Return the main window ptr '''
        mayaMainWindowPtr = OMUI.MQtUtil.mainWindow()
        return mayaMainWindowPtr
    def isOutput(self, obj):
        '''Is it output?'''
        return self.plugExists(obj, "InputObject")

    def isInput(self, obj):
        '''Is it input?'''
        return self.plugExists(obj, "OutputObject")

    def findInputName(self, outObj):
        '''Find input for this output object'''
        if not self.isOutput(outObj):
            return None
        DN = OM.MFnDependencyNode(outObj)
        inAttrName = DN.name() + ".InputObject"
        outAttrName = cmds.connectionInfo(inAttrName, sfd=True)
        inObjName = outAttrName.split('.')[0]
        return inObjName

    def findInput(self, outObj):
        if not self.isOutput(outObj):
            return None
        outDN = OM.MFnDependencyNode(outObj)
        inPlug = outDN.findPlug('InputObject')
        plugs = OM.MPlugArray()
        inPlug.connectedTo(plugs, True, False)
        outPlug = plugs[0]
        if outPlug is None:
            return None
        outObj = outPlug.node()
        # print OM.MFnDependencyNode(outObj).name()
        return outObj

    def findOutputName(self, inObj):
        '''Output name for input object'''
        if not self.isInput(inObj):
            return None
        DN = OM.MFnDependencyNode(inObj)
        outAttrName = DN.name() + ".OutputObject"
        inAttrNames = cmds.connectionInfo(outAttrName, dfs=True)
        outObjName = inAttrNames[0].split('.')[0]
        return outObjName

    def getSceneObjectType(self, obj):
        '''Get the object type after checking it is a scene object'''
        # If it is an output, get type from input
        if self.isOutput(obj):
            inObjName = self.findInputName(obj)
        else:
            inObjName = OM.MFnDependencyNode(obj).name()
        # print 'Get scene object type for :', inObjName
        typeVal = self.typeAttrValByName(inObjName)
        return typeVal

    def selectObjectByName(self, objName):
        '''Select an object from object name'''
        print 'selectObject was called'
        print 'Selecting %s'%objName
        cmds.select(objName, r=True)
        return

    def setLUMLoadH(self, mass):
        cmds.setAttr(self.LUMRhoAttr, mass)
        cmds.setAttr(self.LUMForceAttr, -979.0*mass)
        return

    def getObjectByName(self, objName):
        '''Get MObject with objName. Do not change current selection.'''
        obj = OM.MObject()
        if not cmds.objExists(objName):
            return obj
        selectedObjs = cmds.ls(sl=True)
        cmds.select(objName, r=True)
        sList = self.getSelObjects()
        try:
            sList.getDependNode(0, obj)
        except:
            return obj
        cmds.select(cl=True)
        for s in selectedObjs:
            cmds.select(s, r=False)
        return obj

    # By default, from Obj to Obj
    def getShape(self, obj, toName=False, fromName=False):
        '''
        Get corresponding shapeObj. Can also use objName instead. Can return objName
        or obj itself.
        '''
        retVal = OM.MObject()
        if fromName:
            objName = obj
            obj = self.getObjectByName(objName)
        if toName:
            return cmds.listRelatives(objName, shapes=True)[0]
        else:
            try:
                DP = OM.MDagPath.getAPathTo(obj)
            except:
                return retVal
            try:
                DP.extendToShapeDirectlyBelow(0)
            except:
                return retVal
            return DP.node()
        return retVal

    RIGID      = 0
    POINT      = 1
    STRAND     = 2
    FORCE      = 3
    CONSTRAINT = 4
    JOINT      = 5

    COMPLETED = 0
    RUNNING   = 1
    DELETED   = 2
    CANCELED  = 3
    ERROR     = 4

    SimStatus = ['Completed', 'Running', 'Deleted', 'Canceled', 'Error']

    def typeAttrVal(self, obj):
        '''Return object type'''
        DN = OM.MFnDependencyNode(obj)
        pName = "objType"
        try:
            plug = DN.findPlug(pName)
        except:
            return None
        return plug.asInt()

    def typeAttrValByName(self, objName):
        '''Return object type for given object name'''
        return cmds.getAttr(objName+'.objType')

    def plugExists(self, obj, strPlug):
        '''Check if a plug exists'''
        DN = OM.MFnDependencyNode(obj)
        try:
            DN.findPlug(strPlug)
        except:
            return False
        return True

    def checkAttrsList(self, obj, attrNames):
        bResult = True
        DN = OM.MFnDependencyNode(obj)
        shapeDN = OM.MFnDependencyNode()
        shapeObj = None
        print DN.name()
        try:
            shapeObj = self.getShape(obj)
            shapeDN.setObject(shapeObj)
        except:
            shapeObj = None
            print 'shapeObj for %s is None'%DN.name()
        for attrName in attrNames:
            if not self.plugExists(obj, attrName) and (shapeObj is None or not self.plugExists(shapeObj, attrName)):
                bResult = False
                print 'Plug ' + attrName + ' does not exist in ' + DN.name() + ' or ' + shapeDN.name()
                nAttr = OM.MFnNumericAttribute()
                cAttr = OM.MFnCompoundAttribute()
                if attrName=='PosVel':
                    attr = nAttr.createPoint('PosVel', 'pvel')
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='AngVel':
                    attr = nAttr.createPoint('AngVel', 'avel')
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='Angles':
                    attr = nAttr.createPoint('Angles', 'ang')
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='SimpleStrand':
                    attr = nAttr.create('SimpleStrand', 'sims', OM.MFnNumericData.kBoolean, True)
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='Radius':
                    attr = nAttr.create('Radius', 'rad', OM.MFnNumericData.kDouble, 0.5)
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='Strain':
                    print 'Adding strain'
                    attr = nAttr.create('Strain', 'strain', OM.MFnNumericData.kDouble, 0.0)
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='Inextensible':
                    attr  = nAttr.create('Inextensible', 'inex', OM.MFnNumericData.kBoolean, False)
                    shapeDN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='SInexActive':
                    attr = nAttr.create('SInexActive', 'sia', OM.MFnNumericData.kBoolean, False)
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='SConstForce':
                    attr = nAttr.create('SConstForce', 'scf', OM.MFnNumericData.kDouble, 0.0)
                    DN.addAttribute(attr, DN.kLocalDynamicAttr)
                if attrName=='JLimActive':
                    cAttrObj = cAttr.create('JLimActive', 'jla')
                    attr = nAttr.create('JLimActiveX', 'jlaX', OM.MFnNumericData.kInt, 0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JLimActiveY', 'jlaY', OM.MFnNumericData.kInt, 0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JLimActiveZ', 'jlaZ', OM.MFnNumericData.kInt, 0)
                    cAttr.addChild(attr)
                    DN.addAttribute(cAttrObj, DN.kLocalDynamicAttr)
                if attrName=='JConstForce':
                    cAttrObj = cAttr.create('JConstForce', 'jcf')
                    attr = nAttr.create('JConstForceRX', 'jcfRX', OM.MFnNumericData.kDouble, 0.0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JConstForceRY', 'jcfRY', OM.MFnNumericData.kDouble, 0.0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JConstForceRZ', 'jcfRZ', OM.MFnNumericData.kDouble, 0.0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JConstForceTX', 'jcfTX', OM.MFnNumericData.kDouble, 0.0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JConstForceTY', 'jcfTY', OM.MFnNumericData.kDouble, 0.0)
                    cAttr.addChild(attr)
                    attr = nAttr.create('JConstForceTZ', 'jcfTZ', OM.MFnNumericData.kDouble, 0.0)
                    cAttr.addChild(attr)
                    DN.addAttribute(cAttrObj, DN.kLocalDynamicAttr)
                if attrName=='JNeutralAngle':
                    attrObj = nAttr.create('JNeutralAngle', 'jna', OM.MFnNumericData.kDouble, 0.0)
                    DN.addAttribute(attrObj, DN.kLocalDynamicAttr)
                if attrName=='JStiffness':
                    attrObj = nAttr.create('JStiffness', 'jk', OM.MFnNumericData.kDouble, 0.0)
                    DN.addAttribute(attrObj, DN.kLocalDynamicAttr)
                if attrName=='JDamping':
                    attrObj = nAttr.create('JDamping', 'jd', OM.MFnNumericData.kDouble, 0.0)
                    DN.addAttribute(attrObj, DN.kLocalDynamicAttr)
        return bResult
    
    def selectOutputObj(self, inObj):
        '''Select output object'''
        if not self.isInput(inObj):
            return None
        outObjName = self.findOutputName(inObj)
        cmds.select(outObjName, r=True)

    def isSceneObj(self, obj):
        '''Does the object belong to a scene?'''
        # print 'Does Scene plug exist?: ', plugExists(obj, "Scene")
        if self.plugExists(obj, "InputObject"):
            obj = self.findInput(obj)
        if obj is None:
            raise Exception('No input found')
        return self.plugExists(obj, "Scene")
            

    def getSelSceneObjects(self):
        '''Return selected scene objects. If non-scene objects selected, return
        nothing'''
        sList = self.getSelObjects()
        objTypes = []
        objNames = []
        objects = []
        sLen = sList.length()
        inOut = [0] * sLen # 0 for in, 1 for out
        for i in range(0, sLen):
            objects.append(OM.MObject())
            sList.getDependNode(i, objects[i])
            obj = objects[i]
            DN = OM.MFnDependencyNode(obj)
            objNames.append(DN.name())
            # print 'Checking object ', DN.name()
            if not self.isSceneObj(obj):
                objTypes = []
                objNames = []
                objects = []
                inOut = []
                break
            if self.isOutput(obj):
                inOut[i] = 1
            typeVal = self.getSceneObjectType(obj)
            objTypes.append(typeVal)
            # print 'Type is', objTypes[i]
        return objects, objTypes, objNames, inOut

    def getSingleJoint(self):
        '''Get a single selected joint'''
        objects, objTypes, objNames, inOuts = self.getSelSceneObjects()
        if len(objects) != 1:
            return OM.MObject.kNullObj, None, None, None
        obj = objects[0]
        objType = objTypes[0]
        objName = objNames[0]
        inOut = inOuts[0]
        if objType != self.JOINT:
            print 'Not a joint'
            return OM.MObject.kNullObj, None, None, None
        outObjName = ''
        if inOut == 0:
            outObjName = self.findOutputName(obj)
        else: outObjName = objName
        return obj, objType, objName, outObjName


    def getSelObjects(self):
        '''Return selected objects'''
        sList = OM.MSelectionList()
        OM.MGlobal.getActiveSelectionList(sList)
        return sList

    def getDebugJoints(self, outObjName):
        '''Return debug joints for output MObject'''
        dbgAttrs = cmds.connectionInfo(outObjName+'.DebugA', dfs=True)
        dbgAName = dbgAttrs[0].split('.')[0]
        dbgAttrs = cmds.connectionInfo(outObjName+'.DebugB', dfs=True)
        dbgBName = dbgAttrs[0].split('.')[0]
        return dbgAName, dbgBName

    def shapeType(self, obj):
        '''Get type of shape'''
        shapeObj = self.getShape(obj)
        if shapeObj == OM.MObject():
            return None
        node = OM.MFnDependencyNode(shapeObj)
        sType = cmds.objectType(node.name())
        return sType

    def isMesh(self, obj):
        if self.shapeType(obj) == "mesh":
            return True
        return False

    def movedPoint(self, obj):
        '''Check if point was moved'''
        DN = OM.MFnDependencyNode(obj)
        constName = cmds.parentConstraint(DN.name(), q=True)
        if constName is None:
            return False
        constOffset = cmds.getAttr(constName + ".restTranslate")
        objectLoc = cmds.getAttr(DN.name() + ".translate")
        retVal = False
        for ii in range(0, 3):
            retVal = retVal or (abs(constOffset[0][ii] - objectLoc[0][ii]) > 1e-3)
        return retVal

    def isJunction(self, objName):
        '''Check if node is a junction with multiple connections'''
        # Assumes point object passed
        conns = cmds.listConnections(objName+".PointID")
        if conns is None:
            return False
        return len(conns) > 1

    def getTextOKCancel(self, title, message):
        result = cmds.promptDialog(
            title=title,
            message=message,
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Dismissed')
        text = cmds.promptDialog(query=True, text=True)
        if result=='OK':
            return True, text
        if result=='Dismissed':
            return False, text
        return None, text
        
    def procModString(self, sceneName, modString):
        modList = modString.split(' ')
        modAttrs = [s for s in modList[ ::2] if s!= '']
        modVals  = [s for s in modList[1::2] if s!= '']
        check=False
        for idx, [attr, val] in enumerate(zip(modAttrs, modVals)):
            if attr==self.LUMRhoAttr:
                modAttrs[idx] = self.kMass
                check = True
            elif attr== (sceneName+'.' + self.DT):
                modAttrs[idx] = self.kTimestep
                check=True
        # if special value was hit, check is triggered
        if check:
            print modAttrs, modVals
            deleted=0
            for idx, [attr, val] in enumerate(zip(modAttrs, modVals)):
                ii = idx-deleted
                if attr==self.LUMForceAttr:
                    del modAttrs[ii]
                    del modVals[ii]
                    deleted = deleted + 1
                elif attr==(sceneName+'.'+self.DTX):
                    del modAttrs[ii]
                    del modVals[ii]
                    deleted = deleted + 1
        return modAttrs, modVals
