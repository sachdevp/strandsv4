from PySide2 import QtGui, QtCore, QtWidgets
from shiboken2 import wrapInstance as WI
import itertools
try:
    from mayahelper import MayaHelper
except:
    print 'MayaHelper warning.'
from maya import cmds
import math

def deleteWidgets(widgetList):
    '''Delete all widgets in @widgetList'''
    for widget in widgetList:
        if widget.parentWidget() != 0:
            try:
                widget.setParent(None)
                widget.deleteLater()
            except:
                print 'Could not delete widget'
        else:
            print 'Parent widget is 0'


class SceneModDialog(QtWidgets.QDialog):
    points = []
    nMods = 0
    nSims = 0
    lPoints = []
    tbPoints = []
    pointsLayout = None
    modsLayout = None
    combosLayout = None
    modStrings = None
    offStrings = None
    simLabels = None
    tbVals = []
    tbSimLabels = []
    tbKeys = []
    customResult = 'Failed'
    MH = MayaHelper()
    sceneName=''
    def __init__(self, parent=None, **kwargs):
        QtWidgets.QDialog.__init__(self, parent)
        self.setObjectName("Advanced Mod")
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(150, 250, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.accepted.connect(self.modify)
        self.buttonBox.rejected.connect(self.canceled)
        self.buttonBox.setObjectName("buttonBox")

        # self.nMods = 0
        # self.nSims = 0
        self.customResult = 'Failed'
        self.points       = kwargs["points"]
        self.sceneName    = kwargs["scene"]
        nSims, nMods, modAttrs, lModVals = self.procModStrings(kwargs["modStrings"])
        # self.nSims = nSims
        # self.nMods = nMods
        self.modAttrs = modAttrs
        self.lModVals = lModVals
        nPoints = len(self.points)
        grPoints = self.initPointsLayout()
        grMods   = self.initModsLayout()
        grCombos = self.initCombosLayout(nSims, nMods)
        self.nSims = nSims
        self.nMods = nMods
        self.createGrid(nPoints, nSims, nMods)
        self.setGridVals(modAttrs, lModVals, [], [])

        # self.subLayout.addWidget(self.comboNSims, 0, 0)

        topLayout = QtWidgets.QHBoxLayout()
        topLayout.addWidget(grCombos)
        if len(self.points)!=0:
            topLayout.addWidget(grPoints)
        topLayout.addWidget(grMods)
        topLayout.addWidget(self.buttonBox)
        self.setLayout(topLayout)

    def procModStrings(self, modStrings):
        if modStrings is None:
            return 1, 0, "", ""
        nSims = len(modStrings)
        modAttrs0 = None
        # List of modVals
        lModVals = []
        
        for i, modString in enumerate(modStrings):
            modAttrs, modVals = self.MH.procModString(self.sceneName, modString)
            lModVals.append(modVals)
            if i==0:
                modAttrs0 = modAttrs
            else:
                if modAttrs != modAttrs0:
                    print 'Unequal number of modAttrs: %s, %s'%(modAttrs, modAttrs0)
                    return 1, 0, "", ""
        return nSims, len(modAttrs0), modAttrs, lModVals

    def modify(self):
        self.modStrings = []
        self.offStrings = []
        self.simLabels = []
        for iSim in range(0, self.nSims):
            try:
                modString, offString, simLabel = self.modSim(iSim)
            except ValueError as err:
                print(err.args)
            print 'iSim #%d\nModString: %s\nOffString: %s\nsimLabel: %s'%(iSim, modString, offString, simLabel)
            self.modStrings.append(modString)
            self.offStrings.append(offString)
            self.simLabels.append(simLabel)
        self.customResult = 'OK'
        self.accept()
        self.close()
        return

    def modSim(self, iSim):
        modString = ''
        simLabel = self.tbSimLabels[iSim].text()
        for iMod in range(0, self.nMods):
            attrName = self.tbKeys[iMod].text()
            attrVal = self.tbVals[iSim][iMod].text()
            if attrName == self.MH.kMass:
                LUMObj = self.MH.getObjectByName('LUMLoad')
                if not LUMObj.isNull():
                    rho = float(attrVal)
                    force = -979*rho
                    modString = modString + self.MH.LUMRhoAttr   + ' ' + str(rho)   + ' '
                    modString = modString + self.MH.LUMForceAttr + ' ' + str(force) + ' '
            elif attrName == self.MH.kTimestep:
                old_dt = cmds.getAttr(self.sceneName + '.dt')
                old_dtx = cmds.getAttr(self.sceneName + '.dtx')
                new_dt = float(attrVal)
                new_dtx = math.ceil(old_dtx*old_dt/new_dt)
                modString = modString + '%s.%s '%(self.sceneName, self.MH.DT) + str(new_dt) + ' '
                modString = modString + '%s.%s '%(self.sceneName, self.MH.DTX) + str(new_dtx) + ' '
            elif cmds.objExists(attrName):
                attrVal = self.tbVals[iSim][iMod].text()
                modString = modString + attrName + ' ' + attrVal + ' '
            else:
                raise ValueError('Bad attribute name in mod attributes: %s'%attrName)
        offString = ''
        for point in self.points:
            offString = offString + point + ' ' + \
                self.tbPoints[point][3*iSim+0].text() + ' ' + \
                self.tbPoints[point][3*iSim+1].text() + ' ' + \
                self.tbPoints[point][3*iSim+2].text() + ' '
        return modString, offString, simLabel

    def canceled(self):
        self.modStrings = None
        self.offStrings = None
        self.customResult = 'Cancel'
        self.reject()
        self.close()
        return None

    def getValues(self):
        return self.customResult, self.modStrings, self.offStrings, self.simLabels

    def initModsLayout(self):
        grMods = QtWidgets.QGroupBox('Mods')
        self.modsLayout = QtWidgets.QGridLayout()
        grMods.setLayout(self.modsLayout)
        return grMods

    def initCombosLayout(self, nSims, nMods):
        '''Init layout for combos'''
        grCombos = QtWidgets.QGroupBox('Combos')
        self.comboLayout = QtWidgets.QGridLayout()
        grCombos.setLayout(self.comboLayout)
        
        self.comboNSims = QtWidgets.QComboBox(self)
        self.comboNSims.activated[str].connect(self.setNSims)
        simRange = range(1, 21)
        self.comboNSims.addItems([str(x) for x in simRange])
        self.comboNSims.setCurrentIndex(nSims-1)
        self.lNSims = QtWidgets.QLabel("#Sims", self)

        self.comboNMods = QtWidgets.QComboBox(self)
        self.comboNMods.activated[str].connect(self.setNMods)
        modRange = range(0, 20)
        self.comboNMods.addItems([str(x) for x in modRange])
        self.comboNMods.setCurrentIndex(nMods)
        self.lNMods= QtWidgets.QLabel("#Mods", self)

        self.comboLayout.addWidget(self.lNSims, 0, 0)
        self.comboLayout.addWidget(self.comboNSims, 0, 1)
        self.comboLayout.addWidget(self.lNMods, 1, 0)
        self.comboLayout.addWidget(self.comboNMods, 1, 1)
        return grCombos

    def initPointsLayout(self):
        '''Init layout for setting point relative offsets'''
        grPoints = QtWidgets.QGroupBox('Points')
        self.pointsLayout = QtWidgets.QGridLayout()
        grPoints.setLayout(self.pointsLayout)
        nRow = 0
        for point in self.points:
            lPoint = QtWidgets.QPushButton(point, self, flat=True)
            self.pointsLayout.addWidget(lPoint, nRow, 0)
            self.lPoints.append(lPoint)
            nRow = nRow + 3
        return grPoints

    def getMods(self):
        try:
            modAttrs = [tb.text() for tb in self.tbKeys]
            modVals  = [[tb.text() for tb in tbSimList] for tbSimList in self.tbVals]
            return modAttrs, modVals
        except:
            print 'Failed to get mods.'
            return [], []

    def getSimLabels(self):
        simLabels = []
        try:
            simLabels = [tb.text() for tb in self.tbSimLabels]
        except:
            simLabels = []
            print 'Failed to get sim labels'
        return simLabels

    def getPointOffsets(self):
        pointOffsets = []
        try:
            pointOffsets = [[tb.text() for tb in tbList] for tbList in self.tbPoints]
        except:
            pointOffsets = []
            print 'Failed to get point offsets'
        return pointOffsets 

    def clearGrids(self):
        # Do not delete point labels, but delete nsims and nmods dependent labels
        for tbs in self.tbPoints:
            deleteWidgets(tbs)
        deleteWidgets(self.tbSimLabels)
        deleteWidgets(self.tbKeys)
        for iSim in range(0, self.nSims):
            tbs = self.tbVals[iSim]
            deleteWidgets(tbs)
        self.tbPoints = []
        self.tbSimLabels = []
        self.tbKeys = []
        self.tbVals = []

    def addTB(self, layout, row, col, default):
        tb = QtWidgets.QLineEdit(default, self)
        layout.addWidget(tb, row, col)
        return tb

    def createGrid(self, nPoints, nSims, nMods):
        print nPoints, nSims, nMods
        print int(nPoints), int(nSims), int(nMods)
        self.tbVals = []
        self.tbSimLabels = []
        self.tbPoints = [[]]*len(self.points)
        self.tbKeys = [self.addTB(self.modsLayout, iMod+1, 0, 'modName') for iMod in range(nMods)]
        self.tbSimLabels = [self.addTB(self.modsLayout, 0, iSim+1, 'SimLabel') for iSim in range(nSims)]
        self.tbVals = [[self.addTB(self.modsLayout, iMod+1, iSim+1, '0.0') for iMod in range(nMods)] for iSim in range(nSims)]
        self.tbPoints = [[self.addTB(self.pointsLayout, iPoint*3+iX, iSim+1, '0.0') for iX in range(3) \
                          for iPoint in range(nPoints)] \
                         for iSim in range(nSims)]
        self.setColStretch()
        # for iMod in range(nMods):
        #     tb = QtWidgets.QLineEdit("", self)
        #     self.tbKeys.append(tb)
        #     self.modsLayout.addWidget(tb, iMod+1, 0)
        # for iSim in range(nSims):
            # tbSim = QtWidgets.QLineEdit('SimLabel', self)
            # self.modsLayout.addWidget(tbSim, 0, iSim+1)
            # self.tbSimLabels.append(tbSim)
            # nRow = 0
            # tbModVals = []

            # for iPoint in range(len(self.points)):
            #     for iX in range(3):
            #         tbPoint= QtWidgets.QLineEdit(str(0.0), self)
            #         self.pointsLayout.addWidget(tbPoint, nRow+iX, iSim+1)
            #         self.tbPoints[iPoint].append(tbPoint)
            #     nRow = nRow + 3
            # tbModVals = [addTB(self.modsLayout, iMod+1, iSim+1, '') for iMod in range(nMods)]
            # for iMod in range(0, nMods):
            #     tb = QtWidgets.QLineEdit("", self)
            #     tbModVals.append(tb)
            #     self.modsLayout.addWidget(tb, iMod+1, iSim+1)
            # self.tbVals.append(tbModVals)
        return

    def setGridVals(self, modAttrs, lModVals, simLabels, offsets):
        print modAttrs, lModVals, simLabels, offsets
        print 'sims %s, mods %s'%(self.nSims, self.nMods)
        try:
            for iSim, simLabel in enumerate(simLabels):
                if iSim<self.nSims:
                    self.tbSimLabels[iSim].setText(simLabel)
            for iMod, modAttr in enumerate(modAttrs):
                if iMod<self.nMods:
                    self.tbKeys[iMod].setText(modAttr)
            for iSim, modVals in enumerate(lModVals):
                if iSim<self.nSims:
                    # ModVals for a single sim
                    for iMod in range(len(modVals)):
                        if iMod<self.nMods:
                            self.tbVals[iSim][iMod].setText(modVals[iMod])
        except:
            print 'Failed to set grid vals'
        return
                
    def setNSims(self, sInt):
        _nSims = int(sInt)
        # print 'Current nSims: %d'% self.nSims
        if _nSims != self.nSims:
            modAttrs, modVals = self.getMods()
            simLabels  = self.getSimLabels()
            offsetVals = self.getPointOffsets()
            self.clearGrids()
            self.nSims = _nSims
            self.createGrid(len(self.points), self.nSims, self.nMods)
            self.setGridVals(modAttrs, modVals, simLabels, offsetVals)
        return

    def setNMods(self, sInt):
        _nMods= int(sInt)
        # print 'Current nMods: %d'% self.nMods
        if _nMods != self.nMods:
            modAttrs, modVals = self.getMods()
            simLabels = self.getSimLabels()
            offsetVals = self.getPointOffsets()
            self.clearGrids()
            self.nMods = _nMods
            self.createGrid(len(self.points), self.nSims, self.nMods)
            self.setGridVals(modAttrs, modVals, simLabels, offsetVals)
        return

    def setColStretch(self):
        self.modsLayout.setColumnStretch(0, 0)
        for iSim in range(0, self.nSims):
            self.modsLayout.setColumnStretch(iSim+1, 1)
        return
 
