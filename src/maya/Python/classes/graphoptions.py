try:
    from PySide import QtGui as QtWidgets
    from PySide import QtCore
    from shiboken import wrapInstance as WI
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets
    from shiboken2 import wrapInstance as WI

import os

try:
    import matplotlib.pyplot as plt
    import matplotlib.cbook
    bPlotting = True
except ImportError:
    print 'Could not import matplotlib elements'
    bPlotting = False

class GraphOptions():
    XLabel = 'Undefined'
    YLabel = 'Undefined'
    plotTitle = 'Undefined'
    resLabels = None
    resList = None
    YScales = []
    Legend = None
    attrLabels = None
    attrList = None
    fileName = ''

class CheckGraphOptions(QtWidgets.QDialog):
    graphOptions = GraphOptions()
    resIntList = None
    attrList = None
    tbAttrLabels = []
    tbResLabels = []
    tbYScales = []
    tbXLabel = None
    tbYLabel = None
    def __init__(self, parent=None, **kwargs):
        QtWidgets.QDialog.__init__(self, parent)
        self.setObjectName("Advanced Graph Options")
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(150, 250, 341, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply| \
                                          QtWidgets.QDialogButtonBox.Close| \
                                          QtWidgets.QDialogButtonBox.Save)
        self.buttonBox.clicked[QtWidgets.QAbstractButton].connect(self.buttonPress)
        self.buttonBox.setObjectName("buttonBox")
        self.resIntList = kwargs["resIntList"]
        self.attrList = kwargs["attrs"]
        self.graphOptions.attrList = self.attrList
        self.plot = kwargs["plotFn"]

        self.tbAttrLabels = []
        self.tbResLabels = []
        self.tbXLabel = None
        self.tbYLabel = None
        self.tbYScales = []

        grOpts = QtWidgets.QGroupBox('Options')
        self.optsLayout = QtWidgets.QGridLayout()
        grOpts.setLayout(self.optsLayout)
        print 'Init Opts'
        self.graphOptions.resList = self.resIntList
        resString = ''
        if self.resIntList is None:
            resString = 'Current'
        else:
            resString = ', '.join([str(x) for x in self.resIntList])
        row = 0
        info = QtWidgets.QLabel('Results: %s'%resString, self)
        self.optsLayout.addWidget(info, row, 0)
        row = row+1

        lPlotTitle = QtWidgets.QLabel('Plot Title ', self)
        self.optsLayout.addWidget(lPlotTitle, row, 0)
        self.tbPlotTitle = QtWidgets.QLineEdit('Time', self)
        self.optsLayout.addWidget(self.tbPlotTitle, row, 1)
        row = row + 1

        lResLabels = QtWidgets.QLabel('Result Labels: ', self)
        self.optsLayout.addWidget(lResLabels, row, 0)
        loc = 1
        for res in resString.split(','):
            tbResLabel = QtWidgets.QLineEdit(res, self)
            self.optsLayout.addWidget(tbResLabel, row, loc)
            self.tbResLabels.append(tbResLabel)
            loc = loc+1
        row = row+1

        lAttrLabels = QtWidgets.QLabel('Attr Labels: ', self)
        self.optsLayout.addWidget(lAttrLabels, row, 0)
        loc=1
        for attr in self.attrList:
            tbAttrLabel = QtWidgets.QLineEdit(attr, self)
            self.optsLayout.addWidget(tbAttrLabel, row, loc)
            self.tbAttrLabels.append(tbAttrLabel)
            loc = loc+1
        row = row+1

        lYScale= QtWidgets.QLabel('Y Scale: ', self)
        self.optsLayout.addWidget(lYScale, row, 0)
        loc=1
        for attr in self.attrList:
            tbYScale = QtWidgets.QLineEdit('1.0', self)
            self.optsLayout.addWidget(tbYScale, row, loc)
            self.tbYScales.append(tbYScale)
            loc = loc+1
        row = row + 1

        lXLabel = QtWidgets.QLabel('X Label: ', self)
        self.optsLayout.addWidget(lXLabel, row, 0)
        self.tbXLabel = QtWidgets.QLineEdit('Time', self)
        self.optsLayout.addWidget(self.tbXLabel, row, 1)
        lYLabel = QtWidgets.QLabel('Y Label: ', self)
        self.optsLayout.addWidget(lYLabel, row, 2)
        self.tbYLabel = QtWidgets.QLineEdit('Angles', self)
        self.optsLayout.addWidget(self.tbYLabel, row, 3)
        row = row+1

        lFileName = QtWidgets.QLabel('FileName: ', self)
        self.optsLayout.addWidget(lFileName, row, 0)
        self.tbFileName = QtWidgets.QLineEdit('', self)
        self.getSaveFileName = QtWidgets.QPushButton('...', self)
        self.getSaveFileName.clicked.connect(self.getFileName)
        self.optsLayout.addWidget(self.tbFileName, row, 1)
        self.optsLayout.addWidget(self.getSaveFileName, row, 2)
        row = row + 1

        self.optsLayout.addWidget(self.buttonBox, row, 0)

        self.setLayout(self.optsLayout)

    def modify(self):
        self.graphOptions.XLabel = self.tbXLabel.text()
        self.graphOptions.YLabel = self.tbYLabel.text()
        self.graphOptions.resLabels = []
        self.graphOptions.attrLabels = []
        self.graphOptions.YScales= []
        self.graphOptions.plotTitle = self.tbPlotTitle.text()
        fName = self.tbFileName.text()
        if fName is not None:
            self.graphOptions.fileName = fName
        else:
            self.graphOptions.fileName = os.getcwd() + '/' + self.graphOptions.plotTitle.replace(' ', '_') + '.eps'
            print self.graphOptions.fileName
        for tb in self.tbAttrLabels:
            self.graphOptions.attrLabels.append(tb.text())
        for tb in self.tbResLabels:
            self.graphOptions.resLabels.append(tb.text())
        for tb in self.tbYScales:
            self.graphOptions.YScales.append(float(tb.text()))
        self.plot(self.graphOptions)
        return

    def buttonPress(self, button):
        if self.buttonBox.standardButton(button) == QtWidgets.QDialogButtonBox.Save:
            self.savePlot()
        if self.buttonBox.standardButton(button) == QtWidgets.QDialogButtonBox.Apply:
            self.modify()
        if self.buttonBox.standardButton(button) == QtWidgets.QDialogButtonBox.Close:
            self.closeBox()

    def getFileName(self):
        fileName = QtWidgets.QFileDialog.getSaveFileName(self, 'Save Graph', \
                                                         "/home/sachdevp/",  \
                                                         "Image Files (*.pdf *.jpg *.bmp *.png *.eps)");
        self.tbFileName.setText(fileName[0])
        return
        # text, result = QtWidgets.QFileDialog.getText(self, "Interval Keyframe", "Parameters for %s?:"%attr)

    def savePlot(self):
        plotFileName = self.graphOptions.fileName
        plt.savefig(plotFileName)
        dataFileName = plotFileName[:plotFileName.rfind('.')]+'.log'
        ax = plt.gca()
        datas = []
        max_n = 0
        for line in ax.lines:
            data = line.get_xydata()
            datas.append(data)
            max_n = max(max_n, len(data))
        with open(dataFileName, 'w') as fOut:
            for iDP in range(0, max_n):
                iStr = ''
                for data in datas:
                    if iDP<len(data):
                        iStr = iStr + '%.2e'%data[iDP][0] + ','+ '%.2e'%data[iDP][1] + ','
                    else:
                        iStr = iStr + ','+ ','
                fOut.write(iStr[:-1]+'\n')
        return
        
    def getOptions(self):
        return graphOptions()

    def closeBox(self):
        self.close()
        return
    
