# -*- coding: utf-8 -*-
"""
Module to compute mass and MI properties for mesh objects in Maya.
Returns com (3x1), mass, rotated MI (3x3), axes of rotation (3x3)
auth: sachdevp@cs.ubc.ca
"""
# pylint: disable=C0103
# pylint: disable=C0326

from collections import namedtuple
import maya.api.OpenMaya as OM
import maya.cmds as cmds
import sys
import numpy as np

X = 0
Y = 1
Z = 2
CLIP_THRESH = 1e-10
space = OM.MSpace.kWorld

class InertiaClass(OM.MPxCommand):
    kPluginCmdName = 'Inertia'

    def __init__(self):
        """Constructor"""
        OM.MPxCommand.__init__(self)
    def doIt(self, args):
        """Command parser and caller"""
        syntax = OM.MSyntax()
        syntax.addFlag('rho', 'density', OM.MSyntax.kDouble)
        syntax.addFlag('sc' , 'scale'  , OM.MSyntax.kDouble)
        syntax.addFlag('v'  , 'verbose', OM.MSyntax.kBoolean)
        syntax.setObjectType(OM.MSyntax.kStringObjects,1,1)

        argData = OM.MArgParser(syntax, args)
        density = 1e3
        scale = 1.0
        verbosity = False
        if argData.isFlagSet('density'):
            density = argData.flagArgumentDouble('density', 0)
        if argData.isFlagSet('scale'):
            scale = argData.flagArgumentDouble('scale', 0)
        if argData.isFlagSet('verbose'):
            verbosity = argData.flagArgumentBool('verbose',0)
        # if argData.isFlagSet('center'):
        #     center = argData.flagArgumentBool('center', 0)

        # if center:
        com, mass, MI = compInertiaAtCOM(argData.getObjectStrings()[0], density, scale)
        # else:
        #     com, mass, MI = compInertia(argData.getObjectStrings()[0], density)

        # Transform the inertia to correspond to principal axes of rotation
        # print MI
        for ii in range(0,3):
            for jj in range(0,3):
                if abs(MI[ii,jj])<CLIP_THRESH:
                    MI[ii,jj] = 0
        MI_rot, axes = np.linalg.eig(MI)
        rhaxes = forceRightHanded(axes);
        if verbosity:
            print 'Object: %s'%argData.getObjectStrings()[0]
            print 'MI is: \n',MI
            print 'Mass is: \n',mass
            print 'Rotated MI is: \n',MI_rot
            print 'Axes are: \n',rhaxes
            print 'axes\'*axes is: \n',np.transpose(rhaxes)*rhaxes
        resultArr = OM.MDoubleArray()
        for ii in range(0,3):
            resultArr.append(com[ii])
        resultArr.append(mass)
        for ii in range(0,3):
            resultArr.append(MI_rot[ii])
        for ii in range(0,3):
            for jj in range(0,3):
                resultArr.append(rhaxes[ii,jj])
        self.setResult(resultArr)

def getMesh(objName):
    """Get object(MObject) and DAG path (MDagPath)"""
    if not cmds.objExists(objName):
        print 'Check object name. No object named %s found'%objName
        return None, None, None
    sel     = OM.MSelectionList()
    dagPath = OM.MDagPath()
    sel.add(objName)

    dagPath = sel.getDagPath(0)
    dagPath.extendToShape()
    shapeObj  = dagPath.node()
    transObj  = dagPath.transform()
    shapePath = dagPath
    # Check type of MObject using apiType()
    isMesh = shapeObj.hasFn(OM.MFn.kMesh)
    if not isMesh:
        return None, None, None
    return transObj, shapeObj, shapePath

def getMeshIters(objName):
    """
    Get MItMeshVertex, MItMeshPolygon objects for give Maya object name string.
    """
    _, _, shapePath = getMesh(objName)
    # print 'Got mesh'
    vertIter = OM.MItMeshVertex(shapePath)
    # vertIter.reset(shapeObj)
    faceIter = OM.MItMeshPolygon(shapePath)
    # faceIter.reset(shapeObj)
    return vertIter, faceIter

def compNormal(fVerts):
    """Compute normal like volInt"""
    dx = [[fVerts[ii+1][jj] - fVerts[ii][jj] for jj in range(0,3)] for ii in range(0,2)]
    n  = [dx[0][(ii+1)%3]*dx[1][(ii+2)%3] - dx[1][(ii+1)%3]*dx[0][(ii+2)%3] for ii in range(0,3)]
    return n

def readPoly(vertIter, faceIter):
    """
    Takes MFnMesh object and returns vertex list, face list, face normal list,
    and offsets (refer to original volInt.c file)
    """
    verts  = []
    faces  = []
    fNorms = []
    fw     = []
    while not faceIter.isDone():
        fNormal = OM.MVector()
        fPoints = OM.MPointArray()
        fVertsArr = faceIter.getVertices()
        # fNormal = faceIter.getNormal()
        fPoints = faceIter.getPoints(space)
        points = [[fPoints[ii][jj] for jj in range(0,3)] for ii in range(0,3)]
        point0 = points[0]
        # normal = [fNormal[ii]   for ii in range(0,3)]
        face   = [fVertsArr[ii] for ii in range(0,len(fVertsArr))]
        faces.append(face)
        fNormal = compNormal(points)
        fNorms.append(fNormal)
        # w = -fNormal[0]*fPoints[0][0] - fNormal[1]*fPoints[0][1]-fNormal[2]*fPoints[0][2]
        w = -fNormal[0]*point0[0] - fNormal[1]*point0[1]-fNormal[2]*point0[2]
        fw.append(w)
        faceIter.next(0)
    while not vertIter.isDone():
        # ind = vertIter.index()
        vert = vertIter.position(space)
        # print vert
        verts.append([vert[0], vert[1], vert[2]])
        vertIter.next()
    return verts, faces, fNorms, fw

# 12 tuples
FaceIntegrals = namedtuple("FaceIntegrals", "Fa Fb Fc Faa Fbb Fcc Faaa Fbbb Fccc Faab Fbbc Fcca")
# 10 tuples
ProjIntegrals = namedtuple("ProjIntegrals", "P1 Pa Paa Paaa Pb Pbb Pbbb Pab Paab Pabb")
# 10 tuples: 4 tuples with T1, T2, and TP being lists
VolIntegrals = namedtuple("VolIntegrals", "T0 T1X T1Y T1Z T2X T2Y T2Z TPX TPY TPZ")

ProjIntegralsInput = namedtuple("ProjIntegralsInput", "fVerts A B")
FaceIntegralsInput = namedtuple("FaceIntegralsInput", "fVerts norm w A B C")

def compProjInt(inPI):
    """
    Compute projection integrals. For reference, see compProjectionIntegrals
    function in volInt.c
    """
    nFVerts = len(inPI.fVerts)
    fVerts = inPI.fVerts
    P1, Pa, Paa, Paaa, Pb, Pbb, Pbbb, Pab, Paab, Pabb = [0.0]*10
    for iVert in range(0,nFVerts):
        a0 = fVerts[iVert][inPI.A]
        b0 = fVerts[iVert][inPI.B]
        a1 = fVerts[(iVert+1)%nFVerts][inPI.A]
        b1 = fVerts[(iVert+1)%nFVerts][inPI.B]
        da = a1-a0
        db = b1-b0

        a0_2 = a0 * a0; a0_3 = a0_2 * a0; a0_4 = a0_3 * a0
        b0_2 = b0 * b0; b0_3 = b0_2 * b0; b0_4 = b0_3 * b0
        a1_2 = a1 * a1; a1_3 = a1_2 * a1
        b1_2 = b1 * b1; b1_3 = b1_2 * b1

        C1   = a1 + a0
        Ca   = a1*C1 + a0_2            ; Caa  = a1*Ca + a0_3 ; Caaa = a1*Caa + a0_4
        Cb   = b1*(b1 + b0) + b0_2     ; Cbb  = b1*Cb + b0_3 ; Cbbb = b1*Cbb + b0_4
        Cab  = 3*a1_2 + 2*a1*a0 + a0_2 ; Kab  = a1_2 + 2*a1*a0 + 3*a0_2
        Caab = a0*Cab + 4*a1_3         ; Kaab = a1*Kab + 4*a0_3
        Cabb = 4*b1_3 + 3*b1_2*b0 + 2*b1*b0_2 + b0_3
        Kabb = b1_3 + 2*b1_2*b0 + 3*b1*b0_2 + 4*b0_3

        P1   = P1 + db*C1
        Pa   += db*Ca
        Paa  += db*Caa
        Paaa += db*Caaa
        Pb   += da*Cb
        Pbb  += da*Cbb
        Pbbb += da*Cbbb
        Pab  += db*(b1*Cab + b0*Kab)
        Paab += db*(b1*Caab + b0*Kaab)
        Pabb += da*(a1*Cabb + a0*Kabb)
    P1   /=   2.0
    Pa   /=   6.0
    Paa  /=  12.0
    Paaa /=  20.0
    Pb   /= - 6.0
    Pbb  /= -12.0
    Pbbb /= -20.0
    Pab  /=  24.0
    Paab /=  60.0
    Pabb /= -60.0
    PI = ProjIntegrals(P1, Pa, Paa, Paaa, Pb, Pbb, Pbbb, Pab, Paab, Pabb)
    return PI

def compFaceInt(inFI):
    """
    Compute face integrals. For reference, see compFaceIntegrals function in
    volInt.c
    """
    Fa, Fb, Fc, Faa, Fbb, Fcc, Faaa, Fbbb, Fccc, Faab, Fbbc, Fcca = [0.0]*12
    A = inFI.A; B = inFI.B; C = inFI.C

    inPI = ProjIntegralsInput(fVerts=inFI.fVerts, A=A, B=B)
    PI = compProjInt(inPI)

    w = inFI.w
    n = inFI.norm
    k1 = 1 / n[C]; k2 = k1 * k1; k3 = k2 * k1; k4 = k3 * k1

    Fa = k1 * PI.Pa
    Fb = k1 * PI.Pb
    Fc = -k2 * (n[A]*PI.Pa + n[B]*PI.Pb + w*PI.P1)

    Faa = k1 * PI.Paa
    Fbb = k1 * PI.Pbb
    Fcc = k3 * (n[A]*n[A]*PI.Paa + 2*n[A]*n[B]*PI.Pab + n[B]*n[B]*PI.Pbb \
	               + w*(2*(n[A]*PI.Pa + n[B]*PI.Pb) + w*PI.P1))

    Faaa = k1 * PI.Paaa
    Fbbb = k1 * PI.Pbbb
    Fccc = -k4 * (n[A]*n[A]*n[A]*PI.Paaa + 3*n[A]*n[A]*n[B]*PI.Paab               \
	                 + 3*n[A]*n[B]*n[B]*PI.Pabb + n[B]*n[B]*n[B]*PI.Pbbb             \
	                 + 3*w*(n[A]*n[A]*PI.Paa + 2*n[A]*n[B]*PI.Pab + n[B]*n[B]*PI.Pbb)\
                     + w*w*(3*(n[A]*PI.Pa + n[B]*PI.Pb) + w*PI.P1))

    Faab = k1 * PI.Paab
    Fbbc = -k2 * (n[A]*PI.Pabb + n[B]*PI.Pbbb + w*PI.Pbb)
    Fcca = k3 * (n[A]*n[A]*PI.Paaa + 2*n[A]*n[B]*PI.Paab + n[B]*n[B]*PI.Pabb \
                    + w*(2*(n[A]*PI.Paa + n[B]*PI.Pab) + w*PI.Pa))
    FI = FaceIntegrals(Fa, Fb, Fc, Faa, Fbb, Fcc, Faaa, Fbbb, Fccc, Faab, Fbbc, Fcca)
    return FI

def getFacePositions(verts, faces, faceInd):
    """
    Get vertex positions for i-th face using vertex position list (@verts),
    face-vertex index list (@faces), index (@i)
    """
    face = faces[faceInd]
    return [verts[face[ii]] for ii in range(0, 3)]

def compVolInt(verts, faces, fNorms, fw):
    """Compute volume integrals for final computation of mass and MI values."""
    # Get vertex and face iterators from objName
    nFaces = len(faces)

    T0 = 0.0
    T1 = np.zeros(3)
    T2 = np.zeros(3)
    TP = np.zeros(3)
    for iF in range(0,nFaces):
        fVerts = getFacePositions(verts=verts,faces=faces,faceInd=iF)
        # print 'Vertices are', fVerts
        norm = np.array(fNorms[iF])
        nx, ny, nz = norm
        nx = abs(nx)
        ny = abs(ny)
        nz = abs(nz)
        C = X
        if not (nx>ny and nx>nz):
            if ny>nz:
                C = Y
            else:
                C = Z
        A = (C+1)% 3
        B = (A+1)% 3

        inFI = FaceIntegralsInput(fVerts=fVerts, norm=fNorms[iF], w=fw[iF], A=A, B=B, C=C)
        FI = compFaceInt(inFI)

        T0    += norm[X] * (FI.Fa if (A == X) else (FI.Fb if (B == X) else FI.Fc))
        T1[A] += norm[A] * FI.Faa
        T1[B] += norm[B] * FI.Fbb
        T1[C] += norm[C] * FI.Fcc
        T2[A] += norm[A] * FI.Faaa
        T2[B] += norm[B] * FI.Fbbb
        T2[C] += norm[C] * FI.Fccc
        TP[A] += norm[A] * FI.Faab
        TP[B] += norm[B] * FI.Fbbc
        TP[C] += norm[C] * FI.Fcca
    # end for

    T1[X] /= 2; T1[Y] /= 2; T1[Z] /= 2
    T2[X] /= 3; T2[Y] /= 3; T2[Z] /= 3
    TP[X] /= 2; TP[Y] /= 2; TP[Z] /= 2
    return T0, T1, T2, TP

def getMeshProperties(objName):
    """Get properties of mesh from the name"""
    vertIter, faceIter = getMeshIters(objName)
    # print 'Got iterators'
    verts, faces, fNorms, fw = readPoly(vertIter, faceIter)
    # print 'Got poly parameters'
    return verts, faces, fNorms, fw

def compInertiaAtCOM(objName, density, scale):
    verts, faces, fNorms, fw = getMeshProperties(objName)
    # print 'Got mesh properties'
    T0, T1, T2, TP = compVolInt(verts, faces, fNorms, fw)
    # print T0, T1, T2, TP
    # print 'Got volume integrals ', T0, T1, T2, TP
    mass = density * T0
    # print mass

    r = np.zeros(3)
    MI = np.zeros([3,3])

    r[X] = T1[X] / T0
    r[Y] = T1[Y] / T0
    r[Z] = T1[Z] / T0

    MI[X][X] = density * (T2[Y] + T2[Z])
    MI[Y][Y] = density * (T2[Z] + T2[X])
    MI[Z][Z] = density * (T2[X] + T2[Y])
    MI[Y][X] = - density * TP[X]
    MI[Z][Y] = - density * TP[Y]
    MI[X][Z] = - density * TP[Z]

    MI[X][X] -= mass * (r[Y]*r[Y] + r[Z]*r[Z])
    MI[Y][Y] -= mass * (r[Z]*r[Z] + r[X]*r[X])
    MI[Z][Z] -= mass * (r[X]*r[X] + r[Y]*r[Y])
    MI[Y][X] += mass * r[X] * r[Y]
    MI[Z][Y] += mass * r[Y] * r[Z]
    MI[X][Z] += mass * r[Z] * r[X]
    MI[X][Y] = MI[Y][X]
    MI[Y][Z] = MI[Z][Y]
    MI[Z][X] = MI[X][Z]

    r  = np.array(r) * scale
    s2 = scale*scale
    s3 = s2*scale

    mass = mass * s3
    MI   = (s3 * s2) * np.matrix(MI)

    return r, mass, MI

def getTransform(transObj):
    transformDN = OM.MFnDependencyNode()
    transformDN.setObject(transObj)
    mTransform = np.empty([4,4])
    flatMatrix = cmds.getAttr(transformDN.name()+'.xformMatrix')
    for i in range(0,4):
        for j in range(0,4):
            mTransform[i,j] = flatMatrix[4*i+j]

    # NOTE findPlug did not work as expected
    # transformPlug = transformDN.findPlug("xformMatrix", True)
    # matrix = transformPlug.asMDataHandle().asMatrix()
    # print matrix
    # for i in range(0,4):
    #     for j in range(0,4):
    #         mTransform[i,j] = matrix.getElement(i,j)

    return mTransform

def getRotation(E):
    """Return rotation from transform @E"""
    return E[0:3,0:3]

def getTranslation(E):
    """Return translation from transform @E"""
    return E[3,0:2]

def bracket(v):
    ret_v = np.zeros([3,3])
    ret_v[0,1] = -v[2]
    ret_v[0,2] =  v[1]
    ret_v[1,2] = -v[0]
    ret_v = ret_v - np.transpose(ret_v)
    return ret_v

def forceRightHanded(axes):
    """Ensure Z-axis is cross of X and Y"""
    rhaxes = axes
    rhaxes[2,:] = np.cross(axes[0,:],axes[1,:])
    return rhaxes

##########################################################
# Plug-in initialization.
##########################################################

def maya_useNewAPI():
    """
	The presence of this function tells Maya that the plugin produces, and
	expects to be passed, objects created using the Maya Python API 2.0.
    """
    pass

def cmdCreator():
    ''' Create an instance of the Inertia command. '''
    return InertiaClass();

def initializePlugin(mobject):
    ''' Initialize the plug-in when Maya loads it. '''
    mplugin = OM.MFnPlugin(mobject)
    try:
        mplugin.registerCommand(InertiaClass.kPluginCmdName, cmdCreator)
    except:
        sys.stderr.write('Failed to register command: ' + InertiaClass.kPluginCmdName)

def uninitializePlugin(mobject):
    ''' Uninitialize the plug-in when Maya un-loads it. '''
    mplugin = OM.MFnPlugin(mobject)
    try:
        mplugin.deregisterCommand(InertiaClass.kPluginCmdName)
    except:
        sys.stderr.write(' to unregister command: ' + InertiaClass.kPluginCmdName)


