import numpy as np
import scipy.sparse as sps
from scipy import signal
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import pylab
import matplotlib.cm as cmx
import matplotlib.colors as colors
import matplotlib.cbook
import os
import csv
import sys
import itertools as it

## TODO Needs work. Reading probably working. Plotting needs fixing, and needs edit to use multiple files as input.
## Usage: python plotOutput.py <dir> <relfile1> <relfile2> ....
##        python plotOutput.py <relfile1> <relfile2> ...
# colors = ['black', 'red', 'blue', 'brown', 'cyan', 'yellow', 'green', 'gold']
# lines = ['-', '--', '-.', ':']

# Colormap
values = [10, 20, 50, 100, 150, 200]
jet = cm = plt.get_cmap('jet') 
cNorm  = colors.Normalize(vmin=0, vmax=values[-1])
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet)
print scalarMap.get_clim()

nArgs = len(sys.argv)
inputFiles = [fName for fName in sys.argv[1:] if os.path.isfile(fName)]
offset=1
inputDir = sys.argv[1] if os.path.isdir(fName) else ''
if inputDir:
    inputFiles = ['%s/%s'%(inputDir, fName) for fName in sys.argv[2:] if os.path.isfile(fName)]
    offset=2
if len(inputFiles)!=nArgs-offset:
    print sys.argv[offset:]
    print inputFiles
    print 'Some files invalid. Printing input and output lists.'
    raw_input('Press Enter ... ')

fileName, fileExt = os.path.splitext(inputFiles[0])
fileExt = fileExt[1:]
dirName = fileName + ".d/"
try:
    os.mkdir(dirName)
except:
    print 'Directory already exists. Will be overwritten.'
    raw_input('Press Enter ... ')
# Directory to write graphs in 
# List of dictionaries for every recorded time step

results = []
for inputFile in inputFiles:
    result = lambda: 0
    with open(inputFile, 'r') as fin:
        result.debugInfo  = {}
        result.attrLabels = []
        result.lAttrVals  = []
        result.lAttrValsSeq  = []
        mode = 'dict'
        for line in fin:
            if mode=='dict':
                if line.find(':')!=-1:
                    key, val = line.split(':')
                    result.debugInfo[key] = val
                else:
                    mode = 'labels'
            if mode=='labels':
                result.attrLabels = [item.strip() for item in line.split(',')]
                mode='vals'
            elif mode=='vals':
                attrVals = line.split(',')
                attrDict = {}
                for idx, label in enumerate(result.attrLabels):
                    attrDict[result.attrLabels[idx]] =  float(attrVals[idx])
                result.lAttrVals.append(attrDict)
                result.lAttrValsSeq.append(attrVals)
    results.append(result)

b, a = signal.butter(4, .025)
# a = 1;
# b = [1.0/4, 1.0/4, 1.0/4, 1.0/4];
# b, a = signal.ellip(4, 0.01, 120, 0.125)
allAttrLabels = set([attr for attr in result.attrLabels for result in results])
comAttrLabels = allAttrLabels
for result in results:
    comAttrLabels = comAttrLabels.intersection(result.attrLabels)

for label in allAttrLabels:
    splitLab = label.split('-')
    scale = 1.0
    if len(splitLab)==2:
        if splitLab[1]=='Strain':
            typ = 'Strain (ratio)'
        if splitLab[1]=='SConstForce':
            typ = 'Tension (N)'
            scale = -1.0/10**5
        if splitLab[1]=='SInexActive':
            typ = 'Binary'
    else:
        typ = 'Flexion Angle (rad)'
    for iFig in [1, 2]:
        simLabText = ""
        modText = ""
        plt.figure(iFig)
        fig = plt.figure()
        ax = fig.add_axes((.1, .3, .8, .6))
        # ax = fig.add_axes((0, 0, 1, 1))
        ax.set_ylabel(typ)
        ax.set_xlabel('Time (s)')
        ax.set_title(label)
        iRes=0
        for idx, result in enumerate(results):
            simLab = result.debugInfo['label'].strip()
            y = [yval*scale for yval in [av[label] for av in result.lAttrVals]]
            x = [av['Time'] for av in result.lAttrVals]
            if iFig==2: y = signal.filtfilt(b, a, y)
            # print simLab
            ax.plot(x, y, label=values[iRes], color=scalarMap.to_rgba(values[iRes]))
            simLabText = "%s\n%s"%(simLabText, simLab)
            modText = "%s\n%s"%(modText, result.debugInfo['mod'].strip())
            iRes = iRes+1
        fName = ''
        if iFig==1: fName = '%s%s.eps'%(dirName, label)
        if iFig==2: fName = '%s%s.filt.eps'%(dirName, label)

        if len(splitLab)==2 and splitLab[1]=='SConstForce':
            ax.set_ylim([-.1, 3])
        print 'Drawn'
        # fig.text(.1, .1, simLabText)
        # fig.text(.3, .1, modText)
        fig.canvas.draw()
        plt.legend()
        plt.savefig(fName)
        print 'Saved'
        plt.clf()
        plt.close()
print 'Done'

# for idx, dataMap in enumerate(dataMaps):
#     pylab.plot(dataMap['PIP'], dataMap['DIP'], "o", label=vInputs[idx]+'g')
# ax.set_ylabel('DIP Flexion (rad)')
# ax.set_xlabel('PIP Flexion (rad)')
# ax.set_title('DIP vs PIP')
# pylab.legend()
# pylab.show(block=False)
# fName = '%s.%s.eps'%(fileName, 'DIPvPIP')
# plt.savefig(fName)
# plt.clf()
# pylab.close()

# flexorIdxs = [1, 2, 4, 7]
# for idx, dataMap in enumerate(dataMaps):
#     if idx in flexorIdxs:
#         y = [-x/10**5 for x in dataMap['Flexor-SConstForce']]
#         yfilt = signal.filtfilt(b, a, y, padlen=10)
#         x = dataMap['Time'][5:-5]
#         yfilt = yfilt[5:-5]
#         pylab.plot(x, yfilt, "--", label=vInputs[idx]+'g')
# fig = pylab.gcf()
# ax = pylab.gca()
# ax.set_ylabel('Tension(N)')
# ax.set_xlabel('Time')
# ax.set_title('Flexor Tension')
# pylab.legend()
# pylab.show(block=False)
# fName = '%s.%s.eps'%(fileName, 'FlexorTension')
# plt.savefig(fName)
# plt.clf()
# pylab.close()

# # Tension graph
# plt.clf()
# vStrands = ['CD', 'LUMLoad', 'FE', 'Flexor']
# vPlotLabels = ['Central Slip', 'LUM', 'Lateral/Terminal', 'Flexor']
# vInputsFilter = ['0.1', '200', '50']
# for inp in vInputsFilter:
#     if inp in vInputs:
#         print inp
#         # print aInput, inp, inp==aInput
#         # if inp==aInput:
#         for strand, plotLabel in zip(vStrands, vPlotLabels):
#             # Get data label for constraint force
#             label = strand + '-SConstForce'
#             print label
#             if label in vLabels:
#                 y = [-float(x)/(10**5) for x in dataMaps[vInputs.index(inp)][label]]
#                 yfilt = signal.filtfilt(b, a, y, padlen=10)

#                 x = dataMap['Time']
#                 pylab.plot(x, yfilt, label=plotLabel)
#             else:
#                 print 'Label %s not found'%label
#     else:
#         print 'Invalid input %s'%inp
#     fig = pylab.gcf()
#     ax = pylab.gca()
#     ax.set_ylabel('Force (N)')
#     ax.set_xlabel('Time (s)')
#     ax.set_title(inp)
#     ax.legend(loc='best', fancybox=True, framealpha=0.4)
#     pylab.legend()
#     pylab.show(block=False)
#     fName = '%s.tendon-force.%s.filt.png'%(fileName, inp)
#     plt.savefig(fName)
#     print fName
#     plt.clf()
# pylab.close()
