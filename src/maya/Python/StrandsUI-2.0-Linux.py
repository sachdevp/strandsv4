# pylint: disable=unused-import, unused-wildcard-import, invalid-name, too-few-public-methods, too-many-public-methods, bare-except, no-name-in-module, bad-whitespace, no-self-use

## Disable pylint for maya commands not defining returns
#pylint: disable=assignment-from-no-return
import sys
sys.path.append('/home/sachdevp/Software/python/env/lib/python2.7/site-packages')

import imp
import time
import functools
import shutil
import os
from threading import Timer
import re
import platform
import numpy as np
import scipy.sparse as sps
import math

try:
    import matplotlib.pyplot as plt
    from matplotlib import pylab
    import matplotlib.cbook
    bPlotting = True
except ImportError:
    print 'Could not import matplotlib elements'
    bPlotting = False

from maya import cmds
from maya import mel
from maya import OpenMayaUI as OMUI
from maya import OpenMaya as OM

try:
    from PySide import QtGui as QtWidgets
    from PySide import QtCore
    from shiboken import wrapInstance as WI
except ImportError:
    from PySide2 import QtGui, QtCore, QtWidgets
    from shiboken2 import wrapInstance as WI

from scenemod import SceneModDialog
from graphoptions import GraphOptions, CheckGraphOptions
from mayahelper import MayaHelper
from plugin import *

def isStringInt(s):
    '''Is this string an integer?'''
    try:
        int(s)
        return True
    except ValueError:
        return False

def deleteWidgets(widgetList):
    '''Delete all widgets in @widgetList'''
    for widget in widgetList:
        if widget.parentWidget() != 0:
            try:
                widget.setParent(None)
                widget.deleteLater()
            except:
                print 'Could not delete widget'
        else:
            print 'Parent widget is 0'

def getStrandStiffness(sName):
    '''Get the value of stiffness for the strand'''
    return cmds.getAttr(sName + '.Stiffness')

def setStrandStiffness(sName, stiffness):
    '''Set the value of stiffness for the strand'''
    print 'Setting stiffness for %s: %f'%(sName, stiffness)
    MH = MayaHelper()
    MH.selectObjectByName(sName)
    return cmds.setAttr(sName + '.Stiffness', float(stiffness))

def ToggleNodeSurface():
    '''Toggle surface for selected constraint node'''
    mesh = cmds.ls(typ='parentConstraint', sl=True)
    attr = mesh[0] +'.srfvis'
    outAttr = mesh[0] + '.out'
    inAttr = mesh[0] + '.in'
    val = 1 - cmds.getAttr(attr)
    outAttrDest = cmds.connectionInfo(outAttr, dfs=True)
    inAttrSource = cmds.connectionInfo(inAttr, sfd=True)
    print inAttrSource, outAttrDest
    cmds.setAttr(attr, val)

class CreateStrandsUI(QtWidgets.QWidget):
    '''QWidget class to create UI'''
    vInputs = []
    vLabels = []
    vSimCustom = []
    ypos = 0
    yinc = 26
    xsize = 600
    ysize = 800
    rigidPos = 700
    selectCallback = None
    scriptJobMoved = None
    simProgressDialog = []
    sStrandsUI = 'StrandsUI'
    MH = MayaHelper()
    simLayout = None
    strandsLayout = None
    matrixDisp = None
    matrices = {}
    objAttrsList = {}
    objAttrsList[0, MH.RIGID] = ['RigidID', 'translate', 'rotate', 'ObjectID', 'OutputObject', 'Scene']
    objAttrsList[1, MH.RIGID] = ['translate', 'rotate', 'PosVel', 'AngVel', 'InputObject']
    objAttrsList[0, MH.POINT] = ['PointID', 'translate', 'ObjectID', 'OutputObject', 'Scene']
    objAttrsList[1, MH.POINT] = ['translate', 'rotate', 'PosVel', 'InputObject']
    objAttrsList[0, MH.JOINT] = ['JointID', 'translate', 'rotate', 'ObjectID', 'OutputObject', 'Scene', \
                                 'Radius', 'Lower', 'Upper', 'LimitType']
    objAttrsList[1, MH.JOINT] = ['translate', 'rotate', 'PosVel', 'AngVel', 'InputObject', 'JLimActive',\
                                 'JConstForce', 'Angles']
    objAttrsList[0, MH.STRAND] = ['StrandID', 'ObjectID', 'OutputObject', 'Scene', 'Material', 'Strain', \
                                  'Nodes', 'Inextensible', 'Material', 'SimpleStrand', 'Order', 'Activation', \
                                  'ActivationParameter']
    objAttrsList[1, MH.STRAND] = ['InputObject', 'Strain', 'ExtraNodesPos', 'ExtraNodesIdx', 'SInexActive', \
                                  'SConstForce']
    objAttrsList[0, MH.FORCE] = ['ForceID', 'translate', 'rotate', 'ObjectID', 'OutputObject', 'Scene', \
                                 'ForceMagnitude', 'DynamicObjectID']
    objAttrsList[1, MH.FORCE] = ['translate', 'rotate', 'InputObject']

    objAttrsList[0, MH.CONSTRAINT] = ['ConstraintID', 'translate', 'rotate', 'ObjectID', 'OutputObject', \
                                      'Scene', 'DynObj', 'ConstraintType', 'ConstSurfVisibile']
    objAttrsList[1, MH.CONSTRAINT] = ['translate', 'rotate', 'InputObject']

    def __init__(self, *args, **kwargs):
        super(CreateStrandsUI, self).__init__(*args, **kwargs)
        mayaMainWindowPtr = OMUI.MQtUtil.mainWindow()
        mayaMainWindow = WI(long(mayaMainWindowPtr), QtWidgets.QWidget)

        self.setParent(mayaMainWindow)
        self.setWindowFlags(QtCore.Qt.Window)
        # Check if window already exists
        if cmds.window(self.sStrandsUI, exists=True):
            print 'Deleting existing UI'
            cmds.deleteUI(sStrandsUI)
        self.setObjectName(sStrandsUI)
        self.setWindowTitle(sStrandsUI)
        self.setGeometry(50, 50, self.xsize, self.ysize)
        self.tbStrands = []
        self.lStrands = []
        self.initButtons()
        self.connectButtons()
        self.initOtherElements()
        self.initLayout()
        self.sceneName = None
        '''Generic callback when anything is selected'''
        self.selectCallback = OM.MEventMessage.addEventCallback \
                              ("SelectionChanged",              \
                               lambda *args, **kwargs: self.refreshUI())

    ## Functions to call StrandsCmd
    def CancelSim(self, iSim):
        '''Cancel running simulation.'''
        cmds.StrandsCmd(cmd='CancelSim', sim=iSim, scene=self.sceneName)

    def GetStatus(self, iSim):
        '''Get status of simulation.'''
        status = cmds.StrandsCmd(cmd='GetStatus', sim=iSim, scene=self.sceneName)
        print 'Status of simulation %d is %d'%(iSim, status[0])
        return status

    def CancelAll(self):
        '''Cancel all running simulations.'''
        cmds.StrandsCmd(cmd='CancelSim', sim=-1, scene=self.sceneName)

    def DeleteAllOutputs(self):
        '''Delete all outputs computed so far.'''
        cmds.StrandsCmd(cmd='DeleteOutput', sim=-1, scene=self.sceneName)

    def DeleteOutput(self, iSim):
        '''Delete all outputs computed so far.'''
        cmds.StrandsCmd(cmd='DeleteOutput', sim=iSim, scene=self.sceneName)

    def SelectSimOutput(self, iSim):
        '''Select particular output from the simulated ones.'''
        cmds.StrandsCmd(cmd='SelectOutput', sim=iSim, scene=self.sceneName)

    def ToggleEnable(self):
        '''Enable or disable strings'''
        cmds.StrandsCmd(command='ToggleEnable', scene=self.sceneName)

    def askForDelete(self, objName):
        '''Select object and ask whether to delete it.'''
        cmds.select(objName, replace=True)
        result = cmds.confirmDialog(
            title='Delete object',
            message='Should %s be deleted?'%objName,
            button=['Yes', 'No'],
            defaultButton='Yes',
            cancelButton='No',
            dismissString='Cancel')

        if result == 'Yes':
            return True
        return False

    def hideUIElements(self):
        '''Hide all UI elements. Will make visible as needed.'''
        self.bAddRigid.setVisible(False)
        self.bJoinStrands.setVisible(False)
        self.bAssignToRigid.setVisible(False)
        self.bFixToWorld.setVisible(False)
        self.bAttachForce.setVisible(False)
        self.bDeleteObject.setVisible(False)
        self.bConstrainOnAxis.setVisible(False)
        self.bHinge.setVisible(False)
        self.bRelocateNode.setVisible(False)
        self.bSplitJunction.setVisible(False)
        self.bAddNode.setVisible(False)
        self.bToggleNodeSurface.setVisible(False)
        self.bToggle.setVisible(False)

        # Joint buttons
        self.bSetJointScale.setVisible(False)
        self.bShowDebugJoints.setVisible(False)

        return

    def ShowDebugJoints(self):
        '''Show debug joints for selected joint'''
        obj, objType, objName, outObjName = self.MH.getSingleJoint()

        print 'Show Debug Joints: ', obj, objType, objName, outObjName
        dbgAName, dbgBName = self.getDebugJoints(outObjName)
        if outObjName is None:
            print 'Debug joints found were: ', outObjName, ' ', dbgAName, ' ', dbgBName
            return False

        dbgAShapeName = self.MH.getShape(dbgAName, fromName=True, toName=True)
        dbgBShapeName = self.MH.getShape(dbgBName, fromName=True, toName=True)
        # outObjShapeName = self.MH.getShape(outObjName, fromName=True, toName=True)

        setVal = 1 - cmds.getAttr(dbgAShapeName+'.visibility')
        cmds.setAttr(dbgAShapeName+'.visibility', setVal)
        cmds.setAttr(dbgBShapeName+'.visibility', setVal)

        # attrLocalScale = '.localScale'
        # cmds.connectAttr(outObjShapeName + attrLocalScale, dgbAShapeName + attrLocalScale)
        # cmds.connectAttr(outObjShapeName + attrLocalScale, dgbBShapeName + attrLocalScale)
        return True
    def activateRelocateNode(self, obj):
        self.bRelocateNode.setVisible(False)
        if self.MH.movedPoint(obj):
            self.bRelocateNode.setVisible(True)
        return

    def makeVisibleOneObject(self, obj, objType, objName):
        '''Make options visible for one object'''
        # Rigid can be constrained on an axis
        # print 'Make one object visible ' + str(objType)
        print objType
        if self.MH.isSceneObj(obj):
            self.bToggle.setVisible(True)
        if objType == self.MH.RIGID:
            self.bConstrainOnAxis.setVisible(True)
            self.bHinge.setVisible(True)
            # Node can be relocated
        if objType == self.MH.POINT:
            self.bFixToWorld.setVisible(True)
            self.bConstrainOnAxis.setVisible(True)
            # if self.MH.movedPoint(obj):
            #     self.bRelocateNode.setVisible(True)
            self.activateRelocateNode(obj);
            self.scriptJobMoved = cmds.scriptJob(
                attributeChange=[objName + ".t", \
                                 lambda: self.activateRelocateNode(obj)])
            if self.MH.isJunction(objName):
                self.bSplitJunction.setVisible(True)
        if objType == self.MH.CONSTRAINT:
            self.bToggleNodeSurface.setVisible(True)
        if objType == self.MH.JOINT:
            self.bSetJointScale.setVisible(True)
            self.bShowDebugJoints.setVisible(True)
        return

    def makeVisibleTwoObjects(self, objTypes, objNames):
        '''Make options visible for 2 selected objects'''
        print objTypes
        if objTypes[0] == self.MH.POINT and objTypes[1] == self.MH.POINT:
            self.bJoinStrands.setVisible(True)
            self.bAddNode.setVisible(True)
        # Point and Rigid => AssignToRigid
        if self.MH.POINT in objTypes and self.MH.RIGID in objTypes:
            self.bAssignToRigid.setVisible(True)
        # Force can be attached to rigid or point
        if self.MH.FORCE in objTypes:
            if self.MH.POINT in objTypes or self.MH.RIGID in objTypes:
                self.bAttachForce.setVisible(True)
        if objTypes[0] == objTypes[1] == self.MH.RIGID:
            self.bHinge.setVisible(True)
        return

    def refreshUI(self):
        '''Call when anything is selected. Used to update object actions.'''
        self.hideUIElements()

        if self.scriptJobMoved != None:
            if cmds.scriptJob(exists=self.scriptJobMoved):
                cmds.scriptJob(kill=self.scriptJobMoved)

        sList = self.MH.getSelObjects()

        if sList.length() == 1:
            obj = OM.MObject()
            sList.getDependNode(0, obj)
            if not self.MH.isSceneObj(obj) and self.MH.shapeType(obj) == "mesh":
                self.bAddRigid.setVisible(True)
        self.update()

        objects, objTypes, objNames, inOut = self.MH.getSelSceneObjects()
        if len(objects) != len(objTypes) or len(objects) != len(objNames):
            print 'Unequal length of objects'
            OM.MGlobal.setActiveSelectionList(sList)
            return
        nObjs = len(objects)

        # NOTE True and False are typed. Do not compare 0 to False
        if nObjs > 0:
            self.bDeleteObject.setVisible(True)
        if nObjs == 1 and inOut[0] == 0:
            objType = objTypes[0]
            objName = objNames[0]
            obj = objects[0]
            self.makeVisibleOneObject(obj, objType, objName)
        if nObjs == 2 and inOut[0] == 0 and inOut[1] == 0:
            self.makeVisibleTwoObjects(objTypes, objNames)
        OM.MGlobal.setActiveSelectionList(sList)
        return True

    def closeEvent(self, event):
        '''Remove callback when closing.'''
        if self.selectCallback:
            OM.MEventMessage.removeCallback(self.selectCallback)
        event.accept()

    def initOtherElements(self):
        '''Init UI elements other than buttons'''
        self.lScenes = QtWidgets.QLabel('Scenes')
        self.lResults = QtWidgets.QLabel('Results')
        self.lScale = QtWidgets.QLabel('Locator Scale', self)
        self.lStrandLen = QtWidgets.QLabel('Strand Length', self)

        self.comboSceneName = QtWidgets.QComboBox(self)
        self.comboResults = QtWidgets.QComboBox(self)
        self.comboSceneName.activated[str].connect(self.setSceneName)
        self.comboResults.activated.connect(self.SelectSimOutput)

        self.cbStabilize = QtWidgets.QCheckBox('Stabilize', self)
        self.cbStabilize.setChecked(True)

        self.cbMatrixOutput = QtWidgets.QCheckBox('Matrix Output', self)
        self.cbMatrixOutput.setChecked(True)

        self.tbScale = QtWidgets.QLineEdit('.20', self)
        self.tbStrandLen = QtWidgets.QLineEdit('1', self)

    def initButtons(self):
        '''Initialize all the buttons'''
        self.bAddForce = QtWidgets.QPushButton('Add Force', self)
        self.bAddNode = QtWidgets.QPushButton('Add Node', self)
        self.bAddRigid = QtWidgets.QPushButton('Add Rigid', self)
        self.bAddStrand = QtWidgets.QPushButton('Add Strand', self)
        self.bAssignToRigid = QtWidgets.QPushButton('Assign To Rigid', self)
        self.bAttachForce = QtWidgets.QPushButton('Attach Force', self)
        self.bCancelAll = QtWidgets.QPushButton('Cancel All', self)
        self.bCancelSim = QtWidgets.QPushButton('Cancel Simulation', self)
        self.bClearScene = QtWidgets.QPushButton('Clear Scene', self)
        self.bGetDebugInfo = QtWidgets.QPushButton('Get Debug Info', self)
        self.bExportScene  = QtWidgets.QPushButton('Export Scene', self)
        self.bConstrainOnAxis = QtWidgets.QPushButton('Constrain On Axis', self)
        self.bCreateScene = QtWidgets.QPushButton('Create Scene', self)
        self.bDeleteAll = QtWidgets.QPushButton('Delete All', self)
        self.bDeleteObject = QtWidgets.QPushButton('Delete Object', self)
        self.bDeleteOutput = QtWidgets.QPushButton('Delete Output', self)
        self.bFixToWorld = QtWidgets.QPushButton('Fix To World', self)
        self.bHinge = QtWidgets.QPushButton('Hinge Joint', self)
        self.bJoinStrands = QtWidgets.QPushButton('Join Strands', self)
        self.bPlotAttr = QtWidgets.QPushButton('Plot attribute', self)
        self.bRelocateNode = QtWidgets.QPushButton('Relocate Node', self)
        self.bSetJointScale = QtWidgets.QPushButton('Scale Joint', self)
        self.bShowDebugJoints = QtWidgets.QPushButton('Show debugging locators', self)
        self.bSimulate = QtWidgets.QPushButton('Simulate Scene', self)
        self.bSimCustom = QtWidgets.QPushButton('Simulate custom script', self)
        self.bSaveCustomData = QtWidgets.QPushButton('Save custom data', self)
        self.bSplitJunction = QtWidgets.QPushButton('Split Junction', self)
        self.bToggle = QtWidgets.QPushButton('Toggle Enable', self)
        self.bToggleAll = QtWidgets.QPushButton('Toggle All Strands', self)
        self.bToggleNodeSurface = QtWidgets.QPushButton('Toggle Node Surfaces', self)
        self.bCheckScene = QtWidgets.QPushButton('Check Scene', self)
        self.bKeyframeInterval = QtWidgets.QPushButton('KeyframeInterval', self)
        self.bUpdateResults = QtWidgets.QPushButton('..', self)
        self.bUpdateScenes  = QtWidgets.QPushButton('..', self)

    def connectButtons(self):
        '''Connect buttons to their functions.'''
        # self.bImportDhaiba.clicked.connect(self.ImportDhaiba)
        self.bAddForce.clicked.connect(self.AddForce)
        self.bAddNode.clicked.connect(self.AddNode)
        self.bAddRigid.clicked.connect(self.AddRigid)
        self.bAddStrand.clicked.connect(self.AddStrand)
        self.bAssignToRigid.clicked.connect(self.AssignToRigid)
        self.bAttachForce.clicked.connect(self.AttachForce)
        self.bCancelAll.clicked.connect(self.CancelAll)
        self.bCancelSim.clicked.connect(self.CancelSim)
        self.bCheckScene.clicked.connect(self.CheckAndCorrectScene)
        self.bKeyframeInterval.clicked.connect(self.SetIntervalKeyframe)
        self.bClearScene.clicked.connect(self.ClearScene)
        self.bConstrainOnAxis.clicked.connect(self.ConstrainOnAxis)
        self.bCreateScene.clicked.connect(self.CreateScene)
        self.bDeleteAll.clicked.connect(self.DeleteAllOutputs)
        self.bDeleteObject.clicked.connect(self.DeleteObject)
        self.bDeleteOutput.clicked.connect(self.DeleteOutput)
        self.bFixToWorld.clicked.connect(self.AssignToRigid)
        self.bGetDebugInfo.clicked.connect(self.GetDebugInfo)
        self.bExportScene.clicked.connect(self.ExportScene)
        self.bHinge.clicked.connect(self.HingeJoint)
        self.bJoinStrands.clicked.connect(self.JoinStrands)
        self.bPlotAttr.clicked.connect(self.PlotAttr)
        self.bRelocateNode.clicked.connect(self.RelocateNode)
        self.bSetJointScale.clicked.connect(self.SetJointScale)
        self.bShowDebugJoints.clicked.connect(self.ShowDebugJoints)
        self.bSimulate.clicked.connect(self.Simulate)
        self.bSimCustom.clicked.connect(self.SimCustom)
        self.bSaveCustomData.clicked.connect(self.SaveCustomData)
        self.bSplitJunction.clicked.connect(self.SplitJunction)
        self.bToggle.clicked.connect(self.ToggleEnable)
        self.bToggleAll.clicked.connect(self.ToggleAllStrands)
        self.bToggleNodeSurface.clicked.connect(ToggleNodeSurface)
        self.bUpdateResults.clicked.connect(self.updateResults)
        self.bUpdateScenes.clicked.connect(self.updateScenesCombo)

    def initObjActionsUI(self):
        '''Initiate the Obj Actions grid'''
        # Object action box
        grObject = QtWidgets.QGroupBox('Object Actions')
        gLayoutObjActions = QtWidgets.QVBoxLayout()
        gLayoutObjActions.addWidget(self.bToggleNodeSurface)
        gLayoutObjActions.addWidget(self.bSplitJunction)
        gLayoutObjActions.addWidget(self.bHinge)
        gLayoutObjActions.addWidget(self.bJoinStrands)
        gLayoutObjActions.addWidget(self.bAddNode)
        gLayoutObjActions.addWidget(self.bConstrainOnAxis)
        gLayoutObjActions.addWidget(self.bAssignToRigid)
        gLayoutObjActions.addWidget(self.bFixToWorld)
        gLayoutObjActions.addWidget(self.bAttachForce)
        gLayoutObjActions.addWidget(self.bToggle)
        gLayoutObjActions.addWidget(self.bDeleteObject)
        gLayoutObjActions.addWidget(self.bAddRigid)
        gLayoutObjActions.addWidget(self.bSetJointScale)
        gLayoutObjActions.addWidget(self.bShowDebugJoints)
        gLayoutObjActions.addWidget(self.bRelocateNode)
        if bPlotting:
            gLayoutObjActions.addWidget(self.bPlotAttr)
        grObject.setLayout(gLayoutObjActions)
        return grObject

    def initSceneActionsLayout(self):
        '''Initialise scene actions group box'''
        grScenes = QtWidgets.QGroupBox('Scene Actions')
        grSceneLayout = QtWidgets.QGridLayout()
        grSceneLayout.addWidget(self.lScenes, 0, 0)
        grSceneLayout.addWidget(self.comboSceneName, 1, 0)
        grSceneLayout.addWidget(self.bUpdateScenes, 1, 1)
        grSceneLayout.addWidget(self.lResults, 2, 0)
        grSceneLayout.addWidget(self.comboResults, 3, 0)
        grSceneLayout.addWidget(self.bUpdateResults, 3, 1)
        grSceneLayout.setColumnStretch(0, 3)
        grSceneLayout.setColumnStretch(1, 1)
        grScenes.setLayout(grSceneLayout)
        return grScenes

    def initParamsLayout(self):
        '''Parameter layouts'''
        grParams = QtWidgets.QGroupBox('Parameters')
        grParamLayout = QtWidgets.QGridLayout()
        grParamLayout.addWidget(self.cbStabilize, 0, 0)
        grParamLayout.addWidget(self.cbMatrixOutput, 1, 0)
        grParamLayout.addWidget(self.lScale, 2, 0)
        grParamLayout.addWidget(self.tbScale, 2, 1)
        grParamLayout.addWidget(self.lStrandLen, 3, 0)
        grParamLayout.addWidget(self.tbStrandLen, 3, 1)
        grParamLayout.setColumnStretch(0, 3)
        grParamLayout.setColumnStretch(1, 1)
        grParams.setLayout(grParamLayout)
        return grParams

    def initRegularActionsUI(self):
        '''Initial regular actions'''
        grRActions = QtWidgets.QGroupBox('Regular Actions')
        layout = QtWidgets.QGridLayout()
        layout.addWidget(self.bAddStrand)
        layout.addWidget(self.bAddForce)
        layout.addWidget(self.bKeyframeInterval)
        layout.addWidget(self.bToggleAll)
        grRActions.setLayout(layout)
        return grRActions

    def initStrandsLayout(self):
        '''Init layout for strands info'''
        grStrands = QtWidgets.QGroupBox('Strands')
        self.strandsLayout = QtWidgets.QGridLayout()
        grStrands.setLayout(self.strandsLayout)
        return grStrands

    def plotMatrix(self, matrix):
        M = sps.csr_matrix(self.matrices[matrix])
        pylab.close()
        pylab.clf()
        pylab.spy(M)
        # pylab.grid(b=True, which='major', color='b', linestyle='-')
        # pylab.grid(b=True, which='minor', color='r', linestyle='--')
        pylab.show(block=False)

    def initDebugLayout(self):
        '''Init layout for debugging'''
        grDebug = QtWidgets.QGroupBox('Debug')
        self.debugLayout = QtWidgets.QGridLayout()
        grDebug.setLayout(self.debugLayout)

        self.lMatrices = QtWidgets.QLabel('Matrices')
        self.comboMatrices = QtWidgets.QComboBox(self)
        self.comboMatrices.activated[str].connect(self.plotMatrix)
        self.debugLayout.addWidget(self.lMatrices, 0, 0)
        self.debugLayout.addWidget(self.comboMatrices, 0, 1)
        return grDebug

    def initSimLayout(self):
        '''Init layout for running simulation progress dialogs'''
        grSim = QtWidgets.QGroupBox('Simulations')
        self.simLayout = QtWidgets.QGridLayout()
        grSim.setLayout(self.simLayout)
        return grSim

    def initLayout(self):
        '''Initiate Layout. Add all th buttons etc.'''
        self.bInit = QtWidgets.QPushButton('Initialize Plugin', self)
        self.bInit.clicked.connect(LoadPlugins)

        self.topGrid = QtWidgets.QGroupBox()
        topLayout = QtWidgets.QHBoxLayout()

        self.vLayoutLeft = QtWidgets.QVBoxLayout()
        self.vLayoutMiddle = QtWidgets.QVBoxLayout()
        self.vLayoutRight = QtWidgets.QVBoxLayout()
        self.setLayout(topLayout)
        topLayout.addLayout(self.vLayoutLeft)
        topLayout.addLayout(self.vLayoutMiddle)
        topLayout.addLayout(self.vLayoutRight)
        topLayout.setStretch(0, 1)
        topLayout.setStretch(1, 1)

        self.vLayoutLeft.addWidget(self.bInit)
        self.vLayoutLeft.addWidget(self.initSceneActionsLayout())

        self.vLayoutLeft.addWidget(self.bCreateScene)
        self.vLayoutLeft.addWidget(self.bCheckScene)
        # self.vLayoutLeft.addWidget(self.bImportDhaiba)
        self.vLayoutLeft.addWidget(self.bClearScene)
        self.vLayoutLeft.addWidget(self.bGetDebugInfo)
        self.vLayoutLeft.addWidget(self.bExportScene)
        self.vLayoutLeft.addWidget(self.bSimulate)
        self.vLayoutLeft.addWidget(self.bSimCustom)
        self.vLayoutLeft.addWidget(self.bSaveCustomData)
        self.vLayoutLeft.addWidget(self.bCancelSim)
        self.vLayoutLeft.addWidget(self.bCancelAll)
        self.vLayoutLeft.addWidget(self.bDeleteOutput)
        self.vLayoutLeft.addWidget(self.bDeleteAll)
        self.vLayoutLeft.addWidget(self.initParamsLayout())
        self.vLayoutMiddle.addWidget(self.initRegularActionsUI())
        self.vLayoutMiddle.addWidget(self.initObjActionsUI())
        self.vLayoutMiddle.addWidget(self.initStrandsLayout())

        self.vLayoutRight.addWidget(self.initSimLayout())
        self.vLayoutRight.addWidget(self.initDebugLayout())

        self.hideUIElements()

    def updateResults(self):
        '''Allow selection of results available for the selected scene.'''
        if self.sceneName is None:
            print 'Select a scene first.'
            return
        n = cmds.getAttr(self.sceneName+'.nResults')
        self.comboResults.clear()
        for ii in range(0, n):
            self.comboResults.addItem(str(ii))

    def updateStrandInfo(self):
        '''Get all infor about strands into strand section'''
        if self.sceneName is None:
            print 'Not updating strand info. Scene name is none.'
            return
        nStrands = cmds.getAttr(self.sceneName+'.nStrands')

        ## Delete all strand widgets
        deleteWidgets(self.tbStrands)
        self.tbStrands = []
        self.lStrands  = []
        deleteWidgets(self.lStrands)

        nRow = 0
        print '%d strands found'%nStrands
        for sidx in range(nStrands):
            ## Get strand
            conns = cmds.connectionInfo(self.sceneName+'.Strands[%d]'%sidx, sfd=True)
            sName = conns.split('.')[0].encode('ascii', 'ignore')
            if not conns or not sName:
                continue
            nRow = nRow + 1

            ## Create and connect textbox (QLineEdit) for each strand's stiffness
            tbStrand = QtWidgets.QLineEdit(str(getStrandStiffness(sName)), self)
            tbStrand.textChanged.connect\
                (lambda text, strand=sName: setStrandStiffness(strand, float(text)))

            ## Add label for strand and make them clickable
            lStrand = QtWidgets.QPushButton(sName, self, flat=True)
            lStrand.clicked.connect(lambda strand=sName: self.MH.selectObjectByName(strand))

            ## Arrange the two in layouts
            self.strandsLayout.addWidget(lStrand, nRow, 0)
            self.strandsLayout.addWidget(tbStrand, nRow, 1)
            self.tbStrands.append(tbStrand)
            self.lStrands.append(lStrand)

    def setSceneName(self, text):
        '''Set scene name for UI and also update UI as needed'''
        self.sceneName = text
        print 'Selecting scene %s'%text
        # Select strand information
        self.updateStrandInfo()

    def getSceneName(self):
        '''Return self.sceneName if set'''
        if self.sceneName == '':
            print 'Scene name not set'
            return None
        else:
            return self.sceneName

    def updateScenesCombo(self):
        '''Update scene combo when new scene is added for example'''
        self.comboSceneName.clear()
        sceneList = cmds.ls(type='SceneNode')
        for s in sceneList:
            self.comboSceneName.addItem(s)
        if len(sceneList)>0: self.setSceneName(sceneList[0]);

    def AddStrand(self):
        '''Prompt for new strand and create new one using StrandsCmd'''
        result = cmds.promptDialog(
            title='Strand Name',
            message='Enter name for the strand:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')

        if result == 'OK':
            print self.getScale()
            text = cmds.promptDialog(query=True, text=True)
            result = cmds.StrandsCmd(command='AddStrand', name=text, \
                                     scene=self.sceneName, ls=self.getScale(),\
                                     strandLength=self.getStrandLength())
            print "Strand added: %s"%result[0]
            return result
        else:
            return False

    def getScale(self):
        '''Get scale for simulation. Currently used for locator/joint size.'''
        locScale = float(self.tbScale.text())
        print 'Scale is ', locScale
        return locScale

    def getStrandLength(self):
        '''Get strand length for simulation.'''
        strandLength = float(self.tbStrandLen.text())
        print 'Strand length is ', strandLength
        return strandLength

    def AddNode(self):
        '''Add node to strand'''
        result = cmds.StrandsCmd(command='AddNode', scene=self.sceneName,
                                 locatorScale=self.getScale())
        return result

    def AddForce(self):
        '''Add an extra force'''
        result = cmds.promptDialog(
            title='Force Name',
            message='Enter name for the force:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')

        if result == 'OK':
            text = cmds.promptDialog(query=True, text=True)
            result = cmds.StrandsCmd(command='AddForce', name=text, scene=self.sceneName)
            print "Result is: %s"%result
        else:
            return False

    def AttachForce(self):
        """Attach force to object"""
        cmds.StrandsCmd(command='AttachForce', scene=self.sceneName)

    def AddRigid(self):
        """Add this rigid to the scene"""
        cmds.StrandsCmd(command='AddRigid', scene=self.sceneName)

    def SetJointScale(self):
        '''Set scale for joint'''
        _, _, objName, outObjName = self.MH.getSingleJoint()
        objShapeName = self.getShape(objName, fromName=True, toName=True)
        outObjShapeName = self.getShape(outObjName, fromName=True, toName=True)
        attrLocalScale = '.localScale'
        cmds.promptDialog(
            title='Scale value',
            message='Enter float value for scale:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')
        text = cmds.promptDialog(query=True, text=True)
        scaleValue = float(text)
        if scaleValue is None:
            print 'Could not get a value'
            return False

        cmds.setAttr(objShapeName+attrLocalScale+'X', scaleValue)
        cmds.setAttr(objShapeName+attrLocalScale+'Y', scaleValue)
        cmds.setAttr(objShapeName+attrLocalScale+'Z', scaleValue)
        cmds.setAttr(outObjShapeName+attrLocalScale+'X', scaleValue)
        cmds.setAttr(outObjShapeName+attrLocalScale+'Y', scaleValue)
        cmds.setAttr(outObjShapeName+attrLocalScale+'Z', scaleValue)

    def RelocateNode(self):
        '''Relocate node as needed and make object invisible if success.'''
        result = cmds.StrandsCmd(command='RelocateNode', scene=self.sceneName)
        if result is True:
            self.bRelocateNode.setVisible(False)
        return result

    def SplitJunction(self):
        '''Split selected junction'''
        result = cmds.StrandsCmd(command='SplitJunction', scene=self.sceneName)
        return result

    def HingeJoint(self):
        '''Form hinge joint between 2 selected rigid objects'''
        result = cmds.promptDialog(
            title='Joint Name',
            message='Enter name for the joint:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')

        if result == 'OK':
            text = cmds.promptDialog(query=True, text=True)
            result = cmds.StrandsCmd(command='AddJoint', jointType='hinge',\
                                     name=text, scene=self.sceneName, ls=self.getScale())
            print "Result is: %s"%result
        else:
            return False

    def JoinStrands(self):
        '''Join 2 strands at selected nodes by merging them.'''
        result = cmds.StrandsCmd(command='JoinStrands', scene=self.sceneName)
        print 'Result is %s'%result
        if result is None:
            cmds.promptDialog(
                message='Failed to join strands. Check log.',
                title='Command failed',
                button=['OK'],
                defaultButton='OK')

    def getResultList(self):
        result = cmds.promptDialog(
            title='Results',
            message='Enter results to compare:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')
        resIntList = []
        status = False
        if result=='OK':
            text = cmds.promptDialog(query=True, text=True)
            try:
                print text.split(',')
                resIntList = [int(x) for x in text.split(',')]
                print resIntList
            except:
                return None
            status = True 
        return status, resIntList

    def checkMoreOptions(self):
        msgBox = QtWidgets.QMessageBox()
        msgBox.setText("Check More Options?")
        msgBox.setInformativeText("Say Yes to respond with more options.")
        msgBox.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)
        msgBox.setDefaultButton(QtWidgets.QMessageBox.No)
        ret = msgBox.exec_()
        if ret == QtWidgets.QMessageBox.Yes:
            # No more options to be checked
            return True
        else: return False

    def plotSubFn(self, graphOptions):
        keysList = {}
        pylab.close()
        pylab.clf()
        # weights = self.vInputs

        # fig, ax = pylab.subplots()
        colors = ['black', 'red', 'blue', 'brown', 'cyan']
        lines = ['-', '--', '-.', ':']

        if graphOptions.resList is None:
            for attr in graphOptions.attrList:
                dt = cmds.getAttr(self.sceneName + '.dt')
                dtx = cmds.getAttr(self.sceneName + '.dtx')
                keysList[0] = cmds.keyframe(attr, query=True, valueChange=True, timeChange=True);
                print keysList
                pylab.plot([dtx*dt*x for x in keysList[0][0::2]], \
                           [YScale*y for y in keysList[0][1::2]], \
                           label=graphOptions.attrLabels[0])
        else:
            iL=0;
            for iSim in graphOptions.resList:
                self.SelectSimOutput(iSim)
                dt, dtx = self.GetTimestepInfo(iSim)
                iA = 0
                for attr in graphOptions.attrList:
                    keysList[iSim] = cmds.keyframe(attr, query=True, valueChange=True, timeChange=True);
                    print keysList[iSim]
                    labelStr = ''
                    try:
                        labelStr = graphOptions.resLabels[iL]+'-'+graphOptions.attrLabels[iA]
                    except:
                        labelStr = 'Undefined'
                    print labelStr
                    pylab.plot([dtx*dt*x for x in keysList[iSim][0::2]], \
                               [graphOptions.YScales[iA]*y for y in keysList[iSim][1::2]], \
                               label=labelStr,\
                               color=colors[iSim],\
                               linestyle = lines[iA]\
                               )
                    iA = iA+1
                iL = iL+1
        pylab.legend()
        fig = pylab.gcf()
        ax = pylab.gca()
        ax.set_ylabel(graphOptions.YLabel)
        if graphOptions.XLabel: ax.set_xlabel(graphOptions.XLabel)
        else: ax.set_xlabel('Time')
        ax.set_title(graphOptions.plotTitle)
        fig.set_size_inches(10, 7)
        print 'Show'
        pylab.show(block=False)

    def PlotAttr(self):
        '''Plot joint angle data.'''
        # obj, objType, objName, outObjName = self.MH.getSingleJoint()
        sList = cmds.ls(sl=True)
        print 'Selected: ', sList

        # Check only attributes selected
        for selName in sList:
            try:
                [node, attrName] = selName.split('.')
                if not cmds.attributeQuery(attrName, n=node, exists=True):
                    print 'selName is not a valid attribute.'
            except:
                print 'selName is not a valid attribute.'

        status, resIntList = self.getResultList()
        if not status:
            print 'Canceled plotting.'
            return None
        if resIntList is None:
            print 'Assuming currently applied result'
        print 'resIntList is ', resIntList

        if self.checkMoreOptions():
            dialog = CheckGraphOptions(resIntList = resIntList, attrs = sList, plotFn = self.plotSubFn)
            dialog.setModal(False)
            res = dialog.exec_()

    def ConstrainOnAxis(self):
        '''Constrain object along given axis'''
        result = cmds.StrandsCmd(command='ConstrainOnAxis', scene=self.sceneName)
        print 'Result is %s'%result
        if result is None:
            cmds.promptDialog(
                message='Failed to constrain. Check log.',
                title='Command failed',
                button=['OK'],
                defaultButton='OK')

    def DeleteObject(self):
        '''Delete selected object'''
        result = cmds.StrandsCmd(command='DeleteObject', scene=self.sceneName)
        if result is None:
            cmds.promptDialog(
                message='Failed to delete object. Check log.',
                title='Command failed',
                button=['OK'],
                defaultButton='OK')

    def AssignToRigid(self):
        '''Assign selected point to selected rigid'''
        result = cmds.StrandsCmd(command='AssignToRigid', scene=self.sceneName)
        print "Result is: %s"%result
        if result[0] is False:
            cmds.confirmDialog(
                title='Fix Point Failed',
                message='Could not fix point. Check log for details.',
                button=['OK']
            )
            return True


    def CreateScene(self):
        '''Create new empty scene'''
        result = cmds.promptDialog(
            title='New Scene',
            message='Enter name for the new scene:',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')

        if result == 'OK':
            # Use the text for a new scene name
            text = cmds.promptDialog(query=True, text=True)
            result = cmds.createNode('SceneNode', name=text)
            print "Result is: %s"%result
            if result is None:
                cmds.confirmDialog(title='CreateScene failed',
                                   message='Could not create scene named %s' % text,
                                   button=['OK'])
                return False
            else:
                self.updateScenesCombo()
                self.setSceneName(result)
                self.comboSceneName.setCurrentIndex(self.comboSceneName.findText(text))
                return True
        return False

    def ClearScene(self):
        '''Clears the scene and unloads the plugins'''
        cmds.StrandsCmd(command='ClearScene', scene=self.sceneName)
        self.sceneName = None
        cmds.file(new=True, force=True)
        cmds.flushUndo()

    def checkSim(self, iSim, simProgressDialog, t):
        '''Check and return simulation status'''
        try:
            print 'Status list is'
            statusList = self.GetStatus(iSim)
            print statusList
            status = statusList[0]
            framesCompleted = statusList[1]
            # framesTotal = statusList[2]
            # done = statusList[3]
            print(self.MH.SimStatus)
            print(self.MH.SimStatus[status])
            # print QtCore.QString(self.MH.SimStatus[status])
            simProgressDialog.setValue(framesCompleted)
            simProgressDialog.setLabelText(self.MH.SimStatus[status])
            if status == self.MH.COMPLETED or status == self.MH.ERROR:
                print 'Completed simulation'
                t.stop()
        except:
            print 'Could not check simulation. Stopping timer.'

    def simCanceled(self, iSim, simProgressDialog, timer):
        '''Cleanup after sim calceled'''
        self.CancelSim(iSim)
        timer.stop()
        deleteWidgets([simProgressDialog])
        return

    def startProgressDialog(self, iSim, nSteps):
        '''Start a progress dialog for simulation status'''
        pd = QtWidgets.QProgressDialog\
            ("Running Simulation", "Abort Simulation", 0, nSteps, self)
        pd.setAutoReset(False)
        # simCanceled is called when cancel is called for simProgressDialog
        self.simLayout.addWidget(pd)
        t = QtCore.QTimer(self)
        pd.canceled.connect(lambda: self.simCanceled(iSim, pd, t))

        t.timeout.connect(functools.partial(self.checkSim, iSim, pd, t))
        t.start(5000)
        pd.show()

    def getYorN(self, _title, _message):
        '''Get Y or N for making decision.'''
        result = cmds.confirmDialog(
            title=_title,
            message=_message,
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')
        if result == 'OK':
            return True
        return False

    def manageOutputDir(self):
        bReplaceLog = cmds.getAttr(self.sceneName + ".ReplaceLog")
        debugFile = cmds.getAttr(self.sceneName + ".DebugFile")
        if debugFile == "":
            print 'Error: Debugging with empty debug file.'
            return None
        debugDir = debugFile + ".d/"
        # Directory handling.
        dirNo = 0
        asked = False
        cont = True
        if os.path.isdir(debugDir) and bReplaceLog:
            print 'Log directory already exists'
            bDelDir = self.getYorN('Simulate Scene', 'Delete previous log?')
            if bDelDir:
                shutil.rmtree(debugDir)
        while cont and os.path.isdir(debugDir):
            debugDir = debugFile + '.' + str(dirNo+1) + '.d/'
            if (not asked) and (dirNo>10):
                cont = not self.getYorN('Simulate Scene', \
                                        'Too many previous Matrix logs. Continue?')
                asked = True
            dirNo = dirNo+1
        if cont is False:
            print 'Too many output dirs. Clean up first.'
            return None
        print 'Making debugDir %s with OS lib'%debugDir
        os.mkdir(debugDir)
        return debugDir

    def callModify(self):
        '''Check selected points and allow relative move.'''
        sList = cmds.ls(sl=True)
        objects, objTypes, objNames, inOuts = self.MH.getSelSceneObjects()
        points = []
        for obj, objType, objName, inOut in zip(objects, objTypes, objNames, inOuts):
            if objType == self.MH.POINT:
                points.append(objName)

        dialog = SceneModDialog(points = points)
        res = dialog.exec_()
        if res==QtWidgets.QDialog.Accepted:
            modResult, modStrings, offStrings = dialog.getValues()
            print 'What?'
            print modResult, modStrings, offStrings
            return modResult, modStrings, offStrings
        print 'Unexpected.'
        return 'Exception', None, None

    def Simulate(self):
        '''Simulate the scene and get output fed back into keyframes for the scene objects'''
        if self.sceneName is None:
            print 'Select a scene first'
        result = cmds.promptDialog(
            title='Simulate Scene',
            message='Enter number of timesteps:',
            button=['OK', 'Cancel', 'Modify'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')
        # Get text for nSteps
        text = cmds.promptDialog(query=True, text=True)
        if not isStringInt(text):
            print 'Need integer number of steps'
            return False
        nSteps = int(text)
        simRunning = False
        iSims = []
        stab = self.cbStabilize.isChecked()

        bDebug = cmds.getAttr(self.sceneName + ".Debug")
        matOut = self.cbMatrixOutput.isChecked()
        debugDir = ''
        if result == 'Modify':
            modResult, modStrings, offStrings = self.callModify()
            if modResult == 'OK':
                for modString, offString in zip(modStrings, offStrings):
                    if bDebug and matOut:
                        print 'Debugging and logging matrices'
                        debugDir = self.manageOutputDir()
                        print 'Debug dir: %s'%(debugDir)
                    iSim = cmds.StrandsCmd(command='Simulate', steps=nSteps, scene=self.sceneName, stabilize=stab,\
                                           matrixOutput=matOut, dataOutDir=debugDir, modString=modString,\
                                           offsetString=offString)
                    iSims.append(iSim)
                    print 'Starting simulation number %d with\nmodString (%s) and\noffString(%s)'%(iSim, modString, offString)
                    simRunning = True
        if result == 'OK':
            if bDebug and matOut:
                print 'Debugging and logging matrices'
                debugDir = self.manageOutputDir()
                print 'Debug dir: %s'%(debugDir)
            iSim = cmds.StrandsCmd(command='Simulate', steps=nSteps, scene=self.sceneName,\
                                   stabilize=stab, matrixOutput=matOut, dataOutDir=debugDir)
            iSims.append(iSim)
            print 'Starting simulation number %d'%iSim
            simRunning = True
        if simRunning:
            for iSim in iSims:
                statusList = self.GetStatus(iSim)
                # print statusList
                framesTotal = statusList[2]
                self.startProgressDialog(iSim, framesTotal)

    def SimCustom(self):
        print 'Custom simulation function called. Disabling debug.'
        cmds.setAttr(self.sceneName+'.Debug', 0)
        self.vMass = [0.1, 1, 10, 25, 50, 100, 150, 200]
        self.vSimCustom = []
        self.vInputs = [str(x) for x in self.vMass]
        vTendons = ['outFlexor', 'outORL1', 'outORL2', 'outFE1', 'outFE2', 'outLUMLoad', \
                    'outCG', 'outCD', 'outBC', 'outAExt', 'outExtMuscle']
        vTendonAttrs = ['Strain', 'SInexActive', 'SConstForce']
        self.vSimAttrs = ['outMCP.AnglesZ', 'outDIP.AnglesZ', 'outPIP.AnglesZ']
        self.vLabels = ['Time', 'MCP', 'DIP', 'PIP']
        for tendon in vTendons:
            for attr in vTendonAttrs:
                self.vSimAttrs.append('%s.%s'%(tendon, attr))
                self.vLabels.append('%s-%s'%(tendon[3:], attr))
        for mass in self.vMass:
            cmds.setAttr('LUMLoad.Density', mass)
            cmds.setAttr('LUMLoadForce.ForceWrenchValTY', -979.0*mass)
            iSim = cmds.StrandsCmd(command='Simulate', time=12, scene=self.sceneName, stabilize=True,\
                                   matrixOutput=False)
            # Append iSim to list of simulations done here.
            self.vSimCustom.append(iSim)
        for iSim in self.vSimCustom:
            statusList = self.GetStatus(iSim)
            # print statusList
            framesTotal = statusList[2]
            self.startProgressDialog(iSim, framesTotal)
        return

    # def EditSimCustom(self):
        

    def SaveCustomData(self):
        # for iSim in self.vSimCustom:
        #     self.SelectSimOutput(iSim)
        #     dt, dtx = self.GetTimestepInfo(iSim)

        fileName = QtWidgets.QFileDialog.getSaveFileName \
            (self, 'Filename', "/home/sachdevp/", "Text Files (*.txt *.log *.dat)")[0]
        print fileName
        with open(fileName, 'w') as fOut:
            fOut.write(', '.join(self.vInputs) + '\n')
            fOut.write(', '.join(self.vLabels) + '\n')
        extLoc = fileName.rfind('.')
        filePre = fileName[:extLoc]
        fileSuff = fileName[extLoc+1:]
        for idx, iSim in enumerate(self.vSimCustom):
            inputStr = self.vInputs[idx]
            with open('%s.%s.%s'%(filePre, inputStr, fileSuff), 'w') as fOut:
                iA = 0
                self.SelectSimOutput(iSim)
                dt, dtx = self.GetTimestepInfo(iSim)
                outputList = []
                for attr in self.vSimAttrs:
                    keysList = cmds.keyframe(attr, query=True, valueChange=True, timeChange=True);
                    if iA==0:
                        outputList.append([dtx*dt*x for x in keysList[0::2]])
                    outputList.append(keysList[1::2])
                    iA = iA+1
                for outList in zip(*outputList):
                    outStr = ''
                    for col in outList:
                        outStr = outStr + ',' + '%.2e'%col
                    fOut.write(outStr[1:]+'\n')
        return

    def GetDebugInfoHelper(self, iSim):
        debugInfo = cmds.StrandsCmd(scene=self.sceneName, cmd="GetDebugInfo", sim=iSim)
        ii = 0;
        key = '';
        resultDict = {};
        # Get all values as (key, val) pairs
        for ii in range(len(debugInfo)):
            if ii%2==0:
                key = debugInfo[ii]
            else:
                resultDict[key] = debugInfo[ii]
        return resultDict

    def GetCurrentSelectedSim(self):
        iSim=None
        try:
            iSim = int(self.comboResults.currentText())
        except:
            print 'Could not get combobox sim #'
        if self.comboResults.currentText() is None or iSim is None or iSim<0:
            print 'No iSim found'
            return None
        return iSim

    def GetTimestepInfo(self, iSim):
        print 'Getting timestep info for sim #%d'%iSim
        debugDict = self.GetDebugInfoHelper(iSim);
        dtx = int(debugDict["dtx"])
        dt = 0.0002
        try:
            dt = float(debugDict["dt"])
        except:
            dt = 0.0002
        print 'Timestep info for %d: %f, %d'%(iSim, dt, dtx)
        return dt, dtx

    def GetDebugInfo(self):
        iSim = self.GetCurrentSelectedSim()
        print 'Getting sim debug info for sim #%d'%iSim
        debugDict = self.GetDebugInfoHelper(iSim);
        dtx = int(debugDict["dtx"])
        dt = debugDict["dt"]
        ## Test dict values
        # for key, val in dict.iteritems():
        #     print key, val;

        ## Get current frame number
        cTime = cmds.currentTime(q=True)
        print 'Frame number %d'%(cTime*dtx)
        fNo = int(cTime*dtx)
        filename = debugDict["dataOutDir"]+'/mat' + str(fNo) + '.m'
        if(not os.path.exists(filename)):
            filename = debugDict["dataOutDir"]+'/mat1.m'
        print filename
        if bPlotting and os.path.exists(filename):
            self.matrices = self.getMatrices(filename)
            self.comboMatrices.clear()
            self.comboMatrices.addItems([key for key, val in self.matrices.iteritems()])
        return
        # return debugInfo

    def ExportScene(self):
        fileName = QtWidgets.QFileDialog.getSaveFileName \
            (self, 'Filename', "/home/sachdevp/", "XML Files (*.xml)")[0]
        print fileName
        cmds.StrandsCmd(command="ExportScene", scene=self.sceneName, exportFile=fileName);
        
    def ToggleAllStrands(self):
        '''Toggle all strands to be enabled or disabled as per their values'''
        nStrands = cmds.getAttr(self.sceneName+'.nStrands')
        print 'nStrands: %d'%nStrands
        nStrands = cmds.getAttr(self.sceneName+'.Strands', size=True)
        print 'Strand connections: %d'%nStrands
        for iStrand in range(0, nStrands):
            sceneMsgAttr = self.sceneName+'.Strands[%d]'%iStrand
            strandMsgAttr = cmds.connectionInfo(sceneMsgAttr, sfd=True)
            print strandMsgAttr
            if strandMsgAttr:
                strandName = strandMsgAttr.split('.')[0]
                print 'Toggling '+strandName
                cmds.select(strandName)
                cmds.StrandsCmd(command='ToggleEnable')

    def DisplayPoints(self, newState):
        '''Display of points'''
        cmds.StrandsCmd(command='Display', filter='Points', \
                        state=newState, scene=self.sceneName)
        return

    def DisplayForces(self, newState):
        '''Display of forces'''
        cmds.StrandsCmd(command='Display', filter='Forces', \
                        state=newState, scene=self.sceneName)
        return

    def DisplayStrands(self, newState):
        '''Display of strands'''
        cmds.StrandsCmd(command='Display', filter='Strands', \
                        state=newState, scene=self.sceneName)
        return

    def DisplayConstraints(self, newState):
        '''Display of constraints'''
        cmds.StrandsCmd(command='Display', filter='Constraints', \
                        state=newState, scene=self.sceneName)
        return

    def getMatrices(self, filename):
        '''Get matrices for the current frame.'''
        print 'Getting matrices.'
        matrices = {}
        started = False
        matName = ''
        with open(filename) as inFile:
            for line in inFile:
                if line.endswith('... \n'):
                    matList = []
                    matName = line.split('=')[0]
                if not started and line.startswith('['):
                    row = line[1:-2]
                    if row == '':
                        break;
                    matList.append([float(x) for x in row.split(',')])
                    started = True
                elif started:
                    row = line[:-2]
                    matList.append([float(x) for x in row.split(',')])
                if line.endswith(']\n'):
                    matrices[matName] = np.array(matList)
                    started = False
        print 'Got matrices.'
        return matrices

    def CheckAndCorrectScene(self):
        it = OM.MItDependencyNodes()
        allSceneObjs   = []
        delObjNames = []
        while not it.isDone():
            itObj = it.item()
            objName = OM.MFnDependencyNode(itObj).name()
            bSceneObj = False
            try: 
                bSceneObj = self.MH.isSceneObj(itObj)
            except:
                print 'Could not determine whether obj is for scene: %s'%objName
                if self.askForDelete(objName):
                    delObjNames.append(objName)
            if bSceneObj:
                typeVal = None
                inOut = None
                if self.MH.isInput(itObj):
                    inOut = False; # Input
                    typeVal = self.MH.typeAttrVal(itObj)
                else:
                    inOut = True; # Output
                    inObjName = self.MH.findInputName(itObj)
                    if inObjName is None:
                        print 'inObjName is none for %s.'%objName
                        it.next()
                        continue
                    # print objName, inObjName
                    try:
                        typeVal = self.MH.typeAttrValByName(inObjName)
                    except:
                        print 'Could not get typeVal for: ' + inObjName
                # print typeVal, inOut, itObj
                allSceneObjs.append([typeVal, inOut, itObj])
            it.next()
        print 'Scene Objects are as follows: '
        print [OM.MFnDependencyNode(tup[2]).name() for tup in allSceneObjs]
        for tup in allSceneObjs:
            self.MH.checkAttrsList(tup[2],self.objAttrsList[tup[1],tup[0]])
        points = {}
        for pointTup in filter(lambda tup: tup[0]==self.MH.POINT and not tup[1], allSceneObjs):
            pointName = OM.MFnDependencyNode(pointTup[2]).name()
            print pointName
            pointIdx = cmds.getAttr(pointName+'.pid')
            print '%s is point with idx %d'%(pointName, pointIdx)
            points[pointIdx] = pointName
        for objName in delObjNames:
            print 'Deleting %s'%objName
            cmds.delete(objName)
        return

    def SetIntervalKeyframe(self):
        selection = cmds.ls(sl=True)
        attr = selection[0]
        text, result = QtWidgets.QInputDialog.getText(self, "Interval Keyframe", "Parameters for %s?:"%attr)
        textSplit = text.split(',')
        mag = 0;
        dt  = cmds.getAttr(self.sceneName + '.dt' )
        dtx = cmds.getAttr(self.sceneName + '.dtx')
        ldt = dt*dtx
        print dt, dtx, ldt
        initLStep = cmds.currentTime(q=True)
        timeSteps = 0
        if len(textSplit)==3 or len(textSplit)==2:
            mag = float(textSplit[0])
            timeSteps = math.ceil(float(textSplit[1])/ldt)
        
        if len(textSplit)==3:
            initLStep = int(float(textSplit[2])/ldt)

        validLengths = [2, 3]
        if len(textSplit) not in validLengths:
            print 'Two or three arguments accepted.'
            return
            
        cmds.currentTime(initLStep)
        cmds.setAttr(attr, mag)
        cmds.setKeyframe(attr)
        cmds.currentTime(initLStep+timeSteps)
        cmds.setAttr(attr, 0)
        cmds.setKeyframe(attr)
        print '%s keyframed'%attr
        return

# def main():
#     '''Main function'''
#     return ui

# if __name__ == '__main__':
#     main()

# Commands for creating cube
# result = `polyCube -w 1 -h 1 -d 1 -sx 1 -sy 1 -sz 1 -ax 0 1 0 -cuv 4 -ch 1`
# polyTriangulate -ch 1 pCube1
# select result[0]
