import sys
import os.path
sys.path.insert(0,  os.environ['MAYA_PYTHON_PATH'])

from strands.strandsui import CreateStrandsUI
from strands.plugin import LoadPlugins

LoadPlugins()
ui = CreateStrandsUI()
ui.show()
