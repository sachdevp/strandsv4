#include <maya/MString.h>
#include <maya/MFnDependencyNode.h>

// Maya related custom includes
#include <StrandsCmd/StrandsCmd.hpp>
#include <StrandsCmd/SimParams.hpp>
#include <MayaStrings.h>
#include <MayaHelper.hpp>

// Strands Library
#include <DynObj/Rigid.hpp>
#include <Joint.hpp>
#include <SE3.hpp>
#include <Recorder/Recorder.hpp>

using namespace std;
using namespace Eigen;

#ifdef _WIN32
#define MTHREADRETVAL_NULL 0
#else
#define MTHREADRETVAL_NULL nullptr
#endif
const auto &RunThread=MThreadAsync::createTask;
MThreadRetVal StrandsCmd::stepAndRecord(void *data){
   MStatus ms;
   SimParams* params = (SimParams*)data;
   SP<Recorder> rec  = params->getRecorder();

   MFnDependencyNode sceneDN(params->sceneObj);
   int nSteps  = params->nSteps;
   int nFrames = params->nFrames;
   int dtx; params->getdtx(dtx);

   log_reg("Simulating for %d frames of %d steps (total %d)", nFrames, dtx, nSteps);

   params->stepFrames(nFrames);

   return MTHREADRETVAL_NULL;
}

void StrandsCmd::ThreadCB(void* data){return;}

MStatus getOffMap(MAPSTRV3 &offMap, MString _offStr){
   MStringArray offArray;
   _offStr.split(' ', offArray);
   for(int iAct=0; iAct<offArray.length(); iAct+=4){
      auto name = string(offArray[iAct].asChar());
      float f[3];
      for(int j=0; j<3; j++){
         auto f1 = stof(string(offArray[4*iAct+j+1].asChar()));
         f[j] = f1;
         auto f2= stof(to_string(f1));
         if(!EQUALS(f1, f2)) log_warn("Mismatch (%s): %f, %f", name.c_str(), f1, f2);
      }
      V3 off;
      off<<f[0],f[1], f[2];
      offMap[name] = off;
   }
   return MS::kSuccess;
}

MStatus getActMap(ActMap &actMap, MString _actStr){
   MStringArray actArray;
   _actStr.split(' ', actArray);
   for(int iAct=0; iAct<actArray.length(); iAct+=2){
      auto name = string(actArray[iAct].asChar());
      float act = stof(string(actArray[iAct+1].asChar()));
      actMap[name] = act;
   }
   return MS::kSuccess;
}

MStatus StrandsCmd::getSteps(int &nFrames, int &nSteps, ftype dt, ftype dtx){
   if (mOptSteps>0)    nSteps = mOptSteps;
   else if(mOptTime>0) nSteps = ceil(mOptTime/dt);
   else MCHECKERROR_B(1, "Steps not set.");

   nFrames = ceil((ftype)nSteps/dtx);
   nSteps = nFrames*dtx;
   return MS::kSuccess;
}

MStatus StrandsCmd::Simulate
(bool _stabilize, bool _matOut, MString _dataOutDir,
 MString _modString, MString _offString, MString _actString, MString _labelStr){
   log_reg("ModString: \"%s\"\n" "OffString: \"%s\"", _modString.asChar(), _offString.asChar());
   MStatus ms;
   // Modify the scene as needed before getting parameters
   MStringArray modArr;
   bool bModString = (bOptModStringSet && _modString != "");
   if(bModString){MCHECKERROR(applyModString(modArr, _modString), "Failed to apply modString");}

   Strands::Scene* scene;
   SimOutput* simOut;
   MFnDependencyNode sceneDN(mSceneObj);


   MAPSTRV3 offMap;

   bool bOffString = (bOptOffStringSet && _offString != "");
   if(bOffString) MCHECKERROR(getOffMap(offMap, _offString), "Failed to get offMap from string.");
   auto GET_VAL2(dtx, sceneDN, kAttrSceneDtxLong, Int);
   auto GET_VAL2(dt , sceneDN, kAttrSceneDtLong , Double);

   // Does everything related to available simulation parameters in SceneNode
   MCHECKERROR(mSceneNode->createStrandsScene(scene, simOut, offMap, _labelStr), "Failed to create scene");

   // Check input activation string if available. If not available, 
   ActMap actMapOverride;
   ActKeyframes actKeyframes;
   if(bOptActStringSet){
      MCHECKERROR(getActMap(actMapOverride, _actString), "Failed to get actMap from string.");
   } else{
      MCHECKERROR(mSceneNode->getAllStrandActKeyframes(actKeyframes, simOut->strandMObjs),
                  "Failed to get strand activation keyframes.");
   }

   // Reverse modify the scene
   if(bModString) MCHECKERROR(applyMod(modArr), "Failed to apply modString");

   // Set scene parameters
   // NOTE Order important! Do not call recorder before setting everything for scene
   scene->setStabilize(_stabilize);
   if(scene->willDebug() && _matOut) MCHECKRET_R(scene->setMatOutDir(_dataOutDir.asChar()));

   SP<Recorder> recorder = make_shared<Recorder>();
   SimParams* params;

   int nSteps = -1, nFrames = -1;
   MCHECKERROR(getSteps(nFrames, nSteps, dt, dtx), "Could not simulate cause no input for steps.");
   log_warn("Using timestep %3.2e for %d timesteps", dt, nSteps);

   params = new SimParams(recorder, scene, simOut, mSceneObj, nSteps, nFrames, dt, dtx, _labelStr);
   params->setRunning();
   if(bOffString)       params->setOffMap(offMap);
   if(bModString)       params->setModString(_modString.asChar());
   if(bOptActStringSet) params->setActMap(actMapOverride);
   else params->setActKeyframes(actKeyframes);

   // Record these sim params to allow canceling simulation and noting recorded output
   auto iSim = mSceneNode->pushSim(params);
   INCREMENT_PLUG(sceneDN, kAttrScenenResultsLong);

   MThreadAsync::init();

   MCHECKERROR(RunThread(stepAndRecord, params, ThreadCB, nullptr), "Could not start simulation task.");

   this->setResult((uint)iSim);

   return MS::kSuccess;
}

// Figure out requirements to continue simulation. Pass those requirements and use them to
// continue simulation for required number of timesteps.
MStatus StrandsCmd::Continue(int iSim, MString _actString){
   MCHECKERROR(iSim<0, "Enter valid sim number to continue.");
   MStatus ms;
   int nSteps, nFrames;

   // Work in progress
   auto params = mSceneNode->mSimParams[iSim];
   auto rec    = params->getRecorder();
   if(params->isRunning()){
      setResult(false);
      log_err("This simulation is still running.");
      return MS::kFailure;
   }
   params->setRunning();

   int dtx; ftype dt;
   params->getTimestep(dt, dtx);
   log_verbose("Continuing sim #%d", iSim);
   // TODO Check if params are dirty somehow?
   MCHECKERROR(getSteps(nFrames, nSteps, dt, dtx),
               "Could not simulate cause no input for steps.");
   MCHECKERROR_R(params->setFramesAndSteps(nFrames, nSteps),
                 "Could not set steps and frames.");

   MFnDependencyNode sceneDN(params->sceneObj);
   MCHECKERROR(dtx<1,"dtx is less than 1.");
   log_reg("For %d frames of %d steps (%d small steps)", nFrames, dtx, nSteps);
   if(bOptActStringSet){
      ActMap newActMap;
      MCHECKERROR(getActMap(newActMap, _actString), "Failed to get actMap from string.");
      params->setActMap(newActMap);
   }

   MCHECKERROR(RunThread(stepAndRecord, params, ThreadCB, nullptr), "Could not start simulation task.");
   setResult(true);
   return MS::kSuccess;
}

bool StrandsCmd::isValid(int iSim){
   auto nSims = mSceneNode->mSimParams.size();
   if(iSim<0 || iSim>=nSims) return false;
   return true;
}

MStatus StrandsCmd::GetStatus(int iSim){
   MIntArray result;
   SimStatus s;
   vector<int> vRecResult;
   if(isValid(iSim)){
      mSceneNode->mSimParams[iSim]->getStatus(s, vRecResult);
      result.append((int)s);
      for(int i=0;i<vRecResult.size();i++) result.append(vRecResult[i]);
      this->setResult(result);
      return MS::kSuccess;
   }
   result.append(-1);
   this->setResult(result);
   return MS::kFailure;
}

MStatus StrandsCmd::GetDebugInfo(int iSim){
   MStringArray result;
   // SimStatus s;
   MStatus ms = MS::kFailure;
   if(isValid(iSim)){
      auto dInfo = mSceneNode->mSimParams[iSim]->getDebugInfo();
      for(auto p: dInfo){
         result.append(p.first.c_str());
         result.append(p.second.c_str());
      }
      ms = MS::kSuccess;
   }
   this->setResult(result);
   return ms;
}

MStatus StrandsCmd::ExportScene(MString labelStr, MString filenameStr){
   MCHECKERROR(mSceneNode->exportStrandsScene(labelStr, filenameStr),
      "Could not export scene.");
   return MS::kSuccess;
}

MStatus StrandsCmd::CancelSim(int iSim){
   // To cancel simulation, we can simply set a parameter in simParams
   SimStatus s;
   if(isValid(iSim)){
      mSceneNode->mSimParams[iSim]->getStatus(s);
      switch(s){
      case SimStatus::Deleted:
         log_err("Trying to cancel deleted simulation");
         break;
      case SimStatus::Completed:
         log_reg("Simulation already completed");
         break;
      case SimStatus::Error:
         log_reg("Simulation already completed with error.");
         break;
      case SimStatus::Canceled:
         log_reg("Simulation already canceled.");
         break;
      case SimStatus::Running:
         log_reg("Canceling simulation");
         mSceneNode->mSimParams[iSim]->cancel();
         break;
      default:
         log_err("Cannot understand simulation status.");
      }
   } else if(iSim == -1){
      // Cancel all simulations
      log_reg("Canceling all running simulations.");
      for(auto simParams: mSceneNode->mSimParams){
         simParams->getStatus(s);
         if(s == SimStatus::Running){
            simParams->cancel();}}
   } else log_err("SimNumber not a valid value. Only >=0 accepted.");

   return MS::kSuccess;
}

MStatus StrandsCmd::SelectOutput(int iSim){
   if(isValid(iSim)){
      mSceneNode->outputResult(iSim);
      return MS::kSuccess;
   }
   log_err("Wrong simulation number(%d). Must be positive and less than nParams (%zu).",
           iSim, mSceneNode->mSimParams.size());
   return MS::kFailure;
}

MStatus StrandsCmd::DeleteOutput(int iSim){
   // -1 used to delete all outputs.
   if(isValid(iSim) || iSim==-1){
      mSceneNode->deleteOutput(iSim);
      return MS::kSuccess;
   }
   log_err("Invalid sim number.");
   return MS::kFailure;
}

MStatus StrandsCmd::ReleaseSim(int iSim){
   MThreadAsync::release();
   SimStatus s;
   if(isValid(iSim)){
      SP<SimParams> simParams(mSceneNode->mSimParams[iSim]);
      simParams->getStatus(s);
      if(simParams->getReleased()){
         log_reg("Already released simulation.");
         return MS::kSuccess;
      }
      switch(s){
      case SimStatus::Deleted:
         log_err("Trying to cancel deleted simulation");
         break;
      case SimStatus::Completed:
      case SimStatus::Error:
      case SimStatus::Canceled:
         MThreadAsync::release();
         simParams->release();
         log_reg("Simulation thread released.");
         break;
      case SimStatus::Running:
         log_reg("Simulation still running. Cancel first or run to completion.");
         break;
      default:
         log_err("Cannot understand simulation status.");
      }
   }
   return MS::kSuccess;
}
