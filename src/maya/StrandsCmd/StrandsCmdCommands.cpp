// Project includes
#include <MayaAttr.hpp>
#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <maya/MStringArray.h>
#include <StrandsCmd/StrandsCmd.hpp>
#include <Strand/StrandMayaShape.h>

// Maya includes
#include <maya/MItSelectionList.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MFnDagNode.h>

using namespace std;
using namespace Eigen;

MStatus StrandsCmd::AddRigid(){
   MStatus ms;
   // Objects for input & output rigid meshes
   MObject meshObj, transObj;
   MObject outMeshObj, outTransObj;
   int rid, oid;

   MStringArray result;

   MSelectionList GET_SLIST(sList, 1);
   MString melCommand, transName, outTransName;
   MFnDependencyNode outTransDN, outMeshDN;
   MPlug shearPlug;

   // Select and set OBJs
   MCHECKRET(getMeshObj(transObj, sList));
   MCHECKRET(getShape(meshObj, transObj));
   MFnDependencyNode transDN(transObj), meshDN(meshObj);
   transName = transDN.name();

   // Lock the shear attributes
   transDN.findPlug("shear", &ms).setLocked(true);

   /** Create duplicate object **/
   // Create duplicate for rigid object
   MFnDagNode transDagNode(transObj);
   outTransObj = transDagNode.duplicate(false, false, &ms);
   MCHECKERROR(ms, "Could not duplicate node for adding rigid.");

   outTransDN.setObject(outTransObj);
   outTransName = "out"+transName;
   outTransDN.setName(outTransName);
   outTransName = outTransDN.name();

   /** Attributes **/
   // Shape attributes
   MCHECKRET(addRigidShapeAttr(meshObj));
   // Transform attributes
   MCHECKRET(addRigidTransformAttr(transObj));

   // Get the output mesh and transform objects
   // MCHECKRET(getObjectByName(outTransName, outTransObj));
   MCHECKRET(getShape(outMeshObj, outTransObj));
   outMeshDN.setObject(outMeshObj); outTransDN.setObject(outTransObj);
   MObject outAttr;
   ADD_DN_POINTATTR2(outTransDN, kAttrPosVelLong, kAttrPosVelShort);
   ADD_DN_POINTATTR2(outTransDN, kAttrAngVelLong, kAttrAngVelShort);

   // Connect the input and output objects, and their attributes
   MCHECKRET(connectObjects(transObj, outTransObj));

   // Only scale is allowed. Shear is locked for both
   runMelCommand("connectAttr \"" + transName + ".scale\" \"" + outTransName + ".scale\"");

   /** SceneNode connections **/
   // Add objects to sceneNode
   MCHECKERROR(mSceneNode->addRigid(rid, oid, meshObj, outMeshObj, transObj, outTransObj),
               "Failed to add rigid");
   log_verbose("Rigid and output added to scene: %s(%s)",
               transName.asChar(), outTransName.asChar());

   // Finally, select the input transform again
   // NOTE Transform gets changed when duplicating. Hence this is necessary
   setSelectedObject(transObj);

   // REQUIRED_CMD("select -r \"" + transName + "\"");

   // Set the final result
   this->setResult(transName);
   return MS::kSuccess;
}

MStatus StrandsCmd::AddStrand(MString name, double length, double locatorScale){
   log_verbose("Adding strand %s", name.asChar());

   MObject inTransObj, outTransObj, inShapeObj, outShapeObj;
   MStatus ms;

   /** Strand Objects **/

   // Add a StrandMaya input Object
   MCHECKRET(createInOutObject(inTransObj, outTransObj, StrandMaya::id, name));

   // Get input and Output objects, so far you have only transforms
   MCHECKRET(getShape(inShapeObj, inTransObj));
   MCHECKRET(getShape(outShapeObj, outTransObj));

   MFnDependencyNode inShapeDN(inTransObj), outShapeDN(outTransObj);
   /** In attributes **/
   MCHECKRET(addStrandShapeAttr(inShapeObj));
   MCHECKRET(addStrandTransAttr(inTransObj));
   MCHECKRET(addOutStrandTransAttr(outTransObj));
   MCHECKRET(addOutStrandShapeAttr(outShapeObj));

   /** Out attributes **/
   MCHECKRET(connectObjects(inTransObj, outTransObj));

   // TODO Break this into functions
   /** Locators **/
   // Input locators
   MObject locatorObj[2];
   bool LorQ = 1;
   int pid[2], p_oid[2], pointIndex;
   // pointIndex: Index of point in strand, not taking into account order.
   for(int ii=0; ii<2; ii++){
      // Add a locator
      MCHECKRET(this->addPointAndNode(inShapeObj, ii, locatorObj[ii],
                                     MPoint(ii*length,0,0), (float)locatorScale, pointIndex));
      log_random("Point added at pointIndex %d", pointIndex);

      MCHECKRET(this->addPointAttrs(locatorObj[ii]));
      MCHECKRET(this->addNewIndex(inShapeObj, pointIndex));
      MCHECKRET(this->connectPointID(inShapeObj, locatorObj[ii], pointIndex));
      // Connect translates
      MCHECKRET(this->connectTranslate(inShapeObj, locatorObj[ii], pointIndex));
      // Add a point with the locator object
      MCHECKRET(mSceneNode->addPoint(pid[ii], p_oid[ii], locatorObj[ii], LorQ));
   }

   // Output locators
   // Only need to add the locator, no node to be added to strand for this
   MObject outLocatorObj[2];
   for(int ii=0; ii<2;ii++){
      MCHECKRET(this->addPointAndNode(outShapeObj, ii, outLocatorObj[ii],
                                     MPoint(ii*length,0,0), (float)locatorScale, pointIndex));
      MCHECKRET(connectObjects(locatorObj[ii],outLocatorObj[ii]));
      MCHECKRET(this->connectTranslate(outShapeObj, outLocatorObj[ii], pointIndex));
      MCHECKRET(this->addOutPointAttrs(outLocatorObj[ii]));
   }

   /** Adding strand object **/
   int sid, s_oid;
   MCHECKERROR(mSceneNode->addStrand(sid, s_oid, inTransObj, outTransObj), "Strand could not be added");
   log_verbose("Strand %s added", name.asChar());

   /** Setting the result array **/
   MStringArray strArray;
   strArray.setLength(3);

   strArray[0] = MFnDependencyNode(inTransObj).name();
   MFnDependencyNode locatorDN[2];
   locatorDN[0].setObject(locatorObj[0]);
   locatorDN[1].setObject(locatorObj[1]);
   strArray[1] = MFnDependencyNode(locatorObj[0]).name();
   strArray[2] = MFnDependencyNode(locatorObj[1]).name();
   this->setResult(strArray);
   return ms;
}

MStatus StrandsCmd::AddJoint(MString name, double localScale){
   MStatus ms;
   MFnNumericAttribute nAttr;
   MFnDependencyNode jointDN, rigidDN[2], outJointDN;
   MObject    jointShapeObj,    jointTransObj;
   MObject outJointShapeObj, outJointTransObj;
   MSelectionList GET_SLIST(sList, 2);                   

   MObject rigidTransform[2];
   MString jointShapeName, jointTransformName;
   MString outName = "out" + name;


   // Get the trnasforms for the selected objects
   uint nObjects;
   MCHECKRET(getSelectedRigidObjects(nObjects, rigidTransform, sList));

   // Found rigids. Now get there DNs
   rigidDN[0].setObject(rigidTransform[0]);
   if(nObjects==2){rigidDN[1].setObject(rigidTransform[1]);}

   MCHECKRET(createLocator(   jointTransObj,    jointShapeObj, (ftype)localScale, &name));
   MCHECKRET(createLocator(outJointTransObj, outJointShapeObj, (ftype)localScale, &outName));
   MCHECKRET(connectObjects(jointTransObj, outJointTransObj));
   MCHECKRET(addJointTransformAttr(jointTransObj));
   MCHECKRET(addOutJointTransformAttr(outJointTransObj));

   MCHECKRET(   jointDN.setObject(   jointShapeObj));
   MCHECKRET(outJointDN.setObject(outJointShapeObj));
   log_random("Joint name set to %s(%s)", name.asChar(), outName.asChar());
   REQUIRED_CMD("setAttr \"" + name + ".displayLocalAxis\" 1");
   REQUIRED_CMD("setAttr \"" + name + ".selectHandleX\"    1");
   // Place at the rigid.
   REQUIRED_CMD("copyAttr -v -at t \"" + rigidDN[0].name()  + "\" \"" + name + "\"");

   MCHECKRET(addJointDebugObjs(jointTransObj, outJointTransObj, name));

   int jid, oid;
   MCHECKERROR(mSceneNode->addJoint(jid, oid, jointShapeObj, nObjects, rigidTransform, mOptJointType),
               "Could not add joint to scene node.");

   // Select the joint and set the result
   setSelectedObject(jointTransObj);
   // REQUIRED_CMD("select -r " + name);
   this->setResult(name);
   return ms;
}

MStatus StrandsCmd::AddNode(double locatorScale){
   MStatus ms;

   // 1. Find the selected 2 nodes and strand.
   MSelectionList sList;
   MGlobal::getActiveSelectionList(sList);

   // 2. Get the selected points
   MObject inStrandShapeObj, inStrandTransObj,
      outStrandShapeObj, outStrandTransObj, locatorObj[2];
   int locatorIndex[2];
   MCHECKRET(getSelectedPoints(locatorObj, sList));

   // 3. Find the strand, along with the index for each point, and the position
   // for the new point
   int orderIndex = -1, pointIndex = -1;
   MPoint position;
   MCHECKRET(findStrandPosition(locatorObj, inStrandShapeObj, inStrandTransObj,
                                locatorIndex, position));

   // 4. Check that they are consecutive
   if(abs(locatorIndex[0] - locatorIndex[1]) == 1){
      // Set index accordingly
      orderIndex = (locatorIndex[0]>locatorIndex[1]?locatorIndex[0]:locatorIndex[1]);
   } else {
      log_err("Non-consecutive nodes selected");
      return MS::kFailure;
   }

   // 5. Add the input/output locators and connect locator and corresponding objects.
   MObject inLocator, outLocator;
   // Create the locator input strand
   MCHECKRET(addPointAndNode(inStrandShapeObj, orderIndex, inLocator,
                             position, (float)locatorScale, pointIndex));

   MCHECKRET(findOutput(outStrandTransObj, inStrandTransObj));
   MCHECKRET(getShape(outStrandShapeObj, outStrandTransObj));

   // Create the locator on the output strand
   MCHECKRET(addPointAndNode(outStrandShapeObj, orderIndex, outLocator,
                             position, (float)locatorScale, pointIndex));

   MCHECKRET(addPointAttrs(inLocator));
   MCHECKRET(addOutPointAttrs(outLocator));
   MCHECKRET(addNewIndex(inStrandShapeObj, pointIndex));
   MCHECKRET(connectPointID(inStrandShapeObj, inLocator, pointIndex));

   // Connect translates
   MCHECKRET(connectTranslate(inStrandShapeObj ,  inLocator, pointIndex));
   MCHECKRET(connectTranslate(outStrandShapeObj, outLocator, pointIndex));

   // Connect the two locators
   MCHECKRET(connectObjects(inLocator, outLocator));

   MFnDependencyNode strandShapeDN(inStrandShapeObj);
   log_verbose("Adding node to strand %s", DNName(strandShapeDN));

   // Quasistatic node to be added
   int pid, oid;
   MCHECKRET(mSceneNode->addPoint(pid, oid, inLocator, 0));

   // Set result
   MFnDependencyNode locatorDN(inLocator);
   this->setResult(locatorDN.name());
   setSelectedObject(inLocator);
   // REQUIRED_CMD("select -r \"" + locatorDN.name() + "\"");
   return MS::kSuccess;
}

MStatus StrandsCmd::AssignToRigid(double locatorScale){
   MSelectionList sList;
   MGlobal::getActiveSelectionList(sList);
   MObject pointObj, constObj, rigidObj;
   int cid, oid;
   if(sList.length() == 1){
      // Fix the locator to the world
      getPointObj(pointObj, sList);
      MCHECKRET(fixPointWorld(constObj, pointObj, (float)locatorScale));
   } else if(sList.length() == 2){
      //Get the objects
      MObject rigidObj;
      MCHECKRET(getPointObj(pointObj, sList));
      MCHECKRET(getRigidObj(rigidObj, sList));
      MCHECKERROR_B(rigidObj.isNull() || pointObj.isNull(), "Could not get rigid/point object.");

      /** Setup the parent constraint**/
      // First try to add a constraint attribute and see if it is already constrained
      MObject constMsgAttrObj;
      MCHECKRET(addConstAttr(constMsgAttrObj, pointObj));
      // Constraint attribute successfully added -> add parent constraint
      MCHECKRET(createConstraint(constObj, pointObj, rigidObj, kCTParentConstraint, (float)locatorScale));
   } else{
      this->setResult(false);
      log_err("Incorrect number of selections.");
      return MS::kFailure;
   }
   // Let the scene node fix them too.
   MCHECKRET(mSceneNode->assignToRigid(pointObj, rigidObj));
   MCHECKRET(mSceneNode->addConstraint(cid, oid, constObj, pointObj));
   this->setResult(MFnDependencyNode(constObj).name());
   return MS::kSuccess;
}

MStatus createForceTypeAttr(MObject &forceTypeAttr){
   MStatus ms;
   MFnEnumAttribute eAttr;
   forceTypeAttr = eAttr.create(kAttrForceTypeLong, kAttrForceTypeShort, kFTWrenchInt, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   eAttr.addField(kFTRigidForce, kFTRigidForceInt);
   MCHECKRET(eAttr.addField(kFTWrench,kFTWrenchInt));
   return MS::kSuccess;
}

// NOTE This function adds a force to the scene without attaching it to any object. To use the force
// on any object, it needs to be attached using the AttachForce command
MStatus StrandsCmd::AddForce(MString name, double locatorScale){
   MStatus ms;
   MObject forceLocTransObj, forceLocShapeObj;

   // NOTE create sets the name for the locator, not the transform. But the
   // returned object is the transform (the attributes are added to transform)
   MCHECKRET(createLocator(forceLocTransObj, forceLocShapeObj, locatorScale, &name));
   MFnDependencyNode forceDN(forceLocTransObj);

   /** Attributes **/
   // Force specific attributes
   MCHECKRET(ADD_DN_INTATTR(forceDN , kAttrObjectIDLong      , kAttrObjectIDShort      , -1));
   MCHECKRET(ADD_DN_INTATTR(forceDN , kAttrForceIDLong       , kAttrForceIDShort       , -1));
   MCHECKRET(ADD_DN_BOOLATTR(forceDN, kAttrDebugLong         , kAttrDebugShort         , false));
   MCHECKRET(ADD_DN_INTATTR(forceDN , kAttrForceDynObjIDLong , kAttrForceDynObjIDShort , -1));
   MCHECKRET(ADD_DN_DBLATTR(forceDN , kAttrForceMagnitudeLong, kAttrForceMagnitudeShort, -1));

   MObject fTAttr;
   MCHECKRET(createForceTypeAttr(fTAttr));
   MCHECKRET(forceDN.addAttribute(fTAttr, MLOCAL));

   // Create wrench compound attribute with 6 children
   vector<string> attrSuffix {"RX", "RY", "RZ", "TX", "TY", "TZ"};
   MFnCompoundAttribute cAttr;
   MObject wrenchObj = cAttr.create(kAttrForceWrenchValLong, kAttrForceWrenchValShort, &ms);
   MCHECKRET(ms);
   for(auto suff: attrSuffix){
      MObject childAttr = dblAttr((kAttrForceWrenchValLong + suff).c_str(), (kAttrForceWrenchValShort+suff).c_str(), 0.0f);
      MCHECKERROR_B(childAttr.isNull(), "Could not create required attribute.");
      cAttr.addChild(childAttr);
   }

   MCHECKRET(forceDN.addAttribute(wrenchObj, MLOCAL));

   int fid, oid;
   mSceneNode->addForce(fid, oid, forceLocTransObj);
   this->setResult(forceDN.name());
   setSelectedObject(forceLocTransObj);
   return MS::kSuccess;
}

MStatus StrandsCmd::AttachForce(){
   log_verbose("Command desc: Attach force to select object.");
   MStatus ms;
   MSelectionList GET_SLIST_ITER(sList, it, 2);
   MObject forceObj, pointObj, rigidObj;
   MCHECKERROR(getForceObj(forceObj, sList), "No force selected");
   // Get rigid or point object.
   getPointObj(pointObj, sList); getRigidObj(rigidObj, sList);
   // If one of pointObj and rigidObj was found, proceed.
   MDGModifier dgm;
   // Check that one of them is null and the other is not.
   if(pointObj.isNull() != rigidObj.isNull()){
      MFnDependencyNode forceDN(forceObj);
      // If force and object found, execute this.
      MFnDependencyNode objDN;
      MObject obj        = pointObj.isNull()?rigidObj:pointObj;
      objDN.setObject(obj);
      MPlug oidPlug      = objDN.findPlug(kAttrObjectIDLong, true, &ms);
      MPlug dynObjIDPlug = forceDN.findPlug(kAttrForceDynObjIDLong, true, &ms);
      MCHECKRET(dgm.connect(oidPlug, dynObjIDPlug));
      MCHECKRET(dgm.doIt());
      MCHECKERROR(mSceneNode->attachForce(forceObj, obj), "Could not attach force.");
      return MS::kSuccess;
   }
   log_err("Select rigid OR point object along with the force object");
   return MS::kFailure;
}

MStatus StrandsCmd::ClearScene(){
   if(mSceneNode!=nullptr) return mSceneNode->clearCount();
   return MS::kSuccess;
}

MStatus StrandsCmd::ConstrainOnAxis(double locatorScale){
   log_verbose("Command desc: Constraining object on axis");
   MStatus ms;
   MSelectionList GET_SLIST(sList, 1);

   MObject rigidObj, constMsgAttrObj;
   MCHECKRET(getRigidObj(rigidObj, sList));
   MFnDependencyNode rigidDN(rigidObj);

   // Add constraint field to rigid object
   MCHECKERROR(addConstAttr(constMsgAttrObj, rigidObj), "Could not add const attr");

   // Create locator to hold the transform
   MObject constObj = MObject::kNullObj;
   ms = createConstraint(constObj, rigidObj, MObject::kNullObj, kCTLineConstraint, locatorScale);
   if(MFAIL(ms)){
      log_err("Could not create constraint.");
      MCHECKERROR(MFnDependencyNode(rigidObj).removeAttribute(constMsgAttrObj),
                  "Could not remove constraint message attribute.");
   }
   MFnDependencyNode constDN(constObj);
   constDN.setName(rigidDN.name()+"_constraint");

   // Add info to scene node
   int cid, oid;
   MCHECKRET(mSceneNode->addConstraint(cid, oid, constObj, rigidObj));
   this->setResult(constDN.name());

   // Locked to prevent deletion of constraint
   constDN.setLocked(true);
   return MS::kSuccess;
}

MStatus StrandsCmd::ToggleEnableStrand(){
   log_verbose("Command desc: Enabling/Disabling strand");
   MStatus ms;
   MSelectionList GET_SLIST_ITER(sList, iter, 1);
   MObject obj;
   MCHECKRET(sList.getDependNode(0, obj));
   MFnDependencyNode DN(obj);
   MString name = DN.name();
   bool enabled;
   REQUIRED_CMD("currentTime 1");

   // Check if a strand is selected
   if(!MFAIL(isObjType(obj, ObjType::STRND))){
      MPlug strandEnabledPlug = DN.findPlug(kAttrEnabledLong, &ms);
      ms = strandEnabledPlug.getValue(enabled);
      // Toggle strand's enable attribute
      strandEnabledPlug.setValue(!enabled);
      enabled = !enabled;
      // Find the output object to disable display.
      MObject outObj;
      MCHECKRET(findOutput(outObj, obj));
      MFnDependencyNode outDN(outObj);
      REQUIRED_CMD(MString("currentTime 2; setKeyframe -v ") + enabled
                   + " -at visibility \"" + outDN.name() + "\";");
   } else{
      log_err("Select a strand to toggle enable.");
      return MS::kFailure;
   }
   REQUIRED_CMD("currentTime 1");
   return MS::kSuccess;
}
// This is the relocation function
MStatus StrandsCmd::RelocateNode(){
   log_verbose("Command desc: Relocating node to new position");
   MStatus ms;
   // Get selection
   MSelectionList GET_SLIST_ITER(sList, iter, 1);

   MObject pointObj;
   ms = sList.getDependNode(0, pointObj);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MFnDependencyNode pointDN(pointObj);
   MString name = pointDN.name();
   this->setResult(false);
   if(isObjType(pointObj, ObjType::POINT)){
      MObject constObj, parentObj;
      bool exists = false;
      ms = findConstraint(constObj, pointObj, &exists);
      if(!MFAIL(ms) && exists){
         MCHECKRET(findConstraintSource(parentObj, constObj));
         if(parentObj.isNull()){return MS::kFailure;}
         MFnDependencyNode parentDN, constDN;
         parentDN.setObject(parentObj);
         constDN.setObject(constObj);
         REQUIRED_CMD("parentConstraint -e -maintainOffset " +
                      parentDN.name() +  " " + constDN.name());
      } else{return MS::kFailure;}
      this->setResult(true);
      return MS::kSuccess;
   } else{
      log_reg("Select one point");
   }
   return MS::kFailure;
}

MStatus StrandsCmd::disconnectPlugs(vector<MPlug> &pointPlugs, MPlug &ptPlug, MObject strandShapeObj, int index){
   MStatus ms;
   MDGModifier dgm;
   MPlug nodesPlug = MFnDependencyNode(strandShapeObj).findPlug(kAttrStrandNodesLong, true, &ms);
   MPlug tmpPlug, srcPlug;
   for(uint ii=0; ii<nodesPlug.numElements(); ii++){
      tmpPlug = nodesPlug.elementByLogicalIndex(ii);
      MCHECKRET(findSourcePlug(tmpPlug, srcPlug));
      dgm.disconnect(srcPlug, tmpPlug);
      if(ii==index){
         ptPlug = tmpPlug;
         continue;
      }
      pointPlugs.push_back(tmpPlug);
   }
   MCHECKRET(dgm.doIt());
   return MS::kSuccess;
}

MStatus StrandsCmd::DeleteStrandNode(){
   MStatus ms;
   MSelectionList GET_SLIST(sList, 2);                   

   MObject obj, strandTransObj, strandShapeObj, pointObj;

   MCHECKRET(MGlobal::getActiveSelectionList(sList));

   MCHECKERROR(getStrandObj(strandTransObj, sList), "Could not get strand object.");
   MCHECKERROR(getLocatorObj(pointObj, sList), "Could not get point object.");
   MCHECKRET(getShape(strandShapeObj, strandTransObj));

   vector<MObject> strandObjs; vector<int> indices;
   // Find the strands
   MCHECKERROR(findStrandsAtPoint(strandObjs, indices, pointObj),
               "Could not find strands at point.");
   // Confirm given strand belongs. Get index.
   MObject transObj;
   int index;
   for(int ii=0; ii<strandObjs.size(); ii++){
      if (strandObjs[ii]==strandShapeObj){
         index = indices[ii];
      }
   }

   return MS::kSuccess;
}

MStatus StrandsCmd::DeleteObject(){
   MStatus ms;
   MSelectionList GET_SLIST(sList, 1);                   
   MObject obj;
   MFnDependencyNode DN(obj);
   MString name = DN.name();
   MCHECKRET(sList.getDependNode(0, obj));
   MCHECKRET(deleteSceneObject(obj));
   this->setResult(name);
   return MS::kSuccess;
}

MStatus StrandsCmd::GetState(int iSim){
   log_warn("Getting state for sim %d.", iSim);
   if(isValid(iSim)){
      auto params = mSceneNode->mSimParams[iSim];
      if(params->isRunning()){
         log_err("Simulation still running.");
         return MS::kFailure;
      }
      auto state = params->getState();
      MCHECKERROR(!state.has_value(), "Could not get state.");
      // If state value was obtained, set result.
      setResult(state.value().c_str());
      return MS::kSuccess;
   }
   setResult(false);
   return MS::kFailure;
}

MStatus StrandsCmd::DefineState(int iSim, MString fmtString){
   if(isValid(iSim)){
      auto params = mSceneNode->mSimParams[iSim];
      MStringArray splitFmt;
      MCHECKERROR(fmtString.split(' ', splitFmt), "Could not split format.");
      for(int iStr = 0; iStr<splitFmt.length(); iStr++){
         MStringArray arr;
         auto str = splitFmt[iStr];
         MCHECKERROR(str.split('.', arr), "Could not split format substring.");
         MCHECKERROR(arr.length()!=2    , "Wrong number of splits('.') for substring.");
         string objName  = arr[0].asChar();
         string attrName = arr[1].asChar();
         log_reg("Adding to state: %s %s", objName.c_str(), attrName.c_str());

         params->checkAppendFmt(objName, attrName);
      }
      // Mark format complete
      params->fmtSet();
      return MS::kSuccess;
   }
   return MS::kFailure;
}
