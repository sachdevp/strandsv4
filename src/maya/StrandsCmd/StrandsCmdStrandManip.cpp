#include <MayaAttr.hpp>
#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <maya/MStringArray.h>
#include <StrandsCmd/StrandsCmd.hpp>
#include <Strand/StrandMayaShape.h>

using namespace std;

MStatus combinePoints(vector<MObject> &newPoints, vector<MObject> oldPoints[2]){
   int sz0 = oldPoints[0].size();
   int sz1 = oldPoints[1].size();
   newPoints.resize(sz0+sz1-1);
   if(oldPoints[0].back() == oldPoints[1].front()){
      copy(oldPoints[0].begin(), oldPoints[0].end(), newPoints.begin());
      copy(oldPoints[1].begin()+1, oldPoints[1].end(), newPoints.begin()+sz0);
   }
   if(oldPoints[0].front() == oldPoints[1].front()){
      reverse_copy(oldPoints[0].begin(), oldPoints[0].end(), newPoints.begin());
      copy(oldPoints[1].begin()+1, oldPoints[1].end(), newPoints.begin()+sz0);
   }
   if(oldPoints[0].back() == oldPoints[1].back()){
      copy(oldPoints[0].begin(), oldPoints[0].end(), newPoints.begin());
      reverse_copy(oldPoints[1].begin(), oldPoints[1].end()-1, newPoints.begin()+sz0);
   }
   if(oldPoints[0].front() == oldPoints[1].back()){
      reverse_copy(oldPoints[0].begin(), oldPoints[0].end(), newPoints.begin());
      reverse_copy(oldPoints[1].begin(), oldPoints[1].end()-1, newPoints.begin()+sz0);
   }

   return MS::kSuccess;
}

// Joins strands and sets the result to the name of the joined locator
MStatus StrandsCmd::MergeStrands(){
   log_verbose("Joining strands together.");
   MStatus ms;
   MObject pointObj, outPointObj;
   MSelectionList GET_SLIST(sList, 1);

   MCHECKRET(getPointObj(pointObj, sList));
   MCHECKRET(findOutput(outPointObj, pointObj));

   MPlugArray plugs;
   MCHECKRET(findOutputConnections(MFnDependencyNode(pointObj).findPlug(kAttrPointIDLong), plugs));

   MCHECKERROR_B(plugs.length()>2, "More than 2 plugs.");
   MObject strandObjs[2];
   strandObjs[0] = plugs[0].node();
   strandObjs[1] = plugs[1].node();
   vector<MObject> strandPointObjs[2], newStrandPointObjs;
   bool disconnect = true;
   getStrandPoints(strandPointObjs[0], strandObjs[0], disconnect);
   getStrandPoints(strandPointObjs[1], strandObjs[1], disconnect);

   MCHECKRET(combinePoints(newStrandPointObjs, strandPointObjs));

   MObject outStrand, strandTransObjs[2];
   MCHECKRET(getTransform(strandTransObjs[1], strandObjs[1]));
   MCHECKRET(getTransform(strandTransObjs[0], strandObjs[0]));
   MCHECKRET(createStrand(outStrand, newStrandPointObjs,
                          MFnDependencyNode(strandTransObjs[0]).name() + "_"
                          + MFnDependencyNode(strandTransObjs[1]).name()));

   MCHECKWARN(deleteObjectAndOutput(strandTransObjs[0]), "Could not delete strand 0");
   MCHECKWARN(deleteObjectAndOutput(strandTransObjs[1]), "Could not delete strand 1");

   MFnDependencyNode DN(outStrand);
   this->setResult(DN.name());
   return MS::kSuccess;
}

// Joins strands and sets the result to the name of the joined locator
MStatus StrandsCmd::JoinStrands(MString junctionName){
   log_verbose("Joining strands together.");
   MStatus ms;
   MObject locatorObjs[2];

   MSelectionList sList;
   MGlobal::getActiveSelectionList(sList);
   MCHECKRET(getSelectedPoints(locatorObjs, sList));

   // 1. Get list of destinations of the transform and pointid
   MObject outLocatorObjs[2], delPointObj, outDeleteObj;
   findOutput(outLocatorObjs[0], locatorObjs[0]);
   findOutput(outLocatorObjs[1], locatorObjs[1]);

   MObject inLocator = mergeLocators(locatorObjs, true, &delPointObj, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   // Try to delete the object not used as output
   MObject outLocator = mergeLocators(outLocatorObjs, false, &outDeleteObj, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MCHECKWARN(deletePoint(delPointObj), "Could not delete remaining object");

   MFnDependencyNode DN(inLocator);
   DN.setName(junctionName);
   this->setResult(DN.name());
   return MS::kSuccess;
}

MStatus StrandsCmd::SplitJunction(){
   MStatus ms;
   MObject pointObj, outPointObj;
   MSelectionList GET_SLIST(sList, 1);

   MCHECKRET(getPointObj(pointObj, sList));
   MCHECKRET(findOutput(outPointObj, pointObj));
   MFnDependencyNode pointDN(pointObj);
   log_verbose("Splitting junction %s", DNName(pointDN));
   MPlug pidPlug = pointDN.findPlug(kAttrPointIDLong);
   MPlugArray strandNodePlugs;
   // Get output plugs for pid in strand shape MObject
   MCHECKRET(findOutputConnections(pidPlug, strandNodePlugs));

   MPoint pos;
   MCHECKRET(getTranslate(pos, pointObj));

   if(strandNodePlugs.length()<2){log_warn("Not a junction."); return MS::kSuccess;}

   for(uint ii=1; ii<strandNodePlugs.length(); ii++){
      MObject inLocObj, outLocObj,
         inStrandShapeObj, outStrandShapeObj,
         inStrandTransObj, outStrandTransObj;
      CHECK_MSTATUS_AND_RETURN_IT(ms);

      // Get strand shape from output plug for pid
      int index = strandNodePlugs[ii].logicalIndex(&ms);
      inStrandShapeObj = strandNodePlugs[ii].node(&ms);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
      // Get all corresponding objs
      MCHECKRET(getTransform( inStrandTransObj, inStrandShapeObj));
      MCHECKRET(findOutput  (outStrandTransObj, inStrandTransObj));
      MCHECKRET(getShape    (outStrandShapeObj, outStrandTransObj));

      // Create input and output locators for new point.
      MCHECKRET(createLocatorAtPos(inLocObj, pos));
      MCHECKRET(createLocatorAtPos(outLocObj, pos));
      MCHECKRET(connectObjects(inLocObj, outLocObj));

      // Disconnect current connections
      bool disconnect=true;
      MCHECKRET(connectTranslate(inStrandShapeObj, pointObj, index, disconnect));
      MCHECKRET(connectTranslate(outStrandShapeObj, outPointObj, index, disconnect));
      MCHECKRET(connectPointID(inStrandShapeObj, pointObj, index, disconnect));

      // Fresh connections with new locator
      MCHECKRET(addPointAttrs(inLocObj));
      MCHECKRET(addNewIndex(inStrandShapeObj, index));
      MCHECKRET(connectPointID(inStrandShapeObj, inLocObj, index));

      MCHECKRET(connectTranslate(inStrandShapeObj, inLocObj, index));
      MCHECKRET(connectTranslate(outStrandShapeObj, outLocObj, index));

      // Add new locator/point to scene.
      int pid, oid;
      MCHECKRET(mSceneNode->addPoint(pid, oid, inLocObj, 1));
   }

   this->setResult(true);
   return MS::kSuccess;
}

// Creates strand given pointObjs with all attributes etc already installed.
// Useful for splitting and merging tendons.
MStatus StrandsCmd::createStrand(MObject &strandObj, vector<MObject> pointObjs, MString name){
   MFnDependencyNode inTransDN, outTransDN;
   MObject inTransObj, outTransObj, inShapeObj, outShapeObj;
   MDagPath inTransDP, outTransDP;
   MStatus ms;

   /** Strand Objects **/

   // Add a StrandMaya input Object
   MCHECKRET(createInOutObject(inTransObj, outTransObj, StrandMaya::id, name));

   // Get input and Output objects, so far you have only transforms
   MCHECKRET(getShape(inShapeObj, inTransObj));
   MCHECKRET(getShape(outShapeObj, outTransObj));

   MFnDependencyNode inShapeDN(inTransObj), outShapeDN(outTransObj);
   /** In attributes **/
   MCHECKRET(addStrandShapeAttr(inShapeObj));
   MCHECKRET(addStrandTransAttr(inTransObj));
   MCHECKRET(addOutStrandTransAttr(outTransObj));
   MCHECKRET(addOutStrandShapeAttr(outShapeObj));

   /** Out attributes **/
   MCHECKRET(connectObjects(inTransObj, outTransObj));

   strandObj = inTransObj;

   if(strandObj.isNull()) return MS::kFailure;
   MCHECKERROR_B((pointObjs.size()<2), "Not enough points to add a strand");

   // Connect attributes: pointID and translate
   int pointIndex, oid, pid, orderIndex;
   MObject outPointObj;
   orderIndex=0;
   for(auto pointObj: pointObjs){
      log_verbose("Adding point %s to strand %s", MObjName(strandObj), MObjName(pointObj));
      MCHECKRET(findOutput(outPointObj, pointObj));
      MCHECKRET(addIndexToStrand(inShapeObj, orderIndex, pointIndex));
      MCHECKRET(addNewIndex(inShapeObj, pointIndex));
      MCHECKRET(connectTranslate(inShapeObj, pointObj, pointIndex));
      MCHECKRET(connectPointID  (inShapeObj, pointObj, pointIndex));
      MCHECKRET(addIndexToStrand(outShapeObj, orderIndex, pointIndex));
      MCHECKRET(connectTranslate(outShapeObj, outPointObj, pointIndex));
      orderIndex++;
   }

   int sid, s_oid;
   MCHECKERROR(mSceneNode->addStrand(sid, s_oid, inTransObj, outTransObj), "Strand could not be added");
   log_warn("Strand %s added", name.asChar());
   return MS::kSuccess;
}

MStatus StrandsCmd::getStrandPoints(vector<MObject> &strandPointObjs, MObject inStrandShapeObj, bool disconnect){
   MFnDependencyNode inStrandShapeDN(inStrandShapeObj);
   MStatus ms; MPlug strandNodesPlug;
   // Get the order array
   MIntArray order = getAttrArray(inStrandShapeDN.findPlug(kAttrStrandOrderLong), &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   strandNodesPlug = inStrandShapeDN.findPlug(kAttrStrandNodesLong, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   int strandLen = order.length();
   if(strandLen>0){
      strandPointObjs.resize(strandLen);
      MObject tPointObj;
      for(uint ii=0; ii<strandLen; ii++){
         // Get Point MObject.
         MCHECKRET(findPlugIndexSourceNode(tPointObj, strandNodesPlug, ii));
         if(disconnect) MCHECKRET(connectTranslate(inStrandShapeObj, tPointObj, ii, disconnect));
         if(disconnect) MCHECKRET(connectPointID(inStrandShapeObj, tPointObj, ii, disconnect));
         // Sanity check.
         MCHECKERROR_B(((uint)order[ii])>=order.length(),
                       "Problem with obtained order array of %s", DNName(inStrandShapeDN));
         strandPointObjs[order[ii]]=tPointObj;
      }
   }
   return MS::kSuccess;
}

// Gets rid of split point and any constraints on it. Two new points created in its stead.
MStatus StrandsCmd::getSplitStrandsPoints
(array<vector<MObject>, 2>& vStrandsPointObjs, vector<MObject> strandPointObjs, MObject splitPointObj){
   int index = 0;
   MFnDependencyNode splitPointDN(splitPointObj);
   for(auto pointObj: strandPointObjs){
      if(splitPointObj == pointObj){
         // Split point reached. Duplicate it to two new points. Delete the original point to avoid
         // constraint issues.
         MPoint splitLoc;
         GET_POINT(splitLoc, splitPointDN, kAttrTransLong, 1.0);
         for(int i=0; i<2; i++){
            index=i;
            MObject dupPointObj, outDupPointObj;
            splitLoc = splitLoc + (i-0.5f)*2.0* MPoint(0.01, 0.01, 0.01, 0.0);
            MCHECKRET(createLocatorAtPos(dupPointObj, splitLoc));
            MCHECKRET(addPointAttrs(dupPointObj));
            int pid, oid;
            bool LorQ = true;
            MCHECKRET(mSceneNode->addPoint(pid, oid, dupPointObj, LorQ));
            MCHECKRET(createLocatorAtPos(outDupPointObj, splitLoc));
            MCHECKRET(addOutPointAttrs(outDupPointObj));
            MCHECKRET(connectObjects(dupPointObj, outDupPointObj));
            vStrandsPointObjs[index].push_back(dupPointObj);
         }
         MCHECKRET(deletePoint(splitPointObj));
         continue;
      }
      vStrandsPointObjs[index].push_back(pointObj);
   }
   return MS::kSuccess;
}

MStatus StrandsCmd::SplitStrand(){
   MStatus ms;
   MObject pointObj, inStrandShapeObj, inStrandTransObj;
   // Get selected point.
   MSelectionList GET_SLIST(sList, 1);
   uint splitIndex;
   MCHECKRET(getPointObj(pointObj, sList));
   MFnDependencyNode pointDN(pointObj);
   MPlug pointIDPlug, tPointIDPlug;
   pointIDPlug = pointDN.findPlug(kAttrPointIDLong, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);

   // Get corresponding strand
   MPlug strandShapePointPlug;
   MCHECKERROR(findDestinationPlug(strandShapePointPlug, pointIDPlug), "");

   inStrandShapeObj = strandShapePointPlug.node();
   MCHECKRET(getTransform(inStrandTransObj, inStrandShapeObj));
   MFnDependencyNode inStrandTransDN(inStrandTransObj);
   MFnDependencyNode inStrandShapeDN(inStrandShapeObj);
   MString transName = inStrandTransDN.name();
   // Reorder strandPointObjs according to obtained order
   vector<MObject> strandPointObjs;
   array<vector<MObject>,2> vStrandsPointObjs;
   bool disconnect = true;
   MCHECKRET(getStrandPoints(strandPointObjs, inStrandShapeObj, disconnect));
   MCHECKRET(getSplitStrandsPoints(vStrandsPointObjs, strandPointObjs, pointObj));

   array<MObject, 2> vStrandObjs;
   MCHECKRET(createStrand(vStrandObjs[0], vStrandsPointObjs[0], transName+"0"));
   MCHECKRET(createStrand(vStrandObjs[1], vStrandsPointObjs[1], transName+"1"));

   MCHECKWARN(deleteObjectAndOutput(inStrandTransObj), "Could not delete strand %s.", DNName(inStrandTransDN));
   MStringArray res;
   res.append(transName+"0");
   res.append(transName+"1");
   this->setResult(res);
   return ms;
}
