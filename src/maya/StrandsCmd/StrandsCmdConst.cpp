#include <StrandsCmd/StrandsCmd.hpp>

std::array<const char*, StrandsCmd::nAttrs> StrandsCmd::shortAttrNames = {
    "-" kSceneNameFlag , "-" kCmdFlag         , "-" kNameFlag,
    "-" kStepsFlag     , "-" kJtFlag,
    "-" kStabilizeFlag , "-" kMatrixOutputFlag,
    "-" kDataOutDirFlag, "-" kStrandLengthFlag, "-" kLocScaleFlag,
    "-" kSimNumberFlag , "-" kModStringFlag   , "-" kOffStringFlag,
    "-" kTimeFlagShort , "-" kLabelFlagShort, "-" kActStringFlag,
    "-" kFmtStringFlagShort, "-" kExportFileFlagShort};

std::array<const char*, StrandsCmd::nAttrs> StrandsCmd::longAttrNames = {
   "-" kSceneNameFlagLong , "-" kCmdFlagLong         , "-" kNameFlagLong,
   "-" kStepsFlagLong     , "-" kJtFlagLong,
   "-" kStabilizeFlagLong , "-" kMatrixOutputFlagLong,
   "-" kDataOutDirFlagLong, "-" kStrandLengthFlagLong, "-" kLocScaleFlagLong,
   "-" kSimNumberFlagLong , "-" kModStringFlagLong   , "-" kOffStringFlagLong,
   "-" kTimeFlagLong      , "-" kLabelFlagLong, "-" kActStringFlagLong,
   "-" kFmtStringFlagLong, "-" kExportFileFlagLong};

std::array<MSyntax::MArgType, StrandsCmd::nAttrs> StrandsCmd::attrTypes = {
    MSyntax::kString , MSyntax::kString , MSyntax::kString,
    MSyntax::kLong   , MSyntax::kString ,
    MSyntax::kBoolean, MSyntax::kBoolean,
    MSyntax::kString , MSyntax::kDouble , MSyntax::kDouble,
    MSyntax::kLong   , MSyntax::kString , MSyntax::kString,
    MSyntax::kDouble , MSyntax::kString , MSyntax::kString,
    MSyntax::kString, MSyntax::kString};
