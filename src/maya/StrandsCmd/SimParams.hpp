// FIXME Note purpose of class

#pragma once
#include <include_common.hpp>
#include <maya/MString.h>
#include <maya/MObject.h>
#include <Recorder/Recorder.hpp>

enum class SimStatus{Completed=0, Running=1, Deleted=2, Canceled=3, Error=4};

using ActMap = std::map<std::string, float>;
using ActKeyframes = std::map<int, ActMap>;
using Keyframes = std::map<int, float>;
using OffMap = MAPSTRV3;
using AttrTuple = std::tuple<ObjType, int, std::string>;

class SimParams{
   SP<Recorder> rec;
   bool released, toCancel;
   std::string mModString;
   SimStatus status;
   MString mLabelStr;
   int dtx;
   float dt;
   // Overriding activation keyframes in Maya with input activation map
   bool bActOverrideSet;
   // Keyframes for activation of muscles provided?
   bool bActKeyframesSet;
   // Modstring/Offset set?
   bool bModSet, bOffSet;
   // Was the format of state output set for GetState function
   bool bFmtSet;
   ActKeyframes mActKeyframes;
   ActMap mActMap;
   OffMap mOffMap;
   Strands::Scene* mScene;
   SimOutput* mSimOut;
   std::vector<AttrTuple> mFmtTuples;

public:
   ~SimParams();
   SimParams(SP<Recorder> _rec, Strands::Scene *scene, SimOutput* _simOut, MObject _sceneObj,
             int _nSteps, int _nFrames, double dt, int dtx, MString _labelString);
   // Number of steps the simulation is to take, and the number of frames corresponding to it.
   int nSteps, nFrames;
   MObject sceneObj;
   // Time in seconds
   DUR<double> simTime, recTime;

   /** Get functions **/
   inline bool  getReleased()              {return released;}
   inline auto  getMaxStrandNodes(int sid) {return mSimOut->getMaxStrandNodes(sid);}
   inline auto  getRecorder()              {return rec;}
   inline float getLargeTimestep()         {return dt*dtx;}
   inline bool  toBeCanceled()             {return toCancel;}

   inline void  getStatus(SimStatus& outStatus, IVec &result) {outStatus = status; result = rec->getStatus();}
   inline void  getStatus(SimStatus& outStatus)               {outStatus = status;};

   auto getModString(){return mModString;}
   STRMAP getDebugInfo();
   RET getOffsets(MAPSTRV3 &_offsets);

   /** Set functions **/
   inline void release(){released = true;}
   inline void cancel(){toCancel = true;};
   inline void setStatus(SimStatus s){status = s;}
   inline void setRunning(){status = SimStatus::Running;}
   inline void setCanceled(){status = SimStatus::Canceled;}
   inline void setError(){status = SimStatus::Error;}
   inline void setCompleted(){status = SimStatus::Completed;}
   inline void getdtx(int &_dtx){_dtx = dtx;}
   inline void getdt(float &_dt){_dt = dt;}
   inline void getTimestep(float &_dt, int &_dtx){getdtx(_dtx); getdt(_dt);}
   inline bool isRunning(){return status == SimStatus::Running;}
   RET setOffMap(OffMap _offMap);
   RET setModString(std::string _modString);
   RET setActMap(ActMap _actMap);
   RET setActKeyframes(ActKeyframes _actKeyframes);
   RET stepFrames(int nFrames);
   RET updateInputs(int iFrame=-1);

   std::optional<std::string> getState();
   std::optional<std::string> getAttr(AttrTuple attrTuple);

   RET setFramesAndSteps(int _nFrames, int _nSteps);
   auto& getFrames(){return mSimOut->frames;};
   /** Get maps **/
   auto getStrandMObjMap(){return mSimOut->strandMObjs;}
   auto getPointMObjMap(){return mSimOut->pointMObjs;}
   auto getRigidMObjMap(){return mSimOut->rigidMObjs;}
   auto getForceMObjMap(){return mSimOut->forceMObjs;}
   auto getJointMObjMap(){return mSimOut->jointMObjs;}
   std::optional<std::pair<ObjType, int>> findObj(std::string objName);
   bool isAttr(ObjType, std::string attrName);
   // const static int nPointAttrs  = 2;
   // const static int nRigidAttrs  = 3;
   // const static int nJointAttrs  = 4;
   // const static int nStrandAttrs = 2;
   // const static StrArr<nPointAttrs>  pointAttrs;
   // const static StrArr<nRigidAttrs>  rigidAttrs;
   // const static StrArr<nStrandAttrs> strandAttrs;
   // const static StrArr<nJointAttrs>  jointAttrs;
   const static StrVec vPointAttrs;
   const static StrVec vRigidAttrs;
   const static StrVec vStrandAttrs;
   const static StrVec vJointAttrs;
   const static StrVec vIndicatorAttrs;
   const static std::map<ObjType, StrVec> objTypeAttrsMap;
   MStatus appendFmt(AttrTuple);
   MStatus checkAppendFmt(std::string objName, std::string attrName);
   // Marking the format completed
   MStatus fmtSet(){bFmtSet = true; return MS::kSuccess;}
   auto getFmt(){return mFmtTuples;}
   static std::map<ObjType, StrVec> createMap(){
      std::map<ObjType, StrVec> attrsMap;
      attrsMap[ObjType::POINT] = SimParams::vPointAttrs;
      attrsMap[ObjType::RIGID] = SimParams::vRigidAttrs;
      attrsMap[ObjType::STRND] = SimParams::vStrandAttrs;
      attrsMap[ObjType::JOINT] = SimParams::vJointAttrs;
      attrsMap[ObjType::INDIC] = SimParams::vIndicatorAttrs;
      return attrsMap;
   }
};
