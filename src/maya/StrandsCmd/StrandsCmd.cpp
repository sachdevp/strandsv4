// @auth: Prashant Sachdeva (sachdevp@cs.ubc.ca)
// @desc: Add Strand Command to interact with SceneNode. This file contains
//        command helpers and command parsing.

#include <sstream>
#include <iostream>
#include <iomanip>

#include <maya/MFileIO.h>
#include <maya/MLibrary.h>
#include <maya/MSyntax.h>

#include <maya/MDGModifier.h>
#include <maya/MDagPath.h>
#include <maya/MFnPlugin.h>
#include <maya/MFnTransform.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MItSelectionList.h>

#include <maya/MEulerRotation.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MMatrix.h>
#include <maya/MPlug.h>
#include <maya/MPointArray.h>
#include <maya/MStatus.h>
#include <maya/MString.h>
#include <maya/MSyntax.h>
#include <maya/MVector.h>

#include "MayaHelper.hpp"
#include "MayaStrings.h"
#include <DynObj/Rigid.hpp>
#include <DynObj/Strand.hpp>
#include <MayaAttr.hpp>
#include <SE3.cpp>
#include <Scene/Scene.hpp>
#include <Strand/StrandMayaShape.h>

#include "StrandsCmd.hpp"
#include <StrandsCmd/SimParams.hpp>
#include <Strand/StrandMaya_export.hpp>

using namespace std;
void *StrandsCmd::creator() {
   return new StrandsCmd;
}

MSyntax StrandsCmd::createSyntax(){
   MSyntax syntax;
   MStatus ms;

   for(int iA=0; iA<nAttrs; iA++){
      ms = syntax.addFlag(shortAttrNames[iA], longAttrNames[iA], attrTypes[iA]);
      if(MFAIL(ms)){log_err("Could not create syntax.");}
   }

   syntax.enableQuery(false);
   syntax.enableEdit(false);

   return syntax;
}

MStatus StrandsCmd::FindScene(optional<MString> sceneName){
   MPxNode* customNode = nullptr;
   int i=0;
   MStatus ms;
   MObject tmpObj;
   MItDependencyNodes it(MFn::kPluginDependNode, &ms);
   if(!sceneName.has_value())
   MCHECKRET(ms);

   while(!it.isDone()){
      tmpObj = it.item();
      // Get userNode: the MPxNode for the MObject from the MFnDependencyNode
      customNode = MFnDependencyNode(tmpObj).userNode(&ms);
      // Check that it is a scene node, and if it is, increment count.
      if(!MFAIL(ms) && IS_SCENE_NODE(customNode)){
         mSceneObj = tmpObj;
         i++;
         // If @sceneName was provided, compare it.
         if(sceneName.has_value()){
            if(MFnDependencyNode(tmpObj).name()==sceneName.value()){
               mSceneObj = tmpObj;
               break;
            }
         }
         // If not provided, there better be just one @SceneNode object.
         else MCHECKERROR_B(i>1, "Too many scene nodes to choose from. Give a scene node name.");
      }
      it.next();
   }
   MCHECKERROR_B(i==0, "No scene node.");

   // i is 1 at this point.
   MFnDependencyNode sceneDN(mSceneObj, &ms);
   mSceneNode = dynamic_cast<SceneNode*>(sceneDN.userNode(&ms));
   MCHECKERROR_NOTNULL(mSceneNode, "SceneNode pointer cast failed.");
   
   // Log if scene found. 
   int v = 2; if(mOptCommand == kCmdGetStatus) v = 1;
   log_info(v, "Scene name: %s", DNName(sceneDN));
   return ms;
}

// @pointIndex returns a value
// FIXME Output argument should be first
MStatus StrandsCmd::addIndexToStrand(MObject strandShapeObj, int orderIndex, int &pointIndex){
   MStatus ms;
   MFnDependencyNode strandShapeDN(strandShapeObj);
   char command[500];
   int oldCurveSize, newCurveSize;

   // Get current size of the curve
   sprintf(command, "size (`getAttr %s.%s`)", DNName(strandShapeDN), kAttrStrandOrderLong);
   REQUIRED_CMD_RESULT(command, oldCurveSize);

   // Insert into the curve and get new size of the curve
   sprintf(command, "InsertIntoStrand \"%s %d\"", DNName(strandShapeDN), orderIndex);
   // Number of points after running the above command
   REQUIRED_CMD_RESULT(command, newCurveSize);

   log_verbose("Curve size changed from %d to %d", oldCurveSize, newCurveSize);
   // Check new curve size
   MCHECKERROR_B(newCurveSize!=(oldCurveSize + 1), "Could not add point to curve.");

   // Return new point index
   pointIndex = newCurveSize-1;
   log_verbose("Added point index %d at order index %d to %s",
               pointIndex, orderIndex, MFnDependencyNode(strandShapeObj).name().asChar());
   return MS::kSuccess;
}

// This function is used both for the input and output locator objects to be
// connected to the corresponding input and output strand objects.
// FIXME Output arguments should be first
MStatus StrandsCmd::addPointAndNode
(MObject strandShapeObj, int orderIndex, MObject& locObj, MPoint pos, double locScale, int &pointIndex){
   MStatus ms;
   MFnDependencyNode strandShapeDN(strandShapeObj);
   MCHECKRET(createLocatorAtPos(locObj, pos, locScale));
   MCHECKRET(addIndexToStrand(strandShapeObj, orderIndex, pointIndex));
   return MS::kSuccess;
}

MStatus StrandsCmd::addNewIndex(MObject strandObj, int index){
   MFnDependencyNode strandDN(strandObj);
   // Create extra index in strand nodes list
   char command[500];
   sprintf(command, "setAttr %s.%s[%d] -1",
           DNName(strandDN), kAttrStrandNodesShort, index);
   REQUIRED_CMD(command);;
   return MS::kSuccess;
}

MStatus StrandsCmd::createInOutObject(MObject& inObj, MObject &outObj, MTypeId typeId, MString &name){
   MFnDependencyNode DN;
   MStatus ms;
   MString names[] = {name, "shape"+name, "out"+name, "outShape"+name};
   auto iterNode = MItDependencyNodes();
   while(!iterNode.isDone()){
      for(uint i = 0; i<4; i++){
         if(MFnDependencyNode(iterNode.thisNode()).name() == names[i]){
            log_warn("Name %s already in use.", names[i].asChar());
            // return MS::kFailure;
         }
      }
      iterNode.next();
   }
   inObj = DN.create(typeId, names[1], &ms);
   cout<<MFnDependencyNode(inObj).name()<<endl;
   MObject transObj, shapeObj;
   getShape(shapeObj, inObj);
   getTransform(transObj, inObj);
   cout<<MFnDependencyNode(transObj).name()<<" "<<MFnDependencyNode(shapeObj).name()<<endl;
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   DN.setName(names[0], &ms);
   name = DN.name();
   cout<<MFnDependencyNode(transObj).name()<<" "<<MFnDependencyNode(shapeObj).name()<<endl;

   outObj = DN.create(typeId, names[3], &ms);
   getShape(shapeObj, outObj);
   getTransform(transObj, outObj);
   cout<<MFnDependencyNode(transObj).name()<<" "<<MFnDependencyNode(shapeObj).name()<<endl;
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   DN.setName(names[2], &ms);
   cout<<MFnDependencyNode(transObj).name()<<" "<<MFnDependencyNode(shapeObj).name()<<endl;
   return MS::kSuccess;
}

MStatus StrandsCmd::connectPointID(MObject strandShapeObj, MObject locObj, int pointIndex, bool disconnect){
   MStatus ms;
   char command[500];
   MFnDependencyNode strandDN(strandShapeObj), locatorDN(locObj);
   if(disconnect)
      sprintf(command, "disconnectAttr \"%s.%s\" %s.%s[%d]",
              DNName(locatorDN), kAttrPointIDShort,
              DNName(strandDN), kAttrStrandNodesShort, pointIndex);
   else
      sprintf(command, "connectAttr \"%s.%s\" %s.%s[%d]",
              DNName(locatorDN), kAttrPointIDShort,
              DNName(strandDN), kAttrStrandNodesShort, pointIndex);
   REQUIRED_CMD(command);
   return MS::kSuccess;
}

MStatus StrandsCmd::connectTranslate(MObject strandShapeObj, MObject locObj, int pointIndex, bool disconnect){
   MStatus ms;
   char command[500];
   MFnDependencyNode strandDN(strandShapeObj), locatorDN(locObj);
   if(disconnect) sprintf(command, "disconnectAttr \"%s.translate\" %s.controlPoints[%d];",
                          DNName(locatorDN), DNName(strandDN), pointIndex);
   else sprintf(command, "connectAttr \"%s.translate\" %s.controlPoints[%d];",
                DNName(locatorDN), DNName(strandDN), pointIndex);
   REQUIRED_CMD(command);
   return MS::kSuccess;
}


// Confirms whether the selected transforms are end points on strands
MStatus StrandsCmd::CheckEndPoints(MObject locTransformObjs[2]){
   // TODO Write this function
   return MS::kFailure;
}

// Merging the locators requires finding the plugs for 'translate', 'PointId'
// and other properties that may be connected. The outputs are then reconnected
// to the final merged node. Note that this does not treat it as a point, i.e.,
// no input/output is respected. The two locators are merged, and this can be
// used for both input and output nodes.
#define CHECK_MSTATUS_AND_SET_RET_MS(a, b) if(MFAIL((a))){  \
      if(retMS){*retMS = MS::kFailure;}                     \
      return (b);}
#define MCHECKRET_RNULLOBJ(a) CHECK_MSTATUS_AND_SET_RET_MS((a), MObject::kNullObj);
MObject StrandsCmd::mergeLocators(MObject locObjs[2], bool inputPoints, MObject *delObj, MStatus *retMS){
   // Get DependencyNodes
   MStatus ms, ms2;
   MFnDependencyNode locatorDN[2];
   locatorDN[0].setObject(locObjs[0]);
   locatorDN[1].setObject(locObjs[1]);

   MPlug translatePlug[2];
   translatePlug[0] = locatorDN[0].findPlug(kAttrTransLong, &ms);
   translatePlug[1] = locatorDN[1].findPlug(kAttrTransLong, &ms);
   MCHECKRET_RNULLOBJ(ms);

   // Find the output connections for plugs of the node to be deleted
   MPlugArray translateDests;
   MCHECKRET_RNULLOBJ(findOutputConnections(translatePlug[1], translateDests));

   // Use MDGModifier to replace the old node with the new node in all the
   // connection plugs picked above
   MDGModifier dgMod;
   for(uint ii=0;ii<translateDests.length();ii++){
      MCHECKRET_RNULLOBJ(dgMod.disconnect(translatePlug[1], translateDests[ii]));
      MCHECKRET_RNULLOBJ(dgMod.connect(translatePlug[0], translateDests[ii]));
   }

   if(inputPoints){
      MPlug pidPlug[2];
      pidPlug[0] = locatorDN[0].findPlug(kAttrPointIDShort, &ms);
      pidPlug[1] = locatorDN[1].findPlug(kAttrPointIDShort, &ms);
      MCHECKRET_RNULLOBJ(ms);
      MPlugArray pidDests;
      MCHECKRET_RNULLOBJ(findOutputConnections(pidPlug[1], pidDests));

      for(uint ii=0;ii<pidDests.length();ii++){
         MCHECKRET_RNULLOBJ(dgMod.disconnect(pidPlug[1], pidDests[ii]));
         MCHECKRET_RNULLOBJ(dgMod.connect(pidPlug[0], pidDests[ii]));
      }
   }

   MCHECKRET_RNULLOBJ(dgMod.doIt());

   if(retMS) *retMS = MS::kSuccess;
   if(delObj) *delObj = locObjs[1];
   return locObjs[0];
}

MStatus StrandsCmd::findStrandsAtPoint
(vector<MObject> &strandObjs, IVec &indices, MObject pointTransObj){
   MStatus ms;
   MFnDependencyNode pointTransDN(pointTransObj);
   MPlug pidPlug = pointTransDN.findPlug(kAttrPointIDLong, true, &ms);
   MCHECKERROR(ms, "Could not get pointID Plug");

   // Get output connections' plug array and fill strandObjs
   MPlugArray plugArr;
   MCHECKRET(findOutputConnections(pidPlug, plugArr));
   for(uint ii=0; ii<plugArr.length(); ii++){
      strandObjs.push_back(plugArr[ii].node());
      indices.push_back(plugArr[ii].logicalIndex());
   }

   return MS::kSuccess;
}

// Return the common object in two vectors of MObjects.
MStatus StrandsCmd::findCommonStrand
(MObject &strandShapeObj, MObject &strandTransObj, int index[2], // Outputs
 // Inputs
 vector<MObject> strandObjs1, vector<int> indices1,
 vector<MObject> strandObjs2, vector<int> indices2){
   for(int ii=0; ii<strandObjs1.size(); ii++){
      for(int jj=0; jj<strandObjs2.size(); jj++){
         if (strandObjs1[ii] == strandObjs2[jj]){
            MObject transObj;
            getTransform(transObj, strandObjs1[ii]);
            if(isStrandObj(transObj)){
               strandShapeObj = strandObjs1[ii];
               strandTransObj = transObj;
               index[0] = indices1[ii];
               index[1] = indices2[jj];
               return MS::kSuccess;
            }
         }
      }
   }
   return MS::kFailure;
}

MStatus StrandsCmd::findStrandPosition
(MObject locObjs[2], MObject &strandShapeObj, MObject& strandTransObj, int index[2], MPoint& position){
   MFnDependencyNode loc1DN(locObjs[0]);
   MFnDependencyNode loc2DN(locObjs[1]);

   vector<MObject> strands1, strands2;
   vector<int> indices1, indices2;

   // Find strand object to use, and the indices of the points.
   MCHECKRET(findStrandsAtPoint(strands1, indices1, locObjs[0]));
   MCHECKRET(findStrandsAtPoint(strands2, indices2, locObjs[1]));
   MCHECKRET(findCommonStrand(strandShapeObj, strandTransObj, index,
                              strands1, indices1,
                              strands2, indices2));
   MPoint p1, p2;
   MCHECKRET(getTranslate(p1, locObjs[0]));
   MCHECKRET(getTranslate(p2, locObjs[1]));
   position = (p1 + p2)/2;

   MFnDependencyNode strandShapeDN(strandShapeObj);
   log_verbose("Adding node to strand shape %s", DNName(strandShapeDN));
   MPxNode* node = strandShapeDN.userNode();
   StrandMaya *strandNode = dynamic_cast<StrandMaya*>(node);
   MCHECKERROR_B(!strandNode, "StrandMaya pointer could not be found.");

   // Go from indices in storage order to indices in drawing order
   index[0] = strandNode->getOrderIndex(index[0]);
   index[1] = strandNode->getOrderIndex(index[1]);
   log_verbose("Indices: %d, %d", index[0], index[1]);
   return MS::kSuccess;
}

MStatus StrandsCmd::fixPointWorld(MObject &constObj, MObject pointObj, double locScale){
   MFnDependencyNode pointDN(pointObj);
   // First try to add a constraint attribute and see if it is already
   // constrained
   MObject constMsgAttrObj;
   MCHECKRET(addConstAttr(constMsgAttrObj, pointObj));
   MCHECKRET(createConstraint(constObj, pointObj, MObject::kNullObj,
                              kCTFixedConstraint, locScale));
   return MS::kSuccess;
}

MStatus deleteConstraintMeshObjs(MString constName){
   MStringArray result;
   REQUIRED_CMD_RESULT("connectionInfo -dfs " + constName + ".xformMatrix", result);
   if(result.length() == 0) return MS::kFailure;
   MString meshCreatorName = GET_NODE(result[0]);
   REQUIRED_CMD_RESULT("connectionInfo -dfs " + meshCreatorName + ".os", result);
   for(uint i=0;i<result.length();i++){
      log_verbose("Looking to delete %s",GET_NODE(result[i]).asChar());
      REQUIRED_CMD("delete "+GET_NODE(result[i]));
   }
   log_verbose("Looking to delete %s", meshCreatorName.asChar());
   int exists;
   REQUIRED_CMD_RESULT("objExists "+meshCreatorName, exists);
   if(exists) REQUIRED_CMD("delete "+meshCreatorName);
   return MS::kSuccess;
}

MStatus StrandsCmd::deleteConstraint(MObject constObj){
   MFnDependencyNode constDN(constObj);
   MStatus ms;
   constDN.setLocked(false);
   MObject dynObj, meshCreatorObj, meshObj;
   bool fieldExists = false;
   MCHECKRET(findMessageSource(dynObj, kAttrConstDynObjLong, constObj, &fieldExists));
   MCHECKERROR_B(!fieldExists, "No dynamic object field found");
   deleteConstraintMeshObjs(constDN.name());
   MFnDependencyNode dynObjDN(dynObj);
   REQUIRED_CMD("deleteAttr " + dynObjDN.name() + "." + kAttrConstraintLong);
   REQUIRED_CMD("delete " + constDN.name());
   return MS::kSuccess;
}

MStatus StrandsCmd::checkPointOrphaned(MObject pointObj){
   MStatus ms;
   MCHECKERROR_B(pointObj.isNull(), "Null point object");

   MFnDependencyNode pointDN(pointObj);
   MPlug plug;
   plug = pointDN.findPlug(kAttrPointIDLong, &ms); MCHECKRET(ms);
   bool connected = plug.isConnected(&ms)        ; MCHECKRET(ms);
   if(!connected) return MS::kSuccess;
   return MS::kFailure;
}

// Delete point only if orphaned
MStatus StrandsCmd::deletePoint(MObject pointObj){
   MStatus ms;
   MObject constObj;
   bool exists = false;
   // Confirm that the point is not still in use
   MCHECKERROR(checkPointOrphaned(pointObj), "Cannot delete a point in use.");

   // Check for any constraint and delete if found.
   ms = findConstraint(constObj, pointObj, &exists);
   if(!MFAIL(ms)){
      // Found constraint
      MCHECKRET(deleteConstraint(constObj));
   } else if(exists){
      log_warn("Constraint field, but no constraint. Deleting object anyway.");
   }
   // Delete the object and it's output
   MCHECKRET(deleteObjectAndOutput(pointObj));
   return MS::kSuccess;
}

MStatus StrandsCmd::deleteSceneObject(MObject obj){
   MStatus ms;
   MFnDependencyNode objDN(obj);
   MObject shapeObj;
   // MDGModifier dgMod;
   if(isObjType(obj, ObjType::CONST)){
      MCHECKRET(deleteConstraint(obj));
   } else if(isObjType(obj, ObjType::POINT)){
      MCHECKRET(deletePoint(obj));
   } else if(isObjType(obj, ObjType::STRND)){
      MCHECKRET(getShape(shapeObj, obj));
      objDN.setObject(shapeObj);
      MPlug nodesPlug = objDN.findPlug(kAttrStrandNodesLong, &ms);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
      vector<MObject> pointObjs;
      MObject tObj;
      for(uint ii=0;ii<nodesPlug.numElements();ii++){
         MCHECKCONT(findPlugIndexSourceNode(tObj, nodesPlug, ii),
                    "Failed to find source point for strand %s - node at %d. Deleting anyway.",
                    MFnDependencyNode(obj).name().asChar(), ii);
         pointObjs.push_back(tObj);
      }
      MCHECKRET(deleteObjectAndOutput(obj));
      // deletePoint done after deleting strand to make sure it is indeed orphaned
      for(MObject pointObj: pointObjs) MCHECKWARN(deletePoint(pointObj), "Failed to delete point");
   } else if(isObjType(obj, ObjType::JOINT)){
      MObject outJointObj;
      findOutput(outJointObj, obj);
      MObject debugTransObjA, debugTransObjB;
      MCHECKRET(findMessageDestination(debugTransObjA, kAttrJointDebugALong, outJointObj));
      MCHECKRET(findMessageDestination(debugTransObjB, kAttrJointDebugBLong, outJointObj));
      MFnDependencyNode DN;
      DN.setObject(debugTransObjA);
      MCHECKWARN(runMelCommand("delete " + DN.name()), "Not able to delete debug object A");
      DN.setObject(debugTransObjB);
      MCHECKWARN(runMelCommand("delete " + DN.name()), "Not able to delete debug object A");
      DN.setObject(obj);
      REQUIRED_CMD("delete " + DN.name());
      DN.setObject(outJointObj);
      REQUIRED_CMD("delete " + DN.name());
      // MCHECKWARN(dgMod.deleteNode(debugTransObjA), "Not able to delete debug object A");
      // MCHECKWARN(dgMod.deleteNode(debugTransObjB), "Not able to delete debug object B");
      // MCHECKERROR(dgMod.deleteNode(obj), "Not able to delete joint object");
      // MCHECKERROR(dgMod.deleteNode(outJointObj), "Not able to delete output object");
      // return dgMod.doIt();
   } else MCHECKRET(deleteObjectAndOutput(obj));
   return MS::kSuccess;
}

MStatus StrandsCmd::deleteObjectAndOutput(MObject obj){
   MObject outObj, inObj;
   MFnDependencyNode DN;
   MDGModifier DGM;
   MStatus msIn, msOut, ms;
   msIn  = findOutput(outObj, obj);
   msOut = findInput(inObj, obj);

   // if(msIn == MS::kSuccess){
   //    log_verbose("Input object found.");
   //    MCHECKERROR(DGM.deleteNode(obj), "Could not set object for deletion");
   //    ms = DGM.doIt();
   //    CHECK_MSTATUS_AND_RETURN_IT(ms);
   //    MCHECKERROR(DGM.deleteNode(outObj), "Could not set output of object for deletion");
   //    ms = DGM.doIt();
   //    CHECK_MSTATUS_AND_RETURN_IT(ms);
   // } else if(msOut == MS::kSuccess){
   //    log_warn("This is an output. Deleting input");
   //    MCHECKERROR(DGM.deleteNode(obj), "Could not set object for deletion");
   //    ms = DGM.doIt();
   //    CHECK_MSTATUS_AND_RETURN_IT(ms);
   //    MCHECKERROR(DGM.deleteNode(inObj), "Could not set input of object for deletion");
   //    ms = DGM.doIt();
   //    CHECK_MSTATUS_AND_RETURN_IT(ms);
   // } else{
   //    log_err("Orphan object. Neither input, nor output. Not deleting anything from scene.");
   // }

   if(!MFAIL(msIn)){
      DN.setObject(outObj);
      REQUIRED_CMD("delete " + DN.name());
   }
   if(!MFAIL(msOut)){
      DN.setObject(inObj);
      REQUIRED_CMD("delete " + DN.name());
   }
   DN.setObject(obj);
   REQUIRED_CMD("delete " + DN.name());
   return ms;
}

// MStatus StrandsCmd::Display(MString filter, int state){
//    MStatus ms;
//    return MS::kSuccess;
// }

// Implements the command after the arguments have been processed
MStatus StrandsCmd::execCommand(){
   MStatus ms = MS::kFailure;
   if(mOptCommand == kCmdAddStrand){
      ms = AddStrand(mOptName, mOptStrandLength, mOptLocScale);
   } else if(mOptCommand == kCmdAddNode){
      ms = AddNode(mOptLocScale);
   } else if(mOptCommand == kCmdAssignToRigid){
      ms = AssignToRigid(mOptLocScale);
   } else if(mOptCommand == kCmdSimulate){
      ms = Simulate(mOptStabilize, mOptMatrixOutput, mOptDataOutDir,
                    mOptModString, mOptOffString   , mOptActString,
                    mOptLabel);
   } else if(mOptCommand == kCmdContinue){
      ms = Continue(mOptSimNumber, mOptActString);
   } else if(mOptCommand == kCmdDefineState){
      ms = DefineState(mOptSimNumber, mOptFmtString);
   } else if(mOptCommand == kCmdGetState){
      ms = GetState(mOptSimNumber);
   } else if(mOptCommand == kCmdCancelSim){
      ms = CancelSim(mOptSimNumber);
   } else if(mOptCommand == kCmdDeleteOutput){
      ms = DeleteOutput(mOptSimNumber);
   } else if(mOptCommand == kCmdSelectOutput){
      ms = SelectOutput(mOptSimNumber);
   } else if(mOptCommand == kCmdGetStatus){
      ms = GetStatus(mOptSimNumber);
   } else if(mOptCommand == kCmdGetDebugInfo){
      ms = GetDebugInfo(mOptSimNumber);
   } else if(mOptCommand == kCmdExportScene){
      ms = ExportScene(mOptLabel, mOptExportFile);
   } else if(mOptCommand == kCmdAddRigid){
      ms = AddRigid();
   } else if(mOptCommand == kCmdAddJoint){
      ms = AddJoint(mOptName, mOptLocScale);
   } else if(mOptCommand == kCmdJoinStrands){
      ms = JoinStrands(mOptName);
   } else if(mOptCommand == kCmdDeleteStrandNode){
      ms = DeleteStrandNode();
   } else if(mOptCommand == kCmdAddForce){
      ms = AddForce(mOptName); // TODO Add locator scale
   } else if(mOptCommand == kCmdAttachForce){
      ms = AttachForce();
   } else if(mOptCommand == kCmdClearScene){
      ms = ClearScene();
   } else if(mOptCommand == kCmdConstrainOnAxis){
      ms = ConstrainOnAxis(mOptLocScale);
   // } else if(mOptCommand == kCmdDisplay){
   //    ms = Display(mOptFilter, mOptState);
   } else if(mOptCommand == kCmdDeleteObject){
      ms = DeleteObject();
   } else if(mOptCommand == kCmdToggleEnable){
      ms = ToggleEnableStrand();
   } else if(mOptCommand == kCmdRelocateNode){
      ms = RelocateNode();
   } else if(mOptCommand == kCmdSplitJunction){
      ms = SplitJunction();
   } else if(mOptCommand == kCmdMergeStrands){
      ms = MergeStrands();
   } else if(mOptCommand == kCmdSplitStrand){
      ms = SplitStrand();
   } else {
      log_err("Command not recognized: %s", mOptCommand.asChar());
   }
   return ms;
}

#define NO_ARG_FAIL(short, long){                                 \
      log_err("No valid (-%s/--%s) argument. Argument necessary", \
              (short), (long));                                   \
      return MS::kFailure;}

#define NO_ARG(short, long) log_err("No valid (-%s/--%s) argument. Argument necessary", (short), (long));

// Only parsing options to set working variables
MStatus StrandsCmd::parseArguments(MArgParser &_argData){
   MStatus tempMS = MS::kFailure;
   mOptSceneName = "";
   mOptCommand = "";
   mOptName = "";
   mOptSteps = -1;
   mOptTime  = -0.1;
   mOptStabilize = false;
   mOptMatrixOutput = false;
   mOptDataOutDir = "";
   mOptLocScale = .1;
   mOptStrandLength = 1;
   mOptSimNumber = -1;
   mOptActString = "";
   bOptActStringSet = false;
   mOptModString = "";
   bOptModStringSet = false;
   mOptOffString = "";
   bOptOffStringSet = false;
   mOptLabel = "";
   mSceneNode = nullptr;
   mSceneObj = MObject::kNullObj;
   STRVEC simCmds = {kCmdGetDebugInfo, kCmdDeleteOutput, kCmdCancelSim,
                     kCmdContinue    , kCmdSelectOutput, kCmdGetStatus,
                     kCmdGetState    , kCmdDefineState};
   STRVEC stepCmds = {kCmdContinue, kCmdSimulate};

   // Command flag needs to exist. Fail if not found
   if(_argData.isFlagSet(kCmdFlagLong))
      tempMS = _argData.getFlagArgument(kCmdFlagLong, 0, mOptCommand);
   if(MFAIL(tempMS)){NO_ARG_FAIL(kCmdFlag, kCmdFlagLong);}

   // Scene name argument may be missing. Can still find the scene.
   if(mOptCommand!=kCmdClearScene){
      if(_argData.isFlagSet(kSceneNameFlagLong)){
         tempMS = _argData.getFlagArgument(kSceneNameFlagLong, 0, mOptSceneName);
         MCHECKERROR(tempMS, "Could not get flag argument for scene name.");
         tempMS = FindScene(mOptSceneName);
         MCHECKERROR(tempMS, "Scene name not set and just one scene not found.");
         log_verbose("Scene name: %s", mOptSceneName.asChar());
      } else{
         tempMS = FindScene({});
         MCHECKERROR(tempMS, "Could not find scene.");
      }}

   // Name flag may be necessary.
   if(_argData.isFlagSet(kNameFlagLong))
      tempMS = _argData.getFlagArgument(kNameFlagLong, 0, mOptName);
   if((mOptCommand==kCmdAddJoint || mOptCommand==kCmdAddStrand) && MFAIL(tempMS))
      NO_ARG(kNameFlag, kNameFlagLong);

   if(mOptCommand==kCmdAddJoint && _argData.isFlagSet(kJtFlagLong)){
      tempMS = _argData.getFlagArgument(kJtFlagLong, 0, mOptJointType);
   }
   if(_argData.isFlagSet(kStrandLengthFlagLong)){
      tempMS = _argData.getFlagArgument(kStrandLengthFlagLong, 0, mOptStrandLength);
      MCHECKWARN(tempMS, "Strand length: %f", mOptStrandLength);
   }
   if(_argData.isFlagSet(kLocScaleFlagLong)){
      tempMS = _argData.getFlagArgument(kLocScaleFlagLong, 0, mOptLocScale);
      MCHECKWARN(tempMS, "Could not get value");
   }

   if(mOptCommand==kCmdSimulate){
      if(_argData.isFlagSet(kStabilizeFlagLong)){
         tempMS = _argData.getFlagArgument(kStabilizeFlagLong, false, mOptStabilize);
         if(mOptStabilize) log_reg("Stabilizing simulation.");
      } else log_warn("No stabilize found");
      if(_argData.isFlagSet(kModStringFlagLong)){
         tempMS = _argData.getFlagArgument(kModStringFlagLong, 0, mOptModString);
         log_warn("Running with modification: %s", mOptModString.asChar());
         bOptModStringSet = true;
      }
      if(_argData.isFlagSet(kOffStringFlagLong)){
         tempMS = _argData.getFlagArgument(kOffStringFlagLong, 0, mOptOffString);
         log_warn("Running with offsets: %s", mOptOffString.asChar());
         bOptOffStringSet = true;
      }
      if(_argData.isFlagSet(kLabelFlagLong)){
         tempMS = _argData.getFlagArgument(kLabelFlagLong, 0, mOptLabel);
         log_warn("Running with label: %s", mOptLabel.asChar());
      }
      if(_argData.isFlagSet(kMatrixOutputFlagLong)){
         tempMS = _argData.getFlagArgument(kMatrixOutputFlagLong, false, mOptMatrixOutput);
         MCHECKRET(tempMS);
         if(mOptMatrixOutput){log_reg("Matrix output enabled");}
         else log_reg("Matrix output not enabled");
      }
      if(_argData.isFlagSet(kDataOutDirFlagLong)){
         tempMS = _argData.getFlagArgument(kDataOutDirFlagLong, 0, mOptDataOutDir);
         log_reg("DataOutDir set to: %s", mOptDataOutDir.asChar());
      }}

   // Check whether simulation command
   string cmd = mOptCommand.asChar();
   if(find(simCmds.begin(), simCmds.end(), cmd) != simCmds.end()){
      if(_argData.isFlagSet(kSimNumberFlagLong)){
         tempMS = _argData.getFlagArgument(kSimNumberFlagLong, 0, mOptSimNumber);
         MCHECKWARN(tempMS, "Could not get sim number argument");
      } else{
         log_reg("No sim number provided. Applying defaults.");
         // Delete or cancel all simulations by default
         mOptSimNumber = -1;
         // Select first simulation output by default
         if(mOptCommand.asChar() == kCmdSelectOutput) mOptSimNumber = 0;
      }
      log_verbose("Sim number: %d", mOptSimNumber);
   }

   if(cmd==kCmdExportScene){
      if(_argData.isFlagSet(kExportFileFlagLong)){
         tempMS = _argData.getFlagArgument(kExportFileFlagLong, 0, mOptExportFile);
         MCHECKWARN(tempMS, "Could not get export file.");
      } else{
         log_err("Did not set file to export to.");
      }}

   if(_argData.isFlagSet(kFmtStringFlagLong)){
      if(cmd!=kCmdDefineState){
         log_warn("Spurious " kFmtStringFlagLong " flag.");
      } else{
         tempMS = _argData.getFlagArgument(kFmtStringFlagLong, 0, mOptFmtString);
         MCHECKWARN(tempMS, "Could not get fmt string.");
      }}

   if(find(stepCmds.begin(), stepCmds.end(), cmd) != stepCmds.end()){
      if(_argData.isFlagSet(kStepsFlagLong)){
         tempMS = _argData.getFlagArgument(kStepsFlagLong, 0, mOptSteps);
      } else if(_argData.isFlagSet(kTimeFlagLong)){
         tempMS = _argData.getFlagArgument(kTimeFlagLong, 0, mOptTime);
      } else{
         NO_ARG(kStepsFlag    , kStepsFlagLong);
         NO_ARG(kTimeFlagShort, kTimeFlagLong);
      }
      if(_argData.isFlagSet(kActStringFlagLong)){
         tempMS = _argData.getFlagArgument(kActStringFlagLong, 0, mOptActString);
         log_warn("Running with activation: %s", mOptActString.asChar());
         bOptActStringSet = true;
      }}
   return MS::kSuccess;
}

MStatus StrandsCmd::doIt(const MArgList& _argList){
   MStatus ms;
   MArgParser argData(syntax(), _argList, &ms);
   MCHECKERROR(parseArguments(argData), "Could not find all arguments.");
   MCHECKERROR_B(!argData.isFlagSet(kCmdFlagLong), "No command argument provided.");

   MCHECKERROR(execCommand(), "Failed to execute command");
   // GetStatus command is executed in parallel and interferes with logging.

   int v = 2;
   if(mOptCommand == kCmdGetStatus) v = 1;
   log_info(v, "Command successful.");
   return MS::kSuccess;
}

bool StrandsCmd::isUndoable() const{log_random("Checking whether StrandsCmd is undoable"); return false;}

MStatus initializePlugin(MObject obj) {
   MFnPlugin plugin(obj, "sachdevp", "0.01");
   MCHECKRET(plugin.registerCommand(kPluginCmdName, StrandsCmd::creator, StrandsCmd::createSyntax));
   return MS::kSuccess;
}

MStatus uninitializePlugin(MObject obj) {
   MFnPlugin plugin(obj);
   MCHECKRET(plugin.deregisterCommand("StrandsCmd"));
   return MS::kSuccess;
}
