#include <StrandsCmd/SimParams.hpp>
#include <functional>
#include <optional>
#include <Scene/Scene.hpp>
#include <MayaHelper.hpp>

using namespace std;
SimParams::~SimParams(){rec.reset();}

SimParams::SimParams
(SP<Recorder> _rec, Strands::Scene *_scene, SimOutput* _simOut, MObject _sceneObj,
 int _nSteps, int _nFrames, double _dt, int _dtx, MString _labelStr):
   mSimOut(_simOut), mScene(_scene), mLabelStr(_labelStr), nFrames(_nFrames){
   rec        = _rec;
   toCancel   = false;
   nSteps     = _nSteps;
   mModString = "";
   sceneObj   = _sceneObj;
   released   = false;
   dt         = _dt;
   dtx        = _dtx;

   bActKeyframesSet = false;
   bActOverrideSet  = false;
   bModSet = false;
   bOffSet = false;

   simTime = DUR<double>::zero();
   recTime = DUR<double>::zero();

   rec->clear();
   mSimOut->frames.clear();
}

STRMAP SimParams::getDebugInfo(){
   STRMAP result;

   std::string dataOutDir, debugFile;
   mScene->getDataOutDir(dataOutDir);
   mScene->getDebugFile(debugFile);
   if(mScene->isDebug())  result["debugFile"]  = debugFile;
   if(mScene->isMatOut()) result["dataOutDir"] = dataOutDir;
   result["dt"]    = to_string(dt);
   result["dtx"]   = to_string(dtx);
   result["mod"]   = mModString;
   result["label"] = mLabelStr.asChar();
   return result;
};

RET SimParams::setOffMap(OffMap _offMap){
   bOffSet = true;
   mOffMap = _offMap;
   return R::Success;
}

RET SimParams::setModString(string _modString){
   bModSet = true;
   mModString = _modString;
   return R::Success;
}

RET SimParams::setActMap(ActMap _actMap){
   RCHECKERROR_B(bActKeyframesSet, "Activation keyframes already set. Map should not be set.");
   bActOverrideSet = true;
   mActMap = _actMap;
   updateInputs();
   return R::Success;
}

RET SimParams::setActKeyframes(ActKeyframes _actKeyframes){
   RCHECKERROR_B(bActOverrideSet, "Activation map already set. Keyframes should not be set.");
   bActKeyframesSet = true;
   mActKeyframes = _actKeyframes;
   return R::Success;
}

RET SimParams::updateInputs(int iFrame){
   if(iFrame==-1 && bActOverrideSet){
      for(auto actPair: mActMap){
         cout<<actPair.first<<" "<<actPair.second<<endl;
         RCHECKERROR(mScene->setStrandAct(actPair),
                     "Could not set activation for %s.", actPair.first.c_str());
      }}
   else if(bActKeyframesSet){
      // Find if the frame is in activation keyframe keys
      auto framePair = mActKeyframes.find(iFrame);
      // Apply if found
      if(framePair!=mActKeyframes.end()){
         for(auto actPair: framePair->second){
            log_warn("%d, %s, %3.2f", framePair->first, actPair.first.c_str(), actPair.second);
            RCHECKERROR(mScene->setStrandAct(actPair),
                        "Could not set activation for %s.", actPair.first.c_str());
         }}}
   return R::Success;
}

RET SimParams::stepFrames(int nFrames){
   TIME<HRC> start, end;
   rec->addFrames(nFrames);
   setRunning();
   for(int iFrame=0; iFrame<nFrames; iFrame++){
      start = HRC::now();
      if(toBeCanceled()){
         setCanceled();
         return R::Failure;
      }

      RCHECKERROR(updateInputs(iFrame), "Could not update inputs.");
      // Run for @dtx short steps
      auto r = rec->stepScene(mScene, dtx);
      end = HRC::now();
      simTime += end-start;

      // Record a frame
      start = HRC::now();
      SimFrame* simFrame = rec->recordFrame(mSimOut->indMap, mScene);
      mSimOut->appendFrame(simFrame);
      mSimOut->postRecord();
      end = HRC::now();
      recTime += end-start;

      // Check for simulation error
      if(RFAIL(r)){
         setError();
         rec->setFinished();
         return R::Failure;
      }}
   mSimOut->test();
   rec->setFinished();
   setCompleted();
   cout<<"Completed running frames."<<endl;
   return R::Success;
}

optional<string> SimParams::getState(){
   if(!bFmtSet){
      log_err("State format not yet set.");
      return {};
   }

   std::string retState;
   for(auto fmtTuple: mFmtTuples){
      auto attrVal = getAttr(fmtTuple);
      if(!attrVal.has_value()) return {};
      retState = retState + " " + attrVal.value();
   }

   // Remove one character at the start.
   if(!retState.empty()) return retState.substr(1);
   return retState;
}

optional<pair<ObjType, int>> SimParams::findObj(string objName){
   auto it = mSimOut->mObjMap.find(objName);
   if(it == mSimOut->mObjMap.end()) return {};
   return it->second;
}

bool SimParams::isAttr(ObjType objType, std::string attrName){
   StrVec attrs = this->objTypeAttrsMap.at(objType);
   if(find(attrs.begin(), attrs.end(), attrName)!=attrs.end()) return true;
   return false;
}

// Vector version of attribute names
const StrVec SimParams::vPointAttrs  = {"pos", "vel"};
const StrVec SimParams::vRigidAttrs  = {"pos", "vel", "ang"};
const StrVec SimParams::vStrandAttrs = {"taut", "strain"};
const StrVec SimParams::vJointAttrs =  {"pos", "vel", "ang", "exceeded"};
const StrVec SimParams::vIndicatorAttrs =  {"pos", "vel"};
const map<ObjType, StrVec> SimParams::objTypeAttrsMap = SimParams::createMap();

MStatus SimParams::appendFmt(AttrTuple attrTuple){
   MCHECKERROR_B(bFmtSet, "Appending format after setting complete.");
   bFmtSet = false;
   mFmtTuples.push_back(attrTuple);
   return MS::kSuccess;
}

MStatus SimParams::checkAppendFmt(std::string objName, std::string attrName){
   auto objPair = findObj(objName);
   MCHECKERROR(!objPair.has_value(), "Object name does not exist.");
   auto objType = objPair.value().first;
   auto tid = objPair.value().second;
   MCHECKERROR(!isAttr(objType, attrName), "Attr %s does not exist for object %s",
               attrName.c_str(), objName.c_str());
   appendFmt(make_tuple(objType, tid, attrName));
   return MS::kSuccess;
}

optional<string> SimParams::getAttr(AttrTuple attrTuple){
   ObjType objType;
   int tid;
   string attrName;
   tie(objType, tid, attrName) = attrTuple;

   SP<SimFrame> lastFrame;
   if(mSimOut->frames.size()>0) lastFrame = mSimOut->frames.back();
   else{return {};}

   // cout<<static_cast<int>(objType)<<" "<<tid<<" "<<attrName<<endl;

   if(objType == ObjType::POINT) return lastFrame->vPOut[tid].getAttr(attrName);
   if(objType == ObjType::RIGID) return lastFrame->vROut[tid].getAttr(attrName);
   if(objType == ObjType::JOINT) return lastFrame->vJOut[tid].getAttr(attrName); 
   if(objType == ObjType::STRND) return lastFrame->vSOut[tid].getAttr(attrName);
   if(objType == ObjType::INDIC) return lastFrame->vIOut[tid].getAttr(attrName);
   return {};
}

RET SimParams::setFramesAndSteps(int _nFrames, int _nSteps){
   nFrames = _nFrames;
   nSteps = _nSteps;
   return R::Success;
};
