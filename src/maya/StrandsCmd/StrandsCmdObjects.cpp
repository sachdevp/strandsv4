#include "StrandsCmd.hpp"
#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPlug.h>
#include <maya/MItSelectionList.h>
#include <maya/MDagPath.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>

using namespace std;
using namespace Eigen;

int getObjectEnumVal(MObject obj, MStatus *ms){
   if(ms == nullptr) ms = new MStatus();
   MFnDependencyNode DN(obj);
   MPlug typePlug = DN.findPlug(kAttrObjectTypeLong , ms);
   int plugValue = -1;
   *ms = typePlug.getValue(plugValue);
   return plugValue;
}

MStatus getRawTransObj(MObject &obj, MSelectionList sList, MFn::Type shapeType){
   MStatus ms;
   MDagPath tmpDagP;
   MItSelectionList iter(sList, MFn::kInvalid, &ms);
   MObject tmpObj;
   int nObjects=0;
   obj = MObject::kNullObj;
   while(!iter.isDone()){
      iter.getDagPath(tmpDagP);
      MCHECKERROR(tmpDagP.extendToShapeDirectlyBelow(0), "Could not find shape below this node.");
      if(tmpDagP.hasFn(shapeType)){
         if(nObjects == 0){
            obj = tmpDagP.transform(&ms);
            MCHECKERROR(ms, "Could not get transform.");
         }
         nObjects++;
      }
      iter.next();
   }
   if(nObjects == 1) return MS::kSuccess;
   return MS::kFailure;
}

MStatus getObject(MObject &obj, MSelectionList sList, ObjType objType){
   MStatus ms;
   MDagPath tmpDagP;
   MItSelectionList iter(sList, MFn::kInvalid, &ms);
   MObject tmpObj;
   int nObjects=0;
   obj = MObject::kNullObj;
   while(!iter.isDone()){
      MCHECKRET(iter.getDagPath(tmpDagP));
      MCHECKRET(tmpDagP.extendToShapeDirectlyBelow(0));
      tmpObj = tmpDagP.transform(&ms);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
      if(isObjType(tmpObj, objType) == MS::kSuccess){
         MFnDependencyNode DN(tmpObj);
         if(nObjects == 0) obj = tmpDagP.transform(&ms);
         nObjects++;
      }
      iter.next();
   }
   if(nObjects == 1) return MS::kSuccess;
   return MS::kFailure;
}

MStatus getObjects
(vector<MObject> &vObjects, MSelectionList sList, int nObjects, ObjType objType){
   MStatus ms;
   MDagPath tmpDagP;
   MItSelectionList iter(sList, MFn::kInvalid, &ms);
   MObject tmpObj;
   vObjects.clear();

   while(!iter.isDone()){
      iter.getDagPath(tmpDagP);
      MCHECKRET(tmpDagP.extendToShapeDirectlyBelow(0));
      tmpObj = tmpDagP.transform();
      if(isObjType(tmpObj, objType) == MS::kSuccess) vObjects.push_back(tmpObj);
      iter.next();
   }
   if(nObjects == vObjects.size()) return MS::kSuccess;
   return MS::kFailure;
}

MStatus StrandsCmd::getSelectedRigidObjects
(uint &nRigids, MObject rigidTransform[2], MSelectionList sList){
   MStatus ms;
   MObject tmpObj;
   MItSelectionList it(sList);
   vector<MObject> vRigidObjs;

   MCHECKERROR_B((sList.length() < 1) || (sList.length() > 2),
                 "Select one or two objects to place the joint.");

   ms = getRigidObjs(vRigidObjs, sList, sList.length());
   nRigids = (uint)vRigidObjs.size();

   // Check if non rigid objects were selected or whether there's another failure.
   MCHECKERROR_B(nRigids>0 && nRigids!=sList.length(), "Not all selected objects are rigids.");
   MCHECKERROR(ms, "Failed to get rigid objects.");

   rigidTransform[0] = vRigidObjs[0];
   if(nRigids == 2) rigidTransform[1] = vRigidObjs[1];
   else rigidTransform[1] = MObject::kNullObj;
   return ms;
}

MStatus StrandsCmd::getSelectedPoints(MObject locators[2], MSelectionList sList){
   MStatus ms;
   vector<MObject> vPointObjs;
   CHECK_SEL_LENGTH(sList, 2);
   ms = getPointObjs(vPointObjs, sList, 2);
   if(!MFAIL(ms)){
      assert(vPointObjs.size()==2);
      locators[0] = vPointObjs[0];
      locators[1] = vPointObjs[1];
      return MS::kSuccess;
   } else{
      log_err("Incorrect number of selections.");
      return MS::kFailure;
   }
}

MStatus StrandsCmd::addRigidShapeAttr(MObject meshObj){
   MFnDependencyNode meshDN(meshObj);
   MFnNumericAttribute nAttr;
   MObject MIAttr;

   // Mass attribute
   MCHECKRET(ADD_DN_DBLATTR(meshDN, kAttrMassLong, kAttrMassShort, 1.0));

   // Moment of inertia
   MIAttr = nAttr.createPoint(kAttrMILong, kAttrMIShort);
   nAttr.setDefault(1.0f, 1.0f, 1.0f);
   MCHECKRET(meshDN.addAttribute(MIAttr, MLOCAL));

   // Denisty
   MCHECKRET(ADD_DN_DBLATTR(meshDN, kAttrDensityLong, kAttrDensityShort, DEFAULT_DENSITY));

   // Should mass be computed ;default: compute mass from density
   MCHECKRET(ADD_DN_BOOLATTR(meshDN, kAttrCompMassLong, kAttrCompMassShort, true));

   return MS::kSuccess;
}

MStatus StrandsCmd::addRigidTransformAttr(MObject transObj){
   MFnDependencyNode transDN(transObj);
   MFnNumericAttribute nAttr;
   MObject outAttr;

   // ID Attrs
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrObjectIDLong, kAttrObjectIDShort, -1));
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrRigidIDLong,  kAttrRigidIDShort,  -1));

   // Scripted velocity
   MCHECKRET(ADD_DN_BOOLATTR(transDN, kAttrScriptedLong, kAttrScriptedShort, false));
   MObject vscAttrObj = nAttr.createPoint(kAttrScriptedVelocityLong, kAttrScriptedVelocityShort);
   MCHECKRET(transDN.addAttribute(vscAttrObj, MLOCAL));

   // Debug attribute
   MCHECKRET(ADD_DN_BOOLATTR(transDN, kAttrDebugLong, kAttrDebugShort, false));

   return MS::kSuccess;
}

MStatus StrandsCmd::addOutJointTransformAttr(MObject transObj){
   MFnDependencyNode transDN(transObj);
   MStatus ms;
   MObject cAttrObj, outAttr;
   ADD_DN_POINTATTR(transDN, kAttrPosVelLong, kAttrPosVelShort, MPoint(0,0,0));
   ADD_DN_POINTATTR(transDN, kAttrAngVelLong, kAttrAngVelShort, MPoint(0,0,0));
   MObject cAttrObj1 = intAttr(kAttrJLimActiveLong "X", kAttrJLimActiveShort "X", 0);
   MObject cAttrObj2 = intAttr(kAttrJLimActiveLong "Y", kAttrJLimActiveShort "Y", 0);
   MObject cAttrObj3 = intAttr(kAttrJLimActiveLong "Z", kAttrJLimActiveShort "Z", 0);

   MFnCompoundAttribute cAttr;
   outAttr = cAttr.create(kAttrJLimActiveLong, kAttrJLimActiveShort, &ms);
   MCHECKRET(cAttr.addChild(cAttrObj1));
   MCHECKRET(cAttr.addChild(cAttrObj2));
   MCHECKRET(cAttr.addChild(cAttrObj3));
   MCHECKERROR(ms, "Couldn't create compound attribute.");
   MCHECKRET(transDN.addAttribute(outAttr, MLOCAL));

   cAttrObj = cAttr.create(kAttrJConstForceLong, kAttrJConstForceShort, &ms);
   vector<string> XYZ = {"X", "Y", "Z"};
   vector<string> RT  = {"R", "T"};
   for(auto xyz: XYZ){
      for(auto rt: RT){
         MObject childAttrObj = dblAttr((kAttrJConstForceLong  + rt+xyz).c_str(),
                                        (kAttrJConstForceShort + rt+xyz).c_str(), 0.0);
         MCHECKRET(cAttr.addChild(childAttrObj));
      }
   }
   MCHECKERROR(ms, "Couldn't add compound attribute.");
   MCHECKRET(transDN.addAttribute(cAttrObj, MLOCAL));

   // ADD_DN_POINTATTR(transDN, kAttrJConstForceLong, kAttrJConstForceShort, MPoint(0,0,0));
   // MAKE_NUMERIC_ATTR(mConstActiveAttr, "ConstraintActive", "cfb", MFnNumericData::kBoolean, 0, false, false, true);

   return MS::kSuccess;
}

MStatus StrandsCmd::addJointTransformAttr(MObject transObj){
   MFnDependencyNode transDN(transObj);
   MObject outAttr;

   /** Attributes **/
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrObjectIDLong, kAttrObjectIDShort, -1));
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrJointIDLong, kAttrJointIDShort, -1));

   // Standard attributes for scene and type
   MCHECKRET(ADD_DN_BOOLATTR(transDN, kAttrDebugLong, kAttrDebugShort, false));

   // Add parent and child of joint for easy access
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrJointParentIDLong, kAttrJointParentIDShort, -1));
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrJointChildIDLong, kAttrJointChildIDShort, -1));
   MCHECKRET(ADD_DN_DBLATTR(transDN, kAttrJointRadLong, kAttrJointRadShort, 1));
   MCHECKRET(ADD_DN_DBLATTR(transDN, kAttrJNeutralAngLong, kAttrJNeutralAngShort, 0.0));
   MCHECKRET(ADD_DN_DBLATTR(transDN, kAttrJStiffnessLong,  kAttrJStiffnessShort,  0.0));
   MCHECKRET(ADD_DN_DBLATTR(transDN, kAttrJDampingLong,  kAttrJDampingShort,  0.0));
   
   MObject checkBoxObj, lowerObj, upperObj;
   MCHECKRET(addJointLimitsAttrs(checkBoxObj, lowerObj, upperObj, transObj));
   return MS::kSuccess;
}

// Properties for input locator object defining it as part of the scene. However
// the properties will be filled only later by the SceneNode
// @index is the index where the point will attach to the strand object
// NOTE Do NOT call for output locators
// Follow with connectAttr for controlPoints
MStatus StrandsCmd::addPointAttrs(MObject locatorObj){
   MFnDependencyNode locatorDN(locatorObj);
   MObject outAttr;

   MCHECKRET(ADD_DN_INTATTR(locatorDN, kAttrObjectIDLong, kAttrObjectIDShort, -1));
   MCHECKRET(ADD_DN_INTATTR(locatorDN, kAttrPointIDLong, kAttrPointIDShort, -1));
   MCHECKRET(ADD_DN_BOOLATTR(locatorDN, kAttrDebugLong, kAttrDebugShort, false));

   return MS::kSuccess;
}

MStatus StrandsCmd::addOutPointAttrs(MObject locatorObj){
   MFnDependencyNode locatorDN(locatorObj);
   MObject outAttr;
   ADD_DN_POINTATTR2(locatorDN, kAttrPosVelLong, kAttrPosVelShort);
   ADD_DN_POINTATTR2(locatorDN, kAttrAngVelLong, kAttrAngVelShort);
   return MS::kSuccess;
}

MStatus StrandsCmd::addStrandShapeAttr(MObject shapeObj){
   MFnDependencyNode shapeDN(shapeObj);
   MFnEnumAttribute eAttr;
   MCHECKRET(ADD_DN_BOOLATTR(shapeDN, kAttrInextensibleLong, kAttrInextensibleShort, false));
   MObject matAttr = eAttr.create(kAttrMaterialLong, kAttrMaterialShort, 0);
   MCHECKRET(shapeDN.addAttribute(matAttr, MLOCAL));

   eAttr.addField(kMatSpring      , (short)MatType::SPRING);
   eAttr.addField(kMatMuscleLinear, (short)MatType::MUSCLELINEAR);
   eAttr.addField(kMatMuscleHill  , (short)MatType::MUSCLEHILL);
   eAttr.setHidden(false);

   MCHECKRET(ADD_DN_DBLATTR(shapeDN, kAttrRadiusLong    , kAttrRadiusShort    , 0.1f));
   MCHECKRET(ADD_DN_DBLATTR(shapeDN, kAttrDensityLong   , kAttrDensityShort   , 1.0f));
   MCHECKRET(ADD_DN_DBLATTR(shapeDN, kAttrStiffnessLong , kAttrStiffnessShort , 1e5f));
   MCHECKRET(ADD_DN_DBLATTR(shapeDN, kAttrActParamLong  , kAttrActParamShort  , 0.5f));
   MCHECKRET(ADD_DN_DBLATTR(shapeDN, kAttrActivationLong, kAttrActivationShort, 0.0f));
   MCHECKRET(ADD_DN_DBLATTR(shapeDN, kAttrStrainLong    , kAttrStrainShort    , 0.0f));

   REQUIRED_CMD("addAttr -at long -m -sn " kAttrStrandNodesShort " -ln " kAttrStrandNodesLong
                " -dv -1 " + shapeDN.name());

   return MS::kSuccess;
}

MStatus StrandsCmd::addStrandTransAttr(MObject transObj){
   MFnDependencyNode transDN(transObj);
   // Standard scene and type attributes
   MCHECKRET(ADD_DN_BOOLATTR(transDN, kAttrEnabledLong, kAttrEnabledShort, true));
   MCHECKRET(ADD_DN_BOOLATTR(transDN, kAttrDebugLong, kAttrDebugShort, false));
   MCHECKRET(ADD_DN_BOOLATTR(transDN, kAttrSimpleStrandLong, kAttrSimpleStrandShort, false));

   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrObjectIDLong, kAttrObjectIDShort, -1));
   MCHECKRET(ADD_DN_INTATTR(transDN, kAttrStrandIDLong, kAttrStrandIDShort, -1));

   return MS::kSuccess;
}

MStatus StrandsCmd::addOutStrandTransAttr(MObject outTransObj){
   MStatus ms;
   MFnDependencyNode DN(outTransObj);
   // TODO This is throwing seg fault. No idea why? :(
   MCHECKRET(ADD_DN_BOOLATTR(DN, kAttrSInexActiveLong, kAttrSInexActiveShort, false));
   MCHECKRET(ADD_DN_DBLATTR(DN, kAttrSConstForceLong, kAttrSConstForceShort, 0.0));
   MCHECKRET(ADD_DN_DBLATTR(DN, kAttrStrainLong    , kAttrStrainShort    , 0.0f));
   return ms;
}

MStatus StrandsCmd::addOutStrandShapeAttr(MObject outShapeObj){
   MStatus ms;
   // MFnNumericAttribute nAttr;
   // MFnDependencyNode outShapeDN(outShapeObj);
   // MObject outStrain = nAttr.create(kAttrStrainLong, kAttrStrainShort,
   //                                  MFnNumericData::kDouble, 0.0, &ms );
   // nAttr.setKeyable(true); nAttr.setWritable(true);
   // MCHECKRET(outShapeDN.addAttribute(outStrain,MLOCAL));

   return ms;
}

MStatus StrandsCmd::addJointLimitsAttrs
(MObject &checkBoxAttr, MObject &lowerAttr, MObject &upperAttr, MObject inObj){
   MFnEnumAttribute eAttr;
   MFnNumericAttribute nAttr;
   MStatus ms;
   checkBoxAttr = eAttr.create(kAttrLimitTypeLong, kAttrLimitTypeShort, 0, &ms);
   MFnDependencyNode DN(inObj);
   eAttr.addField("Free" , (short)JointLimitType::FREE);
   eAttr.addField("Upper", (short)JointLimitType::UPPER);
   eAttr.addField("Lower", (short)JointLimitType::LOWER);
   eAttr.addField("Range", (short)JointLimitType::RANGE);
   eAttr.addField("Fixed", (short)JointLimitType::FIXED);
   eAttr.setHidden(false);
   MCHECKRET(DN.addAttribute(checkBoxAttr));

   MPlug checkBoxPlug(inObj, checkBoxAttr);
   checkBoxPlug.setValue((short)JointLimitType::FREE);

   // TODO Lock attributes when changing type of joint.
   lowerAttr = nAttr.createPoint(kAttrLowerLong, kAttrLowerShort, &ms);
   nAttr.setDefault(-1.0e5, -1.0e5, -1.0e5);
   upperAttr = nAttr.createPoint(kAttrUpperLong, kAttrUpperShort, &ms);
   nAttr.setDefault(1.0e5, 1.0e5, 1.0e5);

   MCHECKRET(DN.addAttribute(lowerAttr));
   MCHECKRET(DN.addAttribute(upperAttr));

   return MS::kSuccess;
}

MStatus StrandsCmd::addJointDebugObjs
(MObject jointTransObj, MObject outJointTransObj, MString name){
   MStatus ms;
   MFnDependencyNode outJointDN(outJointTransObj), debugJointDN;
   MFnNumericAttribute nAttr;
   MObject debugJointShapeObjA, debugJointTransObjA;
   MObject debugJointShapeObjB, debugJointTransObjB;

   MObject driftAngObj = nAttr.createPoint(kAttrDriftAngLong, kAttrDriftAngShort, &ms);
   MObject driftPosObj = nAttr.createPoint(kAttrDriftPosLong, kAttrDriftPosShort, &ms);
   MCHECKRET(outJointDN.addAttribute(driftAngObj, MLOCAL));
   MCHECKRET(outJointDN.addAttribute(driftPosObj, MLOCAL));

   // Create locators for debugging joints too
   MString debugJointNameA = "debug" + name+"A";
   MString debugJointNameB = "debug" + name+"B";
   // Visual debugging aids
   MCHECKRET(createLocator(debugJointTransObjA, debugJointShapeObjA,
                           (ftype)mOptLocScale, &debugJointNameA));
   MCHECKRET(createLocator(debugJointTransObjB, debugJointShapeObjB,
	   (ftype)mOptLocScale, &debugJointNameB));

   MStringArray attrNames;
   attrNames.setLength(4);
   attrNames[0] = kAttrJointDebugALong;
   attrNames[1] = kAttrOutJointLong;
   attrNames[2] = kAttrJointDebugAShort;
   attrNames[3] = kAttrOutJointShort;

   MCHECKRET(connectObjects(outJointTransObj, debugJointTransObjA, attrNames));

   attrNames[0] = kAttrJointDebugBLong;
   attrNames[2] = kAttrJointDebugBShort;
   MCHECKRET(connectObjects(outJointTransObj, debugJointTransObjB, attrNames));

   // Set visibility for these objects
   vector<pair<MObject, int>> objects;
   objects.push_back(std::make_pair(debugJointTransObjA, 0));
   objects.push_back(std::make_pair(debugJointTransObjB, 0));
   setVisibility(objects, 1);
   objects.push_back(std::make_pair(debugJointTransObjA, 1));
   objects.push_back(std::make_pair(debugJointTransObjB, 1));
   setVisibility(objects, 2);

   // Get output joint shape
   MObject outJointShapeObj;
   MCHECKRET(getShape(outJointShapeObj, outJointTransObj));
   outJointDN.setObject(outJointShapeObj);

   // Connect scale attributes
   debugJointDN.setObject(debugJointShapeObjA);
   REQUIRED_CMD("connectAttr " + outJointDN.name() + ".localScale " + debugJointDN.name() + ".localScale");
   debugJointDN.setObject(debugJointShapeObjB);
   REQUIRED_CMD("connectAttr " + outJointDN.name() + ".localScale " + debugJointDN.name() + ".localScale");

   return MS::kSuccess;
}

