#pragma once

#include <chrono>
#include <map>

// Maya includes
#include <maya/MArgList.h>
#include <maya/MObject.h>
#include <maya/MGlobal.h>
#include <maya/MPxCommand.h>
#include <maya/MArgParser.h>
#include <maya/MSyntax.h>
#include <maya/MPoint.h>
#include <maya/MSelectionList.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MThreadAsync.h>

// Strands lib
#include <DynObj/Strand.hpp>
#include <Scene/Scene.hpp>

// Maya plugin
#include <SceneNode/SceneNode.hpp>
#include <Recorder/SimOutput.hpp>
#include <MayaHelper.hpp>

// Defined in SimParams.hpp
class SimParams;

class StrandsCmd: public MPxCommand {
public:
   StrandsCmd() {};

   /** Commands **/
   MStatus AddStrand(MString name, double length, double locScale = 0.2);
   MStatus AddNode(double locScale = 0.2);
   MStatus AddRigid();
   MStatus AddJoint(MString name, double locScale = 0.2);
   MStatus JoinStrands(MString junctionName);
   MStatus SplitJunction();
   MStatus AddForce(MString name, double locScale = 0.2);
   MStatus AttachForce();
   MStatus ClearScene();
   MStatus DeleteObject();
   MStatus DeleteStrandNode();
   MStatus Display(MString filter, int state);
   MStatus AssignToRigid(double locScale);
   MStatus ToggleEnableStrand();
   MStatus RelocateNode();
   MStatus SplitStrand();
   MStatus UnConstrainNode();
   MStatus CancelSim(int simNumber);
   MStatus SelectOutput(int simNumber);
   MStatus DeleteOutput(int iSim);
   MStatus getSplitStrandsPoints(std::array<std::vector<MObject>, 2>& vStrandsPointObjs,
                                 std::vector<MObject> strandPointObjs, MObject splitPointObj);
   // Creates Strands scene and runs it.
   // @modString Allows overriding current scene parameters
   // @matrixOut Create matrix output file?
   // @stabilize Simulation to be stabilized?
   // @offString Offset for any points
   // @actString Overriding activation values for muscles
   // @label     Label to be set for simulation result
   MStatus createStrand(MObject& strandObj, std::vector<MObject> pointObjs, MString name);
   MStatus getStrandPoints(std::vector<MObject> &strandPointObjs, MObject inStrandShapeObj, bool disconnect);
   static MStatus createInOutObject (MObject& inObj, MObject &outObj, MTypeId typeId, MString &name);
   MStatus MergeStrands();
   MStatus Simulate
   (bool stabilize, bool matrixOut, MString dataOutDir, MString modString,
    MString offString, MString actString, MString label);
   // NOTE Currently not checking whether simulation parameters were changed before calling to continue.
   MStatus Continue(int iSim, MString _actString);
   MStatus ReleaseSim(int iSim);
   MStatus GetStatus(int iSim);
   MStatus GetDebugInfo(int iSim);
   MStatus GetState(int iSim);
   MStatus DefineState(int iSim, MString fmt);
   MStatus ExportScene(MString labelStr, MString fileNameStr);

   MStatus execCommand();
   MStatus parseArguments(MArgParser &_argData);
   virtual MStatus doIt(const MArgList& argList);
   // Need to implement the following functions to allow undoing commands
   virtual bool isUndoable() const;
   // MStatus undoIt();
   // MStatus redoIt();
   static MSyntax createSyntax();

   static void* creator();

private:
   static const size_t nAttrs = 18;
   static std::array<const char*, nAttrs> shortAttrNames;
   static std::array<const char*, nAttrs> longAttrNames;
   static std::array<MSyntax::MArgType, nAttrs> attrTypes;
   MStatus disconnectPlugs(std::vector<MPlug> &pointPlugs, MPlug &ptPlug, MObject strandShapeObj, int index);
   MStatus findStrandsAtPoint(std::vector<MObject> &strandObjs, IVec &indices, MObject pointTransObj);
   MStatus findCommonStrand(MObject &strandShapeObj, MObject &stradTransObj, int index[2],
                            std::vector<MObject> strandObjs1, IVec indices1,
                            std::vector<MObject> strandObjs2, IVec indices2);
   MStatus findStrandPosition(MObject locators[2], MObject& strandShapeObj,
                              MObject& strandTransObj, int index[2], MPoint& position);
   MStatus createConstraint(MObject &constObj, const MObject targetObj,
                            const MObject srcObj, MString type, double locScale);
   MStatus createApiMesh(MObject &apiMeshObj, MObject constTransObj, MObject cComputeObj);
   // MStatus createApiMesh(MObject &apiMeshObj, MString constTransName, MString cComputeName);
   // Create a new strand, with connections from pointObjs provided as input.
   // @pointObjs size should be at least 2
   // @pointObjs are already added to strand and have all the required attributes
   // Hence, all points also expected to have outputs
   MStatus addNewStrand(MObject &strandObj, std::vector<MObject> pointObjs);
   MStatus createConstObj(MObject &constTransObj, MObject &constObj, MString srcName,
                          MString tName, MString type, double locScale);
   MStatus addConstTypeAttr(MObject &constObj);
   MStatus addConstAttr(MObject &constMsgAttr, MObject obj);
   // TODO Replace this by a map
   int convertConstType(MString type);
   // Find SceneNode by checking all custom nodes.
   MStatus FindScene(std::optional<MString>);
   // Inefficient way of finding scene with ls. Should be removed.

   // Add an index to the strand at @orderIndex.
   // TODO Extend this function to handle multiple insertions at the same point
   MStatus addIndexToStrand(MObject strandShapeObj, int orderIndex, int &pointIndex);
   // Adds a locator and then adds a node to the StrandMaya object
   MStatus addPointAndNode(MObject strandShapeObj, int orderIndex, MObject &locObj,
                           MPoint pos, double locScale, int &pointIndex);
   // Add properties to the locator to identify the point. Use only for input objects
   MStatus addNewIndex(MObject strandObj, int index);
   MStatus addPointAttrs(MObject locObj);
   MStatus addOutPointAttrs(MObject locObj);
   MStatus connectTranslate
   (MObject strandObj, MObject locObj, int pointIndex, bool disconnect = false);
   MStatus connectPointID
   (MObject strandObj, MObject locObj, int pointIndex, bool disconnect = false);
   // Merge the two locator objects provided cleanly while respecting connections
   MObject mergeLocators(MObject locObj[2], bool inputPoints,
                         MObject *delObj = nullptr, MStatus *ms = nullptr);
   MStatus ConstrainOnAxis(double locScale);


   /** Simulation helpers **/
   MStatus getStepSize(double &lStep, MFnDependencyNode &sceneDN);
   // Allows multi-threaded execution of simulation
   static MThreadRetVal stepAndRecord(void *data);
   // Stop execution and release the simulation thread.
   bool isValid(int iSim);
   static void ThreadCB(void* data);
   MStatus getSteps(int &nFrames, int &nSteps, ftype dt, ftype dtx);

   /** Working variables **/
   MObject    mSceneObj;
   SceneNode* mSceneNode;

   MString    mOptCommand;
   // MString    mOptFilter;
   MString    mOptJointType;
   double     mOptLocScale;
   MString    mOptName;
   MString    mOptSceneName;
   bool       mOptStabilize;
   bool       mOptMatrixOutput;
   MString    mOptDataOutDir;
   // int        mOptState;
   int        mOptSteps;
   double     mOptTime;
   int        mOptSimNumber;
   double     mOptStrandLength;
   MString    mOptModString;
   MString    mOptOffString;
   MString    mOptActString;
   MString    mOptFmtString;
   MString    mOptExportFile;

   bool bOptModStringSet;
   bool bOptOffStringSet;
   bool bOptActStringSet;
   MString    mOptLabel;

   bool bActOverrideSet;

   /** Helper functions **/
   MStatus   CheckEndPoints(MObject locTransObjs[2]);
   MStatus   addRigidShapeAttr(MObject meshObj);
   MStatus   addRigidTransformAttr(MObject transObj);
   MStatus   addJointTransformAttr(MObject transObj);
   MStatus   addOutJointTransformAttr(MObject transObj);
   MStatus   addJointLimitsAttrs(MObject &checkBoxAttr, MObject &lowerAttr, MObject &upperAttr, MObject inObj);
   MStatus   addStrandShapeAttr(MObject shapeObj);
   MStatus   addStrandTransAttr(MObject transObj);
   MStatus   addOutStrandTransAttr(MObject outTransObj);
   MStatus   addOutStrandShapeAttr(MObject outShapeObj);
   MStatus   checkPointOrphaned(MObject pointObj);
   MStatus   deleteConstraint(MObject constObj);
   MStatus   deleteObjectAndOutput(MObject obj);
   MStatus   deletePoint(MObject pointObj);
   MStatus   deleteSceneObject(MObject obj);
   MStatus   fixPointWorld(MObject &constObj, MObject pointObj, double locScale);
   // Selection functions
   MStatus getSelectedRigidObjects
   (uint &nObjects, MObject rigidTrans[2], MSelectionList sList);
   MStatus getSelectedPoints(MObject locators[2], MSelectionList sList);
   MStatus addJointDebugObjs
   (MObject jointTransObj, MObject outJointTransObj, MString name);
};

int getObjectEnumVal(MObject obj, MStatus *ms = nullptr);
MStatus getObjects(std::vector<MObject> &vObjects, MSelectionList sList,
                   int nObjects, ObjType objType);
MStatus getObject(MObject &obj, MSelectionList sList,
                  ObjType objType);
MStatus getRawTransObj(MObject &obj, MSelectionList sList, MFn::Type shapeType);

inline MStatus isObjType(MObject obj, ObjType objType){
   MStatus ms;
   int enumVal = getObjectEnumVal(obj, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   if(enumVal == (int)objType){return MS::kSuccess;}
   return MS::kFailure;
}

inline MStatus getLocatorObj(MObject &obj, MSelectionList sList)
{return getRawTransObj(obj, sList, MFn::kLocator);}

inline MStatus getMeshObj(MObject &obj, MSelectionList sList)
{return getRawTransObj(obj, sList, MFn::kMesh);}

inline MStatus getPluginObj(MObject &obj, MSelectionList sList, MTypeId typeId){
   MStatus ms;
   MObject shapeObj;
   MCHECKRET(getRawTransObj(obj, sList, MFn::kPluginShape));
   MCHECKRET(getShape(shapeObj, obj));
   MPxNode* node = MFnDependencyNode(shapeObj).userNode(&ms);
   MCHECKERROR(ms, "Could not get userNode.");
   MCHECKERROR_B(!node, "Null node pointer.");
   if(node->typeId() == typeId) return MS::kSuccess;
   return MS::kFailure;
}

inline MStatus getStrandObj(MObject &obj, MSelectionList sList)
{return getPluginObj(obj, sList, STRAND_NODE_ID);}

inline MStatus isRigidObj(MObject obj)
{return isObjType(obj, ObjType::RIGID);}

inline MStatus isStrandObj(MObject obj)
{return isObjType(obj, ObjType::STRND);}

inline MStatus isConstraintObj(MObject obj)
{return isObjType(obj, ObjType::CONST);}

inline MStatus getRigidObjs
(std::vector<MObject> &vRigidObjs, MSelectionList sList, int nRigids)
{return getObjects(vRigidObjs, sList, nRigids, ObjType::RIGID);}

inline MStatus getPointObjs
(std::vector<MObject> &vPointObjs, MSelectionList sList, int nPoints)
{return getObjects(vPointObjs, sList, nPoints, ObjType::POINT);}

inline MStatus getForceObjs
(std::vector<MObject> &vForceObjs, MSelectionList sList, int nForces)
{return getObjects(vForceObjs, sList, nForces, ObjType::FORCE);}

inline MStatus getPointObj(MObject &pointObj, MSelectionList sList)
{return getObject(pointObj, sList, ObjType::POINT);}

inline MStatus getForceObj(MObject &forceObj, MSelectionList sList)
{return getObject(forceObj, sList, ObjType::FORCE);}

inline MStatus getRigidObj(MObject &rigidObj, MSelectionList sList)
{return getObject(rigidObj, sList, ObjType::RIGID);}
