#include <maya/MFnDependencyNode.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MObject.h>
#include <maya/MDGModifier.h>

// Project includes
#include <ConstraintNode/ConstraintHelper.hpp>
#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <StrandsCmd/StrandsCmd.hpp>

MStatus StrandsCmd::addConstTypeAttr(MObject &constShapeObj){
   MFnDependencyNode constDN(constShapeObj);
   MFnEnumAttribute eAttr;
   MObject constTypeAttr;
   MCHECKRET(createConstTypeAttr(constTypeAttr));
   MCHECKRET(constDN.addAttribute(constTypeAttr));
   return MS::kSuccess;
}

int StrandsCmd::convertConstType(MString type){
   int typeInt;
   if(type == kCTFixedConstraint)    typeInt = kCTFixedConstraintInt;
   if(type == kCTParentConstraint)   typeInt = kCTParentConstraintInt;
   if(type == kCTLineConstraint)     typeInt = kCTLineConstraintInt;
   if(type == kCTCircleConstraint)   typeInt = kCTCircleConstraintInt;
   if(type == kCTCylinderConstraint) typeInt = kCTCylinderConstraintInt;
   return typeInt;
}

MStatus StrandsCmd::
createApiMesh(MObject &apiMeshObj, MObject constTransObj, MObject cComputeObj){
   MStatus ms;
   MObject apiMeshTransObj;
   MFnDependencyNode aMOTransDN, apiMeshDN,
      constTransDN(constTransObj), cComputeDN(cComputeObj);
   MFnNumericAttribute nAttr;
   // Create API mesh shape
   apiMeshObj = apiMeshDN.create("apiMesh", &ms); CHECK_MSTATUS_AND_RETURN_IT(ms);

   // Get transform object and DN
   MCHECKRET(getTransform(apiMeshTransObj, apiMeshObj)); aMOTransDN.setObject(apiMeshTransObj);

   // Connect appropriately
   MCHECKRET(ADD_DN_BOOLATTR(constTransDN, kAttrConstSurfVisLong, kAttrConstSurfVisShort, false));
   MDGModifier dgm;
   MCHECKRET(dgm.connect(constTransDN.findPlug(kAttrTransLong), aMOTransDN.findPlug(kAttrTransLong)));
   MCHECKRET(dgm.connect(constTransDN.findPlug(kAttrRotLong), aMOTransDN.findPlug(kAttrRotLong)));
   MCHECKRET(dgm.connect(constTransDN.findPlug(kAttrConstSurfVisLong), apiMeshDN.findPlug("visibility")));
   REQUIRED_CMD("connectAttr " + cComputeDN.name() +".os " + apiMeshDN.name()+".is");
   MCHECKRET(dgm.doIt());
   return MS::kSuccess;
}

// Create constraint object
MStatus StrandsCmd::createConstObj
(MObject &constTransObj, MObject &constShapeObj, MString srcName,
 MString tName, MString type, double locatorScale){
   MStatus ms;
   MString constShapeName;
   MStringArray result;
   // TODO Fixed constraint should use parent constraint if possible. Not sure how.
   if(type == kCTFixedConstraint || type == kCTLineConstraint){
      MCHECKRET(createLocator(constTransObj, constShapeObj, locatorScale));
   } else{
      // All constraints dependent on rigid bodies are defined by parentConstraint
      REQUIRED_CMD_RESULT("parentConstraint -mo \"" + srcName + "\" \"" + tName + "\"", result);
      constShapeName = result[0];
      getObjectByName(constShapeName, constTransObj);
      getShape(constShapeObj, constTransObj);
      REQUIRED_CMD("setAttr " + constShapeName + ".inheritsTransform 0");
   }

   // ID Attrs
   MFnDependencyNode constTransDN(constTransObj);
   MCHECKRET(ADD_DN_INTATTR(constTransDN, kAttrObjectIDLong, kAttrObjectIDShort, -1));
   MCHECKRET(ADD_DN_INTATTR(constTransDN, kAttrConstraintIDLong, kAttrConstraintIDShort, -1));

   return MS::kSuccess;
}

// FIXME Remove REQUIRED_CMD commands
MStatus StrandsCmd::createConstraint(MObject &constTransObj, const MObject targetObj,
                                     const MObject srcObj, MString type, double locatorScale){
   MStatus ms = MS::kFailure;
   MFnDependencyNode constTransDN, targetDN(targetObj), srcDN(srcObj), constShapeDN, cComputeDN;
   MFnMessageAttribute mAttr;
   MObject constShapeObj, outConstShapeObj, outConstTransObj;
   MStringArray result;
   MDGModifier dgm;
   MString tName = targetDN.name(), srcName = srcDN.name(),
      constShapeName, constTransName,outConstShapeName, outConstTransName,
      cComputeName, apiMeshName, outApiMeshName;

   // Get names for const
   MCHECKERROR(createConstObj(constShapeObj, constTransObj, srcName, tName, type, locatorScale),
               "Could not create constraint object.");
   constShapeDN.setObject(constShapeObj); constTransDN.setObject(constTransObj);
   constShapeName = constShapeDN.name(); constTransName  = constTransDN.name();

   // Create output for constraint
   MCHECKRET(createLocator(outConstTransObj, outConstShapeObj));
   // Get names for for the output constraints
   MFnDependencyNode outConstTransDN(outConstTransObj), outConstShapeDN(outConstShapeObj);
   outConstTransName = outConstTransDN.name(); outConstShapeName = outConstShapeDN.name();

   // Connect them and add constraint type
   MCHECKRET(connectObjects(constTransObj, outConstTransObj));
   MCHECKRET(addConstTypeAttr(constShapeObj));
   REQUIRED_CMD("setAttr " + constShapeName + "." kAttrConstTypeLong " " + convertConstType(type));

   // Constraint's target dynamic object as message attribute
   MObject dynObjAttr = mAttr.create(kAttrConstDynObjLong, kAttrConstDynObjShort, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MCHECKRET(constShapeDN.addAttribute(dynObjAttr, MLOCAL));
   MCHECKRET(dgm.connect(targetDN.findPlug(kAttrConstraintLong),
                         constShapeDN.findPlug(kAttrConstDynObjLong)));
   REQUIRED_CMD("copyAttr -v -at t \"" + tName + "\" \"" + constShapeName + "\"");

   MObject apiMeshObj, outApiMeshObj, cComputeObj;

   // Get compute node
   cComputeObj  = cComputeDN.create("apiMeshCreator", &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   // Connect constraint and compute
   MCHECKRET(dgm.connect(constShapeDN.findPlug(kAttrConstTypeLong), cComputeDN.findPlug(kAttrConstTypeLong)));
   MCHECKRET(dgm.connect(targetDN.findPlug(kAttrTransLong), cComputeDN.findPlug("tpos")));
   MCHECKRET(dgm.connect(constTransDN.findPlug("xformMatrix"), cComputeDN.findPlug("cxform")));
   MCHECKRET(dgm.doIt());

   // Mesh drawing nodes
   MCHECKRET(createApiMesh (   apiMeshObj,    constTransObj, cComputeObj));
   MCHECKRET(createApiMesh (outApiMeshObj, outConstTransObj, cComputeObj));
   MCHECKRET(connectObjects(   apiMeshObj, outApiMeshObj));

   return MS::kSuccess;
}

MStatus StrandsCmd::addConstAttr(MObject &constMsgAttr, MObject obj){
   MStatus ms;
   MFnDependencyNode DN(obj);
   MFnMessageAttribute mAttr;
   constMsgAttr= mAttr.create(kAttrConstraintLong, kAttrConstraintShort);
   MCHECKRET(mAttr.setDisconnectBehavior(MFnAttribute::DisconnectBehavior::kDelete));
   MCHECKRET(DN.addAttribute(constMsgAttr, MLOCAL));
   if(MFAIL(ms)){
      MPlug plug, inPlug = DN.findPlug(kAttrConstraintLong);
      MCHECKWARN(findDestinationPlug(plug, inPlug), "Constraint field without any output.");
      // If destination plug was found, must have constrainted already.
      log_err("Already constrained. Remove existing constraint on the object first");
      return MS::kFailure;
   }
   return MS::kSuccess;
}

