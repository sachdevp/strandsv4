/** @auth: Prashant Sachdeva (sachdevp@cs.ubc.ca)
    @desc: Strings used for attributes etc. File created to prevent errors in
    strings used for setting/getting attribute values
 **/
/** Syntax details **/
#pragma once
#define kPluginCmdName        "StrandsCmd"
#define kSceneNameFlagLong    "scene"
#define kSceneNameFlag        "sc"
#define kCmdFlagLong          "command"
#define kCmdFlag              "cmd"

#define kStepsFlagLong        "steps"
#define kStepsFlag            "st"

#define kTimeFlagLong         "time"
#define kTimeFlagShort        "tm"

#define kNameFlagLong         "name"
#define kNameFlag             "nm"

#define kJtFlagLong           "jointType"
#define kJtFlag               "jt"

#define kFilterFlagLong       "filter"
#define kFilterFlag           "ft"

#define kGetStateFlagLong     "getState"
#define kGetStateFlag         "gst"
#define kDefineStateFlagLong  "defState"
#define kDefineStateFlag      "dst"

#define kStabilizeFlagLong    "stabilize"
#define kStabilizeFlag        "stb"
#define kMatrixOutputFlagLong "matrixOutput"
#define kMatrixOutputFlag     "mo"
#define kDataOutDirFlagLong   "dataOutDir"
#define kDataOutDirFlag       "od"

#define kStrandLengthFlagLong "strandLength"
#define kStrandLengthFlag     "sl"

#define kLocScaleFlagLong     "locatorScale"
#define kLocScaleFlag         "ls"

#define kSimNumberFlagLong    "simNumber"
#define kSimNumberFlag        "sim"

#define kModStringFlagLong    "modString"
#define kModStringFlag        "mod"

#define kOffStringFlagLong    "offsetString"
#define kOffStringFlag        "off"

#define kActStringFlagLong    "actString"
#define kActStringFlag        "act"

#define kLabelFlagLong        "label"
#define kLabelFlagShort       "lbl"

#define kFmtStringFlagLong    "format"
#define kFmtStringFlagShort   "fmt"

#define kExportFileFlagLong   "exportFile"
#define kExportFileFlagShort  "exp"

/** Commands to be implemented by StrandsCmd **/

// Adds a strand and returns string array of transform names as result (strand,
// locator1, locator2)
/** Simulation related flags **/
#define kCmdSimulate        "Simulate"
#define kCmdContinue        "Continue"
#define kCmdDefineState     "DefineState"
#define kCmdGetState        "GetState"
#define kCmdCancelSim       "CancelSim"
#define kCmdSelectOutput    "SelectOutput"
#define kCmdDeleteOutput    "DeleteOutput"
#define kCmdGetStatus       "GetStatus"
#define kCmdGetDebugInfo    "GetDebugInfo"
#define kCmdExportScene     "ExportScene"

/** Modeling flags **/
#define kCmdAddStrand       "AddStrand"
#define kCmdAddRigid        "AddRigid"
#define kCmdAddNode         "AddNode"
#define kCmdAssignToRigid   "AssignToRigid"
#define kCmdAddJoint        "AddJoint"
#define kCmdJoinStrands     "JoinStrands"
#define kCmdDeleteStrandNode "DeleteStrandNode"
#define kCmdSplitJunction   "SplitJunction"
#define kCmdMergeStrands    "MergeStrands"
#define kCmdAddForce        "AddForce"
#define kCmdAttachForce     "AttachForce"
#define kCmdClearScene      "ClearScene"
#define kCmdConstrainOnAxis "ConstrainOnAxis"
// #define kCmdDisplay         "Display"
#define kCmdDeleteObject    "DeleteObject"
#define kCmdToggleEnable    "ToggleEnable"
#define kCmdRelocateNode    "RelocateNode"
#define kCmdSplitStrand     "SplitStrand"

/** Attribute short names **/
#define kAttrOutputLong      "OutputObject"
#define kAttrOutputShort     "outObj"
#define kAttrInputLong       "InputObject"
#define kAttrInputShort      "inObj"
#define kAttrObjectTypeLong  "ObjectType"
#define kAttrObjectTypeShort "objType"

#define kAttrConstraintLong        "Constraint"
#define kAttrConstraintShort       "constraint"

#define kAttrPointIDLong           "PointID"
#define kAttrPointIDShort          "pid"
#define kAttrObjectIDLong          "ObjectID"
#define kAttrObjectIDShort         "oid"
#define kAttrRigidIDLong           "RigidID"
#define kAttrRigidIDShort          "rid"
#define kAttrJointIDLong           "JointID"
#define kAttrJointIDShort          "jid"
#define kAttrForceIDLong           "ForceID"
#define kAttrForceIDShort          "fid"
#define kAttrConstraintIDLong      "ConstraintID"
#define kAttrConstraintIDShort     "cid"

#define kAttrOutputPointLong "OutputPoint"
#define kAttrOutputPointShort "op"

#define kAttrScriptedLong          "Scripted"
#define kAttrScriptedShort         "sc"
#define kAttrScriptedVelocityLong  "ScriptedVelocity"
#define kAttrScriptedVelocityShort "v_sc"
#define kAttrDebugLong             "Debug"
#define kAttrDebugShort            "db"
#define kAttrScaleLong             "Scale"
#define kAttrScaleShort            "sc"
#define kAttrSolverLong            "Solver"
#define kAttrSolverShort           "sol"
#define kAttrSolverLinear          "Linear"
#define kAttrSolverQPMosek         "QPMosek"
#define kAttrDebugFileLong         "DebugFile"
#define kAttrDebugFileShort        "dbFile"
#define kAttrReplaceLogLong        "ReplaceLog"
#define kAttrReplaceLogShort       "rLog"
#define kAttrSceneLong             "Scene"
#define kAttrSceneShort            "scn"
#define kAttrEnabledLong           "Enabled"
#define kAttrEnabledShort          "en"
#define kAttrSConstForceLong       "SConstForce"
#define kAttrSConstForceShort      "scf"
#define kAttrSInexActiveLong       "SInexActive"
#define kAttrSInexActiveShort      "sia"

/** Rigid shape **/
#define kAttrMassLong      "Mass"
#define kAttrMassShort     "mass"
#define kAttrMILong        "MI"
#define kAttrMIShort       "mi"
#define kAttrCompMassLong  "ComputeMass"
#define kAttrCompMassShort "compmass"

/** Strand Shape **/
#define kAttrRadiusLong        "Radius"
#define kAttrRadiusShort       "rad"
// Also used for rigids
#define kAttrMaterialLong       "Material"
#define kAttrMaterialShort      "mat"
#define kAttrDensityLong        "Density"
#define kAttrDensityShort       "rho"
#define kAttrStiffnessLong      "Stiffness"
#define kAttrStiffnessShort     "k"
#define kAttrActParamLong       "ActivationParameter"
#define kAttrActParamShort      "dl_act"
#define kAttrActivationLong     "Activation"
#define kAttrActivationShort    "act"
#define kAttrStrainLong         "Strain"
#define kAttrStrainShort        "strain"
#define kAttrExtraNodesPosLong  "ExtraNodesPos"
#define kAttrExtraNodesPosShort "enodesp"
#define kAttrExtraNodesIdxLong  "ExtraNodesIdx"
#define kAttrExtraNodesIdxShort "enodesidx"
#define kAttrStrandNodesLong    "Nodes"
#define kAttrStrandNodesShort   "nodes"
#define kAttrInextensibleLong   "Inextensible"
#define kAttrInextensibleShort  "inex"
#define kAttrSimpleStrandLong   "SimpleStrand"
#define kAttrSimpleStrandShort  "sims"
// Strand materials
#define kMatSpring       "Spring"
#define kMatMuscleLinear "LinearMuscle"
#define kMatMuscleHill   "HillMuscle"

// Fixed by Maya as internal attributes
/* NOTE These are not used */
/* #define kAttrStrandPointsLong "Points" */
/* #define kAttrStrandPointsShort "pts" */

// Used by strand to keep track of the order of storage of points.
// Primarily a way of saving the strand
#define kAttrStrandOrderLong  "Order"
#define kAttrStrandOrderShort "order"

// Strand Transform
#define kAttrStrandIDLong     "StrandID"
#define kAttrStrandIDShort    "sid"

// Scene
#define  kAttrScenePointsLong       "Points"
#define  kAttrScenePointsShort      "pts"
#define  kAttrSceneRigidsLong       "Rigids"
#define  kAttrSceneRigidsShort      "rgd"
#define  kAttrSceneStrandsLong      "Strands"
#define  kAttrSceneStrandsShort     "str"
#define  kAttrSceneJointsLong       "Joints"
#define  kAttrSceneJointsShort      "jt"
#define  kAttrSceneForcesLong       "Forces"
#define  kAttrSceneForcesShort      "frc"
#define  kAttrSceneConstraintsLong  "Constraints"
#define  kAttrSceneConstraintsShort "cnst"
#define  kAttrIndicatorsFileLong    "IndicatorsFile"
#define  kAttrIndicatorsFileShort   "indFile"

#define  kAttrScenenObjectsLong      "nObjects"
#define  kAttrScenenObjectsShort     "nobjects"
#define  kAttrScenenRigidsLong       "nRigids"
#define  kAttrScenenRigidsShort      "nrigids"
#define  kAttrScenenPointsLong       "nPoints"
#define  kAttrScenenPointsShort      "npoints"
#define  kAttrScenenStrandsLong      "nStrands"
#define  kAttrScenenStrandsShort     "nstrands"
#define  kAttrScenenJointsLong       "nJoints"
#define  kAttrScenenJointsShort      "njoints"
#define  kAttrScenenForcesLong       "nForces"
#define  kAttrScenenForcesShort      "nforces"
#define  kAttrScenenConstraintsLong  "nConstraints"
#define  kAttrScenenConstraintsShort "nconstraints"

#define  kAttrSceneDtLong  "Timestep"
#define  kAttrSceneDtShort "dt"

#define  kAttrSceneGravLong       "Gravity"
#define  kAttrSceneGravShort      "grav"

#define  kAttrSceneDampingLong    "Damping"
#define  kAttrSceneDampingShort   "dmp"

#define  kAttrSceneStabLong    "Stab"
#define  kAttrSceneStabShort   "stb"

#define  kAttrSceneDtxLong        "TimestepMultiplier"
#define  kAttrSceneDtxShort       "dtx"

#define kAttrScenenResultsLong    "nResults"
#define kAttrScenenResultsShort   "nresults"

// Point
#define kAttrLorQLong "LorQ"
#define kAttrLorQShort "lq"

// Joint
#define kAttrJointParentIDLong  "JointParentID"
#define kAttrJointParentIDShort "parentId"
#define kAttrJointChildIDLong   "JointChildID"
#define kAttrJointChildIDShort  "childId"
#define kAttrJointRadLong       "Radius"
#define kAttrJointRadShort      "rad"
#define kAttrJAnglesLong        "Angles"
#define kAttrJAnglesShort       "ang"
#define kAttrDriftPosLong       "DriftPos"
#define kAttrDriftPosShort      "driftpos"
#define kAttrDriftAngLong       "DriftAng"
#define kAttrDriftAngShort      "driftang"
#define kAttrJointDebugALong    "DebugA"
#define kAttrJointDebugAShort   "dbgA"
#define kAttrJointDebugBLong    "DebugB"
#define kAttrJointDebugBShort   "dbgB"
#define kAttrOutJointLong       "OutJoint"
#define kAttrOutJointShort      "outJ"
#define kAttrJLimActiveLong     "JLimActive"
#define kAttrJLimActiveShort    "jla"
#define kAttrJConstForceLong    "JConstForce"
#define kAttrJConstForceShort   "jcf"
#define kAttrJNeutralAngLong    "JNeutralAngle"
#define kAttrJNeutralAngShort   "jna"
#define kAttrJStiffnessLong     "JStiffness"
#define kAttrJStiffnessShort    "jk"
#define kAttrJDampingLong     "JDamping"
#define kAttrJDampingShort    "jd"

#define kAttrLowerLong          "Lower"
#define kAttrLowerShort         "lower"
#define kAttrUpperLong          "Upper"
#define kAttrUpperShort         "upper"
#define kAttrLimitTypeLong      "LimitType"
#define kAttrLimitTypeShort     "limt"

// Force
#define kAttrForceMagnitudeLong  "ForceMagnitude"
#define kAttrForceMagnitudeShort "mag"
#define kAttrForceDynObjIDLong   "DynamicObjectID"
#define kAttrForceDynObjIDShort  "dobjid"
#define kAttrForceWrenchValLong  "ForceWrenchVal"
#define kAttrForceWrenchValShort "wrv"
#define kAttrForceTypeLong       "ForceType"
#define kAttrForceTypeShort      "ftp"

// Constraint
#define kAttrConstSurfVisLong      "ConstSurfVisibile"
#define kAttrConstSurfVisShort     "srfvis"
#define kAttrConstDynObjLong       "DynObj"
#define kAttrConstDynObjShort      "dobj"
#define kAttrConstCompOutSurf      "OutputSurface"
#define kAttrConstCompOutSurfShort "ops"

#define kAttrConstTypeLong   "ConstraintType"
#define kAttrConstTypeShort  "ctype"


#define kAttrShapeTypeLong  "ShapeType"
#define kAttrShapeTypeShort "stype"

#define kSpotShape     "spot"
#define kLineShape     "line"
#define kCircleShape   "circle"
#define kCylinderShape "cylinder"

#define kAttrTransLong   "translate"
#define kAttrTransShort  "t"
#define kAttrRotLong     "rotate"
#define kAttrRotShort    "r"
#define kAttrPosVelLong  "PosVel"
#define kAttrPosVelShort "pvel"
#define kAttrAngVelLong  "AngVel"
#define kAttrAngVelShort "avel"

#define kCustomPoints "CustomPoints"
