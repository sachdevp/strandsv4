#pragma once

#include <maya/MPlug.h>
#include <maya/MMatrix.h>
#include <include_common.hpp>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MTypeId.h>
#include <maya/MPoint.h>
#include <maya/MTimeArray.h>
#include <maya/MFnAnimCurve.h>
#include <MayaStrings.h>
#include <iomanip>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MDGModifier.h>
#include <maya/MDataBlock.h>
#include <maya/MDoubleArray.h>
#include <maya/MDataHandle.h>
#include <maya/MStatus.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MPxNode.h>
#include <maya/MPoint.h>
#include <maya/MPlugArray.h>
#include <maya/MTimeArray.h>
#define MAYA_DEFAULT_TYPE_ID 0x000FFFFF
/** Probing functions for plugs **/
MStatus findInputConnections (MPlug plug, MPlugArray &plugs);
MStatus findOutputConnections(MPlug plug, MPlugArray &plugs);
MStatus findDestinationObjs(std::vector<MObject> &vDest, MPlug plug, MTypeId id = MAYA_DEFAULT_TYPE_ID);
MStatus plugsConnected       (MPlug &src, MPlug &dest);
MStatus findSourcePlug       (MPlug plug, MPlug &src);
MStatus findDestinationPlug  (MPlug &dest, MPlug src, MTypeId id = MAYA_DEFAULT_TYPE_ID);

// Common function to create a locator and return the transform and shape object
MStatus createLocator(MObject &locTrans, MObject &locShape,
                      double locatorScale = 0.2, MString* namePtr=nullptr);
MStatus createLocatorAtPos(MObject &locatorObj, MPoint pos, double locatorScale = 0.2);
MStatus createLocatorInOut(MObject &inLocTrans , MObject &inLocShape ,
                           MObject &outLocTrans, MObject &outLocShape);

// Get functions for aggregate values from plugs
MStatus getPointFromPlug(MPoint& pt, MPlug plug, double scale = 1.0);
MStatus getPointFromPlug(V3& pt, MPlug plug, double scale = 1.0);
MStatus getV6FromPlug(V6& pt, MPlug plug, double scale = 1.0);
MStatus getTranslate(MPoint &p, MObject obj);

// Convert point to vector
inline V3 getVecFromPoint(MPoint &pt){return V3((float)pt[0], (float)pt[1], (float)pt[2]);}

// Process MDoubleArray structure
inline MStatus getVec3FromMDArr(V3 &retv, uint &ind, MDoubleArray arr){
   if(arr.length()<(ind+3)) return MS::kFailure;
   for(uint jj=0; jj<3; jj++, ind++) retv(jj) = (ftype)arr[ind];
   return MS::kSuccess;
}

inline MStatus getMat3FromMDArr(M3 &retm, uint &ind, MDoubleArray arr){
   if(arr.length()<(ind+9)) return MS::kFailure;
   for(int ii=0; ii<3; ii++)
      for(int jj=0; jj<3; jj++, ind++)
         retm(ii,jj) = (ftype)arr[ind];
   return MS::kSuccess;
}

// Get the xform Matrix from the corresponding plug of node @object
MStatus getxform(Eigen::Matrix4t &E, MObject object, double scale=1.0);

// Set the plug value assuming a point
// TODO Write set point to name plug
MStatus setPointToPlug(V3 pt    , MPlug plug);
MStatus setPointToPlug(MPoint pt, MPlug plug);
// MStatus setPointToPlug(MPoint pt, MFnDependencyNode DN, const char* plugName);
// MStatus setPointToPlug(V3 pt    , MFnDependencyNode DN, const char* plugName);
#define SET_POINT_PLUG(pt, obj, attr) setPointToPlug(pt, MFnDependencyNode(obj).findPlug(attr))
#define SET_TRANSLATE(pt, obj) SET_POINT_PLUG(pt, obj, kAttrTransLong)
#define SET_ROTATE(pt, obj) SET_POINT_PLUG(pt, obj, kAttrRotLong)
#define SET_POSVEL(pt, obj) SET_POINT_PLUG(pt, obj, kAttrPosVelLong)
#define SET_ANGVEL(pt, obj) SET_POINT_PLUG(pt, obj, kAttrAngVelLong)

// MStatus setTransformToObj(MObject obj, M4 E);

// Message source and destination information
MStatus findMessageDestination(MObject &outObj, MString plugName, MObject inObj,
                               bool *fieldExists=nullptr);
MStatus findMessageSource     (MObject &inObj, MString plugName, MObject outObj,
                               bool *fieldExists=nullptr);

// Plug array index source
MStatus findPlugIndexSourceNode(MObject &obj, MPlug plug, int ii);

inline MStatus findOutput(MObject &outObj, MObject inObj)
{return findMessageDestination(outObj, kAttrOutputLong, inObj);}
inline MStatus findInput(MObject &inObj, MObject outObj)
{return findMessageSource(inObj, kAttrInputLong, outObj);}

inline MStatus findConstraint(MObject& constObj, MObject dynObj, bool* fieldExists=nullptr)
{return findMessageDestination(constObj, kAttrConstraintLong, dynObj, fieldExists);}

MStatus findConstraintSource(MObject& rigidObj, MObject constObj);

MStatus setVisibility(std::vector<std::pair<MObject, int>> objects, int time);

// Connect objects as input and output using message attributes
MStatus connectObjects(MObject inObj, MObject outObj, MStringArray attrNames);
MStatus connectObjects(MObject inObj, MObject outObj);

// Based on the selection (only 1 object, but maybe 2 nodes), get the MObject
// instances for the shape and the transform
MStatus getShape(MObject &shape, MObject transform);
// TODO Rename to make clear that object is returned
MStatus getTransform(MObject &transform, MObject shape);
MStatus getSelectedMeshObjects(MObject& shape, MObject& transform, MSelectionList sList);
MStatus getObjectByName(const MString name, MObject& object);

// Get the @object to which the source plug for @plug[idx] belongs
MStatus getObjectFromPlugArray(MObject &object, MPlug plug, int idx);
MStatus getObjectLogicalFromPlugArray(MObject &object, MPlug plug, int idx);
// Used for joining strands together
MStatus getSelectedPoints(MObject locators[2], MSelectionList sList);

inline MStatus setSelectedObject(MObject obj){
   MSelectionList sList;
   sList.add(obj);
   MGlobal::setActiveSelectionList(sList);
   return MS::kSuccess;
}

// Check length == @n of the selection list @selected
#define CHECK_SEL_LENGTH(selected, n){                                            \
      if(selected.length()!=n){                                                   \
         log_reg("Select only %d item. %d selected", n, selected.length());       \
         return MS::kFailure;                                                     \
      }}

#define GET_SLIST(sList, n) sList;                \
   MGlobal::getActiveSelectionList(sList);        \
   CHECK_SEL_LENGTH(sList, n);

#define GET_SLIST_ITER(sList, iter, n) GET_SLIST(sList, n); \
   MItSelectionList iter(sList, MFn::kInvalid, &ms);        \
   CHECK_MSTATUS_AND_RETURN_IT(ms);

/* Plug access functions */
// Get value from plug and return the required (ms or nullptr)
#define MPLUG_RET(plugName, DN, attrName) plugName = (DN).findPlug(attrName, false, &ms); \
   CHECK_MSTATUS_AND_RETURN_IT(ms);
#define MPLUG_WARN(plugName, DN, attrName) plugName = (DN).findPlug(attrName, false, &ms); \
   CHECK_MSTATUS(ms);
#define MPLUG_RET_NULL(plugName, DN, attrName) plugName = (DN).findPlug(attrName, false, &ms); \
   CHECK_MSTATUS_AND_RETURN(ms, nullptr);

// Generic functions to access different types of values.
#define GET_VAL(varName, plugName, Type) varName = (plugName).as##Type (MDGContext::fsNormal, &ms); \
   CHECK_MSTATUS_AND_RETURN_IT(ms);
#define GET_VAL2(varName, DN, attrName, Type) varName = ((DN).findPlug(attrName, &ms)).as##Type (MDGContext::fsNormal, &ms); \
   CHECK_MSTATUS_AND_RETURN_IT(ms);
#define GET_VAL3(varName, DN, attrName, Type) varName = ((DN).findPlug(attrName, &ms)).as##Type (MDGContext::fsNormal, &ms);
// Functions like above but returning null.
#define GET_VAL_ELSE_NULL(varName, plugName, Type) varName = (plugName).as##Type (MDGContext::fsNormal, &ms); \
   CHECK_MSTATUS_AND_RETURN(ms, nullptr);
#define GET_VAL_ELSE_NULL2(varName, DN, attrName, Type) varName = (DN).findPlug(attrName, &ms).as##Type (MDGContext::fsNormal, &ms); \
   CHECK_MSTATUS_AND_RETURN(ms, nullptr);
#define GET_VAL_ELSE_NULLOPT(varName, DN, attrName, Type) varName = (DN).findPlug(attrName, &ms).as##Type (MDGContext::fsNormal, &ms); \
   CHECK_MSTATUS_AND_RETURN(ms, std::nullopt);
#define GET_VAL2_ELSE_NULLOPT(varName, plug, Type) varName = plug.as##Type (MDGContext::fsNormal, &ms); \
   CHECK_MSTATUS_AND_RETURN(ms, std::nullopt);
#define GET_POINT(varName, DN, attrName, scale) MCHECKRET(getPointFromPlug(varName, (DN).findPlug(attrName)));

// FIXME RET_NULL -> ELSE_NULL OR RNULL
#define GET_POINT_RET_NULL(varName, DN, attrName, scale) CHECK_MSTATUS_AND_RETURN(getPointFromPlug(varName, (DN).findPlug(attrName)), nullptr);
#define GET_POINT_RET_NULLOPT(varName, DN, attrName, scale) CHECK_MSTATUS_AND_RETURN(getPointFromPlug(varName, (DN).findPlug(attrName)), std::nullopt);
#define GET_POINT_RET(varName, plug, ret) CHECK_MSTATUS_AND_RETURN(getPointFromPlug(varName, plug), ret);
#define GET_POINT2(varName, plug) MCHECKRET(getPointFromPlug(varName, plug));
#define GET_V6_RET_NULL(varName, DN, attrName, scale) CHECK_MSTATUS_AND_RETURN(getV6FromPlug(varName, (DN).findPlug(attrName)), nullptr);

// Works on MString
#define GET_NODE(ATTR) ATTR.substring(0, ATTR.index('.')-1)

/* Required Commands */
/******************* Required commands **********************/
// For MEL commands that need to run successfully to proceed
// Run command, get result if needed, and return required value
#define REQUIRED_CMD_RESULT(command, result)                    \
   CHECK_MSTATUS_AND_RETURN_IT(runMelCommand(command, result));
#define REQUIRED_CMD_RESULT_RNULLOPT(command, result)                   \
   CHECK_MSTATUS_AND_RETURN(runMelCommand(command, result), std::nullopt);
#define REQUIRED_CMD(command)                           \
   CHECK_MSTATUS_AND_RETURN_IT(runMelCommand(command));
#define REQUIRED_CMD_ELSE_NULL(command)                         \
   CHECK_MSTATUS_AND_RETURN(runMelCommand(command), nullptr);
#define REQUIRED_CMD_RESULT_ELSE_NULL(command, result)                  \
   CHECK_MSTATUS_AND_RETURN(runMelCommand(command, result), nullptr);

/*************** MEL Command running functions **************/

inline MStatus runMelCommand(MString melCommand){
   MStatus ms = MGlobal::executeCommand(melCommand);
   if(MFAIL(ms)) log_warn("Command failed: %s", melCommand.asChar());
   log_random("Command: %s", melCommand.asChar());
   return ms;
}

inline MStatus runMelCommand(MString melCommand, MStringArray &result){
   MStatus ms = MGlobal::executeCommand(melCommand, result, false, false);
   if(MFAIL(ms)) log_warn("Command failed: %s", melCommand.asChar());
   log_random("Command: %s", melCommand.asChar());
   std::ostringstream oss;
   oss<<"[";
   for(uint ii=0; ii<result.length();ii++){oss<<result[ii]<<" ";}
   oss<<"]";
   log_random("Result:\n%s", oss.str().c_str());
   return ms;
}

inline MStatus runMelCommand(MString melCommand, MDoubleArray &result){
   MStatus ms = MGlobal::executeCommand(melCommand, result, false, false);
   if(MFAIL(ms)) log_warn("Command failed: %s", melCommand.asChar());
   log_random("Command: %s", melCommand.asChar());
   std::ostringstream oss;
   oss<<"[";
   for(uint ii=0; ii<result.length();ii++){oss<<std::setprecision(4)<<result[ii]<<" ";}
   oss<<"]";
   log_random("Result:\n%s", oss.str().c_str());
   // for(int i=0; i<result.length();i++){log_random("%3.2e",result[i]);}
   return ms;
}

inline MStatus runMelCommand(MString melCommand, MIntArray &result){
   MStatus ms = MGlobal::executeCommand(melCommand, result, false, false);
   if(MFAIL(ms)) log_warn("Command failed: %s", melCommand.asChar());
   log_random("Command: %s", melCommand.asChar());
   std::ostringstream oss;
   oss<<"[";
   for(uint ii=0; ii<result.length();ii++){oss<<std::setprecision(4)<<result[ii]<<" ";}
   oss<<"]";
   log_random("Result:\n%s", oss.str().c_str());
   // for(int i=0; i<result.length();i++){log_random("%3.2e",result[i]);}
   return ms;
}

inline MStatus runMelCommand(MString melCommand, int &result){
   MStatus ms = MGlobal::executeCommand(melCommand, result, false, false);
   if(MFAIL(ms)) log_warn("Command failed: %s", melCommand.asChar());
   log_random("Command: %s", melCommand.asChar());
   log_random("Result: %d" , result);
   return ms;
}

inline MStatus runMelCommand(MString melCommand, double &result){
   MStatus ms = MGlobal::executeCommand(melCommand, result, false, false);
   if(MFAIL(ms)) log_warn("Command failed: %s", melCommand.asChar());
   log_random("Command: %s", melCommand.asChar());
   log_random("Result: %3.2f" , result);
   return ms;
}

#define MLOCAL MFnDependencyNode::kLocalDynamicAttr

/************* Check, Error, and Warn Functions *************/

#define MCHECKRET(STAT)                                                 \
   if(MFAIL(STAT)){                                                     \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<std::endl;    \
      std::cerr<<"Failed command."<<std::endl;                          \
      return STAT;                                                      \
   }

#define MCHECKRET_NULL(STAT)                                            \
   if(MFAIL(STAT)){                                                     \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<std::endl;    \
      std::cerr<<"Failed command."<<std::endl;                          \
      return nullptr;                                                   \
   }

#define MCHECKRETVAL(STAT, VAL)                                         \
   if(MFAIL(STAT)){                                                     \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<std::endl;    \
      std::cerr<<"Failed command."<<std::endl;                          \
      return VAL;                                                       \
   }


#define MCHECKRET_NULLOPT(STAT) MCHECKRETVAL(STAT, std::nullopt);

#define MCHECKWARN(STAT,MSG, ...)                                       \
   if(MS::kSuccess!=(STAT)){                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[WARN] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
   }
#define MCHECKWARN_B(BOOL,MSG, ...)                                     \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[WARN] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
   }
#define MCHECKWARN_R(RET,MSG, ...)                                      \
   if(RFAIL(RET)){                                                      \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[WARN] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
   }
#define MCHECKCONT(STAT,MSG, ...)                                       \
   if(MFAIL(STAT)){                                                     \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[WARN] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      continue;                                                         \
   }

#define MCHECKERROR(STAT,MSG, ...)                                      \
   if(MS::kSuccess!=(STAT)){                                              \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return MS::kFailure;                                              \
   }

#define MCHECKERROR_B(BOOL,MSG, ...)                                    \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return MS::kFailure;                                              \
   }

#define MCHECKERROR_NOTNULL(PTR, MSG, ...)                              \
   if(PTR == nullptr){                                                  \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return MS::kFailure;                                              \
   }

#define MCHECKERROR_RNULL(STAT,MSG, ...)                                \
   if(MS::kSuccess!=(STAT)){                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      ms = MS::kFailure; return nullptr;                                \
   }

#define MCHECKERROR_RNULLOPT(STAT,MSG, ...)                             \
   if(MS::kSuccess!=(STAT)){                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      ms = MS::kFailure; return std::nullopt;                           \
   }

#define MCHECKERROR_NOTNULL_RNULL(PTR,MSG, ...)                         \
   if(PTR == nullptr){                                                  \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      ms = MS::kFailure; return nullptr;                                \
   }

#define MCHECKERROR_B_R(BOOL, RETVAL, MSG, ...)                         \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      ms = MS::kFailure;                                                \
      return RETVAL;                                                    \
   }


#define MCHECKERROR_B_RNULL(BOOL, MSG, ...)                             \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      ms = MS::kFailure;                                                \
      return nullptr;                                                   \
   }

/** Section taken from api_macros.h provided in Maya devkit **/
#define MCHECKERRORNORET(STAT,MSG)              \
   if ( MS::kSuccess != (STAT) ) {              \
      std::cerr << MSG << std::endl;            \
   }
#define MCHECKRET_R(r)                                                  \
   if(RFAIL(r)){                                                        \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<std::endl;    \
      std::cerr<<"Failed command."<<std::endl;                          \
      return MS::kFailure;                                              \
   }

#define MCHECKRET_R_RNULL(r)                                          \
   if(RFAIL(r)){                                                      \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<std::endl;  \
      std::cerr<<"Failed command."<<std::endl;                        \
      ms = MS::kFailure; return nullptr;                              \
   }

#define MCHECKERROR_B(BOOL, MSG, ...)                                 \
   if(BOOL){                                                          \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);               \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl;  \
      return MS::kFailure;                                            \
   }
#define MCHECKERROR_R(STAT, MSG, ...)                                 \
   if(RFAIL(STAT)){                                                   \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);               \
      std::cerr<<"[ERROR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl;  \
      return MS::kFailure;                                            \
   }

/************************************************************/

/************** Attribute related macros ********************/

#define ADD_ATTRIBUTE(ATTR)                                           \
   MStatus ATTR##_stat;                                               \
   ATTR##_stat = addAttribute(ATTR);                                  \
   MCHECKWARN(ATTR##_stat, "addAttribute: " #ATTR); \
   CHECK_MSTATUS_AND_RETURN_IT(ATTR##_stat);

#define MAKE_NUMERIC_ATTR( NAME, LONGNAME, SHORTNAME, TYPE, DEFAULT,  \
                           ARRAY, BUILDER, KEYABLE )                  \
   MStatus NAME##_stat;                                               \
   MFnNumericAttribute NAME##_fn;                                     \
   NAME = NAME##_fn.create( LONGNAME, SHORTNAME, TYPE, DEFAULT );     \
   MCHECKERROR(NAME##_stat, "numeric attr create error");             \
   NAME##_fn.setArray( ARRAY );                                       \
   NAME##_fn.setUsesArrayDataBuilder( BUILDER );                      \
   NAME##_fn.setHidden( ARRAY );                                      \
   NAME##_fn.setKeyable( KEYABLE );                                   \
   NAME##_stat = addAttribute( NAME );                                \
   MCHECKERROR(NAME##_stat, "addAttribute error");

#define ATTRIBUTE_AFFECTS( IN, OUT )                                  \
   MStatus IN##OUT##_stat;                                            \
   IN##OUT##_stat = attributeAffects( IN, OUT );                      \
   MCHECKERROR(IN##OUT##_stat,"attributeAffects:" #IN "->" #OUT);
#define MAKE_TYPED_ATTR( NAME, LONGNAME, SHORTNAME, TYPE, DEFAULT )   \
   MStatus NAME##_stat;                                               \
   MFnTypedAttribute NAME##_fn;                                       \
   NAME = NAME##_fn.create( LONGNAME, SHORTNAME, TYPE, DEFAULT );     \
   NAME##_fn.setHidden( true );                                       \
   NAME##_stat = addAttribute( NAME );                                \
   MCHECKERROR(NAME##_stat, "addAttribute error");

// MStatus dblAttr(MFnDependencyNode DN, const char* longName, const char* shortName, ftype defVal){
//    MFnNumericAttribute nAttr;
//    return DN.addAttribute(nAttr.create(longName, shortName, MFnNumericData::kDouble, defVal), MLOCAL);
// }
inline MStatus pointAttr(MObject &pointAttr, const char* longName, const char* shortName, MPoint defVal = MPoint(0,0,0)){
   MFnNumericAttribute nAttr;
   MStatus ms;
   pointAttr = nAttr.createPoint(longName, shortName, &ms);
   // FIXME defVal ignored for now
   MCHECKERROR(ms, "Could not create numeric attr.");
   return MS::kSuccess;
}

inline MObject dblAttr(const char* longName, const char* shortName, ftype defVal = 0.0f){
   MFnNumericAttribute nAttr;
   MObject outAttr = nAttr.create(longName, shortName, MFnNumericData::kDouble, defVal);
   nAttr.setKeyable(true);
   return outAttr;
}

inline MObject intAttr(const char* longName, const char* shortName, int defVal = -1){
   MFnNumericAttribute nAttr;
   return nAttr.create(longName, shortName, MFnNumericData::kInt, defVal);
}

inline MObject boolAttr(const char* longName, const char* shortName, bool defVal = false){
   MFnNumericAttribute nAttr;
   return nAttr.create(longName, shortName, MFnNumericData::kBoolean, defVal);
}

#define ADD_DN_DBLATTR(DN, ln, sn, defVal) (DN).addAttribute(dblAttr(ln, sn, defVal), MLOCAL)
#define ADD_DN_INTATTR(DN, ln, sn, defVal) (DN).addAttribute(intAttr(ln, sn, defVal), MLOCAL)
#define ADD_DN_BOOLATTR(DN, ln, sn, defVal) (DN).addAttribute(boolAttr(ln, sn, defVal), MLOCAL)
#define ADD_DN_POINTATTR(DN, ln, sn, defVal) MCHECKRET(pointAttr(outAttr, ln, sn, defVal)); \
   MCHECKRET((DN).addAttribute(outAttr, MLOCAL))
#define ADD_DN_POINTATTR2(DN, ln, sn) MCHECKRET(pointAttr(outAttr, ln, sn)); \
   MCHECKRET((DN).addAttribute(outAttr, MLOCAL))

#define DNName(A) ((A).name().asChar())
#define MObjName(A) (MFnDependencyNode(A).name().asChar())

#define INCREMENT_PLUG(DN, attrName){                     \
      MPlug __plug = DN.findPlug(attrName, true, &ms);    \
      int __r;                                            \
      MCHECKRET(__plug.getValue(__r));                    \
      MCHECKRET(__plug.setValue(__r+1));                  \
   }

/************************************************************/

inline MStatus checkChildren(MPlug plug, int nC){
   MCHECKERROR_B(plug.numChildren() != nC, "Incorrect # of attr children.");
   return MS::kSuccess;
}

MStatus clearAnimCurve(MPlug plug);

MStatus resetAnimCurve(MPlug plug, std::vector<double> vVals, MTimeArray *tArr);
MStatus resetAnimCurve(MPlug plug, std::vector<int> vVals, MTimeArray *tArr);

MStatus applyMod(MStringArray &_modArr);
MStatus applyModString(MStringArray &_modArr, MString _modString);
inline MStatus applyModString(MStringArray &_modArr, std::string _modString)
{return applyModString(_modArr, MString(_modString.c_str()));}
MStatus applyModString(MString _modString);
inline MStatus applyModString(std::string _modString)
{return applyModString(MString(_modString.c_str()));}

MIntArray getAttrArray(MPlug plug, MStatus *ret_ms);
