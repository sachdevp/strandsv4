
import maya.cmds as cmds

names=cmds.ls(type="Strand");
for name in names:
    if(cmds.attributeQuery("out", node=name, exists=True)):
        inputNode = name
        outputAttr = inputNode +'.out'
        inputAttr=cmds.connectionInfo(outputAttr, dfs=True);
        outputNode=inputAttr[0].partition(".")[0]
        print outputNode
        inputTransform = cmds.listRelatives(inputNode, parent=True)[0]
        print inputTransform
        outputTransform = cmds.listRelatives(outputNode, parent=True)[0]
        cmds.deleteAttr( outputAttr );
        cmds.deleteAttr( inputAttr );
        cmds.select(inputTransform)
        outputAttr = cmds.addAttr(at="message", sn="out", ln="Output")
        enabledAttr = cmds.addAttr(at="bool", dv=1, sn="en", ln="Enabled")
        cmds.select(outputTransform)
        inputAttr = cmds.addAttr(at="message", sn="in", ln="Input")
        cmds.connectAttr(inputTransform + '.out', outputTransform + '.in' );
