from maya import cmds
import sys
sceneName = 'TEST'
cmds.select(sceneName, r=True)

attrs = ['Points'];
scale = 0.2
print attrs
for attr in attrs:
    print attr
    sz = cmds.getAttr(sceneName+'.nPoints');
    print sz;
    
    for ii in range(0,sz):
        attribute = cmds.connectionInfo(sceneName+'.%s[%d]'%(attr,ii), sfd=True);
        if attribute!=None and attribute!="":
            pointName = attribute.split('.')[0]
            # print pointName
            constAttrExists = cmds.attributeQuery('Constraint', node=pointName, exists=True)
            constAttr = cmds.connectionInfo(pointName+'.Constraint', dfs=True)[0]
            constName = constAttr.split('.')[0]
            # print pointName, ' ', constAttrExists, constName

            constOutAttrs = cmds.connectionInfo(constName+'.outObj', dfs=True)
            if constOutAttrs==[]:
                print 'Output not found for ', constName
                locatorName = cmds.createNode('locator')
                cmds.setAttr(locatorName+'.localScale', scale, scale, scale)
                cmds.addAttr(locatorName, ln='InputObject', sn='inObj', at='message')
                cmds.connectAttr(constName+'.outObj', locatorName+'.inObj')
            # if constAttrExists and (constExists == None):
            #     print pointName
            # constAttr = cmds.connectionInfo(pointName+'.Constraint', sfd=True);
            # cmds.select (node, replace=True);
            # if
            # cmds.addAttr(ln='Debug', sn='db', at='bool');
