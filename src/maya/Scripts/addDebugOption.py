from maya import cmds
import sys
sceneName = 'FingerScene'
cmds.select(sceneName, r=True)

attrs = ['Rigids', 'Strands', 'Joints', 'Points', 'Forces'];
print attrs
for attr in attrs:
    print attr
    sz = cmds.getAttr(sceneName+'.'+attr, size=True);
    print sz;
    
    for ii in range(0,sz):
        attribute = cmds.connectionInfo(sceneName+'.%s[%d]'%(attr,ii), sfd=True);
        print attribute
        if attribute!=None and attribute!="":
            print attribute
            node = attribute.split('.')[0]
            cmds.select (node, replace=True);
            cmds.addAttr(ln='Debug', sn='db', at='bool');
