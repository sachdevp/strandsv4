#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MString.h>
#include <maya/MPoint.h>
#include <maya/MDagPath.h>
#include <maya/MItSelectionList.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPxNode.h>
#include <maya/MDGModifier.h>
#include <maya/MFnMessageAttribute.h>
#include <maya/MGlobal.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnAnimCurve.h>
#include "MayaHelper.hpp"
#include <SE3.hpp>

MStatus clearAnimCurve(MPlug plug){
   MPlugArray connections;
   plug.connectedTo(connections, true, false, 0);
   for(int i=0; i!=connections.length(); i++){
      MObject connObj = connections[i].node();
      MFnDependencyNode connDN(connObj);
      // log_warn("Deleting %s", DNName(connDN));
      MDGModifier dgMod;
      MCHECKWARN(dgMod.disconnect(connections[i], plug),
                 "Could not disconnect animation curve.");
      MCHECKERROR(dgMod.doIt(), "Could not disconnect curve.");
      MDGModifier dgMod2;
      MCHECKWARN(dgMod2.deleteNode(connObj),
                 "Could not delete node for animation curve.");
      MCHECKERROR(dgMod2.doIt(), "Could not delete curve.");
   }
   return MS::kSuccess;
}

MStatus resetAnimCurve(MPlug plug, std::vector<double> vVals, MTimeArray *tArr){
   if(plug.isNull()){
      log_err("Null plug %s", plug.name().asChar());
      return MS::kFailure;
   } else log_verbose("Reseting anim curve for plug %s", plug.name().asChar());

   MDoubleArray dArr(vVals.data(), vVals.size());
   MCHECKWARN(clearAnimCurve(plug), "Could not clear plug for %s", DNName(plug));
   MFnAnimCurve curve;
   MStatus ms;
   MDGModifier dgMod;
   
   MObject curveObj = curve.create(plug, MFnAnimCurve::AnimCurveType::kAnimCurveTU, NULL, &ms);
   MCHECKERROR_B(tArr->length()!=dArr.length(), "Unequal size lengths.");
   MCHECKRET(curve.addKeys(tArr, &dArr));
   return MS::kSuccess;
}

MStatus findInputConnections(MPlug plug, MPlugArray& plugs)
{MStatus ms; plug.connectedTo(plugs, true, false, &ms); return ms;}

MStatus findOutputConnections(MPlug plug, MPlugArray& plugs)
{MStatus ms; plug.connectedTo(plugs, false, true, &ms); return ms;}

MStatus findDestinationObjs(std::vector<MObject> &vDest, MPlug plug, MTypeId id){
   MStatus ms;
   MPlugArray plugs;
   MCHECKRET(findOutputConnections(plug, plugs));
   for(int i=0; i<plugs.length(); i++){
      MObject plugNodeObj = plugs[i].node();
      MPxNode* uNode = MFnDependencyNode(plugNodeObj).userNode();
      if(id==MAYA_DEFAULT_TYPE_ID || uNode->typeId()==id){
         vDest.push_back(plugNodeObj);
      }
   }
   return MS::kSuccess;
}

MStatus findDestinationPlug(MPlug &dest, MPlug src, MTypeId id){
   log_random("Looking for destination plug: %s", DNName(src));

   // get output plugs
   MPlugArray dests;
   MCHECKRET(findOutputConnections(src, dests));
   // Check if there are enough outputs
   uint nPlugs = (uint)dests.length();
   if(nPlugs == 0){log_warn("No outputs"); return MS::kFailure;}
   // Check if @id was supplied by comparing to default
   if(id == MAYA_DEFAULT_TYPE_ID){
      // Return 1st element if found, otherwise fail gracefully
      if(nPlugs==1){dest = dests[0]; return MS::kSuccess;}
      if(nPlugs>0){log_err("Multiple destinations found.");}
      else{log_random("No destination found for: %s", DNName(src)); return MS::kFailure;}
   } else{
      for(uint i=0;i<nPlugs;++i) {
         MObject node = dests[i].node();
         MPxNode* uNode = MFnDependencyNode(node).userNode();
         if(uNode->typeId()==id){
            dest = dests[i];
            log_random("Found id'd destination: %s", DNName(dest));
            return MS::kSuccess;
         }
      }
   }
   return MS::kFailure;
}

MStatus findSourcePlug(MPlug plug, MPlug &src){
   log_random("Looking for source plug: %s", DNName(plug));

   // get input plugs
   MPlugArray srcs;
   MCHECKRET(findInputConnections(plug, srcs));
   if(srcs.length() == 0) {
      // log_warn("No inputs");
      return MS::kFailure;
   }
   src = srcs[0];

   log_random("Found source %s", DNName(src));
   return MS::kSuccess;
}

MStatus getPointFromPlug(MPoint& pt, MPlug plug, double scale){
   MStatus ms = MS::kSuccess;
   MCHECKRET(checkChildren(plug, 3));

   for(int ii=0; ii<3; ii++){
      pt[ii] = plug.child(ii, &ms).asDouble()*scale;
      CHECK_MSTATUS_AND_RETURN_IT(ms);
   }
   pt[3] = 1.0;
   return MS::kSuccess;
}

MStatus getPointFromPlug(Eigen::Vector3t& pt, MPlug plug, double scale){
   MStatus ms = MS::kSuccess;
   MCHECKRET(checkChildren(plug, 3));
   // Get value for each plug
   for(int ii=0; ii<3; ii++){
      pt(ii) = (ftype)plug.child(ii, &ms).asDouble();
      CHECK_MSTATUS_AND_RETURN_IT(ms);
   }
   pt = pt*scale;
   return MS::kSuccess;
}

MStatus getV6FromPlug(V6& pt, MPlug plug, double scale){
   MStatus ms = MS::kSuccess;
   MCHECKRET(checkChildren(plug, 6));
   // Get value for each plug
   for(int ii=0; ii<6; ii++){
      pt(ii) = (ftype)plug.child(ii, &ms).asDouble();
      CHECK_MSTATUS_AND_RETURN_IT(ms);
   }
   pt = pt*scale;
   return MS::kSuccess;
}

MStatus setPointToPlug(V3 pt, MPlug plug){
   MStatus ms = MS::kSuccess;
   // Confirm that it is a point plug
   MCHECKRET(checkChildren(plug, 3));
   // Get value for each plug
   for(int ii=0; ii<3; ii++){
      plug.child(ii, &ms).setValue(pt(ii));
      CHECK_MSTATUS_AND_RETURN_IT(ms);
   }
   return MS::kSuccess;
}

MStatus setPointToPlug(MPoint pt, MPlug plug){
   MStatus ms = MS::kSuccess;
   MCHECKRET(checkChildren(plug, 3));
   // Set value for all plugs
   for(int ii=0; ii<3; ii++){
      plug.child(ii, &ms).setValue(pt[ii]);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
   }
   return MS::kSuccess;
}

MStatus getShape(MObject &shape, MObject transform){
   MStatus ms;
   if(transform.hasFn(MFn::kTransform)){
      MDagPath dagP;
      MDagPath::getAPathTo(transform, dagP);
      dagP.extendToShape();
      shape = dagP.node(&ms);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
      return MS::kSuccess;
   }
   return MS::kFailure;
}

MStatus getTransform(MObject &transform, MObject shape){
   MStatus ms;
   MDagPath dagP;
   MDagPath::getAPathTo(shape, dagP);
   transform = dagP.transform(&ms);
   return ms;
}

MStatus getSelectedMeshObjects(MObject& shape, MObject& transform, MSelectionList sList){
   MStatus ms;
   MObject tmpObj;
   MItSelectionList it(sList);
   MCHECKRET(it.getDependNode(tmpObj));

   // The only purpose to allow 2 selections is the case where both the mesh and
   // it's transform are selected
   // if(sList.length() < 1 || sList.length() > 2 ){
   //    log_err("Too many or too few objects selected.");
   // }

   // Populates the mesh and transform object by looking at the first object in
   // the selection list
   while(!it.isDone()){
      if(tmpObj.hasFn(MFn::kTransform)){
         transform = tmpObj;
         MCHECKRET(getShape(shape, transform));
         return MS::kSuccess;
      } else if(tmpObj.hasFn(MFn::kMesh)){
         shape = tmpObj;
         MCHECKRET(getTransform(transform, shape));
         return MS::kSuccess;
      } else{
         it.next();
         MCHECKRET(it.getDependNode(tmpObj));
         MFnDependencyNode tmpDN(tmpObj);
         log_random("Object: %s", DNName(tmpDN));
      }
   }
   return MS::kFailure;
}

MStatus findMessageDestination
(MObject &outObj, MString plugName, MObject inObj, bool* fieldExists){
   MStatus ms;
   MFnDependencyNode inDN(inObj);

   // Find output plug
   MPlug outPlug, inPlug;
   outPlug = inDN.findPlug(plugName, &ms);
   if(MFAIL(ms)){
      if(fieldExists) *fieldExists = false;
      log_random("Field does not exist");
      return MS::kFailure;
   } else{if(fieldExists) *fieldExists = true;}

   // Find the destination of the outPlug
   // NOTE inPlug is on outObject. PS: I know it's confusing
   MCHECKERROR(findDestinationPlug(inPlug, outPlug), "No message destination.");
   outObj = inPlug.node(&ms);
   return ms;
}

MStatus findMessageSource(MObject &inObj, MString plugName, MObject outObj, bool* fieldExists){
   MStatus ms;
   MFnDependencyNode outDN;
   ms = outDN.setObject(outObj);
   log_random("Looking for plug %s's source in %s", plugName.asChar(), DNName(outDN));

   // Find outPlug to find out object
   MPlug outPlug, inPlug;
   inPlug = outDN.findPlug(plugName, &ms);
   if(MFAIL(ms)){
      if(fieldExists) *fieldExists = false;
      log_random("Field does not exist");
      return MS::kFailure;
   } else{
      if(fieldExists) *fieldExists = true;
   }

   // Find the source of the inPlug
   // NOTE outPlug is on inObject
   // PS: I know it's confusing
   ms = findSourcePlug(inPlug, outPlug);
   if(MFAIL(ms)){
      log_random("No message destination.");
      return ms;
   }

   // Take input object
   inObj = outPlug.node(&ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   return ms;
}

MStatus findPlugIndexSourceNode(MObject & obj, MPlug plug, int ii){
   MPlug tPlug;
   MStatus ms;
   ms = findSourcePlug(plug.elementByLogicalIndex(ii), tPlug);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   obj = tPlug.node(&ms);
   return ms;
}

// NOTE This finds the source, even though the property used is "target"
MStatus findConstraintSource(MObject& rigidObj, MObject constraintObj){
   // DN -> target -> target[0] -> target[0].child(0) -> node
   MStatus ms;
   MFnDependencyNode constDN(constraintObj);
   MPlug targetplug = constDN.findPlug("target", &ms);
   if(MFAIL(ms)){
      rigidObj = MObject::kNullObj;
      log_verbose("No \"target\" attr. Fixed to world.");
      return MS::kSuccess;
   }
   MPlug tplug = targetplug.elementByPhysicalIndex(0);
   MPlug comp_plug = tplug.child(0);
   MPlugArray conns;
   comp_plug.connectedTo(conns,true,false, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   if(conns.length() > 0){
      rigidObj = conns[0].node(&ms);
      CHECK_MSTATUS_AND_RETURN_IT(ms);
      return MS::kSuccess;
   }
   return MS::kFailure;
}

MStatus connectObjects(MObject inObj, MObject outObj, MStringArray attrNames){
   MFnMessageAttribute mAttr;
   MFnDependencyNode inDN, outDN;
   MDGModifier dgMod;
   MStatus ms;
   MObject outLocMsg, inLocMsg;

   // Input object first
   inDN.setObject(inObj);
   outLocMsg = mAttr.create(attrNames[0], attrNames[2], &ms );
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MCHECKRET(inDN.addAttribute(outLocMsg, MLOCAL));

   // Now for output object
   outDN.setObject(outObj);
   inLocMsg = mAttr.create( attrNames[1], attrNames[3], &ms );
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MCHECKRET(outDN.addAttribute(inLocMsg, MLOCAL));

   // Connect outMsg of inObj to inLocMsg of outObj
   dgMod.connect(inObj, outLocMsg, outObj, inLocMsg);
   MCHECKRET(dgMod.doIt());

   return MS::kSuccess;
}

MStatus setVisibility(std::vector<std::pair<MObject, int>> objects, int time){
   MString strCTime = "currentTime ";
   MString strSetKeyframe = "setKeyframe -v ";
   int cTime;
   REQUIRED_CMD_RESULT(strCTime + "-q", cTime);
   REQUIRED_CMD(strCTime + time);
   MFnDependencyNode DN;

   for(auto pair: objects){
      DN.setObject(pair.first);
      // FIXME Time access and keyframing with C++ as much as possible
      // MPlug vPlug = DN.findPlug("visibility");
      // MFnAnimCurve vAnimCurve(vPlug);
      // vAnimCurve.addKeyFrame(
      REQUIRED_CMD(strSetKeyframe + pair.second + " -at visibility \"" + DN.name() + "\"");
   }
   REQUIRED_CMD(strCTime + cTime);
   return MS::kSuccess;
}

MStatus connectObjects(MObject inObj, MObject outObj){
   MStringArray attrNames;
   attrNames.setLength(4);
   attrNames[0] = kAttrOutputLong;   attrNames[1] = kAttrInputLong;
   attrNames[2] = kAttrOutputShort;  attrNames[3] = kAttrInputShort;

   MCHECKRET(connectObjects(inObj, outObj, attrNames));
   MFnDependencyNode inDN(inObj), outDN(outObj);

   std::vector<std::pair<MObject, int>> objects;
   objects.push_back(std::make_pair(inObj, 1));
   objects.push_back(std::make_pair(outObj, 0));
   setVisibility(objects, 1);
   objects.push_back(std::make_pair(inObj, 0));
   objects.push_back(std::make_pair(outObj, 1));
   setVisibility(objects, 2);

   return MS::kSuccess;
}

MStatus isMeshObj(MObject obj){
   MStatus ms;
   MDagPath dagPath;
   MDagPath::getAPathTo(obj, dagPath);
   if(dagPath.hasFn(MFn::kTransform)){MCHECKRET(dagPath.extendToShapeDirectlyBelow(0));}
   if(dagPath.hasFn(MFn::kMesh)){return MS::kSuccess;}
   return MS::kFailure;
}

MStatus isPointObj(MObject &shapeObj, MObject &transObj, MObject obj, MDagPath *dagPath){
   MStatus ms;
   shapeObj = MObject::kNullObj;
   transObj = MObject::kNullObj;
   if(dagPath == nullptr){
      dagPath = new MDagPath;
      MDagPath::getAPathTo(obj, *dagPath);
   }
   if(dagPath->hasFn(MFn::kTransform)){
      MCHECKRET(dagPath->extendToShapeDirectlyBelow(0));
   }
   if(dagPath->hasFn(MFn::kLocator)){
      shapeObj = dagPath->node();
      transObj = dagPath->transform();
      return MS::kSuccess;
   }
   return MS::kFailure;
}

MStatus getObjectByName(const MString name, MObject & object){
    object = MObject::kNullObj;
    MSelectionList sList;
    if(!MFAIL(sList.add(name))) return sList.getDependNode(0, object);
    log_err("Object not found: %s", name.asChar());
    return MS::kFailure;
}

MStatus getxform(M4 &E, MObject object, double scale){
   MDoubleArray dbArray;
   MFnDependencyNode DN(object);
   // CHECK Use data handle instead?
   MCHECKRET(MGlobal::executeCommand("getAttr " + DN.name() + ".xformMatrix", dbArray));
   // Write Matrix4t
   // FIXME Replace by available functions.
   for(uint ii=0;ii<4;ii++) for(uint jj=0;jj<4;jj++) E(jj,ii) = (ftype)dbArray[ii*4+jj];
   V3 t = E.block<3,1>(0,3);
   E.block<3,1>(0,3) = t*scale;
   return MS::kSuccess;
}

MStatus getObjectFromPlugArray(MObject &object, MPlug plug, int idx){
   MStatus ms;
   MPlug srcPlug, idxPlug;
   if(!plug.isArray()) return MS::kFailure;
   idxPlug = plug.elementByPhysicalIndex(idx, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   // Find source plug for the child found
   ms = findSourcePlug(idxPlug, srcPlug);
   // Do not throw error here. Errors here are meaningless.
   if(MFAIL(ms)) return MS::kFailure;

   // Get object and dependency node
   object = srcPlug.node(&ms);
   return ms;
}

MStatus getObjectLogicalFromPlugArray(MObject &object, MPlug plug, int idx){
   MStatus ms;
   MPlug srcPlug, idxPlug;
   if(!plug.isArray()) return MS::kFailure;
   idxPlug = plug.elementByLogicalIndex(idx, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   // Find source plug for the child found
   MCHECKRET(findSourcePlug(idxPlug, srcPlug));

   // Get object and dependency node
   object = srcPlug.node(&ms);
   return ms;
}

MStatus createLocator(MObject &locTrans, MObject &locShape,
                      double locScale, MString *namePtr){
   MFnDependencyNode locDN;
   MStatus ms; MString name;
   if(namePtr) name = *namePtr; else name = "locator";
   locShape = locDN.create("locator", name + "Shape", &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);

   MString retName = locDN.setName(name, &ms);
   if(namePtr) *namePtr = name;
   MCHECKRET(getTransform(locTrans, locShape));
   MString attrName = MFnDependencyNode(locShape).name() + ".localScale ";
   REQUIRED_CMD("currentTime 1");
   REQUIRED_CMD("setAttr " + attrName + locScale + " " + locScale + " " + locScale);
   REQUIRED_CMD("setKeyframe " + attrName);
   return MS::kSuccess;
}

MStatus createLocatorAtPos(MObject &locatorObj, MPoint pos, double locScale){
   MString locatorName;
   MFnDependencyNode locatorDN;

   locatorObj = locatorDN.create("locator");
   locatorName = locatorDN.name();

   // Set local scale for the locator to resize its arrows
   MString attrName = locatorName + ".localScale ";
   REQUIRED_CMD("currentTime 1");
   runMelCommand("setAttr " + attrName +
                 locScale + " " + locScale + " " + locScale);
   REQUIRED_CMD("setKeyframe " + attrName);

   MCHECKERROR(setPointToPlug(pos, locatorDN.findPlug("translate")),
               "Could not set position for locator");
   return MS::kSuccess;
}

MStatus createLocatorInOut(MObject &inLocTrans , MObject &inLocShape ,
                           MObject &outLocTrans, MObject &outLocShape){
   MCHECKRET(createLocator(inLocTrans, inLocShape));
   MCHECKRET(createLocator(outLocTrans, outLocShape));
   MCHECKRET(connectObjects(inLocTrans, outLocTrans));
   return MS::kSuccess;
}

// MStatus setTransformToObj(MObject obj, M4 E){
//    MCHECKERROR_B(!obj.hasFn(MFn::kTransform), "Not a transform");
//    MStatus ms;
//    MFnDependencyNode DN(obj);
//    // Values to be set
//    V3 pos, angles;
//    MCHECKERROR_R(SE3::getPosAngles(pos, angles, E), "Could not get pos/angles.");

//    // Plugs
//    // MPlug transPlug = outConstTransDN.findPlug("translate", &ms);
//    // MPlug rotPlug   = outConstTransDN.findPlug("rotate"   , &ms);
//    // CHECK_MSTATUS_AND_RETURN_IT(ms);
//    MCHECKERROR(SET_TRANSLATE(pos, obj), "Could not set translate");
//    MCHECKERROR(SET_ROTATE(angles, obj), "Could not set rotate");

//    REQUIRED_CMD("setKeyframe {\"" + DN.name() + ".t\"}");
//    REQUIRED_CMD("setKeyframe {\"" + DN.name() + ".r\"}");
//    return MS::kSuccess;
// }

MStatus getTranslate(MPoint &p, MObject obj){
   MStatus ms;
   MPlug transPlug = MFnDependencyNode(obj).findPlug(kAttrTransLong, true, &ms);
   MCHECKERROR(ms, "Could not find translate plug.");
   MCHECKERROR(getPointFromPlug(p, transPlug), "Could not get point from translate plug.");
   return MS::kSuccess;
}

MStatus applyMod(MStringArray &_modArr){
   MCHECKERROR_B(_modArr.length()%2 == 1,
                 "Modifier string array is not of even length. Check format.");
   double result;
   MStringArray tempArr;
   MObject obj;
   for(uint ii = 0; ii<_modArr.length(); ii+=2){
      tempArr.clear();
      MCHECKERROR(_modArr[ii].split('.', tempArr), "Could not split mod arr element %s", _modArr[ii].asChar());
      MCHECKERROR(getObjectByName(tempArr[0], obj), "Couldn't get object by name %s", tempArr[0].asChar());
      MPlug plug = MFnDependencyNode(obj).findPlug(tempArr[1]);
      plug.getValue(result);
      plug.setValue(_modArr[ii+1].asFloat());
      _modArr[ii+1] = MString()+result;
   }
   return MS::kSuccess;
}

MStatus applyModString(MStringArray &_modArr, MString _modString){
   MStatus ms;
   MCHECKERROR(_modString.split(' ', _modArr), "Could not split mod string");
   MCHECKERROR(applyMod(_modArr), "Failed to apply modString");
   return MS::kSuccess;
}

MStatus applyModString(MString _modString){
   MStatus ms;
   MStringArray modArr;
   MCHECKERROR(_modString.split(' ', modArr), "Could not split mod string");
   MCHECKERROR(applyMod(modArr), "Failed to apply modString");
   return MS::kSuccess;
}

// @plug is an array plug.
MIntArray getAttrArray(MPlug plug, MStatus *ret_ms){
   MIntArray values;
   MPlug childPlug;
   MStatus ms;
   for(uint ii=0; ii<plug.numElements(); ii++){
      childPlug = plug.elementByLogicalIndex(ii, &ms);
      if(MFAIL(ms)){
         log_err("Failed to get array from %s", DNName(plug));
         if(ret_ms) *ret_ms = MS::kFailure;
      }
      values.append(childPlug.asInt());
   }

   if(ret_ms) *ret_ms = MS::kSuccess;
   return values;
}
