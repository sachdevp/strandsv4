//-
// ==========================================================================
// Copyright 2015 Autodesk, Inc.  All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+

///////////////////////////////////////////////////////////////////////////////
//
// apiMeshCreator.cpp
//
////////////////////////////////////////////////////////////////////////////////

#include <math.h>
#include <maya/MIOStream.h>

#include "apiMeshCreator.h"
#include "apiMeshData.h"
#include "apiMeshGeom.h"

#include <include_common.hpp>
#include <MayaHelper.hpp>

#include <maya/MFnMesh.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <MayaStrings.h>
#include <ConstraintNode/ConstraintHelper.hpp>
#include <SceneConstants.hpp>

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif

#ifndef M_PI_2
#define M_PI_2		1.57079632679489661923
#endif

////////////////////////////////////////////////////////////////////////////////
//
// Shape implementation
//
////////////////////////////////////////////////////////////////////////////////

MObject apiMeshCreator::size;
MObject apiMeshCreator::constTypeAttr;
MObject apiMeshCreator::constTransAttr;
MObject apiMeshCreator::targetPosAttr;
MObject apiMeshCreator::inputMesh;
MObject apiMeshCreator::outputSurface;

MTypeId apiMeshCreator::id(0x80089);

apiMeshCreator::apiMeshCreator() {}
apiMeshCreator::~apiMeshCreator() {}

///////////////////////////////////////////////////////////////////////////////
//
// Overrides
//
///////////////////////////////////////////////////////////////////////////////

MStatus apiMeshCreator::computeDist(double &dist, MDataBlock dataBlock){
   MDataHandle targetHandle = dataBlock.inputValue(targetPosAttr);
   float3& targetPos = targetHandle.asFloat3();
   MDataHandle constTransHandle = dataBlock.inputValue(constTransAttr);
   MMatrix& transform = constTransHandle.asMatrix();
   float3 axisz, constPos;
   axisz[0] = (ftype)transform[2][0];
   axisz[1] = (ftype)transform[2][1];
   axisz[2] = (ftype)transform[2][2];

   constPos[0] = (ftype)transform[3][0];
   constPos[1] = (ftype)transform[3][1];
   constPos[2] = (ftype)transform[3][2];

   // Interpret here how the constraint is drawn
   float dist2=0, dotp=0; float3 tmp;

   for(int i=0; i<3; i++){
      tmp[i] = targetPos[i] - constPos[i];
      dotp  += tmp[i] * axisz[i];
   }
   for(int i=0; i<3; i++){
      tmp[i] = tmp[i] - dotp*axisz[i];
      dist2 += tmp[i] * tmp[i];
   }
   dist = sqrt(dist2);
   return MS::kSuccess;
}

/* override */
MStatus apiMeshCreator::compute(const MPlug& plug, MDataBlock& dataBlock)
//
// Description
//
//    When input attributes are dirty this method will be called to
//    recompute the output attributes.
//
{
   MStatus stat;
   if (plug == outputSurface) {
      // Create some user defined geometry data and access the
      // geometry so we can set it
      //
      MFnPluginData fnDataCreator;
      MTypeId tmpid(apiMeshData::id);
      fnDataCreator.create(tmpid, &stat);
      MCHECKERROR(stat, "compute : error creating apiMeshData")

         apiMeshData * newData = (apiMeshData*)fnDataCreator.data(&stat);
      MCHECKERROR(stat, "compute : error gettin at proxy apiMeshData object")

         apiMeshGeom * geomPtr = newData->fGeometry;

      // If there is an input mesh then copy it's values
      // and construct some apiMeshGeom for it.
      //
      bool hasHistory = computeInputMesh(plug, dataBlock,
                                          geomPtr->vertices,
                                          geomPtr->face_counts,
                                          geomPtr->face_connects,
                                          geomPtr->normals,
                                          geomPtr->uvcoords
                                         );
      // There is no input mesh so check the constraintType attribute
      // and create either a cube or a sphere.
      //
      if (!hasHistory) {
         MDataHandle sizeHandle = dataBlock.inputValue(size);
         double shape_size = sizeHandle.asDouble();
         MDataHandle typeHandle = dataBlock.inputValue(constTypeAttr);
         short constType = typeHandle.asShort();
         double dist=3;
         computeDist(dist, dataBlock);
         // Interpret here how the constraint is drawn
         switch(constType){
         case kCTFixedConstraintInt:
         case kCTParentConstraintInt:
            break;
         case kCTLineConstraintInt:
            // Build line by repurposing cylinder
            buildCylinder(0.01, // Small radius
                          shape_size,
                          32,
                          geomPtr->vertices,
                          geomPtr->face_counts,
                          geomPtr->face_connects,
                          geomPtr->normals,
                          geomPtr->uvcoords);
            break;
         case kCTCircleConstraintInt:
            // Build circle by repurposing cylinder
            buildCylinder(dist,
                          0.01, // Small length
                          32,
                          geomPtr->vertices,
                          geomPtr->face_counts,
                          geomPtr->face_connects,
                          geomPtr->normals,
                          geomPtr->uvcoords);
            break;
         case kCTCylinderConstraintInt:
            buildCylinder(dist,
                          shape_size,
                          32,
                          geomPtr->vertices,
                          geomPtr->face_counts,
                          geomPtr->face_connects,
                          geomPtr->normals,
                          geomPtr->uvcoords);
            break;
            // buildCube(shape_size,
            //            geomPtr->vertices,
            //            geomPtr->face_counts,
            //            geomPtr->face_connects,
            //            geomPtr->normals,
            //            geomPtr->uvcoords);
            // break;
         // case 1 : // buld a sphere
            // buildSphere(shape_size,
            //             32,
            //             geomPtr->vertices,
            //             geomPtr->face_counts,
            //             geomPtr->face_connects,
            //             geomPtr->normals,
            //             geomPtr->uvcoords);
            // break;
            } // end switch
      }

      geomPtr->faceCount = geomPtr->face_counts.length();

      // Assign the new data to the outputSurface handle
      //
      MDataHandle outHandle = dataBlock.outputValue(outputSurface);
      outHandle.set(newData);
      dataBlock.setClean(plug);
      return MS::kSuccess;
   }
   else {
      return MS::kUnknownParameter;
   }
}

///////////////////////////////////////////////////////////////////////////////
//
// Helper routines
//
///////////////////////////////////////////////////////////////////////////////

void apiMeshCreator::buildCube(
                               double cube_size,
                               MPointArray& pa,
                               MIntArray& faceCounts,
                               MIntArray& faceConnects,
                               MVectorArray& normals,
                               apiMeshGeomUV& uvs
                              )
//
// Description
//
//    Constructs a cube
//
{
   const int num_faces			= 6;
   const int num_face_connects	= 24;
   const double normal_value   = 0.5775;
   const int uv_count			= 14;
	
   pa.clear(); faceCounts.clear(); faceConnects.clear();
   uvs.reset();

   pa.append(MPoint(-cube_size, -cube_size, -cube_size));
   pa.append(MPoint( cube_size, -cube_size, -cube_size));
   pa.append(MPoint( cube_size, -cube_size,  cube_size));
   pa.append(MPoint(-cube_size, -cube_size,  cube_size));
   pa.append(MPoint(-cube_size,  cube_size, -cube_size));
   pa.append(MPoint(-cube_size,  cube_size,  cube_size));
   pa.append(MPoint( cube_size,  cube_size,  cube_size));
   pa.append(MPoint( cube_size,  cube_size, -cube_size));

   normals.append(MVector(-normal_value, -normal_value, -normal_value));
   normals.append(MVector( normal_value, -normal_value, -normal_value));
   normals.append(MVector( normal_value, -normal_value,  normal_value));
   normals.append(MVector(-normal_value, -normal_value,  normal_value));
   normals.append(MVector(-normal_value,  normal_value, -normal_value));
   normals.append(MVector(-normal_value,  normal_value,  normal_value));
   normals.append(MVector( normal_value,  normal_value,  normal_value));
   normals.append(MVector( normal_value,  normal_value, -normal_value));

   // Define the UVs for the cube.
   //
   float uv_pts[uv_count * 2] = { 0.375, 0.0,
                                  0.625, 0.0,
                                  0.625, 0.25,
                                  0.375, 0.25,
                                  0.625, 0.5,
                                  0.375, 0.5,
                                  0.625, 0.75,
                                  0.375, 0.75,
                                  0.625, 1.0,
                                  0.375, 1.0,
                                  0.875, 0.0,
                                  0.875, 0.25,
                                  0.125, 0.0,
                                  0.125, 0.25
   };

   // UV Face Vertex Id.
   //
   int uv_fvid [ num_face_connects ] = {  0, 1, 2, 3,
                                          3, 2, 4, 5,
                                          5, 4, 6, 7,
                                          7, 6, 8, 9,
                                          1, 10, 11, 2,
                                          12, 0, 3, 13 };

   int i;
   for (i = 0; i < uv_count; i ++) {
      uvs.append_uv(uv_pts[i*2], uv_pts[i*2 + 1]);
   }

   for (i = 0; i < num_face_connects; i ++) {
      uvs.faceVertexIndex.append(uv_fvid[i]);
   }

   // Set up an array containing the number of vertices
   // for each of the 6 cube faces (4 verticies per face)
   //
   int face_counts[num_faces] = { 4, 4, 4, 4, 4, 4 };

   for (i=0; i<num_faces; i++)
      {
         faceCounts.append(face_counts[i]);
      }

   // Set up and array to assign vertices from pa to each face
   //
   int face_connects[ num_face_connects ] = {	0, 1, 2, 3,
                                              4, 5, 6, 7,
                                              3, 2, 6, 5,
                                              0, 3, 5, 4,
                                              0, 4, 7, 1,
                                              1, 7, 6, 2	};
   for (i=0; i<num_face_connects; i++)
      {
         faceConnects.append(face_connects[i]);
      }
}

void apiMeshCreator::
buildSphere(double          rad,
            int             div,
            MPointArray &		vertices,
            MIntArray &			counts,
            MIntArray &			connects,
            MVectorArray &	normals,
            apiMeshGeomUV &	uvs)
// Description
//
//    Create circles of vertices starting with
//    the top pole ending with the botton pole
{
   double u = -M_PI_2;
   double v = -M_PI;
   double u_delta = M_PI / ((double)div);
   double v_delta = 2 * M_PI / ((double)div);

   MPoint topPole(0.0, rad, 0.0);
   MPoint botPole(0.0, -rad, 0.0);

   // Build the vertex and normal table
   //
   vertices.append(botPole);
   normals.append(botPole-MPoint::origin);
   int i;
   for (i=0; i<(div-1); i++)
      {
         u += u_delta;
         v = -M_PI;

         for (int j=0; j<div; j++)
            {
               double x = rad * cos(u) * cos(v);
               double y = rad * sin(u);
               double z = rad * cos(u) * sin(v) ;
               MPoint pnt(x, y, z);
               vertices.append(pnt);
               normals.append(pnt-MPoint::origin);
               v += v_delta;
            }
      }
   vertices.append(topPole);
   normals.append(topPole-MPoint::origin);

   // Create the connectivity lists
   //
   int vid = 1;
   int numV = 0;
   for (i=0; i<div; i++)
      {
         for (int j=0; j<div; j++)
            {
               if (i==0) {
                  counts.append(3);
                  connects.append(0);
                  connects.append(j+vid);
                  connects.append((j==(div-1)) ? vid : j+vid+1);
               }
               else if (i==(div-1)) {
                  counts.append(3);
                  connects.append(j+vid+1-div);
                  connects.append(vid+1);
                  connects.append(j==(div-1) ? vid+1-div : j+vid+2-div);
               }
               else {
                  counts.append(4);
                  connects.append(j + vid+1-div);
                  connects.append(j + vid+1);
                  connects.append(j == (div-1) ? vid+1 : j+vid+2);
                  connects.append(j == (div-1) ? vid+1-div : j+vid+2-div);
               }
               numV++;
            }
         vid = numV;
      }

   // TODO: Define UVs for sphere ...
   //
}

void apiMeshCreator::
buildCylinder(double          rad,
              double          length,
              int             div,
              MPointArray &		vertices,
              MIntArray &			counts,
              MIntArray &			connects,
              MVectorArray &	normals,
              apiMeshGeomUV &	uvs){
   double u_delta = 2*M_PI / ((double)div);

   MPoint topCenter(0.0, 0.0,  length/2.0);
   MPoint botCenter(0.0, 0.0, -length/2.0);
   MPoint center = botCenter;

   // Build the vertex and normal table
   double u = 0;
   for(int i=0;i<2;i++){
      u = -M_PI;
      for (int j=0; j<div; j++) {
         double x = rad * cos(u);
         double y = rad * sin(u);
         MPoint offset(x, y, 0.0);
         vertices.append(center + offset);
         normals.append(offset);
         u += u_delta;
      }
      center = topCenter;
   }

   // Create the connectivity lists
   for(int i=0; i<div; i++){
      counts.append  (4);
      connects.append(i);
      connects.append(i+div);
      connects.append(i == (div-1) ? div : (i+div+1));
      connects.append(i == (div-1) ? 0 : i+1);
   }
}

MStatus apiMeshCreator::
computeInputMesh(const MPlug&		plug,
                 MDataBlock&		dataBlock,
                 MPointArray&		vertices,
                 MIntArray&			counts,
                 MIntArray&			connects,
                 MVectorArray&	normals,
                 apiMeshGeomUV&	uvs)
// Description
//
//     This function takes an input surface of type kMeshData and converts
//     the geometry into this nodes attributes.
//     Returns kFailure if nothing is connected.
{
   MStatus ms;

   // Get the input subdiv
   MDataHandle inputData = dataBlock.inputValue(inputMesh, &ms);
   MCHECKERROR(ms, "compute get inputMesh") MObject surf = inputData.asMesh();

   // Check if anything is connected
   //
   MObject thisObj = thisMObject();
   MPlug surfPlug(thisObj, inputMesh);
   if (!surfPlug.isConnected()) {
    	ms = dataBlock.setClean(plug);
	    MCHECKERROR(ms, "compute setClean") return MS::kFailure;
   }

   // Extract the mesh data
   //
   MFnMesh surfFn (surf, &ms);
   MCHECKERROR(ms, "compute - MFnMesh error");
   ms = surfFn.getPoints(vertices, MSpace::kObject);
   MCHECKERROR(ms, "compute getPoints");

   // Check to see if we have UVs to copy.
   //
   bool hasUVs = surfFn.numUVs() > 0;
   surfFn.getUVs(uvs.ucoord, uvs.vcoord);

   for (int i=0; i<surfFn.numPolygons(); i++) {
      MIntArray polyVerts;
      surfFn.getPolygonVertices(i, polyVerts);
      int pvc = polyVerts.length();
      counts.append(pvc);
      int uvId;
      for (int v=0; v<pvc; v++) {
         if (hasUVs) {
            surfFn.getPolygonUVid(i, v, uvId);
            uvs.faceVertexIndex.append(uvId);
         }
         connects.append(polyVerts[v]);
      }
   }

   for (int n=0; n<(int)vertices.length(); n++) {
      MVector normal;
      surfFn.getVertexNormal(n, normal);
      normals.append(normal);
   }

   return MS::kSuccess;
}

void* apiMeshCreator::creator() {return new apiMeshCreator();}

MStatus apiMeshCreator::initialize(){
   MStatus             ms;
   MFnTypedAttribute   typedAttr;
   MFnEnumAttribute    eAttr;
   MFnMatrixAttribute  mAttr;
   MFnNumericAttribute nAttr;

   // ----------------------- INPUTS -------------------------
   MAKE_NUMERIC_ATTR(size, "size", "sz", MFnNumericData::kDouble, 1, false, false, true);
   ms = createConstTypeAttr(constTypeAttr);
   constTransAttr = mAttr.create("ConstraintTransform", "cxform",
                                 MFnMatrixAttribute::Type::kDouble, &ms);
   targetPosAttr = nAttr.createPoint("TargetPosition", "tpos", &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MAKE_TYPED_ATTR(inputMesh, "inputMesh", "im", MFnData::kMesh, NULL);
   ADD_ATTRIBUTE(constTypeAttr);
   ADD_ATTRIBUTE(targetPosAttr);
   ADD_ATTRIBUTE(constTransAttr);

   // ----------------------- OUTPUTS -------------------------
   outputSurface = typedAttr.create("outputSurface", "os",
                                     apiMeshData::id,
                                     MObject::kNullObj, &ms);
   MCHECKERROR(ms, "create outputSurface attribute") typedAttr.setWritable(false);
   ADD_ATTRIBUTE(outputSurface);

   // ---------- Specify what inputs affect the outputs ----------
   ATTRIBUTE_AFFECTS(inputMesh, outputSurface);
   ATTRIBUTE_AFFECTS(size, outputSurface);
   ATTRIBUTE_AFFECTS(constTypeAttr, outputSurface);
   ATTRIBUTE_AFFECTS(targetPosAttr, outputSurface);
   ATTRIBUTE_AFFECTS(constTransAttr, outputSurface);

   return MS::kSuccess;
}
