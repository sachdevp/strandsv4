/**
   @auth: Prashant Sachdeva (sachdevp@cs.ubc.ca)
   @desc: Node to compute constraint properties based on scene
*/

#include <maya/MColor.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MDistance.h>
#include <maya/MEventMessage.h>
#include <maya/MMatrix.h>
#include <maya/MPlugArray.h>
#include <maya/MPointArray.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MVector.h>
#include <maya/MFnMesh.h>
#include <maya/MVectorArray.h>

#include <maya/MFnDependencyNode.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MFnPlugin.h>
#include <maya/MFnPluginData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnUnitAttribute.h>

#include <maya/M3dView.h>
#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MUserData.h>

// Project includes
#include <ConstraintNode/ConstraintHelper.hpp>
#include <ConstraintNode/ConstraintNode.hpp>
#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <ConstraintNode/apiMeshShape/apiMeshData.h>
#include <ConstraintNode/apiMeshShape/apiMeshGeom.h>
#include <include_common.hpp>
#include <ConstraintNode/apiMeshShape/apiMeshShape_export.hpp>
#include <SceneConstants.hpp>

MObject ConstraintComputeNode::targetPosAttr;
MObject ConstraintComputeNode::constTypeAttr;
MObject ConstraintComputeNode::constTransAttr;
MObject ConstraintComputeNode::outSurfAttr;
MObject ConstraintComputeNode::inputMesh;

MTypeId ConstraintComputeNode::id(0x80019);
MString ConstraintComputeNode::drawDbClassification("drawdb/geometry/ConstraintComputeNode");
MString ConstraintComputeNode::drawRegistrantId("ConstraintComputeNodePlugin");

ConstraintComputeNode::ConstraintComputeNode(){}
ConstraintComputeNode::~ConstraintComputeNode(){}

void ConstraintComputeNode::
buildCylinder(double        rad,
              double        length,
              int           div,
              MPointArray  &vertices,
              MIntArray    &counts,
              MIntArray    &connects,
              MVectorArray &normals){
   double u_delta = 2*M_PI / ((double)div);

   MPoint topCenter(0.0, 0.0,  length/2.0);
   MPoint botCenter(0.0, 0.0, -length/2.0);
   MPoint center = botCenter;

   // Build the vertex and normal table
   double u = 0;
   for(int i=0;i<2;i++){
      u = -M_PI;
      for (int j=0; j<div; j++) {
         double x = rad * cos(u);
         double y = rad * sin(u);
         MPoint offset(x, y, 0.0);
         vertices.append(center + offset);
         normals.append(MVector(offset));
         u += u_delta;
      }
      center = topCenter;
   }

   // Create the connectivity lists
   for(int i=0; i<div; i++){
      counts.append  (4);
      connects.append(i);
      connects.append(i+div);
      connects.append(i == (div-1) ? div : (i+div+1));
      connects.append(i == (div-1) ? 0 : i+1);
   }
}

MStatus ConstraintComputeNode::computeShape(MDataBlock &dataBlock, apiMeshGeom* geomPtr){
   MStatus ms;
   MDataHandle typeHandle = dataBlock.inputValue(constTypeAttr);
   short constType = typeHandle.asShort();
   MDataHandle targetHandle = dataBlock.inputValue(targetPosAttr);
   float3& targetPos = targetHandle.asFloat3();
   MDataHandle constTransHandle = dataBlock.inputValue(constTransAttr);
   MMatrix& transform = constTransHandle.asMatrix();
   float3 axisz, constPos;
   axisz[0] = transform[2][0];
   axisz[1] = transform[2][1];
   axisz[2] = transform[2][2];

   constPos[0] = transform[3][0];
   constPos[1] = transform[3][1];
   constPos[2] = transform[3][2];

   // Interpret here how the constraint is drawn
   float dist2=0, dotp=0; float3 tmp;

   for(int i=0; i<3; i++){
      tmp[i] = targetPos[i] - constPos[i];
      dotp  += tmp[i] * axisz[i];
   }
   for(int i=0; i<3; i++){
      tmp[i] = tmp[i] - dotp*axisz[i];
      dist2 += tmp[i] * tmp[i];
   }
   float dist = sqrt(dist2);

   // FIXME Length should not be determinder automatically
   float radius, length;
   int shapeTypeInt;
   switch(constType){
   case kCTFixedConstraintInt:
   case kCTParentConstraintInt:
      radius = 0.01f;
      length = 0.01f;
      shapeTypeInt = kSpotShapeInt;
      break;
   case kCTLineConstraintInt:
      radius = 0.01f;
      length = 1.0f;
      shapeTypeInt = kLineShapeInt;
      break;
   case kCTCircleConstraintInt:
      radius = dist;
      length = 0.01f;
      shapeTypeInt = kCircleShapeInt;
      break;
   case kCTCylinderConstraintInt:
      radius = dist;
      length = 1.0f;
      shapeTypeInt = kCylinderShapeInt;
      break;
   }
   buildCylinder(radius, length, 32, geomPtr->vertices,
                 geomPtr->face_counts, geomPtr->face_connects, geomPtr->normals);
   return MS::kSuccess;
}

MStatus ConstraintComputeNode::
setDependentsDirty(const MPlug& plug, MPlugArray& affectedPlugs){
   // if(plug == constTypeAttr) affectedPlugs.append(MPlug(thisMObject(),shapeTypeAttr));
   return MS::kSuccess;
}

MStatus ConstraintComputeNode::compute(const MPlug &plug, MDataBlock &dataBlock) {
   // World position and constraintType define radius
   MStatus ms;
   if(plug == outSurfAttr) {
      MFnPluginData fnDataCreator;
      MTypeId tmpId(apiMeshData::id);
      fnDataCreator.create(tmpId, &ms);
      MCHECKERROR(ms, "compute : error creating apiMeshData")
         apiMeshData * newData = (apiMeshData*)fnDataCreator.data(&ms);

      MCHECKERROR(ms, "compute : error gettin at proxy apiMeshData object")
         apiMeshGeom * geomPtr = newData->fGeometry;
      bool hasHistory = computeInputMesh(plug, dataBlock,
                                         geomPtr->vertices,
                                         geomPtr->face_counts,
                                         geomPtr->face_connects,
                                         geomPtr->normals);

      if(!hasHistory){
         ms = computeShape(dataBlock, geomPtr);
         CHECK_MSTATUS(ms);
         // Set output attributes
      }
      geomPtr->faceCount = geomPtr->face_connects.length();

      MDataHandle outSurfHandle = dataBlock.outputValue(outSurfAttr, &ms);
      CHECK_MSTATUS(ms);
      outSurfHandle.set(newData);
      dataBlock.setClean(plug);
      return MS::kSuccess;
   }

   return MS::kUnknownParameter;
}
MStatus ConstraintComputeNode::
computeInputMesh(const MPlug&		plug,
                 MDataBlock&		datablock,
                 MPointArray&		vertices,
                 MIntArray&			counts,
                 MIntArray&			connects,
                 MVectorArray&	normals)
// Description
//
//     This function takes an input surface of type kMeshData and converts
//     the geometry into this nodes attributes.
//     Returns kFailure if nothing is connected.
{
   MStatus stat;

   // Get the input subdiv
   MDataHandle inputData = datablock.inputValue(inputMesh, &stat);
   MCHECKERROR(stat, "compute get inputMesh")
      MObject surf = inputData.asMesh();

   // Check if anything is connected
   //
   MObject thisObj = thisMObject();
   MPlug surfPlug(thisObj, inputMesh);
   if (!surfPlug.isConnected()) {
    	stat = datablock.setClean(plug);
	    MCHECKERROR(stat, "compute setClean")
         return MS::kFailure;
   }

   // Extract the mesh data
   //
   MFnMesh surfFn (surf, &stat);
   MCHECKERROR(stat, "compute - MFnMesh error");
   stat = surfFn.getPoints(vertices, MSpace::kObject);
   MCHECKERROR(stat, "compute getPoints");

   for (int i=0; i<surfFn.numPolygons(); i++)
      {
         MIntArray polyVerts;
         surfFn.getPolygonVertices(i, polyVerts);
         int pvc = polyVerts.length();
         counts.append(pvc);
         for (int v=0; v<pvc; v++) {
            connects.append(polyVerts[v]);
         }
      }

   for (int n=0; n<(int)vertices.length(); n++)
      {
         MVector normal;
         surfFn.getVertexNormal(n, normal);
         normals.append(normal);
      }

   return MS::kSuccess;
}

bool ConstraintComputeNode::isBounded() const {return true;}
void* ConstraintComputeNode::creator() {return new ConstraintComputeNode();}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Plugin Registration
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MStatus ConstraintComputeNode::initialize() {
   MFnNumericAttribute nAttr;
   MFnMatrixAttribute  mAttr;
   MStatus             ms;

   ms = createConstTypeAttr(constTypeAttr);
   CHECK_MSTATUS_AND_RETURN_IT(ms);

   /** Output attributes **/
   // Shape type attribute
   constTransAttr = mAttr.create("ConstraintTransform", "cxform",
                                 MFnMatrixAttribute::Type::kDouble, &ms);
   targetPosAttr = nAttr.createPoint("TargetPosition", "tpos", &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
//    MFnTypedAttribute typedAttr;
//    outSurfAttr = typedAttr.create(kAttrConstCompOutSurf, kAttrConstCompOutSurfShort,
//                                   apiMeshData::id,
//                                   MObject::kNullObj, &ms);
//    MCHECKERROR(ms, "create outSurfAttr attribute")
//       typedAttr.setWritable(false);

   MAKE_TYPED_ATTR(outSurfAttr, kAttrConstCompOutSurf, kAttrConstCompOutSurfShort, MFnData::kMesh, NULL);
   outSurfAttr_fn.setWritable(false);
   MAKE_TYPED_ATTR(inputMesh, "inputMesh", "im", MFnData::kMesh, NULL);

   ADD_ATTRIBUTE(targetPosAttr);
   ADD_ATTRIBUTE(constTypeAttr);
   ADD_ATTRIBUTE(constTransAttr);

   ATTRIBUTE_AFFECTS(inputMesh, outSurfAttr);
   ATTRIBUTE_AFFECTS(targetPosAttr, outSurfAttr);
   ATTRIBUTE_AFFECTS(constTransAttr, outSurfAttr);
   ATTRIBUTE_AFFECTS(constTypeAttr, outSurfAttr);

   return MS::kSuccess;
}

MStatus initializePlugin(MObject obj) {
   MStatus   status;
   MFnPlugin plugin(obj, "Prashant Sachdeva", "0.1", "Any");

   status = plugin.registerNode("ConstraintComputeNode",
                                ConstraintComputeNode::id,
                                &ConstraintComputeNode::creator,
                                &ConstraintComputeNode::initialize,
                                MPxNode::kDependNode,
                                &ConstraintComputeNode::drawDbClassification);
   if(!status){status.perror("registerNode"); return status;}
   return MS::kSuccess;
}

MStatus uninitializePlugin(MObject obj){
   MStatus   status;
   MFnPlugin plugin(obj);

   status = plugin.deregisterNode(ConstraintComputeNode::id);
   if (!status) {status.perror("deregisterNode"); return status;}
   return MS::kSuccess;
}
