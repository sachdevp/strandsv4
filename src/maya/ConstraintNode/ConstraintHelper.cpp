#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnNumericData.h>
#include <MayaStrings.h>
#include <maya/MString.h>
#include <SceneConstants.hpp>

MStatus createShapeAttrs(MObject &shapeAttr, MObject &radiusAttr, MObject &lengthAttr, bool output){
   MStatus ms;
   MFnEnumAttribute eAttr;
   MFnNumericAttribute nAttr;
   shapeAttr = eAttr.create(kAttrShapeTypeLong, kAttrShapeTypeShort, 0, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   eAttr.addField(kSpotShape, kSpotShapeInt);
   eAttr.addField(kLineShape, kLineShapeInt);
   eAttr.addField(kCircleShape, kCircleShapeInt);
   eAttr.addField(kCylinderShape, kCylinderShapeInt);
   CHECK_MSTATUS(eAttr.setHidden(false));
   // CHECK_MSTATUS(eAttr.setKeyable(true));
   // Output should not be written
   // CHECK_MSTATUS(eAttr.setWritable(!output));

   // Radius
   radiusAttr = nAttr.create("Radius", "rad", MFnNumericData::kDouble, 0.01, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   CHECK_MSTATUS(nAttr.setHidden(false));
   // CHECK_MSTATUS(nAttr.setKeyable(true));
   // CHECK_MSTATUS(nAttr.setWritable(!output));

   // Length
   lengthAttr = nAttr.create("Length", "len", MFnNumericData::kDouble, 0.01, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   CHECK_MSTATUS(nAttr.setHidden(false));
   // CHECK_MSTATUS(nAttr.setKeyable(true));
   // CHECK_MSTATUS(nAttr.setWritable(!output));

   return MS::kSuccess;
}

MStatus createConstTypeAttr(MObject &constTypeAttr){
   MStatus ms;
   MFnEnumAttribute eAttr;
   constTypeAttr = eAttr.create(kAttrConstTypeLong, kAttrConstTypeShort, kCTParentConstraintInt, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   eAttr.addField(kCTFixedConstraint,         kCTFixedConstraintInt);
   eAttr.addField(kCTParentConstraint,        kCTParentConstraintInt);
   eAttr.addField(kCTLineConstraint,          kCTLineConstraintInt);
   eAttr.addField(kCTCircleConstraint,        kCTCircleConstraintInt);
   ms = eAttr.addField(kCTCylinderConstraint, kCTCylinderConstraintInt);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   return MS::kSuccess;
}
