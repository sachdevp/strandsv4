#include<maya/MPxNode.h>
#include<maya/MPxLocatorNode.h>

class apiMeshGeom;
class ConstraintComputeNode : public MPxNode{
public:
   ConstraintComputeNode();
   virtual ~ConstraintComputeNode();
   virtual MStatus compute(const MPlug& plug, MDataBlock& data);
   virtual bool    isBounded() const;
   virtual MStatus setDependentsDirty(const MPlug& plug, MPlugArray& affectedPlugs);

   static void*   creator();
   static MStatus initialize();

   // Input Attributes
   static MObject targetPosAttr;      // Position of target
   static MObject constTypeAttr;      // Constraint type to render
   static MObject constTransAttr;

   // Ouput attribute
   static MObject outSurfAttr;
   static MObject inputMesh;

   // Special functions for constraint shape computation
   MStatus computeShape(MDataBlock &dataBlock, apiMeshGeom* geomPtr);
   static void buildCylinder(double         rad,
                             double         length,
                             int            div,
                             MPointArray &  vertices,
                             MIntArray &    counts,
                             MIntArray &    connects,
                             MVectorArray & normals);
   MStatus computeInputMesh(const MPlug&		plug,
                            MDataBlock&		datablock,
                            MPointArray&		vertices,
                            MIntArray&			counts,
                            MIntArray&			connects,
                            MVectorArray&	normals);

public:
   static MTypeId id;
   static MString drawDbClassification;
   static MString drawRegistrantId;
};

/** MPxLocatorNode extension to shape of constraint node **/
class ConstraintShapeNode:public MPxLocatorNode{
public:
   ConstraintShapeNode();
   virtual ~ConstraintShapeNode();
   virtual MStatus      compute(const MPlug& plug, MDataBlock& data);
   virtual void         draw(M3dView &view, const MDagPath& path,
                             M3dView::DisplayStyle style,
                             M3dView::DisplayStatus status);
   virtual bool         isBounded() const;
   virtual MBoundingBox boundingBox() const;

   static void*   creator();
   static MStatus initialize();

   // Attributes
   static MObject transparencyAttr;   // Required to allow better control on visualization
   static MObject constTransAttr;
   static MObject lengthAttr;
   static MObject radiusAttr;
   static MObject shapeTypeAttr;

   MStatus setDependentsDirty(const MPlug& plug, MPlugArray& plugArray);

public:
   static MTypeId id;
   static MString drawDbClassification;
   static MString drawRegistrantId;

private:
   float radius, length;
};
