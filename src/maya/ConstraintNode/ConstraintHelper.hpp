#pragma once
MStatus createShapeAttrs(MObject &shapeAttr, MObject &radiusAttr, MObject &lengthAttr, bool output=false);
MStatus createConstTypeAttr(MObject &constTypeAttr);
