/**
   @auth: Prashant Sachdeva (sachdevp@cs.ubc.ca)
   @desc: Locator node implementation for constraints based on footPrintNode.cpp
*/

//- Original copyright:
// ==========================================================================
// Copyright 2015 Autodesk, Inc.  All rights reserved.
// Use of this software is subject to the terms of the Autodesk license agreement
// provided at the time of installation or download, or which otherwise
// accompanies this software in either electronic or hard copy form.
// ==========================================================================
//+

////////////////////////////////////////////////////////////////////////
// NOTE: For original Autodesk documentation, check footPrintNode.cpp available
// in the footPrint plugin in the Maya 2016.5 devkit
//
//
// This plug-in demonstrates how to draw a simple mesh like foot Print in an
// easy way.
//
// This easy way is supported in Viewport 2.0. In Viewport 2.0, MUIDrawManager
// can used to draw simple UI elements in method addUIDrawables().
//
// For comparison, you can reference a Maya Developer Kit sample named
// rawfootPrintNode. In that sample, we draw the footPrint with OpenGL\DX in
// method rawFootPrintDrawOverride::draw().
//
// Note the method
//   footPrint::draw()
// is only called in legacy default viewport to draw foot Print.
// while the methods
//   FootPrintDrawOverride::prepareForDraw()
//   FootPrintDrawOverride::addUIDrawables()
// are only called in Viewport 2.0 to prepare and draw foot Print.
//
////////////////////////////////////////////////////////////////////////

#include <maya/MPxLocatorNode.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MMatrix.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>
#include <maya/M3dView.h>
#include <maya/MFnPlugin.h>
#include <maya/MDistance.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnNumericData.h>
#include <maya/MVectorArray.h>

// Viewport 2.0 includes
#include <maya/MDrawRegistry.h>
#include <maya/MPxDrawOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawContext.h>
#include <maya/MHWGeometryUtilities.h>
#include <maya/MPointArray.h>
#include <maya/MGlobal.h>
#include <maya/MEventMessage.h>
#include <maya/MFnDependencyNode.h>

#include <cassert>

#include <MayaStrings.h>
#include <MayaHelper.hpp>
#include <SceneConstants.hpp>

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Node implementation with standard viewport draw
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

class ConstraintShapeNode : public MPxLocatorNode{
public:
   ConstraintShapeNode();
   virtual ~ConstraintShapeNode();
   virtual MStatus         compute(const MPlug& plug, MDataBlock& data);
   virtual void            draw(M3dView &view, const MDagPath& path,
                                M3dView::DisplayStyle style,
                                M3dView::DisplayStatus status);
   virtual bool            isBounded() const;
   virtual MBoundingBox    boundingBox() const;

   static void*   creator();
   static MStatus initialize();

   // Attributes
   static MObject transparencyAttr;   // Required to allow better control on visualization
   static MObject sizeAttr;           // Interpreted differently for different constraints
   static MObject constraintTypeAttr; // Constraint type to render
   static MObject targetPosAttr;      // Position of target
   static MObject constraintTransformAttr;
   static MObject lengthAttr;
   static MObject radiusAttr;

   // Special functions for constraint shape computation
   MStatus computeShape(MDataBlock &dataBlock);
   static void buildCylinder(double         rad,
                             double         length,
                             int            div,
                             MPointArray &  vertices,
                             MIntArray &    counts,
                             MIntArray &    connects,
                             MVectorArray & normals);
   MStatus setDependentsDirty(const MPlug& plug, MPlugArray& plugArray);
public:
   static    MTypeId        id;
   static    MString        drawDbClassification;
   static    MString        drawRegistrantId;
private:
   float radius, length;
};

MObject ConstraintShapeNode::transparencyAttr;
MObject ConstraintShapeNode::sizeAttr;
MObject ConstraintShapeNode::constraintTypeAttr;
MObject ConstraintShapeNode::targetPosAttr;
MObject ConstraintShapeNode::constraintTransformAttr;
MObject ConstraintShapeNode::radiusAttr;
MObject ConstraintShapeNode::lengthAttr;

MTypeId ConstraintShapeNode::id(0x80007);
MString    ConstraintShapeNode::drawDbClassification("drawdb/geometry/ConstraintShapeNode");
MString    ConstraintShapeNode::drawRegistrantId("ConstraintShapeNodePlugin");

ConstraintShapeNode::ConstraintShapeNode(){}
ConstraintShapeNode::~ConstraintShapeNode(){}

void ConstraintShapeNode::
buildCylinder(double        rad,
              double        length,
              int           div,
              MPointArray  &vertices,
              MIntArray    &counts,
              MIntArray    &connects,
              MVectorArray &normals){
   double u_delta = 2*M_PI / ((double)div);

   MPoint topCenter(0.0, 0.0,  length/2.0);
   MPoint botCenter(0.0, 0.0, -length/2.0);
   MPoint center = botCenter;

   // Build the vertex and normal table
   double u = 0;
   for(int i=0;i<2;i++){
      u = -M_PI;
      for (int j=0; j<div; j++) {
         double x = rad * cos(u);
         double y = rad * sin(u);
         MPoint offset(x, y, 0.0);
         vertices.append(center + offset);
         normals.append(MVector(offset));
         u += u_delta;
      }
      center = topCenter;
   }

   // Create the connectivity lists
   for(int i=0; i<div; i++){
      counts.append  (4);
      connects.append(i);
      connects.append(i+div);
      connects.append(i == (div-1) ? div : (i+div+1));
      connects.append(i == (div-1) ? 0 : i+1);
   }
}

MStatus ConstraintShapeNode::computeShape(MDataBlock &dataBlock){
   MDataHandle sizeHandle = dataBlock.inputValue(sizeAttr);
   double shape_size = sizeHandle.asDouble();
   MDataHandle typeHandle = dataBlock.inputValue(constraintTypeAttr);
   short constraint_type = typeHandle.asShort();
   MDataHandle targetHandle = dataBlock.inputValue(targetPosAttr);
   float3& targetPos = targetHandle.asFloat3();
   MDataHandle constraintTransformHandle = dataBlock.inputValue(constraintTransformAttr);
   MMatrix& transform = constraintTransformHandle.asMatrix();
   float3 axisz, constPos;
   axisz[0] = transform[2][0];
   axisz[1] = transform[2][1];
   axisz[2] = transform[2][2];

   constPos[0] = transform[3][0];
   constPos[1] = transform[3][1];
   constPos[2] = transform[3][2];

   // Interpret here how the constraint is drawn
   float dist2=0;
   float3 tmp;
   float dotp=0;

   for(int i=0; i<3; i++){
      tmp[i] = targetPos[i] - constPos[i];
      dotp  += tmp[i] * axisz[i];
   }
   for(int i=0; i<3; i++){
      tmp[i] = tmp[i] - dotp*axisz[i];
      dist2 += tmp[i] * tmp[i];
   }
   float dist = sqrt(dist2);

   switch(constraint_type){
   case kCTFixedConstraintInt:
   case kCTParentConstraintInt:
      radius = 0.01;
      length = 0.01;
      break;
   case kCTLineConstraintInt:
      radius = 0.01;
      length = shape_size;
      break;
   case kCTCircleConstraintInt:
      radius = dist;
      length = 0.01;
      break;
   case kCTCylinderConstraintInt:
      radius = dist;
      length = shape_size;
      break;
   }
   MStatus ms;
   MDataHandle radiusHandle = dataBlock.outputValue(radiusAttr, &ms);
   CHECK_MSTATUS(ms);
   radiusHandle.set(radius);
   MDataHandle lengthHandle = dataBlock.outputValue(lengthAttr, &ms);
   CHECK_MSTATUS(ms);
   lengthHandle.set(length);
   return MS::kSuccess;
}

MStatus ConstraintShapeNode::compute(const MPlug &plug, MDataBlock &dataBlock) {
   // World position and constraintType define radius
   return computeShape(dataBlock);
   return MS::kSuccess;
}

MStatus ConstraintShapeNode::setDependentsDirty(const MPlug& plug, MPlugArray& plugArray){
   if(plug == constraintTransformAttr ||
      plug == targetPosAttr ||
      plug == constraintTypeAttr ){
      MHWRender::MRenderer::setGeometryDrawDirty(thisMObject());
   }
   return MS::kSuccess;
}

// called by legacy default viewport
void ConstraintShapeNode::draw(M3dView &view, const MDagPath &path,
                          M3dView::DisplayStyle style,
                          M3dView::DisplayStatus status){
   // Get data for draw
   MObject thisNode = thisMObject();
   MPlug sizePlug(thisNode, sizeAttr);
   MPlug transparencyPlug(thisNode, transparencyAttr);
   MPlug radiusPlug(thisNode, radiusAttr);
   MPlug lengthPlug(thisNode, lengthAttr);
   MDistance sizeVal;
   sizePlug.getValue(sizeVal);
   float alpha;
   transparencyPlug.getValue(alpha);
   // float multiplier = (float) sizeVal.asCentimeters();

   MPointArray vertices;
   MIntArray counts, connects;
   MVectorArray normals;
   float radius2 = radiusPlug.asFloat();
   float length2 = lengthPlug.asFloat();
   buildCylinder(radius, length, 32, vertices, counts, connects, normals);
   view.beginGL();
   if((style == M3dView::kFlatShaded) || (style == M3dView::kGouraudShaded)){
      glPushAttrib(GL_CURRENT_BIT);

      if(status == M3dView::kActive){view.setDrawColor(2, M3dView::kActiveColors);}
      else{view.setDrawColor(2, M3dView::kDormantColors);}

      int faceCount = counts.length();
      int vid = 0;
      for(int i=0; i<faceCount; i++){
         glBegin(GL_POLYGON);
         for(int j=0; j<counts[i]; j++){
            MPoint vertex = vertices[connects[vid]];
            MVector normal = normals[connects[vid]];
            glNormal3f((float)normal[0], normal[1], normal[2]);
            glVertex3f((float)vertex[0], vertex[1], vertex[2]);
            vid++;
         }
         glEnd();
      }
      glPopAttrib();
   }
   view.endGL();

   // Draw the name of the ConstraintShapeNode
   view.setDrawColor(MColor(0.1f, 0.8f, 0.8f, 1.0f));
   view.drawText(MString("ConstraintShapeNode"), MPoint(0.0, 0.0, 0.0), M3dView::kCenter);
}

bool ConstraintShapeNode::isBounded() const {return true;}

MBoundingBox ConstraintShapeNode::boundingBox() const {
   // TODO To be implemented
   MObject thisNode = thisMObject();
   MPlug plug(thisNode, sizeAttr);
   MDistance sizeVal;
   plug.getValue(sizeVal);

   double multiplier = sizeVal.asCentimeters();

   MPoint corner1(-0.17, 0.0, -0.7);
   MPoint corner2(0.17, 0.0, 0.3);

   corner1 = corner1 * multiplier;
   corner2 = corner2 * multiplier;

   return MBoundingBox(corner1, corner2);
}

void* ConstraintShapeNode::creator() {return new ConstraintShapeNode();}

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Viewport 2.0 override implementation
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

// class ConstraintShapeNodeData : public MUserData
// {
// public:
//    ConstraintShapeNodeData() : MUserData(false) {} // don't delete after draw
//    virtual ~ConstraintShapeNodeData() {}

//    MColor fColor;
//    MPointArray fSoleLineList;
//    MPointArray fSoleTriangleList;
//    MPointArray fHeelLineList;
//    MPointArray fHeelTriangleList;
// };

// class ConstraintShapeNodeDrawOverride : public MHWRender::MPxDrawOverride
// {
// public:
//    static MHWRender::MPxDrawOverride* Creator(const MObject& obj)
//    {
//       return new ConstraintShapeNodeDrawOverride(obj);
//    }

//    virtual ~ConstraintShapeNodeDrawOverride();

//    virtual MHWRender::DrawAPI supportedDrawAPIs() const;

//    virtual bool isBounded(
//                           const MDagPath& objPath,
//                           const MDagPath& cameraPath) const;

//    virtual MBoundingBox boundingBox(
//                                     const MDagPath& objPath,
//                                     const MDagPath& cameraPath) const;

//    virtual MUserData* prepareForDraw(
//                                      const MDagPath& objPath,
//                                      const MDagPath& cameraPath,
//                                      const MHWRender::MFrameContext& frameContext,
//                                      MUserData* oldData);

//    virtual bool hasUIDrawables() const { return true; }

//    virtual void addUIDrawables(
//                                const MDagPath& objPath,
//                                MHWRender::MUIDrawManager& drawManager,
//                                const MHWRender::MFrameContext& frameContext,
//                                const MUserData* data);

//    virtual bool traceCallSequence() const
//    {
//       // Return true if internal tracing is desired.
//       return false;
//    }
//    virtual void handleTraceMessage(const MString &message) const
//    {
//       MGlobal::displayInfo("ConstraintShapeNodeDrawOverride: " + message);

//       // Some simple custom message formatting.
//       fprintf(stderr, "ConstraintShapeNodeDrawOverride: ");
//       fprintf(stderr, message.asChar());
//       fprintf(stderr, "\n");
//    }


// private:
//    ConstraintShapeNodeDrawOverride(const MObject& obj);
//    float getMultiplier(const MDagPath& objPath) const;

//    static void OnModelEditorChanged(void *clientData);

//    ConstraintShapeNode*  fConstraintShapeNode;
//    MCallbackId fModelEditorChangedCbId;
// };

// // By setting isAlwaysDirty to false in MPxDrawOverride constructor, the
// // draw override will be updated (via prepareForDraw()) only when the node
// // is marked dirty via DG evaluation or dirty propagation. Additional
// // callback is also added to explicitly mark the node as being dirty (via
// // MRenderer::setGeometryDrawDirty()) for certain circumstances. Note that
// // the draw callback in MPxDrawOverride constructor is set to NULL in order
// // to achieve better performance.
// ConstraintShapeNodeDrawOverride::ConstraintShapeNodeDrawOverride(const MObject& obj)
//    : MHWRender::MPxDrawOverride(obj, NULL, false)
// {
//    fModelEditorChangedCbId = MEventMessage::addEventCallback(
//                                                              "modelEditorChanged", OnModelEditorChanged, this);

//    MStatus status;
//    MFnDependencyNode node(obj, &status);
//    fConstraintShapeNode = status ? dynamic_cast<ConstraintShapeNode*>(node.userNode()) : NULL;
// }

// ConstraintShapeNodeDrawOverride::~ConstraintShapeNodeDrawOverride()
// {
//    fConstraintShapeNode = NULL;

//    if (fModelEditorChangedCbId != 0)
//       {
//          MMessage::removeCallback(fModelEditorChangedCbId);
//          fModelEditorChangedCbId = 0;
//       }
// }

// void ConstraintShapeNodeDrawOverride::OnModelEditorChanged(void *clientData)
// {
//    // Mark the node as being dirty so that it can update on display appearance
//    // switch among wireframe and shaded.
//    ConstraintShapeNodeDrawOverride *ovr = static_cast<ConstraintShapeNodeDrawOverride*>(clientData);
//    if (ovr && ovr->fConstraintShapeNode)
//       {
//          MHWRender::MRenderer::setGeometryDrawDirty(ovr->fConstraintShapeNode->thisMObject());
//       }
// }

// MHWRender::DrawAPI ConstraintShapeNodeDrawOverride::supportedDrawAPIs() const
// {
//    // this plugin supports both GL and DX
//    return (MHWRender::kOpenGL | MHWRender::kDirectX11 | MHWRender::kOpenGLCoreProfile);
// }

// float ConstraintShapeNodeDrawOverride::getMultiplier(const MDagPath& objPath) const
// {
//    // Retrieve value of the size attribute from the node
//    MStatus status;
//    MObject footprintNode = objPath.node(&status);
//    if (status)
//       {
//          MPlug plug(footprintNode, ConstraintShapeNode::size);
//          if (!plug.isNull())
//             {
//                MDistance sizeVal;
//                if (plug.getValue(sizeVal))
//                   {
//                      return (float)sizeVal.asCentimeters();
//                   }
//             }
//       }

//    return 1.0f;
// }

// bool ConstraintShapeNodeDrawOverride::isBounded(const MDagPath& /*objPath*/,
//                                            const MDagPath& /*cameraPath*/) const
// {
//    return true;
// }

// MBoundingBox ConstraintShapeNodeDrawOverride::boundingBox(
//                                                      const MDagPath& objPath,
//                                                      const MDagPath& cameraPath) const
// {
//    MPoint corner1(-0.17, 0.0, -0.7);
//    MPoint corner2(0.17, 0.0, 0.3);

//    float multiplier = getMultiplier(objPath);
//    corner1 = corner1 * multiplier;
//    corner2 = corner2 * multiplier;

//    return MBoundingBox(corner1, corner2);
// }

// // Called by Maya each time the object needs to be drawn.
// MUserData* ConstraintShapeNodeDrawOverride::prepareForDraw(
//                                                       const MDagPath& objPath,
//                                                       const MDagPath& cameraPath,
//                                                       const MHWRender::MFrameContext& frameContext,
//                                                       MUserData* oldData)
// {
//    // Any data needed from the Maya dependency graph must be retrieved and cached in this stage.
//    // There is one cache data for each drawable instance, if it is not desirable to allow Maya to handle data
//    // caching, simply return null in this method and ignore user data parameter in draw callback method.
//    // e.g. in this sample, we compute and cache the data for usage later when we create the
//    // MUIDrawManager to draw footprint in method addUIDrawables().
//    ConstraintShapeNodeData* data = dynamic_cast<ConstraintShapeNodeData*>(oldData);
//    if (!data)
//       {
//          data = new ConstraintShapeNodeData();
//       }

//    float fMultiplier = getMultiplier(objPath);

//    data->fSoleLineList.clear();
//    for (int i = 0; i < soleCount; i++)
//       {
//          data->fSoleLineList.append(sole[i][0] * fMultiplier, sole[i][1] * fMultiplier, sole[i][2] * fMultiplier);
//       }

//    data->fHeelLineList.clear();
//    for (int i = 0; i < heelCount; i++)
//       {
//          data->fHeelLineList.append(heel[i][0] * fMultiplier, heel[i][1] * fMultiplier, heel[i][2] * fMultiplier);
//       }

//    data->fSoleTriangleList.clear();
//    for (int i = 1; i <= soleCount - 2; i++)
//       {
//          data->fSoleTriangleList.append(sole[0][0] * fMultiplier, sole[0][1] * fMultiplier, sole[0][2] * fMultiplier);
//          data->fSoleTriangleList.append(sole[i][0] * fMultiplier, sole[i][1] * fMultiplier, sole[i][2] * fMultiplier);
//          data->fSoleTriangleList.append(sole[i+1][0] * fMultiplier, sole[i+1][1] * fMultiplier, sole[i+1][2] * fMultiplier);
//       }

//    data->fHeelTriangleList.clear();
//    for (int i = 1; i <= heelCount - 2; i++)
//       {
//          data->fHeelTriangleList.append(heel[0][0] * fMultiplier, heel[0][1] * fMultiplier, heel[0][2] * fMultiplier);
//          data->fHeelTriangleList.append(heel[i][0] * fMultiplier, heel[i][1] * fMultiplier, heel[i][2] * fMultiplier);
//          data->fHeelTriangleList.append(heel[i+1][0] * fMultiplier, heel[i+1][1] * fMultiplier, heel[i+1][2] * fMultiplier);
//       }

//    // get correct color based on the state of object, e.g. active or dormant
//    data->fColor = MHWRender::MGeometryUtilities::wireframeColor(objPath);

//    return data;
// }

// // addUIDrawables() provides access to the MUIDrawManager, which can be used
// // to queue up operations for drawing simple UI elements such as lines, circles and
// // text. To enable addUIDrawables(), override hasUIDrawables() and make it return true.
// void ConstraintShapeNodeDrawOverride::addUIDrawables(
//                                                 const MDagPath& objPath,
//                                                 MHWRender::MUIDrawManager& drawManager,
//                                                 const MHWRender::MFrameContext& frameContext,
//                                                 const MUserData* data)
// {
//    // Get data cached by prepareForDraw() for each drawable instance, then MUIDrawManager 
//    // can draw simple UI by these data.
//    ConstraintShapeNodeData* pLocatorData = (ConstraintShapeNodeData*)data;
//    if (!pLocatorData)
//       {
//          return;
//       }

//    drawManager.beginDrawable();

//    // Draw the foot print solid/wireframe
//    drawManager.setColor(pLocatorData->fColor);
//    drawManager.setDepthPriority(5);

//    if (frameContext.getDisplayStyle() & MHWRender::MFrameContext::kGouraudShaded) {
//       drawManager.mesh(MHWRender::MUIDrawManager::kTriangles, pLocatorData->fSoleTriangleList);
//       drawManager.mesh(MHWRender::MUIDrawManager::kTriangles, pLocatorData->fHeelTriangleList);
//    }

//    drawManager.mesh(MHWRender::MUIDrawManager::kClosedLine, pLocatorData->fSoleLineList);
//    drawManager.mesh(MHWRender::MUIDrawManager::kClosedLine, pLocatorData->fHeelLineList);

//    // Draw a text "Foot"
//    MPoint pos(0.0, 0.0, 0.0); // Position of the text
//    MColor textColor(0.1f, 0.8f, 0.8f, 1.0f); // Text color

//    drawManager.setColor(textColor);
//    drawManager.setFontSize(MHWRender::MUIDrawManager::kSmallFontSize);
//    drawManager.text(pos,  MString("Footprint"), MHWRender::MUIDrawManager::kCenter);

//    drawManager.endDrawable();
// }

//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// Plugin Registration
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------

MStatus ConstraintShapeNode::initialize() {
   MFnUnitAttribute    unitFn;
   MFnNumericAttribute nAttr;
   MFnMatrixAttribute mAttr;
   MFnEnumAttribute    eAttr;
   MStatus             stat;

   sizeAttr = unitFn.create("Size", "sz", MFnUnitAttribute::kDistance);
   unitFn.setDefault(1.0);
   // Setting transparency default to 0.5.
   // FIXME transparency limits
   transparencyAttr = nAttr.create("Transparency", "tr", MFnNumericData::kDouble, 0.5, &stat);
   CHECK_MSTATUS_AND_RETURN_IT(stat);
   constraintTypeAttr = eAttr.create(kAttrConstTypeLong, kAttrConstTypeShort, 0, &stat);
   constraintTypeAttr= eAttr.create(kAttrConstTypeLong, kAttrConstTypeShort, 0, &stat);
   CHECK_MSTATUS_AND_RETURN_IT(stat);
   eAttr.addField(kCTFixedConstraint, kCTFixedConstraintInt);
   eAttr.addField(kCTParentConstraint, kCTParentConstraintInt);
   eAttr.addField(kCTLineConstraint, kCTLineConstraintInt);
   eAttr.addField(kCTCircleConstraint, kCTCircleConstraintInt);
   eAttr.addField(kCTCylinderConstraint, kCTCylinderConstraintInt);
   CHECK_MSTATUS(eAttr.setHidden(false));
   CHECK_MSTATUS(eAttr.setKeyable(true));
   radiusAttr = nAttr.create("Radius", "rad", MFnNumericData::kDouble, 0.01, &stat);
   CHECK_MSTATUS_AND_RETURN_IT(stat);
   CHECK_MSTATUS(nAttr.setHidden(true));
   CHECK_MSTATUS(nAttr.setWritable(false));
   lengthAttr = nAttr.create("Length", "len", MFnNumericData::kDouble, 0.01, &stat);
   CHECK_MSTATUS_AND_RETURN_IT(stat);
   CHECK_MSTATUS(nAttr.setHidden(true));
   CHECK_MSTATUS(nAttr.setWritable(false));

   constraintTransformAttr = mAttr.create("ConstraintTransform", "cxform",
                                          MFnMatrixAttribute::Type::kDouble, &stat);
   CHECK_MSTATUS_AND_RETURN_IT(stat);
   targetPosAttr = nAttr.createPoint("TargetPosition", "tpos", &stat);
   CHECK_MSTATUS_AND_RETURN_IT(stat);

   ADD_ATTRIBUTE(sizeAttr);
   ADD_ATTRIBUTE(transparencyAttr);
   ADD_ATTRIBUTE(constraintTypeAttr);
   ADD_ATTRIBUTE(targetPosAttr);
   ADD_ATTRIBUTE(constraintTransformAttr);
   ADD_ATTRIBUTE(radiusAttr);
   ADD_ATTRIBUTE(lengthAttr);

   ATTRIBUTE_AFFECTS(constraintTransformAttr, radiusAttr);
   ATTRIBUTE_AFFECTS(constraintTransformAttr, lengthAttr);
   ATTRIBUTE_AFFECTS(targetPosAttr, radiusAttr);
   ATTRIBUTE_AFFECTS(targetPosAttr, lengthAttr);
   ATTRIBUTE_AFFECTS(constraintTypeAttr, radiusAttr);
   ATTRIBUTE_AFFECTS(constraintTypeAttr, lengthAttr);

   return MS::kSuccess;
}

MStatus initializePlugin(MObject obj) {
   MStatus   status;
   MFnPlugin plugin(obj, "Prashant Sachdeva", "0.1", "Any");

   status = plugin.registerNode("ConstraintShapeNode",
                                ConstraintShapeNode::id,
                                &ConstraintShapeNode::creator,
                                &ConstraintShapeNode::initialize,
                                MPxNode::kLocatorNode,
                                &ConstraintShapeNode::drawDbClassification);
   if(!status){status.perror("registerNode"); return status;}

   // TODO DrawOverride needs to be fixed
   // status = MHWRender::MDrawRegistry::
   //    registerDrawOverrideCreator(ConstraintShapeNode::drawDbClassification,
   //                                ConstraintShapeNode::drawRegistrantId,
   //                                ConstraintShapeNodeDrawOverride::Creator);
   // if(!status){status.perror("registerDrawOverrideCreator"); return status;}
   return status;
}

MStatus uninitializePlugin(MObject obj){
   MStatus   status;
   MFnPlugin plugin(obj);

   status = MHWRender::MDrawRegistry::
      deregisterDrawOverrideCreator(ConstraintShapeNode::drawDbClassification,
                                    ConstraintShapeNode::drawRegistrantId);
   if(!status){status.perror("deregisterDrawOverrideCreator"); return status;}

   status = plugin.deregisterNode(ConstraintShapeNode::id);
   if (!status) {status.perror("deregisterNode"); return status;}
   return status;
}
