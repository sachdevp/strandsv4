//
//  Copyright 2012 Autodesk, Inc.  All rights reserved.
//
//  Use of this software is subject to the terms of the Autodesk license
//  agreement provided at the time of installation or download, or which
//  otherwise accompanies this software in either electronic or hard copy form.
//

//-----------------------------------------------------------------------------
//- StrandMaya.cpp : Initialization functions
//-----------------------------------------------------------------------------
#define MNoVersionString
#include <maya/MFnPlugin.h>

#include "StrandMayaIterator.h"
#include "StrandMayaShape.h"
#include "StrandMayaShapeUI.h"
#include <include_common.hpp>

MString   StrandMaya::drawDbClassification("drawdb/geometry/strand");
MString   StrandMaya::drawRegistrantId("StrandPlugin");

//-----------------------------------------------------------------------------
MStatus initializePlugin (MObject obj) {
#if !defined(WIN32)
#pragma EXPORT
#endif
   //   Description:
   //      this method is called when the plug-in is loaded into Maya. It
   //      registers all of the services that this plug-in provides with
   //      Maya.
   //   Arguments:
   //      obj - a handle to the plug-in object (use MFnPlugin to access it)
   MFnPlugin plugin (obj, "PS", "0.01", "Any") ;

   log_err("Initializing hardware renderer.");
   MStatus ms;

   //   MStatus ms = MHWRender::MDrawRegistry::
   //      registerGeometryOverrideCreator(StrandMaya::drawDbClassification,
   //                                      StrandMaya::drawRegistrantId,
   //                                      StrandMayaGeometryOverride::Creator);
   // if (MFAIL(ms)) {
   //      ms.perror("registerDrawOverrideCreator");
   //      return ms;
   // }


   //- Examples:
   //NodeRegisterOk(plugin.registerNode (_T("myNode"), myNode::id, myNode::creator, myNode::initialize)) ;
   //NodeRegisterOk(myNode::registerMe (plugin)) ;

   //-{{MAYA_REGISTER
   //-MAYA_REGISTER}}
   ms = StrandMaya::registerMe (plugin) ;

   if (MFAIL(ms)) {
      ms.perror("Could not register plugin");
      return ms;
   }

   return (MS::kSuccess) ;
}

MStatus uninitializePlugin (MObject obj) {
#if !defined(WIN32)
#pragma EXPORT
#endif
   //   Description:
   //      this method is called when the plug-in is unloaded from Maya. It 
   //      deregisters all of the services that it was providing.
   //   Arguments:
   //      obj - a handle to the plug-in object (use MFnPlugin to access it)
   MFnPlugin plugin (obj) ;

   //   MStatus ms = MHWRender::MDrawRegistry::
   //      deregisterGeometryOverrideCreator(StrandMaya::drawDbClassification,
   //                                      StrandMaya::drawRegistrantId);
   // if (MFAIL(ms)) {
   //      ms.perror("deregisterDrawOverrideCreator");
   //      return ms;
   // }


   //- Examples:
   //NodeUnregisterOk(plugin.deregisterNode (myNode::id)) ;
   //NodeUnregisterOk(myNode::unregisterMe (plugin)) ;
   StrandMaya::unregisterMe (plugin) ;

   return (MS::kSuccess) ;
}
