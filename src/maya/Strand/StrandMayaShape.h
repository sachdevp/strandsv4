// @auth: Prashant Sachdeva (sachdevp@cs.ubc.ac)
// @desc: Strand object in Maya adapted from Autodesk trial code

// Original copyright:
// Copyright 2012 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk license
// agreement provided at the time of installation or download, or which
// otherwise accompanies this software in either electronic or hard copy form.

#pragma once
#include <include_common.hpp>
#include <maya/MPxSurfaceShape.h>
#include <maya/MBoundingBox.h>
#include <maya/MPointArray.h>
#include <maya/MDataHandle.h>
#define MNoVersionString
#include <maya/MFnPlugin.h>
#include <list>
#include <vector>

// Viewport 2.0 includes
#include <maya/MDrawRegistry.h>
#include <maya/MPxGeometryOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawContext.h>
#include <maya/MShaderManager.h>
#include <maya/MHWGeometry.h>
#include <maya/MHWGeometryUtilities.h>
#include <maya/MPointArray.h>
#define MAX_STRAND_POINTS 30

//-----------------------------------------------------------------------------
class StrandMaya : public MPxSurfaceShape {
 public:
   static const MTypeId id;
   static MObject bboxCorner1;
   static MObject bboxCorner2;
   // static MObject aConstForceAttrObj;
   // static MObject aInexActiveAttrObj;

   static MString drawDbClassification;
   static MString drawRegistrantId;

 private:
   MPointArray fCurvePoints;
   // Order storage as part of the curve's attributes
   static MObject aOrder;
   // static MObject aInextensible;
   // Index and point corresponding to extra points
   static MObject aExtraNodesIdx, aExtraNodesPos;
   uint nCurvePoints;

 public:
   StrandMaya();
   virtual ~StrandMaya();

   // Overrides

   // Bounding box methods
   virtual bool isBounded() const;
   virtual MBoundingBox boundingBox() const;

   static void *creator();
   static MStatus initialize();
   static MStatus registerMe(MFnPlugin &plugin);
   static MStatus unregisterMe(MFnPlugin &plugin);

   virtual bool setInternalValue(const MPlug &, const MDataHandle &);
   virtual bool getInternalValue(const MPlug &plug, MDataHandle &result);
   virtual MStatus compute(const MPlug &plug, MDataBlock &datablock);

   virtual void transformUsing(const MMatrix& mat, const MObjectArray& componentList ){};
   virtual void transformUsing(const MMatrix& mat, const MObjectArray& componentList,
                               MVertexCachingMode cachingMode, MPointArray* pointCache);

   virtual void componentToPlugs(MObject &component, MSelectionList &list) const;
   virtual MatchResult matchComponent(const MSelectionList &item, const MAttributeSpecArray &spec, MSelectionList &list);

   // Associates a user defined iterator with the shape(components)
   virtual MPxGeometryIterator *geometryIteratorSetup(MObjectArray &objArray, MObject &obj, bool forReadOnly = false);
   virtual bool acceptsGeometryIterator(bool writeable = true);
   virtual bool acceptsGeometryIterator(MObject &, bool writeable = true, bool forReadOnly = false);

   // Utility methods
   void points(MPointArray &points);
   MPoint point(unsigned index) const;
   unsigned pointCount() const;
   void setPoint(unsigned index, const MPoint &val);
   /* MStatus addNode(int index, int& nPoints); */
   /* MStatus addNodes(int index, int nNodes, int& nPoints); */
   int getOrderIndex(int index);
   MStatus getPointIds(IVec& pointIds);

   // Added to shut errors about virtual things up
   virtual MStatus dependsOn(const MPlug &plug, const MPlug &otherPlug, bool& depends)const {return MS::kSuccess;};
   virtual bool evalNodeAffectsDrawDb(const MEvaluationNode &evaluationNode){return true;};


 private:
   void setPointValue(const MPlug &plug, const MDataHandle &handle, unsigned vtInd);
   MStatus computeBoundingBox(MDataBlock &block);

};


/** Based on footPrintNode plugin in Maya 2016 dev kit **/

class StrandMayaGeometryOverride :
public MHWRender::MPxGeometryOverride{
 public:
   static MHWRender::MPxGeometryOverride* Creator(const MObject& obj){
      return new StrandMayaGeometryOverride(obj);
   }

   virtual ~StrandMayaGeometryOverride();

   virtual MHWRender::DrawAPI supportedDrawAPIs() const;

   virtual bool hasUIDrawables()const { return false; }

   virtual void updateDG();
   virtual void updateRenderItems(const MDagPath &path,
                                  MHWRender::MRenderItemList& list);
   virtual void populateGeometry(const MHWRender::MGeometryRequirements &requirements,
                                 const MHWRender::MRenderItemList &renderItems,
                                 MHWRender::MGeometry &data);

   virtual bool isIndexingDirty(const MHWRender::MRenderItem &item){ return false; }
   virtual bool isStreamDirty(const MHWRender::MVertexBufferDescriptor &desc)
   { return mMultiplierChanged; }
   virtual void cleanUp(){};

 private:
   StrandMayaGeometryOverride(const MObject& obj);

   MHWRender::MShaderInstance* mSolidUIShader;
   MObject mStrandObj;
   float mMultiplier;
   bool mMultiplierChanged;
   float mStrain = 0.0f;
   float mRadius = 0.01f;
   MPointArray mPoints;
};


