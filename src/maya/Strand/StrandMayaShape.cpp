// @auth: Prashant Sachdeva
// @desc: Main code for the shape StrandMaya

// Original copyright
// Copyright 2012 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk license
// agreement provided at the time of installation or download, or which
// otherwise accompanies this software in either electronic or hard copy form.

#include <cmath>
#include <iterator>
#include <vector>

#include "StrandMayaShape.h"
#include "StrandMayaShapeUI.h"
#include "StrandMayaIterator.h"

#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MAttributeSpecArray.h>
#include <maya/MAttributeSpec.h>
#include <maya/MAttributeIndex.h>
#include <maya/MPlugArray.h>
#include <maya/MPlug.h>
#include <maya/MDagPath.h>
#include <maya/MGlobal.h>

#include <MayaHelper.hpp>
#include <MayaStrings.h>
#include <SceneConstants.hpp>
#include <Strand/StrandMaya_export.hpp>

// Curve attributes
MObject StrandMaya::bboxCorner1;
MObject StrandMaya::bboxCorner2;
MObject StrandMaya::aOrder;
MObject StrandMaya::aExtraNodesIdx;
MObject StrandMaya::aExtraNodesPos;
// MObject StrandMaya::aInextensible;
// MObject StrandMaya::aConstForceAttrObj;
// MObject StrandMaya::aInexActiveAttrObj;

//-----------------------------------------------------------------------------
StrandMaya::StrandMaya(){
   // NOTE No dynamic expansion
   fCurvePoints.setLength(MAX_STRAND_POINTS);
   nCurvePoints = 0;
}

StrandMaya::~StrandMaya(){}

//-----------------------------------------------------------------------------
// Overrides

// Specifies that this object has a boundingBox.
bool StrandMaya::isBounded() const{return(true);}

// Returns the bounding box for this object.
// It is a good idea not to recompute here as this function is called often.
MBoundingBox StrandMaya::boundingBox() const{
   StrandMaya *nonConstThis = const_cast<StrandMaya *>(this);
   MDataBlock block = nonConstThis->forceCache();
   MDataHandle lowerHandle = block.inputValue(bboxCorner1);
   MDataHandle upperHandle = block.inputValue(bboxCorner2);
   double3 &lower = lowerHandle.asDouble3();
   double3 &upper = upperHandle.asDouble3();

   MPoint corner1(lower[0], lower[1], lower[2]);
   MPoint corner2(upper[0], upper[1], upper[2]);
   return(MBoundingBox(corner1, corner2));
}

// When input attributes are dirty this method will be called to
// recompute the output attributes.
//    plug - the attribute that triggered the compute
//    datablock - the nodes data
MStatus StrandMaya::compute(const MPlug &plug, MDataBlock &datablock){
   if(plug == bboxCorner1 ||  plug == bboxCorner2) return(computeBoundingBox(datablock));
   return(MS::kUnknownParameter);
}

// Compute and set the bounding box.
MStatus StrandMaya::computeBoundingBox(MDataBlock &datablock){
   MDataHandle lowerHandle = datablock.outputValue(bboxCorner1);
   MDataHandle upperHandle = datablock.outputValue(bboxCorner2);
   double3 &lower = lowerHandle.asDouble3();
   double3 &upper = upperHandle.asDouble3();

   // This clears any old bbox values
   if(nCurvePoints == 0){
      return MS::kSuccess;
   }
   MPoint tmppnt = fCurvePoints[0];
   lower[0] = tmppnt[0]; lower[1] = tmppnt[1]; lower[2] = tmppnt[2];
   upper[0] = tmppnt[0]; upper[1] = tmppnt[1]; upper[2] = tmppnt[2];
   double x;
   for(uint ii = 1; ii < nCurvePoints; ii++){
      for(unsigned jj = 0; jj < 3; jj++){
         x = fCurvePoints[ii][jj];
         if(x > upper[jj]) upper[jj] = x;
         if(x < lower[jj]) lower[jj] = x;
      }
   }
   lowerHandle.setClean();
   upperHandle.setClean();

   // Signal that the bounding box has changed.
   childChanged(MPxSurfaceShape::kBoundingBoxChanged);
   return(MS::kSuccess);
}

bool StrandMaya::setInternalValue(const MPlug &plug, const MDataHandle &handle){
   bool isOk = true;
   if((plug == mControlPoints)
      ||(plug == mControlValueX)
      ||(plug == mControlValueY)
      ||(plug == mControlValueZ)){
      if(plug == mControlPoints && !plug.isArray()){
         unsigned index = plug.logicalIndex();
         if(index < fCurvePoints.length()){
            double3& ptData = handle.asDouble3();
            fCurvePoints[index].x = ptData[0];
            fCurvePoints[index].y = ptData[1];
            fCurvePoints[index].z = ptData[2];
            fCurvePoints[index].w = 1.0;
         }
      }
      else if(plug == mControlValueX) setPointValue(plug, handle, 0);
      else if(plug == mControlValueY) setPointValue(plug, handle, 1);
      else if(plug == mControlValueZ) setPointValue(plug, handle, 2);
      MDataBlock block = forceCache();
      computeBoundingBox(block);
   }
   isOk = MPxSurfaceShape::setInternalValue(plug, handle);
   return(isOk);
}

// Transforms the given components. This method is used by
// the move, rotate, and scale tools in component mode.
// The bounding box has to be updated here, so do the normals and
// any other attributes that depend on vertex positions.
//     mat - matrix to tranform the components by
//     componentList - list of components to be transformed,
//                     or an empty list to indicate the whole surface
//     cachingMode - how to use the supplied pointCache
//     pointCache - if non-null, save or restore points from this list base
//                  on the cachingMode
void StrandMaya::transformUsing(const MMatrix &mat, const MObjectArray &componentList,
                                MVertexCachingMode cachingMode, MPointArray *pointCache){
   bool savePoints =(cachingMode == MPxSurfaceShape::kSavePoints);
   unsigned int i = 0, j = 0;
   unsigned int len = componentList.length();

   if(cachingMode == MPxSurfaceShape::kRestorePoints){
      // restore the points based on the data in the pointCache attribute
      unsigned int cacheLen = pointCache->length();
      if(len > 0){
         // traverse the component list
         for(i = 0; i < len && j < cacheLen; i++){
            MObject comp = componentList[i];
            MFnSingleIndexedComponent fnComp(comp);
            int elemCount = fnComp.elementCount();
            for(int idx = 0; idx < elemCount && j < cacheLen; idx++, ++j){
               int elemIndex = fnComp.element(idx);
               fCurvePoints[elemIndex] =(*pointCache)[j];
            }
         }
      } else{
         // if the component list is of zero-length, it indicates that we
         // should transform the entire surface
         unsigned idx;
         len = fCurvePoints.length();
         for(idx = 0; idx < len && j < cacheLen; ++idx, ++j){
            fCurvePoints[idx] =(*pointCache)[j];
         }
      }
   } else{
      // Transform the surface vertices with the matrix.
      // If savePoints is true, save the points to the pointCache.
      if(len > 0){
         // Traverse the componentList
         for(i=0; i<len; i++){
            MObject comp = componentList[i];
            MFnSingleIndexedComponent fnComp(comp);
            int elemCount = fnComp.elementCount();
            if(savePoints && i == 0)
               pointCache->setSizeIncrement(elemCount);
            for(int idx = 0; idx < elemCount; idx++){
               int elemIndex = fnComp.element(idx);
               if(savePoints)
                  pointCache->append(fCurvePoints[elemIndex]);
               fCurvePoints[elemIndex] *=mat;
            }
         }
      } else{
         // If the component list is of zero-length, it indicates that we
         // should transform the entire surface
         len = fCurvePoints.length();
         if(savePoints)
            pointCache->setSizeIncrement(len);
         for(unsigned int idx = 0; idx < len; ++idx){
            if(savePoints)
               pointCache->append(fCurvePoints[idx]);
            fCurvePoints[idx] *=mat;
         }
      }
   }

   // Update the bounding box
   MDataBlock block = forceCache();
   computeBoundingBox(block);
}

// Handle internal attributes.
//
// Attributes that require special storage, bounds checking,
// or other non-standard behavior can be marked as "Internal" by
// using the "MFnAttribute::setInternal" method.
//
// The get/setInternalValue methods will get called for internal
// attributes whenever the attribute values are stored or retrieved
// using getAttr/setAttr or MPlug getValue/setValue.
//
// The inherited attribute mControlPoints is internal and we want
// its values to get stored only if there is input history. Otherwise
// any changes to the vertices are stored in the cachedMesh and outputMesh
// directly.
//
// If values are retrieved then we want the controlPoints value
// returned if there is history, this will be the offset or tweak.
// In the case of no history, the vertex position of the cached mesh
// is returned.
bool StrandMaya::getInternalValue(const MPlug &plug, MDataHandle &result){
   bool isOk = true;
   if((plug == mControlPoints)
      ||(plug == mControlValueX)
      ||(plug == mControlValueY)
      ||(plug == mControlValueZ)){
      double val = 0.0;
      if((plug == mControlPoints)&& !plug.isArray()){
         int index = plug.logicalIndex();
         const MPoint& pnt = fCurvePoints[index];
         result.set(pnt[0], pnt[1], pnt[2]);
      } else if(plug == mControlValueX){
         MPlug parentPlug = plug.parent();
         int index = parentPlug.logicalIndex();
         val = fCurvePoints[index].x;
         result.set(val);
      } else if(plug == mControlValueY){
         MPlug parentPlug = plug.parent();
         int index = parentPlug.logicalIndex();
         val = fCurvePoints[index].y;
         result.set(val);
      } else if(plug == mControlValueZ){
         MPlug parentPlug = plug.parent();
         int index = parentPlug.logicalIndex();
         val = fCurvePoints[index].z;
         result.set(val);
      }
   } else{
      isOk = MPxSurfaceShape::getInternalValue(plug, result);
   }
   return(isOk);
}

// Converts the given component values into a selection list of plugs.
// This method is used to map components to attributes.
//     component - the component to be translated to a plug/attribute
//     list - a list of plugs representing the passed in component
void StrandMaya::componentToPlugs(MObject &component, MSelectionList &list) const{
   if(component.hasFn(MFn::kSingleIndexedComponent)){
      MFnSingleIndexedComponent fnVtxComp(component);
      MObject thisNode = thisMObject();
      MPlug plug(thisNode, mControlPoints);

      int len = fnVtxComp.elementCount();
      for(int i = 0; i < len; i++){
         plug.selectAncestorLogicalIndex(fnVtxComp.element(i), plug.attribute());
         list.add(plug);
      }
   }
}

// Component/attribute matching method.
// This method validates component names and indices which are
// specified as a string and adds the corresponding component
// to the passed in selection list.
//
// For instance, select commands such as "select shape1.vtx[0:7]"
// are validated with this method and the corresponding component
// is added to the selection list.
//
//    item - DAG selection item for the object being matched
//    spec - attribute specification object
//    list - list to add components to
MPxSurfaceShape::MatchResult StrandMaya::matchComponent
(const MSelectionList &item, const MAttributeSpecArray &spec, MSelectionList &list){
   MPxSurfaceShape::MatchResult result = MPxSurfaceShape::kMatchOk;
   MAttributeSpec attrSpec = spec[0];
   int dim = attrSpec.dimensions();

   // Look for attributes specifications of the form :
   // vtx[ index ]
   // vtx[ lower:upper ]
   if((spec.length()== 1)&&(dim > 0)&&(attrSpec.name()== "cv")){
      int numVertices = pointCount();
      MAttributeIndex attrIndex = attrSpec[0];

      int upper = 0;
      int lower = 0;
      if(attrIndex.hasLowerBound())
         attrIndex.getLower(lower);
      if(attrIndex.hasUpperBound())
         attrIndex.getUpper(upper);
      // Check the attribute index range is valid
      if((lower > upper)||(upper >= numVertices)){
         result = MPxSurfaceShape::kMatchInvalidAttributeRange;
      } else{
         MDagPath path;
         item.getDagPath(0, path);
         MFnSingleIndexedComponent fnVtxComp;
         MObject vtxComp = fnVtxComp.create(MFn::kCurveCVComponent);
         for(int i=lower; i<=upper; i++)
            fnVtxComp.addElement(i);
         list.add(path, vtxComp);
      }
   } else{
      // Pass this to the parent class
      return(MPxSurfaceShape::matchComponent(item, spec, list));
   }
   return(result);
}

// Creates a geometry iterator compatible with this shape.
//    componentList - list of components to be iterated
//    components - component to be iterator
//    forReadOnly -
MPxGeometryIterator* StrandMaya::geometryIteratorSetup
(MObjectArray &componentList, MObject &components, bool forReadOnly){
   StrandMayaIterator *result = NULL;
   if(components.isNull()) result = new StrandMayaIterator(this, componentList);
   else result = new StrandMayaIterator(this, components);
   return(result);
}

// Specifies that this shape can provide an iterator for getting/setting
// control point values.
//      writable - maya asks for an iterator that can set points if this is true
bool StrandMaya::acceptsGeometryIterator(bool writeable){return(true);}

// Specifies that this shape can provide an iterator for getting/setting
// control point values.
//     writable - maya asks for an iterator that can set points if this is true
//     forReadOnly - maya asking for an iterator for querying only
bool StrandMaya::acceptsGeometryIterator(MObject &, bool writeable, bool forReadOnly){
   return(true);
}

void StrandMaya::points(MPointArray &points){
   MDataBlock block = forceCache();
   block.inputArrayValue(mControlPoints);
   MPlug plug(thisMObject(), mControlPoints);
   nCurvePoints = plug.numElements();

   // Check that the order list is of the same size.
   MPlug orderPlug(thisMObject(), aOrder);
   MPlug eNodesIdxPlug(thisMObject(), aExtraNodesIdx);
   MPlug eNodesPosPlug(thisMObject(), aExtraNodesPos);
   int orderLength = orderPlug.numElements();
   if(nCurvePoints != orderLength){
      log_err("Point and order lengths not matching. Not drawing strand: %d, %d",
              nCurvePoints, orderLength);
      points.setLength(0);
      return;
   }

   // NOTE Ideally you would also like to check that the order has no duplicate elements.
   bool active;
   int loc;
   MStatus ms;
   uint nENodesIdx = eNodesIdxPlug.numElements();
   uint nENodesPos = eNodesPosPlug.numElements();
   int idx;
   V3 pos;

   // if(nENodesIdx!=nENodesPos) log_err("Unequal indices in map attributes.");
   ExtraNodes extraPoints;
   for(uint i=0; i<nENodesIdx; i++){
      MPlug idxPlug = eNodesIdxPlug.elementByLogicalIndex(i, &ms);
      CHECK_MSTATUS(ms);
      // CHECK_MSTATUS(posPlug.get Value(dhPos));
      CHECK_MSTATUS(idxPlug.getValue(idx));
      if(idx == -1) continue;
      if(nENodesPos<=i) log_err("Pos for extra node index probably not set.");
      MPlug posPlug = eNodesPosPlug.elementByLogicalIndex(i, &ms);
      getPointFromPlug(pos, posPlug);
      extraPoints.push_back(std::pair<int, V3>(idx, pos));
   }

   points.setLength(orderLength + extraPoints.size());
   int iExtraPointPair = 0;
   for(int i=0;i<orderLength;i++){
      MPlug locPlug = orderPlug.elementByLogicalIndex(i, &ms);
      CHECK_MSTATUS(ms);
      CHECK_MSTATUS(locPlug.getValue(loc));
      points.set(fCurvePoints[loc], i+iExtraPointPair);
      // If extraPoints still left to check
      while(iExtraPointPair < extraPoints.size()
            // If current index matches the extraPoint's index
            && i==extraPoints[iExtraPointPair].first){
         V3 extraPointPos = extraPoints[iExtraPointPair].second;
         MPoint p = MPoint(extraPointPos[0], extraPointPos[1], extraPointPos[2], 1.0);
         points.set(p, i + iExtraPointPair +1);
         iExtraPointPair++;
      }
   }
   if(iExtraPointPair!=extraPoints.size()) log_warn("Didn't go through all extra points.");
   // for(int i=0;i<orderLength;i++){
   //    MPlug locPlug = orderPlug.elementByLogicalIndex(i, &ms);
   //    CHECK_MSTATUS(ms);
   //    CHECK_MSTATUS(locPlug.getValue(loc));
   //    points.set(fCurvePoints[loc], i);
   // }
   return;
}

unsigned StrandMaya::pointCount() const{
   return(fCurvePoints.length());
}

void StrandMaya::setPoint(unsigned index, const MPoint &val){
   //Get location of this point by checking the order
   MStatus ms;
   MPlug orderPlug(thisMObject(), aOrder);
   MPlug locPlug = orderPlug.elementByLogicalIndex(index, &ms);
   CHECK_MSTATUS(ms);
   // Set the curve point

   if(index < fCurvePoints.length()){
      for(int i=0; i<3;i++){
         ms = locPlug.child(i).setValue(val[i]);
         CHECK_MSTATUS(ms);
      }
   }
   return;
}

MPoint StrandMaya::point(unsigned index) const{
   if(index < fCurvePoints.length())
      return(fCurvePoints[index]);
   return(MPoint());
}

void StrandMaya::setPointValue(const MPlug &plug, const MDataHandle &handle, unsigned vtInd){
   MPlug parentPlug = plug.parent();
   unsigned index = parentPlug.logicalIndex();
   if(index < fCurvePoints.length()){
      fCurvePoints[index][vtInd] = handle.asDouble();
      fCurvePoints[index].w = 1.0;
   }
}

int StrandMaya::getOrderIndex(int index){
   MPlug orderPlug(thisMObject(), aOrder);
   int orderLength = orderPlug.numElements();
   MStatus ms;
   int loc;
   for(int i=0;i<orderLength;i++){
      MPlug locPlug = orderPlug.elementByLogicalIndex(i, &ms);
      CHECK_MSTATUS(ms);
      ms = locPlug.getValue(loc);
      CHECK_MSTATUS(ms);
      if(loc == index)return i;
   }
   return -1;
}

MStatus StrandMaya::getPointIds(IVec& pointIds){
   MDataBlock block = forceCache();
   block.inputArrayValue(mControlPoints);
   MPlug plug(thisMObject(), mControlPoints);
   nCurvePoints = plug.numElements();
   MObject strandObj = this->thisMObject();
   MFnDependencyNode strandDN(strandObj);
   MStatus ms;
   MPlug nodesPlug = strandDN.findPlug(kAttrStrandNodesLong, &ms);
   MPlug nodePlug;
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   MPlug orderPlug(thisMObject(), aOrder);
   uint nOrder = (uint)orderPlug.numElements();
   uint nNodes = (uint)nodesPlug.numElements();
   if(nCurvePoints != nOrder)ms = MS::kFailure;
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   if(nCurvePoints != nNodes)ms = MS::kFailure;
   CHECK_MSTATUS_AND_RETURN_IT(ms);

   MPlug locPlug;
   int loc;
   for(uint ii=0; ii<nCurvePoints; ii=ii+1){
      locPlug = orderPlug.elementByLogicalIndex(ii, &ms);
      ms = locPlug.getValue(loc);
      nodePlug = nodesPlug.elementByLogicalIndex(loc, &ms);
      pointIds.push_back(nodePlug.asInt());
   }

   return MS::kSuccess;
}

void *StrandMaya::creator(){return(new StrandMaya());}

// Static attributes initialization
MStatus StrandMaya::initialize(){
   MStatus ms;

   // bbox attributes
   MAKE_NUMERIC_ATTR(bboxCorner1, "bboxCorner1", "bb1", MFnNumericData::k3Double, 0, false, false, false);
   MAKE_NUMERIC_ATTR(bboxCorner2, "bboxCorner2", "bb2", MFnNumericData::k3Double, 0, false, false, false);

   // Keyable attributes
   // MAKE_NUMERIC_ATTR(aConstForceAttrObj, kAttrSConstForceLong , kAttrSConstForceShort,
   //                   MFnNumericData::kDouble, 0, false, false, true);
   // MAKE_NUMERIC_ATTR(aInexActiveAttrObj, kAttrSInexActiveLong, kAttrSInexActiveShort,
   //                   MFnNumericData::kBoolean, 0, false, false, true);
   MFnNumericAttribute nAttr;
   aOrder = nAttr.create(kAttrStrandOrderLong, kAttrStrandOrderShort, MFnNumericData::kInt, -1, &ms);
   CHECK_MSTATUS_AND_RETURN_IT(ms);
   nAttr.setWritable(true);
   nAttr.setArray(true);
   nAttr.setIndexMatters(true);
   nAttr.setHidden(true);
   MCHECKWARN(nAttr.setDisconnectBehavior(MFnAttribute::DisconnectBehavior::kDelete),
              "Could not set disconnect behavior.");
   // aInextensible = nAttr.create(kAttrInextensibleLong, kAttrInextensibleShort,
   //                              MFnNumericData::kBoolean, 0, &ms);
   // MCHECKRET(ms);
   nAttr.setWritable(true);
   aExtraNodesIdx = nAttr.create(kAttrExtraNodesIdxLong, kAttrExtraNodesIdxShort,
                                 MFnNumericData::kInt, -1, &ms);
   nAttr.setWritable(true);
   nAttr.setArray(true);
   nAttr.setIndexMatters(true);
   nAttr.setHidden(true);
   aExtraNodesPos = nAttr.createPoint(kAttrExtraNodesPosLong, kAttrExtraNodesPosShort, &ms);
   nAttr.setWritable(true);
   nAttr.setArray(true);
   nAttr.setIndexMatters(true);
   nAttr.setHidden(true);

   MCHECKRET(addAttribute(aOrder));
   // MCHECKRET(addAttribute(aInextensible));
   MCHECKRET(addAttribute(aExtraNodesIdx));
   MCHECKRET(addAttribute(aExtraNodesPos));

   ATTRIBUTE_AFFECTS(mControlPoints, bboxCorner1);
   ATTRIBUTE_AFFECTS(mControlValueX, bboxCorner1);
   ATTRIBUTE_AFFECTS(mControlValueY, bboxCorner1);
   ATTRIBUTE_AFFECTS(mControlValueZ, bboxCorner1);
   ATTRIBUTE_AFFECTS(mControlPoints, bboxCorner2);
   ATTRIBUTE_AFFECTS(mControlValueX, bboxCorner2);
   ATTRIBUTE_AFFECTS(mControlValueY, bboxCorner2);
   ATTRIBUTE_AFFECTS(mControlValueZ, bboxCorner2);
   return(MS::kSuccess);
}

MStatus StrandMaya::registerMe(MFnPlugin &plugin){
   return(plugin.registerShape
          ("Strand",
           StrandMaya::id,
           StrandMaya::creator,
           StrandMaya::initialize,
           StrandMayaUI::creator,
           &StrandMaya::drawDbClassification));
}

MStatus StrandMaya::unregisterMe(MFnPlugin &plugin){
   return(plugin.deregisterNode(StrandMaya::id));
}
