// @author: Prashant Sachdeva (sachdevp@cs.ubc.ca)
// @desc:   Class for drawing a strand in the Viewport. Supports only legacy viewport.
// NOTE File modified with reference from Autodesk's available code.
// Original copyright:
// Copyright 2012 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk license
// agreement provided at the time of installation or download, or which
// otherwise accompanies this software in either electronic or hard copy form.

#include "StrandMayaIterator.h"
#include "StrandMayaShape.h"
#include "StrandMayaShapeUI.h"
#include <maya/MFnSingleIndexedComponent.h>
#include <maya/MDagPath.h>
#include <maya/MMatrix.h>
#include <maya/MDrawData.h>
#include <maya/MFnDependencyNode.h>

#include <maya/MDrawRegistry.h>
#include <maya/MPxGeometryOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawContext.h>
#include <maya/MShaderManager.h>
#include <maya/MHWGeometry.h>
#include <maya/MHWGeometryUtilities.h>
#include <maya/MPointArray.h>
#include <maya/MDistance.h>

#include <include_common.hpp>
#include <assert.h>

// Object and component color defines
#define  LEAD_COLOR             18  // green
#define  ACTIVE_COLOR           15  // white
#define  ACTIVE_AFFECTED_COLOR  8   // purple
#define  DORMANT_COLOR          4   // blue
#define  HILITE_COLOR           17  // pale blue
#define  DORMANT_VERTEX_COLOR   8   // purple
#define  ACTIVE_VERTEX_COLOR    16  // yellow

#define POINT_SIZE 20.0
#define LINE_WIDTH 5.0

//-----------------------------------------------------------------------------
StrandMayaUI::StrandMayaUI(){}

StrandMayaUI::~StrandMayaUI(){}

//-----------------------------------------------------------------------------
void *StrandMayaUI::creator(){return (new StrandMayaUI());}

//-----------------------------------------------------------------------------
// Overrides

// Add draw requests to the draw queue
//    info - current drawing state
//    objectsAndActiveOnly - no components if true
//    queue - queue of draw requests to add to
void StrandMayaUI::getDrawRequests
(const MDrawInfo &info, bool objectAndActiveOnly, MDrawRequestQueue &queue){
   // Get the data necessary to draw the shape
   MDrawData data;
   StrandMaya *strandMayaNode =(StrandMaya *)surfaceShape();
   if ( strandMayaNode == NULL ) log_err("NO DrawRequest for StrandMaya");

   // This call creates a prototype draw request that we can fill
   // in and then add to the draw queue.
   MDrawRequest request =info.getPrototype (*this);
   getDrawData (strandMayaNode , data);
   request.setDrawData (data);
   request.setToken (kDrawWireframe);

   // Set the color based on the display status
   M3dView::DisplayStatus displayStatus =info.displayStatus();
   M3dView::ColorTable activeColorTable =M3dView::kActiveColors;
   M3dView::ColorTable dormantColorTable =M3dView::kDormantColors;

   switch ( displayStatus ){
   case M3dView::kLead:
      request.setColor (LEAD_COLOR, activeColorTable);
      break;
   case M3dView::kActive:
      request.setColor (ACTIVE_COLOR, activeColorTable);
      break;
   case M3dView::kActiveAffected:
      request.setColor (ACTIVE_AFFECTED_COLOR, activeColorTable);
      break;
   case M3dView::kDormant:
      request.setColor (DORMANT_COLOR, dormantColorTable);
      break;
   case M3dView::kHilite:
      request.setColor (HILITE_COLOR, activeColorTable);
      break;
   default:
      break;
   }
   queue.add (request);

   // Add draw requests for components
   if(!objectAndActiveOnly){
      // Inactive components
      M3dView::DisplayStyle appearance =info.displayStyle();
      if ( (appearance ==M3dView::kPoints ) || (displayStatus ==M3dView::kHilite) ){
         MDrawRequest vertexRequest =info.getPrototype (*this);
         vertexRequest.setDrawData (data);
         vertexRequest.setToken (kDrawVertices);
         vertexRequest.setColor (DORMANT_VERTEX_COLOR, M3dView::kActiveColors);
         queue.add (vertexRequest);
      }
      // Active components
      if ( surfaceShape()->hasActiveComponents() ){
         MDrawRequest activeVertexRequest =info.getPrototype (*this);
         activeVertexRequest.setDrawData (data);
         activeVertexRequest.setToken (kDrawVertices);
         activeVertexRequest.setColor (ACTIVE_VERTEX_COLOR, M3dView::kActiveColors);

         MObjectArray clist =surfaceShape()->activeComponents();
         MObject vertexComponent =clist [0]; // Should filter list
         activeVertexRequest.setComponent (vertexComponent);
         queue.add (activeVertexRequest);
      }
   }
}

double StrandMayaUI::getPointSizeMultiplier(M3dView &view) const{
   MMatrix mat1, mat2, mat;
   MStatus ms;
   ms = view.modelViewMatrix(mat);
   MPoint nearPt, farPt;
   ms = view.viewToWorld(0,0,nearPt, farPt);
   MPoint WorldCenter(mat.inverse()[3]);
   ms = view.projectionMatrix(mat1);
   mat2 = mat1*mat;
   double multiplier = mat2[2][2];
   return multiplier;
}

// Main (OpenGL) draw routine. Based on the type of drawing, draw the curve or the points.
//    request - request to be drawn
//    view - view to draw into
void StrandMayaUI::draw(const MDrawRequest &request, M3dView &view) const{
   int token =request.token();
   MPointArray points;
   double multiplier = getPointSizeMultiplier(view);

   switch (token){
   case kDrawWireframe:
   case kDrawWireframeOnShaded:
      drawSegments(view);
      break;
   case kDrawVertices:
      StrandMaya *curveNode = (StrandMaya *)surfaceShape();
      curveNode->points(points);
      drawPoints(points, multiplier);
      break;
   }
}

void StrandMayaUI::drawSegments(M3dView &view) const{
   // Get the curve
   StrandMaya* curveNode = (StrandMaya*)surfaceShape();
   MPointArray points;
   curveNode->points(points);
   double multiplier = getPointSizeMultiplier(view);

   // Start drawing segments
   view.beginGL();
   glPushAttrib(GL_LINE_BIT);
   glLineWidth(LINE_WIDTH);
   glBegin(GL_LINE_STRIP);
   MPoint next;
   uint length = points.length();
   for(uint ii =0; ii<length; ++ii){
      next = points[ii];
      glVertex3f((float)next[0], (float)next[1], (float)next[2]);
   }
   glEnd();

   // Start drawing nodes
   drawPoints(points, multiplier);
   glPopAttrib();
   view.endGL();
}

// Main selection routine. Use OpenGL selection.
//    selectInfo - the selection state information
//    selectionList - the list of selected items to add to
//    worldSpaceSelectPts -
bool StrandMayaUI::select (MSelectInfo &selectInfo, MSelectionList &selectionList, MPointArray &worldSpaceSelectPts) const{
   bool selected          = false;
   bool componentSelected = false;
   bool hilited           = false;

   hilited = (selectInfo.displayStatus() ==M3dView::kHilite);
   if(hilited){
      componentSelected = selectPoints(selectInfo, selectionList, worldSpaceSelectPts);
      selected = selected || componentSelected;
   }
   if ( !selected ){
      M3dView myView =selectInfo.view();
      myView.beginSelect();
      drawSegments (myView);
      selected =(myView.endSelect() > 0);
      if(selected){
         StrandMaya *curveNode =(StrandMaya *)surfaceShape();
         MSelectionMask priorityMask (MSelectionMask::kSelectCVs);
         MSelectionList item;
         item.add(selectInfo.selectPath());
         MPoint xformedPt;
         if(selectInfo.singleSelection()){
            MPoint center =curveNode->boundingBox().center();
            xformedPt =center;
            xformedPt *=selectInfo.selectPath().inclusiveMatrix();
         }
         selectInfo.addSelection (item, xformedPt, selectionList, worldSpaceSelectPts, priorityMask, false);
      }
   }
   return (selected);
}

// Check to see whether points have been selected.
bool StrandMayaUI::selectPoints (MSelectInfo &selectInfo, MSelectionList &selectionList, MPointArray &worldSpaceSelectPts) const{
   bool selected =false;
   M3dView view =selectInfo.view();

   MPoint xformedPoint;
   MPoint currentPoint;
   MPoint selectionPoint;
   double z,previousZ =0.0;
   int closestPointVertexIndex =-1;
   const MDagPath &path =selectInfo.multiPath();

   // Create a component that will store the selected vertices
   MFnSingleIndexedComponent fnComponent;
   MObject surfaceComponent =fnComponent.create (MFn::kCurveCVComponent);
   int vertexIndex;

   // if the user did a single mouse click and we find > 1 selection
   // we will use the alignmentMatrix to find out which is the closest
   MMatrix   alignmentMatrix;
   MPoint singlePoint;
   bool singleSelection =selectInfo.singleSelection();
   if ( singleSelection )
      alignmentMatrix =selectInfo.getAlignmentMatrix();

   // Get the geometry information
   StrandMaya *curveNode =(StrandMaya *)surfaceShape();
   MPointArray points;
   curveNode->points (points);
   // Loop through all vertices of the mesh and
   // see if they lie withing the selection area
   int numVertices =points.length();
   for ( vertexIndex=0; vertexIndex<numVertices; vertexIndex++ ){
      const MPoint &currentPoint =points [vertexIndex];
      // Sets OpenGL's render mode to select and stores
      // selected items in a pick buffer
      view.beginSelect();
      glBegin (GL_POINTS);
      glVertex3f ((float)currentPoint [0], (float)currentPoint [1], (float)currentPoint [2]);
      glEnd();
      if ( view.endSelect() > 0 ){ // Hit count > 0
         selected =true;
         if ( singleSelection ){
            xformedPoint =currentPoint;
            xformedPoint.homogenize();
            xformedPoint*=alignmentMatrix;
            z =xformedPoint.z;
            if ( closestPointVertexIndex < 0 || z > previousZ ){
               closestPointVertexIndex =vertexIndex;
               singlePoint =currentPoint;
               previousZ =z;
            }
         } else{
            // multiple selection, store all elements
            fnComponent.addElement (vertexIndex);
         }
      }
   }

   // If single selection, insert the closest point into the array
   //
   if ( selected && selectInfo.singleSelection() ){
      fnComponent.addElement (closestPointVertexIndex);
      // need to get world space position for this vertex
      selectionPoint =singlePoint;
      selectionPoint *=path.inclusiveMatrix();
   }

   // Add the selected component to the selection list
   if ( selected ){
      MSelectionList selectionItem;
      selectionItem.add (path, surfaceComponent);
      MSelectionMask mask (MSelectionMask::kSelectComponentsMask);
      selectInfo.addSelection (selectionItem, selectionPoint, selectionList, worldSpaceSelectPts, mask, true);
   }

   return (selected);
}

GLubyte lineIndices[] ={0, 1, 1, 2, 2, 3, 3, 0, // First face
                        5, 6, // v3-v4
                        6, 7, // v4-v5
                        8, 9, // v5-v0
                        9, 10, // v5-v6
                        10, 11, // v6-v1
                        13, 14, // v6-v7
                        14, 15, // v7-v2
                        16, 17 // v4-v7
};
GLubyte indices[]  ={ 0, 1, 2,   2, 3, 0,      // front
                      4, 5, 6,   6, 7, 4,      // right
                      8, 9,10,  10,11, 8,      // top
                      12,13,14,  14,15,12,      // left
                      16,17,18,  18,19,16,      // bottom
                      20,21,22,  22,23,20 };    // back

GLfloat normals[]  ={ 0, 0, 1,   0, 0, 1,   0, 0, 1,   0, 0, 1,   // v0,v1,v2,v3 (front)
                      1, 0, 0,   1, 0, 0,   1, 0, 0,   1, 0, 0,   // v0,v3,v4,v5 (right)
                      0, 1, 0,   0, 1, 0,   0, 1, 0,   0, 1, 0,   // v0,v5,v6,v1 (top)
                      -1, 0, 0,  -1, 0, 0,  -1, 0, 0,  -1, 0, 0,   // v1,v6,v7,v2 (left)
                      0,-1, 0,   0,-1, 0,   0,-1, 0,   0,-1, 0,   // v7,v4,v3,v2 (bottom)
                      0, 0,-1,   0, 0,-1,   0, 0,-1,   0, 0,-1 }; // v4,v7,v6,v5 (back)

GLfloat vertices[] ={ 1, 1, 1,  -1, 1, 1,  -1,-1, 1,   1,-1, 1,   // v0,v1,v2,v3 (front)
                      1, 1, 1,   1,-1, 1,   1,-1,-1,   1, 1,-1,   // v0,v3,v4,v5 (right)
                      1, 1, 1,   1, 1,-1,  -1, 1,-1,  -1, 1, 1,   // v0,v5,v6,v1 (top)
                      -1, 1, 1,  -1, 1,-1,  -1,-1,-1,  -1,-1, 1,   // v1,v6,v7,v2 (left)
                      -1,-1,-1,   1,-1,-1,   1,-1, 1,  -1,-1, 1,   // v7,v4,v3,v2 (bottom)
                      1,-1,-1,  -1,-1,-1,  -1, 1,-1,   1, 1,-1 }; // v4,v7,v6,v5 (back)

void drawLineCube(){
   glPolygonMode( GL_FRONT_AND_BACK, GL_LINE);
   glEnableClientState(GL_VERTEX_ARRAY);

   // glColorPointer(3, GL_FLOAT, 0, colors);
   glVertexPointer(3, GL_FLOAT, 0, vertices);

   // draw a cube
   glDrawElements(GL_LINES, 24, GL_UNSIGNED_BYTE, lineIndices);

   // deactivate vertex arrays after drawing
   glDisableClientState(GL_VERTEX_ARRAY);
}

void drawFillCube(){
   glPolygonMode( GL_FRONT_AND_BACK, GL_FILL);
   glEnableClientState(GL_VERTEX_ARRAY);
   glEnableClientState(GL_NORMAL_ARRAY);
   glVertexPointer(3, GL_FLOAT, 0, vertices);
   glNormalPointer(GL_FLOAT, 0, normals);
   glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, indices);
   glDisableClientState(GL_NORMAL_ARRAY);
   glDisableClientState(GL_VERTEX_ARRAY);
}

void drawEulerian(MPoint point, GLfloat scale){
   glPushMatrix();
   glTranslatef((GLfloat)point[0], (GLfloat)point[1], (GLfloat)point[2]);
   glScalef(scale, scale, scale);
   glPushAttrib(GL_POLYGON_BIT | GL_COLOR_BUFFER_BIT| GL_LINE_BIT);
   glColor3f(0.0f, 1.0f, 0.0f);
   glLineWidth(0.1f);
   drawLineCube();
   glPopAttrib();
   glPopMatrix();
   return;
}

void drawLagrangian(MPoint point, GLfloat scale){
   glPushMatrix();
   glTranslatef((GLfloat)point[0], (GLfloat)point[1], (GLfloat)point[2]);
   glScalef(scale, scale, scale);
   glPushAttrib(GL_POLYGON_BIT | GL_COLOR_BUFFER_BIT);
   glColor3f(1, 0, 0);
   drawFillCube();
   glPopAttrib();
   glPopMatrix();
   return;
}

void StrandMayaUI::drawPoints (const MPointArray &points, double multiplier) const{
   // MObject comp =request.component();
   // if ( !comp.isNull() ){
   //    MFnSingleIndexedComponent fnComponent (comp);
   //    unsigned count =fnComponent.elementCount();
   //   drawLagrangian(points[0]);
   //    for ( unsigned ii=0; ii < count; ii++ ){
   //       int index =fnComponent.element (ii);
   //       const MPoint& vertex =points [index];
   //     drawEulerian(vertex);
   //    }
   //   drawLagrangian(points[count-1]);
   // } else{
   double scale = 0.05*fmin(multiplier/10.0, 1);
   unsigned count = points.length();
   if(count>0) drawLagrangian(points[0], (GLfloat)scale);
   for ( unsigned ii=0; ii < count; ii++ ){
      const MPoint &vertex = points[ii];
      drawEulerian(vertex, (GLfloat)scale);
   }
   if(count>1) drawLagrangian(points[count-1], (GLfloat)scale);
   //}
}

StrandMayaGeometryOverride::
StrandMayaGeometryOverride(const MObject& obj)
   : MHWRender::MPxGeometryOverride(obj)
   , mSolidUIShader(NULL)
   , mStrandObj(obj)
   , mMultiplier(1.0f)
   , mMultiplierChanged(true)
   , mStrain(0.0f)
   , mRadius(0.01f){
   MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
   if (!renderer)
      return;
   const MHWRender::MShaderManager* shaderMgr = renderer->getShaderManager();
   if (!shaderMgr)
      return;
   mSolidUIShader = shaderMgr->getStockShader(MHWRender::MShaderManager::k3dSolidShader);
}

StrandMayaGeometryOverride::
~StrandMayaGeometryOverride(){
   if(mSolidUIShader){
      MHWRender::MRenderer* renderer = MHWRender::MRenderer::theRenderer();
      if (renderer){
         const MHWRender::MShaderManager*
            shaderMgr = renderer->getShaderManager();
         if (shaderMgr) shaderMgr->releaseShader(mSolidUIShader);
      }
      mSolidUIShader = NULL;
   }
}

MHWRender::DrawAPI StrandMayaGeometryOverride::
supportedDrawAPIs() const{
   // this plugin supports both GL and DX
   return (MHWRender::kOpenGL | MHWRender::kDirectX11 | MHWRender::kOpenGLCoreProfile);
}

const MString colorParameterName_   = "solidColor";
const MString cylinderItemName    = "cylinder";
const MString circleItemName    = "circle";
const MString squareItemName    = "square";

void StrandMayaGeometryOverride::updateDG(){
   MFnDependencyNode strandDN(mStrandObj);
   MStatus ms;
   // Get strain
   MPlug strainPlug = strandDN.findPlug("strain", &ms);
   if(MFAIL(ms)){
      log_err("Could not find plug for strain");
   }
   // Get points in the right drawing order
   StrandMaya *curveNode =(StrandMaya *)strandDN.userNode();
   curveNode->points(mPoints);
   MPlug radiusPlug = strandDN.findPlug("radius", &ms);

   if(MFAIL(ms)){
      log_err("Could not find plug for radius");
   }
   if (!strainPlug.isNull() && !radiusPlug.isNull()){
      if (!strainPlug.getValue(mStrain)){
         log_err("Could not get value for strain");
      }
      if (!radiusPlug.getValue(mRadius)){
         log_err("Could not get value for radius");
      }
   } else{
      log_err("Null plugs");
   }
}

// FIXME Construct the list properly
void StrandMayaGeometryOverride::
updateRenderItems(const MDagPath& path, MHWRender::MRenderItemList& list){
   MHWRender::MRenderItem* cylinderItem = NULL;

   for(uint ii=0; ii<mPoints.length()-1; ii++ ){
      cylinderItem = MHWRender::
         MRenderItem::Create(cylinderItemName,
                             MHWRender::MRenderItem::DecorationItem,
                             MHWRender::MGeometry::kLineStrip);
      cylinderItem->setDrawMode(MHWRender::MGeometry::kWireframe);
      cylinderItem->depthPriority(5);
      list.append(cylinderItem);

      if(cylinderItem){
         if (mSolidUIShader){
            MColor color = MHWRender::MGeometryUtilities::wireframeColor(path);
            float  wireframeColor[4] = { color.r, color.g, color.b, 1.0f };
            mSolidUIShader->setParameter(colorParameterName_, wireframeColor);
            cylinderItem->setShader(mSolidUIShader);
         }
         cylinderItem->enable(true);
      }
   }
}

// Number of vertices of the cross section of the strand.
// nCrossSectionVertices per node of strand
int nCrossSectionVertices = 10;

void StrandMayaGeometryOverride::
populateGeometry(const MHWRender::MGeometryRequirements& requirements,
                 const MHWRender::MRenderItemList& renderItems,
                 MHWRender::MGeometry& data){

   MHWRender::MVertexBuffer* verticesBuffer = NULL;
   float* vertices                          = NULL;
   int nVertices = (nCrossSectionVertices*mPoints.length());
   const MHWRender::MVertexBufferDescriptorList&
      vertexBufferDescriptorList = requirements.vertexRequirements();
   const int numberOfVertexRequirments = vertexBufferDescriptorList.length();

   MHWRender::MVertexBufferDescriptor vertexBufferDescriptor;
   for (int requirmentNumber = 0; requirmentNumber < numberOfVertexRequirments; ++requirmentNumber){
      if (!vertexBufferDescriptorList.
          getDescriptor(requirmentNumber, vertexBufferDescriptor)){continue;}

      switch (vertexBufferDescriptor.semantic()){
      case MHWRender::MGeometry::kPosition:
         if (!verticesBuffer){
            verticesBuffer = data.createVertexBuffer(vertexBufferDescriptor);
            if (verticesBuffer){
               vertices = (float*)verticesBuffer->acquire(nVertices);
            }
         }
         break;
      default:
         // do nothing for stuff we don't understand
         break;
      }
   }

   int verticesPointerOffset = 0;

   // We concatenate the heel and sole positions into a single vertex buffer.
   // The index buffers will decide which positions will be selected for each render items.
   for (int currentVertex = 0; currentVertex < nVertices; ++currentVertex){
      if(vertices){
         int pointNumber  = currentVertex/mPoints.length();
         int csVertNumber = currentVertex % mPoints.length();
         MPoint csCenter  = mPoints[pointNumber];
         MPoint csRelative;
         float theta    = (csVertNumber/( nCrossSectionVertices +1))*2*M_PI;
         csRelative[0]  = mRadius*cos(theta);
         csRelative[1]  = mRadius*sin(theta);
         csRelative[2]  = 0;
         MPoint vertPos = csCenter + csRelative;
         vertices[verticesPointerOffset++] = (ftype)vertPos[0];
         vertices[verticesPointerOffset++] = (ftype)vertPos[1];
         vertices[verticesPointerOffset++] = (ftype)vertPos[2];
      }
   }

   if(verticesBuffer && vertices){
      verticesBuffer->commit(vertices);
   }

   for (int i=0; i < renderItems.length(); ++i){
      const MHWRender::MRenderItem* item = renderItems.itemAt(i);

      if (!item) continue;

      // Start position in the index buffer
      int startIndex = i*2*nCrossSectionVertices;
      // int endIndex   = startIndex + 2*nCrossSectionVertices;
      // Number of index to generate (for line strip, or triangle list)
      int numIndex = 0;
      bool isWireFrame = true;
      if (item->name() == "cylinder"){
         numIndex = 2*nCrossSectionVertices; // FIXME Find this value
      }

      if (numIndex){
         MHWRender::MIndexBuffer*  indexBuffer = data.createIndexBuffer(MHWRender::MGeometry::kUnsignedInt32);
         unsigned int* indices = (unsigned int*)indexBuffer->acquire(numIndex);

         for(int i = 0; i < numIndex; ){
            if (isWireFrame){
               // Line strip starts at first position and iterates thru all vertices:
               indices[i] = startIndex + i/2;
               indices[i+1] = startIndex + i/2 + 1;
               i+=2;
            } else{
               // Triangle strip zig-zagging thru the points:
               indices[i] = startIndex + i/2;
               indices[i+1] = startIndex + i/2 + 1;
               i+=2;
            }
         }

         indexBuffer->commit(indices);
         item->associateWithIndexBuffer(indexBuffer);
      }
   }
   mMultiplierChanged = false;
}
