/**
   @author: PrashantSachdeva
   @desc:   Access functions for scene class
*/

#include <Scene/Scene.hpp>
#include <Joint.hpp>
#include <DynObj/Rigid.hpp>
#include <DynObj/PointL.hpp>
#include <DynObj/Strand.hpp>
#include <MatrixCentral.hpp>
#include <Scene/SceneAttr.hpp>
#include <DynObj/WorldObject.hpp>
#include <DoF/DoF.hpp>
#include <SE3.hpp>

namespace Strands{
   uint Scene::getnStrands(){
      return (uint)this->vStrands.size();
   }
   uint Scene::getnRigids(){
      return (uint)this->vRigids.size();
   }
   uint Scene::getnDoFs(){
      return (uint)this->vDoFs.size();
   }
   uint Scene::getnJoints(){
      return (uint)this->vJoints.size();
   }
   uint Scene::getnPoints(){
      return (uint)this->vPoints.size();
   }

   // If @idIsPhysical is true, then just give vStrands(id), otherwise return
   // the strand with the strandId @id
   RET Scene::getStrand(Strand* &strand, int id, bool idIsPhysical){
      if(idIsPhysical && this->vStrands.size() < id){
         strand = this->vStrands.at(id).get();
         return R::Success;
      } else{
         for(auto iStrand: this->vStrands){
            if(iStrand->getStrandId() == id){
               strand = iStrand.get();
               return R::Success;
            }
         }
      }
      strand = nullptr;
      return R::Failure;
   }

   RET Scene::getJoint(SP<Joint> &joint, int id, bool idIsPhysical){
      if(idIsPhysical && this->vJoints.size() < id){
         joint = this->vJoints.at(id);
         return R::Success;
      } else{
         for(auto iJoint: this->vJoints){
            if(iJoint->getJointId() == id){
               joint = iJoint;
               return R::Success;
            }
         }
      }
      joint = nullptr;
      return R::Failure;
   }

   // If @idIsPhysical is true, then just give vPoints(id), otherwise return the
   // point with the pointId @id
   RET Scene::getPointL(PointL* &point, M4 &dofE, int id, bool idIsPhysical){
      if(idIsPhysical && this->vPoints.size() < id){
         point = this->vPoints.at(id).get();
         return R::Success;
      } else{
         for(auto iPoint: this->vPoints){
            if(iPoint->getPointId() == id){
               point = iPoint.get();
               dofE  = iPoint->getDoFE();
               return R::Success;
            }
         }
      }
      point = nullptr;
      return R::Failure;
   }

   // If @idIsPhysical is true, then just give vPoints(id), otherwise return the
   // point with the pointId @id
   RET Scene::getRigid(SP<Rigid> &rigid, int id, bool idIsPhysical){
      log_random("Looking for rigid body ID: %d", id);
      if(idIsPhysical && this->vRigids.size() < id){
         rigid = this->vRigids.at(id);
         return R::Success;
      } else{
         for(auto iRigid: this->vRigids){
            if(iRigid->getRigidId() == id){
               rigid = iRigid;
               return R::Success;
            }
         }
      }
      rigid = nullptr;
      return R::Failure;
   }

}
