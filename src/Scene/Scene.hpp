/**
   Scene.h
   @auth: Prashant Sachdeva
   @desc: Scene is the base class used by the Strands system. It is the central
   matrix organizer and keeps track of all the objects
*/

#pragma once
#include <include_common.hpp>
#include <fstream>
// #include <Recorder/Recorder.hpp>
#include "SceneAttr.hpp"

class Rigid;
class PointL;
class MatrixCentral;
class SceneAttr;
class Object;
class WorldObject;
class DoF;
class DynObj;
class StopWatch;
class Indicator;

class Strands::Scene {
   // Tests should be able to access all properties of strands
   friend class TestScene;
   friend class Strands::SceneCreator;
   // friend class ::Recorder;

private:
   SceneAttr* sceneAttr;

   SPVEC<PointL>     vPoints;
   SPVEC<Rigid>      vRigids;
   SPVEC<Joint>      vJoints;
   SPVEC<Strand>     vStrands;
   SPVEC<Force>      vForces;
   SPVEC<Indicator>  vIndicators;
   std::vector<DoF*> vDoFs;

   std::string label;
   bool bFailed;

#ifdef PROFILING
   static StopWatch *sw;
#endif

   MatrixCentral* matrices;
   // @tEnd: Scene length
   // @t: Current time of scene
   ftype tEnd, t, Dt;
   // Number of times @step function was called
   int nSteps;
   bool isEnd;
   bool bStabilize, bSolverSet, bInit, bUpdated;

   // Current numbers. Filled by @step().
   // FullEq, FullIneq, RedEq, RedIneq
   int indFECons;
   int indFICons;
   int indRECons;
   int indRICons;

   STRVEC vFILabels, vRILabels;
   IVec vFILen, vRILen;
   STRVEC vRELabels, vFELabels;
   IVec vRELen, vFELen;

   // Debugging
   bool bDebug, bDebugSet, bDebugAcc, bMatOut;
   std::string mDebugFile;
   std::string mDataOutDir;
   std::ofstream outFile;
   KeysMap<bool> mDebugMap;

   SP<WorldObject> mWorld;

   const Eigen::Vector3t &getGravity() const;
   SP<DynObj> getWorldObject();
   const inline V3 grav(){return this->sceneAttr->gravity;}

   // Profiling
   // TIME<HRC> start, end;

   // DUR<double> fill_time, dynamics_time, update_time, stab_time, debug_time;

   // Fill the block matrices in the correct regions.
   // Requires taking the LL, EL, LE, EE blocks of @M_in and placing them at
   // their required locations in @M_out, based on nLStrand and nEStrand
   RET blockFill(Eigen::MatrixXt &M_out, Eigen::MatrixXt &M_in,
                 int indL, int indE, int nLStrand, int nEStrand);

   // Clear scene for stepping.
   RET clearForStep();
   void InitWorld();
   RET computeSizes(int &nFull, int &nRL, int &nR, int &nC_r,
                    int &nC_f, int &nC_ir, int &nC_if, int &nJDoFs);
   RET setMatLogFile(std::string suffix);
   Rigid* getRigid(std::string rigidName);
   Strand* getStrand(std::string strandName);
   Wrench* getWrench(std::string wrenchName);

public:
   Scene();
   void endScene();
   void InitMatrices();

   RET setTime(ftype);
   RET setTimestep(ftype);
   ftype getTimestep();
   RET setDamping (ftype);
   RET setStab    (ftype);
   RET setMatOutDir(const char* dataOutDir);
   RET setInitialInternalState(Eigen::VectorXt x_r, Eigen::VectorXt v_r);
   RET getInitialInternalState(Eigen::VectorXt &x_r, Eigen::VectorXt &v_r);
   RET getReducedMat(Eigen::MatrixXt &M_r);
   RET getRHS(Eigen::VectorXt &rhs);
   RET gatherVelocity();

   /** Get functions for objects **/
   SP<Object> getObjectById(uint id);
   SP<DynObj> getDynObj(uint id);
   SP<DynObj> getRigid(int rigidId);

   // How to create scene
   void addRigid(SP<Rigid> _rigid);
   void addJoint(SP<Joint> _joint);
   void addStrand(SP<Strand> _strand);
   void addPoint(SP<PointL> _point);
   void addForce(SP<Force> _force);
   void addDoF(DoF* _dof);

   MAP<PointL> getPointsMap();   
   // Get functions to probe the scene
   uint getnStrands();
   uint getnRigids();
   uint getnPoints();
   uint getnDoFs();
   uint getnJoints();
   // Get strand based on either the strandId or physical strand id in the vector vStrands
   RET getStrand(Strand* &strand, int id, bool idIsPhysical = false);
   RET getJoint(SP<Joint> &joint, int id, bool idIsPhysical = false);
   RET getPointL(PointL* &point, M4 &dofE, int id, bool idIsPhysical = false);
   RET getRigid(SP<Rigid> &rigid, int id, bool idIsPhysical = false);

   auto getStrands(){return vStrands;}
   auto getRigids(){return vRigids;}
   auto getPoints(){return vPoints;}
   auto getJoints(){return vJoints;}

   // Get reduced and full constraint drift for implementing stabilization (Cline and Pai)
   RET getRedDrift(Eigen::VectorXt &eqDrift, Eigen::VectorXt &ineqDrift);
   RET getFullDrift(Eigen::VectorXt &eqDrift, Eigen::VectorXt &ineqDrift);
   // Compile reduced and full drift together
   RET getDrift(Eigen::VectorXt &drift);
   RET getJointIneqDrift(MAPSTRV3&);
   bool getDataOutDir(std::string &outString);
   bool getDebugFile(std::string &outFile);

   /** Debugging functions **/
   RET setDebug(KeysMap<bool> _debugMap, std::string filename, bool replace=true);
   RET debugOutput();
   RET debugLambda();
   RET updateDebug();
   bool willDebug();

   // The most important function of the class. Steps through with timestep _h
   RET step();
   RET step(float t);
   // Initialize the scene once created. Handles stuff like matrix initialization.
   RET Init();
   // Check whether the scene has ended
   bool hasEnded();
   // Things like activation per timestep,
   RET prefill();
   // Fill up the matrices
   RET fillRigid(SP<Rigid> rigid, int &ind);
   RET fill();
   // Update scene elements
   RET update(bool postStab, bool finalUpdate);
   RET scriptUpdate();

   RET setJointConstImp(SP<Joint>);
   RET setStrandConstImp(SP<Strand>);
   ftype getTime(){return t;}
   RET setStabilize(bool _stabilize);
   RET setSolver(Solver _solver);
   void setGravity(const Eigen::Vector3t &_gravity);
   RET setStrandAct(std::pair<std::string, ftype> actPair);
   RET setStrandAct(std::string, ftype);
   RET     setWrenchVal(std::pair<std::string, V6> wrenchPair);
   RET     setWrenchVal(std::string wrenchName, V6 wrenchPair);
   RET     clearWrench (std::string wrenchName);
   RET     setRigidScVel(std::pair<std::string, V6> wrenchPair);
   RET     setRigidScVel(std::string wrenchName, V6 wrenchPair);
   RET     clearRigidScVel(std::string wrenchName);

   // Remove unused points for the scene, which may be dangling without any passing strand cause of
   // mistakes in the scene design.
   RET removeUnusedPoints();

   // Accumulate results for large timestep stepped with smaller timestep.
   // Particularly used for finding the constraint force averaged over the timestep.
   RET accumulate();
   RET cleanAcc();

   RET setLabel(std::string _label){label = _label; return R::Success;}
   RET getLabel(std::string &_label){_label=label; return R::Success;}
   const char* getLabel(){return label.c_str();}
   inline bool isDebug(){return bDebug;}
   inline bool isMatOut(){return bMatOut;}
   // std::optional<std::pair<std::string, std::string>> getState();
   std::string getStateAttrs();
   std::optional<std::string> getState();
   void setFailed();
   bool checkFailed();
   friend bool set_initial_state(void* _scene, ftype* _x, ftype* _v, int len, float t);
};
