#pragma once
#include <include_common.hpp>

class SceneAttr{
public:
   Eigen::Vector3t gravity;
   unsigned int nPoints;
};
