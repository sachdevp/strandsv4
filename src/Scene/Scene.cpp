/**
   @auth: PrashantSachdeva
   @desc:   Main class for driving the simulation.
*/

#include <Scene/Scene.hpp>
#include <Joint.hpp>
#include <JointStiffness.hpp>
#include <JointDamping.hpp>
#include <DynObj/Rigid.hpp>
#include <DynObj/PointL.hpp>
#include <DynObj/Strand.hpp>
#include <Force.hpp>
#include <MatrixCentral.hpp>
#include <Scene/SceneAttr.hpp>
#include <DynObj/WorldObject.hpp>
#include <DoF/DoF.hpp>
#include <SE3.hpp>
#include <StopWatch/StopWatch.hpp>
#include <SceneConstants.hpp>
#include <Wrench.hpp>

#include <fstream>
#include <sstream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>

#define CHECK_SCENE_INITIALIZED(str)     CHECK_INITIALIZED(str, Scene)
#define CHECK_SCENE_NOT_INITIALIZED(str) CHECK_NOT_INITIALIZED(str, Scene)

using namespace std;
using namespace Eigen;
using namespace Strands;
#ifdef PROFILING
StopWatch* Scene::sw;
#endif
Scene::Scene(){
#ifdef PROFILING
   Scene::sw = new StopWatch("");
#endif
   sceneAttr     = new SceneAttr();
   matrices      = new MatrixCentral();
   InitWorld();
   t             = 0.0;
   Dt            = 0.0;
   nSteps        = 0.0;
   bStabilize    = false;
   bDebug        = false;
   bDebugAcc     = false;
   bSolverSet    = false;
   bInit         = false;
   bFailed = false;
   bMatOut = false;
   Eigen::setNbThreads(1);
   log_verbose("Scene object created");
}

void Scene::addRigid(SP<Rigid> _rigid){
   vRigids.push_back(_rigid);
   _rigid->addedToScene();
}

void Scene::addStrand(SP<Strand> _strand){
   vStrands.push_back(_strand);
   _strand->addedToScene();
}

void Scene::addJoint(SP<Joint> _joint){
   vJoints.push_back(_joint);
   _joint->addedToScene();
}

void Scene::addPoint(SP<PointL> _point){
   vPoints.push_back(_point);
   _point->addedToScene();
}

void Scene::addDoF(DoF* _dof){
   vDoFs.push_back(_dof);
   _dof->addedToScene();
}

void Scene::addForce(SP<Force> _force){
   vForces.push_back(_force);
   _force->addedToScene();
}

const V3 &Scene::getGravity() const{return sceneAttr->gravity;}

void Scene::setGravity(const V3 &_gravity){
   sceneAttr->gravity = _gravity;
   log_verbose("Scene gravity set to: %s", MS_T(sceneAttr->gravity));
}

RET Scene::removeUnusedPoints(){
   vector<int> deleteDoFs;
   SPVEC<PointL> usedPoints;
   for(auto point: vPoints){
      if(point->isUsed()){
         usedPoints.push_back(point);
         continue;
      }
      auto pointDoF = point->getDoF();
      int dof_idx = 0;
      for(auto dof: vDoFs){
         dof_idx++;
         if(dof == pointDoF){
            deleteDoFs.push_back(dof_idx);
         }
      }
      // NOTE Do not need to delete, since shared_ptr goes out of use and
      // will try to delete this. Not deleting avoids undefined behavior of
      // double deletion
      // delete point.get();
   }
   // If no points need to be deleted, then nothing to be done.
   if(deleteDoFs.size()==0) return R::Success;
   vector<DoF*> usedDoFs;
   int jj=0;
   for(int ii=0; ii<vDoFs.size(); ii++){
      if(jj<deleteDoFs.size() && ii == deleteDoFs[jj]){
         // DoF to be deleted. Do not push_back
         delete vDoFs[ii];
         jj++;
      } else{
         usedDoFs.push_back(vDoFs[ii]);
      }
   }
   vPoints.swap(usedPoints);
   vDoFs.swap(usedDoFs);
   return R::Success;
}

RET Scene::Init(){
   log_verbose("Initializing scene");
   // TODO Test this
   // this->removeUnusedPoints();

   // R for rigid, P for point and D for DoF
   int indR=0, indP = 0, indD = 0;
   int RFinalInd;
   for(auto rigid: vRigids){
      rigid->Init();
      rigid->setIndex(indR);
      DoF* dof = rigid->getDoF();
      dof->setIndex(indR);
      indR+=6;
   }

   RFinalInd = indR;
   indP = indR;
   indD = indR;
   for(auto point: vPoints){
      point->Init();
      point->setIndex(indP);
      DoF* dof = point->getDoF();
      dof->setIndex(indD);
      indP += 3;
      indD += dof->getnReduced();
   }

   sceneAttr->nPoints = (uint)vPoints.size();
   for(auto joint: vJoints){
      SP<JointStiffness> jStiff;
      SP<JointDamping> jDamp;
      RCHECKRET(joint->initStiffness(jStiff));
      
#ifdef EXPLICIT_JOINT_DAMPING
      RCHECKRET(joint->initDamping(jDamp));
#else
      RCHECKRET(joint->initDamping());
#endif
      // Add joint stiffness to scene forces.
      vForces.push_back(static_pointer_cast<Force>(jStiff));

#ifdef EXPLICIT_JOINT_DAMPING
      vForces.push_back(static_pointer_cast<Force>(jDamp));
#endif
      joint->Init();
   }

   for(auto force: vForces){force->Init();}

   for(auto strand: vStrands){
      strand->setSceneAttr(sceneAttr);
      RCHECKERROR(strand->Init(), "Could not initialize strand.");
      strand->setLIndex(indR);
      indR += strand->getLSize();
   }

   for(auto strand: vStrands){
      strand->setEIndex(indR);
      indR += strand->getESize();
   }

   for(auto dof: vDoFs){RCHECKRET(dof->Init());}

   this->InitMatrices();
   int SPInd = 0;
   int tmpSize = 0;
   for(auto strand: vStrands){
      // Place the strand to points Jacobian
      tmpSize = strand->getLSize();
      BlockMXt mat = matrices->JRL.block(RFinalInd+SPInd, 0, tmpSize, matrices->nRL);
      strand->fillJSP(mat);
      SPInd += tmpSize;
   }

   vector<int> vIdx, vLen;
   vector<BlockVXt> blocks;
   for(auto force: vForces){
      blocks.clear();
      force->getIndices(vIdx, vLen);
      RCHECKERROR_B(vIdx.size()!=vLen.size(), "Size of index and length vectors don't match.");
      for(auto i=0; i<vIdx.size(); i++){
         blocks.push_back(matrices->f.segment(vIdx[i], vLen[i]));
      }
      RCHECKRET(force->setForceBlocks(blocks));
      if(IS_MATRIX_NULL(matrices->f)) log_err("Force is null after init %s.", force->getCName());
   }
   log_verbose("Scene initialized");

   assert(indR == matrices->getFullSize());
   assert(indP == matrices->getnRL());
   assert(indD == matrices->getnR());
   bInit = true;
   return R::Success;
}

RET Scene::computeSizes(int &nFull, int &nRL, int &nR, int &nC_r, int &nC_f, int &nC_ir, int &nC_if, int &nJDoFs){
   uint nRigid = vRigids.size()*6;
   // nL does not have any overlapping points
   uint nL = vPoints.size()*3;
   // nL_strand includes repeated points too
   uint nL_strand = 0;
   uint nE_strand = 0;

   nC_ir  = 0;
   nC_if  = 0;
   nC_r   = 0;
   nC_f   = 0;
   nJDoFs = 0;
   for(auto strand: vStrands){
      // NOTE The fixed E values are also included in the complete mass matrix.
      // Jacobian will take care of that.
      nE_strand += strand->getESize();
      nL_strand += strand->getLSize();
      // Full space constraints for inextensibility
      // TODO This can be lesser. Define and use getnMaxActiveSegments
      if(strand->isInextensible()) nC_if += strand->getnSegments();
   }
   int nReducedL = 0;
   for(auto dof: vDoFs) nReducedL += dof->getnReduced();

   // Joints to be added to reduced constraint rows;
   for(auto joint: vJoints){
      nC_r   += joint->getnC();
      nJDoFs += 6 - joint->getnC();
      nC_ir  += joint->getnCi();
   }

   // DoFs add to constraint rows if scripted
   for(auto dof: vDoFs) nC_r += dof->isScripted()*dof->getnReduced();

   nR  = nReducedL;
   nRL = nRigid + nL;

   // nFull has frames for rigids (6 dof), and 4 for each strand node.
   nFull = nRigid + nL_strand + nE_strand;
   log_verbose("Matrix sizes: %d, %d, %d, %d, %d", nFull, nRL, nR, nC_r, nC_f);
   return R::Success;
}

void Scene::InitMatrices(){
   int nFull, nRL, nR, nC_r, nC_f, nC_ir, nC_if, nJDoFs;
   if(bSolverSet==false){
      log_reg("Solver not set. Assuming linear solver.");
      this->setSolver(Solver::LINEAR);
   }
   this->computeSizes(nFull, nRL, nR, nC_r, nC_f, nC_ir, nC_if, nJDoFs);
   matrices->setMatrixSizes(nFull, nRL, nR, nC_r, nC_f, nC_ir, nC_if, nJDoFs);
   this->gatherVelocity();
}

RET Scene::gatherVelocity(){
   int ind=0;
   log_random("Only gathering rigid velocities");
   VectorXt v;
   for(auto rigid: vRigids){
      matrices->v.segment<6>(ind) = rigid->getv();
      ind+=6;
   }
   return R::Success;
}

void Scene::InitWorld(){
   mWorld = WorldObject::creator();
   log_verbose("Created WorldObject: %d", mWorld->getId());
}

bool Scene::hasEnded(){return this->isEnd;}

void Scene::endScene(){this->isEnd = true;}

RET Scene::getJointIneqDrift(MAPSTRV3 &vJIneqDrifts){
   vJIneqDrifts.clear();
   int ce, ci;
   VectorXt ineqDrift;
   for(auto joint: vJoints){
      ci = joint->getnExceeded();
      ineqDrift.setZero(ci);
      if(ci!=0) joint->computeIneqDrift(ineqDrift);
      vJIneqDrifts.insert(make_pair(joint->getName(), VectorXt::Zero(0)));
   }
   return R::Success;
}

// Returns the vector of drift
RET Scene::getRedDrift(VectorXt &eqDrift, VectorXt &ineqDrift){
   int ne = 0, ni = 0, ce, ci;
   for(auto joint: vJoints){
      ce = joint->getnC();
      ci = joint->getnExceeded();
      VectorBlock<VectorXt, Dynamic> dBlock  =   eqDrift.segment(ne, ce);
      VectorBlock<VectorXt, Dynamic> diBlock = ineqDrift.segment(ni, ci);
      joint->computeEqDrift(dBlock);
      joint->computeIneqDrift(diBlock);
      ne += ce;
      ni += ci;
   }
   for(auto dof: vDoFs){
      if(!dof->isScripted()) continue;
      ce = dof->getnReduced();
      VectorBlock<VectorXt, Dynamic> dBlock = eqDrift.segment(ne, ce);
      RCHECKERROR(dof->computeScriptDrift(dBlock), "Drift computation failed.");
      ne += ce;
   }
   return R::Success;
}

RET Scene::getFullDrift(VectorXt &eqDrift, VectorXt &ineqDrift){
   int iStrand=0;
   RET r;
   int cIdx = 0;
   for(auto strand: vStrands){
      // Check if inextensibility constraint was active in the last timestep.
      if(!strand->isInexActive()) continue;
      int nS = strand->getnActiveSegments();
      BlockVXt g = ineqDrift.segment(cIdx, nS);
      RCHECKERROR(strand->fillInexDrift(g), "Could not get drift for strand %s", strand->getCName());
      cIdx+=nS;
      iStrand++;
   }
   return R::Success;
}

RET Scene::getDrift(VectorXt &drift){
   VectorXt vREDrift, vRIDrift, vFEDrift, vFIDrift;
   vREDrift.setZero(indRECons);
   vFEDrift.setZero(indFECons);
   vRIDrift.setZero(indRICons);
   vFIDrift.setZero(indFICons);
   RCHECKERROR(getFullDrift(vFEDrift, vFIDrift), "Could not get full drift.");
   RCHECKERROR(getRedDrift(vREDrift, vRIDrift), "Could not get reduced drift.");
   // Full and reduced equality constraints
   int idx = 0, sz = vREDrift.size();
   drift.segment(idx, sz) = vREDrift;
   idx+=sz; sz = vFEDrift.size();
   drift.segment(idx, sz) = vFEDrift;

   // Full and reduced inequality constraints
   idx+=sz; sz = vRIDrift.size();
   drift.segment(idx, sz) = vRIDrift;
   idx+=sz; sz = vFIDrift.size();
   drift.segment(idx, sz) = vFIDrift;

   RCHECKERROR_B((sz+idx)!=drift.size(), "Incorrect drift size.");
   return R::Success;
}

RET Scene::blockFill(MatrixXt &M_out, MatrixXt &M_in, int indL, int indE, int nL, int nE){
   M_out.block(indL, indL, nL, nL) = M_in.block(0, 0, nL, nL);
   M_out.block(indE, indE, nE, nE) = M_in.block(nL, nL, nE, nE);
   M_out.block(indL, indE, nL, nE) = M_in.block(0, nL, nL, nE);
   M_out.block(indE, indL, nE, nL) = M_in.block(nL, 0, nE, nL);
   return R::Success;
}

// TODO Break up this function into different fill functions.
RET Scene::fillRigid(SP<Rigid> rigid, int &ind){
   M3 rigidR = SE3::getRotation(rigid->getTransform());
   // Run only once.
   rigid->computeMassMatrix();
   M6 rigidMass = rigid->getMassMatrix();
   // TODO Should not fill rigidmass again and again.
   matrices->M.block<6, 6>(ind,ind) = rigidMass;
   M3 RMass = rigidMass.bottomRightCorner(3,3);// block<3,3>(3,3);
   matrices->f.segment<3>(ind+3) = RMass * rigidR.transpose() * grav();
   if(IS_MATRIX_NULL(matrices->f)){
      log_err("Force is null after filling %s.", rigid->getCName());
   }
   ind+=6;
   // NOTE Ignoring coriolis force for hand simulation.
   // M6 crossphi;
   // V6 coriolis;
   // V3 omega = rigid->getv().head<3>();
   // V3 v = rigid->getv().tail<3>();
   // crossphi<<SE3::computeBracket(omega), M3::Zero(),
   //    SE3::computeBracket(v), SE3::computeBracket(omega);
   // coriolis = crossphi.transpose() * rigidMass*rigid->getv();
   return R::Success;
}

RET Scene::fill(){
#ifdef PROFILING
   auto fn_sw = StopWatch::find("|Step|Fill");
   fn_sw->tic();
#endif
   int ind = 0;
   vRILabels.clear();
   vRELabels.clear();
   vRILen.clear();
   vRELen.clear();
   vFILabels.clear();
   vFELabels.clear();
   vFILen.clear();
   vFELen.clear();
   // Fill rigid mass and force.
   // Also, coriolis force
   for(auto rigid: vRigids){fillRigid(rigid, ind);}

   int sz = 0;
   uint nRigid = vRigids.size()*6;
   uint totalStrandLSize = 0;
   uint nLStrand, nEStrand;
   for(auto strand: vStrands){
      nLStrand = strand->getLSize();
      totalStrandLSize += nLStrand;
   }
   int qrow = totalStrandLSize + nRigid;
   int indL, indE;
   // Full constraints are applied with Jacobian
   indFECons = 0;
   indFICons = 0;
   indRECons = 0;
   indRICons = 0;
   matrices->vActiveIneq.clear();

   // JRL Identity for rigid bodies.
   // TODO Do not need to keep setting this.
   matrices->JRL.block(0, 0, nRigid, nRigid).setIdentity();
#ifdef PROFILING
   auto strands_sw = StopWatch::find("|Step|Fill|Strands");
   strands_sw->tic();
#endif

   // TODO Break out into another function
   for(auto strand: vStrands){
      strand->recordPrevStrain();
      RCHECKRET(strand->computeMK());
      strand->computeForceVector();
      strand->computeJacobian();
      MatrixXt strandMass      = strand->getMassMatrix();
      MatrixXt strandStiffness = strand->getStiffnessMatrix();
      VectorXt force           = strand->getForceVector();
      if(IS_MATRIX_NULL(force)){
         log_err("Force for %s is null.", strand->getCName());
      }

      nLStrand = strand->getLSize();
      nEStrand = strand->getESize();

      indL = strand->getLIndex();
      RCHECKERROR_B(indL==-1, "Index L is -1");

      indE = strand->getEIndex();
      RCHECKERROR_B(indE==-1, "Index E is -1");

      blockFill(matrices->M, strandMass,      indL, indE, nLStrand, nEStrand);
      blockFill(matrices->K, strandStiffness, indL, indE, nLStrand, nEStrand);

      // TODO Shift matrix block fetch to MatrixCentral
      if(strand->isInextensible()){
         // NOTE strand's @bInexActive is cleared.
         RCHECKERROR_B(matrices->isLinearSolve(),
                       "Cannot run inextensibility constraints with Linear solver.");

         if(matrices->isQPSolve()){
            log_verbose("Tendon %s is taut", strand->getCName());
            matrices->vActiveIneq.push_back(strand->getName()+"-Inex-Full");
            int nS = strand->getnActiveSegments();
            BlockMXt GX = matrices->C_if.block(indFICons, indL, nS, nLStrand);
            BlockMXt GS = matrices->C_if.block(indFICons, indE, nS, nEStrand);
            BlockVXt g  = matrices->c_iuf.segment(indFICons, nS);
            RCHECKERROR(strand->fillInexCons(GX, GS, g, matrices->h),
                        "Failed to fill inextensibility inequality constraints LHS.");
            // RCHECKERROR(strand->fillInexVelUB(g),
            //             "Failed to fill inextensibility inequality constraint RHS.");
            // matrices->c_iuf[indFICons] = g;
            for(int iS=0; iS<nS; iS++){
               matrices->c_ikf[indFICons+iS] = IneqKey::UB;
            }
            RCHECKERROR(strand->setInexActive(indFICons, nS),
                        "Could not set inex constraint active for strand %s.", strand->getCName());
            indFICons += nS;
            vFILabels.push_back(strand->getName());
            vFILen.push_back(nS);
         }
      }

      matrices->f.segment(indL, nLStrand) = force.segment(0       , nLStrand);
      matrices->f.segment(indE, nEStrand) = force.segment(nLStrand, nEStrand);
      RCHECKERROR_B(IS_MATRIX_NULL(matrices->f), "Force is null after %s.", strand->getCName());

      BlockMXt strandJQ = matrices->JRL.block(qrow, 0, nEStrand, matrices->nRL);
      strand->fillJQ(strandJQ);
      // Increment counters
      qrow += nEStrand; // Eulerian DoFs
      ind  += sz;
   }
#ifdef PROFILING
   strands_sw->toc();
#endif

   ind = 0;
   for(auto dof: vDoFs){
      dof->computeJacobian();
      int Gr, Gc, Or, Oc, nOr, nOc;
      RCHECKERROR(dof->getIndex(Gr, Gc, Or, Oc, nOr, nOc),
                  "Could not get indices for DoF");

      /** Set Gamma and Omega **/
      // If Gamma is to be set
      if(IS_VALID_INDEX(Gr) && IS_VALID_INDEX(Gc)){
         dof->fillGamma(matrices->JR.block(Gr, Gc, nOr, 6));
      }
      // If Omega is to be set
      if(IS_VALID_INDEX(Or) && IS_VALID_INDEX(Oc)){
         dof->fillOmega(matrices->JR.block(Or, Oc, nOr, nOc));
      }
      int nd = dof->getnReduced();
      if (nd!=0) {
         // Fill the reduced position
         matrices->x_r.segment(ind, nd) = dof->getx();
         ind+=nd;
      }
   }
#ifdef PROFILING
   auto joints_sw = StopWatch::find("|Step|Fill|Joints");
   joints_sw->tic();
#endif
   int ia, ib, c_a, c_b;
   int iJDamp=0;
   bool AExists;
   int iJoint=0;
   for(auto joint: vJoints){
      // cout<<iJoint<<" "<<joint->getName()<<endl;
      iJoint++;
      if(!joint->isActive()) continue;
      joint->computeConstraint();
      AExists = true;
      int nCJoint = joint->getnC();
      // Ci is same as uC
      int nCiJoint = joint->getnCi();
      joint->getCIndices(ia, ib);
      joint->getCDoFs(c_a, c_b);
      if(ia==-1) AExists = false;

      // Unconstrained rows for damping and inequality constraints
      MatrixXt uCa, uCb;
      VectorXt uc;
      if(AExists) uCa = joint->getuCa();
      uCb = joint->getuCb();
      uc  = joint->getuc();
      int nFree = uc.rows();

      // cout<<indRECons<<" "<<ia<<" "<<nCJoint<<" "<<c_a<<endl;
      RCHECKERROR_B((indRECons+nCJoint)>matrices->C_r.rows(), "Too many reduced constraint rows.");

      if(AExists) matrices->C_r.block(indRECons, ia, nCJoint, c_a) = joint->getiCa();
      // 6-c_b is for ignoring angular DoFs for the reduced DoF of point like dynamic objects
      matrices->C_r.block(indRECons, ib, nCJoint, c_b) = joint->getiCb().block(0, 6-c_b, nCJoint, c_b);
      matrices->c_r.segment(indRECons, nCJoint) = joint->getic();
      joint->setEIdx(indRECons);
      indRECons += nCJoint;
      vRELen.push_back(nCJoint);
      vRELabels.push_back(joint->getName());

      // Damping related matrices
#ifndef EXPLICIT_JOINT_DAMPING
      if(AExists) matrices->mAdjMat.block(iJDamp, ia, nFree, c_a) = uCa;
      matrices->mAdjMat.block(iJDamp, ib, nFree, c_b) = uCb;
      matrices->mJDampMat.block(iJDamp, iJDamp, nFree, nFree) = joint->getDamping().asDiagonal();
      iJDamp += nFree;
#endif

      RCHECKRET(joint->setIIdx(indRICons));
      array<int,  3> exceeded;
      array<bool, 3> constrained;
      RCHECKRET(joint->getExceeded(exceeded, constrained));
      int e_idx = -1, iFree=-1, nEx = 0;
      // @e_idx is the iFree in the commented code. It gives the adjoint matrix row index for the
      // inequality constraint in question.
      for(int e: exceeded){
         // Setting the values in C_i
         e_idx++;
         if(constrained[e_idx]) continue;
         iFree++;
         if(e!=1 && e!=2) continue;
         if(AExists) matrices->C_ir.block(indRICons, ia, 1, 6) = uCa.row(iFree);
         matrices->C_ir.block(indRICons, ib, 1, 6) = uCb.row(iFree);
         switch(e){
         case 1:
            // Lower limit exceeded
            // @ci gives velocity limits
            matrices->c_ilr(indRICons) = uc(iFree);
            matrices->c_ikr[indRICons] = IneqKey::LB;
            matrices->vActiveIneq.push_back(joint->getName()+"-Lower-Red");
            break;
         case 2:
            // Upper limit exceeded
            // @ci gives velocity limits
            matrices->c_iur(indRICons) = uc(iFree);
            matrices->c_ikr[indRICons] = IneqKey::UB;
            matrices->vActiveIneq.push_back(joint->getName()+"-Upper-Red");
            break;
         }
         indRICons++;
         nEx++;
      }
      // Number of exceeded for this joint.
      if(nEx){
         vRILabels.push_back(joint->getName());
         vRILen.push_back(nEx);
      }
   }

#ifdef PROFILING
   joints_sw->toc();
#endif

#ifdef PROFILING
   auto scripted_sw = StopWatch::find("|Step|Fill|Scripted");
   scripted_sw->tic();
#endif
   // Implement scripted velocities
   int Gr, Gc, Or, Oc, nOr, nOc;
   for(auto dof: vDoFs){
      // Skip if not scripted
      if(!dof->isScripted()) continue;
      VectorXt v_sc = dof->getScriptedVel();
      int nc = (int)v_sc.rows();
      dof->getIndex(Gr, Gc, Or, Oc, nOr, nOc);
      matrices->C_r.block(indRECons, Oc, nc, nc) = MatrixXt::Identity(nc, nc);
      matrices->c_r.segment(indRECons, nc) = v_sc;
      indRECons+=nc;
      vRELabels.push_back(dof->getTargetName());
      vRELen.push_back(nc);
   }
#ifdef PROFILING
   scripted_sw->toc();
#endif

   // Use forces
#ifdef PROFILING
   auto forces_sw = StopWatch::find("|Step|Fill|Forces");
   forces_sw->tic();
#endif
   for(auto force:vForces){
      force->fillForce();
   }

#ifdef PROFILING
   forces_sw->toc();
#endif

#ifdef PROFILING
   auto jac_sw = StopWatch::find("|Step|Fill|Jacobian");
   jac_sw->tic();
#endif
   matrices->J = matrices->JRL * matrices->JR;

#ifdef PROFILING
   jac_sw->toc();
#endif

   RCHECKERROR(matrices->combineConstraints(indRICons, indFICons),
               "Failed to combine reduced and full constraint matrices.");

#ifdef PROFILING
   fn_sw->toc();
#endif
   return RET::Success;
}

RET Scene::updateDebug(){
   bDebug = false;
   for(pair<double, bool> debugPair: mDebugMap){
      if(debugPair.first>t){break;}
      bDebug = debugPair.second;
   }

   if(bDebug && bMatOut){
      setMatLogFile(to_string(nSteps));
      matrices->startLogging();
   } else{
      matrices->stopLogging();
   }
   return R::Success;
}

RET Scene::clearForStep(){
   // Clear matrices
   for(auto strand: vStrands){RCHECKERROR(strand->clearForStep(this->t), "Could not clear strand.");}
   for(auto rigid : vRigids ){RCHECKERROR(rigid->clearForStep(this->t) , "Could not clear rigid."); }
   for(auto joint : vJoints ){RCHECKERROR(joint->clearForStep(this->t) , "Could not clear joint."); }
   for(auto force : vForces ){RCHECKERROR(force->clearForStep(this->t) , "Could not clear force."); }
   updateDebug();
   bUpdated = false;
   return matrices->clear();
}

RET Scene::setMatLogFile(string suffix){
   auto r = matrices->setFilename(mDataOutDir + "mat" + suffix + ".m");
   if(RFAIL(r)){
      log_warn("Could not set filename for matrix output.");
      matrices->stopLogging();
      return R::Failure;
   }
   return R::Success;
}

RET Scene::prefill(){
   for(auto strand: vStrands){strand->prefill();}

   return R::Success;
}

RET Scene::step(){
   nSteps++;

   // Steps to be done on matrices
#ifdef PROFILING
   StopWatch* fn_sw = StopWatch::find("|Step");
   fn_sw->tic();
#endif

   clearForStep();

   prefill();
   RCHECKERROR(fill(), "Failed to fill the matrix at %f seconds", t);

   // Gauss principle of least constraint
   bool preStab=false, postStab = true, finalUpdate=false;
   // If not stabilizaing, final update for the step directly.
   if(!bStabilize){finalUpdate = true;}
   bool bDyn = RSUCC(matrices->dynamics());
   if(bDyn){ 
      RCHECKERROR(update(preStab, finalUpdate), "Update failed at %f seconds", t);
      // RCHECKERROR(update(),"Update failed at %f seconds", t);

      VectorXt c_drift;
      c_drift.setZero(indRICons + indRECons + indFICons + indFECons);
      // c_drift.setZero(indRECons + indFECons);
      if(bStabilize) {
         RET r = getDrift(c_drift);
         if(r == R::Failure) {
            log_warn("Stabilization failed: Could not obtain drift vector");
         } else{
            // Post stabilization according to [Cline and Pai, 2003]
            // Calculate c_drift
            log_verbose("Drift: %s", MS_T(c_drift));
            RCHECKERROR(matrices->stabilize(c_drift),
                        "Post stabilization failed at %f seconds", t);
         }
         // If stabilizing, final update for the step happens here.
         finalUpdate = true;
         RCHECKERROR(update(postStab, finalUpdate), "Update after stabilization failed at %f seconds", t);
      }

      ftype h = matrices->getTimestep();
      this->t = this->t + h;
      this->Dt = this->Dt + h;
      if(t > this->tEnd){endScene();}
   } else{
      log_err("Failed to compute dynamics at %f seconds", t);
   }
   RCHECKERROR(debugOutput(), "Failed to output debug log.");
   if(bDebug && bMatOut) RCHECKERROR(matrices->closeLog(), "Logging issues.");
   if(!bDyn) return R::Failure;
#ifdef PROFILING
   fn_sw->toc();
#endif

#ifdef LOG_PROFILING
   StopWatch::printAll();
#endif
   return R::Success;
}

RET Scene::setDebug(KeysMap<bool> _debugMap, string filename, bool replace){
   bool debugAcc = false;
   for(pair<double, bool> debugPair: _debugMap){
      debugAcc |= debugPair.second;
   }
   bDebugAcc = debugAcc;
   if(!bDebugAcc) return R::Success;

   log_warn("Debugging: %s, %d", filename.c_str(), replace);
   ifstream f(filename);
   if(f.good()){
      log_warn("Log file already exists.");
      if(!replace) return R::Failure;
   }

   RCHECKERROR_B(debugAcc && filename.empty(), "File not specified. Debugging needs log file input.");
   mDebugMap  = _debugMap;
   mDebugFile = filename; 
   // Replace file if already exists
   outFile.open(mDebugFile, ios_base::out);
   bDebugSet = true;
   return R::Success;
};

bool Scene::willDebug(){
   if(bDebugSet && bDebugAcc) return true;
   return false;
}

RET Scene::debugLambda(){
   outFile<<matrices->nC_r  <<" "<<matrices->nC_f  <<" "
          <<matrices->cnC_ir<<" "<<matrices->cnC_if<< endl;
   outFile<<MS_T(matrices->lambda)<<endl;
   vector<string>         labels     = {"RE",      "FE",      "RI",      "FI"};
   vector<vector<string>> labelLists = {vRELabels, vFELabels, vRILabels, vFILabels};
   vector<vector<int>>    lenLists   = {vRELen,    vFELen,    vRILen,    vFILen};

   int lambda_idx=0, lambda_len;
   for(int idx=0; idx< 4; idx++){
      outFile<<labels[idx]<<": ";
      lambda_len = 0;
      for(int lidx=0; lidx<labelLists[idx].size(); lidx++){
         outFile<<labelLists[idx][lidx]<<" "<<lenLists[idx][lidx]<<", ";
         lambda_len += lenLists[idx][lidx];
      }
      outFile<<endl;
      outFile<<MS_T(matrices->lambda.segment(lambda_idx, lambda_len))<<endl;
      lambda_idx+=lambda_len;
   }
   return R::Success;
}

RET Scene::debugOutput(){
   if(!bDebug) return R::Success;
   if(mDebugFile.empty() || !outFile.is_open()) return R::Failure;
   // outFile<< "Absolute: " << matrices->abs_error   <<
   //    "\tRelative: "      << matrices->rel_err     <<
   //    "\tConstraints: "   << matrices->const_error << endl;
   outFile<< "---------- " << nSteps << ": " << t << " ----------" <<endl;
   if(matrices->isLinearSolve())
      outFile<< "Error(abs, rel, const): "
             << matrices->abs_error   << " "
             << matrices->rel_err     << " "
             << matrices->const_error << endl;
   if(matrices->isQPSolve()) outFile<< "Error(const): " << matrices->const_error << endl;

   debugLambda();

   string output;
   for(auto rigid : vRigids){
      rigid->debugOutput(output);
      outFile<<output;
   }
   outFile<<endl;
   for(auto point : vPoints){
      point->debugOutput(output);
      outFile<<output;
   }
   outFile<<endl;
   for(auto strand : vStrands){
      strand->debugOutput(output);
      outFile<<output;
   }
   outFile<<endl;
   for(auto joint : vJoints){
      joint->debugOutput(output);
      outFile<<output;
   }
   outFile<<endl;

   return R::Success;
}

RET Scene::update(bool postStab, bool finalUpdate){
   // Assumes the right structure for the matrix.
   // Rigid bodies -> Reduced DoFs
   if(bUpdated){
      if(postStab) log_warn("Stabilizing in second round of update.");
      else log_err("Extra update run.");
   }
   int ind = 0;

   // i. For each dof, get the reduced co-ordinate length and set the
   // corresponding reduced velocity.
   // ii. Then set the velocity for each dof target.
   for(auto dof : vDoFs ){
      int nd = dof->getnReduced();
      if (nd!=0) {
         VectorXt dofv = matrices->v_r.segment(ind, nd);
         VectorXt dofx = matrices->x_r.segment(ind, nd);
         VectorXt dofdx, dofdx_stab;
         if(postStab) dofdx_stab = matrices->dx_r_stab.segment(ind, nd);
         else dofdx = matrices->dx_r.segment(ind, nd);
         dof->setv(dofv);
         dof->setx(dofx);
         if(postStab) dof->setdx_stab(dofdx_stab);
         else dof->setdx(dofdx);
         ind+=nd;
      }
      if(finalUpdate) dof->scriptUpdate(matrices->h);
      dof->setTargetVelocity();
      dof->setTargetCoordinates();
   }

   for(auto rigid: vRigids){
      RCHECKERROR(rigid->update(), "Failed to update rigid %s", ObjName(rigid));
   }

   for(auto joint: vJoints){
      RCHECKERROR(joint->update(), "Failed to update joint %s", ObjName(joint));
      if(!postStab)
         RCHECKERROR(setJointConstImp(joint), "Failed to set joint constraint force for %s", ObjName(joint));
   }
   for(auto force: vForces){
      RCHECKERROR(force->update(), "Failed to update force %s", ObjName(force));
   }
   for(auto strand: vStrands){
      // Active segments are checked only in final update.
      auto name = ObjName(strand);
      if(finalUpdate){
         int eIdx, nNodes;
         RCHECKERROR(strand->update(), "Update failed for strand %s.", name);
      }
      else{RCHECKERROR(strand->update_s(), "Update failed for strand %s.", name);}
      // Set constraint impulse only prior to stabilization.
      if(!postStab)
         RCHECKERROR(setStrandConstImp(strand),
                     "Failed to set strand constraint force for %s", name);
   }
   bUpdated = true;
   return RET::Success;
}

RET Scene::scriptUpdate(){
   for(auto dof: vDoFs) dof->scriptUpdate(matrices->h);
   return R::Success;
}

RET Scene::setStrandConstImp(SP<Strand> strand){
   int ineqIdx, ineqRow, nInex;
   if(!strand->isInexActive()) return R::Success;
   // NOTE ineqIdx corresponds to the first segment index.
   RCHECKERROR(strand->getInexIndex(ineqIdx, nInex),
               "Failed to get constraint index for strand %s", ObjName(strand));
   // log_warn("Inex index for %s is %d", ObjName(strand), ineqIdx);
   matrices->getFIConstRow(ineqRow, ineqIdx);

   ftype lambda_sum=0, lambda_stab_sum=0;
   for(int i=0; i<nInex; i++){
      lambda_sum+=matrices->lambda(ineqRow+i);
      lambda_stab_sum+=matrices->lambda_stab(ineqRow+i);
   }
   // log_warn("%s force: %d, %3.2e, %3.2e", strand->getCName(), nInex, lambda_sum, lambda_stab_sum);
   
   RCHECKERROR(strand->setConstImp(lambda_sum, lambda_stab_sum),
               "Failed to set constraint force for strand %s", ObjName(strand));

   return R::Success;
}

RET Scene::setJointConstImp(SP<Joint> joint){
   // Get the equality and inequality constraint indices in the corresponding reduced constraint
   // matrices before combineConstraints is called.
   int eq, eqRow, ineq, ineqRow;
   // Accumulation of constraint force over multiple steps
   RCHECKERROR(joint->getConstIndex(eq, ineq), "Failed to get constraint index for joint %s", ObjName(joint));
   // Get reduced eq constraint index in combined constraint matrix
   matrices->getREConstRow(eqRow, eq);
   // Get reduced ineq constraint index in combined constraint matrix
   matrices->getRIConstRow(ineqRow, ineq);

   uint nEq = joint->getnC();
   uint nIneq = joint->getnExceeded();
   // FIXME Use G^T*lambda, not just lambda
   RCHECKERROR(joint->setConstImp(matrices->lambda.segment(eqRow, nEq), matrices->lambda.segment(ineqRow, nIneq),
                                 matrices->lambda_stab.segment(eqRow, nEq), matrices->lambda_stab.segment(ineqRow, nIneq)),
               "Could not set joint constraint force for %s", joint->getCName());
   return R::Success;
}

SP<Object> Scene::getObjectById(uint id){
   if(IS_WORLD_ID(id)){return mWorld;}
   // Check rigids
   for(auto rigid: vRigids) if(rigid->getId() == id) return rigid;
   // Check points
   for(auto point: vPoints) if(point->getId() == id) return point;
   // Check strands 
   for(auto strand: vStrands) if(strand->getId() == id) return strand;
   // Check joints
   for(auto joint: vJoints) if(joint->getId() == id) return joint;
   log_err("No rigid body # %ud found", id);
   return nullptr;
}

SP<DynObj> Scene::getDynObj(uint id){
   // Check rigids
   for(auto rigid: vRigids) if(rigid->getId() == id) return rigid;
   // Check points
   for(auto point: vPoints) if(point->getId() == id) return point;
   // Check strands 
   for(auto strand: vStrands) if(strand->getId() == id) return strand;
   log_err("Not a dynamic object.");
   return nullptr;
}

SP<DynObj> Scene::getWorldObject(){return mWorld;}
RET Scene::setTimestep(ftype _h){return matrices->setTimestep(_h);}
RET Scene::setTime(ftype _t){t = _t; return R::Success;}
ftype Scene::getTimestep(){return matrices->getTimestep();}
RET Scene::setDamping(ftype _dmp){return matrices->setDamping(_dmp);}
RET Scene::setStab(ftype _stab){bStabilize = true; return matrices->setStab(_stab);}
RET Scene::setStabilize(bool _stabilize){bStabilize = _stabilize; return R::Success;}

RET Scene::setMatOutDir(const char* _dataOutDir){
   CHECK_SCENE_INITIALIZED("Cannot set matrix output boolean.");
   RCHECKERROR_B(strlen(_dataOutDir)==0, "Invalid data output directory.");
   bMatOut = true;
   mDataOutDir = _dataOutDir;
   return matrices->startLogging();
}

RET Scene::setSolver(Solver _solver){
   CHECK_SCENE_NOT_INITIALIZED("Cannot set solver.");
   switch(_solver){
   case Solver::LINEAR:
   case Solver::QPMOSEK:
      matrices->setSolver(_solver);
      break;
   default:
      log_err("Solver not recognized.");
      return R::Failure;
   }
   bSolverSet = true;
   return R::Success;
}

SP<DynObj> Scene::getRigid(int rigidId){
   if(IS_WORLD_ID(rigidId)){
      log_random("Fixing point to world.");
      return getWorldObject();
   }
   for(auto rigid: vRigids){ if(rigid->getRigidId() == rigidId){return rigid;}}
   return nullptr;
}

bool Scene::getDataOutDir(string &outString){
   if(bMatOut){outString = mDataOutDir; return true;}
   else{outString = ""; return false;}
}

bool Scene::getDebugFile(string &outFile){
   if(bMatOut){outFile = mDebugFile; return true;}
   else{outFile = ""; return false;}
}

RET Scene::accumulate(){
   for(auto joint : vJoints ){joint->accumulate(Dt);}
   for(auto strand: vStrands){strand->accumulate(Dt);}
   this->Dt = 0;
   return R::Success;
}

RET Scene::cleanAcc(){
   for(auto strand: vStrands){strand->cleanAcc();}
   return R::Success;
}

Strand* Scene::getStrand(std::string strandName){
   Strand* retStrand = nullptr;
   for(auto strand: vStrands){
      if(strand->getName() == strandName){
         retStrand = dynamic_cast<Strand*>(strand.get());
      }}
   return retStrand;
}

RET Scene::setStrandAct(std::string strandName, ftype actVal){
   auto strand = getStrand(strandName);
   return strand?strand->setAct(actVal, t):R::Failure;
}

RET Scene::setStrandAct(pair<std::string, ftype> actPair){
   return setStrandAct(actPair.first, actPair.second);
}

MAP<PointL> Scene::getPointsMap(){
   MAP<PointL> retMap;
   for(SP<PointL> point: vPoints){
      retMap[point->getPointId()] = point;
   }
   return retMap;
}

string Scene::getStateAttrs(){
   string stateAttrsStr;
   for(auto rigid: vRigids){
      stateAttrsStr += rigid->getName() + ".pos ";
   }

   for(auto joint: vJoints){
      array<ftype, 3> angles;
      array<int, 3> exceeded;
      array<bool, 3> constrained;
      stateAttrsStr += joint->getName() + ".ang ";
      joint->getExceeded(exceeded, constrained);
      array<char, 3> txt = {'x', 'y', 'z'};
      for(int i=0; i<3; i++){
         if(!constrained[i]){
            stateAttrsStr += joint->getName() + ".exceeded_" + txt[i]+" ";
         }}}

   for(auto strand: vStrands){
      stateAttrsStr += strand->getName() + ".strain ";
      if(strand->isInextensible())
         stateAttrsStr += strand->getName() + ".taut ";
   }

   return stateAttrsStr;
}

optional<string> Scene::getState(){
   string stateValsStr;
   M4 E;
   for(auto rigid: vRigids){
      E = rigid->getTransformExt();
      V3 pos, ang;
      SE3::getPosAngles(pos, ang, E);
      stateValsStr += string(MSF_T(ang)) + " " + string(MSF_T(pos)) + " ";
   }

   for(auto joint: vJoints){
      array<ftype, 3> angles;
      array<int, 3> exceeded;
      array<bool, 3> constrained;
      auto r = joint->getAngles(angles);
      if(RFAIL(r)){log_err("Could not get angles.");}
      stateValsStr += to_string(angles[2]) + " ";
      joint->getExceeded(exceeded, constrained);
      array<char, 3> txt = {'x', 'y', 'z'};
      for(int i=0; i<3; i++){
         if(!constrained[i]){
            stateValsStr += to_string(exceeded[i]) + " ";
         }}}

   for(auto strand: vStrands){
      stateValsStr += to_string(strand->getStrain()) + " ";
      float constForce;
      if(strand->isInextensible()){
         // strand->getConstForce(constForce);
         // stateValsStr += to_string(abs(constForce)>1e2) + " ";
         stateValsStr += to_string(strand->isTaut()) + " ";
      }
   }
   return stateValsStr;
}

void Scene::setFailed(){bFailed = true;}
bool Scene::checkFailed(){return bFailed;}

Wrench* Scene::getWrench(std::string wrenchName){
   Wrench* wrench = nullptr;
   for(auto force: vForces){
      if(force->getName() == wrenchName){
         wrench = dynamic_cast<Wrench*>(force.get());
      }}
   return wrench;
}

RET Scene::setWrenchVal(std::string wrenchName, V6 wrenchVal){
   auto wrench = getWrench(wrenchName);
   return wrench?wrench->setWrench(wrenchVal, this->t):R::Failure;
}

RET Scene::setWrenchVal(pair<std::string, V6> wrenchPair){
   return setWrenchVal(wrenchPair.first, wrenchPair.second);
}

RET Scene::clearWrench(std::string wrenchName){
   auto wrench = getWrench(wrenchName);
   return wrench?wrench->clearWrenchMap():R::Failure;
}

Rigid* Scene::getRigid(std::string rigidName){
   Rigid* retRigid = nullptr;
   for(auto rigid: vRigids){
      if(rigid->getName() == rigidName) retRigid = rigid.get();
   }
   return retRigid;
}

RET Scene::setRigidScVel(std::string rigidName, V6 vel){
   auto rigid = getRigid(rigidName);
   return rigid?rigid->setScVel(t, vel):R::Failure;
}

RET Scene::setRigidScVel(pair<std::string, V6> scVelPair){
   return setRigidScVel(scVelPair.first, scVelPair.second);
}

RET Scene::clearRigidScVel(std::string rigidName){
   auto rigid = getRigid(rigidName);
   return rigid?rigid->clearScVelMap():R::Failure;
}

RET Scene::setInitialInternalState(VectorXt x_r, VectorXt v_r){
   RCHECKERROR(matrices->setInitialState(x_r, v_r), "Could not set initial state for MatrixCentral.");
   RCHECKERROR(fill(), "Could not fill scene matrices.");
   bool preStab = false, finalUpdate = true;
   matrices->v = matrices->J*matrices->v_r;
   RCHECKERROR(matrices->setInitialState(x_r, v_r), "Could not set initial state for MatrixCentral.");
   RCHECKERROR(update(preStab, finalUpdate), "Could not update after setting state.");
   for(auto strand: vStrands){
      strand->checkTaut();
   }
   return R::Success;
}

RET Scene::getInitialInternalState(Eigen::VectorXt &x_r, Eigen::VectorXt &v_r){
   x_r = matrices->x_r;
   v_r = matrices->v_r;
   return R::Success;
}

RET Scene::getReducedMat(Eigen::MatrixXt &M_r){
   M_r = matrices->M_r;
   return R::Success;
}

RET Scene::getRHS(Eigen::VectorXt &rhs) {
   rhs = matrices->outRHS;
   return R::Success;
}
