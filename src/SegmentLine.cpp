#include <SegmentLine.hpp>
#include <DynObj/StrandAttr.hpp>

RET SegmentLine::computeSegmentMKf() {
   ftype rho = getDensity();
   Node *n0, *n1;
   RCHECKERROR(getNodes(n0, n1), "Could not get nodes");
   log_random("Node locations: \n %s \n %s",
              MS_T(n0->getx()), MS_T(n1->getx()));

   // Get the node and strand values
   V3    x1 = n1->getx();
   V3    x0 = n0->getx();
   ftype s1 = n1->gets();
   ftype s0 = n0->gets();

   // Get different summations
   V3    dx = x1 - x0;
   ftype ds = s1 - s0;

   // Calculate length, strain etc.
   ftype l = dx.norm() ;
   ftype epsilon = l/ds - 1.0f;

   // Vector3t dxhat = dx/l;
   M3 I = EYE3;

   log_random("Segment material length (ds): %.4f", ds);
   if(EQUALS(ds, 0)){
      log_err("Segment material length is too small between points %d, %d at %s, %s.", n0->getPointId(), n1->getPointId(), MS_T(x0), MS_T(x1));
      return R::Failure;
   }

   M3 mxx = ds*I;
   V3 mxs = dx;
   ftype mss = dx.dot(dx)/ds;
   tMLL[0] = (rho/3) *   2  * mxx;
   tMLL[1] = (rho/3) *        mxx;
   tMLE[0] = (rho/3) * (-2) * mxs;
   tMLE[1] = (rho/3) * (-1) * mxs;
   // NOTE Getting around the ftype to 1x1 matrix problem
   tMEE[0](0,0) = 2 * (rho/3) * mss;
   tMEE[1](0,0) =     (rho/3) * mss;

   // log_random("l, ds, eps:\t%f, %f, %f", l, ds, epsilon);
#ifdef STIFFNESS
   RCHECKERROR(strandAttr->material->computeStiffness(matStiffness, matInputs),
               "Could not compute material stiffness.");
#endif
   return mMatInputs.strandAttr->material->computeForces(mMatForces, mMatInputs);
}

ftype SegmentLine::getLength() {
   return getVec().norm() ;
}

V3 SegmentLine::getLenDiff(bool first) {
   if(first) return -getDir();
   else return getDir();
}
