#include <include_common.hpp>
#include <StrandsHelper.hpp>
#include <Joint.hpp>
#include <DynObj/Strand.hpp>
#include <DynObj/PointL.hpp>
#include <Creator/Creator.hpp>
#include <Scene/Scene.hpp>
#include <SE3.hpp>
#include <cmath>
#include <Eigen/Geometry>
#include <EigenHelper.hpp>
#include <maya/MayaStrings.h>
#include <SceneConstants.hpp>

using namespace std;
using namespace Strands;

// Find the point along cylinder axis closest to p.
V3 findCylProjCenter(V3 p, M4 E){
   V3 c     = SE3::getTranslation(E);
   M3 rotE  = SE3::getRotation(E);
   V3 zAxis = rotE.col(Z);
   // Center projected to make (p-c') perpendicular to the zAxis of the cylinder
   return (c+(p-c).dot(zAxis)*zAxis);
}

inline string virtualPointName(SP<DynObj> currPoint, SP<DynObj> prevPoint, string suffix){
   return (currPoint->getName() + "-" + prevPoint->getName() + suffix);
}

typedef void (*pickPointFn) (VECV3 &points, VECV3 availPoints, V3 jointPos, V3 axis, VECV3 options, int sDir);

void pickPoint0(VECV3 &points, VECV3 availPoints, V3 jointPos, V3 xAxis, VECV3 options, int sDir){
   ftype o0signPre = sDir*signPre(options[0], jointPos, xAxis);
   ftype o1signPre = sDir*signPre(options[1], jointPos, xAxis);
   points.push_back((o0signPre>o1signPre)?options[0]:options[1]);
   return;
}

void pickPoint1(VECV3 &points, VECV3 availPoints, V3 jointPos, V3 yAxis, VECV3 options, int sDir){
   V3 dir  = VecDir(points[0] , availPoints[0]);
   V3 dir0 = VecDir(options[0], availPoints[0]);
   V3 dir1 = VecDir(options[1], availPoints[0]);
   // Pick the one which fits the direction best
   points.push_back(dir0.dot(dir)>dir1.dot(dir)?options[0]:options[1]);
   return;
}

RET getNewPointsPos(VECV3 &vPointsPos, VECV3 availPoints, SP<Joint> joint, int sDir, bool inOrder){
   RCHECKERROR_B(abs(sDir)!=1, "Bad strand direction int.");
   // RCHECKRET(joint->update());
   ftype r = joint->getRad();
   M4 E[2];
   // NOTE Check order to compare to corresponding transforms.
   E[(!inOrder  )%2] = joint->getTransformA();
   E[(!inOrder+1)%2] = joint->getTransformB();
   // zAxis assumed same.
   V3 zAxis;
   zAxis = E[0].col(Z).head<3>();
   ftype theta[2], d[2];
   // log_warn("Getting new point pos:\nE=\n %s", MS(E[0]));
   // log_warn("Getting new point pos:\nE=\n %s", MS(E[1]));
   pickPointFn pickPoint[2] = {pickPoint0, pickPoint1};
   // Two possible point solutions, cause of acos.
   V3 offsets[2], offset;
   M3 rotTheta;
   V3 c_ = findCylProjCenter(availPoints[0], E[0]);
   for(int iP = 0; iP<2; iP++){
      GET_Rp(rotE, jPos, E[iP]);
      V3 xAxis = rotE.col(X);
      // Put point to plane of availPoints[0]
      V3 dz = (availPoints[1] - availPoints[0]).dot(zAxis)*zAxis;
      if(iP==0) dz.setZero();
      V3 p  = availPoints[iP]-dz;
      d[iP] = (p-c_).norm();
      RCHECKERROR_B(d[iP] < r, "Strand point too close to joint.");
      theta[iP] = acos(r/d[iP]);
      rotTheta << cos(theta[iP]), sin(theta[iP]), 0,
         -sin(theta[iP]), cos(theta[iP]), 0,
         0, 0, 1;
      // rotTheta = rotTheta;
      // Move c_ by (rc, rs, 0) in the joint coordinate frame.
      VECV3 options;
      V3 pDir = VecDir(p, c_);
      V3 p1 = (r * rotE * rotTheta  * rotE.transpose() * pDir) + c_;
      V3 p2 = (r * rotE * rotTheta.transpose() * rotE.transpose() * pDir) + c_;
      options.push_back(p1);
      options.push_back(p2);
      // Pick the offset in the same direction from joint to line connecting the points;
      pickPoint[iP](vPointsPos, availPoints, jPos, xAxis, options, iP?sDir:-sDir);
      // offset = ((offsets[0].dot(yAxis))>(offsets[1].dot(yAxis)))?offsets[0]:offsets[1];
      // vPointsPos.push_back(c_ + offset);
   }
   V3 dz = (availPoints[1] - availPoints[0]).dot(zAxis)*zAxis;
   V3 a0 = availPoints[0], a1 = availPoints[1]-dz;
   V3 p0 = vPointsPos[0], p1 = vPointsPos[1];

   if(IS_MATRIX_NULL(p0) || IS_MATRIX_NULL(p1))
      log_warn("Problem.");

   ftype costheta = VecDir(p0, c_).dot(VecDir(p1, c_));
   ftype dtheta = acos(costheta>1?1: costheta);
   ftype arc = r * fabs(dtheta);
   ftype td0 = (p0 - a0).norm();
   ftype td1 = (p1 - a1).norm();
   ftype len = td0 + td1 + arc;
   vPointsPos[0] = vPointsPos[0] + (td0/len)*dz;
   vPointsPos[1] = vPointsPos[1] + ((td0+arc)/len)*dz;

   if(IS_MATRIX_NULL(vPointsPos[0]) || IS_MATRIX_NULL(vPointsPos[1]))
      log_warn("Problem.");

   return R::Success;
}

inline RET findJoint(SP<Joint> &joint, SP<DynObj> r1, SP<DynObj> r2, SPVEC<Joint> joints){
   for(auto joint_: joints){
      if(joint_->checkRigids(r1, r2)){
         joint = joint_;
         return R::Success;
      }
   }
   return R::Failure;
}

RET getDummyStrandNodeInputs
(SPVEC<PointL> &strandPoints, MAP<Joint> &pointJoints, SPVEC<PointL> &newPoints, Scene* scene, IVec pids){
   map<int,SP<PointL>>::iterator pointIter;
   RCHECKERROR_B(pids.size() < 2, "Number of nodes to be loaded (%zu) < 2", pids.size());

   strandPoints.clear();
   auto allPoints = scene->getPointsMap();
   for(auto pid: pids){
      pointIter = allPoints.find(pid);
      RCHECKERROR_B(pointIter == allPoints.end(), "Could not find point in map");
      strandPoints.push_back(allPoints[pid]);
   }

   newPoints.clear();
   pointJoints.clear();
   return R::Success;
}

// TODO Delicate code. Could be written better.
RET getStrandNodeInputs
(SPVEC<PointL> &strandPoints, MAP<Joint> &pointJoints, SPVEC<PointL> &newPoints, Scene* scene, IVec pids){
   map<int,SP<PointL>>::iterator pointIter;
   uint len = (uint)pids.size();
   RCHECKERROR_B(len < 2, "Number of nodes to be loaded (%u) < 2", len);
   int pid;

   // Create vector of all points for strand
   SP<PointL> prevPoint,  currPoint;
   SP<DynObj> prevSource, currSource;
   int iPoint = 0;
   auto allPoints = scene->getPointsMap();
   for(uint ii=0; ii<len; ii++){
      pid = pids[ii];
      pointIter = allPoints.find(pid);
      RCHECKERROR_B(pointIter == allPoints.end(), "Could not find point in map");

      currPoint  = allPoints[pid];
      currSource = currPoint->getDoFSource();

      /** Extended segment possibility **/
      // For all but the first node, check if the previous segment was over a joint, i.e., they are
      // not on the same rigid body.
      if(ii>0 &&  currSource != prevSource){
         // For segment (ii-1)
         if(prevSource == nullptr || currSource == nullptr)
            log_err("One of the points is free and hanging. Dunno what to do.");
         // Crossing between two rigid bodies. Look for a joint between them.
         bool jointFound = false;
         SP<Joint> joint;
         auto r = findJoint(joint, prevSource, currSource, scene->getJoints());
         // Create points if joint was found
         if(RSUCC(r)){

            // NOTE Create 2 points using cylinder_point with the cylinder corresponding to the 2
            // rigid bodies as the base respectively.

            // 1. Compute pos for the two points
            VECV3 vNewPointsPos;
            // VECV3 vAvailPointsPos;
            // Transform of the new cylinder, with axis assumed along Z axis
            M4 cylE   = joint->getTransform();
            V3 cylE_x = SE3::getTranslation(cylE);
            M3 cylE_R = SE3::getRotation(cylE);
            // vAvailPointsPos.push_back(prevPoint->getx());
            // vAvailPointsPos.push_back(currPoint->getx());
            pointJoints[strandPoints.size()-1] = joint;
            int sDir = sign(currPoint->getx(), cylE_x, cylE_R.col(Y));

            // RCHECKERROR(getNewPointsPos(vNewPointsPos, vAvailPointsPos, joint, sDir),
            //    "Could not compute initial position for new points.");
            vNewPointsPos.push_back(V3(0,0,0));
            vNewPointsPos.push_back(V3(1,0,0));
            // Debug output for the two new points positions
            log_verbose("New points computed at: %s, %s",
                        MS_T(vNewPointsPos[0]), MS_T(vNewPointsPos[1]));
            // First virtual point
            auto point =
               createPointL(scene,
                            // Working with cylinderical constraints
                            kCTFixedConstraintInt,
                            // @rigid - Previous point's rigid body
                            prevSource,
                            // @constPos - only translation
                            cylE_x,
                            // @pointPos - Computed in the @getNewPointsPos call
                            vNewPointsPos[0],
                            // @constAxes - only rotation
                            cylE_R,
                            // Absolute positions specified - @useLocalPos is false
                            false,
                            // Name as a combination of current names
                            virtualPointName(currPoint, prevPoint, ":1"),
                            // Do not record -- extra point
                            false);
            // log_warn("Point at %s fixed to rigid %s", MS_T(vNewPointsPos[0]), prevSource->getCName());
            point->setPointId(currPoint->getPointId()*1000+1);
            RCHECKERROR_NOTNULL(point, "Could not create new point - 1.");
            newPoints.push_back(point);
            strandPoints.push_back(point);
            pointJoints[strandPoints.size()-1] = joint;
            //Second virtual point
            point = createPointL(scene,
                                 kCTFixedConstraintInt,
                                 // With the other rigid body at the joint
                                 currSource,
                                 cylE_x,
                                 vNewPointsPos[1],
                                 cylE_R,
                                 false,
                                 virtualPointName(currPoint, prevPoint, ":2"),
                                 false);
            // log_warn("Point at %s fixed to rigid %s", MS_T(vNewPointsPos[1]), prevSource->getCName());
            RCHECKERROR_NOTNULL(point, "Could not create new point - 2.");
            newPoints.push_back(point);
            strandPoints.push_back(point);
            pointJoints[strandPoints.size()-1] = joint;
            pointJoints[strandPoints.size()]   = joint;
            point->setPointId(currPoint->getPointId()*1000+2);
         }
      }
      strandPoints.push_back(currPoint);
      prevSource = currSource;
      // @prevPoint not used at the moment.
      prevPoint = currPoint;
   }
   return R::Success;
}

// FIXME Move these to creator

SP<PointL> createPointL
   (Scene* scene, int constType, int rid,
    V3 constPos, V3 pointPos, M3 constAxes, bool useLocalPos, std::string name, bool record){
   SP<DynObj> rigid = scene->getRigid(rid);
   return createPointL(scene, constType, rigid, constPos, pointPos, constAxes, useLocalPos, name);
}

SP<PointL> createPointL
   (Scene* scene, int constType, SP<DynObj> rigid,
    V3 constPos, V3 pointPos, M3 constAxes, bool useLocalPos, std::string name, bool record){
   SP<PointL> point;
   if(constType == kCTFixedConstraintInt || constType == kCTParentConstraintInt) {
      point = Creator::fixed_point(scene, rigid, pointPos, false, name, record);
   }
   else if(constType == kCTLineConstraintInt){
      point = Creator::line_point(scene, rigid, constPos, pointPos, constAxes, false, name, record);
   }
   else if(constType == kCTCircleConstraintInt){
      // Constraint centered at constPos
      point = Creator::circle_point(scene, rigid, constPos, pointPos, constAxes, false, name, record);
   } else if(constType == kCTCylinderConstraintInt){
      // Three vectors to be provided for making the cylinder
      point = Creator::cylinder_point(scene, rigid, constPos, pointPos, constAxes, false, name, record);
   } else if(constType == kCTFreeConstraintInt){
      log_random("Point unconstrained.");
      point = Creator::free_point(scene, pointPos, name, record);
   }
   return point;
}

ftype signPre(V3 pa, V3 pb, V3 axis){return (pa-pb).dot(axis);}
int sign(V3 pa, V3 pb, V3 axis){return (pa-pb).dot(axis)>0?+1:-1;}
