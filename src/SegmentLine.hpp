#include <include_common.hpp>
#include <Segment.hpp>

class SegmentLine: public Segment{
public:
   RET computeSegmentMKf();
   ftype getLength();
   V3 getLenDiff(bool first);
   SegmentLine(Node *_n0, Node *_n1, SP<StrandAttr> strandAttr,
               MatBlocks *_M, MatBlocks *_K): Segment(_n0, _n1, strandAttr, _M, _K){};
   SegmentLine(Node *_n0, Node *_n1, SP<StrandAttr> strandAttr): Segment(_n0, _n1, strandAttr){};
};
