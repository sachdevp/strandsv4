#include <Material.hpp>

class Strands::MaterialSpring: public Strands::Material{
public:
   MaterialSpring(ftype _stiffness);
   virtual RET computeForces(Strands::MaterialForces &matForces,
                             Strands::MaterialInputs matInputs) override;
   virtual RET computeStiffness(Strands::MaterialStiffness &matStiffness,
                                Strands::MaterialInputs matInputs) override;

private:
   ftype fStiffness;
};
