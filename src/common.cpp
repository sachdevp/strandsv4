#include <include_common.hpp>
#include <sstream>
using namespace std;

RET& operator&=(RET& r, RET r2){
   return r = (RET) ((int)r && (int)r2);
}

RET& operator&=(RET& r, int r2){
   return r = (RET) ((int)r && r2);
}

RET& operator|=(RET& r, RET r2){
   return r = (RET) ((int)r || (int)r2);
}

std::ostream& operator<<(std::ostream& os, const RET& r){
   if(r==RET::Success){
      os<<"Success";
   } else{
      os<<"Failure";
   }
   return os;
}

Eigen::IOFormat OctaveFmt(3 , 0 , ", " , ";\n" , ""  , ""  , "[" , "]");
Eigen::IOFormat NoEndsFmt(3 , 0 , " "  , ";\n" , ""  , ""  , ""  , "");
Eigen::IOFormat FlatFmt  (3 , 0 , " "  , " "   , ""  , ""  , ""  , "");
Eigen::IOFormat HeavyFmt(7  , 0 , ", " , ";\n" , "[" , "]" , "[" , "]");

