#pragma once

#include <Force.hpp>
class DynObj;

class Strands::JointStiffness: public Strands::Force{
private:
   V3 mTorqueVec;
   M4 E0, E1;

public:
   JointStiffness(SP<DynObj> _ra, SP<DynObj> _rb);
   virtual R fillForce() override final;
   R updateTorque(V3 torque);
   // Update the direction based on the rigid body
   virtual R update()    override final;
   virtual R Init()      override final;
   virtual R getIndices(IVec &vIdx, IVec &vLen) override final;
   virtual RET clearForStep(ftype t) override final;
   V3 getTorque();
};
