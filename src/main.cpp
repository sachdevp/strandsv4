#include <iostream>
#include <vector>
#include <fstream>

// #include "SceneCreator.hpp"
#include <StrandsWrapper.hpp>
#include <Scene/Scene.hpp>
#include <DynObj/Strand.hpp>
#include <DynObj/PointL.hpp>
#include <DynObj/Rigid.hpp>
#include <DoF/DoFFixed.hpp>
#include <Creator/Creator.hpp>
#include <Joint.hpp>
#include <SE3.hpp>

using namespace std;

#define SPTR(TYPE, NAME, ...) shared_ptr<TYPE> \
   NAME(new TYPE ( ##__VA_ARGS__ ) );

// void SceneSingleStrand(){
//    Strands::Scene* scene = new Strands::Scene();
//    Vector3t gravity;
//    gravity << 0,0,-9.8;
//    scene->setGravity(gravity);
//    /** Scene and strand parameters **/
//    // 1 mm radius for strands
//    ftype r   = 1e-2;
//    // stiffness of 1e5
//    ftype k   = 1e3;
//    // Water's density
//    ftype rho = 1e1;
//    // 1 ms timestep
//    ftype dt = 1e-2;
//    // Side of cube
//    ftype sz = 1.0;
//    // Cube file
//    string cubeFile = string("data/cube.obj");
//    // Location of COM of cube
//    ftype translate[3] = {0,0,0};
//    // Position of fixed point w.r.t rigid body
//    Vector3t localPos;
//    // Distance of point from world center as a function of size of mass (sz)
//    ftype relDis = 5;
//    Vector4t rigidMass;
//    ftype scale = sz*sz/6.0; // Cube moment of inertia is M*sz*sz/6
//    rigidMass << scale, scale, scale, 1.0;
//    rigidMass*= sz*sz*sz*rho;
//    // Number of steps to take
//    int nSteps = 1;

//    /** Rigid Body Creation **/
//    log_random("Rigid body creation");
//    Eigen::Matrix4t rigidE = Eigen::Matrix4t::Identity();
//    // Implement translate
//    for(int ii=0; ii<3;ii++){rigidE(ii, 3) = translate[ii];}
//    shared_ptr<Rigid> rigid(new Rigid(cubeFile, rigidE , rho));
//    rigid->setInfo(0, "Cube");
//    // Frame DoFs for cube
//    DoFFrame* frame = new DoFFrame(scene->getWorldObject(), rigid);
//    rigid->setMassMatrix(rigidMass);
//    rigid->setFrame(frame);
//    scene->addDoF(frame);
//    scene->addRigid(rigid);

//    log_random("Rigid body added");

//    /** Create points **/
//    SPVEC<PointL>> points;
//    vector<Node*> nodes;
//    SPTR(PointL, tmpPoint1);
//    localPos << 0, 0, relDis*sz;
//    tmpPoint1 = Creator::fixed_point(scene, scene->getWorldObject(), localPos, true);
//    tmpPoint1->setPointId(0);
//    points.push_back(tmpPoint1);

//    // SPTR(PointL, tmpPointE);
//    // localPos << 0, 0, (relDis - .5)*sz;
//    // tmpPointE = Creator::fixed_point(scene, scene->getWorldObject(), localPos, true, "AlphaE");
//    // tmpPointE->setPointId(1);
//    // points.push_back(tmpPointE);

//    SPTR(PointL, tmpPoint2);
//    localPos << 0, 0, sz/2;
//    tmpPoint2 = Creator::fixed_point(scene, rigid, localPos, true);
//    tmpPoint2->setPointId(2);
//    points.push_back(tmpPoint2);


//    /** Create strand, add it and initialize **/
//    SP<Strands::Strand> strand(new Strands::Strand(1, string("FDP") , r, rho, k));
//    strand->loadNodes(points);
//    scene->addStrand(strand);

//    /** Initialize and run scene **/
//    if(scene->Init() == R::Failure){
//       log_err("Could not initialize scene");
//    };
//    scene->setTimestep(dt);
//    ofstream plotdata;
//    plotdata.open("rigid_z.dat");
//    plotdata << 0 << " "<<rigid->getx()(5) <<endl;
//    scene->step();
//    plotdata << dt << " " << rigid->getx()(5) <<endl;
//    for(int i=0; i<nSteps-1; i++){
//       scene->step();
//       plotdata << scene->getTime() <<" "<< rigid->getx().transpose() <<endl;
//    }
//    plotdata.close();
// }

// void SceneSingleRigid(){
//    Strands::Scene* scene = new Strands::Scene();
//    // localPos: Position of fixed point w.r.t rigid body;
//    Vector3t gravity, localPos;
//    Vector4t rigidMass;

//    // Set gravity
//    gravity << 0,0,-9.8;
//    scene->setGravity(gravity);

//    /** Scene parameters **/
//    ftype rho = 1e1;
//    // 1 ms timestep
//    ftype dt = 1e-2;
//    // Side of cube
//    ftype sz = 1.0;
//    // Cube file: Unused
//    string cubeFile = string("data/cube.obj");
//    // Location of COM of cube
//    Vector3t translate;
//    translate << 1,0,0;

//    // Set mass of cube
//    ftype scale = sz*sz/6.0; // Cube moment of inertia is M*sz*sz/6
//    rigidMass << scale, scale, scale, 1.0;
//    rigidMass*= rho*sz*sz*sz;
//    // Number of steps to take
//    int nSteps = 1000;

//    /** Rigid Body Creation **/
//    log_info(3, "Rigid body creation");
//    Eigen::Matrix4t rigidE;
//    rigidE = Eigen::Matrix4t::Identity();

//    // TODO Replace by function to translate at any point
//    rigidE.block<3, 1>(0, 3) = translate;
//    shared_ptr<Rigid> rigid(new Rigid(cubeFile, rigidE , rho));
//    rigid->setInfo(0, "Cube");
//    rigid->setRigidId(0);

//    // Frame DoFs for cube
//    DoFFrame* frame = new DoFFrame(scene->getWorldObject(), rigid);
//    Vector6t x;
//    x << 0,0,0, translate;
//    frame->setx(x);
//    rigid->setMassMatrix(rigidMass);
//    rigid->setFrame(frame);

//    // Add to scene
//    scene->addDoF(frame);
//    scene->addRigid(rigid);

//    log_verbose("Rigid body added");

//    /** Create joint to hinge rigid **/
//    Creator::free_axis(scene, rigid, AXIS::theta_y);

//    /** Initialize and run scene **/
//    scene->Init();
//    scene->setTimestep(dt);
//    ofstream plotdata;
//    plotdata.open("rigid_z.dat");
//    Matrix4t E = rigid->getTransform();
//    Vector3t angles, position;
//    SE3::computeEulerDegrees(angles, E);
//    position = E.block<3, 1>(0, 3);
//    plotdata << scene->getTime() << " " << angles.transpose() << " " << position.transpose()<<endl;
//    scene->step();
//    E = rigid->getTransform();
//    SE3::computeEulerDegrees(angles, E);
//    position = E.block<3, 1>(0, 3);
//    plotdata << scene->getTime() << " " << angles.transpose() << " " <<position.transpose()<<endl;
//    for(int i=0; i<nSteps-1; i++){
//       scene->step();
//       E = rigid->getTransform();
//       SE3::computeEulerDegrees(angles, E);
//       position = E.block<3, 1>(0, 3);
//       plotdata << scene->getTime() << " " << angles.transpose() << " " <<position.transpose()<<endl;
//    }
//    plotdata.close();
// }

// void SceneTestRigidJoint(){
//    Strands::Scene* scene = new Strands::Scene();
//    // localPos: Position of fixed point w.r.t rigid body;
//    Vector3t gravity, localPos;
//    Vector4t rigidMass;

//    // Set gravity
//    gravity << 0,0,-9.8;
//    scene->setGravity(gravity);

//    /** Scene parameters **/
//    ftype rho = 1e1;
//    // 1 ms timestep
//    ftype dt = 1e-2;
//    // Side of cube
//    ftype sz = 1.0;
//    // Cube file: Unused
//    string cubeFile = string("data/cube.obj");
//    // Location of COM of cube
//    Vector3t translate;
//    translate << 1,0,0;

//    // Set mass of cube
//    ftype scale = sz*sz/6.0; // Cube moment of inertia is M*sz*sz/6
//    rigidMass << scale, scale, scale, 1.0;
//    rigidMass*= rho*sz*sz*sz;
//    // Number of steps to take
//    int nSteps = 1000;

//    /** Rigid Body Creation **/
//    log_info(3, "Rigid body creation");
//    Eigen::Matrix4t rigidE;
//    rigidE = Eigen::Matrix4t::Identity();

//    rigidE.block<3, 1>(0, 3) = translate;
//    shared_ptr<Rigid> rigid(new Rigid(cubeFile, rigidE , rho));
//    rigid->setInfo(0, "Cube");
//    rigid->setRigidId(0);

//    // Frame DoFs for cube
//    DoFFrame* frame = new DoFFrame(scene->getWorldObject(), rigid);
//    Vector6t x, v_sc;
//    x << 0,0,0, translate;
//    frame->setx(x);
//    rigid->setMassMatrix(rigidMass);
//    rigid->setFrame(frame);
//    v_sc << 0, 0, 0, 0, 1, 0;
//    frame->setScripted(v_sc);

//    // Add to scene
//    scene->addDoF(frame);
//    scene->addRigid(rigid);

//    log_verbose("Rigid body added");


//    // Implement translate
//    translate << 0, 0, 1;
//    rigidE.block<3, 1>(0,3) = translate;
//    shared_ptr<Rigid> rigid2(new Rigid(cubeFile, rigidE , rho));
//    rigid2->setInfo(0, "Cube");
//    // Frame DoFs for cube
//    frame = new DoFFrame(scene->getWorldObject(), rigid2);
//    v_sc << 0, 0, 0, 0, -1, 0;
//    rigid2->setMassMatrix(rigidMass);
//    rigid->setRigidId(1);
//    rigid2->setFrame(frame);
//    rigid2->setScripted(v_sc);
//    scene->addDoF(frame);
//    scene->addRigid(rigid2);


//    log_random("Rigid body added");

//    /** Initialize and run scene **/
//    scene->Init();
//    scene->setTimestep(dt);
//    ofstream plotdata;
//    plotdata.open("rigid_z.dat");
//    Matrix4t E = rigid->getTransform();
//    Vector3t angles, position;
//    SE3::computeEulerDegrees(angles, E);
//    position = E.block<3, 1>(0, 3);
//    plotdata << scene->getTime() << " " << angles.transpose() << " " << position.transpose()<<endl;
//    scene->step();
//    E = rigid->getTransform();
//    SE3::computeEulerDegrees(angles, E);
//    position = E.block<3, 1>(0, 3);
//    plotdata << scene->getTime() << " " << angles.transpose() << " " <<position.transpose()<<endl;
//    for(int i=0; i<nSteps-1; i++){
//       scene->step();
//       E = rigid->getTransform();
//       SE3::computeEulerDegrees(angles, E);
//       position = E.block<3, 1>(0, 3);
//       plotdata << scene->getTime() << " " << angles.transpose() << " " <<position.transpose()<<endl;
//    }
//    plotdata.close();
// }

// void SceneCoriolis(){
//    Strands::Scene* scene = new Strands::Scene();
//    Vector3t gravity;
//    gravity << 0,0,-9.8;
//    scene->setGravity(gravity);

//    /** Scene parameters **/
//    // Water's density
//    ftype rho = 1e1;
//    // 1 ms timestep
//    ftype dt = 1e-2;
//    // Side of cube
//    ftype sz = 1.0;
//    // Cube file
//    string cubeFile = string("data/cube.obj");
//    // Location of COM of cube
//    Vector3t translate;
//    translate <<0,0,0;
//    // Position of fixed point w.r.t rigid body
//    Vector3t localPos;
//    Vector4t rigidMass;
//    ftype scale = sz*sz/6.0; // Cube moment of inertia is M*sz*sz/6
//    rigidMass << scale, scale, scale, 1.0;
//    rigidMass*= rho*sz*sz*sz;
//    // Number of steps to take
//    int nSteps = 2000;

//    /** Rigid Body Creation **/
//    log_info(3, "Rigid body creation");
//    Eigen::Matrix4t rigidE;

//    rigidE = Eigen::Matrix4t::Identity();

//    // Implement translate
//    rigidE.block<3, 1>(0,3) = translate;
//    shared_ptr<Rigid> rigid(new Rigid(cubeFile, rigidE , rho));
//    rigid->setInfo(0, "Cube");
//    // Frame DoFs for cube
//    DoFFrame* frame = new DoFFrame(scene->getWorldObject(), rigid);
//    Vector6t v0;
//    v0 << 0, 1, 0, 0, 0, 1;
//    frame->setInitVelocity(v0);
//    rigid->setMassMatrix(rigidMass);
//    rigid->setFrame(frame);
//    scene->addDoF(frame);
//    scene->addRigid(rigid);

//    rigidE = Eigen::Matrix4t::Identity();

//    /** Create joint to hinge rigid **/
//    // bool constrain[6] = {1, 0, 1, 1, 1, 1};
//    // Eigen::Matrix4t jointE = Eigen::Matrix4t::Identity();
//    // Joint* joint = new Joint(shared_ptr<DynObj>
//    // (scene->getWorldObject()), rigid,  constrain, jointE);
//    // scene->addJoint(joint);

//    /** Initialize and run scene **/
//    scene->Init();
//    scene->setTimestep(dt);
//    ofstream plotdata;
//    plotdata.open("rigid_z.dat");
//    plotdata <<scene->getTime()<<" "<< rigid->getx().transpose()
//             <<" "<< rigid->getv().transpose()<<endl;
//    scene->step();
//    plotdata <<scene->getTime()<<" "<< rigid->getx().transpose()
//             <<" "<< rigid->getv().transpose()<<endl;
//    for(int i=0; i<nSteps-1; i++){
//       scene->step();
//       plotdata << scene->getTime() <<" "<< rigid->getx().transpose()
//                <<" "<< rigid->getv().transpose() <<endl;
//    }
//    plotdata.close();
// }

// void SceneRigidForce(){
//    Strands::Scene* scene = new Strands::Scene();
//    Vector3t gravity;
//    gravity << 0,0,0;
//    scene->setGravity(gravity);
//    Eigen::Matrix4t rigidE = Eigen::Matrix4t::Identity();

//    string cubeFile = string("data/cube.obj");
//    ftype rho = 1e3;
//    ftype forceMag = rho; // Accelerate at 1 m/s2
//    Vector4t rigidMass;
//    ftype sz = 1;
//    ftype scale = sz*sz/6.0; // Cube moment of inertia is M*sz*sz/6
//    ftype dt = 1e-2;
//    int nSteps = 1000;
//    rigidMass << scale, scale, scale, 1.0;
//    rigidMass*= rho*sz*sz*sz;
//    shared_ptr<Rigid> rigid(new Rigid(cubeFile, rigidE, rho));
//    rigid->setInfo(0, "Cube");
//    // Frame DoFs for cube
//    DoFFrame* frame = new DoFFrame(scene->getWorldObject(), rigid);
//    Vector6t v0;
//    v0 << 0, 0, 0, 1, 0, 0;
//    frame->setInitVelocity(v0);
//    rigid->setMassMatrix(rigidMass);
//    rigid->setFrame(frame);
//    scene->addDoF(frame);
//    scene->addRigid(rigid);
//    shared_ptr<Strands::Force> force =
//       Creator::rigid_force(scene, rigid, forceMag, Eigen::Matrix4t::Identity());


//    /** Initialize and run scene **/
//    scene->Init();
//    scene->setTimestep(dt);
//    ofstream plotdata;
//    plotdata.open("rigid_z.dat");
//    plotdata <<scene->getTime()<<" "<< rigid->getx().transpose()
//             <<" "<< rigid->getv().transpose()<<endl;
//    scene->step();
//    plotdata <<scene->getTime()<<" "<< rigid->getx().transpose()
//             <<" "<< rigid->getv().transpose()<<endl;
//    for(int i=0; i<nSteps-1; i++){
//       scene->step();
//       plotdata << scene->getTime() <<" "<< rigid->getx().transpose()
//                <<" "<< rigid->getv().transpose() <<endl;
//    }
//    plotdata.close();

// }

int main(int argc, char* argv[]) {
   // SceneSingleStrand();
   // SceneRigidForce();
   // SceneSingleRigid();
   // SceneTestRigidJoint();
   // SceneCoriolis();
   void* scene;
   auto filename = DATA_DIR "/xml/Flex-Ex.xml";
   if(argc==2){filename = argv[1];}
   scene = load_scene(filename);
   if(argc>2) log_err("Wrong number of arguments.");

   CHECK_NOTNULL_R1(scene, "Scene is null.");
   for(int i=0;i<200;i++){
      step(scene);
      if(failed_check(scene)) break;
      printf("State is %s\n", get_state(scene));
   }
   
   return 0;
}
