#include <include_common.hpp>
#include <iostream>
#include <Eigen/Eigen>
#include <Serializer/InputData.hpp>

class PointL;
class DoF;
class DoFFixed;
class DoFCurve;
class DoFSurface;
class DoFFull;
class Rigid;
class DynObj;
class Curve;
class Surface;

namespace Creator{
   DoFFixed*
   dof_fixed(Strands::Scene* scene, SP<DynObj> rigid,
             SP<PointL> point, V3 pos, bool useLocalPos = true);
   DoFCurve*
   dof_curve(Strands::Scene* scene, SP<DynObj> rigid,
             SP<PointL> point, Curve* curve, V3 curveCenter,
             bool useLocalPos = true);
   DoFSurface* dof_surface(Strands::Scene* scene, SP<DynObj> rigid,
                           SP<PointL> point, Surface* surface, V3 center,
                           V2 u, bool useLocalPos);
   DoFFull*
   dof_full(Strands::Scene* scene, SP<PointL> point, V3 pos);
   SP<PointL>
   fixed_point(Strands::Scene* scene, SP<DynObj> rigid, V3 pos, bool useLocalPos = true,
               std::string ptName = "", bool record = true);
   SP<PointL>
   fixed_point(Strands::Scene* scene, int rigidId, V3 pos, bool useLocalPos = true,
               std::string ptName = "", bool record = true);
   SP<PointL>
   fixed_point(Strands::Scene* scene, SP<DynObj> rigid, ftype x, ftype y, ftype z,
               bool useLocalPos = true, std::string ptName = "", bool record = true);
   SP<PointL>
   fixed_point(Strands::Scene* scene, int rigidId, ftype x, ftype y, ftype z,
               bool useLocalPos = true, std::string ptName = "", bool record = true);
   SP<PointL>
   line_point(Strands::Scene* scene, SP<DynObj> rigid, V3 center, V3 pos, M3 constAxes,
              bool useLocalPos = true, std::string ptName = "", bool record = true);
   // SP<PointL>
   // line_point(Strands::Scene* scene, SP<DynObj> rigid, V3 center,
   //            ftype pos_x, ftype pos_y, ftype pos_z,
   //            ftype axis_x, ftype axis_y, ftype axis_z,
   //            bool useLocalPos, std::string ptName);
   SP<PointL>
   line_point(Strands::Scene* scene, int rigidId, V3 center, V3 pos, M3 constAxes,
              bool useLocalPos, std::string ptName, bool record = true);
   // SP<PointL>
   // line_point(Strands::Scene* scene, int rigidId, V3 center,
   //            ftype pos_x, ftype pos_y, ftype pos_z,
   //            ftype axis_x, ftype axis_y, ftype axis_z,
   //            bool useLocalPos = true, std::string ptName = "");
   SP<PointL>
   circle_point(Strands::Scene* scene, int rigidId, V3 center, V3 pos, M3 constAxes,
                bool useLocalPos, std::string ptName, bool record = true);
   SP<PointL>
   circle_point(Strands::Scene* scene, SP<DynObj> rigid, V3 center, V3 pos, M3 constAxes,
                bool useLocalPos, std::string ptName, bool record = true);
   SP<PointL>
   cylinder_point(Strands::Scene* scene, int rigidId, V3 center, V3 pos, M3 constAxes,
                  bool useLocalPos, std::string ptName, bool record = true);
   SP<PointL>
   cylinder_point(Strands::Scene* scene, SP<DynObj> rigid, V3 center, V3 pos, M3 constAxes,
                  bool useLocalPos, std::string ptName, bool record = true);
   /** 3-DOF point **/
   // Full DoF point for creating a free floating point
   SP<PointL>
   free_point(Strands::Scene* scene, V3 pos, std::string ptName="", bool record = true);
   // Alternative header for different position input
   inline SP<PointL>
   free_point(Strands::Scene* scene, ftype x, ftype y, ftype z,
              std::string ptName="", bool record = true){
      return free_point(scene, V3(x,y,z), ptName, record);
   };

   SP<Strands::Joint>
   joint(Strands::Scene* scene, int _raId, int _rbId,
         std::array<bool, 6> _cnstrnd, M4 E, RET *r = nullptr);
   // Function to constrain the dynamic object to a single DoF.
   // NOTE Assumes identity by default
   SP<Strands::Joint> free_axis(Strands::Scene* scene, SP<DynObj> rigid, AXIS axis, M4 jointE = EYE4);

   SP<Strands::Force> rigid_force(Strands::Scene* scene, SP<DynObj> dynObj, ftype mag, M4 localE = EYE4);
   SP<Strands::Force> rigid_force(Strands::Scene* scene, int objectId, ftype mag, M4 globalE);

   SP<Strands::Force> wrench_force(Strands::Scene* scene, SP<DynObj> dynObj, KeysMap<V6> wrenchMap);
   SP<Strands::Force> wrench_force(Strands::Scene* scene, int objectId, KeysMap<V6> wrenchMap, M4 globalE);

   Strands::Scene*     create_scene(SceneInput);
   SP<Strands::Joint>  create_joint(Strands::Scene* scene, ObjInput *oin);
   SP<Strands::Force>  create_force(Strands::Scene* scene, ObjInput *oin);
   SP<Rigid>           create_rigid(Strands::Scene* scene, ObjInput *oin);
   SP<PointL>          create_point(Strands::Scene* scene, ObjInput* oin);
   SP<Strands::Strand> create_strand(Strands::Scene* scene, ObjInput *oin);
}


