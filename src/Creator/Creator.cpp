#include <iostream>
#include <Eigen/Eigen>

#include <Creator/Creator.hpp>
#include <Scene/Scene.hpp>
#include <DynObj/Strand.hpp>
#include <DynObj/Rigid.hpp>
#include <Joint.hpp>
#include <RigidForce.hpp>
#include <Wrench.hpp>
#include <DynObj/PointL.hpp>
#include <DoF/DoFFixed.hpp>
#include <DoF/DoFCurve.hpp>
#include <DoF/DoFSurface.hpp>
#include <DoF/DoFFull.hpp>
#include <DoF/DoF.hpp>
#include <SE3.hpp>
#include <DoF/CurveLine.hpp>
#include <DoF/CurveCircle.hpp>
#include <DoF/SurfaceCylinder.hpp>
#include <StrandsHelper.hpp>

#include <MaterialSpring.hpp>
#include <MaterialLinearMuscle.hpp>

using namespace std;
using namespace Eigen;
using namespace Strands;

namespace Creator{
   // Helper function
   /** DoF creation**/

   DoFFixed* dof_fixed(Scene* scene, SP<DynObj> rigid,
                       SP<PointL> point, V3 pos, bool useLocalPos){
      DoFFixed* dof = new DoFFixed(rigid, point, pos, useLocalPos);
      scene->addDoF(dof);
      return dof;
   }

   DoFCurve* dof_curve(Scene* scene, SP<DynObj> rigid, SP<PointL> point,
                       Curve* curve, V3 _curveCenter, bool useLocalPos){
      DoFCurve* dof = new DoFCurve(rigid, point, curve, 0, _curveCenter, useLocalPos);
      scene->addDoF(dof);
      return dof;
   }

   DoFSurface* dof_surface(Scene* scene, SP<DynObj> rigid, SP<PointL> point,
                           Surface* surface, V3 center, V2 u, bool useLocalPos){
      DoFSurface* dof = new DoFSurface(rigid, point, surface, center, u, useLocalPos);
      scene->addDoF(dof);
      return dof;
   }

   DoFFull* dof_full(Scene* scene, SP<PointL> point, V3 pos){
      DoFFull* dof = new DoFFull(point, pos);
      scene->addDoF(dof);
      return dof;
   }

   /** Point creation**/
   SP<PointL>
   fixed_point(Scene* scene, SP<DynObj> rigid,
               V3 pos, bool useLocalPos, string ptName, bool record){
      SP<PointL> point(new PointL());
      DoFFixed* dof = dof_fixed(scene, rigid, point, pos, useLocalPos);
      point->setDoF(dof);
      point->setInfo(1, ptName);
      point->setRecord(record);
      scene->addPoint(point);
      log_verbose("Point %s added at %s", ObjName(point), MS_T(point->getx()));
      return point;
   }

   SP<PointL>
   fixed_point(Scene* scene, SP<DynObj> rigid,
               ftype x, ftype y, ftype z, bool useLocalPos, string ptName, bool record){
      return fixed_point(scene, rigid, V3(x, y, z), useLocalPos, ptName, record);
   }

   SP<PointL>
   fixed_point(Scene* scene, int rigidId, V3 point, bool useLocalPos, string ptName, bool record){
      return fixed_point(scene, scene->getRigid(rigidId), point, useLocalPos, ptName, record);
   }

   SP<PointL>
   fixed_point(Scene* scene, int rigidId,
               ftype x, ftype y, ftype z, bool useLocalPos, string ptName, bool record){
      return fixed_point(scene, scene->getRigid(rigidId), V3(x, y, z), useLocalPos, ptName, record);
   }

   SP<PointL>
   circle_point(Scene* scene, int rigidId,
                V3 center, V3 pos, M3 constAxes,
                bool useLocalPos, string ptName, bool record){
      return circle_point(scene, scene->getRigid(rigidId),
                          center, pos, constAxes, useLocalPos, ptName, record);
   }

   SP<PointL>
   circle_point(Scene* scene, SP<DynObj> rigid,
                V3 center, V3 pos, M3 constAxes,
                bool useLocalPos, string ptName, bool record){
      SP<PointL> point(new PointL());
      V3 global_axisx, global_axisy;
      V3 axisx, axisy;
      global_axisx = constAxes.col(X);
      global_axisy = constAxes.col(Y);
      if(global_axisy.norm() < EPSILON)
         log_err("DoF circle axis not generic. Parallel to direction of center to current position.");

      M4 E = rigid->getTransform();
      M3 R = SE3::getRotation(E);
      V3  disp   = pos - center;
      V3T dispT  = disp.transpose() ;
      // project displacement on the cross-section at the center of the cylinder.
      ftype distz = disp.transpose()*(constAxes.col(Z));
      V3 projDisp = (disp - distz*constAxes.col(Z));
      V2 initCoord;
      initCoord[0] = atan2(dispT*constAxes.col(Y), dispT*constAxes.col(X));
      initCoord[1] = distz;
      ftype radius = projDisp.norm() ;
      axisx = R.partialPivLu().solve(global_axisx);
      axisy = R.partialPivLu().solve(global_axisy);
      axisx.normalize();
      axisy.normalize();
      Curve* circle = new CurveCircle(radius, axisx, axisy, -1, "DummyCurve");
      DoFCurve* dof = dof_curve(scene, rigid, point, circle, center, useLocalPos);
      point->setDoF(dof);
      point->setInfo(1, ptName);
      point->setRecord(record);
      scene->addPoint(point);
      log_verbose("Point %s added at %s", ObjName(point), MS_T(point->getx()));
      return point;
   }

   SP<PointL>
   cylinder_point(Scene* scene, int rigidId,
                  V3 center, V3 pos, M3 constAxes,
                  bool useLocalPos, string ptName, bool record){
      return cylinder_point(scene, scene->getRigid(rigidId),
                            center, pos, constAxes, useLocalPos, ptName, record);
   }

   SP<PointL>
   cylinder_point(Scene* scene, SP<DynObj> rigid,
                  V3 center, V3 pos, M3 constAxes,
                  bool useLocalPos, string ptName, bool record){
      SP<PointL> point(new PointL());
      M4 E = rigid->getTransform();
      M3 R = SE3::getRotation(E);
      V3 disp   = pos - center;
      V3T dispT = disp.transpose();
      // project displacement on the cross-section at the center of the cylinder.
      ftype distz = dispT*(constAxes.col(Z));
      V3 projDisp = (disp - distz*constAxes.col(Z));

      // Initial coordinates for the cylinder point
      ftype dispY = dispT*constAxes.col(Y); ftype dispX = dispT*constAxes.col(X);
      V2 initCoord;
      initCoord[0] = atan2(dispY, dispX); initCoord[1] = distz;

      ftype radius = projDisp.norm() ;
      M3 localConstAxes = R.partialPivLu().solve(constAxes);
      Surface* cylinder = new SurfaceCylinder(radius, localConstAxes, -1, "DummySurface");
      DoFSurface* dof = dof_surface(scene, rigid, point, cylinder,
                                    center, initCoord, useLocalPos);
      point->setDoF(dof);
      point->setInfo(1, ptName);
      point->setRecord(record);
      scene->addPoint(point);
      log_verbose("Point %s added at %s", ObjName(point), MS_T(point->getx()));
      return point;
   }

   SP<PointL>
   line_point(Scene* scene, SP<DynObj> rigid, V3 center,
              V3 pos, M3 constAxes, bool useLocalPos, string ptName, bool record){
      SP<PointL> point(new PointL());
      M4 E    = rigid->getTransform();
      M3 R    = SE3::getRotation(E);
      M3 axes = R.partialPivLu().solve(constAxes);
      Curve*    line = new CurveLine(axes, -1, "DummyCurve");
      DoFCurve* dof  = dof_curve(scene, rigid, point, line, pos, useLocalPos);
      point->setDoF(dof);
      point->setInfo(1, ptName);
      point->setRecord(record);
      scene->addPoint(point);
      log_verbose("Point %s added at %s", ObjName(point), MS_T(point->getx()));
      return point;
   }

   SP<PointL>
   line_point(Scene* scene, int rigidId,
              V3 center, V3 pos, M3 axes, bool useLocalPos, string ptName, bool record){
      auto r = scene->getRigid(rigidId);
      return line_point(scene, r, center, pos, axes, useLocalPos, ptName, record);
   }

   SP<PointL>
   free_point(Scene* scene, V3 pos, string ptName, bool record){
      SP<PointL> point(new PointL);
      DoF* dof = dof_full(scene, point, pos);
      point->setDoF(dof);
      point->setInfo(1, ptName);
      point->setRecord(record);
      scene->addPoint(point);
      return point;
   }

   /** Joint creation**/
   SP<Joint>
   joint(Scene* scene, int parentId, int childId, std::array<bool, 6> _const, M4 E, RET *r){
      SP<Joint> joint = nullptr;
      SP<Rigid> ra, rb;

      // Get the rigids
      *r = scene->getRigid(rb, childId);
      RCHECKERROR_RNULL(*r, "Could not find child rigid body (id %d)", childId);
      *r = scene->getRigid(ra, parentId);
      RCHECKERROR_RNULL(*r, "Could not find parent rigid body (id %d)", parentId);

      joint = make_shared<Joint>(ra, rb, _const, E);
      return joint;
   }

   /** Constraint creation**/
   inline auto get_world(Scene* scene){return scene->getRigid(static_cast<int>(RigidBodies::World));}
   SP<Joint>
   free_axis(Scene* scene, SP<DynObj> rigid, AXIS axis, M4 jointE){
      std::array<bool, 6> constrained = FULLCONS;
      // Free the relevant axis
      constrained[static_cast<int>(axis)] = 0;
      SP<Joint> joint;
      joint = make_shared<Joint>(get_world(scene), rigid, constrained, jointE);
      scene->addJoint(joint);
      return joint;
   }

   /** Force creation**/
   SP<Force>
   rigid_force(Scene* scene, SP<DynObj> dynObj, ftype mag, M4 localE){
      SP<Force> force;
      force = make_shared<RigidForce>(dynObj, mag, localE);
      scene->addForce(force);
      return force;
   }

   SP<Force>
   rigid_force(Scene* scene, int objectId, ftype mag, M4 globalE){
      SP<DynObj> dynObj = scene->getDynObj(objectId);
      CHECK_NOTNULL_RNULL(dynObj, "Failed to find dynamic object. Cannot create force.");
      M4 localE;
      RET r = SE3::computeERel(localE, globalE, dynObj->getTransform());
      RCHECKERROR_RNULL(r, "Could not create rigid_force.");

      SP<DynObj> sDynObj(dynObj);
      SP<Force> force = rigid_force(scene, sDynObj, mag, localE);
      CHECK_NOTNULL_RNULL(force, "Failed to create force");
      return force;
   }

   SP<Force> wrench_force(Scene* scene, SP<DynObj> dynObj, KeysMap<V6> wrenchMap){
      SP<Wrench> wrench;
      auto rigid = std::dynamic_pointer_cast<Rigid>(dynObj);
      CHECK_NOTNULL_RNULL(rigid, "Dynamic object not rigid.");
      wrench = make_shared<Wrench>(rigid, wrenchMap);
      auto force = std::static_pointer_cast<Force>(wrench);
      scene->addForce(force);
      return force;
   }

   SP<Force> wrench_force(Scene* scene, int objectId, KeysMap<V6> wrenchMap, M4 globalE){
      SP<DynObj> dynObj = scene->getDynObj(objectId);
      CHECK_NOTNULL_RNULL(dynObj, "Failed to find dynamic object. Cannot create wrench.");
      M4 localE;
      RET r = SE3::computeERel(localE, globalE, dynObj->getTransform());

      RCHECKERROR_RNULL(r, "Could not create wrench.");
      SP<Force> force = wrench_force(scene, dynObj, wrenchMap);
      CHECK_NOTNULL_RNULL(force, "Failed to create wrench.");
      return force;
   }

   Scene* create_scene(SceneInput sceneIn){
      Scene* scene = new Scene();
      scene->setGravity(sceneIn.grav);
      scene->setDamping(sceneIn.damp);
      if(sceneIn.stab>0) scene->setStab(sceneIn.stab);
      scene->setTimestep(sceneIn.dt);
      scene->setSolver(Solver::QPMOSEK);
      for(auto objIn:sceneIn.vObjects){
         switch(objIn->objType){
         case ObjType::POINT:
            create_point(scene, objIn);
            break;
         case ObjType::RIGID:
            create_rigid(scene, objIn);
            break;
         case ObjType::JOINT:
            create_joint(scene, objIn);
            break;
         case ObjType::STRND:
            if(!create_strand(scene, objIn)){
               log_err("Could not load strand %s.", objIn->name.c_str());
               scene->setFailed();
               return scene;
            }
            break;
         case ObjType::FORCE:
            create_force(scene, objIn);
            break;
         case ObjType::INDIC:
            log_err("Indicator not handled.");
            break;
         case ObjType::CONST:
            log_err("Constraint not handled.");
            break;
         }
      }
      return scene;
   }

   RET set_joint_limits(SP<Joint> joint, V6 l6, V6 u6, JointLimitType limitType){
      RET r;
      r = joint->setLimitType(limitType);
      if(Joint::needsUpper(limitType)) joint->setUpperLimit(u6);
      if(Joint::needsLower(limitType)) joint->setLowerLimit(l6);
      if(limitType == JointLimitType::FIXED){
         // TODO Not yet implemented
         log_err("Fixed joint limit not implemented.");
         return R::Failure;
      }
      return r;
   }

   SP<PointL> create_point(Scene* scene, ObjInput* oin){
      auto pin = (PointInput*)(oin);
      SP<PointL> point = make_shared<PointL>();
      V3 constPos;
      int rid = -1, constType = kCTFreeConstraintInt;
      M3 constAxes = EYE3;
      if(pin->optConst.has_value()){
         auto pcin = pin->optConst.value();
         rid = pcin.rid;
         constPos  = pcin.constE.block<3,1>(0, 3);
         constAxes = pcin.constE.block<3,3>(0,0);
         constType = pcin.constType;
      }
      point = createPointL(scene, constType, rid, constPos,
                           pin->x, constAxes, false, pin->name);
      CHECK_NOTNULL_RNULL(point, "Could not create point.");

      if(pin->debug) point->setDebugOutput();
      // Update point Info
      point->setInfo(pin->oid, pin->name);
      point->setPointId(pin->pid);
      return point;
   }

   SP<Joint> create_joint(Scene* scene, ObjInput* oin){
      auto jin = (JointInput*)(oin);
      bool constrained[6] = ZROT;
      RET r;
      auto joint = Creator::joint(scene, jin->parentId, jin->childId, jin->bConst, jin->E, &r);
      r &= set_joint_limits(joint, jin->l6, jin->u6, jin->limitType);
      if(RFAIL(r)){
         if(joint) log_err("Null joint expected on failure. Non-null returned.");
         log_err("Failed to create joint."); return nullptr;
      }

      r &= joint->setInfo(jin->oid, jin->name);
      r &= joint->setJointId(jin->jid);
   
      CHECKERROR_B_RNULL(RFAIL(r) || !joint, "Could not create joint.");

      if(jin->debug) joint->setDebugOutput();
      joint->setRad(jin->rad);
      joint->setAng0(jin->ang0);
      if(jin->stiffnessType == 'l') joint->setLinStiffness(jin->k);
      if(jin->stiffnessType == 'e') joint->setExpStiffness(jin->params);
      joint->setDamping(jin->damp);
      
      // Add to scene
      scene->addJoint(joint); return joint;
   }

   SP<Rigid> create_rigid(Scene* scene, ObjInput *oin){
      auto rin = (RigidInput*)oin;
      // Use given mass if asked to do so, and calculate if needed.
      SP<Rigid> rigid = make_shared<Rigid>();
      rigid->setTransform(rin->Ecom, rin->E);
      rigid->setMassMatrix(rin->mass);
      rigid->setInfo(rin->oid, rin->name);
      rigid->setRigidId(rin->rid);
      
      // Frame DoFs for cube
      DoFFrame* frame = new DoFFrame(scene->getRigid((int)RigidBodies::World), rigid);
      rigid->setFrame(frame);

      if(rin->bScripted){
         frame->setScVelMap(rin->scVelMap);
         if(rin->scVelMap.empty()) log_err("%s scripted but map empty.", rin->name.c_str());
      }
      
      // Add to scene
      scene->addDoF(frame); scene->addRigid(rigid);
      
      /** Constraint **/
      if(rin->optConstE.has_value()) {
         auto joint = Creator::free_axis(scene, rigid, ROT_AXIS_0, rin->optConstE.value());
         log_check_err(!joint, "Failed to create joint on rigid body. Null joint returned.");
      }
      return rigid;
   }

   SP<Force> create_force(Scene* scene, ObjInput *oin){
      auto fin = (ForceInput*)oin;

      RET r;
      SP<Force> force;
      if(fin->fType == kFTRigidForceInt) force = Creator::rigid_force(scene , fin->dobjId, fin->mag, fin->E);
      if(fin->fType == kFTWrenchInt)     force = Creator::wrench_force(scene, fin->dobjId, fin->wrenchMap, fin->E);

      CHECKERROR_B_RNULL(!force, "Could not create force %s", fin->name.c_str());
      r  = force->setInfo(fin->oid, fin->name);
      r &= force->setForceId(fin->fid);
      RCHECKERROR_RNULL(r, "Force info issues.");
      return force;
   }

   SP<Strand> create_strand(Scene* scene, ObjInput *oin){
      auto sin = (StrandInput*)oin;
      SP<Strand> strand; 
      SP<Material> mat;
      if(sin->matType == MatType::SPRING)       mat = make_shared<MaterialSpring>      (sin->k);
      if(sin->matType == MatType::MUSCLELINEAR) mat = make_shared<MaterialLinearMuscle>(sin->k, sin->dl_act);

      strand = make_shared<Strand>(0, sin->name, sin->r, sin->rho, mat, sin->initStrain);
      // NOTE Strand no longer has an act map.
      // if(matType == (short)MatType::MUSCLELINEAR) MCHECKRET_R_RNULL(strand->setActMap(actMap));
      SPVEC<PointL> strandPoints, newPoints;
      MAP<Joint> pointJoints;
      if(sin->simStrand){
         RCHECKERROR_RNULL(getDummyStrandNodeInputs(strandPoints, pointJoints, newPoints, scene, sin->pids),
                           "Could not construct input for loading strand nodes.");
      } else{
         RCHECKERROR_RNULL(getStrandNodeInputs(strandPoints, pointJoints, newPoints, scene, sin->pids),
                           "Could not construct input for loading strand nodes.");
      }

      SPVEC<Joint> vPointCons;
      int ret = strand->loadNodes(vPointCons, strandPoints, pointJoints);
      CHECKERROR_B_RNULL(ret<=0, "Failed to load nodes for %s", sin->name.c_str());

      // Add all the constraints created when creating strand
      for(auto cons: vPointCons) scene->addJoint(cons);
   
      strand->setInfo(sin->oid, sin->name);
      strand->setStrandId(sin->sid);
      if(sin->debug       ) strand->setDebugOutput();
      if(sin->inextensible) strand->setInextensible();
      scene->addStrand(strand);
      return strand;
   }

}
