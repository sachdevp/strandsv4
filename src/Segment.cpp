/**
   @auth: sachdevp
   @desc: Handles a segment of the strand between two nodes
*/

#include <Segment.hpp>
#include <DynObj/StrandAttr.hpp>
#include <Material.hpp>

using namespace Eigen;
using namespace std;
using namespace Strands;

Segment::Segment(Node* _n0, Node* _n1, SP<StrandAttr> _strandAttr,
                 MatBlocks *_M, MatBlocks *_K){
   basicInit(_n0, _n1, _strandAttr);
   setMatrices(_M, _K);
};

Segment::Segment(Node* _n0, Node* _n1, SP<StrandAttr> _strandAttr){
   basicInit(_n0, _n1, _strandAttr);
}

RET Segment::basicInit(Node* _n0, Node* _n1, SP<StrandAttr> _strandAttr){
   RCHECKERROR_NOTNULL(_strandAttr,
                       "StrandAttr not correctly set. Make sure it is set before creating segment.");
   mMatInputs.strandAttr = _strandAttr;
   mMatInputs.node0 = _n0;
   mMatInputs.node1 = _n1;
   tMLL[0].setZero();
   tMLL[1].setZero();
   tMLE[0].setZero();
   tMLE[1].setZero();
   this->matInitialized = false;
   this->active = true;
   return R::Success;
}

RET Segment::clearForStep(){
   mMatForces.clear();
   return R::Success;
}

RET Segment::setMatrices(MatBlocks* _M, MatBlocks* _K){
   if(_M==nullptr || _K==nullptr) return R::Failure;
   M = _M; K = _K;
   this->matInitialized = true;
   return R::Success;
}

// Macro to allow block filling without repeated patterns of code
#define FILL_MATRIX(KM, LLEE, A, B) KM->LLEE##00 += A;   \
   KM->LLEE##01 += B;                                    \
   KM->LLEE##10 += B;                                    \
   KM->LLEE##11 += A;            

RET Segment::fillInactiveMK(){
   if(!mMatInputs.node0->isActive()) M->LL00 = EYE3;
   if(!mMatInputs.node1->isActive()) M->LL11 = EYE3;
   // CHECK Fill the E-related stuff to zero too?
   return R::Success;
}
// Computes mass, stiffness, and force for the segment
RET Segment::fillMK(){
   // Matrix6t tmpMLL;
   // Matrix2t tmpMEE;
   // Matrix<ftype, 2, 6> tmpMEL;
   // Matrix<ftype, 6, 2> tmpMLE;
   
   // Direct fill into MLL, MLE, MEL, and MEE for speed up. Same for K matrices.
   FILL_MATRIX(M, LL, tMLL[0], tMLL[1]);
   FILL_MATRIX(M, LE, tMLE[0], tMLE[1]);
   FILL_MATRIX(M, EL, tMLE[0].transpose() , tMLE[1].transpose());
   FILL_MATRIX(M, EE, tMEE[0], tMEE[1]);

#ifdef STIFFNESS
   if(!mMatInputs.strandAttr->material->isInextensible()){
      FILL_MATRIX(mMatStiffness, LL, tKLL[0], tKLL[1]);
      FILL_MATRIX(mMatStiffness, LE, tKLE[0], tKLE[1]);
      FILL_MATRIX(mMatStiffness, EL, tKLE[0].transpose() , tKLE[1].transpose());
      FILL_MATRIX(mMatStiffness, EE, tKEE[0], tKEE[1]);
   }
#endif

   return R::Success;
}

RET Segment::fillNodeForces(){
   // Use M.LL, M.LE and M.EE to fill the mass for each
   mMatInputs.node0->addForce(mMatForces.fx[0], mMatForces.fs[0]);
   mMatInputs.node1->addForce(mMatForces.fx[1], mMatForces.fs[1]);
   return R::Success;
}

RET Segment::Init(){
   assert(this->matInitialized);
   return R::Success;
}

// Returns computed linear density using strand attributes
ftype Segment::getDensity(){
   return mMatInputs.strandAttr->getLinearDensity();
}

RET Segment::getNodes(Node* &_node0, Node* &_node1, uint* nodesSkipped){
   RCHECK_NOTNULL(mMatInputs.node0);
   RCHECK_NOTNULL(mMatInputs.node1);
   Node* n1;
   _node0 = mMatInputs.node0;
   _node1 = mMatInputs.node1;
   return R::Success;
}
