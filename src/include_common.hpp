/**
   @author: sachdevp
   @desc:   Common includes for the projects
*/

#pragma once
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-private-field"
#endif

#ifdef Success
#undef Success
#warning "Success" macro already defined.Undefining.
#endif

#ifdef _WIN32
#define NOMINMAX
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#define M_PI 3.1416f
#else
#include <cstdio>
#endif

#ifdef _WIN32
#define uint UINT;
//typedef UINT uint
#endif

// #define ftype double
#define ftype float
// #define FTYPE Float
// #define EIGEN_STACK_ALLOCATION_LIMIT 0
// #define EIGEN_USE_BLAS

#include <Eigen/Eigen>
#include <Eigen/Dense>
#include <cmath>
#include <cassert>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <cfloat>

#include <optional>
#include <exception>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <utility>
#include <functional>
#include <map>
#include <memory>
#include <chrono>
#include <iterator>

#include <config.h>

// Custom vector/matrix types based on ftype
namespace Eigen{
   typedef Matrix<ftype,1,1> Vector1t;
   typedef Matrix<ftype,2,1> Vector2t;
   typedef Matrix<ftype,3,1> Vector3t;
   typedef Matrix<ftype,4,1> Vector4t;
   typedef Matrix<ftype,6,1> Vector6t;
   typedef Matrix<ftype,2,2> Matrix2t;
   typedef Matrix<ftype,1,1> Matrix1t;
   typedef Matrix<ftype,3,3> Matrix3t;
   typedef Matrix<ftype,4,4> Matrix4t;
   typedef Matrix<ftype,6,6> Matrix6t;
   typedef Matrix<ftype, Dynamic, Dynamic> MatrixXt;
   typedef Block<MatrixXt, 6, 6> BlockM6t;
   typedef Block<MatrixXt, 2, 2> BlockM2t;
   typedef Block<MatrixXt, 6, 2> BlockM62t;
   typedef Block<MatrixXt, 2, 6> BlockM26t;
   // Split the above matrices.
   // Needed for non-contiguous implementation
   typedef Block<MatrixXt, 3, 3> BlockM33t;
   typedef Block<MatrixXt, 1, 1> BlockM11t;
   typedef Block<MatrixXt, 3, 1> BlockM31t;
   typedef Block<MatrixXt, 1, 3> BlockM13t;
   typedef Matrix<ftype, Dynamic, 1> VectorXt;
   typedef Matrix<ftype, 1, 3> Vector3tT;

   typedef Block<MatrixXt, Dynamic, Dynamic> BlockMXt;
   typedef VectorBlock<VectorXt, Dynamic> BlockVXt;
}

using V1 = Eigen::Vector1t;
using V2 = Eigen::Vector2t;
using V3 = Eigen::Vector3t;
using V4 = Eigen::Vector4t;
using V6 = Eigen::Vector6t;
using M1 = Eigen::Matrix1t;
using M2 = Eigen::Matrix2t;
using M3 = Eigen::Matrix3t;
using M4 = Eigen::Matrix4t;
using M6 = Eigen::Matrix6t;
using V3T= Eigen::Vector3tT;
#define EYE4 Eigen::Matrix4t::Identity()
#define EYE3 Eigen::Matrix3t::Identity()
using LinSolver = Eigen::PartialPivLU<Eigen::MatrixXt>;
template<class T>
using SP = std::shared_ptr<T>;
#define STRMAP std::map<std::string, std::string>
#define STRVEC std::vector<std::string>
using StrVec = std::vector<std::string>;
using IVec = std::vector<int>;
template<class T>
using MAP = std::map<int, std::shared_ptr<T>>;
template<class T>
using SPVEC = std::vector<std::shared_ptr<T>>;
using VECV3 = std::vector<V3>;
using MAPSTRV3 = std::map<std::string, V3>;
#define X 0
#define Y 1
#define Z 2

#define MAT_CLIP(C) C.unaryExpr([threshold](ftype x) -> ftype {return (fabs(x) > threshold) ? x: 0;})

typedef enum class ObjType_t{RIGID = 0,
                             POINT = 1,
                             STRND = 2,
                             FORCE = 3,
                             CONST = 4,
                             JOINT = 5,
                             // Point meant only as an indicator. Computed on the fly.
                             INDIC = 6} ObjType;
//////////////////////////////////////////////////
// Logging Functions                            //
//////////////////////////////////////////////////

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define VERBOSITY 1u
#define DEBUG
/* Define this if profiling. */
// #define LOG_PROFILING

#ifndef QUIET
// Verbose logging
#define log_info(v, M, ...)                                             \
   if((v) <= VERBOSITY){                                                \
      const char* _loc  = __FILENAME__;                                 \
      const char* _func = __FUNCTION__;                                 \
      const int   _line = __LINE__;                                     \
      fprintf(stdout, "[INFO] (%s:%s:%d) \t " M "\n",                   \
              _loc, _func, _line, ##__VA_ARGS__);                       \
   }                                                                    \
   
#else
#define log_info(M, ...)
#endif
#define log_err(M, ...)     fprintf(stderr, "[ERR] %s:%d: \t" M "\n",   \
                                    __FILENAME__, __LINE__, ##__VA_ARGS__)
#define log_check_err(B, M, ...) if(B) log_err(M, ##__VA_ARGS__)
#define log_warn(M, ...)    fprintf(stderr, "[WARN] %s:%d: \t" M "\n",  \
                                    __FILENAME__, __LINE__, ##__VA_ARGS__)
#define log_random(M, ...)  log_info(3, M, ##__VA_ARGS__)
#define log_verbose(M, ...) log_info(2, M, ##__VA_ARGS__)
#define log_reg(M, ...)     log_info(1, M, ##__VA_ARGS__)

/* Float related macros */
#define EPSILON 1e-12
#define THRESH 1e-8 // Used in SE3 calculations
#define Z_ESEG_THRESH 0.3 // To use extended segment or not
#define DEFAULT_INT -1
#define DEFAULT_DENSITY 1.0f
#define SEG_THRESH 1e-3
#define EQUALS(x, y) ( fabs((x) - (y)) < EPSILON)
#define EQUALS_ANGLE(x, y) ( fabs((x) - (y)) < THRESH)
#define EQUALS_DET(M, d) ((M.determinant() - d) < THRESH)
#define EQUALS_VEC(x, y) (((x)-(y)).norm() < EPSILON)

typedef enum class RigidBodies_t{
   World=4096,
   NoRigid=4095
} RigidBodies;

/** Return class **/
typedef enum class RET_T{
   Success=1,
   Failure=0
} RET;
using R = RET;
#define RFAIL(r) (r == R::Failure)
#define RSUCC(r) (r == R::Success)

#define CHECK_BOOL_AND_RET(BOOL, RET, MSG, ...)                      \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return RET;                                                \
   }

#define RCHECKRET(STAT) if(RFAIL(STAT)){                                \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<#STAT<<std::endl; \
      return R::Failure;                                                \
   }

#define RCHECKERROR_RET(STAT,RET, MSG, ...)                             \
   if(STAT == R::Failure){                                              \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return RET;                                                       \
   }

#define RCHECKERROR(STAT,MSG, ...)                                      \
   if(STAT == R::Failure){                                              \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return R::Failure;                                                \
   }
#define RCHECK_NOTNULL(PTR, ...)                                        \
   if(PTR == nullptr){                                                  \
      return R::Failure;                                                \
   }
#define RCHECKERROR_NOTNULL(PTR, MSG, ...)                              \
   if(PTR == nullptr){                                                  \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return R::Failure;                                                \
   }
#define RCHECKERROR_RNULL(STAT,MSG, ...)                                \
   if(STAT == R::Failure){                                              \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return nullptr;                                                   \
   }
#define RCHECKERROR_R1(STAT,MSG, ...)                                   \
   if(STAT == R::Failure){                                              \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return -1;                                                        \
   }
#define RCHECKERROR_B(BOOL,MSG, ...)                                    \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return R::Failure;                                                \
   }
#define CHECKERROR_B_RNULL(BOOL,MSG, ...)                               \
   if(BOOL){                                                            \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return nullptr;                                                   \
   }
#define CHECK_NOTNULL_RNULL(PTR,MSG, ...)                               \
   if(PTR == nullptr){                                                  \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return nullptr;                                                   \
   }
#define CHECK_NOTNULL_R1(PTR,MSG, ...)                                  \
   if(PTR == nullptr){                                                  \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[ERR] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
      return -1;                                                        \
   }
#define RCHECKWARN(STAT,MSG, ...)                                       \
   if(STAT == R::Failure){                                              \
      char str[1024]; sprintf(str, MSG, ##__VA_ARGS__);                 \
      std::cerr<<"[WARN] "<<__FILENAME__<<":"<<__LINE__<<":"<<str<<std::endl; \
   }

// Operators for RET
std::ostream& operator<<(std::ostream& os, const RET& r);
RET& operator|=(RET& r, RET r2);
RET& operator&=(RET& r, RET r2);
RET& operator&=(RET& r, int r2);

/** Matrix related functions **/
#define CHECK_MATRICES_APPROX_EQUAL(A, B) ((A).rows() == (B).rows() && (A).cols() == (B).cols() && A.isApprox(B))
extern Eigen::IOFormat OctaveFmt;
extern Eigen::IOFormat NoEndsFmt;
extern Eigen::IOFormat FlatFmt;

template<typename Derived>
std::string matrixString(const Eigen::EigenBase<Derived>& M){
   std::stringstream str;
   str<<M.derived().format(OctaveFmt);
   return str.str();
}

template<typename Derived>
std::string matrixStringNoEnds(const Eigen::EigenBase<Derived>& M){
   std::stringstream str;
   str<<M.derived().format(NoEndsFmt);
   return str.str();
}

template<typename Derived>
std::string matrixStringFlat(const Eigen::EigenBase<Derived>& M){
   std::stringstream str;
   str<<M.derived().format(FlatFmt);
   return str.str();
}

enum class AXIS{
   theta_x = 0,
   theta_y = 1,
   theta_z = 2,
   pos_x = 3,
   pos_y = 4,
   pos_z = 5
};
// Axis order for rotation.
#define ROT_AXIS_0 AXIS::pos_z
#define ROT_AXIS_1 AXIS::pos_y
#define ROT_AXIS_2 AXIS::pos_x
#define FORCE_AXIS AXIS::pos_x
#define ZROT {1, 1, 0, 1, 1, 1}
#define FULLCONS {1, 1, 1, 1, 1, 1}

// Various preprocessing functions for outputs and common checks
#define IS_VALID_INDEX(a) (a>=0)
#define IS_MATRIX_NULL(A) (!((((A)-(A)).array() == ((A)-(A)).array()).all()))
#define MS(A) (matrixString((A)).c_str())
#define MS_T(A) (matrixString((A.transpose())).c_str())
#define MSNE(A) (matrixStringNoEnds((A)).c_str())
#define MSNE_T(A) (matrixStringNoEnds((A.transpose())).c_str())
#define MSF(A) (matrixStringFlat((A)).c_str())
#define MSF_T(A) (matrixStringFlat((A.transpose())).c_str())
#define ObjName(A) (A==nullptr?"Null":((A)->getName().c_str()))
#define HS(A) "\n" A "= ... \n%s\n"
#define MATRICES "\nMatrices\n--------\n"
#define PRINTMAT(A) log_random(HS(#A), MS(A))
#define EQUAL_ARRAY(A, B, n) for(int ii=0; ii<(n); ii++)        \
      (A)[ii] = (B)[ii];
#define IS_WORLD_ID(id) (id == static_cast<int>(RigidBodies::World))
#define IS_WORLD(s)      IS_WORLD_ID((s)->getId())
#define EQLEN(x, y) ((x).rows() == (y).rows())

// Find MAX_STRAND_POINTS in StrandMayaShape.h

// Time related macros
#define HRC std::chrono::high_resolution_clock
#define DUR std::chrono::duration
#define TIME std::chrono::time_point

// Mosek
#define MSK_CHECK_STATUS_AND_RETURN_IT(r) if ((r)!=MSK_RES_OK) {        \
      MSK_deletetask(&task);                                            \
      MSK_deleteenv(&env);                                              \
      return RET::Failure;                                              \
   }
#define CHECK_MOSEK_STATUS(r) if((r)==MSK_RES_OK)

typedef enum class
IneqKey_t {LB=0, UB=1, FX=2, FR=3, RA=2} IneqKey;

typedef enum class Solver_t{LINEAR = 0, QPMOSEK = 1} Solver;
#define LOWERLIMIT 0
#define UPPERLIMIT 1
enum class JointLimitType{FREE = 0, UPPER = 1, LOWER = 2, RANGE = 3, FIXED = 4};
enum class MatType{SPRING = 0, MUSCLELINEAR = 1, MUSCLEHILL = 2};

// Progressing towards a Strands namespace
namespace Strands{
   class ESegment;

   class Force;
   class Wrench;
   class JointStiffness;
   class JointDamping;
   class RigidForce;

   class Joint;

   class Material;
   class MaterialForces;
   class MaterialInputs;
   class MaterialStiffness;

   class MaterialLinearMuscle;
   class MaterialSpring;

   class Scene;
   class SceneCreator;
   class Strand;
}
// #define indexed(...) indexed_v(i, __VA_ARGS__)
// #define indexed_v(v, ...) (bool _i_ = true, _break_ = false; _i_;)   \
//    for(size_t v = 0; _i_; _i_ = false)                               \
//       for(__VA_ARGS__) if(_break_) break;                            \
//          else for(bool _j_ = true; _j_;)                             \
//                  for(_break_ = true; _j_; _j_ = false)               \
//                     for(bool _k_ = true; _k_; v++, _k_ = false, _break_ = false)

#define CHECK_INITIALIZED(str, Type)             \
   if(!bInit){                                   \
      log_reg(#Type " not initialized: " str);   \
      return R::Failure;                         \
   }
#define CHECK_NOT_INITIALIZED(str, Type)            \
   if(bInit){                                       \
      log_reg(#Type " already initialized: " str);  \
      return R::Failure;                            \
   }

// your map:
class own_double_less : public std::binary_function<double,double,bool>
{
public:
   own_double_less( double arg_ = EPSILON ) : epsilon(arg_) {}
   bool operator()( const double &left, const double &right  ) const
      {
         // you can choose other way to make decision
         // (The original version is: return left < right;) 
         return (left < (right-epsilon));
      }
   double epsilon;
};
template <class T>
using KeysMap = std::map<double,T,own_double_less>;
typedef std::vector<std::pair<int, V3>> ExtraNodes;

// Defining offset of iterator by one.
template <class It>
struct iter_pair {
   It b, e;

   It begin() const { return b; }
   It end() const { return e; }
};
template <class C>
using iterator_t = decltype(std::begin(std::declval<C>()));
template <class C>
iter_pair<iterator_t<C&>> offset(C& container, size_t skip) {
   return {std::next(container.begin(), skip), container.end()};
}

#define UNIT_SCALE 1.0

