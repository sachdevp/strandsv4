/**
   @author: sachdevp
   @desc:   This file deals with creating and maintaining matrices.
            Also helps solving the system to generate the final solution vectors.
*/

#pragma once
#include <include_common.hpp>
#include <DoF/DoF.hpp>
#include <mosek.h>

#define SOL_THRESH 1e3

class MatrixCentral{
public:
   // JR: vR -> vRL
   // JQ: vRL-> v
   // M: Full mass matrix
   // _r: Subscript for reduced
   // _f: Subscript for full
   // C_f/c_f: Full DoF Constraints
   // C_r/c_r: Reduced DoF Constraints
   // C_if/c_iuf, c_ilf, c_ikf: Full DoF inequality constraints (upper, lower, key)

   Solver solver;
   Eigen::MatrixXt M, M_r, K;
   // Jacobians
   Eigen::MatrixXt JRL, JR, J;

   // Equality constraints
   // NOTE Constraints need to be reduced before they can be used in dynamics.  C_f is reduced and
   // copied to the appropriate submatrix of C.  Same with c_f and c.
   Eigen::MatrixXt C_f, C_r, C;
   // Inequality constraints
   Eigen::MatrixXt C_i, C_if, C_ir;
   // Inequality keys (lower or upper bound)
   std::vector<IneqKey> c_ik, c_ikf, c_ikr;

   std::map<IneqKey, MSKboundkeye> mapIneqKey;

#ifndef EXPLICIT_JOINT_DAMPING
   Eigen::MatrixXt mAdjMat, mJDampMat;
#endif
   Eigen::MatrixXd KKTM;
   Eigen::VectorXd KKTrhs, vec_error;
   Eigen::VectorXt outRHS;
   Eigen::VectorXt f, v, v_prev,
      c, c_f, c_r,
      c_iu, c_il,
      c_iuf, c_ilf,
      c_iur, c_ilr,
      v_r, x_r, lambda, dx_r,
      dx_r_stab, lambda_stab;
   ftype abs_error, rel_err, const_error;

   inline bool isLinearSolve(){return solver == Solver::LINEAR;}
   inline bool isQPSolve()    {return solver == Solver::QPMOSEK;}

   inline bool getnInexEqCons()  {return isLinearSolve();}
   inline bool getnInexIneqCons(){return isQPSolve();}

   // Damping
   ftype fDamping, fStab;
   // Timestep
   ftype h;
   // Size of full, rigid-lagrangian, rigid-reduced, constraint, and full constraint matrix;
   int nF, nRL, nR, nJDoF,
      nC_r, nC_f, nC_ir, nC_if, cnC_ir, cnC_if, lambda_len;
   // Check at initialization whether the timestep was set
   bool bTimeStepSet, bConstraintsSet, bDampingSet, bStabSet;

   public:
   MatrixCentral();
   // Setting functions
   RET setMatrixSizes(int &nFull, int &nRL, int &nR,
                      int &nC_r, int &nC_f, int &nC_ir, int &nC_if,
                      int &nJDoF);
   RET setMassMatrix();
   RET setTimestep(ftype);
   RET setDamping(ftype);
   RET setStab(ftype);

   /** Getting functions**/
   inline int getFullSize() {return nF;}
   inline int getnRL()      {return nRL;}
   inline int getnR()       {return nR;}
   inline int getnC()       {return (nC_r + nC_f);}
   inline int getnCimax()   {return (nC_ir + nC_if);}
   inline int getnCi()      {return (cnC_ir + cnC_if);}

   inline ftype getTimestep(){return h;}
   // Get system velocity at the current timestep
   Eigen::VectorXt getVelocity(){return v;}

   /** System solver related functions**/
   // Clear the matrices
   RET clear();

   STRVEC vActiveIneq;
   // NOTE cnC_ir and cnC_if need to be refreshed since velocity constraints are
   // only active when positional constraints are violated.
   RET combineConstraints (int _cnC_ir, int _cnC_if);
   RET getREConstRow(int &consRow, int eqIdx);
   RET getRIConstRow(int &consRow, int ineqIdx);
   RET getFIConstRow(int &consRow, int ineqIdx);
   // Run the dynamics and integrate the reduced position.
   RET dynamics();
   // Post stabilization for constraints
   RET stabilize(Eigen::VectorXt c_drift);
   RET setSolver(Solver _solver){solver = _solver; return R::Success;}
   /** Logging related functions **/
   RET setFilename(std::string _filename);
   RET setInitialState(Eigen::VectorXt x_r, Eigen::VectorXt v_r);

   // NOTE start- and stop- do not touch anything but bLogMat
   // Dynamically call for starting logging.
   inline RET startLogging(){bLogMat = true; return R::Success;}
   // Dynamically call for stopping logging.
   inline RET stopLogging(){bLogMat = false; return R::Success;}
   // Closes log file.
   RET closeLog();

private:
   std::string sLogFile;
   FILE* fpLog;
   bool bLogMat;

   RET linearSolve(Eigen::VectorXt rhs);
   RET QPMosekSolve(Eigen::VectorXt rhs);
   RET preSolve(Eigen::VectorXt rhs);
   RET postSolve(Eigen::VectorXt rhs);
   RET printPreSolve(FILE* file, Eigen::VectorXt rhs);
   RET printPostSolve(FILE* file);
};

