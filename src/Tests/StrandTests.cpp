#include "Tests.hpp"
#include <DynObj/Strand.hpp>
#include <Scene/Scene.hpp>
#include <Scene/SceneCreator.hpp>
using namespace std;

// The aim of this test is to check the construction of the strand matrix.
RET strand_mass_test(){
   Strands::SceneCreator* creator = new Strands::SceneCreator();
   Strands::Scene* scene = creator->loadScene(TESTS_DIR STRAND_TEST_FILE);
   assert(scene!=nullptr);
   TestScene* testScene = new TestScene(scene);
   scene->Init();
   Eigen::MatrixXt expectedMassMatrix(3,3);
   expectedMassMatrix << 0, 0, 0,
      0, 0, 0,
      0, 0, 0;
   // cout<<expectedMassMatrix<<endl;
   // cout<<testScene->nodeLocation(0, 0)<<testScene->nodeLocation(0, 1)<<endl;
   log_info(2, "Strand Mass:\n%s", MS(testScene->strandMass(0)));
   if(CHECK_MATRICES_APPROX_EQUAL(testScene->strandMass(0), expectedMassMatrix)){
      return RET::Success;
   }
   return RET::Failure;
}
