#include "../include_common.hpp"
#include "Tests.hpp"

using namespace std;

int main(){
   if (xml_matrix_test() == RET::Failure){log_info(1, "xml_matrix_test: Failed test to read matrices from XML file.");}
   else{log_info(2, "xml_matrix_test: Succeeded.");}
   if (gamma_test() == RET::Failure){log_info(1, "gamma_test: Failed test to compute gamma");}
   else{log_info(2, "gamma_test: Succeeded.");}
   // if (strand_mass_test() == RET::Failure){log_info(1, "strand_mass_test: Failed test to compute strand mass matrix");}
   // else{log_info(2, "strand_mass_test: Succeeded.");}
}

