#include <Eigen/Eigen>
#include <include_common.hpp>

using namespace std;
using namespace Eigen;
using MX = Eigen::MatrixXt;
using VX = Eigen::VectorXt;

#define MI 166.7
#define MASS 1.0e3
#define GRAV 9.79

RET MatrixSolve(){
   MX M;
   M = MX::Zero(24,24);
   // Mass values
   M.block<3,3>(0,0) = MI*(Matrix3t::Identity());
   M.block<3,3>(3,3) = MASS*(Matrix3t::Identity());
   M.block<3,3>(6,6) = MI*(Matrix3t::Identity());
   M.block<3,3>(9,9) = MASS*(Matrix3t::Identity());
   // Scripted Constraints
   MX cMat = Matrix6t::Identity();
   M.block<6,6>(12,0) = cMat;
   M.block<6,6>(18,6) = cMat;
   M.block<6,6>(0,12) = cMat;
   M.block<6,6>(6,18) = cMat;

   VX rhs;
   rhs = VX::Zero(24, 1);
   rhs(4, 0) = -MASS*GRAV;
   rhs(10, 0) = -MASS*GRAV;

   VX sol = M.fullPivHouseholderQr().solve(rhs);
   cout<<M<<"\n\n"<<rhs<<"\n\n"<<sol<<endl;
   return R::Success;
}

RET MatrixMult(){
   MX M1, M2;
   M1 = Matrix<ftype, 12, 12>::Identity();
   M2 = Matrix<ftype, 12, 12>::Identity();
   M1 = M1 * M2;
   return R::Success;
}

int main(){
   RET r;
   RCHECKWARN(MatrixMult(), "Could not mult matrices");
   RCHECKWARN(MatrixSolve(), "Could not solve matrix");
   return 0;
}
