#pragma once
#include <include_common.hpp>
#include <Scene/Scene.hpp>
#include <DynObj/Strand.hpp>
#define XML_MATRIX_TEST_FILE "Matrices.xml"
#define STRAND_TEST_FILE "StrandTest.xml"
#define N_GAMMA_TESTS 10000

// List of tests
RET xml_matrix_test();
RET gamma_test();
RET strand_mass_test();

class TestScene{
private:
   Strands::Scene* scene;
public:
   TestScene(Strands::Scene* _scene): scene(_scene){};
   Eigen::MatrixXt strandMass(uint strandId){
      return scene->vStrands[strandId]->getMassMatrix();
   };
};
