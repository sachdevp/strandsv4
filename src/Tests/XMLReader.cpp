#include "../include_common.hpp"
#include "../xml_helper.hpp"
#include "Tests.hpp"
using namespace std;
using namespace Eigen;
using namespace pugi;

RET xml_matrix_test(){
   xml_document xml;
   xml_parse_result r = xml.load_file(TESTS_DIR XML_MATRIX_TEST_FILE);
   if (!r){
      cerr << "Open scene file " << XML_MATRIX_TEST_FILE << " failed because of " << r.description() << endl;
      return RET::Failure;
   }
   xml_node Matrices = xml.child("Matrices");
   MatrixXt mat3;
   Matrix4t mat4;
   RET result = RET::Success;
   // Get matrices from the XML file
   result &= XML::getMatrix(mat3, Matrices, "Matrix3", 3, 3);
   result &= XML::getMatrix4t(mat4, Matrices, "Matrix4");

   // Comparison 3x3 matrix
   Matrix3t mat3_2;
   mat3_2 << 1, 4, 7,
      2, 5, 8,
      3, 6, 9;

   // Confirm the result
   result &= (mat3 == mat3_2);

   Matrix4t mat4_2;
   mat4_2 << 1, 5, 9, 13,
      2, 6, 10, 14,
      3, 7, 11, 15,
      4, 8, 12, 16;
   result &= (mat4 == mat4_2);
   return (result);
}
