#include "../include_common.hpp"
#include "../SE3.hpp"
#include <ctime>
#include "Tests.hpp"

using namespace std;
using namespace Eigen;

#define NEW_RAND (((ftype)rand()/RAND_MAX - 0.5)*2)
#define SEED_RAND (srand(time(NULL)))

RET gamma_test(){
   SEED_RAND;
   Vector3t pos;
   VectorXt phi(6,1);
   VectorXt v1(3,1);
   VectorXt v2(3,1);
   phi.setRandom();
   MatrixXt Gamma(3, 6);
   Matrix3t R = Matrix3t::Identity();
   bool result = 1;
   for(uint16_t i = 1; i <N_GAMMA_TESTS; i++){
      phi.setRandom();
      pos.setRandom();
      v1 = phi.tail<3>() + pos.cross(phi.head<3>()); // v + r x omega
      Gamma = SE3::computeGamma(R, pos);
      v2 = Gamma*phi;
      result = result && EQUALS_VEC(v1,v2);
   }
   Vector3t angles;
   Matrix4t I = Matrix4t::Identity();
   auto r = SE3::computeEulerDegrees(angles, I);
   
   if(result == true) return RET::Success;
   else return RET::Failure;
}
