#include <include_common.hpp>
#include <MaterialSpring.hpp>
#include <Node/Node.hpp>
#include <DynObj/StrandAttr.hpp>
#include <Scene/SceneAttr.hpp>

namespace Strands{
   MaterialSpring::MaterialSpring(ftype _stiffness)
      :fStiffness(_stiffness){clear();};

   RET MaterialSpring::computeStiffness
   (MaterialStiffness &K, MaterialInputs in){
      // ftype rho = in.strandAttr->getLinearDensity();

      // Get the node and strand values
      V3    x1 = in.node1->getx();
      V3    x0 = in.node0->getx();
      V3    dx = x1 - x0;
      ftype s1 = in.node1->gets();
      ftype s0 = in.node0->gets();
      ftype ds = s1 - s0;
      ftype k  = fStiffness;

      // Calculate length, strain etc.
      ftype l  = dx.norm() ;
      ftype epsilon  = (l/ds) - 1.0f;

      if (!isInextensible()){
         V3 dxhat = dx/l;
         M3 I     = EYE3;

         K.tKLL   = -(k/ l) * (I*epsilon + dxhat * dxhat.transpose());
         K.tKLE   =    k    * (epsilon + 1) * dxhat;
         K.tKEE   = -(k/ds) * (epsilon + 1) * (epsilon + 1);
      }

      return R::Success;
   }

   RET MaterialSpring::computeForces(MaterialForces &F,
                                     MaterialInputs in){
      if((in.node0 == nullptr) ||
         (in.node1 == nullptr)) return R::Failure;
      V3 x0 = in.node0->getx();
      V3 x1 = in.node1->getx();
      ftype s0 = in.node0->gets();
      ftype s1 = in.node1->gets();
      ftype ds = s1 - s0;
      V3 dx = x1 - x0;
      V3 x01= x1 + x0;
      // FIXME Needs to use segment's length function
      ftype l = dx.norm() ;
      ftype epsilon = (l/std::abs(ds)) - 1.0f;
      // log_warn("Testing values: %f %f, %f, %f, %f, %f", l, ds, abs(ds), fabs(ds), epsilon, l/abs(ds)-1);
      assert(in.strandAttr->sceneAttr != nullptr);
      V3 g = in.strandAttr->sceneAttr->gravity;
      ftype rho = in.strandAttr->getLinearDensity();
      V3 fx = 0.5f*rho*ds*g, fex = V3::Zero();
      ftype fs=0, fes=0;
      fs = 0.5f * rho * x01.transpose() * g;
      // Does not push, and does not pull for inextensible.
      if(epsilon>0 && !isInextensible()){
         fes = -(fStiffness/2) * epsilon * (epsilon + 2);
         fex = fStiffness * epsilon * dx/l;
      }
      // else both fes and fex initialize to zero
      F.fx[0] = fx + fex;
      F.fx[1] = fx - fex;
      F.fs[0] = fs + fes;
      F.fs[1] = fs - fes;
      return R::Success;
   }
};

