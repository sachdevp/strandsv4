#include <include_common.hpp>
#include <Serializer/XMLImporter.hpp>
#include <Creator/Creator.hpp>
#include <Scene/Scene.hpp>
#include <StrandsWrapper.hpp>

using namespace std;

Strands::Scene* cast_scene(void* _scene){
   Strands::Scene* scene = (Strands::Scene*)_scene;
   return scene;
}

void* load_scene(const char* fname){
   auto importer = new XMLImporter(fname);
   // log_reg("Loading scene %s.", fname);
   if(importer->checkFailed()) return nullptr;
   importer->init();
   auto optScene = importer->importScene();
   if(!optScene.has_value()){return nullptr;}
   SceneInput sceneIn = optScene.value();
   Strands::Scene* scene = Creator::create_scene(sceneIn);
   CHECK_BOOL_AND_RET(!scene || scene->checkFailed(), nullptr, "Could not load scene.");
   scene->setLabel("Testing");

   scene->Init();
   // KeysMap<bool> debugMap;
   // debugMap[-9.9e-14] = 1;
   // scene->setDebug(debugMap, "/home/sachdevp/tmp/Strands/testing-py.log", 1);
   // scene->setMatOutDir("/home/sachdevp/tmp/Strands/testing-py.d/");
   return scene;
}

bool set_initial_state(void* _scene, ftype* _x, ftype* _v, int len, float t){
   auto scene = cast_scene(_scene);
   Eigen::VectorXt x(len), v(len);
   for(int i=0; i<len; i++){
      x[i] = _x[i];
      v[i] = _v[i];
   }
   auto r = scene->setInitialInternalState(x, v);
   if(RFAIL(r)) return false;
   r = scene->setTime(t);
   if(RFAIL(r)) return false;
   return true;
}

const char* get_label(void* _scene){
   auto scene = cast_scene(_scene);
   return scene->getLabel();
}

bool get_internal_state(void* _scene, ftype* &_x, ftype* &_v, int &len){
   auto scene = cast_scene(_scene);
   Eigen::VectorXt x, v;
   scene->getInitialInternalState(x, v);
   if(x.size()!=v.size()){
      log_err("v (size %ld) is not the same size as x (size %ld).", x.size(), v.size());
      return false;
   }
   len = x.size();
   _x = new float[len];
   _v = new float[len];
   for(int i=0; i<len; i++){
      _x[i] = x[i];
      _v[i] = v[i];
   }
   return true;
}

void step(void* _scene){
   auto scene = cast_scene(_scene);
   log_reg("Stepping scene.");
   if(RFAIL(scene->step())) scene->setFailed();
   else scene->accumulate();
   return;
}

bool failed_check(void* _scene){
   auto scene = cast_scene(_scene);
   if(scene->checkFailed()) return true;
   return false;
}

char* get_state_attrs(void* _scene){
   auto scene = cast_scene(_scene);
   auto stateAttrs = scene->getStateAttrs();
   char* retVal = new char[stateAttrs.size()+1];
   strcpy(retVal, stateAttrs.c_str());
   return retVal;
}

char* get_state(void* _scene){
   auto scene = cast_scene(_scene);
   auto optState = scene->getState();
   CHECK_BOOL_AND_RET(!optState.has_value(), nullptr, "No state value.");
   auto stateStr = optState.value();
   char* retVal = new char[stateStr.size()+1];
   strcpy(retVal, stateStr.c_str());
   return retVal;
}

float get_dt(void* _scene){
   auto scene = cast_scene(_scene);
   return scene->getTimestep();
}

bool set_act(void* _scene, const char* _strand_name, float _act){
   auto scene = cast_scene(_scene);
   auto r = scene->setStrandAct(make_pair(_strand_name, _act));
   RCHECKERROR_RET(r, false, "Failed to set activation.");
   return true;
}

bool set_act_vec(void* _scene, const char* _strand_arr[], float* _act_arr, int _n_arr){
   auto scene = cast_scene(_scene);
   for(int iS=0; iS<_n_arr; iS++){
      auto r = scene->setStrandAct(_strand_arr[iS], _act_arr[iS]);
      RCHECKERROR_RET(r, false,
                      "Failed to set activation for %s(%d).", _strand_arr[iS], iS);
   }
   return true;
}
// Step
// No parse scene reset
// State string and vector
// angles of the joints, fingertip location, joint limits exceeded, strands taut.
// Return whether state finished
// Non blocking signal
// Activation rate capping/ max velocity of tendon

void step_for_time(void* _scene, float _time){
   auto scene = cast_scene(_scene);
}

void disable_wrench(void* _scene, const char* _wrench_name){
   auto scene = cast_scene(_scene);
   scene->setWrenchVal(_wrench_name, V6::Zero());
   scene->clearWrench(_wrench_name);
}

void script_rigid(void* _scene, const char* _rigid_name, float* _vel){
   auto scene = cast_scene(_scene);
   V6 v;
   v<<0, 0, 0, _vel[0], _vel[1], _vel[2];
   scene->setRigidScVel(_rigid_name, v);
   scene->clearRigidScVel(_rigid_name);
}

void script_wrench(void* _scene, const char* _wrench_name, float* _wrench){
   auto scene = cast_scene(_scene);
   V6 wrenchVal;
   wrenchVal<<_wrench[0], _wrench[1], _wrench[2], _wrench[3], _wrench[4], _wrench[5];
   scene->setWrenchVal(_wrench_name, wrenchVal);
   scene->clearWrench(_wrench_name);
}

void get_red_mat(void *_scene, float *&red_mat, float *&rhs, int &sz) {
   auto scene = cast_scene(_scene);
   Eigen::MatrixXt M;
   scene->getReducedMat(M);
   Eigen::VectorXt vRHS;
   scene->getRHS(vRHS);
   sz = M.rows();
   // cout<<MS_T(vRHS)<<" "<<vRHS.size()<<" "<<sz<<endl;
   red_mat = new float[sz*sz];
   rhs = new float[sz];
   for (int i = 0; i < sz; i++) {
      rhs[i] = vRHS(i);
      for (int j = 0; j < sz; j++) {
         red_mat[i*sz+j] = M(i,j);
      }
   }
   // cout<<"Pointer "<< red_mat<<endl;
}
