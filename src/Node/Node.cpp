/**
   @author: sachdevp
   @desc:   Node of a strand abstracted out.
*/

#include <Node/Node.hpp>
#include <DynObj/PointL.hpp>
#include <Node/CoordE.hpp>
#include <cstring>

using namespace Strands;
using namespace Eigen;
using namespace std;

Node::Node(SP<PointL> _point){
   this->setPoint(_point);
   this->pointId = _point->getId();
   extra = false;
   active = true;
   Force.setZero();
   nodeId = -1;
}

#define STRAND_MAT_INDEX_SETTER_DEFN(LE)        \
   RET Node::setStrandMat##LE##Idx(int idx){    \
      strandMat##LE##Index = idx;               \
      if(idx==-1) return R::Failure;            \
      return R::Success;                        \
   }

#define STRAND_MAT_INDEX_GETTER_DEFN(LE)                        \
   RET Node::getStrandMat##LE##Idx(int &idx){                   \
      idx = strandMat##LE##Index;                               \
      if(idx == -1){                                            \
         log_err("Probably accessing index before it is set."); \
         return R::Failure;                                     \
      }                                                         \
      return R::Success;                                        \
   }

STRAND_MAT_INDEX_SETTER_DEFN(L);
STRAND_MAT_INDEX_SETTER_DEFN(E);
STRAND_MAT_INDEX_GETTER_DEFN(L);
STRAND_MAT_INDEX_GETTER_DEFN(E);

RET Node::getNodeId(int &_nodeId){
   RCHECKERROR_B(_nodeId==-1, "Node id not set.");
   _nodeId = nodeId;
   return R::Success;
}
// Set the point to which the node corresponds.
RET Node::setPoint(SP<PointL> _point){
   if(this->pointL==nullptr){
      this->pointL = _point;
      log_verbose("Setting node at point %d: \n %s", _point->getPointId(), MS_T(_point->getx()));
      return RET::Success;
   }
   log_err("Trying to reset point.");
   return RET::Failure;
}

RET Node::getDebugOutput(std::string &dOut){
   dOut = "NodeForce: " + matrixString(Force.transpose()) + "\n";
   return R::Success;
}
// Correct count only returned if original count was zero.
// RET Node::findActivePost(Node* retNode, uint* nodesSkipped){
//    // Initialize retNode.
//    retNode = this;
//    // If this node is active, just return.
//    if(isActive()) return R::Success;
//    // Increment nodesSkipped count.
//    if(nodesSkipped) *nodesSkipped += 1;
//    // Find active node from the following nodes.
//    RCHECKERROR(post->findActivePost(retNode, nodesSkipped), "Could not find active post.");
//    return R::Success;
// }

void Node::getMassMultiplier(Eigen::Matrix<ftype, 3, 3> &Mm) {
   // CHECK Matrix may be passed by value. Confirm.
   MatrixXt J = pointL->getJacobian();
   // TODO Find if there is a faster way to find mass multiplier (A^T A)
   Mm = J*J.transpose() ;
}

// CHECK Pass by reference not value
MatrixXt Node::getJacobian() {return pointL->getJacobian();}

uint Node::getPointMatIndex(){return this->pointL->getIndex();};

// LNode and QNode related functions
#include <Node/LNode.hpp>
#include <Node/QNode.hpp>
ftype LNode::gets(){return this->s;}

ftype QNode::gets(){return this->coordE->gets();}

void LNode::sets(ftype _s){this->s = _s;}

LNode::LNode(SP<PointL> _point):Node(_point){}

void QNode::sets(ftype _s){this->coordE->sets(_s);}
// void QNode::setsdot(ftype _sdot){this->coordE->setsdot(_sdot);}

QNode::QNode(SP<PointL> _point):Node(_point){
   this->pointL = _point;
   this->pointId = _point->getId();
   this->coordE = new CoordE();
}
