#pragma once
#include <Node/Node.hpp>

class LNode: public Node{
private:
   ftype s;
public:
   LNode(SP<PointL> _point);
   bool isLag() override final{return true;};
   bool isEul() override final{return false;};
   void sets(ftype _s) override final;
   ftype gets() override final;

   // ftype getsdot() override final;
   // void setsdot(ftype _sdot) override final;
};
