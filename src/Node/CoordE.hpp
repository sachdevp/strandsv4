/**
   @author: sachdevp
   @desc:   Eulerian coordinate information for nodes
*/
#pragma once
#include <include_common.hpp>

// TODO Set is using matrix manager
class CoordE {
private:
   ftype s;
   ftype sdot;
   int is;
public:
   ftype gets() const {return s;}
   void sets(ftype _s) {this->s = _s;}
   ftype getsdot() const {return this->sdot;}
   void setsdot(ftype _sdot) {this->sdot = _sdot;}
   uint8_t getis() const {assert(is!=-1); return is;}
   void setis(uint8_t is) {this->is = is;}
   CoordE(){is = -1;}
};
