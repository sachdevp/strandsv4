/**
   @author: sachdevp
   @desc:   Node class for strands.
*/
#pragma once

#include <Object.hpp>
#include <DynObj/PointL.hpp>

class CoordE;

enum class NodeType{NodeFixed, NodeCurve, NodeFull};

class Node: public Object {
   friend class Strands::Strand;
private:
   uint nodeId;
   Eigen::Vector4t Force;
   // Doubly linked list
   // Node *pre, *post;
   bool active, extra;
   int strandMatLIndex;
   int strandMatEIndex;

protected:
   SP<PointL> pointL;
   NodeType nodeType;
   void setNodeId(uint _nodeId){this->nodeId = _nodeId;};
   RET setPoint(SP<PointL> _point);
   // this is only to be used when loading that the point can be set later
   uint pointId;

public:
   // Constructors
   Node(SP<PointL> _point);
   inline bool isActive(){return active;}
   inline bool isInActive(){return !isActive();}
   void setActive(bool _active = true){active = _active;}
   void setInActive(){active = false;}
   void markExtra(bool _extra){extra = _extra;}
   bool isExtra(){return extra;}
   RET setStrandMatLIdx(int idx);
   RET getStrandMatLIdx(int &idx);
   RET setStrandMatEIdx(int idx);
   RET getStrandMatEIdx(int &idx);
   RET getNodeId(int &_nodeId);
   
   // Get J^T*J from the dof
   void getMassMultiplier(Eigen::Matrix<ftype,3,3> &Mm);
   Eigen::MatrixXt getJacobian();
   virtual ftype gets(){return -1;};

   // virtual ftype getsdot();
   // virtual void setsdot(ftype _sdot);
   inline V3 getx(){return pointL->getx();}
   inline V3 getv(){return pointL->getv();}

   // Is the node Lagrangian?
   virtual bool isLag(){
      log_err("Abstract class.");
      return false;
   };
   // Is the node Eulerian?
   // Note: All E nodes are Q nodes. No pure E nodes
   virtual bool isEul(){
      log_err("Abstract class.");
      return false;
   };
   void addForce(Eigen::Vector3t fx, ftype fs){
      Force.segment<3>(0) += fx;
      Force(3) += fs;
   };
   uint getPointMatIndex();
   uint getPointId(){return this->pointL->getPointId();}
   RET Init(){return RET::Success;};
   virtual void sets(ftype _s) = 0;
   virtual RET getDebugOutput(std::string &s) override final;
   virtual RET clearForStep(ftype t) override final{
      Force.setZero();
      return R::Success;
   };
   inline auto getSourceDynObj(){return pointL->getDoFSource();};
};
