#pragma once
#include <Node/Node.hpp>

class QNode: public Node{
private:
   CoordE *coordE;
public:
   QNode(SP<PointL> _point);
   bool isLag() override final{return false;};
   bool isEul() override final{return true;};
   ftype gets() override final;
   void sets(ftype _s) override final;

   // ftype getsdot() override final;
   // void setsdot(ftype _sdot) override final;
};
