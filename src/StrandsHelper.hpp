/**
   @auth Prashant Sachdeva
   @desc Functions to help Maya plugin @SceneNode and @StrandsCmd
   process inputs to provide to Strands library.
 */

#pragma once

#include <include_common.hpp>
#include <DynObj/Strand.hpp>

class PointL;
class DynObj;

// Return initial position for new points based on joint paradigm and position.
// @sDir: Direction of strand wrt initial @joint position
RET getNewPointsPos(VECV3 &vPointsPos, VECV3 availPoints, SP<Strands::Joint> joint, int sDir, bool inOrder = true);

RET getDummyStrandNodeInputs
(SPVEC<PointL> &strandPoints, MAP<Strands::Joint> &pointJoints, SPVEC<PointL> &newPoints,
 Strands::Scene* scene, IVec pids);

// pointJoints marks each point that is affected by a joint. Map is
// NOT with pid. Map uses index in @strandPoints
RET getStrandNodeInputs
(SPVEC<PointL> &strandPoints, MAP<Strands::Joint> &pointJoints, SPVEC<PointL> &newPoints,
 Strands::Scene* scene, IVec pids);
// Create a PointL, based on the type constType
SP<PointL> createPointL
(Strands::Scene* scene, int constType, int rid,
 V3 constPos, V3 pointPos, M3 constAxes, bool useLocalPos, std::string name, bool record = true);

SP<PointL> createPointL
(Strands::Scene* scene, int constType, SP<DynObj> rigid,
 V3 constPos, V3 pointPos, M3 constAxes, bool useLocalPos, std::string name, bool record = true);

ftype signPre(V3 pa, V3 pb, V3 axis);
int sign(V3 pa, V3 pb, V3 axis);


// SP<PointL> createPointL
// (Strands::Scene* scene, int constType, SP<DynObj> rigid,
//  M4 constE, V3 pointPos, bool useLocalPos, std::string name);
