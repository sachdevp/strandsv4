#include <include_common.hpp>
#include <Segment.hpp>

class SegmentCurve: public Segment{
private:
   Strands::Joint* joint;
public:
   RET computeSegmentMKf();
   ftype getLength();
   SegmentCurve(Node* _n0, Node* _n1, StrandAttr*, Strands::Joint*, MatBlocks _M, MatBlocks _K);
};
