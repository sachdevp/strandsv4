#include <BaseObject.hpp>
RET BaseObject::Init(){
   this->bInit = true;
   return R::Success;
}

RET BaseObject::isInit(){
   if(this->bInit) return R::Success;
   return R::Failure;
}

void BaseObject::clear(){
   this->bInit = false;
   this->bDebug = false;
}

bool BaseObject::debugOutput(std::string &s){
   if(!bDebug){s = ""; return false;}
   if(RFAIL(getDebugOutput(s))){
      log_err("Could not get debug output for object %s.", getCName());
      return false;
   }
   return true;
}

RET BaseObject:: appendDebugOutput(std::string &s){
   std::string dOut;
   RCHECKERROR(getDebugOutput(dOut), "Could not debug %s", getCName());
   s += dOut;
   return R::Success;
}
