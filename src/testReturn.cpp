#include <iostream>

using namespace std;

int main(){
   RET r1 = RET::Success;
   RET r2 = RET::Success;
   RET r3 = RET::Failure;
   cout << (r1&=r2)<<endl;
   cout << (r1&=r3)<<endl;
   cout << (r1|=r2)<<endl;
   cout << (r1|=r3)<<endl;
}
