#include <SegmentCurve.hpp>
#include <DynObj/StrandAttr.hpp>
#include <Joint.hpp>

RET SegmentCurve::computeSegmentMKf() {
   ftype rho = getDensity();
   Node *n0, *n1;
   RCHECKERROR(getNodes(n0, n1), "Could not get nodes");
   log_random("Node locations: \n %s \n %s",
              MS_T(n0->getx()), MS_T(n1->getx()));

   // Get the node and strand values
   V3    x1 = n1->getx();
   V3    x0 = n0->getx();
   ftype s1 = n1->gets();
   ftype s0 = n0->gets();

   // Get different summations
   ftype ds = s1 - s0;

   // Calculate length, strain etc.
   ftype l = getLength();
   ftype epsilon = l/ds - 1.0f;

   // Vector3t dxhat = dx/l;
   M3 I = EYE3;
   ftype r = getRadius();

   log_random("Segment material length (ds): %.4f", ds);
   if(EQUALS(ds, 0)) log_err("Segment material length is too small.");

   // TODO Needs fixing!
   M3 mxx = ds*I;
   V3 mxs = ;
   ftype mss = dx.dot(dx)/ds;
   tMLL[0] = (rho/3) *   2  * mxx;
   tMLL[1] = (rho/3) *        mxx;
   tMLE[0] = (rho/3) * (-2) * mxs;
   tMLE[1] = (rho/3) * (-1) * mxs;
   tMEE[0] = (rho/3) *   2  * mss;
   tMEE[1] = (rho/3) *        mss;

   log_random("l, ds, eps:\t%f, %f, %f", l, ds, epsilon);
#ifdef STIFFNESS
   RCHECKERROR(strandAttr->material->computeStiffness(matStiffness, matInputs),
               "Could not compute material stiffness.");
#endif
   return strandAttr->material->computeForces(matForces, matInputs);
}

ftype SegmentCurve::getLength(){
   
}

ftype SegmentCurve::getRadius(){
}

SegmentCurve::SegmentCurve
(Node *_n0, Node *_n1, StrandAttr* strandAttr,
 Strands::Joint* _joint, MatBlocks _M, MatBlocks _K):
   Segment(_n0, _n1, strandAttr, _M, _K), joint(_joint){};
