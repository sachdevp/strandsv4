#pragma once

#include <include_common.hpp>

class Node;
class StrandAttr;

class Strands::MaterialForces{
public:
   V3    fx[2];
   ftype fs[2];
   MaterialForces(){
      clear();
   };
   RET clear(){
      fx[0].setZero();
      fx[1].setZero();
      fs[0] = 0;
      fs[1] = 0;
      return R::Success;
   }
};

class Strands::MaterialInputs{
public:
   Node* node0;
   Node* node1;
   SP<StrandAttr> strandAttr;
};

class Strands::MaterialStiffness{
public:
   M3 tKLL;
   V3 tKLE;
   ftype tKEE;
};

class Strands::Material{
private:
   ftype fStrainLimits[2];

protected:
   // Is activation available?
   bool bActivation;
   bool bInextensible;
   // Is gravity applied.
// TODO Currently unused.
   bool bGravity;

public:
   Material();
   RET clear();
   RET setInextensible(bool inex = true);
   RET setLimits(ftype _limits[2]);
   RET noGravity();
   bool canBeActive(){return bActivation;}
   bool isInextensible(){return bInextensible;};
   virtual RET computeForces(Strands::MaterialForces &matForces,
                             Strands::MaterialInputs matInputs)
   {return R::Success;};
   virtual RET computeStiffness(Strands::MaterialStiffness &matStiffness,
                                Strands::MaterialInputs matInputs);
   virtual RET setActivation(ftype){return R::Success;};
   static bool isMaterialActive(MatType);
};

