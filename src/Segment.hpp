/**
   @author: sachdevp
   @desc:   Helper class to compute segment mass. No instance.
*/
#pragma once

#include <include_common.hpp>
#include <Material.hpp>
#include <BaseObject.hpp>
#include <Node/Node.hpp>

class StrandAttr;
class MatBlocks{
public:
   Eigen::BlockM33t LL00, LL01, LL10, LL11;
   Eigen::BlockM31t LE00, LE01, LE10, LE11;
   Eigen::BlockM13t EL00, EL01, EL10, EL11;
   Eigen::BlockM11t EE00, EE01, EE10, EE11;
   MatBlocks(Eigen::BlockM33t _LL00,
             Eigen::BlockM33t _LL01,
             Eigen::BlockM33t _LL10,
             Eigen::BlockM33t _LL11,
             Eigen::BlockM31t _LE00,
             Eigen::BlockM31t _LE01,
             Eigen::BlockM31t _LE10,
             Eigen::BlockM31t _LE11,
             Eigen::BlockM13t _EL00,
             Eigen::BlockM13t _EL01,
             Eigen::BlockM13t _EL10,
             Eigen::BlockM13t _EL11,
             Eigen::BlockM11t _EE00,
             Eigen::BlockM11t _EE01,
             Eigen::BlockM11t _EE10,
             Eigen::BlockM11t _EE11):
      LL00(_LL00), LL01(_LL01), LL10(_LL10), LL11(_LL11),
      LE00(_LE00), LE01(_LE01), LE10(_LE10), LE11(_LE11),
      EL00(_EL00), EL01(_EL01), EL10(_EL10), EL11(_EL11),
      EE00(_EE00), EE01(_EE01), EE10(_EE10), EE11(_EE11){};
};

class Segment: public BaseObject {
private:
   ////////////////////////////////////////////////////////////////////////////
   // For indices [ix0,ix1,is0,is1], the mass matrix (8x8) looks as follows: //
   //    +---------------------------------+                                 //
   //    | tMLL(0) tMLL(1) tMLE(0) tMLE(1) |                                 //
   //    | tMLL(1) tMLL(0) tMLE(1) tMLE(0) |                                 //
   //    | tMLE(0) tMLE(1) tMEE(0) tMEE(1) |                                 //
   //    | tMLE(1) tMLE(0) tMEE(1) tMEE(0) |                                 //
   //    +---------------------------------+                                 //
   ////////////////////////////////////////////////////////////////////////////

   // MLL, MLE, MEL, MEE are block matrices picked from the Strand's mass matrix.
   // tMLL etc. are calculated to avoid double computations
   MatBlocks *M, *K;

   // Segment force calculations
   ftype fs, fes;
   Eigen::VectorXt fx, fex;
   bool active;
   bool matInitialized;

protected:
   // Temporary matrices to be filled by subclasses
   M3 tMLL[2];
   V3 tMLE[2];
   M1 tMEE[2];
   ftype getDensity();
   Strands::MaterialForces    mMatForces;
   Strands::MaterialStiffness mMatStiffness;

public:
   Strands::MaterialInputs    mMatInputs;
   RET clearForStep();
   Segment(Node *_n0, Node *_n1,
           SP<StrandAttr> strandAttr, MatBlocks *_M, MatBlocks *_K);
   RET basicInit(Node* _n0, Node* _n1, SP<StrandAttr> _strandAttr);
   Segment(Node *_n0, Node *_n1, SP<StrandAttr> strandAttr);
   inline V3
   getVec(){return (this->mMatInputs.node1->getx() - this->mMatInputs.node0->getx());}
   inline ftype getMatDiff(){return mMatInputs.node1->gets() - mMatInputs.node0->gets();}
   inline ftype getMatLength(){return fabs(getMatDiff());}
   inline V3 getDir(){return getVec()/getLength();}
   RET Init();
   RET fillNodeForces();
   RET setMatrices(MatBlocks* _M, MatBlocks* _K);
   // Computes the elements for the mass matrix for the segment.
   virtual RET computeSegmentMKf() = 0;
   virtual ftype getLength() = 0;
   virtual V3 getLenDiff(bool first) = 0;
   // Fills mass for the segment
   RET fillMK();
   RET fillInactiveMK();
   void setActive(bool _active = true){active = _active;}
   void setInActive(){active = false;}
   inline bool isActive(){return active;}
   inline bool isInActive(){return !isActive();}
   RET getNodes(Node* &, Node* &m, uint* nodesSkipped=nullptr);
};
