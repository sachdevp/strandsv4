#pragma once

#include <include_common.hpp>

class BaseObject{
protected:
   std::string name;
   bool bDebug;
   bool bInit;
   // Called by each object's own initialization function
   // Marks the object as initialized.
   RET Init();
   /** Query functions **/
   // Is it initialized? Ready for use?
   RET isInit();
   /** Set functions **/
   virtual RET getDebugOutput(std::string &s){return R::Failure;};
   void clear();

public:
   virtual RET setDebugOutput(){this->bDebug = true; return R::Success;};
   inline std::string getName(){return name;}
   inline const char* getCName(){return name.c_str();}
   RET appendDebugOutput(std::string &s);
   bool debugOutput(std::string &s);
};
