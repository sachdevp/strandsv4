#include <include_common.hpp>
#include <Eigen/Eigen>

// NOTE dir always assumed normalized throughout this file
inline V3 VecDir(V3 a, V3 b){
   V3 c = a-b;
   c.normalize();
   return c;
}

inline V3 CrossDir(V3 a, V3 b){
   V3 c = a.cross(b);
   c.normalize();
   return c;
}

inline V3 projectPerp(V3 a, V3 dir){
   return a-a.dot(dir)*dir;
}

inline ftype getAngle(V3 a, V3 b, V3 dir){
   a.normalize(); b.normalize();
   ftype angle = acos(a.dot(b));
   V3 c = a.cross(b);
   // Invert angle if cross does not match
   return (c.dot(dir)<0)?-angle:angle;
}
