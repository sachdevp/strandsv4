#include <include_common.hpp>
#include <Eigen/Eigen>

// NOTE dir always assumed normalized throughout the file
inline V3 VecDir(V3 a, V3 b){
   V3 c = a-b;
   c.normalize();
   return c;
}

inline V3 CrossDir(V3 a, V3 b){
   V3 c = a.cross(b);
   c.normalize();
   return c;
}

inline V3 projectPerp(V3 a, V3 dir){
   return a-(a.dot(dir)*dir);
}

inline V3 projectPerpDir(V3 a, V3 dir){
   V3 c = projectPerp(a, dir);
   c.normalize();
   return c;
}

inline ftype getAngle(V3 a, V3 b, V3 dir){
   a.normalize(); b.normalize();
   ftype angle = acos(a.dot(b));
   V3 c = a.cross(b);
   // Invert angle if cross does not match
   return (c.dot(dir)<0)?-angle:angle;
}

#define GET_Rp(Rot, p, E) M3 Rot; V3 p; SE3::getRp(Rot, p, E);
