#include <DynObj/Rigid.hpp>
#include <EigenHelper.hpp>
#include <SE3.hpp>
#include <Wrench.hpp>
using namespace std;
using namespace Eigen;
using namespace Strands;

#define CHECK_WRENCH_INITIALIZED(str) CHECK_INITIALIZED(str, Wrench)
#define CHECK_WRENCH_NOT_INITIALIZED(str) CHECK_NOT_INITIALIZED(str, Wrench)

Wrench::Wrench(SP<Rigid> _dynObj, KeysMap<V6> _wrenchMap): Force(), mWrenchMap(_wrenchMap) {
  mDynObjs.push_back(_dynObj);
  E = _dynObj->getTransform();
  nBlocks = 1;
}

R Wrench::Init() {
  CHECK_WRENCH_NOT_INITIALIZED("Already initialized.");
  bInit = true;
  return R::Success;
}

R Wrench::fillForce() {
  CHECK_WRENCH_INITIALIZED("Cannot compute force.");

  // Set the force
  V6 localWrenchVec;
  M3 rotE = SE3::getRotation(E);
  localWrenchVec.head<3>() = rotE.transpose() * mWrenchVec.head<3>();
  localWrenchVec.tail<3>() = rotE.transpose() * mWrenchVec.tail<3>();
  vForceBlocks[0] += localWrenchVec;
  log_random("%s wrench: %s", getCName(), MS_T(mWrenchVec));
  return R::Success;
}

R Wrench::getIndices(IVec &vIdx, IVec &vLen) {
   Force::getIndices(vIdx, vLen);
   vIdx.push_back(mDynObjs[0]->getIndex());
   vLen.push_back(mDynObjs[0]->getMatrixSize());
   return R::Success;
}

// Update the transform for the force. Note that the @Object transform matrix,
// @E, is in GLOBAL space, not local.
RET Wrench::update() {
   CHECK_WRENCH_INITIALIZED("Cannot update.");
   E = mDynObjs[0]->getTransform();
   return R::Success;
}

RET Wrench::clearForStep(ftype t){
   // mWrenchVec.setZero();
   for(pair<double, V6> wrenchPair: mWrenchMap){
      auto tKey = wrenchPair.first;
      // if(tKey>tWrenchSet && t>tKey){
      if(t>tKey){
         mWrenchVec = wrenchPair.second;
         // cout<<t<<" "<<tKey<<" "<<tWrenchSet<<" "<<mWrenchVec<<endl;
         tWrenchSet = tKey;
      }
   }
   // log_warn("Updating wrench: %3.2f, %s", t, MS_T(mWrenchVec));
   return R::Success;
}

R Wrench::setWrench(V6 wrench, ftype t){
   tWrenchSet = t;
   mWrenchVec = wrench;
   return R::Success;
}

R Wrench::clearWrenchMap(){
   mWrenchMap.clear();
   return R::Success;
}
