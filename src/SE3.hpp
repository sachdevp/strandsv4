#include <include_common.hpp>

using namespace Eigen;

namespace SE3{
   MatrixXt computeGamma(M3 R, V3 l);
   MatrixXt computeGamma(Matrix4t E);
   MatrixXt computeGamma(M3 R, ftype x, ftype y, ftype z);
   M3 computeBracket(V3 l);
   M3 getRotation(Matrix4t E);
   V3 getTranslation(Matrix4t E);
   RET getRp(M3 &R, V3 &p, M4 E);
   Matrix4t computeTransform(Vector6t phi, ftype h = 1.0);
   RET computeAdjoint(Matrix6t& Adj, Matrix4t E);
   RET computeERel(Matrix4t &aEb, Matrix4t aEw, Matrix4t bEw);

   // Compute the Euler angles corresponding to the transform provided.
   // This computation assumes XYZ ordering, i.e., R_z*R_y*R_x
   // Refer to http://www.staff.city.ac.uk/~sbbh653/publications/euler.pdf for details
   RET computeEulerRadians(std::array<ftype, 3> &angles, Matrix4t E);
   RET computeEulerDegrees(std::array<ftype, 3> &angles, Matrix4t E);
   RET getPosAngles(V3 &pos, V3 &angles, M4 E);
   RET getPosAngles(V3 &pos, std::array<ftype, 3> &angles, M4 E);
   RET snapMatrix(MatrixXt M);
}
// // Gets the upper 3x3 rotation matrix
// void se3_getRp(double *R, double *p, const double *E);
// // Sets the upper 3x3 rotation matrix
// void se3_setRp(double *E, const double *R, const double *p);
// // Inverts a transformation matrix
// void se3_invert(double *Ei, const double* E);
// // Gets the skew symmetric cross product matrix
// // unbracket
// void se3_unbracket(double *a, const double *S, int n = 3);
// // Gets the adjoint transform
// void se3_adjoint(double *Ad, const double *E);
// // Axis angle to rotation matrix
// void se3_aaToMat(double *R, const double *axis, double angle);
// void se3_aaToMat(double *R, double x, double y, double z, double angle);
// // Gets the upper 3x4 rotation matrix and 1x4 translation vector (homogeneous)
// void se3_getRpHomT(double *RH, double *pH, const double *E);
// // xout = E * xin
// void se3_transform(double *xout, const double *E, const double *xin);
// // Eout = E * Ein
// void se3_transformMat(double *Eout, const double *E, const double *Ein);
// // xout = E \ xin
// void se3_invTransform(double *xout, const double *E, const double *xin);
// // Eout = E \ Ein
// void se3_invTransformMat(double *Eout, const double *E, const double *Ein);

// // q: a unit quaternion with elements (x, y, z, w)
// // http://www.j3d.org/matrix_faq/matrfaq_latest.html

// // Converts a quaternion to a rotation matrix
// void se3_quatToMat(double *R, const double *q);
// // Converts a rotation matrix to a quaternion
// void se3_matToQuat(double *q, const double *R);
// // Interpolates a rotation using a quaternion
// void se3_interpQuat(double *R, const double *R0, const double *R1, double t, bool flip = false);
// // Gets the 4x3 matrix L such that \dot{q} = L * \omega, where \omega is the angular velocity in local coords
// void se3_quatJac(double *L, const double *q);

// // A = sqrtm(B)
// void se3_sqrt3x3(double *A, const double* B);

// // Ecur = interp(E0, E1, s)
// void se3_interpSE3(double* Ecur, double* E0, double * E1, double s);

// // Ecur = interp(E0, E1, s^2)
// void se3_quadr_interpSE3(double* Ecur, double* E0, double * E1, double s);

// // spherical interpolate two quaternion
// void se3_slerp(double * qt, double * q0, double* q1, double s);
