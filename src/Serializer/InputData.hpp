/**
 * @auth sachdevp@cs.ubc.ca
 * @desc Intermediate representation description for @Strands library @Scene object construction.
 */

#pragma once

#include <include_common.hpp>
#define INDICATOR_OID -150
class ObjInput{
public:
   int oid;
   std::string name;
   bool debug;
   ObjType objType;
};

typedef struct PointConstInput_t{
   int rid;
   int constType;
   M4  constE;
} PointConstInput;

class PointInput: public ObjInput{
public:
   int pid;
   // Global initial position
   V3 x;
   // Scripted velocity
   // FIXME Not used currently. Should be corrected.
   std::optional<V3> optScVel;
   // Constraint related attrs.
   std::optional<PointConstInput> optConst;
   PointInput(){objType = ObjType::POINT;};
};

class RigidInput: public ObjInput{
public:
   int rid;
   // A constraint might or might not be available.
   std::optional<M4> optConstE;
   M4 E, Ecom;
   std::array<ftype, 4> mass;
   // Velocity map only relevant if scripted. If not available, not scripted.
   bool bScripted;
   KeysMap<V3> scVelMap;
   RigidInput(){objType = ObjType::RIGID; bScripted = false;};
};

class StrandInput: public ObjInput{
public:
   int sid;
   MatType matType;
   bool inextensible;
   bool simStrand;
   // Radius of strand
   ftype r;
   // Volumentric density
   ftype rho;
   // Stiffness
   ftype k;
   // Activation parameter
   ftype dl_act;
   // Initial strain
   ftype initStrain;
   // Point ids.
   IVec pids;
   StrandInput(){objType = ObjType::STRND;};
};

class JointInput: public ObjInput{
public:
   JointInput(){objType = ObjType::JOINT;l6.setZero(); u6.setZero();}
   int jid, parentId, childId;
   // Type of joint limits
   JointLimitType limitType;
   // Inital angle set to?
   ftype ang0;
   char stiffnessType;
   std::array<ftype, 6> params;
   float k;
   // Linear damping of joint
   ftype damp;
   // Radius of joint for obstacle set method
   ftype rad;
   // Limits
   V6 l6, u6;
   M4 E;
   std::array<bool, 6> bConst;
};

class ForceInput: public ObjInput{
public:
   int fid;
   int dobjId;
   // Type of force
   int fType;
   // Magnitude of the force/torque.
   ftype mag;
   M4 E;
   KeysMap<V6> wrenchMap;
   ForceInput(){objType = ObjType::FORCE;};
   // bool operator==(const ForceInput &other) const;
};

class IndicatorInput: public ObjInput{
public:
   int iid;
   int rid;
   V3 localPos;
   IndicatorInput(){objType = ObjType::INDIC;};
};

class SceneInput{
public:
   std::map<int, ObjInput*> mapObjects;
   std::vector<ObjInput*> vObjects;
   std::string name;
   ftype   dt;
   int     dtx;
   ftype   stab;
   ftype   damp;
   Solver  sol;
   V3      grav;
   RET appendObjInput(ObjInput *objIn){
      mapObjects[objIn->oid] = objIn;
      vObjects.push_back(objIn);
      return R::Success;
   }
};
