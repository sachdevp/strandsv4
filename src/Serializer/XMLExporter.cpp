#include <Serializer/XMLExporter.hpp>
#include <Material.hpp>
#include <Joint.hpp>
#include <ctime>   // localtime
#include <iomanip> // put_time

using namespace std;

auto to_cstring(float f){return to_string(f).c_str();}
auto to_cstring(int d){return to_string(d).c_str();}

RET XMLExporter::createObjNode(pugi::xml_node &obj_node, ObjInput *oin){
   obj_node.append_attribute("oid").set_value(oin->oid);
   obj_node.append_attribute("name").set_value(oin->name.c_str());
   return R::Success;
}

RET XMLExporter::createRigidNode(pugi::xml_node &rigid_node, ObjInput *oin){
   auto rin = static_cast<RigidInput*>(oin);
   rigid_node.append_attribute("rid").set_value(rin->rid);
   string E = matrixStringFlat(rin->E);
   string Ecom = matrixStringFlat(rin->Ecom);
   rigid_node.append_child("E").text().set(E.c_str());
   rigid_node.append_child("Ecom").text().set(Ecom.c_str());
   rigid_node.append_child("mass").text().set(rin->mass[3]);
   auto MItext = (to_string(rin->mass[0])+" "+ to_string(rin->mass[1])+" "+ to_string(rin->mass[2]));
   rigid_node.append_child("MI").text().set(MItext.c_str());

   if(rin->optConstE.has_value()){
      auto constE = rin->optConstE.value();
      rigid_node.append_child("constE").text().set(MSF(constE));
   }

   if(rin->bScripted){
      auto sc_node = rigid_node.append_child("scripted");
      auto keysmap = rin->scVelMap;
      for(auto keypair: keysmap){
         auto key_child = sc_node.append_child("vel");
         key_child.append_attribute("time").set_value(keypair.first);
         key_child.text().set(MSF_T(keypair.second));
      }
   }
   return R::Success;
}

RET XMLExporter::createForceNode(pugi::xml_node &force_node, ObjInput *oin){
   auto fin = static_cast<ForceInput*>(oin);
   force_node.append_attribute("fid").set_value(fin->fid);
   force_node.append_attribute("dobjId").set_value(fin->dobjId);
   force_node.append_attribute("fType").set_value(fin->fType);
   force_node.append_child("mag").text().set(fin->mag);
   force_node.append_child("E").text().set(MSF(fin->E));
   auto sc_node = force_node.append_child("scripted");
   for(auto keypair:fin->wrenchMap){
      auto key_child = sc_node.append_child("wrench");
      key_child.append_attribute("time").set_value(keypair.first);
      key_child.text().set(MSF_T(keypair.second));
   }
   return R::Success;
}

RET XMLExporter::createPointNode(pugi::xml_node &point_node, ObjInput *oin){
   auto pin = static_cast<PointInput*>(oin);
   point_node.append_attribute("pid").set_value(pin->pid);
   point_node.append_child("x").text().set(MSF_T(pin->x));
   // auto sc_node = point_node.append_child("scripted");
   // auto velsc_node = sc_node.append_child("vel");
   // velsc_node.append_attribute("time").set_value(0);
   // velsc_node.text().set(MSF_T(pin->v_sc));
   if(pin->optConst.has_value()){
      auto pcin = pin->optConst.value();
      auto const_node = point_node.append_child("constraint");
      const_node.append_attribute("rid").set_value(pcin.rid);
      const_node.append_child("type").text().set(pcin.constType);
      const_node.append_child("E").text().set(MSF(pcin.constE));
   }
   return R::Success;
}

RET XMLExporter::createStrandNode(pugi::xml_node &strand_node, ObjInput *oin){
   auto sin = static_cast<StrandInput*>(oin);
   strand_node.append_attribute("sid").set_value(sin->sid);
   strand_node.append_attribute("initStrain").set_value(sin->initStrain);
   auto mat_node = strand_node.append_child("material");
   mat_node.append_attribute("type").set_value(static_cast<int>(sin->matType));
   mat_node.append_attribute("inextensible").set_value(sin->inextensible);
   mat_node.append_attribute("simple").set_value(sin->simStrand);
   mat_node.append_attribute("stiffness").set_value(sin->k);
   auto shape_node = mat_node.append_child("shape");
   shape_node.append_attribute("radius").set_value(sin->r);
   shape_node.append_attribute("rho").set_value(sin->rho);
   if(Strands::Material::isMaterialActive(sin->matType)){
      auto active_node = strand_node.append_child("active");
      active_node.append_attribute("dl_act").set_value(sin->dl_act);
   }

   // Collect up PIDs into a string.
   std::ostringstream oss;
   // Convert all but the last element to avoid a trailing ","
   std::copy(sin->pids.begin(), sin->pids.end()-1,
             std::ostream_iterator<int>(oss, " "));
   // Now add the last element with no delimiter
   oss << sin->pids.back();
   strand_node.append_child("pids").text().set(oss.str().c_str());
   return R::Success;
}

RET XMLExporter::createJointNode(pugi::xml_node &joint_node, ObjInput *oin){
   auto jin = static_cast<JointInput*>(oin);
   joint_node.append_attribute("jid").set_value(jin->jid);
   joint_node.append_attribute("parentId").set_value(jin->parentId);
   joint_node.append_attribute("childId").set_value(jin->childId);
   std::ostringstream oss;
   // Convert all but the last element to avoid a trailing ","
   for(int i=0; i<6; i++){
      if(jin->bConst[i]) oss<<1<<" ";
      else oss<<0<<" "; 
   }
   joint_node.append_attribute("constrained").set_value(oss.str().c_str());

   auto limits_node = joint_node.append_child("limits");
   limits_node.append_attribute("type").set_value(static_cast<int>(jin->limitType));
   if(Strands::Joint::needsUpper(jin->limitType))
      limits_node.append_child("upper").text().set(MSF_T(jin->u6));
   if(Strands::Joint::needsLower(jin->limitType))
      limits_node.append_child("lower").text().set(MSF_T(jin->l6));

   auto shape_node = joint_node.append_child("shape");
   shape_node.append_child("ang0").text().set(jin->ang0);
   shape_node.append_child("damping").text().set(jin->damp);
   shape_node.append_child("radius").text().set(jin->rad);
   auto stiffness_node = shape_node.append_child("stiffness");
   stiffness_node.append_attribute("type").set_value((string("")+jin->stiffnessType).c_str());
   if(jin->stiffnessType == 'e'){
      oss.str("");
      std::copy(jin->params.begin(), jin->params.end()-1,
                std::ostream_iterator<float>(oss, " "));
      // Now add the last element with no delimiter
      oss << jin->params.back();
      stiffness_node.text().set(oss.str().c_str());
   } else if(jin->stiffnessType=='l'){
      stiffness_node.text().set(jin->k);
   } else{
      log_err("Could not recognize stiffness type for joint %s.", jin->name.c_str());
   }
   
   joint_node.append_child("E").text().set(MSF(jin->E));
   return R::Success;
}

RET XMLExporter::createIndicatorNode(pugi::xml_node &indicator_node, ObjInput *oin){
   auto iin = static_cast<IndicatorInput*>(oin);
   indicator_node.append_attribute("iid").set_value(iin->iid);
   indicator_node.append_attribute("rid").set_value(iin->rid);
   indicator_node.append_child("localPos").text().set(MSF_T(iin->localPos));
   return R::Success;
}

RET XMLExporter::exportObj(ObjInput *objIn){
   auto node = scene_node.append_child(XMLNodeTitle[objIn->objType]);
   createObjNode(node, objIn);
   XMLCreateNodeFns[objIn->objType](node, objIn);
   return R::Success;
}

RET XMLExporter::exportScene(SceneInput sin){
   for(auto objIn: sin.vObjects){exportObj(objIn);}
   scene_node.append_attribute("dt").set_value(sin.dt);
   scene_node.append_attribute("dtx").set_value(sin.dtx);
   scene_node.append_child("stab").text().set(sin.stab);
   scene_node.append_child("damp").text().set(sin.damp);
   scene_node.append_child("grav").text().set(MSF_T(sin.grav));

   // Testing the export
   ofstream scene_stream(this->fname);
   if(scene_stream.good()){
      scene_doc.save(scene_stream,"  ");
   }
   return R::Success;
}

XMLExporter::XMLExporter(string _fname){
   // Check if file name is usable.
   if(_fname=="") log_err("Bad filename %s", _fname.c_str());
   fname = _fname;
}

// Refer to https://stackoverflow.com/questions/17223096/outputting-date-and-time-in-c-using-stdchrono
string current_time_and_date() {
    auto now = chrono::system_clock::now();
    auto in_time_t = chrono::system_clock::to_time_t(now);

    stringstream ss;
    ss << put_time(localtime(&in_time_t), "%Y-%m-%d %X");
    return ss.str();
}

RET XMLExporter::init(string label){
   scene_node = scene_doc.append_child("Scene");
   scene_node.append_attribute("name").set_value(label.c_str());
   scene_node.append_attribute("time_created")
      .set_value(current_time_and_date().c_str());
   XMLCreateNodeFns[ObjType::RIGID] = &createRigidNode;
   XMLCreateNodeFns[ObjType::STRND] = &createStrandNode;
   XMLCreateNodeFns[ObjType::JOINT] = &createJointNode;
   XMLCreateNodeFns[ObjType::FORCE] = &createForceNode;
   XMLCreateNodeFns[ObjType::POINT] = &createPointNode;
   XMLCreateNodeFns[ObjType::INDIC] = &createIndicatorNode;
   XMLNodeTitle    [ObjType::RIGID] = "rigid";
   XMLNodeTitle    [ObjType::STRND] = "strand";
   XMLNodeTitle    [ObjType::JOINT] = "joint";
   XMLNodeTitle    [ObjType::FORCE] = "force";
   XMLNodeTitle    [ObjType::POINT] = "point";
   XMLNodeTitle    [ObjType::INDIC] = "indicator";
   return R::Success;
}

RET XMLExporter::setTimestep(float _dt){
   scene_node.append_child("dt").text().set(_dt);
   return R::Success;
}

RET XMLExporter::setTimestepMultiplier(int _dtx){
   scene_node.append_child("dtx").text().set(_dtx);
   return R::Success;
}
