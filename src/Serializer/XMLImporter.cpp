#include <Serializer/XMLImporter.hpp>
#include <Material.hpp>
#include <Joint.hpp>

using namespace std;

map<ObjType, importFn> XMLImporter::XMLCreateObjInputFns;
map<string, ObjType > XMLImporter::XMLNodeType;

#define CHECK_GET(outVal, inOpt, msg, ...)      \
   if(!inOpt.has_value()){                      \
      log_err(msg, ##__VA_ARGS__);              \
      return nullopt;                           \
   } else{                                      \
      outVal = inOpt.value();                   \
   }

#define CHECK_BOOL(BOOL, MSG, ...) CHECK_BOOL_AND_RET(BOOL, nullopt, MSG, ##__VA_ARGS__)

template<int ni, int nj> Eigen::Matrix<ftype, ni, nj>
readMat(const char* matString){
   CHECK_BOOL_AND_RET(!matString || strcmp(matString, "")==0, 
                      (Eigen::Matrix<ftype, ni, nj>::Zero()), "Could not load matrix.");

   Eigen::Matrix<ftype, ni, nj> mat;
   istringstream iss(matString);
   for(int i=0; i<ni; i++) for(int j=0; j<nj; j++) iss>>mat(i,j);
   return mat;
}

#define READ_M4(NODE) readMatFromNode<4,4>(NODE)
#define READ_M4_CHILD(NODE, NAME) READ_M4((NODE).child(NAME))

template<int ni, int nj> inline Eigen::Matrix<ftype, ni, nj>
readMatFromNode(pugi::xml_node node){return readMat<ni, nj>(node.text().as_string());}

optional<ObjInput*> XMLImporter::createRigidInput(pugi::xml_node rigid_node){
   RigidInput* rin = new RigidInput();
   rin->rid  = rigid_node.attribute("rid").as_int();
   rin->E    = READ_M4_CHILD(rigid_node, "E");
   rin->Ecom = READ_M4_CHILD(rigid_node, "Ecom");
   rin->mass[3]  = rigid_node.child("mass").text().as_float();
   auto MIstring = rigid_node.child("MI").text().as_string();

   istringstream iss(MIstring);
   vector<float> readFloats((istream_iterator<float>(iss)), istream_iterator<float>());
   copy_n(readFloats.begin(), 3, rin->mass.begin());
   auto constE_node = rigid_node.child("constE");
   if(!constE_node.empty()) rin->optConstE = READ_M4(constE_node);

   auto sc_node = rigid_node.child("scripted");

   if(!sc_node.empty()){
      rin->bScripted = true;
      for (pugi::xml_node obj_node = sc_node.first_child(); obj_node;
           obj_node = obj_node.next_sibling()){
         auto key_child = sc_node.child("vel");
         auto time = key_child.attribute("time").as_float();
         V3 vel = readMatFromNode<3, 1>(key_child);
         rin->scVelMap[time] = vel;
      }} else rin->bScripted = false;

   return rin;
}

optional<ObjInput*> XMLImporter::createForceInput(pugi::xml_node force_node){
   ForceInput* fin = new ForceInput();
   fin->fid    = force_node.attribute("fid")   .as_int();
   fin->dobjId = force_node.attribute("dobjId").as_int();
   fin->fType  = force_node.attribute("fType") .as_int();
   fin->mag    = force_node.child    ("mag")   .text  ().as_float();
   fin->E      = READ_M4_CHILD(force_node, "E");
   auto sc_node = force_node.child("scripted");

   if(!sc_node.empty()){
      for (pugi::xml_node wrench_node = sc_node.first_child(); wrench_node;
           wrench_node = wrench_node.next_sibling()){
         CHECK_BOOL(strcmp(wrench_node.name(),"wrench")!=0, "Expected wrench under scripted.");
         auto time = wrench_node.attribute("time").as_float();
         fin->wrenchMap[time] = readMatFromNode<6,1>(wrench_node);
      }}

   return fin;
}

optional<ObjInput*> XMLImporter::createPointInput(pugi::xml_node point_node){
   PointInput* pin = new PointInput();
   pin->pid = point_node.attribute("pid").as_int();
   pin->x   = readMatFromNode<3, 1>(point_node.child("x"));
   auto const_node = point_node.child("constraint");
   if(!const_node.empty()){
      PointConstInput pcin;
      pcin.rid  = const_node.attribute("rid").as_int();
      pcin.constType = const_node.child("type").text().as_int();
      pcin.constE = READ_M4_CHILD(const_node, "E");
      pin->optConst = pcin;
   }
   return pin;
}

optional<ObjInput*> XMLImporter::createStrandInput(pugi::xml_node strand_node){
   StrandInput* sin = new StrandInput();
   sin->sid = strand_node.attribute("sid").as_int();

   sin->initStrain = strand_node.attribute("initStrain").as_float();
   auto mat_node = strand_node.child("material");
   sin->matType = static_cast<MatType>(mat_node.attribute("type").as_float());
   sin->inextensible = mat_node.attribute("inextensible").as_bool();
   sin->simStrand = mat_node.attribute("simple").as_bool();
   sin->k = mat_node.attribute("stiffness").as_float();
   auto shape_node = mat_node.child("shape");
   sin->r = shape_node.attribute("radius").as_float();
   sin->rho = shape_node.attribute("rho").as_float();
   if(Strands::Material::isMaterialActive(sin->matType)){
      auto active_node = strand_node.child("active");
      sin->dl_act = active_node.attribute("dl_act").as_float();
   }

   auto pids_string = strand_node.child("pids").text().as_string();
   istringstream iss(pids_string);

   vector<int> readPids((istream_iterator<int>(iss)), istream_iterator<int>());
   for(auto v: readPids) sin->pids.push_back(v);
   return sin;
}

optional<ObjInput*> XMLImporter::createJointInput(pugi::xml_node joint_node){
   JointInput* jin = new JointInput();
   jin->jid      = joint_node.attribute("jid")     .as_int();
   jin->parentId = joint_node.attribute("parentId").as_int();
   jin->childId  = joint_node.attribute("childId") .as_int();
   auto constString = joint_node.attribute("constrained").as_string();

   istringstream iss(constString);
   // Convert all but the last element to avoid a trailing ","
   // Split the stream and copy into jin->bConst
   vector<bool> readBools((istream_iterator<int>(iss)),
                          istream_iterator<int>());
   CHECK_BOOL(readBools.size()!=6, "Could not read bools.");
   copy_n(readBools.begin(), 6, jin->bConst.begin());

   auto limits_node = joint_node.child("limits");
   jin->limitType = static_cast<JointLimitType>(limits_node.attribute("type").as_int());

   if(Strands::Joint::needsUpper(jin->limitType)) jin->u6 = readMatFromNode<6,1>(limits_node.child("upper"));
   if(Strands::Joint::needsLower(jin->limitType)) jin->l6 = readMatFromNode<6,1>(limits_node.child("lower"));

   // Shape
   auto shape_node = joint_node.child("shape");
   jin->ang0 = shape_node.child("ang0").text().as_float();
   jin->damp = shape_node.child("damping").text().as_float();
   jin->rad  = shape_node.child("radius").text().as_float();
   auto stiffness_node = shape_node.child("stiffness");
   jin->stiffnessType = stiffness_node.attribute("type").as_string()[0];

   if(jin->stiffnessType == 'e'){
      iss.str(stiffness_node.text().as_string());
      iss.clear();

      vector<float> vals;
      copy(istream_iterator<float>(iss), istream_iterator<float>(), back_inserter(vals));
      CHECK_BOOL(vals.size()!=6, "Could not read float parameters.");
      copy_n(vals.begin(), 6, jin->params.begin());
   } else if(jin->stiffnessType=='l'){
      jin->k = stiffness_node.text().as_float();
   } else{
      log_err("Could not stiffness.");
   }

   jin->E = READ_M4_CHILD(joint_node, "E");
   return jin;
}

optional<ObjInput*> XMLImporter::createIndicatorInput(pugi::xml_node indicator_node){
   IndicatorInput* iin = new IndicatorInput();
   iin->iid      = indicator_node.attribute("iid").as_int();
   iin->rid      = indicator_node.attribute("rid").as_int();
   iin->localPos = readMatFromNode<3, 1>(indicator_node.child("localPos"));
   return iin;
}

optional<ObjInput*> XMLImporter::createObjInput(pugi::xml_node node){
   auto objType = XMLNodeType[(const char*)node.name()];
   CHECK_BOOL(XMLCreateObjInputFns.find(objType)==XMLCreateObjInputFns.end(),
              "No input function for : %s", node.name());

   auto optObjIn = XMLCreateObjInputFns[objType](node);
   ObjInput* objIn;
   CHECK_GET(objIn, optObjIn, "Could not get object input.");
   objIn->oid  = node.attribute("oid") .as_int();
   objIn->name = node.attribute("name").as_string();
   return objIn;
}

inline bool isObj(const char* name){
   if(strcmp(name, "strand"   )==0 || strcmp(name, "point")==0 ||
      strcmp(name, "joint"    )==0 || strcmp(name, "rigid")==0 ||
      strcmp(name, "indicator")==0 || strcmp(name, "force")==0) return true;
   return false;
}

optional<SceneInput> XMLImporter::importScene(){
   SceneInput sin;
   pugi::xml_node scene_node;
   for (pugi::xml_node obj_node = scene_doc.first_child(); obj_node; obj_node = obj_node.next_sibling()){
      if(strcmp(obj_node.name(),"Scene")==0){
         scene_node = obj_node;
         log_reg("Scene node found.");
      }}

   for (pugi::xml_node obj_node = scene_node.first_child(); obj_node; obj_node = obj_node.next_sibling()){
      if(!isObj(obj_node.name())) continue;
      // log_warn("Loading object %s.", obj_node.name());
      auto optObjIn = createObjInput(obj_node);
      ObjInput* objIn;
      CHECK_GET(objIn, optObjIn, "Could not get object input.");
      sin.appendObjInput(objIn);
   }

   sin.dt = scene_node.attribute("dt").as_float();
   sin.dtx = scene_node.attribute("dtx").as_float();
   pugi::xml_node stabc = scene_node.child("stab");
   if(stabc) sin.stab = scene_node.child("stab").text().as_float();
   else sin.stab = 0;
   sin.damp = scene_node.child("damp").text().as_float();
   sin.grav = readMatFromNode<3, 1> (scene_node.child("grav"));
   return sin;
}

XMLImporter::XMLImporter(string _fname){
   // Check if file name is usable.
   bFailed = false;
   if(_fname=="") log_err("Bad filename %s", _fname.c_str());
   auto parse_result = scene_doc.load_file(_fname.c_str());
   log_reg("Status %d for file %s", parse_result.status, _fname.c_str());
   if(parse_result.status != pugi::status_ok){
      log_err("Could not import file.");
      bFailed = true;
   }
}

RET XMLImporter::init(){
   XMLCreateObjInputFns [ObjType::RIGID] = &createRigidInput;
   XMLCreateObjInputFns [ObjType::STRND] = &createStrandInput;
   XMLCreateObjInputFns [ObjType::JOINT] = &createJointInput;
   XMLCreateObjInputFns [ObjType::FORCE] = &createForceInput;
   XMLCreateObjInputFns [ObjType::POINT] = &createPointInput;
   XMLCreateObjInputFns [ObjType::INDIC] = &createIndicatorInput;
   XMLNodeType["rigid"]     = ObjType::RIGID;
   XMLNodeType["strand"]    = ObjType::STRND;
   XMLNodeType["joint"]     = ObjType::JOINT;
   XMLNodeType["force"]     = ObjType::FORCE;
   XMLNodeType["point"]     = ObjType::POINT;
   XMLNodeType["indicator"] = ObjType::INDIC;
   return R::Success;
}

bool XMLImporter::checkFailed(){return bFailed;}
