#include <Serializer/InputData.hpp> 

class Exporter{
public:
   virtual RET init(std::string label) = 0;
   virtual RET exportScene(SceneInput) = 0;
};
