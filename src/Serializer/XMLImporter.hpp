#include <Serializer/Import.hpp>
#include <pugixml.hpp>

typedef std::optional<ObjInput*> (*importFn)(pugi::xml_node);
class XMLImporter: Importer{
public:
   RET init();
   XMLImporter(std::string filename);
   std::optional<SceneInput> importScene();
   bool checkFailed();

private:
   // File to import to.
   pugi::xml_document scene_doc;
   static std::map<ObjType, importFn> XMLCreateObjInputFns;
   static std::map<std::string, ObjType>    XMLNodeType;
   static std::optional<ObjInput*>     createRigidInput(pugi::xml_node);
   static std::optional<ObjInput*>     createPointInput(pugi::xml_node);
   static std::optional<ObjInput*>     createJointInput(pugi::xml_node);
   static std::optional<ObjInput*>     createForceInput(pugi::xml_node);
   static std::optional<ObjInput*>    createStrandInput(pugi::xml_node);
   static std::optional<ObjInput*> createIndicatorInput(pugi::xml_node);
   static std::optional<ObjInput*>       createObjInput(pugi::xml_node);
   bool bFailed;
};
