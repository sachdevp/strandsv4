#include <Serializer/Export.hpp>
#include <pugixml.hpp>

typedef RET (*exportFn)(pugi::xml_node&, ObjInput *oin);
class XMLExporter: Exporter{
public:
   RET init(std::string label);
   XMLExporter(std::string filename);
   RET exportScene(SceneInput);
   RET setTimestepMultiplier(int dtx);
   RET setTimestep(float);

private:
   pugi::xml_document scene_doc;
   pugi::xml_node     scene_node;
   // File to export to.
   std::string fname;
   std::map<ObjType, exportFn> XMLCreateNodeFns;
   std::map<ObjType, const char*> XMLNodeTitle;
   static RET createObjNode      (pugi::xml_node&, ObjInput*);
   static RET createRigidNode    (pugi::xml_node&, ObjInput*);
   static RET createPointNode    (pugi::xml_node&, ObjInput*);
   static RET createJointNode    (pugi::xml_node&, ObjInput*);
   static RET createForceNode    (pugi::xml_node&, ObjInput*);
   static RET createStrandNode   (pugi::xml_node&, ObjInput*);
   static RET createIndicatorNode(pugi::xml_node&, ObjInput*);
   RET exportObj(ObjInput *objIn);
};
