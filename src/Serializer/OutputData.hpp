#pragma once

#include <include_common.hpp>
class ObjOutput{
public:
   int oid;
};

class PointOutput: ObjOutput{
public:
   int pid;
   V3 v;
   // DoF center
   M4 dofE;
   V3 dofPos;
   std::array<ftype, 3> dofAng;
   // Point position
   V3 x;
   std::string getVelStr(){return MSNE_T(v);}
   std::string getPosStr(){return MSNE_T(x);}
   std::optional<std::string> getAttr(std::string attrName){
      if(attrName == "vel") return getVelStr();
      if(attrName == "pos") return getPosStr();
      return {};
   }
};

class RigidOutput: ObjOutput{
public:
   int rid;
   V6 v;
   M4 E;
   V3 pos;
   V3 ang;
   std::string getVelStr(){return MSNE_T(v);}
   std::string getPosStr(){return MSNE_T(pos);}
   std::string getAngStr(){return MSNE_T(ang);}
   std::optional<std::string> getAttr(std::string attrName){
      if(attrName == "vel") return getVelStr(); 
      if(attrName == "pos") return getPosStr();
      if(attrName == "ang") return getAngStr();
      return {};
   }
};

class JointOutput: ObjOutput{
public:
   int jid;
   V6 drift;
   V6 v;
   // Transformation of the joint globally, wrt A, and wrt B
   M4 E, Ea, Eb;
   // Position and orientation of the joint
   V3 pos, ang;
   V3 posA, angA, posB, angB;
   std::array<ftype, 3> angles;
   std::array<int, 3> exceeded;
   // Constraint force, useful when exceeded
   std::array<ftype, 6> constForce;
   std::string getVelStr(){return MSNE_T(v);}
   std::string getPosStr(){return MSNE_T(pos);}
   std::string getAngStr(){return std::to_string(angles[2]);}
   std::string getExceededStr(){
      return std::to_string(exceeded[0]) + " "
         + std::to_string(exceeded[1])+ " "
         + std::to_string(exceeded[2]);
   }

   std::optional<std::string> getAttr(std::string attrName){
      if(attrName == "vel") return getVelStr();
      if(attrName == "pos") return getPosStr();
      if(attrName == "ang") return getAngStr();
      if(attrName == "exceeded") return getExceededStr();
      return {};
   }
};

class StrandOutput: ObjOutput{
public:
   int sid;
   ftype strain, prevStrain;
   ExtraNodes extraNodes;
   bool inexActive;
   ftype constForce;
   std::string getInexStr(){return std::to_string(inexActive);}
   std::string getStrainStr(){return std::to_string(strain);}
   std::optional<std::string> getAttr(std::string attrName){
      if(attrName == "strain") return getStrainStr();
      if(attrName == "taut"  ) return getInexStr();
      return {};
   }
};

class IndicatorOutput: ObjOutput{
public:
   // Indicator id
   int iid;
   // Indicator velocity
   V3 v;
   // Indicator position
   V3 x;
   std::string getVelStr(){return MSNE_T(v);}
   std::string getPosStr(){return MSNE_T(x);}
   std::optional<std::string> getAttr(std::string attrName){
      if(attrName == "vel") return getVelStr();
      if(attrName == "pos") return getPosStr();
      return {};
   }
};
