#include <Serializer/InputData.hpp> 

class Importer{
public:
   virtual RET init() = 0;
   virtual std::optional<SceneInput> importScene() = 0;
};
