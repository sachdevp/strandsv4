/**
   Indicator.hpp
   @author: sachdevp
   @desc: Indicator set to extract output for the location. Does not play into the dynamics.
           
*/
#pragma once
#include <include_common.hpp>
#include <Serializer/OutputData.hpp>

class Indicator{
private:
   int indId;
   V3 l;
   int rigidId;
   std::string indName;

public:
   Indicator(int indId, std::string indName, V3 l, int rigidId);
   // void clear();
   // void setRigid(SP<Rigid> _rigid){rigid = _rigid;};
   // void setIndId(int id){indId = id;}
   int getIndicatorId(){return indId;};
   int getRigidId(){return rigidId;}
   V3 getx(RigidOutput rOut);
   V3 getv(RigidOutput rOut);
   // Added for consistency.
   // bool toBeRecorded(){return true;};
};
