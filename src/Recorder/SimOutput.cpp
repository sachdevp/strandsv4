#include <Recorder/SimOutput.hpp>
#include <include_common.hpp>

RET SimOutput::postRecord(){
   for(auto sPair: this->frames.back()->vSOut){
      auto &curr_size= vSMaxExtraNodes[sPair.first];
      auto curr_max = sPair.second.extraNodes.size();
      curr_size = std::max(curr_max, curr_size);
      if(curr_size!=vSMaxExtraNodes[sPair.first])
         log_err("Assignment error.");
   }
   return R::Success;
}

int SimOutput::getMaxStrandNodes(int sid){
   auto it = vSMaxExtraNodes.find(sid);
   if(it == vSMaxExtraNodes.end()){
      log_err("Strand does not exist: %d", sid);
      return -1;
   }
   return it->second;
}

RET SimOutput::combineMap(NameMap nameMap, ObjType objType){
   for(auto nameId: nameMap){
      std::string name = nameId.first;
      auto id = nameId.second;
      RCHECKERROR_B(mObjMap.find(name)!=mObjMap.end(),
                    "Found %s in ObjMap already", name.c_str());
      mObjMap[name] = std::make_pair(objType, id);
   }
   return R::Success;
}

RET SimOutput::test(){
   SP<SimFrame> prevFrame = frames[0];
   int frameIdx = 0;
   for(auto frame: offset(frames, 1)){
      // frameIdx++;
      // for(auto strandPair: prevFrame->vSOut){
      //    int strandIdx = strandPair.first;
      //    auto prevSOut = strandPair.second;
      //    auto currSOut = frame->vSOut[strandIdx];
      //    auto prevStrain = prevSOut.strain;
      //    auto prevStrain2 = currSOut.prevStrain;
      //    if(!currSOut.inexActive)
      //       log_err("Frame number %d, strand %d - prev strain %3.2e %3.2e - Not active", frameIdx, strandIdx, prevStrain, prevStrain2);
      //    if(currSOut.inexActive){
      //       log_err("Frame number %d, strand %d - prev strain %3.2e %3.2e - Active", frameIdx, strandIdx, prevStrain, prevStrain2);
      //    }
      // }
      prevFrame = frame;
   }
   return R::Success;
}

RET SimOutput::appendFrame(SimFrame* frame){
   frames.push_back(SP<SimFrame>(frame));
   return R::Success;
}
