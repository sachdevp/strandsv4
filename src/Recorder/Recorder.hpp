#pragma once
#include <include_common.hpp>
#include "SimOutput.hpp"

class PointL;
class Rigid;
class StrandOutput;
class JointOutput;
class PointOutput;
class RigidOutput;

class Recorder{
public:
   std::string mDebugFile;
   bool bRecorded, bContinued;
   int dFrameCount, dTotalFrames;
   Recorder();
   // RET recordFrame(SimFrame* simFrame, Strands::Scene*);

   SimFrame* recordFrame(std::map<int, SP<Indicator>> indMap, Strands::Scene* scene);
   // Set expected number of total frames
   RET addFrames(int);
   RET setFinished();
   RET test(SimFrame *simFrame);
   RET clear();
   int getMaxStrandNodes(SimFrame *simFrame, int sid);
   // [frames done, total expected frames, recorded?]
   IVec getStatus();
   RET setResumed();
   // Returns a map containing any debug info that was output for this
   // simulation
   RET stepScene(Strands::Scene*, int nStepsInFrame);
private:
   RET recordStrandData   (StrandOutput &sOut   , SP<Strands::Strand> strand);
   RET recordJointData    (JointOutput &jOut    , SP<Strands::Joint> joint);
   RET recordPointData    (PointOutput &pOut    , SP<PointL> point);
   RET recordIndicatorData(IndicatorOutput &iOut, RigidOutput rOut, SP<Indicator> indicator);
   RET recordRigidData    (RigidOutput &rOut    , SP<Rigid> rigid);
};
