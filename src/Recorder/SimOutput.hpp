#pragma once
#include <include_common.hpp>
#include <maya/MObject.h>
#include <Serializer/OutputData.hpp>
#include <Recorder/Indicator.hpp>

// Map from name to pair of ObjType and id.
using ObjMap   = std::map<std::string, std::pair<ObjType, int>>;
using NameMap  = std::map<std::string, int>;
using IdObjMap = std::map<int, MObject>;

typedef struct SimFrame_t{
   float t;
   std::map<int, PointOutput    > vPOut;
   std::map<int, RigidOutput    > vROut;
   std::map<int, JointOutput    > vJOut;
   std::map<int, StrandOutput   > vSOut;
   std::map<int, IndicatorOutput> vIOut;
} SimFrame;

class SimOutput{
public:
   SPVEC<SimFrame> frames;
   std::map<int, size_t> vSMaxExtraNodes;
   RET postRecord();
   IdObjMap strandMObjs;
   IdObjMap pointMObjs;
   IdObjMap forceMObjs;
   IdObjMap jointMObjs;
   IdObjMap rigidMObjs;
   std::map<int, SP<Indicator>> indMap;
   // Map from name of object to type and map key.
   ObjMap mObjMap;

   RET combineMap(NameMap nameMap, ObjType objType);
   int getMaxStrandNodes(int sid);
   RET test();
   RET appendFrame(SimFrame*);
};
