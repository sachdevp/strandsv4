#include "Recorder.hpp"
#include <Scene/Scene.hpp>
#include <DynObj/Strand.hpp>
#include <Joint.hpp>
#include <DynObj/Rigid.hpp>
#include <DynObj/PointL.hpp>
#include <Recorder/Indicator.hpp>
#include <algorithm>
#include <SE3.hpp>

using namespace Eigen;
using namespace std;
// NOTE toBeRecorded check ensures that we don't try to output objects that are not modeled in the
// scene.
#define recordObjsData(outMap, inVector, Type)                          \
   for(auto inst: inVector){                                            \
      int id = inst->get ## Type ## Id();                                \
      if(inst->toBeRecorded())                                           \
         RCHECKERROR_RNULL(record##Type##Data((outMap[id]), inst), "Could not record data"); \
   }

RET Recorder::clear(){
   bRecorded = false;
   dFrameCount = 0;
   dTotalFrames = 0;
   return R::Success;
}

RET Recorder::setFinished(){
   bRecorded = true;
   return R::Success;
}

RET Recorder::setResumed(){
   bRecorded = false;
   bContinued = true;
   return R::Success;
}

RET Recorder::addFrames(int _nAddFrames){
   dTotalFrames = dTotalFrames + _nAddFrames;
   return R::Success;
}

Recorder::Recorder(){}

RET Recorder::stepScene(Strands::Scene* scene, int nStepsInFrame){
   scene->cleanAcc();
   for(int iStep=0; iStep<nStepsInFrame; iStep++) RCHECKERROR(scene->step(),"Simulation step failed.");
   scene->accumulate();
   return R::Success;
}

// Needs: Drift values, 3 transforms (Ea, Eb, E)
RET Recorder::recordJointData(JointOutput &jOut, SP<Strands::Joint> joint){
   jOut.jid = joint->getJointId();
   jOut.E   = joint->getTransform();
   array<bool, 3> constrained;
   RCHECKERROR(joint->computeEqDrift6(jOut.drift),       "Failed to get drift."                 );
   RCHECKERROR(joint->getEDebug      (jOut.Ea, jOut.Eb), "Failed to get E for debugging."       );
   RCHECKERROR(joint->getExceeded    (jOut.exceeded, constrained),    "Failed to get exceeded for debugging.");
   RCHECKERROR(joint->getAngles      (jOut.angles),      "Failed to get angles."                );
   RCHECKERROR(joint->getConstForce  (jOut.constForce),  "Failed to get constraint force."      );

   RCHECKRET(SE3::getPosAngles(jOut.posA, jOut.angA, jOut.Ea));
   RCHECKRET(SE3::getPosAngles(jOut.posB, jOut.angB, jOut.Eb));
   RCHECKRET(SE3::getPosAngles(jOut.pos,  jOut.ang,  jOut.E));
   return R::Success;
}

// Needs only strain
RET Recorder::recordStrandData(StrandOutput &sOut, SP<Strands::Strand> strand){
   sOut.sid        = strand->getStrandId();
   sOut.strain     = strand->getStrain();
   sOut.prevStrain = strand->getPrevStrain();
   RCHECKERROR(strand->getExtraNodes(sOut.extraNodes), "Could not get extra nodes.");
   sOut.inexActive = strand->isInexActiveAcc();
   if(sOut.inexActive){
      RCHECKERROR(strand->getConstForce(sOut.constForce), "Could not get constraint force for strand");
   } else sOut.constForce = 0.0;
   return R::Success;
}

RET Recorder::recordPointData(PointOutput &pOut, SP<PointL> point){
   pOut.pid  = point->getPointId();
   pOut.dofE = point->getDoFEExt();
   pOut.x    = point->getx();
   pOut.v    = point->getv();
   RCHECKRET(SE3::getPosAngles(pOut.dofPos, pOut.dofAng, pOut.dofE));
   return R::Success;
}

RET Recorder::recordIndicatorData(IndicatorOutput &iOut, RigidOutput rOut, SP<Indicator> ind){
   iOut.iid  = ind->getIndicatorId();
   iOut.x    = ind->getx(rOut);
   iOut.v    = ind->getv(rOut);
   return R::Success;
}

// Needs rigid transform
RET Recorder::recordRigidData(RigidOutput &rOut, SP<Rigid> rigid){
   rOut.rid = rigid->getRigidId();
   rOut.E   = rigid->getTransformExt();
   rOut.v   = rigid->getv();
   RCHECKRET(SE3::getPosAngles(rOut.pos, rOut.ang, rOut.E));
   return R::Success;
}

SimFrame* Recorder::recordFrame(std::map<int, SP<Indicator>> indMap, Strands::Scene* scene){
   // log_verbose("Sim frame #%zu", simOut->frames.size());
   auto simFrame = new SimFrame();
   if(!simFrame){
      log_err("Frame created is null.");
      return nullptr;
   }
   recordObjsData(simFrame->vSOut, scene->getStrands(), Strand);
   recordObjsData(simFrame->vJOut, scene->getJoints(), Joint);
   recordObjsData(simFrame->vPOut, scene->getPoints(), Point);
   recordObjsData(simFrame->vROut, scene->getRigids(), Rigid);
   // recordObjsData(simFrame->vIOut, scene->vIndicators, Indicator);
   for(auto inst: indMap){
      auto& indicator = inst.second;
      int id = indicator->getIndicatorId();
      RCHECKERROR_RNULL(recordIndicatorData
                        ((simFrame->vIOut[id]), simFrame->vROut[indicator->getRigidId()],
                         indicator), "Could not record indicators.");
   }

   dFrameCount++;
   return simFrame;
}

IVec Recorder::getStatus(){
   IVec retVal;
   retVal.push_back(dFrameCount);
   retVal.push_back(dTotalFrames);
   retVal.push_back(bRecorded);
   return retVal;
}
