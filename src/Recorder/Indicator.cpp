#include<Recorder/Indicator.hpp>
#include <Serializer/OutputData.hpp>

using namespace Eigen;

V3 Indicator::getx(RigidOutput rOut){
   // TODO Write this function
   return (rOut.E*V4(l[0], l[1], l[2], 1.0)).head<3>();
}
V3 Indicator::getv(RigidOutput rOut){
   // TODO Write this function
   return V3::Zero();
}

Indicator::Indicator(int _indId, std::string _indName, V3 _l, int _rigidId){
   indId = _indId;
   indName = _indName;
   l = _l;
   rigidId = _rigidId;
}
