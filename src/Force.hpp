#pragma once

#include <Object.hpp>
#include <DynObj/DynObj.hpp>
// class DynObj;

class Strands::Force: public Object{
protected:
   SPVEC<DynObj> mDynObjs;

   // Pair of force and index where to apply it
   std::vector<Eigen::BlockVXt> vForceBlocks;
   int forceId;
   size_t nBlocks;
   bool bBlocksInit;

public:
   // NOTE Any derived class must only add to forces, not reset the value.
   virtual RET fillForce() = 0;
   // Process any changes to dynobjs to update force transform and magnitude
   virtual RET update() = 0;
   virtual RET Init() = 0;

   R setForceId(int _fid);
   R setForceBlocks(std::vector<Eigen::BlockVXt> _blocks);
   virtual R getIndices(IVec &vIdx, IVec &vLen);
   virtual RET clearForStep(ftype t) override = 0;
   std::string getNames() {return name+" " + mDynObjs[0]->getName()+MS_T(vForceBlocks[0]);}
};
