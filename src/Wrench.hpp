#pragma once

#include <Force.hpp>
class Rigid;

class Strands::Wrench: public Strands::Force{
private:
   V6 mWrenchVec;
   KeysMap<V6> mWrenchMap;
   float tWrenchSet = -1;
public:
   Wrench(SP<Rigid> _dynObj, KeysMap<V6> wrenchMap);
   virtual R fillForce() override final;
   // Update the direction based on the rigid body
   virtual R update()    override final;
   virtual R Init()      override final;
   virtual R getIndices(IVec &vIdx, IVec &vLen) override final;
   virtual R clearForStep(ftype t) override final;
   R setWrench(V6 wrench, ftype t);
   R clearWrenchMap();
};
