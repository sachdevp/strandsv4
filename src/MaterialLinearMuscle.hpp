#pragma once

#include <Material.hpp>

class Strands::MaterialLinearMuscle: public Strands::Material{
public:
   // @dl_act: Length change on activation 1.0
   MaterialLinearMuscle(ftype _stiffness, ftype dl_act);
   virtual RET computeForces(Strands::MaterialForces &matForces,
                             Strands::MaterialInputs matInputs) override final;
   // virtual RET computeStiffness(Strands::MaterialStiffness &matStiffness,
   //                              Strands::MaterialInputs matInputs) override;
   virtual RET setActivation(ftype _a) override final;
private:
   ftype fStiffness, fAct, fDl_act;
};
