/**
   @author: sachdevp 
   @desc: Helps XML file reading. All functions related to XML reading should be
   stored here. No direct reading XML files.
*/
#include <include_common.hpp>
#include <xml_helper.hpp>
#include <pugixml.hpp>
#include <Eigen/Eigen>

namespace XML{

   const char* strId = "id";
   const char* strName = "name";
   const char* strParams = "params";
   const char* strGravity =  "gravity";
   const char* strTransform = "transformation";

   // For points
   const char* strPoints = "points";
   const char* strPoint = "point";
   const char* strPointType = "type";
   const char* strGlobalPos = "global_pos";

   // For rigids
   const char* strRigids = "rigids";
   const char* strRigid = "rigid";

   // For joints
   const char* strJoints = "joints";
   const char* strJoint = "joint";
   const char* strRigid1 = "ra";
   const char* strRigid2 = "rb";
   const char* strConstrained = "Constrained";

   // For strands
   const char* strStrands = "strands";
   const char* strStrand = "strand";
   const char* strNode = "node";
   const char* strObject = "object";

   // For nodes
   const char* strNodeType = "NodeType";
   const char* strLorQ = "LorQ";
   const char* strNodeFixed = "fixed";
   const char* strNodeFull = "full";
   const char* strNodeCurve = "curve";

   // For Strand
   const char* strRadius = "radius";
   const char* strDensity = "rho";
   const char* strNodes = "nodes";

   RET getMatrix(Eigen::MatrixXt& matrix, const xml_node& parent, const char* child, int m, int n){
      ftype *data = new ftype[m*n];
      RET result = getChildVarArr(data, m*n, parent, child);
      if (result == RET::Success){
         matrix = Eigen::MatrixXt::Map(data, m, n);
         return RET::Success;
      }
      else return RET::Failure;
   }

   // Special case of getMatrix
   RET getMatrix4t(Eigen::Matrix4t& matrix, const xml_node& parent, const char* child){
      ftype data[16];
      RET result = getChildVarArr(data, 16, parent, child);
      if (result == RET::Success){
         matrix = Eigen::Map<Eigen::Matrix4t>(data);
         return RET::Success;
      }
      else return RET::Failure;
   }
   RET getVector(Eigen::VectorXt& v, uint8_t sz, const xml_node& parent, const char* child){
      ftype *data = new ftype[sz];
      RET result = getChildVarArr(data, sz, parent, child);
      if (result == RET::Success){
         v = Eigen::VectorXt::Map(data, sz);
         return RET::Success;
      }
      else return RET::Failure;
   }
   RET getVector3t(Eigen::Vector3t& v, const xml_node& parent, const char* child){
      ftype data[3];
      RET result = getChildVarArr(data, 3, parent, child);
      if (result == RET::Success){
         v = Eigen::Map<Eigen::Vector3t>(data);
         return RET::Success;
      }
      else return RET::Failure;
   }

}
