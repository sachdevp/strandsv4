#include <SE3.hpp>
#include <string>
#include <iostream>

/* Constructs a 3x6 Jacobian matrix */
namespace SE3{
   MatrixXt computeGamma(M3 R, V3 l) {
      M3 lb = computeBracket(l);
      MatrixXt Gamma(3, 6), tmp(3, 6);
      tmp<< lb.transpose(), EYE3; // Concatenate into tmp
      Gamma = R * tmp;
      return Gamma;
   }

   MatrixXt computeGamma(M4 E) {
      return computeGamma(E.block<3, 3>(0,0), E.block<3, 1>(0, 3));
   }

   MatrixXt computeGamma(M3 R, ftype x, ftype y, ftype z) {
      V3 l;
      l<<x, y, z;
      MatrixXt Gamma = computeGamma(R, l);
      return Gamma;
   }

   M3 getRotation(M4 E){
      return E.block<3,3>(0,0);
   }

   V3 getTranslation(M4 E){
      return E.block<3,1>(0,3);
   }

   RET getRp(M3 &_R, V3 &p, M4 E){
      _R = E.block<3,3>(0,0);
      p = E.block<3,1>(0,3);
      return R::Success;
   }

   M3 computeBracket(V3 l){
      M3 lb;
      lb << 0, -l(2), l(1),
         l(2), 0, -l(0),
         -l(1), l(0), 0;
      return lb;
   }

   // Rodriguez formula applied on phi
   // E0 is the initial transform
   M4 computeTransform(V6 phi, ftype h){
      V3 w, v, cc, d, d1;
      M4 phib;
      M3 R, I = EYE3;
      V3 p;
      // rotation matrix
      // Block<M4, 3, 3> R = E.block<3,3>(0,0);
      // Block<M4, 3, 1> p = E.block<3,1>(0,3);
      ftype wx, wy, wz, wlen, c, s, c1, wv;
      w = phi.head<3>();
      v = phi.tail<3>();
      wlen = w.norm();
      phib.setZero();
      if (wlen < THRESH){
         phib.setIdentity();
         phib.block<3, 1>(0,3) = h*v;
      } else{
         w /= wlen;
         v /= wlen;
         wv = w.dot(v);
         wx = w(0);   wy = w(1);   wz = w(2);
         c = cos(wlen*h);
         s = sin(wlen*h);
         c1 = 1 - c;

         R << //         c + wX * wX * c1, -wZ * s + wX * wY * c1, wY * s + wX * wZ * c1,
            // wZ * s + wX * wY * c1, c + wY * wY * c1, -wX * s + wY * wZ * c1,
            // -wY * s + wX * wZ * c1, wX * s + wY * wZ * c1, c + wZ * wZ * c1;
           c + wx * wx * c1,
            -wz * s + wx * wy * c1,
            wy * s + wx * wz * c1,
            wz * s + wx * wy * c1, 
            c + wy * wy * c1,
            -wx * s + wy * wz * c1,
            -wy * s + wx * wz * c1,
            wx * s + wy * wz * c1,
            c + wz * wz * c1;
         cc = w.cross(v);
         d = (I - R) * cc;
         V3 p = w * (wv*wlen*h) + d;
         phib.block<3,3>(0,0) = R;
         phib.block<3,1>(0,3) = p;
         phib(3,3) = 1;
         // log_random(HS("cc") "\n" HS("w") "\n" HS("d") "\n" HS("phi") "\n" HS("phib"), MS_T(cc), MS_T(w),MS_T(d),MS_T(phi), MS(phib));
      }

      return phib;
   }

   RET computeAdjoint(M6& Adj, M4 E){
      Block<M4, 3, 3> R = E.block<3,3>(0,0);
      Block<M4, 3, 1> p = E.block<3,1>(0,3);
      Adj.setZero();
      Adj.block<3, 3>(3,0) = computeBracket(p)*R;
      Adj.block<3, 3>(0,0) = R;
      Adj.block<3, 3>(3,3) = R;
      return RET::Success;
   }

   // Inverts second matrix and left multiples to first
   // So new transform: (w -> b) * (a -> w) = a -> b
   // (Transform multiplications are read right to left as usual)
   RET computeERel(M4 &aEb, M4 aEw, M4 bEw){
      // aEb = wEb*aEw
      aEb = bEw.inverse()*aEw;
      if(IS_MATRIX_NULL(aEb)){
         log_err("Failed computation of relative transformation\n"
                 HS("aEw") "\n" HS("bEw") "\n",
                 MS(aEw), MS(bEw));
         return R::Failure;
      }
      return R::Success;
   }
   
   RET getPosAngles(V3 &pos, std::array<ftype, 3> &angles, M4 E){
      pos = SE3::getTranslation(E);
      RCHECKWARN(SE3::computeEulerRadians(angles, E), "Could not compute angles.");
      return R::Success;
   }

   RET getPosAngles(V3 &pos, V3 &angles, M4 E){
      pos = SE3::getTranslation(E);
      std::array<ftype, 3> anglesArr;
      RCHECKWARN(SE3::computeEulerRadians(anglesArr, E), "Could not compute angles.");
      for(int i=0; i<3; i++) angles[i] = anglesArr[i];
      return R::Success;
   }
   
   RET computeEulerRadians(std::array<ftype, 3> &angles, M4 E){
      if(fabs(fabs(E(2, 0))-1)>EPSILON){
         angles[1] = -asin(E(2, 0));
         auto c1   = cos(angles[1]);
         angles[0] = atan2(E(2, 1)/c1, E(2, 2)/c1);
         angles[2] = atan2(E(1, 0)/c1, E(0, 0)/c1);
      } else{
         angles[2] = 0;
         if(fabs(E(2, 0) + 1)<EPSILON){
            angles[1] = M_PI/2;
            angles[0] = angles[2] + atan2(E(0, 1), E(0, 2));
         } else{
            angles[1] = -M_PI/2;
            angles[0] = -angles[2] + atan2(E(0, 1), E(0, 2));
         }
      }
      return R::Success;
   }

   RET computeEulerDegrees(std::array<ftype, 3> &angles, M4 E){
      auto r = computeEulerRadians(angles, E);
      for(auto &angle: angles) angle = angle * (180.0f/M_PI);
      // log_verbose("angles^T = %s", MS_T(angles));
      return r;
   }

   RET snapMatrix(MatrixXt M){
      for (int ii=0;ii<M.rows();ii++)
         for (int jj=0;jj<M.cols();jj++)
            if (fabs(M(ii,jj))<EPSILON) M(ii,jj) = 0;
      return R::Success;
   }
}
