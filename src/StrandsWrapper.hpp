// #include <include_common.hpp>
// #include <Serializer/Import.hpp>

extern "C" {
   void*       load_scene      (const char* fname);
   const char* get_label       (void* _scene);
   void        step            (void* _scene);
   void        step_for_time   (void* _scene, float _time);
   char*       get_state       (void* _scene);
   // Use reduced x and v vectors to bring scene to state.
   bool        set_initial_state(void* _scene, float* _x, float* _v, int len, float t=0.0);
   bool        get_internal_state(void* _scene, float* &_x, float* &_v, int &len);
   void        get_red_mat      (void* _scene, float* &red_mat, float* &rhs, int &len);
   char*       get_state_attrs (void* _scene);
   float       get_dt          (void* _scene);
   // Returns true if failed.
   bool        failed_check    (void* _scene);
   // Returns true if activation succeeded.
   bool        set_act         (void* _scene, const char* _strand_name , float act);
   bool        set_act_vec     (void* _scene, const char* _strand_arr[], float* _act_arr, int _n_arr);
   void        disable_wrench  (void* _scene, const char* _wrench);
   void        script_rigid    (void* _scene, const char* _rigid_name  , float* _vel);
   void        script_wrench   (void* _scene, const char* _wrench_name , float* _wrench);
};
