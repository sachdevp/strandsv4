#pragma once

#define SCENE_NODE_ID 66
#define STRAND_NODE_ID 0x8701D
#define DEFAULT_GRAVITY 0.0f, -9.79f, 0.0f
#define DEFAULT_TIMESTEP 0.01f
#define DEFAULT_DAMPING 0.01f
#define DEFAULT_MULTIPLIER 1
#define DEFAULT_SCALE 1.0f

#define STIFFNESS
// #define EXPLICIT_JOINT_DAMPING

/** Macros **/
#define IS_SCENE_NODE(node)  (node->typeId() == MTypeId(SCENE_NODE_ID))
#define IS_STRAND_NODE(node) (node->typeId() == MTypeId(STRAND_NODE_ID))

#define kFTRigidForce            "RigidForce"
#define kFTRigidForceInt         0
#define kFTWrench                "Wrench"
#define kFTWrenchInt             1

#define kSpotShapeInt       0
#define kLineShapeInt       1
#define kCircleShapeInt     2
#define kCylinderShapeInt   3
// Types of constraints
// World constraint
#define kCTFixedConstraint    "fixed"
// DoFFixed to rigid body
#define kCTParentConstraint   "parent"
// Constrained to line w.r.t. rigid body
#define kCTLineConstraint     "linear"
// Constrained to circle w.r.t. rigid body
#define kCTCircleConstraint   "circle"
// Constrained to cylinder w.r.t. rigid body
#define kCTCylinderConstraint "cylinder"
// Int correspondents for enumAttr
#define kCTFixedConstraintInt    0
#define kCTParentConstraintInt   1
#define kCTLineConstraintInt     2
#define kCTCircleConstraintInt   3
#define kCTCylinderConstraintInt 4
#define kCTFreeConstraintInt     5
