#include <Joint.hpp>
#include <JointStiffness.hpp>
#include <JointDamping.hpp>
#include <SE3.hpp>
#include <DynObj/DynObj.hpp>
#include <SceneConstants.hpp>

using namespace std;
using namespace Eigen;

using namespace Strands;
#define CONSTRAINED(i) (this->constrained & (1<<i))
Joint::Joint(SP<DynObj> _ra, SP<DynObj> _rb, std::array<bool, 6> _constrained, M4 _E):
   ra(_ra), rb(_rb), Object(){
   log_verbose("Creating joint");
   this->E = _E;
   this->constrained = 0x00;
   for(int i=0;i<6; i++){this->constrained |= (_constrained[i])<<i;}
   // Default radius
   shape.rad  = 1;
   shape.ang0 = 0.0;
   shape.k    = 0.0;

   bActive        = true;
   bInitStiffness = false;
   bInitDamping   = false;
   mLimitType     = JointLimitType::FREE;

   constImp        = {};
   constImpSum     = {};
   constStabImp    = {};
   constStabImpSum = {};
   constForce      = {};

   angles = {};
}

RET Joint::Init(){
   this->nC = 0;
   this->nCi = 0;
   for(int i=0; i<6;i++){
      if(!CONSTRAINED(i)){
         log_random("Joint %s free in row %d", ObjName(this), i);
         this->nCi++;
      } else this->nC++;
   }

   this->iCa.setZero(nC, 6);
   this->iCb.setZero(nC, 6);
   this->uCa.setZero(nCi, 6);
   this->uCb.setZero(nCi, 6);
   this->ic.setZero(nC, 1);
   this->uc.setZero(nCi, 1);
   this->dampVec.setZero(nCi, 1);
   // FIXME This works currently since unconstrained axes are all at the top of the vector. Won't
   // work when that is broken.
   for(int i=0; i<nCi; i++){dampVec(i) = fDamp;}

   Matrix4t aEw = ra->getTransform();
   Matrix4t bEw = rb->getTransform();
   j0Ew = this->getTransform();

   // a -> w -> j0
   SE3::computeERel(aEj0, aEw, j0Ew);
   SE3::computeERel(bEj0, bEw, j0Ew);

   pa = aEj0.inverse().block<4, 1>(0, 3);
   pb = bEj0.inverse().block<4, 1>(0, 3);

   setTransforms();

   return R::Success;
}

RET Joint::setDamping(ftype _d){
   bInitDamping = true;
   fDamp = _d;
   return R::Success;
}

VectorXt Joint::getDamping(){return dampVec;}

RET Joint::computeConstraint(){
   Matrix4t aEw = ra->getTransform();
   Matrix4t bEw = rb->getTransform();
   // TODO Make sure that this is updated
   Matrix4t jEw = this->getTransform();
   Matrix4t aEj, bEj, bEa;
   aEj = jEw.inverse() *aEw;
   bEa = aEw.inverse() *bEw;
   // SE3::computeERel(aEj, aEw, jEw);
   // SE3::computeERel(bEa, bEw, aEw);
   bEj = aEj * bEa;

   log_random(HS("aEj"), MS(aEj));
   log_random(HS("bEa"), MS(bEa));
   log_random(HS("bEj"), MS(bEj));
   SE3::computeAdjoint(Adja, aEj);
   SE3::computeAdjoint(Adjb, bEj);
   log_random(HS("Adja"), MS(Adja));
   log_random(HS("Adjb"), MS(Adjb));
   // Fill jth row of C with ith row of Adj based on constrained

   // je -> Equality, ji -> Inequality
   int je=0, ji = 0, ii;
   for(ii=0; ii<6;ii++){
      if(CONSTRAINED(ii)){
         this->iCa.row(je) =  Adja.row(ii);
         this->iCb.row(je) = -Adjb.row(ii);
         je++;
      } else{
         this->uCa.row(ji) = -Adja.row(ii);
         this->uCb.row(ji) =  Adjb.row(ii);
         ji++;
      }}
   log_random(HS("iCa"), MS(iCa));
   log_random(HS("iCb"), MS(iCb));

   log_random(HS("uCa"), MS(uCa));
   log_random(HS("uCb"), MS(uCb));

   this->ic.setZero();
   this->uc.setZero();

   return R::Success;
}

// @exceeded 0 if not exceeded, 1 if lower limit exceeded, 2 if upper limit exceeded
RET Joint::computeExceeded(){
   exceeded = {};
   if(mLimitType == JointLimitType::FREE){return R::Success;}
   // Only implemented for angles
   for(int i=0; i<3;i++){
      // Find ones not constrained and increment count.
      if (!CONSTRAINED(i)){
         if(mLimitType == JointLimitType::LOWER || mLimitType == JointLimitType::RANGE){
            if(this->angles[i]<(lowerLimit[i]-.01+THRESH)){
               exceeded[i]=1;
            }
         }
         if(mLimitType == JointLimitType::UPPER || mLimitType == JointLimitType::RANGE){
            if(this->angles[i]>(upperLimit[i]-THRESH)) {
               exceeded[i]=2;
            }}}}
   return R::Success;
}

RET Joint::getExceeded(std::array<int, 3> &outExceeded, std::array<bool, 3> &outConstrained){
   outExceeded = exceeded;
   outConstrained[0] = constrained & 1<<0;
   outConstrained[1] = constrained & 1<<1;
   outConstrained[2] = constrained & 1<<2;
   return R::Success;
}

RET Joint::computeIneqDrift(VectorXt &drift){
   RCHECKERROR_B(drift.size()!=getnExceeded(), "Drift vector of incorrect size.");
   BlockVXt blockDrift = drift.segment(0, drift.size());
   return computeIneqDrift(blockDrift);
   // drift.setZero();
   // int idx=0, dIdx=0;
   // // CHECK Sign of the drift calculation
   // for(auto e: exceeded){
   //    switch(e){
   //    case 1:
   //       // Lower limit drift should only be negative.
   //       drift[dIdx] = min<ftype>(0.0, angles[idx]-lowerLimit[idx]);
   //       dIdx++;
   //       break;
   //    case 2:
   //       // Upper limit drift should only be positive.
   //       drift[dIdx] = max<ftype>(0.0, angles[idx]-upperLimit[idx]);
   //       dIdx++;
   //       break;
   //    }
   //    idx++;
   // }
   // return R::Success;
}

RET Joint::computeIneqDrift(BlockVXt &drift){
   drift.setZero();
   int idx=0, dIdx=0;
   // CHECK Sign of the drift calculation
   for(auto e: exceeded){
      switch(e){
      case 1:
         // Lower limit drift should only be negative.
         drift[dIdx] = min<ftype>(0.0, angles[idx]-lowerLimit[idx]);
         dIdx++;
         break;
      case 2:
         // Upper limit drift should only be positive.
         drift[dIdx] = max<ftype>(0.0, angles[idx]-upperLimit[idx]);
         dIdx++;
         break;
      }
      idx++;
   }
   return R::Success;
}

RET Joint::computeEqDrift(BlockVXt& drift){
   drift.setZero();
   if(CONSTRAINED(3) && CONSTRAINED(4) && CONSTRAINED(5)){
      Matrix4t aEw = ra->getTransform();
      Matrix4t bEw = rb->getTransform();
      Matrix4t jEw = this->getTransform();
      Matrix4t bEa, aEj, bEj;
      SE3::computeERel(bEa, bEw, aEw);
      SE3::computeERel(aEj, aEw, jEw);
      bEj = aEj * bEa;
      // Set position drift terms
      drift.tail<3>() = (aEj*pa - bEj*pb).head<3>();
      return R::Success;
   }
   // Issue with eq drift computation.
   return R::Failure;
}

RET Joint::computeEqDrift6(Vector6t &v){
   v.setZero();
   VectorXt tmpv;
   tmpv.setZero(getnC());
   VectorBlock<VectorXt, Dynamic> vb = tmpv.segment(0, getnC());
   RCHECKERROR(computeEqDrift(vb), "Could not compute drift");
   int ti = 0;
   for(int ii=0; ii<6; ii++){
      if(CONSTRAINED(ii)){
         v[ii] = tmpv[ti];
         ti++;
      }}
   return R::Success;
}

uint Joint::getJointId(){return this->jointId;}

RET Joint::setJointId(int _jid){
   this->jointId = _jid;
   return R::Success;
}

RET Joint::getCIndices(int &inda, int &indb){
   inda = ra->getRedIndex();
   indb = rb->getRedIndex();
   return R::Success;
}

void Joint::getCDoFs(int &cols_a, int &cols_b){
   cols_a = ra->getnRed();
   cols_b = rb->getnRed();
}

RET Joint::getEDebug(M4 &_Ea, M4 &_Eb){
   _Ea = Ea;
   _Eb = Eb;
   return R::Success;
}

RET Joint::setTransforms(){
   Matrix4t aEw = ra->getTransform();
   Matrix4t bEw = rb->getTransform();
   // E = Ea specifies the position of joint with respect to a.
   // Eb is joint position with respect to drift; used for debug purposes
   this->E = this->Ea = aEw * aEj0.inverse() ;
   this->Eb          = bEw * bEj0.inverse() ;
   return R::Success;
}

RET Joint::update(){
   RCHECKERROR_B(!bInitStiffness, "Joint stiffness not initialized.");
   RCHECKERROR_B(!bInitDamping, "Joint damping not initialized.");
   Matrix4t aEw = ra->getTransform();
   Matrix4t bEw = rb->getTransform();
   Vector3t va = SE3::getRotation(aEw)*ra->getv().head<3>();
   Vector3t vb = SE3::getRotation(bEw)*rb->getv().head<3>();
   setTransforms();
   M4 wEa  = aEw.inverse() ;
   M4 bEa  = wEa  * bEw;
   M4 bEj  = aEj0 * bEa;
   M4 j0Ej = bEj  * bEj0.inverse() ;

   RCHECKERROR(SE3::computeEulerRadians(angles, j0Ej), "Could not compute angles for joint %s", getCName());

   // Torque computation in clearForStep
   #ifdef EXPLICIT_JOINT_DAMPING
   dampTorque[axis] = -damp   * (vb-va)[axis];
   mJDamp->updateTorque(dampTorque);
   #endif
   return R::Success;
}

RET Joint::setConstImp(VectorXt eqImp, VectorXt ineqImp, VectorXt eqStabImp, VectorXt ineqStabImp){
   RCHECKERROR_B(  eqImp.rows()     != getnC(),        "Incorrected size for equality constraint force.");
   RCHECKERROR_B(ineqImp.rows()     != getnExceeded(), "Incorrected size for inequality constraint force.");
   RCHECKERROR_B(  eqStabImp.rows() != getnC(),        "Incorrected size for equality constraint force.");
   RCHECKERROR_B(ineqStabImp.rows() != getnExceeded(), "Incorrected size for inequality constraint force.");
   // Zero it out.
   constImp     = {};
   constStabImp = {};
   // Iterate over constrained DoFs and set equality force
   int iEq = 0;
   for(int i=0; i<6;i++){
      if(CONSTRAINED(i)){
         log_random("Joint %s constrained in row %d", ObjName(this), i);
         constImp[i] = eqImp[iEq];
         constStabImp[i] = eqStabImp[iEq];
         iEq++;
      }
   }
   // Iterate over exceeded inequality constrained DoFs and set inequality force
   int inEq = 0;
   if (ineqImp.rows()==0) return R::Success;
   for(int j=0; j<3; j++){
      if(exceeded[j] == 1 || exceeded[j]==2){
         // Limit exceeded
         constImp[j] = ineqImp[inEq];
         constStabImp[j] = ineqStabImp[inEq];
         inEq++;
      }
   }

   // Add up for large step
   for(int i=0; i>6; i++){
      constImpSum[i]     += constImp[i];
      constStabImpSum[i] += constStabImp[i];
   }

   return R::Success;
}

RET Joint::clearForStep(float t){
   RCHECKERROR(computeExceeded(), "Could not check whether joint limits exceeded.");
   V3 torque;
   torque.setZero();
   int axis = static_cast<int>(AXIS::theta_z);
   // torque[axis] = shape.k * (angles[axis] - shape.ang0);

   // Compute stiffness /////
   ftype theta_deg = angles[axis]*180/M_PI;
   ftype theta_rad = angles[axis];

   // FIXME Much of this logic belongs to JointStiffness.
   switch(shape.type){
   case 'l':
      torque[axis] = shape.k * (theta_rad - shape.ang0);
      break;
   case 'e':
      torque[axis] = get<0>(shape.params) *
         (exp(-get<1>(shape.params) * (theta_deg - get<4>(shape.params)))-1)
         - get<2>(shape.params) *
         (exp(get<3>(shape.params)*(theta_deg - get<5>(shape.params)))-1) ;
   }
   /////////////////////

   M3 rotE = SE3::getRotation(E);

   torque = rotE*torque;
   mJStiff->updateTorque(torque);
   return R::Success;
}

RET Joint::setLowerLimit(V6 lim){
   lowerLimit = lim;
   return R::Success;
}

RET Joint::setUpperLimit(V6 lim){
   upperLimit = lim;
   return R::Success;
}

RET Joint::setLimitType(JointLimitType jlt){
   mLimitType = jlt;
   return R::Success;
}

bool Joint::checkRigids(SP<DynObj> r1, SP<DynObj> r2, bool *inOrder){
   if(r1 == ra && r2 == rb){
      if(inOrder) *inOrder = true;
      return true;
   }
   if(r1 == rb && r2 == ra){
      if(inOrder) *inOrder = false;
      return true;
   }
   return false;
}

RET Joint::getAngles(array<ftype, 3> &_angles){
   _angles = angles;
   return R::Success;
}

RET Joint::getConstForce(array<ftype, 6> &_constForce){
   _constForce = constForce;
   return R::Success;
}

RET Joint::getConstIndex(int &_eIdx, int &_iIdx){
   _eIdx = currEIdx;
   _iIdx = currIIdx;
   return R::Success;
}

RET Joint::initStiffness(SP<JointStiffness> &_jStiff){
   bInitStiffness = true;
   // Moved out of here.
   // char c='l';
   // string fname = string(DATA_DIR) + name+".in";
   // ifstream fin(fname);
   // if(fin.good()){
   //    log_reg("exponential stiffness from %s", fname.c_str());
   //    fin>>c;
   //    if(c=='e'){
   //       log_reg("input 6 params for %s", getCName());
   //       ftype a, b, c, d, e, f;
   //       fin>>a>>b>>c>>d>>e>>f;
   //       shape.params = std::make_tuple(a, b, c, d, e, f);
   //    } else{
   //       log_err("malformed joint stiffness parameter files.");
   //    }
   // }
   // shape.type = c;
   // log_reg("Shape type for %s: %c", getCName(), c);
   mJStiff.reset(new JointStiffness(ra, rb));
   mJStiff->setInfo(1200, getName() + "-stiffness");
   _jStiff = mJStiff;
   return R::Success;
}

RET Joint::initDamping(){
   bInitDamping = true;
   return R::Success;
}

RET Joint::accumulate(ftype Dt){
   for(int i=0; i<6; i++){
      constForce[i] = constImpSum[i]/Dt;// + (constStabImpSum[i]/Dt)/Dt;
   }
   constImpSum = {};
   constStabImpSum = {};
   
   return R::Success;
}

RET Joint::getDebugOutput(std::string &dOut){
   auto strName = getName();
   auto cName   = getCName();
   dOut  = strName + "-torque: " + MS_T(mJStiff->getTorque()) + "\n";
   dOut += strName + "-angles: " + to_string(angles[Z])      + "\n";
   return R::Success;
}


bool Joint::needsUpper(JointLimitType lt){
   return lt==JointLimitType::UPPER || lt==JointLimitType::RANGE;
}
bool Joint::needsLower(JointLimitType lt){
   return lt==JointLimitType::LOWER || lt==JointLimitType::RANGE;
}
