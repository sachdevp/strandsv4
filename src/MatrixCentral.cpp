/**
   @author: sachdevp

   @desc: Defines everything about handling the matrices. This function is
   completely independent of any scene description. It works only with the
   matrices without any context.
*/

#include <MatrixCentral.hpp>
#include <StopWatch/StopWatch.hpp>
#include <SceneConstants.hpp>
#include <algorithm>
#include <iterator>
#include <mosek.h>

using namespace Eigen;
using namespace std;
#define DEBUG_CONSTRAINTS_MATRIX

MatrixCentral::MatrixCentral(){
   fDamping    = 0.0;
   bDampingSet = false;
   solver      = Solver::LINEAR;
   bLogMat     = false;
   fpLog       = NULL;
   mapIneqKey[IneqKey::LB] = MSKboundkeye::MSK_BK_LO;
   mapIneqKey[IneqKey::UB] = MSKboundkeye::MSK_BK_UP;
   mapIneqKey[IneqKey::RA] = MSKboundkeye::MSK_BK_RA;
   mapIneqKey[IneqKey::FX] = MSKboundkeye::MSK_BK_FX;
   mapIneqKey[IneqKey::FR] = MSKboundkeye::MSK_BK_FR;
}

RET MatrixCentral::setDamping(ftype _damping){
   if(bDampingSet) log_warn("Setting damping again.");
   fDamping    = _damping;
   bDampingSet = true;
   return R::Success;
}

RET MatrixCentral::setStab(ftype _stab){
   if(bStabSet) log_warn("Setting stabilization again.");
   fStab    = _stab;
   bStabSet = true;
   return R::Success;
}

RET MatrixCentral::printPreSolve(FILE* file, VectorXt rhs){
   ftype threshold   = 1e-10;
   MatrixXt JR_clip  = MAT_CLIP(JR);
   MatrixXt JRL_clip = MAT_CLIP(JRL);
   MatrixXt J_clip   = MAT_CLIP(J);
   MatrixXt M_r_clip = MAT_CLIP(M_r);
   MatrixXt cC_if, cC_i;
   VectorXt cc_iu, cc_il;
   cC_if = C_if.topRows(cnC_if);
   cC_i  = C_i.topRows(getnCi());
   cc_iu = c_iu.head(getnCi());
   cc_il = c_il.head(getnCi());
   fprintf(file,
           HS("M")      "\n" HS("K")    "\n" HS("M_r")  "\n"
           HS("JR")     "\n" HS("JRL")  "\n" HS("J")    "\n"
           HS("C")      "\n" HS("C_f")  "\n" HS("C_r")  "\n"
           HS("C_i")    "\n" HS("C_if") "\n"
           HS("v_prev") "\n" HS("f")    "\n" HS("Jf")   "\n" HS("rhs") "\n"
           HS("c")      "\n" HS("c_iu") "\n" HS("c_il") "\n",
           MS(M)      , MS(K)       , MS(M_r_clip),
           MS(JR_clip), MS(JRL_clip), MS(J_clip)  ,
           MS(C)      , MS(C_f)     , MS(C_r)     ,
           MS(cC_i)   , MS(cC_if)   , 
           MS(v_prev) , MS(f)       , MS(J.transpose() *f)   , MS(rhs) ,
           MS(c)      , MS(cc_iu)   , MS(cc_il));
   fflush(file);

   return R::Success;
}

RET MatrixCentral::preSolve(VectorXt rhs){
   if(bLogMat) printPreSolve(fpLog, rhs);
   RCHECKERROR_B(IS_MATRIX_NULL(rhs), "RHS is nan: \n %s", MS(rhs));

   return R::Success;
}

RET MatrixCentral::printPostSolve(FILE* file){
   fprintf(file, HS("v_r") "\n" HS("lambda") "\n", MS(v_r), MS(lambda));
   return R::Success;
}

RET MatrixCentral::postSolve(VectorXt rhs){
   // char tmpnambuf[L_tmpnam];
   // std::string filename = "/dbg.log";
   // std::string basedir = TMPDIR;
   // if(lambda.lpNorm<Infinity>() > 1e6 || v_r.lpNorm<Infinity>() > 1e6){
   //    // tmpnam(tmpnambuf);
   //    FILE* fp = nullptr;
   //    fp = fopen((basedir+filename).c_str(), "w");
   //    RCHECKERROR_NOTNULL(fp,"Could not create temporary file.");
   //    printPreSolve(fp, rhs);
   //    printPostSolve(fp);
   //    fclose(fp);

   //    return R::Failure;
   // }
   return R::Success;
}

RET MatrixCentral::setMatrixSizes
(int &_nF, int &_nRL, int &_nR, int &_nC_r, int &_nC_f, int &_nC_ir, int &_nC_if, int &_nJDoF){
   this->nF     = _nF;
   this->nRL    = _nRL;
   this->nR     = _nR;
   this->nC_r   = _nC_r;
   this->nC_f   = _nC_f;
   this->nC_if  = _nC_if;
   this->nC_ir  = _nC_ir;
   this->cnC_ir = 0;
   this->cnC_if = 0;
   this->nJDoF  = _nJDoF;

   M.setZero     (nF, nF);
   K.setZero     (nF, nF);
   JRL.setZero   (nF, nRL);
   JR.setZero    (nRL, nR);
   J.setZero     (nF, nR);
   f.setZero     (nF);
   v_prev.setZero(nF);
   v.setZero     (nF);

   /** Constraints **/
   // Equality
   C_r.setZero(nC_r, nR);
   c_r.setZero(nC_r);
   C_f.setZero(nC_f, nF);
   c_f.setZero(nC_f);
   C.setZero  (nC_r + nC_f, nR);
   c.setZero  (nC_r + nC_f);

   C_ir.setZero (nC_ir, nR);
   c_iur.setZero(nC_ir);
   c_ilr.setZero(nC_ir);
   c_ikr.resize (nC_ir);
   C_if.setZero (nC_if, nF);
   c_iuf.setZero(nC_if);
   c_ilf.setZero(nC_if);
   c_ikf.resize (nC_if);
   C_i.setZero  (nC_ir + nC_if, nR);
   c_il.setZero (nC_ir + nC_if);
   c_iu.setZero (nC_ir + nC_if);
   c_ik.resize  (nC_ir + nC_if);

#ifndef EXPLICIT_JOINT_DAMPING
   mAdjMat.setZero(nJDoF, nF);
   mJDampMat.setZero(nJDoF, nJDoF);
#endif

   // Reduced
   M_r.setZero      (nR, nR);
   v_r.setZero      (nR);
   x_r.setZero      (nR);
   dx_r_stab.setZero(nR);

   int r = nR + nC_r + nC_f;
   KKTM.setZero(r, r);
   KKTrhs.setZero(r);

   lambda_len = 0;

   return RET::Success;
}

RET MatrixCentral::getREConstRow(int &consRow, int redEqIdx){
   // Reduced equality constraints on top of the list.
   // Refer to @MatrixCentral::combineConstraints
   consRow = redEqIdx;
   RCHECKERROR_B(
      redEqIdx>nC_r,
      "Only %d reduced eq constraints available, but trying to find row for %d",
      nC_r, redEqIdx);
   return R::Success;
}

RET MatrixCentral::getRIConstRow(int &consRow, int redIneqIdx){
   // Reduced equality, full equality, then reduced inequality
   consRow = nC_r + nC_f + redIneqIdx;
   RCHECKERROR_B(
      redIneqIdx>cnC_ir,
      "Only %d reduced ineq constraints available, but trying to find row for %d",
      cnC_ir, redIneqIdx);
   return R::Success;
   
}

RET MatrixCentral::getFIConstRow(int &consRow, int ineqIdx){
   // Reduced equality, full equality, reduced inequality, then full inequality
   consRow = nC_r + nC_f + cnC_ir + ineqIdx;
   RCHECKERROR_B(
      ineqIdx > cnC_if,
      "Only %d full ineq constraints available, but trying to find row for %d",
      cnC_if, ineqIdx);
   return R::Success;
}
RET MatrixCentral::combineConstraints(int _cnC_ir, int _cnC_if){
   // Copy any reduced constraints first
   cnC_ir = _cnC_ir;
   cnC_if = _cnC_if;
   if(nC_r > 0) {
      C.topLeftCorner(nC_r, nR) = C_r;
      c.head(nC_r)              = c_r;
   }

   // Combine any full constraints after reduction with Jacobian
   if(nC_f > 0) {
      C.block(nC_r, 0, nC_f, nR) = C_f*J;
      c.segment(nC_r, nC_f)      = c_f;
   }

   if(cnC_ir > 0) {
      C_i.topLeftCorner(cnC_ir, nR) = C_ir.topLeftCorner(cnC_ir, nR);
      c_iu.head(cnC_ir)             = c_iur.head(cnC_ir);
      c_il.head(cnC_ir)             = c_ilr.head(cnC_ir);
      std::copy_n(c_ikr.begin(), cnC_ir, &c_ik[0]);
   }

   // Combine any full constraints after reduction with Jacobian
   if(cnC_if > 0) {
      C_i.block(cnC_ir, 0, cnC_if, nR) = C_if.topRows(cnC_if)*J;
      c_iu.segment(cnC_ir, cnC_if)     = c_iuf.head(cnC_if);
      c_il.segment(cnC_ir, cnC_if)     = c_ilf.head(cnC_if);
      std::copy_n(c_ikf.begin(), cnC_if, &c_ik[cnC_ir]);
   }
   ftype threshold = 1e-13;
   lambda_len = nC_r+nC_f+cnC_ir+cnC_if;
   lambda.setZero(lambda_len);
   lambda_stab.setZero(lambda_len);

   bConstraintsSet = true;
   return R::Success;
}

RET MatrixCentral::setFilename(std::string _filename){
   sLogFile = _filename;
   fpLog    = fopen(sLogFile.c_str(), "a");
   if(fpLog == NULL){
      perror("Failed to set filename: ");
      return R::Failure;
   }
   return R::Success;
}

RET MatrixCentral::linearSolve(VectorXt rhs){
   int nC = getnC();

   MatrixXd M_rd, rhsd, Cd, cd, zeroC;
   zeroC = MatrixXd::Zero(nC, nC);
#ifdef PROFILING
   auto KKT_sw = StopWatch::find("|Step|Dynamics|KKT");
   KKT_sw->tic();
#endif
   M_rd = M_r.cast<double>();
   rhsd = rhs.cast<double>();
   if(C.rows()!=0){
      Cd = C.cast<double>();
      cd = c.cast<double>();
      KKTM << M_rd, Cd.transpose() ,
         Cd, zeroC;
      KKTrhs << rhsd, cd;
   } else{
      KKTM   = M_rd;
      KKTrhs = rhsd;
   }
   // If the matrix is empty, then do not solve
   if(KKTM.rows() == 0 || KKTM.cols() == 0) return RET::Success;

#ifdef PROFILING
   KKT_sw->toc();
#endif

#ifdef PROFILING
   auto solve_sw = StopWatch::find("|Step|Dynamics|Solve");
   solve_sw->tic();
#endif
   VectorXd KKTv = KKTM.fullPivHouseholderQr().solve(KKTrhs);
   if(bLogMat) fprintf(fpLog, HS("KKTv") "\n", MS(KKTv));
   v_r = KKTv.head(nR).cast<ftype>();
   RCHECKERROR_B(v_r.lpNorm<Infinity>()>SOL_THRESH,"KKTv larger than %f.", SOL_THRESH);
   vec_error = KKTM*KKTv - KKTrhs;
   abs_error = (ftype)vec_error.norm() ;
   rel_err   = abs_error/(ftype)KKTrhs.norm() ;
#ifdef PROFILING
   solve_sw->toc();
#endif
   return R::Success;
}

// Taken from qo1.c example
static void MSKAPI printstr(void *handle, const char str[]){printf("%s", str);} /* printstr */

// qo1.c used as template
#define SP_REF 1.0
#define SP_EPS 1.0e-10

inline double ineqLo(ftype f, IneqKey ineqKey){
   return ineqKey==IneqKey::LB? f : -MSK_INFINITY;
}
inline double ineqUp(ftype f, IneqKey ineqKey){
   return ineqKey==IneqKey::UB? f : +MSK_INFINITY;
}

#define MSKCHECK MSK_CHECK_STATUS_AND_RETURN_IT

RET MatrixCentral::QPMosekSolve(VectorXt rhs){
   MSKenv_t env   = nullptr;
   MSKtask_t task = nullptr;
   MSKCHECK(MSK_makeenv(&env, NULL));
   int nVar   = (int)M_r.rows();
   int nECons = getnC();
   int nICons = getnCi();
   int nCons  = nECons + nICons;
   RCHECKERROR_B(nCons>lambda.rows(), "Too many constraints!");
   VectorXd m_rhsd = -1.0f*rhs.cast<double>();
   double xx[nVar];
   auto retVal = R::Success;
   CHECK_MOSEK_STATUS(MSK_maketask(env, nCons, nVar, &task)){
      // MSKCHECK(MSK_linkfunctotaskstream(task, MSK_STREAM_LOG, NULL, printstr));
      MSKCHECK(MSK_appendcons(task, nCons));
      MSKCHECK(MSK_appendvars(task, nVar));
      // TODO Set the constant term
      MSKCHECK(MSK_putcfix(task, 0.0));

      // Objective
      for(uint iVar=0; iVar<nVar; iVar++){
         //MSKCHECK(MSK_putvarbound(task, j, bkx, blx, bux));
         MSKCHECK(MSK_putcj(task, iVar, m_rhsd(iVar)));
         // Simulation does not work without setting bound keys for variables
         MSK_putvarbound(task, iVar, MSK_BK_FR, -MSK_INFINITY, +MSK_INFINITY);
         for (uint jVar=0; jVar<=iVar; jVar++){
            MSKCHECK(MSK_putqobjij(task, iVar, jVar, M_r(iVar, jVar)));
         }
      }
      // MSKCHECK(MSK_putqobj(task, NUMQNZ, qsubi, qsubj, qval));

      // Equality constraints
      for(uint iECons=0; iECons<nECons; iECons++){
         for (uint jVar=0; jVar<nVar; jVar++){
            MSKCHECK(MSK_putaij(task, iECons, jVar, C(iECons, jVar)));
         }
         MSKCHECK(MSK_putconbound(task, iECons, MSK_BK_FX, c(iECons), c(iECons)));
      }
      // Inequality constraints
      for (uint iICons=0, iCons=nECons; iICons<nICons; iICons++, iCons++){
         IneqKey ineqKey = c_ik[iICons];
         for (uint jVar=0; jVar<nVar; jVar++){
            MSKCHECK(MSK_putaij(task, iCons, jVar, C_i(iICons, jVar)));
         }
         MSKCHECK(MSK_putconbound(task, iCons, mapIneqKey[ineqKey],
                                  ineqLo(c_il(iICons), ineqKey),
                                  ineqUp(c_iu(iICons), ineqKey)));
      }
      MSKrescodee trmcode, r;

      /** Testing objective **/
      /*
        int nq, *n_extra, *qi, *qj, *nqonz;
        double * qval;
        nq = 20;
        n_extra = new int;
        n_extra[0] = 20;
        qi = new int[nq];
        qj = new int[nq];
        nqonz = new int;
        qval = new double[nq];
        r = MSK_getqobj(task, nq, n_extra, nqonz, qi, qj, qval);

        printf("<Printing Q>\n");
        for(int i=0;i<*nqonz;i++){
        printf("(%d, %d): %4.4f\n",qi[i],qj[i],qval[i]);
        }
        printf("</Printing Q>\n\n");

        printf("Getting objective. Return Value: %d\n",r);
      */

      /* Run optimizer */

      // r = MSK_analyzeproblem(task, MSK_STREAM_MSG);
      r = MSK_optimizetrm(task, &trmcode);
      /* Print a summary containing information
         about the solution for debugging purposes*/
      // MSK_solutionsummary (task, MSK_STREAM_MSG);

      if(r == MSK_RES_OK){
         log_verbose("Optimization OK.");
         MSKsolstae solsta;
         MSKprostae prosta;
         uint j;

         MSK_getsolsta(task, MSK_SOL_ITR, &solsta);

         MSKstakeye skc[nCons], skx[nCons], skn[nCons];
         MSKrealt    xc[nCons],   y[nCons];
         MSKrealt   slc[nCons], suc[nCons], slx[nVar], sux[nVar], snx[nVar];

         switch(solsta){
         case MSK_SOL_STA_OPTIMAL:
         case MSK_SOL_STA_NEAR_OPTIMAL:
            /* Request the interior solution. */
            MSK_getxx(task, MSK_SOL_ITR, xx);

            for (j = 0; j < nVar; ++j){v_r(j) = (ftype)xx[j];}

            MSK_getsolution(task, MSK_SOL_ITR, &prosta, &solsta,
                            skc, skx, skn,
                            xc, xx, y,
                            slc, suc, slx, sux, snx);

            for (j = 0; j < nVar; ++j){v_r(j) = (ftype)xx[j];}
            for (j = 0; j < nCons; ++j){
               lambda[j] = slc[j] - suc[j];
               // printf("skc, lambda[%d]:%d %3.2e %3.2e %3.2e\n", j, skc[j], slc[j], suc[j], lambda[j]);
            }

            log_verbose("Near optimal.");
            break;

         case MSK_SOL_STA_DUAL_INFEAS_CER:
         case MSK_SOL_STA_PRIM_INFEAS_CER:
         case MSK_SOL_STA_NEAR_DUAL_INFEAS_CER:
         case MSK_SOL_STA_NEAR_PRIM_INFEAS_CER:
            log_warn("Infeasible.");
            printf("Primal or dual infeasibility certificate found.\n");
            retVal = R::Failure;
            break;

         case MSK_SOL_STA_UNKNOWN:
            log_warn("Unknown.");
            printf("The status of the solution could not be determined.\n");
            retVal = R::Failure;
            break;

         default:
            log_warn("Default - Other.");
            printf("Other solution status.\n");
            retVal = R::Failure;
            break;
         }
      } else{
         log_warn("Error while opt.");
         printf("Error while optimizing.\n");
      }


      if(r != MSK_RES_OK){
         /* In case of an error print error code and description. */
         char symname[MSK_MAX_STR_LEN];
         char desc[MSK_MAX_STR_LEN];
         log_err("Could not call optimization.");
         MSK_getcodedesc (r, symname, desc);
         printf("Error %s - '%s'\n", symname, desc);
         MSK_deletetask(&task);
         MSK_deleteenv(&env);
         return R::Failure;
      }
   } else{
      MSK_deletetask(&task);
      MSK_deleteenv(&env);
      return R::Failure;
   }

   MSK_deletetask(&task);
   MSK_deleteenv(&env);

   return retVal;
}

RET MatrixCentral::dynamics(){
#ifdef PROFILING
   auto fn_sw = StopWatch::find("|Step|Dynamics");
   fn_sw->tic();
#endif
   if(bLogMat && !fpLog) log_warn("Logging, but no file found to log to. Disabling logging.");
   v_prev = v;
   if(!bTimeStepSet){
      ftype dt = 1e-2f;
      log_reg("Timestep not provided. Using default value of %.3e", dt);
      setTimestep(DEFAULT_TIMESTEP);
   }
   if(!bDampingSet){
      log_reg("Assuming no damping.");
      setDamping(0.0);
   }
   // NOTE Jacobian is set in Scene class

#ifdef PROFILING
   auto Mr_sw = StopWatch::find("|Step|Dynamics|ReducedMass");
   Mr_sw->tic();
#endif
#ifndef EXPLICIT_JOINT_DAMPING
   MatrixXt A = mAdjMat;
   MatrixXt D = mJDampMat;
   MatrixXt jointEffDampMat = A.transpose() * D * A;
#endif
   
   M_r = J.transpose() * ( M + h*fDamping*MatrixXt::Identity(M.rows(), M.cols())
#ifndef EXPLICIT_JOINT_DAMPING 
                + h*jointEffDampMat
#endif
#ifdef STIFFNESS
                - h * h * K
#endif
                ) * J;
   VectorXt rhs = J.transpose() * (M*v_prev + h*f);

#ifdef PROFILING
   Mr_sw->toc();
#endif
   RET r;
   r = preSolve(rhs);
   if(RFAIL(r)){
      time_t t = time(0);   // get time now
      struct tm * now = localtime( & t );
      char buffer [120];
      strftime (buffer,120,"/home/sachdevp/tmp/Strands/%Y-%m-%d.log",now);
      log_err("Could not presolve.");
      FILE* file = fopen(buffer, "w");
      printPreSolve(file, rhs);
      // printPostSolve(file);
      return R::Failure;
   }
   outRHS = rhs;
   // RCHECKERROR(r, "Could not clear presolve.");
   switch(solver){
   case Solver::LINEAR:
      r = linearSolve(rhs);
      break;
   case Solver::QPMOSEK:
      r = QPMosekSolve(rhs);
      break;
   default:
      log_err("Solver not specified.");
   }
   if(RFAIL(r)){
      time_t t = time(0);   // get time now
      struct tm * now = localtime( & t );
      char buffer [120];
      strftime (buffer,120,"/home/sachdevp/tmp/Strands/%Y-%m-%d.log",now);
      log_err("Could not solve.");
      FILE* file = fopen(buffer, "w");
      printPreSolve(file, rhs);
      printPostSolve(file);
      return R::Failure;
   }

   v = J * v_r;
   if(bLogMat) printPostSolve(fpLog);
   RCHECKERROR(postSolve(rhs), "Could not clear postsolve.");

#ifdef DEBUG_CONSTRAINTS_MATRIX
   if(bLogMat) fprintf(fpLog, HS("const_error"), MS(C*v_r-c));
   const_error = (C*v_r-c).norm() ;
   log_verbose("Norm of constraint violation: %f", const_error);
#endif

   // Integrate reduced position
   dx_r = h*v_r;
   x_r = x_r + dx_r;

   // TODO Re-instate this.
   // if(rhs.norm()>EPSILON){
   //    relative_error = (M_r*v_r - rhs).norm() / rhs.norm() ; // norm() is L2 norm
   //    log_verbose("The relative error is: %f\n", relative_error);
   //    assert(relative_error < 1e-2);
   // } else{
   //    log_verbose("RHS norm is small: %f\n", rhs.norm() );
   // }

   // Compute full velocity.
   PRINTMAT(v_r);
   if(bLogMat) fprintf(fpLog, HS("v") "\n", MS(v));
#ifdef PROFILING
   fn_sw->toc();
#endif
   return R::Success;
}

RET MatrixCentral::clear(){
   M.setZero();
   M_r.setZero();
   K.setZero();
   J.setZero();
   JR.setZero();
   // JRL does not need to be set zero.
   f.setZero();

   C.setZero();
   c.setZero();

   mAdjMat.setZero();
   mJDampMat.setZero();

   C_f.setZero();
   c_f.setZero();
   C_r.setZero();
   c_r.setZero();

   C_if.setZero();
   C_ir.setZero();
   c_iuf.setZero();
   c_iur.setZero();
   c_ilf.setZero();
   c_ilr.setZero();

   dx_r_stab.setZero();
   return R::Success;
}

RET MatrixCentral::stabilize(VectorXt c_drift){
   // return R::Success;
   int nC = getnC();
   if(nC==0 && cnC_if==0 && cnC_ir==0){
      return R::Success;
   }
   VectorXd zero_v, rhs;
   log_random("New reduced x: \n %s", MS_T(x_r));
   MatrixXd M_rd = M_r     .cast<double>();
   VectorXd rhsd = rhs     .cast<double>();
   MatrixXd Cd   = C       .cast<double>();
   MatrixXd C_id = C_i     .cast<double>();
   VectorXd cd   = c_drift .cast<double>();
   MatrixXd C_com;
   int nC_total = C.rows() + cnC_ir + cnC_if;
   C_com.setZero(nC_total, nR);
   C_com.topRows(C.rows()) = Cd;
   C_com.bottomRows(cnC_ir+cnC_if) = C_id.topRows(cnC_ir+cnC_if);
   MatrixXd zero = MatrixXd::Zero(nC_total, nC_total);
   MatrixXd KKTMat;
   KKTMat.setZero(nC_total+nR, nC_total+nR);
   KKTMat << M_rd, C_com.transpose() ,
      C_com, zero;

   rhs.resize(nR+nC_total);
   zero_v = VectorXd::Zero(nR);
   rhs << zero_v, -cd;
   VectorXt stab_lhs = KKTMat.fullPivLu().solve(rhs).cast<ftype>();
   dx_r_stab = stab_lhs.head(nR);
   lambda_stab = stab_lhs.tail(lambda_len);
   // Add the stabilization term
   x_r = x_r + dx_r_stab*fStab;
   if(bLogMat){
      fprintf(fpLog, // MATRICES
              HS("stabMat")     "\n"
              HS("stab_rhs")    "\n"
              HS("stab_dx")     "\n"
              HS("stab_lambda") "\n"
              HS("stab_x_r")    "\n",
              MS(KKTMat),
              MS(rhs) ,
              MS(dx_r_stab)  ,
              MS(lambda_stab),
              MS(x_r));
   }
   return R::Success;
}

RET MatrixCentral::setTimestep(ftype _h){
   h = _h;
   log_reg("Timestep set to %f", h);
   bTimeStepSet = true;
   return R::Success;
}

RET MatrixCentral::closeLog(){
   RCHECKERROR_B(!bLogMat && fpLog!=NULL, "fpLog not null even though not logging.");
   RCHECKERROR_B(!bLogMat               , "Trying to close log when not logging matrices.");

   int res = fclose(fpLog);
   if(res!=0) perror("Could not close file: ");
   fpLog = NULL;
   return R::Success;
}

RET MatrixCentral::setInitialState(Eigen::VectorXt _x_r, Eigen::VectorXt _v_r){
   RCHECKERROR_B(x_r.size() != _x_r.size(), "State vector lengths do not match.");
   RCHECKERROR_B(v_r.size() != _v_r.size(), "Velocity vector lengths do not match.");
   x_r = _x_r;
   v_r = _v_r;
   v = J*v_r;
   dx_r = _x_r;
   return R::Success;
}
