#pragma once

#include <list>
#include <map>
#include <stack>
#include <string>
#include <vector>
#include <cmath>
#include <chrono>
/* #include "PrecisionTimer.h" */

class StopWatch {
   using stringc = const std::string;
 public:
   void tic();
   void toc();
   void reset();
   std::string getString();

 private:
   StopWatch();
   StopWatch(stringc& fullname);
   ~StopWatch();
   void clear();
   void addChild(StopWatch* child);
   void buildString(std::list<std::string>& strs);
   void buildList();
   void rebuildList();

 public:
   std::string fullname; // e.g. "|step|fill|strand"
   std::string name;     // e.g. "strand"
   int count;
   double average;
   int depth;
   std::chrono::duration<double> total;
   std::string comment;

 private:
   bool running;
   std::chrono::time_point<std::chrono::high_resolution_clock> start;
   /* CPrecisionTimer timer; */

   StopWatch* parent;
   std::list<StopWatch*> children;

   // Static methods
 public:
   static StopWatch* find(stringc& fullname);
   static StopWatch* tic(stringc& fullname);
   static StopWatch* toc(stringc& fullname);
   static StopWatch* toc(stringc& fullname, stringc& comment);
   static StopWatch* toctic(stringc& fullname);
   static std::list<StopWatch*> getAll();
   static void resetAll();
   static void destroyAll();

   static std::map<std::string,StopWatch*> swMap;
   static std::list<StopWatch*> swList;
   static std::stack<StopWatch*> swStack;
   static std::vector<int> nameLenMaxPerDepth;
   static int countCols;
   static int totalCols;
   static int averageCols;
};
