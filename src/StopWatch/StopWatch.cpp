#include "StopWatch.hpp"
#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

map   <string,StopWatch*> StopWatch::swMap;
list  <StopWatch*>        StopWatch::swList;
stack <StopWatch*>        StopWatch::swStack;
vector<int>               StopWatch::nameLenMaxPerDepth;

int StopWatch::countCols   = 6;
int StopWatch::totalCols   = 6;
int StopWatch::averageCols = 5;

StopWatch::StopWatch(){clear();}

StopWatch::StopWatch(const string& fullname){
   clear();
   this->fullname = fullname;
   // Get name from fullname
   int index = fullname.find_last_of("|");
   name = fullname.substr(index+1);
   // Add to global map
   StopWatch::swMap[fullname] = this;
   if(name.compare("") == 0){
      depth = 0;
      StopWatch::nameLenMaxPerDepth.resize(1);
   } else{
      // Find parent
      string parentFullname = fullname.substr(0, index);
      // map<string,StopWatch*>::iterator
      auto pit = StopWatch::swMap.find(parentFullname);
      StopWatch* parent = nullptr;
      // Try to find the parent, or create a new one.
      if(pit != swMap.end()) {parent = pit->second;}
      else {parent = new StopWatch(parentFullname);}
      parent->addChild(this);
   }
   StopWatch::rebuildList();
   // Update max name length per level
   StopWatch::nameLenMaxPerDepth[depth] =
      fmax<int>(name.length(), StopWatch::nameLenMaxPerDepth[depth]);
}

StopWatch::~StopWatch(){}

void StopWatch::clear(){
   fullname = "";
   name = "";
   comment = "";
   parent = NULL;
   children.clear();
   reset();
}

void StopWatch::buildList(){
   StopWatch::swList.push_back(this);
   for(auto it = children.begin(); it != children.end(); ++it){
      (*it)->buildList();
   }
}

void StopWatch::tic(){
   if (!running) {start = HRC::now();}
   StopWatch::swStack.push(this);
   running = true;
}

void StopWatch::toc(){
   if(running){
      total += (HRC::now() - start)*1e3; // in milli seconds
      count++;
      average = total.count()/(double)count;
   }
   StopWatch::swStack.pop();
   running = false;
}

void StopWatch::reset(){
   count = 0;
   total = total.zero();
   average = 0.0;
   running = false;
}

void StopWatch::addChild(StopWatch* child){
   child->parent = this;
   this->children.push_back(child);
   child->depth = this->depth + 1;
   if(child->depth >= (int)StopWatch::nameLenMaxPerDepth.size()){
      StopWatch::nameLenMaxPerDepth.resize(child->depth + 1, 0);
   }
}

string StopWatch::getString(){
   list<string> strs;
   buildString(strs);
   // Reverse the order to go from root
   string str = "|";
   for(auto rit = strs.rbegin(); rit != strs.rend(); ++rit){
      str.append(*rit).append("|");
   }
   // Add extra spaces to the end of the string
   for(int i = depth + 1; i < (int)StopWatch::nameLenMaxPerDepth.size(); ++i){
      for(int j = 0; j < (int)StopWatch::nameLenMaxPerDepth[i]; ++j){
         str.append("-");
      }
      str.append("|");
   }
   str.append(" ");
   // Add stats
   ostringstream ss;
   ss << setiosflags(ios::right);
   ss << setiosflags(ios::fixed);
   if(fullname.compare("") == 0){
      ss << setw(StopWatch::countCols)   << setprecision(0) << "count" << "  "
         << setw(StopWatch::totalCols)   << setprecision(2) << "total" << "  "
         << setw(StopWatch::averageCols) << setprecision(2) << "avg";
      ss << " | (time in ms)";
   } else{
      ss << setw(StopWatch::countCols)   << setprecision(0) << count << "  "
         << setw(StopWatch::totalCols)   << setprecision(2) << total.count() << "  "
         << setw(StopWatch::averageCols) << setprecision(2) << average;
      ss << " | " << comment;
   }
   str.append(ss.str());
   return str;
}

void StopWatch::buildString(list<string>& strs){
   if(parent){
      string str = name;
      // Pad extra space
      int nameLen = (int)name.length();
      for(int i = 0; i < StopWatch::nameLenMaxPerDepth[depth] - nameLen; ++i){
         name.append("-");
      }
      strs.push_back(name);
      parent->buildString(strs);
   }
}

////////////// Static methods

void StopWatch::rebuildList(){
   StopWatch::swList.clear();
   StopWatch* root = swMap[""];
   root->buildList();
}

StopWatch* StopWatch::find(const string& fullname){
   auto it = StopWatch::swMap.find(fullname);
   // Create a new StopWatch from fullname
   if(it == swMap.end()) return new StopWatch(fullname);
   return it->second;
}

StopWatch* StopWatch::tic(const string& fullname){
#ifdef SW
   StopWatch* sw = NULL;
   if(fullname.compare("") != 0){
      sw = StopWatch::find(fullname);
   } else{
      sw = swStack.empty() ? NULL : StopWatch::swStack.top();
   }
   if(sw){sw->tic();}
   return sw;
#endif
   return NULL;
}

StopWatch* StopWatch::toc(const string& fullname){
#ifdef SW
   StopWatch* sw = NULL;
   if(fullname.compare("") != 0){
      sw = StopWatch::find(fullname);
   } else{
      sw = swStack.empty() ? NULL : StopWatch::swStack.top();
   }
   if(sw){
      sw->toc();
   }
   return sw;
#endif
   return NULL;
}

StopWatch* StopWatch::toc(const string& fullname, const string& comment){
#ifdef SW
   StopWatch* sw = StopWatch::toc(fullname);
   sw->comment = comment;
   return sw;
#endif
   return NULL;
}

StopWatch* StopWatch::toctic(const string& fullname){
#ifdef SW
   // Stop last started StopWatch
   StopWatch::toc("");
   // Start new StopWatch
   return StopWatch::tic(fullname);
#endif
   return NULL;
}

list<StopWatch*> StopWatch::getAll(){
   list<StopWatch*> nonempty;
   for(auto iter = swList.begin(); iter != swList.end(); ++iter){
      StopWatch* sw = *iter;
      if(sw->fullname.compare("") == 0 || sw->count > 0){nonempty.push_back(sw);}
      // Update the column counts
      int cols = sw->count == 0 ? 0 : (int)ceil(log10((double)sw->count));
      StopWatch::countCols = std::fmax(StopWatch::countCols, cols);
      cols = sw->count == 0 ? 0 : (int)ceil(log10(sw->total.count())) + 3;
      StopWatch::totalCols = std::fmax(StopWatch::totalCols, cols);
      cols = sw->count == 0 ? 0 : (int)ceil(log10(sw->average)) + 3;
      StopWatch::averageCols = std::fmax(StopWatch::averageCols, cols);
   }
   return nonempty;
}

void StopWatch::resetAll(){
   for(auto it = swList.begin(); it != swList.end(); ++it){
      (*it)->reset();
   }
   StopWatch::countCols = 6;
   StopWatch::totalCols = 6;
   StopWatch::averageCols = 5;
}

void StopWatch::destroyAll(){
   for(auto it = swList.begin(); it != swList.end(); ++it){
      delete *it;
   }
   swList.clear();
   swMap.clear();
   while(!swStack.empty()){
      swStack.pop();
   }
}

void StopWatch::printAll(){
   for(auto it = swMap.begin(); it != swMap.end(); ++it){
      cout<<it->second->getString()<<endl;
   }
}
