#pragma once

#include <list>
#include <map>
#include <stack>
#include <string>
#include <vector>
#include <cmath>
#include <chrono>

namespace SC = std::chrono;
#define HRC std::chrono::high_resolution_clock
#define DUR std::chrono::duration
#define TIME std::chrono::time_point

class StopWatch {
   using stringc = const std::string;
 public:
   void tic();
   void toc();
   void reset();
   std::string getString();
   StopWatch();
   StopWatch(stringc& fullname);
   ~StopWatch();
   void addChild(StopWatch* child);

 private:
   void clear();
   void buildString(std::list<std::string>& strs);
   void buildList();
   void rebuildList();

 public:
   std::string fullname; // e.g. "|step|fill|strand"
   std::string name;     // e.g. "strand"
   int count;
   double average;
   int depth;
   SC::duration<double> total;
   std::string comment;

 private:
   bool running;
   SC::time_point<HRC> start;
   /* CPrecisionTimer timer; */

   StopWatch* parent;
   std::list<StopWatch*> children;

   // Static methods
 public:
   static StopWatch* find(stringc& fullname);
   static StopWatch* tic(stringc& fullname);
   static StopWatch* toc(stringc& fullname);
   static StopWatch* toc(stringc& fullname, stringc& comment);
   static StopWatch* toctic(stringc& fullname);
   static std::list<StopWatch*> getAll();
   static void resetAll();
   static void destroyAll();
   static void printAll();

   static std::map<std::string,StopWatch*> swMap;
   static std::list<StopWatch*> swList;
   static std::stack<StopWatch*> swStack;
   static std::vector<int> nameLenMaxPerDepth;
   static int countCols;
   static int totalCols;
   static int averageCols;
};
