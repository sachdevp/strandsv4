classdef DoF < handle
% RIGID Defines a handle class for rigid to allow reference passing. Important to get
% pointer-like bhavior.
    properties
        nodeType   % Type of node
        strandID   % What strand does DoF belong to
        l          % Position
        n_r
        x_r
        rigidBody
        Gamma
        Omega
        circle
        dirtyJR
        node
    end
    methods
        function success = ComputeJacobian(dof)
        % COMPUTEJACOBIAN Computes Gamma and Omega
            if ~isequal(dof.rigidBody, [])
                R = dof.rigidBody.E(1:3, 1:3);
            else
                R = eye(3);
            end
            switch dof.nodeType
              case 'circle'             % 1-DoF
                % CHECK if this needs to be multiplied by R'
                c = dof.circle.radius*cos(dof.x_r);
                s = dof.circle.radius*sin(dof.x_r);
                dof.Omega = -(rigidlib.cross(R'*dof.circle.axis, R'*dof.circle.rx)*c ...
                             + rigidlib.cross(R'*dof.circle.axis, R'*dof.circle.ry)*s);
                dof.Gamma = R*rigidlib.gamma(dof.l);
              case 'full'               % 3-DoF
                assert(isequal(dof.rigidBody, []));
                dof.Omega = eye(3);
                dof.Gamma = [];
              case 'fixed'              % 0-DoF
                if ~isequal(dof.rigidBody, [])
                    dof.Omega = [];
                    dof.Gamma = R*rigidlib.gamma(dof.l);
                end
            end
            dof.dirtyJR = true;
            success = true;
        end
        function dfInd = getdfInd(dof)
        % GETDFIND Return the full index 
            dfInd = dof.node.LInd;
        end
        function sInd = getsInd(dof)
            sInd = dof.rigidBody.ind;
        end
        function success = UpdateDoF(dof, v_r, h)
            
            if(dof.n_r > 0)
                assert(length(v_r) == dof.n_r);
                dof.x_r = dof.x_r + h*v_r;
            end
            success = true;
        end
        function success = UpdatePosition(dof)
        % FIXME for other types of nodes
            tmpx = [dof.l; 1];
            
            if (~isequal(dof.Omega, []))
                if (isequal(dof.nodeType, 'circle'))
                    theta = dof.x_r;
                    c = dof.circle.rx*dof.circle.radius*cos(theta);
                    s = dof.circle.ry*dof.circle.radius*sin(theta);
                    tmpx(1:3) = tmpx(1:3) + c + s;
                end
            end
            tmpx = dof.rigidBody.E*tmpx;
            dof.node.x = tmpx(1:3);
            success = true;
        end
        function success = Draw(dof)
			xs = zeros(3,0);
            for theta = 0:0.01:2*pi
                if isequal(dof.nodeType,'circle')
                    c = dof.circle.rx*dof.circle.radius*cos(theta);
                    s = dof.circle.ry*dof.circle.radius*sin(theta);
                    x = [dof.circle.center + c + s; 1];
                    x = dof.rigidBody.E*x;
					xs(:,end+1) = x(1:3); %#ok<AGROW>
                end
            end
			plot3(xs(1,:),xs(2,:),xs(3,:),'k');
            success = true;
        end
    end
end