function success = CustomPrint(obj_name, obj, debug)
% CUSTOMPRINT Write a statement containing the file location and line number
% before printing the object.
%
% Makes it easier to trace outputs. Example usage:
% success = CustomPrint("Mass matrix", M)
    if (nargin == 2) | (debug == true)
        fprintf(1, 'Printing %s (%s: %d): \n', obj_name, MFileName(), ...
                MFileLineNr());
        disp(obj);
    end
    success = true;
end
    
function LineNr = MFileLineNr()
% MFILELINENR returns the current linenumber
    Stack  = dbstack;
    LineNr = Stack(3).line;   % the line number of the calling function
end

function FileName = MFileName()
% MFILELINENR returns the current linenumber
    Stack  = dbstack;
    FileName = Stack(3).file;   % the line number of the calling function
end