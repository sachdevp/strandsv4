function E = interpDq(E0, E1, t, flip)
% Interpolate using dual quaternion

if nargin < 4
    flip = false;
end

dq0 = rigidlib.matToDq(E0);
dq1 = rigidlib.matToDq(E1);
if flip
    dq1 = -dq1;
end
dq = (1-t)*dq0 + t*dq1;

dq = dq';
len = norm(dq(1,:));
dq(1,:) = dq(1,:) / len;
t(1) = 2.0*(-dq(2,1)*dq(1,2) + dq(2,2)*dq(1,1) - dq(2,3)*dq(1,4) + dq(2,4)*dq(1,3)) / len;
t(2) = 2.0*(-dq(2,1)*dq(1,3) + dq(2,2)*dq(1,4) + dq(2,3)*dq(1,1) - dq(2,4)*dq(1,2)) / len;
t(3) = 2.0*(-dq(2,1)*dq(1,4) - dq(2,2)*dq(1,3) + dq(2,3)*dq(1,2) + dq(2,4)*dq(1,1)) / len;

E(1:3,1:3) = rigidlib.quatToMat(dq(1,:));
E(1:3,4) = t;
E(4,:) = [0 0 0 1];

end
