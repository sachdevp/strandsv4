classdef Mesh < handle
    %Mesh Summary of this class goes here
    %   Detailed explanation goes here
    
    properties %(Access = protected)
        V
        F
        bb
    end
    
    methods
        %% Constructor
        function this = Mesh()
        end
        
        %%
        function setV(this, V)
            this.V = V;
            % Update bouding box
            this.bb = [inf, -inf, inf, -inf, inf, -inf];
            for j = 1 : size(this.V,2)
                v = this.V(:,j);
                this.bb(1) = min(v(1), this.bb(1));
                this.bb(2) = max(v(1), this.bb(2));
                this.bb(3) = min(v(2), this.bb(3));
                this.bb(4) = max(v(2), this.bb(4));
                this.bb(5) = min(v(3), this.bb(5));
                this.bb(6) = max(v(3), this.bb(6));
            end
        end
        
        %%
        function setF(this, F)
            this.F = F;
        end
        
        %% Displays this mesh on screen
        function draw(this, E, color)
            if isempty(this.V)
                return;
            end
            % Transform the mesh vertices by E
            V1 = E * [this.V; ones(1, size(this.V,2))];
            % Draw mesh
            h = trisurf(this.F', V1(1,:), V1(2,:), V1(3,:),...
                'LineStyle', 'none', 'FaceColor', color, 'BackFaceLighting', 'unlit');
            alpha(h, 0.5);
%             % debug
%             hold on;
%             for i = 1 : size(this.V,2)
%                 x = this.V(:,i);
%                 plot3(x(1), x(2), x(3), '.');
%             end
%             for i = 1 : size(this.F,2)
%                 i0 = this.F(1,i);
%                 i1 = this.F(2,i);
%                 i2 = this.F(3,i);
%                 x0 = this.V(:,i0);
%                 x1 = this.V(:,i1);
%                 x2 = this.V(:,i2);
%                 v1 = x1 - x0;
%                 v2 = x2 - x0;
%                 v1 = v1 / norm(v1);
%                 v2 = v2 / norm(v2);
%                 n = rigidlib.cross(v1, v2);
%                 n = n / norm(n);
%                 x = (x0 + x1 + x2)/3.0;
%                 xn = x + n;
%                 plot3([x(1), xn(1)], [x(2), xn(2)], [x(3), xn(3)], 'k');
%             end
        end
        
        %% Prints this mesh to an STL file
        function print(this, folder, filename, E)
            if isempty(this.V)
                return;
            end
            
            % Transform the mesh vertices by E
            V1 = E * [this.V; ones(1, size(this.V,2))];
            
            file = fopen([folder, '/', filename, '.stl'], 'w');
            fprintf(file,'solid %s\r\n', filename);
            
            for i=1:size(this.F,2)
                v1 = V1(1:3,this.F(1,i));
                v2 = V1(1:3,this.F(2,i));
                v3 = V1(1:3,this.F(3,i));
                
                n = cross(v1-v2,v3-v2);
                n = n/norm(n);
                
                fprintf(file,'\tfacet normal %f %f %f\r\n',n(1),n(2),n(3));
                fprintf(file,'\t\touter loop\r\n');
                fprintf(file,'\t\t\tvertex %f %f %f\r\n',v1(1),v1(2),v1(3));
                fprintf(file,'\t\t\tvertex %f %f %f\r\n',v2(1),v2(2),v2(3));
                fprintf(file,'\t\t\tvertex %f %f %f\r\n',v3(1),v3(2),v3(3));
                fprintf(file,'\t\tendloop\r\n');
                fprintf(file,'\tendfacet\r\n');
                
            end
            
            fprintf(file,'endsolid %s', filename);
            fclose(file);
        end
        
        %%
        function printScad(this, scadFile, E)
            fprintf(scadFile,'multmatrix(m = [ [%f, %f, %f, %f],\n[%f, %f, %f, %f],\n[%f, %f, %f, %f],\n[%f, %f, %f, %f ] ]) {', E');
            fprintf(scadFile,'polyhedron(points = ['); 
            for i=1:size(this.V,2)-1
                fprintf(scadFile,'[%f, %f, %f],',this.V(1,i), this.V(2,i), this.V(3,i));
            end
            fprintf(scadFile,'[%f, %f, %f] ], triangles = [', this.V(1,end), this.V(2,end), this.V(3,end));
            for i=1:size(this.F,2)-1
                fprintf(scadFile,'[%d, %d, %d],',this.F(1,i)-1, this.F(2,i)-1, this.F(3,i)-1);
            end
            fprintf(scadFile,'[%d, %d, %d] ]);}\n',this.F(1,end)-1, this.F(2,end)-1, this.F(3,end)-1); 
        end
    end
    
    methods (Static)
        %% Creates a cuboid mesh
        function [V, F] = cuboid(w, h, d)
            whd = 0.5 * [w, h, d];
            V = zeros(3,8);
            V(:,1) = [-whd(1), -whd(2), -whd(3)]';
            V(:,2) = [ whd(1), -whd(2), -whd(3)]';
            V(:,3) = [-whd(1),  whd(2), -whd(3)]';
            V(:,4) = [ whd(1),  whd(2), -whd(3)]';
            V(:,5) = [-whd(1), -whd(2),  whd(3)]';
            V(:,6) = [ whd(1), -whd(2),  whd(3)]';
            V(:,7) = [-whd(1),  whd(2),  whd(3)]';
            V(:,8) = [ whd(1),  whd(2),  whd(3)]';
            F = zeros(3,0);
            % +X
            F(:,end+1) = [2 4 6]';
            F(:,end+1) = [8 6 4]';
            % -X
            F(:,end+1) = [1 5 3]';
            F(:,end+1) = [7 3 5]';
            % +Y
            F(:,end+1) = [4 3 8]';
            F(:,end+1) = [7 8 3]';
            % -Y
            F(:,end+1) = [2 6 1]';
            F(:,end+1) = [5 1 6]';
            % +Z
            F(:,end+1) = [6 5 8]';
            F(:,end+1) = [7 8 5]';
            % -Z
            F(:,end+1) = [1 2 3]';
            F(:,end+1) = [4 3 2]';
        end
        
        %% Creates a cylinder mesh
        function [V, F] = cylinder(r, h, slices)
            V = zeros(3,0);
            F = zeros(3,0);
            % vertices
            if nargin < 3
                slices = 10;
            end
            % number of faces along z
            % NOTE: setting to 1 for 2D collision poly generation
            % (see CompRodCircle.makeMesh())
            stacks = 1;%max([1, floor(h/(2*pi*r/slices))]);
            % create circle verts for circles
            for i = 1:slices
                theta = (i-1)/slices*2*pi;
                x = r*cos(theta);
                y = r*sin(theta);
                V(:,end+1) = [x, y, -h*0.5]';
                % create circle verts for stacks
                for k = 1 : stacks-1
                    z = -h*0.5 + h*k/stacks;
                    V(:,end+1) = [x, y, z]';
                end
                V(:,end+1) = [x, y,  h*0.5]';
            end
            % create centre verts for lower and upper circles
            V(:,end+1) = [0.0, 0.0, -h*0.5]';
            V(:,end+1) = [0.0, 0.0,  h*0.5]';
            % indices for these two verts
            iTop = size(V,2);
            iBot = iTop - 1;
            % create faces
            for i = 1:slices
                % create pie face 
                i00 = (i-1)*(stacks+1) + 1; % index for bottom vertex
                i01 = i00 + (stacks+1); % next index on bottom
                if i == slices
                    i01 = 1;
                end
                i10 = i00 + stacks;
                i11 = i01 + stacks;
                F(:,end+1) = [iBot, i01, i00]'; % bottom face
                F(:,end+1) = [iTop, i10, i11]'; % top face
                % create side faces
                for k = 0 : stacks-1
                    F(:,end+1) = [i00+k,   i01+k,   i00+k+1]'; % side face 1
                    F(:,end+1) = [i01+k+1, i00+k+1, i01+k]';   % side face 2
                end
            end
        end
        
        %%
        function [V, F] = sphere(radius, stacks, slices)
            if nargin < 3
                slices = 10;
            end
            if nargin < 2
                stacks = 10;
            end
            V = zeros(3,0);
            % Create verts
            for k = 0 : stacks - 2
                theta = (k+1.0)/stacks*pi;
                z = radius*cos(theta);
                for i = 0 : slices - 1
                    phi = i/slices*2*pi;
                    x = radius * cos(phi) * sin(theta);
                    y = radius * sin(phi) * sin(theta);
                    V(:,end+1) = [x, y, z]';
                end
            end
            % Faces (not touching the poles)
            F = zeros(3,0);
            for k = 0 : stacks - 3
                for i = 0 : slices - 1
                    i00 = k*slices + i + 1;
                    i01 = k*slices + 1;
                    i10 = (k+1)*slices + i + 1;
                    i11 = (k+1)*slices + 1;
                    if i < slices - 1
                        i01 = i00 + 1;
                        i11 = i10 + 1;
                    end
                    F(:,end+1) = [i00, i10, i01]';
                    F(:,end+1) = [i11, i01, i10]';
                end
            end
            % Pole verts
            V(:,end+1) = [0.0, 0.0,  radius]';
            V(:,end+1) = [0.0, 0.0, -radius]';
            % Pole faces
            iBot = size(V,2);
            iTop = iBot - 1;
            for i = 0 : slices - 1
                iT0 = i + 1;
                iT1 = 1;
                iB0 = (stacks-2)*slices + i + 1;
                iB1 = (stacks-2)*slices + 1;
                if i < slices - 1
                    iT1 = iT0 + 1;
                    iB1 = iB0 + 1;
                end
                F(:,end+1) = [iTop, iT0, iT1]';
                F(:,end+1) = [iBot, iB1, iB0]';
            end
        end
    end
end
