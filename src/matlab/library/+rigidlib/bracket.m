function [ S ] = bracket( x )
% bracket Gets the skew symmetric cross product matrix

if length(x) < 6
    S = [0, -x(3), x(2); x(3), 0, -x(1); -x(2), x(1), 0];
else
    S(1:3,1:3) = [0, -x(3), x(2); x(3), 0, -x(1); -x(2), x(1), 0];
    S(1:3,4) = [x(4); x(5); x(6)];
    S(4,:) = zeros(1,4);
end

end
