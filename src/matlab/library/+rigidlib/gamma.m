function [ G ] = gamma( r )
%% gamma Gets the 3x6 gamma matrix, for computing the point velocity
G = [rigidlib.bracket(r(1:3))', eye(3)];

end
