function [ P ] = logE( E )
%logE logarithm of a transformation matrix
%   Detailed explanation goes here

R = E(1:3,1:3);
t = E(1:3,4);

trc=R(1,1)+R(2,2)+R(3,3);
trc2=(trc-1)/2;

if (1- trc2*trc2) >= eps
    theta = acos(0.5*(trc-1));
    W = 0.5*theta/sin(theta)*(R-R');
    w = rigidlib.unbracket(W);
    wlen = norm(w);
    sw = sin(wlen);
    cw = cos(wlen);
    Ainv = eye(3)-0.5*W+(2*sw-wlen*(1+cw))/(2*wlen*wlen*sw)*W*W;
else
    % BUGGY! If there is a 180 rotation, deal with outside this function.
    W = zeros(3);
    Ainv = eye(3);
end
P = [W,Ainv*t;0 0 0 0];

end
