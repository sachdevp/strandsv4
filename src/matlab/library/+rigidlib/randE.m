function [ E ] = randE(  )
%randE Creates a random transformation matrix

[Q,R] = qr(randn(3)); %#ok<NASGU>
if det(Q) < 0
    % Negate the Z-axis
    Q(:,3) = -Q(:,3);
end
E = [Q, randn(3,1); 0 0 0 1];

end
