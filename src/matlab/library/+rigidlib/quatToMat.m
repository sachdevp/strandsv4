function R = quatToMat(q)
% q: a quaternion with elements (x, y, z, w)
% http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm

sqw = q(4)*q(4);
sqx = q(1)*q(1);
sqy = q(2)*q(2);
sqz = q(3)*q(3);

% invs (inverse square length) is only required if quaternion is not already normalised
invs = 1 / (sqx + sqy + sqz + sqw);
R(1,1) = ( sqx - sqy - sqz + sqw)*invs ;
R(2,2) = (-sqx + sqy - sqz + sqw)*invs ;
R(3,3) = (-sqx - sqy + sqz + sqw)*invs ;

tmp1 = q(1)*q(2);
tmp2 = q(3)*q(4);
R(2,1) = 2.0 * (tmp1 + tmp2)*invs ;
R(1,2) = 2.0 * (tmp1 - tmp2)*invs ;

tmp1 = q(1)*q(3);
tmp2 = q(2)*q(4);
R(3,1) = 2.0 * (tmp1 - tmp2)*invs ;
R(1,3) = 2.0 * (tmp1 + tmp2)*invs ;
tmp1 = q(2)*q(3);
tmp2 = q(1)*q(4);
R(3,2) = 2.0 * (tmp1 + tmp2)*invs ;
R(2,3) = 2.0 * (tmp1 - tmp2)*invs ;

end
