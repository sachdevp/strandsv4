function [ R ] = expR( w, h )
% R = expm(w) for a rotation matrix

if nargin < 2
    h = 1;
end

wlen = norm(w);
if wlen < 1e-10
    R = eye(3);
else
    w = w / wlen;
    wX = w(1);
    wY = w(2);
    wZ = w(3);
    c = cos(wlen * h);
    s = sin(wlen * h);
    c1 = 1 - c;
    R = [
        c + wX * wX * c1, -wZ * s + wX * wY * c1, wY * s + wX * wZ * c1;
        wZ * s + wX * wY * c1, c + wY * wY * c1, -wX * s + wY * wZ * c1;
        -wY * s + wX * wZ * c1, wX * s + wY * wZ * c1, c + wZ * wZ * c1];
end

return
