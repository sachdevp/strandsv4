function R = aaToMat(axis, angle)
% From vecmath
R = eye(3);
ax = axis(1);
ay = axis(2);
az = axis(3);
EPS = 1.110223024E-16;
mag = sqrt( ax*ax + ay*ay + az*az);
if mag > EPS
    mag = 1.0/mag;
    ax = ax*mag;
    ay = ay*mag;
    az = az*mag;
    if abs(ax) < EPS && abs(ay) < EPS
        % Rotation about Z
        if az < 0
            angle = -angle;
        end
        sinTheta = sin(angle);
        cosTheta = cos(angle);
        R(1,1) =  cosTheta;
        R(1,2) = -sinTheta;
        R(2,1) =  sinTheta;
        R(2,2) =  cosTheta;
    elseif abs(ay) < EPS && abs(az) < EPS
        % Rotation about X
        if ax < 0
            angle = -angle;
        end
        sinTheta = sin(angle);
        cosTheta = cos(angle);
        R(2,2) =  cosTheta;
        R(2,3) = -sinTheta;
        R(3,2) =  sinTheta;
        R(3,3) =  cosTheta;
    elseif abs(az) < EPS && abs(ax) < EPS
        % Rotation about Y
        if ay < 0
            angle = -angle;
        end
        sinTheta = sin(angle);
        cosTheta = cos(angle);
        R(1,1) =  cosTheta;
        R(1,3) =  sinTheta;
        R(3,1) = -sinTheta;
        R(3,3) =  cosTheta;
    else
        % General rotation
        sinTheta = sin(angle);
        cosTheta = cos(angle);
        t = 1.0 - cosTheta;
        xz = ax * az;
        xy = ax * ay;
        yz = ay * az;
        R(1,1) = t * ax * ax + cosTheta;
        R(1,2) = t * xy - sinTheta * az;
        R(1,3) = t * xz + sinTheta * ay;
        R(2,1) = t * xy + sinTheta * az;
        R(2,2) = t * ay * ay + cosTheta;
        R(2,3) = t * yz - sinTheta * ax;
        R(3,1) = t * xz - sinTheta * ay;
        R(3,2) = t * yz + sinTheta * ax;
        R(3,3) = t * az * az + cosTheta;
    end
end
return
