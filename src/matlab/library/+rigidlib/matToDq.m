function dq = matToDq(E)
% dq: unit dual quaternion
R = E(1:3,1:3);
t = E(1:3,4);
q0 = rigidlib.matToQuat(R);

% non-dual part (just copy q0):
dq(:,1) = q0;
% dual part:
dq(1,2) = -0.5*(t(1)*q0(2) + t(2)*q0(3) + t(3)*q0(4));
dq(2,2) = 0.5*( t(1)*q0(1) + t(2)*q0(4) - t(3)*q0(3));
dq(3,2) = 0.5*(-t(1)*q0(4) + t(2)*q0(1) + t(3)*q0(2));
dq(4,2) = 0.5*( t(1)*q0(3) - t(2)*q0(2) + t(3)*q0(1));

end
