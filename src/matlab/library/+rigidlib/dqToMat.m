function E = dqToMat(dq)

% TODO: remove this transpose
dq = dq';
% regular quaternion (just copy the non-dual part):
q0 = dq(1,:);
% translation vector:
t(1) = 2.0*(-dq(2,1)*dq(1,2) + dq(2,2)*dq(1,1) - dq(2,3)*dq(1,4) + dq(2,4)*dq(1,3));
t(2) = 2.0*(-dq(2,1)*dq(1,3) + dq(2,2)*dq(1,4) + dq(2,3)*dq(1,1) - dq(2,4)*dq(1,2));
t(3) = 2.0*(-dq(2,1)*dq(1,4) - dq(2,2)*dq(1,3) + dq(2,3)*dq(1,2) + dq(2,4)*dq(1,1));

R = rigidlib.quatToMat(q0);
E = [R t'; 0 0 0 1];

end
