function [ Ei ] = invert( E )
%invert Inverts a transformation matrix
R = E(1:3,1:3);
p = E(1:3,4);
Ei = [R', -R' * p; 0 0 0 1];
end
