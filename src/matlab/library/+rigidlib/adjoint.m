function Ad = adjoint(E)
%adjoint Gets the adjoint transform
Ad = zeros(6);
R = E(1:3,1:3);
p = E(1:3,4);
Ad(1:3,1:3) = R;
Ad(4:6,4:6) = R;
Ad(4:6,1:3) = rigidlib.bracket(p) * R;
end
