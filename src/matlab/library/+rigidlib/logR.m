function [ W ] = logR( R )
% W = logm(R) for a rotation matrix
% http://www.cs.ucla.edu/~soatto/vision/courses/268/rodrigues.m

trc=R(1,1)+R(2,2)+R(3,3);
trc2=(trc-1)/2;

%s=[R(3,2)-R(2,3), R(1,3)-R(3,1), R(2,1)-R(1,2)]';
S = R-R';
if (1- trc2*trc2) >= eps
    tHeta = (acos(trc2));
    tHetaf=tHeta/ (2 *sin(tHeta));
else
    tHeta = real(acos(trc2));
    tHetaf=0.5/(1-tHeta/6); % = ~ 1/2+tHeta/12
    %disp('approximate result in rodrigues');
end;
W = tHetaf * S;

return
