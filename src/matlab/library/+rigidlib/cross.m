function [ v ] = cross( v1, v2 )
%crossprod Matlab's version was a bottleneck!

% From vecmath
v = zeros(3,1);
v1x = v1(1);
v1y = v1(2);
v1z = v1(3);
v2x = v2(1);
v2y = v2(2);
v2z = v2(3);
x = v1y*v2z - v1z*v2y;
y = v2x*v1z - v2z*v1x;
v(3) = v1x*v2y - v1y*v2x;
v(1) = x;
v(2) = y;

end
