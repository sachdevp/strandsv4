function drawAxis(E,r)

if nargin < 2
    r = 1;
end
x = E(1:3,1);
y = E(1:3,2);
z = E(1:3,3);
p = E(1:3,4);
plot3([p(1), p(1) + r*x(1)], [p(2), p(2) + r*x(2)], [p(3), p(3) + r*x(3)], 'r');
plot3([p(1), p(1) + r*y(1)], [p(2), p(2) + r*y(2)], [p(3), p(3) + r*y(3)], 'g');
plot3([p(1), p(1) + r*z(1)], [p(2), p(2) + r*z(2)], [p(3), p(3) + r*z(3)], 'b');

end
