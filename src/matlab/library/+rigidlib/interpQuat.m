function R = interpQuat(R0, R1, t, flip)
% Interpolate using quaternion

if nargin < 4
    flip = false;
end

q0 = rigidlib.matToQuat(R0);
q1 = rigidlib.matToQuat(R1);
if flip
    q1 = -q1;
end
q = (1-t)*q0 + t*q1;
q = q / norm(q);
R = rigidlib.quatToMat(q);

end
