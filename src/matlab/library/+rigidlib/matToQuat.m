function q = matToQuat(R)
% q: a unit quaternion with elements (x, y, z, w)
% http://www.j3d.org/matrix_faq/matrfaq_latest.html

T = R(1,1) + R(2,2) + R(3,3);
if T > 1e-8
    S = sqrt(T + 1.0) * 2.0;
    X = (R(2,3)-R(3,2)) / S;
    Y = (R(3,1)-R(1,3)) / S;
    Z = (R(1,2)-R(2,1)) / S;
    W = 0.25 * S;
else
    if ( R(1,1) > R(2,2) && R(1,1) > R(3,3) )
        S  = sqrt( 1.0 + R(1,1) - R(2,2) - R(3,3) ) * 2;
        X = 0.25 * S;
        Y = (R(1,2) + R(2,1) ) / S;
        Z = (R(3,1) + R(1,3) ) / S;
        W = (R(2,3) - R(3,2) ) / S;
    elseif ( R(2,2) > R(3,3) )
        S  = sqrt( 1.0 + R(2,2) - R(1,1) - R(3,3) ) * 2;
        X = (R(1,2) + R(2,1) ) / S;
        Y = 0.25 * S;
        Z = (R(2,3) + R(3,2) ) / S;
        W = (R(3,1) - R(1,3) ) / S;
    else
        S  = sqrt( 1.0 + R(3,3) - R(1,1) - R(2,2) ) * 2;
        X = (R(3,1) + R(1,3) ) / S;
        Y = (R(2,3) + R(3,2) ) / S;
        Z = 0.25 * S;
        W = (R(1,2) - R(2,1) ) / S;
    end
end
q = [X Y Z W]';
q = q / norm(q);

end
