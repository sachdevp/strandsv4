classdef Node < handle
% NODE Makes the nodes available by reference.
    properties
        x
        s
        v
        % ix and is represent indices in strand velocity vector
        ix
        is
        nodeType
        % LInd and EInd represent indices in global velocity vector v
        LInd
        EInd
    end
end