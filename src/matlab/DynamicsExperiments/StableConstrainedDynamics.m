function clothMS
%clothMS Mass-Spring cloth with 'Stable Constrained Dynamics'

N = 10; % number of vertices along x and y
tEnd = 1.0; % end time
h = 1e-2; % time step
drawHz = 1000; % draw frequency
grav = [0 0 -9.8]'; % gravity
density = 1e0; % linear density (kg/m)
e = 1e7; % Young's modulus
stable = true; % turn on/off Stable Constrained Dynamics

% Cloth geometry
xmin = -0.2;
xmax = 0.2;
ymin = -0.2;
ymax = 0.2;

verts = [];
n = 0;
% Create main grid vertices
for i = 1 : N
	si = (i-1)/(N-1);
	xi = (1-si)*xmin + si*xmax;
	for j = 1 : N
		sj = (j-1)/(N-1);
		yj = (1-sj)*ymin + sj*ymax;
		verts(end+1).X = [xi yj]'; %#ok<*AGROW> % reference coords
		verts(end  ).x = [xi yj 0]'; % world coords
		verts(end  ).v = zeros(3,1); % velocity
		verts(end  ).m = 0; % mass
		if i == 1 && j == 1 || i == 1 && j == N
			% fixed vertices do not get any indices
			verts(end).i = [];
		else
			% free vertices get indices
			verts(end).i = n + (1:3);
			n = n + 3;
		end
	end
end
kc = length(verts); % number of vertices in the main grid

% Create off-grid vertices
dx = 0.5*(xmax-xmin)/(N-1);
dy = 0.5*(ymax-ymin)/(N-1);
xmin1 = xmin + dx;
xmax1 = xmax - dx;
ymin1 = ymin + dy;
ymax1 = ymax - dy;
for i = 1 : N-1
	if N == 2
		si = 0;
	else
		si = (i-1)/(N-2);
	end
	xi = (1-si)*xmin1 + si*xmax1;
	for j = 1 : N-1
		if N == 2
			sj = 0;
		else
			sj = (j-1)/(N-2);
		end
		yj = (1-sj)*ymin1 + sj*ymax1;
		verts(end+1).X = [xi yj]'; % reference coords
		verts(end  ).x = [xi yj 0]'; % world coords
		verts(end  ).v = zeros(3,1); % world velocity
		verts(end  ).m = 0; % mass
		verts(end  ).i = n + (1:3); % indices
		n = n + 3;
	end
end

% Create X springs
springs = [];
for i = 1 : N
	for j = 1 : N-1
		k0 = (i-1)*N + (j-1) + 1; % lower left index
		springs(end+1).i = [k0,k0+1];
	end
end
% Create Y springs
for i = 1 : N-1
	for j = 1 : N
		k0 = (i-1)*N + (j-1) + 1; % lower left index
		springs(end+1).i = [k0,k0+N];
	end
end
% Diagonal springs
for i = 1 : N-1
	for j = 1 : N-1
		k0 = (i-1)*N + (j-1) + 1; % lower left index
		kc0 = kc + (i-1)*(N-1) + (j-1) + 1; % center index
		springs(end+1).i = [k0,kc0];
		springs(end+1).i = [k0+N,kc0];
		springs(end+1).i = [k0+N+1,kc0];
		springs(end+1).i = [k0+1,kc0];
	end
end

% Pre-process springs
m = length(springs);
for k = 1 : m
	% Vertex indices
	ia = springs(k).i(1);
	ib = springs(k).i(2);
	% Reference coordinates (2D)
	Xa = verts(ia).X;
	Xb = verts(ib).X;
	% Compute length
	dx = Xb - Xa;
	L = norm(dx);
	springs(k).L = L;
	% Compute lumped mass for each triangle vertex
	m2 = density*L/2; % mass/2
	verts(ia).m = verts(ia).m + m2;
	verts(ib).m = verts(ib).m + m2;
	% Lagrange multiplier
	springs(k).lambda = 0;
end

t = 0;
draw(0,verts,springs);
tDrawLast = 0;
I3 = eye(3);
while t < tEnd
	M = sparse(n,n); % mass matrix
	K = sparse(n,n); % stiffness matrix
	J = sparse(m,n); % spring Jacobian matrix
	C = sparse(m,m); % spring compliance matrix
	f = zeros(n,1); % force vector
	v = zeros(n,1); % velocity vector
	phi = zeros(m,1); % spring stabilization vector
	% Go through all vertices and fill M, f, and v
	for k = 1 : length(verts)
		i = verts(k).i;
		if isempty(i)
			continue;
		end
		mk = verts(k).m;
		M(i,i) = mk*eye(3); %#ok<*SPRIX>
		v(i) = verts(k).v;
		f(i) = mk*grav;
	end
	% Go through all springs and fill K, f, J, and C
	for k = 1 : m
		spring = springs(k);
		ia = spring.i(1);
		ib = spring.i(2);
		xa = verts(ia).x;
		xb = verts(ib).x;
		dx = xb - xa;
		l = norm(dx);
		u = dx/l;
		phi(k) = l - spring.L;
		lambda = e*phi(k);
		fk = lambda*u;
		uu = u*u';
		Kk1 = -e*uu; %#ok<*NASGU>
		if stable
			Kk2 = -spring.lambda/l*(I3 - uu);
			Kk = Kk2;
		else
			Kk2 = -lambda/l*(I3 - uu);
			Kk = Kk1 + Kk2;
		end
		iia = verts(ia).i;
		iib = verts(ib).i;
		freeA = ~isempty(iia);
		freeB = ~isempty(iib);
		if freeA
			f(iia) = f(iia) + fk;
			K(iia,iia) = K(iia,iia) + Kk;
			J(k,iia) = u';
		end
		if freeB
			f(iib) = f(iib) - fk;
			K(iib,iib) = K(iib,iib) + Kk;
			J(k,iib) = -u';
		end
		if freeA && freeB
			K(iia,iib) = K(iia,iib) - Kk;
			K(iib,iia) = K(iib,iia) - Kk;
			C(k,k) = 1/e;
		end
	end
	% Solve linear system
	if stable
		A = [M - h*h*K, J'; J, -C/h^2];
		b = [M*v + h*f; 0.1*phi/h];
		vl = A\b;
		v = vl(1:n);
		l = vl(n+1:end);
	else
		A = M - h*h*K; %#ok<*UNRCH>
		b = M*v + h*f;
		v = A\b;
		l = zeros(m,1);
	end
	% Go through all vertices and update position and velocities
	for k = 1 : length(verts)
		i = verts(k).i;
		if isempty(i)
			continue;
		end
		verts(k).v = v(i);
		verts(k).x = verts(k).x + h*verts(k).v;
	end
	% Go through springs and update Lagrange multipliers
	for k = 1 : m
		springs(k).lambda = l(k);
	end
	t = t + h;
	% Optionally draw
	if drawHz ~= 0 && (t == 0 || t - tDrawLast > 1/drawHz)
		draw(t,verts,springs);
		tDrawLast = t;
	end
end

end

function draw(t,verts,springs)
if t == 0
	clf;
	xlabel('X');
	ylabel('Y');
	zlabel('Z');
	axis equal;
	grid on;
	view(3);
end
cla;
hold on;

% Draw springs
xs = zeros(3,0);
for k = 1 : length(springs)
	xa = verts(springs(k).i(1)).x;
	xb = verts(springs(k).i(2)).x;
	xs(:,end+(1:3)) = [xa,xb,nan(3,1)];
end
plot3(xs(1,:),xs(2,:),xs(3,:),'b-');

% Draw nodes
x = [verts.x];
plot3(x(1,:),x(2,:),x(3,:),'r.');

str = sprintf('%.3f', t);
title(str);
drawnow

end
