function success = InitMatrices(scene)
% TODO Also add code for initializing indices
    assert(scene.n==0);
    scene.n_rigid = 6*length(scene.rigids);
    scene.n_RL = scene.n_rigid;
    scene.n_N = 0;
    scene.n_R = scene.n_rigid;        % n, nR, nRL are the same till here
    
    for strand = scene.strands
        nStrandNodes = length(scene.strands.nodes);
        scene.n_N = scene.n_N + nStrandNodes;
        % CHECK Confirm size
        strand.JQ = sparse(nStrandNodes - 2, 3*nStrandNodes); 
    end
    
    % Compute total number of reduced dof
    for dof = scene.dofs
        scene.n_r = scene.n_r + dof.n_r;
    end
    
    scene.n_R  = scene.n_R + scene.n_r;
    scene.n_RL = scene.n_RL + 3*scene.n_N;
    scene.n = scene.n_RL;       % n and nRL are the same till here.
    for strand = scene.strands
        scene.n = scene.n + length(scene.strands.nodes) - 2;
    end
    scene.M = sparse(scene.n,scene.n);
    scene.D = sparse(scene.n,scene.n);
    scene.K = sparse(scene.n,scene.n);
    scene.f = sparse(scene.n,1);
    scene.v = sparse(scene.n,1);
    scene.v_R = zeros(scene.n_R, 1);
    scene.JR  = sparse(scene.n_RL, scene.n_R);
    rigidIndv = (1:scene.n_rigid)';
    scene.JR(rigidIndv, rigidIndv) = eye(scene.n_rigid);
    scene.JRL  = sparse(scene.n, scene.n_RL);
    RLIndv = (1:scene.n_RL)';
    scene.JRL(RLIndv, RLIndv) = eye(scene.n_RL);
    success = true;
end