function C = GetConstCompl(scene)

% Returns complicane matrix for all joints
    nJ = size(scene.joints,2);
    sz = 0;
    for iJ=1:nJ
        sz = sz + size(scene.joints(iJ).g,1);
    end
    % Return column vector
    % C = zeros(sz);
    C = eye(sz);
    C(1:6, 1:6)=zeros(6);
end
