function vDrift = GetPosDrift(scene)
    nJ = size(scene.joints,2);
    sz = 0;
    vDrift = [];
    for iJ=1:nJ
      joint = scene.joints(iJ);
      sz = sz + size(joint.g,1);
      vDrift = [vDrift; ComputeDrift(joint)];
    end
    % Return column vector
    % vDrift = zeros(sz,1);
end

function drift = ComputeDrift(joint)
  ra = joint.rigid1;
  rb = joint.rigid2;
  if isempty(ra)
    aToW = eye(4);
  else
    aToW = ra.E;
  end
  bToW = rb.E;
  bJE  = bToW * inv(joint.bToJ0);
  aJE  = aToW * inv(joint.aToJ0);
  dx   = (bJE(1:3, 4) - aJE(1:3, 4));
  drift = [];
  for iR=1:6
    if any(abs(joint.rows-iR)<1e-10)
      if iR<=3
        drift = [drift; 0];
        continue
      end
      drift = [drift; dx(iR-3)];
    end
  end
end
