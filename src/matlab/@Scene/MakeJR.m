function success = MakeJR(scene)
% TODO Construct Jacobian JR. FIXME Incorrect code
    ind = scene.n_rigid;
    for i = 1:length(scene.dofs)
        scene.dofs(i).ComputeJacobian();
        dof = scene.dofs(i);
        % Only update JR for dof if it was updated. This will prevent
        % updates to identity part of the matrix.
        if dof.dirtyJR
            % Set Gamma from source index to full destination indices
            dfInd = dof.getdfInd();
            sInd = dof.getsInd();
            
            scene.JR(dfInd, sInd) = dof.Gamma;
            % Set Omega from reduced destination index to full
            % destination indices
            if ~isequal(dof.n_r, 0)
                drInd = ind + (1:dof.n_r);
                scene.JR(dfInd, drInd) = dof.Omega;
                ind = ind + dof.n_r;
            end
        end
    end
end