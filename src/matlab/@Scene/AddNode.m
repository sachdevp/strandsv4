% CHECK The addnode function is a little shaky. Confirm that it works.
function node = AddNode(scene, strandID, nodeType, pos, l, rigid, circle)
% ADDNODE This function adds the node to the strand and adds a corresponding DOF to
% the scene.
    
% Add node to strand
    strand = scene.strands(strandID);
    node = strand.AddNode(pos);
    % Create corresponding DoF
    % CHECK Does dof need xr?
    dof = DoF();
    dof.node = node;
    dof.nodeType = nodeType;
    % dof.node = node;
    switch nodeType
      case 'full'
        dof.l = [];
        dof.J = speye(3);
        dof.n_r = 3;
        dof.rigidBody = [];
      case 'circle'
        assert(nargin >= 5);    % May not attach to rigid body
        dof.l = l;
        if nargin == 5
            dof.rigidBody = [];
        else
            dof.rigidBody = rigid;
        end
        dof.Gamma = sparse(3,6);
        dof.Omega = zeros(3,1);
        dof.n_r = 1;
        dof.circle = circle;
        dof.x_r = 0;
      case 'fixed'
        assert(nargin >= 5);
        % Ignore l input
        if nargin == 5
            dof.rigidBody = [];
        else
            dof.rigidBody = rigid;
            dof.l = pos - rigid.x(4:6);
        end
        dof.Gamma = sparse(3,6);
        dof.Omega = [];
        dof.n_r = 0;
      otherwise
        disp('Node type not recognized')
    end
    scene.dofs = [scene.dofs dof];          % Append dof
end
