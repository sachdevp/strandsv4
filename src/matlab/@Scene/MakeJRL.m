function success = MakeJRL(scene)
% Construct JRL matrix. Do not update identity. 
% Only update the strand JQ.
    for strand = scene.strands
        strand.ComputeJQ();
        scene.JRL(strand.EInd, strand.LInd) = strand.JQ;
    end
    success=true;
end