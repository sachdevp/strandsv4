function success = InitMassMatrix(scene,h)
% Sets the size of the mass matrix and initializes terms that are
% pre-determined, such as the rigid body mass.
%CustomPrint('Mass Matrix Size: %d', scene.n);
    for rigid = scene.rigids
        % Set mass matrix for rigid body
        scene.M(rigid.ind, rigid.ind) = rigid.M;
        % NOTE Use damping if needed
        % scene.D(rigid.ind,rigid.ind) = rigid.M *0.1;
    end
    success = true;
end