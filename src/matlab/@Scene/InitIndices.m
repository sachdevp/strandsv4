function success = InitIndices(scene)
% INITINDICES Function to initialize index values
% for all the elements of the scene.
    ind = 0;
    % CustomPrint('Rigids length',length(scene.rigids), scene.debug);
    for i = 1:length(scene.rigids)
        scene.rigids(i).ind = ind + (1:6)';
        % CustomPrint('Rigid Ind',scene.rigids(i).ind, scene.debug);
        ind = ind + 6;
    end
    mgi = 0;
    for strand = scene.strands
        if strand.isInex()
            length(strand.nodes)
            nS = strand.getnSegs();
            mgi = mgi + nS;
        end
        nL = strand.getnL();
        LInd = ind + (1:nL)';
        ind = ind + nL;
        strand.setLInd(LInd);
    end
    for strand = scene.strands
        nE = strand.getnE(); % 2 nodes at the ends have fixed E.
        EInd = ind + (1:nE)';
        ind = ind + nE;
        strand.setEInd(EInd);
        strand.InitMassMatrices();
    end
    success = true;
    if mgi
        scene.Gi = zeros(mgi, ind);
        scene.gi = zeros(mgi, 1);
    end
end
