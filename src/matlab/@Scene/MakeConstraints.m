function success = MakeConstraints(scene)
% Solving for Joint constraints
    scene.g = 0*scene.g;
    for i = 1 : length(scene.joints)
        joint = scene.joints(i);
        ra = joint.rigid1;
        rb = joint.rigid2;
        if isequal(ra, [])
            aToW = eye(4);
        else
            aToW = ra.E;
        end
        bToW  = rb.E;
        aToJ0 = joint.aToJ0;
        wToA  = rigidlib.invert(aToW);
        bToJ  = aToJ0 * wToA * bToW;
        aAdJ0 = rigidlib.adjoint(aToJ0);
        bAdJ  = rigidlib.adjoint(bToJ);
        rows  = joint.rows;
        if ~isequal(ra, [])
            scene.G(ra.ind,joint.m) = aAdJ0(rows,:)';
        end
        scene.G(rb.ind,joint.m) = -bAdJ(rows,:)';
        scene.g(joint.m) = joint.g;
    end
end