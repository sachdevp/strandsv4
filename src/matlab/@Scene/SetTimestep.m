function success = SetTimestep(scene, dt)
    if(dt>0.0)
        scene.dt = dt;
        success = true;
        return;
    end
    success = false;
end