function strand = AddStrand(scene)
    strand = Strand();
    scene.strands = [scene.strands strand];
    assert(~isequal(scene.grav,[]));
end