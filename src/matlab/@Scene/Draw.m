function success = Draw(scene)
% TODO Write draw function
    scene.tDrawLast = scene.t;
    if scene.t == 0
        clf;
        xlabel('X');
        ylabel('Y');
        zlabel('Z');
        axis equal;
        % grid on;
        grid off;
        view(0,0);
    end
    cla;
    hold on;
    % Draw rigid bodies
    for rigid = scene.rigids
        rigid.Draw();
    end
    for strand = scene.strands
        for i = 1 : length(strand.nodes)
            x = strand.nodes(i).x;
            if (i>1) && (i < length(strand.nodes)-1)
                %                         if quasi
                plot3(x(1),x(2),x(3),'ko','MarkerSize',10);
                %                         else
                %                             plot3(x(1),x(2),x(3),'ks','MarkerSize',10);
                %                         end
            else
                plot3(x(1),x(2),x(3),'k^','MarkerSize',10);
            end
        end
        for i = 2 : length(strand.nodes)
            x0 = strand.nodes(i-1).x;
            x1 = strand.nodes(i).x;
            plot3([x0(1),x1(1)],[x0(2),x1(2)],[x0(3),x1(3)],'b-');
        end
        for s = linspace(0,strand.nodes(end).s,4);
            drawAtS(s,strand.nodes);
        end
    end
    for dof = scene.dofs
        dof.Draw();
    end
    str = sprintf('t = %.2f s', scene.t);
    title(str);
    
    axis([-0.4 0.8 -1 1 -0.8 0.2]*10);
    pbaspect([1 1 1])
    drawnow
    
    %CustomPrint('Reduced Velocity: ', scene.v_R);
    success = true;
end

function drawAtS(s,nodes)
    for i = 2 : length(nodes)
        s0 = nodes(i-1).s;
        s1 = nodes(i).s;
        if s0 <= s && s <= s1
            a = (s - s0)/(s1 - s0);
            x0 = nodes(i-1).x;
            x1 = nodes(i).x;
            x = (1-a)*x0 + a*x1;
            plot3(x(1),x(2),x(3),'rs');
            break;
        end
    end
end