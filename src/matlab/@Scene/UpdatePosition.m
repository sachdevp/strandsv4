function success = UpdatePosition(scene, h)
    for rigid = scene.rigids
        rigid.UpdatePosition(h);
    end
    for strand = scene.strands
        strand.UpdateNodes(h);
    end
    ind = scene.n_rigid;
    for dof = scene.dofs
        if dof.n_r>0
            tmpv = scene.v_R(ind+(1:dof.n_r));
            dof.UpdateDoF(tmpv, h);
            ind = ind + dof.n_r;
        end
        dof.UpdatePosition();
    end

    success = true;
end