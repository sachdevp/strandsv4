function success = AddJoint(scene, rigid1, rigid2, E, fixed, phi)
% ADDJOINT Adds a joint to the rigid body with fixed specifying the bo

    % Create joint
    joint.rigid1 = rigid1;      % [] if world constraint for rigid2
    joint.rigid2 = rigid2;
    assert(length(fixed) == 6);
    joint.rows = [];
    for i=1:6
        if fixed(i) == true
            joint.rows = [joint.rows i];          % What rows to constrain
        end
    end
    joint.E = E;                 % Joint location
    if nargin >= 6
        joint.g = phi;
    else
        joint.g = zeros(length(joint.rows),1);
    end
    % Append to joint
    scene.joints = [scene.joints joint];
    success = true;
end
