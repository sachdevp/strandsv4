function success = WriteVelocity(scene)
% Writes the velocities for the various objects.
    for rigid = scene.rigids
        rigid.v = scene.v(rigid.ind);
    end
    for strand = scene.strands
        % Update Lagrangian velocities
        strand.setLv(scene.v(strand.LInd));
        % Update Eulerian velocities for the strand
        strand.setEv(strand.JQ*strand.Lv);
    end
    success = true;
end