
function success = InitJR(scene)
    scene.JR = sparse(scene.n_RL, scene.n_R);
    % Set square matrix at head of JRL to identity
    sz = size(scene.JRL,2);
    scene.JRL(1:sz,1:sz) = eye(sz);
    success = true;
end