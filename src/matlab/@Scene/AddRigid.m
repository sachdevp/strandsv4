% Currently only adds cuboids with size vector [l w d]'
function rigid = AddRigid(scene, pos, rho, sz, scripted, phi)
    if(nargin==4)
        % @scripted not provided
        scripted = false;
    end
    if nargin<=5
        % @phi not provided
        phi = [0 0 0 0 0 0]';
    else
        % Check @phi length
        assert(length(phi) == 6);
    end
    % Use @Rigid handle class. Passes as reference
    rigid = Rigid();
    % Set properties
    rigid.x = pos;
    rigid.scripted = scripted;
    % if scripted
    % Set velocity if scripted
    rigid.v = phi;
    % end
    % Set dimensions
    rigid.whd = sz;
    % Append to @scene.rigids vector
    scene.rigids = [scene.rigids rigid];
    % Set the mass matrix using density
    rigid.Init(rho);
    if scripted
        % FIXME Not done yet
        E = eye(4);
        scene.AddJoint([], rigid, E, [1 1 1 1 1 1]', phi);
    end
    % CHECK That rigid indices are defined
end
