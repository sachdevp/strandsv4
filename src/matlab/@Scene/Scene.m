%% Scene Class
% This is the entry point to the simulator.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Notes:                                                    %
% 1. Assume that there are no nodes shared between strands. %
% 2. DOFs are not handles, so you cannot refer to it.       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef Scene < handle
% SCENE class
% Provides the interface for Strands.m to use.
    properties
        M                               % Mass matrix
        JR                              % Jacobian for RL to R space
        JRL                             % Quasistatic Jacobian
        v                               % Full velocity vector
        v_RL                            % RL-space velocity
        v_R                             % R-space velocity
        f                               % Force vector
        D
        K
        G
        g
        Gi
        gi
        mgi
        AdjMat
        JDampMat
        dataDir = './Data/';
        imgID = 0;
        t = 0;
        tDrawLast = 0;
        strands = [];
        rigids = [];
        joints = [];
        dofs = [];
        n = 0;
        n_R = 0;
        n_RL = 0;
        n_N = 0;
        n_r = 0;
        % n_rigid is NOT the count of rigid bodies, but rather the rigid DoFs
        n_rigid = 0;
        grav = [];
        debug = false;
        quasi = true;
        export = false;
        dt = 1e-2;
        lambda = [];
    end

    methods
        % Currently only adds cuboids with size vector [l w d]'
        rigid = AddRigid(scene, pos, rho, sz, scripted, phi);
        strand = AddStrand(scene);

        % CHECK The addnode function is a little shaky. Confirm that it works.
        node = AddNode(scene, strandID, nodeType, pos, l, rigid, circle);
        % ADDNODE This function adds the node to the strand and adds a corresponding DOF to
        % the scene.

        success = InitIndices(scene);
        % INITINDICES Function to initialize index values
        % for all the elements of the scene.

        success = InitMatrices(scene);
        
        success = InitJoints(scene);
        success = Init(scene);
        % INIT Function to perform any computation prior to the actual
        % recurring simulation steps.
        success = CleanUp(scene);
        % CLEANUP Cleans up after the whole simulatoin has been done. Might
        % not be a useful function, but created just in case. Any post
        % processing should be done here
        success = MakeJR(scene);
        success = MakeJRL(scene);
        success = InitMassMatrix(scene,h);
        success = InitJR(scene);

        % Fill mass matrix with strand mass
        % Also fill JRL with the strand JQ matrices
        success = MakeMassMatrix(scene);

        % Solving for Joint constraints
        success = MakeConstraints(scene);
        vDrift  = GetPosDrift(scene);
        % Returns complicane matrix for all joints
        mCompl  = GetConstCompl(scene);
        success = SetGravity(scene, grav);

        success = Step(scene);

        success = WriteVelocity(scene);
        success = UpdatePosition(scene, h);
        success = PostStabilize(scene,h);
        success = Draw(scene);
        % CHECK PritImage may not be working
        success = PrintImage(scene);
        % ADDJOINT Adds a joint to the rigid body with fixed specifying the bo
        success = AddJoint(scene, rigid1, rigid2, E, fixed, phi);
        success = SetExport(scene, val, dir);
        success = SetTimestep(scene, dt);
        success = SetDebug(scene, debug);
    end
end

