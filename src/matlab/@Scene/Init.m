function success = Init(scene)
% INIT Function to perform any computation prior to the actual
% recurring simulation steps.
    success = true;
    % Not only set the size, but also the indices
    success = success & scene.InitMatrices();
    success = success & scene.InitIndices();
    success = success & scene.InitMassMatrix();  
    for strand = scene.strands
        strand.Init();
    end
    scene.InitJoints();
end