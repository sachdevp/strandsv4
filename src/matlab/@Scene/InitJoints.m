function success = InitJoints(scene)
% Initialize joints
    mg = 0;
    for i = 1 : length(scene.joints)
        r1 = scene.joints(i).rigid1;
        r2 = scene.joints(i).rigid2;
        if isequal(r1, [])
            aToW = eye(4);
        else
            aToW = r1.E;
        end
        bToW = r2.E;
        jToW = scene.joints(i).E; % where the joint is wrt world
        wToJ = rigidlib.invert(jToW);
        scene.joints(i).aToJ0 = wToJ * aToW; % where A is wrt to J
        scene.joints(i).bToJ0 = wToJ * bToW; % where B is wrt to J

        % CHECK Don't think joints need to know rows.
        % Row indices
        scene.joints(i).m = mg + (1:length(scene.joints(i).rows));
        % Increment number of constraint rows
        mg = mg + length(scene.joints(i).rows);
        scene.joints(i).damping = 1;
    end
    scene.G = zeros(scene.n_R, mg);
    scene.g = zeros(mg,1);
    nJ = length(scene.joints);
    scene.JDampMat = zeros(nJ, nJ);
    % Rows: Number of joints; Cols: Number of reduced DoFs
    scene.AdjMat   = zeros(nJ, scene.n_R);
    success = true;
end
