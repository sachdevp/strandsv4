function success = MakeMassMatrix(scene)
% Fill mass matrix with strand mass
% Also fill JRL with the strand JQ matrices
    scene.f(:) = 0;
    mgi = 0;
    scene.Gi(1:end, 1:end) = 0;
    scene.gi(1:end) = 0;
    for strand = scene.strands
        strand.ComputeMass(scene.grav);
        % At this point, M and f have been computed for the strand
        % Note that strand mass is not consecutive
        scene.M(strand.LInd, strand.LInd)   = strand.getMLL();
        scene.M(strand.EInd, strand.EInd)   = strand.getMEE();
        scene.M(strand.LInd, strand.EInd)   = strand.getMLE();
        scene.M(strand.EInd, strand.LInd)   = strand.getMLE()';
        scene.f([strand.LInd; strand.EInd]) = [strand.getfx(); strand.getfs()];
        if strand.isInex() && strand.isTaut()
            [Gxi, Gsi, gi] = strand.getInexCons();
            lInd = strand.LInd;
            eInd = strand.EInd;
            nS = strand.getnSegs();
            rows = (mgi+1):(mgi+nS);
            size(scene.Gi);
            scene.Gi(rows, lInd) = Gxi;
            if ~isempty(eInd)
                scene.Gi(rows, eInd) = Gsi(:, 2:(end-1));
            end
            scene.gi(rows) = gi;
            mgi = mgi+nS;
        end 
    end
    for rigid = scene.rigids
        tr_ind = rigid.ind(4:6);
        R = rigid.E(1:3, 1:3);
        scene.f(tr_ind) = rigid.M(4:6, 4:6)*R'*scene.grav;
    end
    scene.JDampMat(:,:) = 0;
    scene.AdjMat(:,:) = 0;
    m = 0;
    for joint = scene.joints
        ra = joint.rigid1;
        rb = joint.rigid2;
        if isequal(ra, [])
            aToW = eye(4);
        else
            aToW = ra.E;
        end
        bToW  = rb.E;
        aToJ0 = joint.aToJ0;
        wToA  = rigidlib.invert(aToW);
        bToJ  = aToJ0 * wToA * bToW;
        aAdJ0 = rigidlib.adjoint(aToJ0);
        bAdJ  = rigidlib.adjoint(bToJ);
        m = m+1;
        if ~isequal(ra, [])
            scene.AdjMat(m, ra.ind) = aAdJ0(2,:)';
        end
        scene.AdjMat(m, rb.ind) = -bAdJ(2,:)';
        scene.JDampMat(m, m) = joint.damping;
    end
    scene.mgi = mgi;
    success = true;
end
