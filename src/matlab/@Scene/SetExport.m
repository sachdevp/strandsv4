function success = SetExport(scene, val, dir)
    export = val;
    dataDir = dir;
    success = true;
    if(export && (~exist(dataDir)))
        success = mkdir(dataDir);
    end
end