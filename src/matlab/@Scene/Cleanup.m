function success = Cleanup(scene)
% CLEANUP Cleans up after the whole simulatoin has been done. Might
% not be a useful function, but created just in case. Any post
% processing should be done here
    success = true;
end