function success = Step(scene)
    h = scene.dt;
    scene.t = scene.t + h;
    % Construct the mass matrix
    scene.MakeMassMatrix();
    % Construct Jacobians
    scene.MakeJRL();
    scene.MakeJR();

    C    = scene.GetConstCompl();
    Cphi = scene.GetPosDrift();

    % Rigid bodies Coriolis force
    for rigid = scene.rigids
        ir = rigid.ind;
        phi = rigid.v;
        % force
        coriolis = rigid.M * phi;
        coriolis(1:3) = cross(phi(1:3), coriolis(1:3));
        coriolis(4:6) = cross(phi(1:3), coriolis(4:6));
        scene.f(ir) = scene.f(ir) - coriolis;
    end

    % Multiply the matrices to compute a tall and skinny matrix
    scene.MakeConstraints();
    G = scene.G;
    % FIXME g should come from @scene
    g = zeros(size(scene.G,2), 1);
    J = scene.JRL*scene.JR;

    Mv = (scene.M * scene.v);
    hf = (h * scene.f);
    rhs = J' * (Mv + hf);
    % A = scene.AdjMat;
    % DJ = scene.JDampMat;
    % dmat = A' * DJ * A;
    % size(dmat)
    % size(scene.D)
    %     Mtilde = scene.M + h*scene.D + h*dmat - h*h* scene.K;
    Mtilde = scene.M + h*scene.D + - h*h* scene.K;

    Meff = J' * Mtilde * J;
    if scene.mgi
        [success, scene.v_R, scene.lambda] = ...
            QuadSolve(Meff, rhs, G, g, scene.Gi(1:scene.mgi, :)*J, scene.gi(1:scene.mgi)); 
    else
        [success, scene.v_R, scene.lambda] = LinSolve(Meff, rhs, G, g, scene.n_R); 
    end
    % Update velocities
    scene.v_RL = scene.JR  * scene.v_R;
    scene.v    = scene.JRL * scene.v_RL;

    %CustomPrint('Mass Matrix', full(scene.M), scene.debug);
    % CustomPrint('JR', full(scene.JR), scene.debug);
    % CustomPrint('JRL', full(scene.JRL), scene.debug);
    %CustomPrint('f', full(scene.f), scene.debug);
    %CustomPrint('v', full(scene.v), scene.debug);

    scene.WriteVelocity();
    % TODO Write position update function. Should work with dofs.
    scene.UpdatePosition(h);

    % Confirm position
    %             if ~isequal(scene.dofs,[])
    %                 d= scene.dofs(3);
    %                 r= d.rigidBody;
    %                 assert(isequal([d.node.x; 1],r.E*[d.l; 1]));
    %             end

    % FIXME Post stabilization not implemented yet.
    scene.PostStabilize();
    % CustomPrint('Rigid Position',transpose(scene.rigids(2).x), scene.debug);
    % CustomPrint('Rigid Matrix',full(scene.rigids(2).E), scene.debug);
end
