function [phib] = rodrigues (phi)

h = 1.0;
I = eye(3);
w = phi(1:3);
v = phi(4:6);
wlen = norm(w);
if wlen < 1e-10
    phib = [I, h*v; 0 0 0 1];
else
    w = w / wlen;
    v = v / wlen;
    % Rodrigues formula %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    wX = w(1);
    wY = w(2);
    wZ = w(3);
    c = cos(wlen * h);
    s = sin(wlen * h);
    c1 = 1 - c;
    R = [
        c + wX * wX * c1, -wZ * s + wX * wY * c1, wY * s + wX * wZ * c1;
        wZ * s + wX * wY * c1, c + wY * wY * c1, -wX * s + wY * wZ * c1;
        -wY * s + wX * wZ * c1, wX * s + wY * wZ * c1, c + wZ * wZ * c1];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    A = I - R;
    cc = cross(w, v);
    d = A * cc;
    wv = w' * v;
    p = (wv * wlen * h) * w + d;
    phib = [R p; 0 0 0 1];
end
return 


endfunction
