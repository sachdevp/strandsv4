function [phi, alt_phi] = transform_to_euler(E) 
% psi, theta, phi
    if size(E, 2) == 16
        E = reshape(E, [4, 4])
    end
    R = E(1:3, 1:3);
    phi(4:6) = E(1:3, 1);
    alt_phi(4:6) = E(1:3, 1);
    if (R(3, 1) ~= 1 && R(3, 1) ~= -1)
        phi(2) = -asin(E(3,1));
        alt_phi(2) = pi - phi(1);
        c1 = cos(phi(2));
        c2 = cos(alt_phi(2));
        phi(1) = atan2(R(3, 2)/c1, R(3, 3)/c1);
        alt_phi(1) = atan2(R(3, 2)/c2, R(3, 3)/c2);
        phi(3) = atan2(R(2, 1)/c1, R(1, 1)/c1);
        alt_phi(3) = atan2(R(2, 1)/c2, R(1, 1)/c2);
    else
        phi(3) = 0;
        if(R(3, 1) == -1)
            phi(2) = pi/2;
            phi(1) = phi(3) + atan2(R(1, 2), R(1, 3));
        else
            phi(2) = -pi/2;
            phi(1) = -phi(3) + atan2(R(1, 2), R(1, 3));
        end
    end
    phi(1:3)=phi(1:3)*180.0/pi;
    alt_phi(1:3)=alt_phi(1:3)*180.0/pi;
end