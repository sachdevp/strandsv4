%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Notes:                                                                  %
% 1. node is assumed to have the following attributes: x,s.               %
% 2. node's ix and is are given as an index in strand.Lv and strand.Ev    %
% respectively. node's LInd and EInd are set                              %
% 3. Appended mass matrices are maintained and therefore, no assumptions  %
% should be made about the sizes. Use designated functions such as getnE, %
% getnL and getMLL etc.                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef Strand < handle
% STRAND class
    properties
        MLL
        MEE
        MLE
        nodes
        JQ
        fx
        fs
        LInd
        EInd
        Lv
        Ev
        rho = 1e2*(0.01*0.01*pi);
        ended = false;
        stiffness = 1e3;
        initStrain = 0;
        bInex = false;
    end
    methods
        function node = AddNode(strand, x)
        % ADDNODE Adds node to the strand
        % When this function is called a corresponding dof must be created
        % @x Position of the node
        % @nodeType 'full', 'circle', or 'fixed'
            assert(~strand.ended);
            node = Node();
            node.x = x;
            node.s = 0;      % Will be computed once the strand is setup
            strand.nodes = [strand.nodes node];
            node.ix      = length(strand.Lv)+(1:3);
            strand.Lv    = [strand.Lv 0 0 0];
            strand.Ev    = [strand.Ev 0];
            node.is      = length(strand.Ev);
        end
        function success = EndStrand(strand)
        % ENDSTRAND Called at the end of the strand to add any post
        % processing and to note that the strand nodes have been added and
        % other steps may now be performed. No nodes may be added after
        % ending the strand
            success = true;
            strand.ended = true;
        end
        function setLInd(strand, LInd) 
        % Function likely not to be used because node will not need it's LInd
        % or EInd
            assert(strand.ended);
            strand.LInd = LInd;
            for ii = 1:length(strand.nodes)
                strand.nodes(ii).LInd = LInd(3*(ii-1)+(1:3));
            end
        end
        function setEInd(strand, EInd)
        % Function likely not to be used because node will not need it's LInd
        % or EInd
            assert(strand.ended);
            strand.EInd = EInd;
            strand.nodes(1).EInd = -1;
            for ii = 2:length(strand.nodes)-1
                strand.nodes(ii).EInd = EInd(ii-1);
            end
            strand.nodes(end).EInd = -1;
        end
        function success = InitMassMatrices(strand)
            assert(strand.ended);
            nL = length(strand.Lv);
            nE = length(strand.Ev);
            strand.MLL = sparse(nL,nL);
            strand.MLE = sparse(nL,nE);
            strand.MEE = sparse(nE,nE);
            strand.fx = zeros(nL,1);            
            strand.fs = zeros(nE,1);
            success = true;
        end
        function nS = getnSegs(strand)
            nS = length(strand.nodes)-1;
        end
        function nL = getnL(strand)
            nL = length(strand.Lv);
        end
        function nE = getnE(strand)
            nE = length(strand.Ev) - 2;
        end
        function fx = getfx(strand)
            fx = strand.fx;
        end
        function fs = getfs(strand)
            fs = strand.fs(2:end-1);
        end
        function setEv(strand, Ev)
            strand.Ev(2:end-1) = Ev;
        end
        function setLv(strand, Lv)
            strand.Lv = Lv;
        end
        function Init(strand)
            assert(strand.ended);
            strand.ComputeInitS();
        end
        function ComputeInitS(strand)
        % COMPUTEINITS Compute the initial Eulerian co-ordinates for the
        % strand based on initial location of nodes. 
            strand.nodes(1).s = 0;
            s = 0;
            for i=2:length(strand.nodes)
                l = norm(strand.nodes(i).x - strand.nodes(i-1).x);
                s = l/(1+strand.initStrain) + s;
                strand.nodes(i).s = s;
            end
        end
        function ComputeMass(strand, grav)
            strand.MLL = 0*strand.MLL;
            strand.MLE = 0*strand.MLE;
            strand.MEE = 0*strand.MEE;
            strand.fx = 0*strand.fx;
            strand.fs = 0*strand.fs;
            for i = 2 : length(strand.nodes)
                % Set all local variables
                node0 = strand.nodes(i-1);
                node1 = strand.nodes(i);
                x0 = node0.x;
                x1 = node1.x;
                s0 = node0.s;
                s1 = node1.s;
                ix0 = node0.ix;
                ix1 = node1.ix;
                is0 = node0.is;
                is1 = node1.is;
                dx = x1 - x0;
                ds = s1 - s0;
                % Mass
                mxx = ds*eye(length(ix0));
                mxs = dx;
                mss = (dx'*dx)/ds;
                ix = [ix0 ix1];
                is = [is0 is1];
                strand.MLL(ix, ix) = strand.MLL(ix, ix) + ...
                    2*strand.rho/6*[ 2*mxx mxx; mxx 2*mxx];
                strand.MLE(ix, is) = strand.MLE(ix, is) + ...
                    2*strand.rho/6*[-2*mxs -mxs; -mxs -2*mxs];
                % NOTE: Transpose of blocks is the same as transpose of the
                % whole matrix
                strand.MEE(is, is) = strand.MEE(is, is) + ...
                    2*strand.rho/6*[2*mss mss; mss 2*mss];
                % Gravity
                x01 = x0 + x1;
                strand.fx(ix) = strand.fx(ix) + 0.5*strand.rho*[ds*grav;ds*grav];
                strand.fs(is) = strand.fs(is) + 0.5*strand.rho*[-x01'*grav;x01'*grav];
                % Elasticity -- Explicit
                l = norm(dx);
                strain = l/ds - 1;
                if strain < 0
                    strain = 0;
                end
                fex            = strand.stiffness*strain*dx/l;
                strand.fx(ix0) = strand.fx(ix0) + fex;
                strand.fx(ix1) = strand.fx(ix1) - fex;
                fes            = strand.stiffness/2*strain*(strain+2);
                strand.fs(is0) = strand.fs(is0) - fes;
                strand.fs(is1) = strand.fs(is1) + fes; 
                
            end
        end
        function ComputeJQ(strand)
        % COMPUTEJQ Create JQ matrix 
        % Based on builQ function from qnodes.m
        % JQ = strand.ComputeJQ() computes JQ for strand
            nsegs = strand.getnSegs();
            L = zeros(nsegs - 1, nsegs - 1);
            S = zeros(nsegs - 1, nsegs); 
            % CHECK this. Size might be incorrect
            X = zeros(nsegs, 3*(nsegs + 1)); 
            ls = zeros(1, nsegs);
            dss = zeros(1, nsegs);
            % CHECK this. Size might be incorrect
            dxs = zeros(3, nsegs);
            % Compute l, ds, dx for each segment
            for i = 1 : nsegs
                node0 = strand.nodes(i);
                node1 = strand.nodes(i+1);
                x0 = node0.x;
                x1 = node1.x;
                s0 = node0.s;
                s1 = node1.s;
                dx = x1 - x0;
                ds = s1 - s0;
                l = norm(dx);
                ls(i) = l;
                dss(i) = ds;
                dxs(:,i) = dx/l;
            end
            % L
            for i = 1 : nsegs
                li = ls(i);
                if i < nsegs-1
                    L(i,i+1) = -li;
                end
                if i < nsegs
                    L(i,i) = L(i,i) + li;
                end
                if i > 1
                    L(i-1,i-1) = L(i-1,i-1) + li;
                end
                if i > 2
                    L(i-1,i-2) = -li;
                end
            end
            % S
            for i = 1 : nsegs
                % Check corner cases
                if i < nsegs
                    S(i,i+1) = dss(i);
                end
                if i > 1
                    S(i-1,i-1) = -dss(i);
                end
            end
            % X
            % Columns corresponding to the Eulerian DoFs will be 0
            for i = 1 : nsegs
                i0 = 3*(i-1)+(1:3); % index of left Lagrangian DoF -- Could use node properties (Fixed ordering because of this line)
                i1 = 3*i+(1:3); % index of right Lagrangian DoF -- Could use node properties
                X(i,i0) = -dxs(:,i)';
                X(i,i1) =  dxs(:,i)';
            end
            strand.JQ = -L\(S*X);
        end
        function MLL = getMLL(strand)
            MLL = strand.MLL;
            return;
        end
        function MLE = getMLE(strand)
        % CHECK Size
            [rows, cols] = size(strand.MLE);
            MLE = strand.MLE(1:(rows), 2:(cols-1));
            return;
        end
        function MEE = getMEE(strand)
        % CHECK Size
            [rows, cols] = size(strand.MEE);
            MEE = strand.MEE(2:(rows-1), 2:(cols-1));
            return;
        end
        function UpdateNodes(strand,h)
        % TODO Update velocity and position of nodes
        % Forms last step of the scene. Called from Scene.m
        % NOTE Updated to not change s value for end points.
            for node=strand.nodes(2:end-1)
                %node.x = node.x + strand.Lv(node.ix)*h;
                if node.is > 0          % Check id Eulerian Node
                    node.s = node.s + strand.Ev(node.is)*h;
                end
            end
        end
        function [Gxi, Gsi, gi]  = getInexCons(strand)
            nNodes = length(strand.nodes);
            nSegs = strand.getnSegs();
            Gxi = zeros(nSegs, nNodes*3);
            Gsi = zeros(nSegs, nNodes);
            gi = zeros(nSegs, 1);
            for iSeg = 1:nSegs
                n0 = strand.nodes(iSeg);
                n1 = strand.nodes(iSeg+1);
                dx = n1.x-n0.x;
                ds = n1.s-n0.s;

                % Fill Gxi
                ix = (3*iSeg-2):(3*iSeg+3);
                Gxi(iSeg, ix) = 2*[-dx', dx'];
               
                % Fill Gsi
                is = iSeg:(iSeg+1);
                Gsi(iSeg, is) = 2*[ds, -ds];

                % Fill Gi
                gi(iSeg) = 0;
            end
        end
        function bTaut = isTaut(strand)
            l = 0;
            bTaut = false;
            nSegs = strand.getnSegs();
            for iSeg = 1:nSegs
                l = l + norm(strand.nodes(iSeg+1).x - strand.nodes(iSeg).x);
            end
            if l>(strand.nodes(end).s - strand.nodes(1).s - 1e-5)
                bTaut = true;
            end
        end
        function bInex = isInex(strand)
            bInex = strand.bInex;
        end
    end
end