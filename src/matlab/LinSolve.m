function [success, v_R, lambda] = LinSolve(M, rhs, G, g, n_R)
% Solving optimization with equality constraints using a KKT system.

    % Run least squares method
    mg = size(G, 2);
    KKTM = [M, G; G' zeros(mg, mg)];
    %CustomPrint('KKT Matrix',full(MG(7:end, 7:end)), scene.debug);
    % scene.g should be used but only for velocity level
    % constraints. Joints are all zero without drift. Hence scene.g
    % can be replaced by Cphi.
    % rhs = [rhs; -Cphi/h];
    KKTrhs = [rhs; g];
    vp     = KKTM\KKTrhs;
    v_R    = vp(1:n_R);
    lambda = vp(n_R:end);
    success = true;
    %             vp = Meff\rhs;
    %             Mi = inv(Meff);
    %             G = scene.G;
    %             l = -(G'*Mi*G)\(G'*vp);
    %             vl = Mi*G*l;
    %
    %             scene.v_R = vp + vl;
end