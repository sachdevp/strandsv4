function [success, v_R, lambda] = QuadSolve(M, rhs, G, g, Gi, gi)
% Solving optimization with equality constraints and inequality
% constraints using quadprog.
% Run least squares method
    lb = [];
    ub = [];
    x0 = [];
    options = optimset('Display', 'off');
    [v_R, fval, exitflag, output, lambda] = ...
        quadprog(M, -rhs, Gi, gi, G', g, lb, ub, x0, options);
    success=true;
%CustomPrint('KKT Matrix',full(MG(7:end, 7:end)), scene.debug);
% scene.g should be used but only for velocity level
% constraints. Joints are all zero without drift. Hence scene.g
% can be replaced by Cphi.
% rhs = [rhs; -Cphi/h];
% KKTrhs = [rhs; g];
% vp     = KKTM\KKTrhs;
% v_R    = vp(1:n_R);
% lambda = vp(n_R:end);
%             vp = Meff\rhs;
%             Mi = inv(Meff);
%             G = scene.G;
%             l = -(G'*Mi*G)\(G'*vp);
%             vl = Mi*G*l;
%
%             scene.v_R = vp + vl;
end