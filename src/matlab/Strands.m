% STRANDS Script to drive a scene using Scene.m
function Strands()
% First sets up the scene and then runs it and calls draw on the scene when
% needed.
    %scene = SceneMultiJoint(3);
    % scene = SceneSingleJoint();
    scene = SceneStrandRigidMoved();
    scene.SetExport(false, '');
    scene.SetDebug(true);
    scene.SetTimestep(1e-2);
    dataDir = './Data';
    drawHz = 60;
    nTimesteps = 3000;
    RunScene(scene, drawHz, nTimesteps);
end

% TODO This scene fails and needs fixing.
function scene = SceneStrandRigidRotated()
    % Initialize scene
    scene = Scene();
    % Test adding rigid bodies
    scene.SetGravity([0 0 -9.8]');
    rho = 1;
    rigid1 = scene.AddRigid([0 0 0 -1.0 0 0]', rho, [1 1 1]', true, [0 0 0 0 0 0]');
    rigid2 = scene.AddRigid([0 0 0  3.5 0 0]', rho, [1 1 1]');
    E = [eye(3), [0 0 0]'; 0 0 0 1];
    strand1 = scene.AddStrand();
    strand1.initStrain = 0.00;
    scene.AddNode(1, 'fixed', [0 0 0]', [-1 -1 -1]', rigid1);
    scene.AddNode(1, 'fixed', [3 0 0]', [-1 -1 -1]', rigid2);
    strand1.EndStrand();
end

% TODO This scene fails and needs fixing.
function scene = SceneStrandRigidMoved()
    % Initialize scene
    scene = Scene();
    % Test adding rigid bodies
    scene.SetGravity([0 0 -9.79]');
    rho = 1;
    rigid1 = scene.AddRigid([0 0 0 0 0 -2]', rho, [1 1 1]', true, [0 0 0 0 0 0]');
    rigid2 = scene.AddRigid([0 0 0 3.64 0 1]', rho, [1 1 1]');
    E = [eye(3), [0 0 0]'; 0 0 0 1];
    strand1 = scene.AddStrand();
    strand1.bInex = true;
    strand1.initStrain = 0.00;
    strand1.stiffness = 0.00;
    scene.AddNode(1, 'fixed', [0 0 1]', [-1 -1 -1]', rigid1);
    scene.AddNode(1, 'fixed', [1.52 0 1]', [-1 -1 -1]', rigid1);
    scene.AddNode(1, 'fixed', [3.04 0 1]', [-1 -1 -1]', rigid2);
    strand1.EndStrand();
end


function scene = SceneSingleJoint()
    % Initialize scene
    scene = Scene();
    % Test adding rigid bodies
    scene.SetGravity([0 0 -9.8]');
    rho = 1e1;
    rigid1 = scene.AddRigid([0 0 0 0 0 0]', rho, [1 1 1]', true, [0 0 0 0 0 0]');
    rigid2 = scene.AddRigid([0 0 0 1 0 0]', rho, [1 1 1]');
    rigid3 = scene.AddRigid([0 0 0 4 0 0]', rho, [1 1 1]');
    E = [eye(3), [0 0 0]'; 0 0 0 1];
    scene.AddJoint(rigid1, rigid2, E, [1 0 1 1 1 1]')
end

function scene = SceneMultiJoint(nJ)
    % Initialize scene
    scene = Scene();
    % Test adding rigid bodies
    scene.SetGravity([0 0 -9.8]');
    rho = 1e1;
    nR = nJ+1;
    rigids=[];
    rigid = scene.AddRigid([0 0 0 0 0 0]', rho, [1 1 1]', true, [0 0 0 0 0 0]');
    rigids = [rigids rigid];
    prevRigid = rigid;
    for i = 1:nJ
        rigid  = scene.AddRigid([0 0 0 i 0 0]', rho, [1 1 1]');
        rigids = [rigids rigid];
        E = [eye(3), [i-0.5 0 0]'; 0 0 0 1];
        scene.AddJoint(prevRigid, rigid, E, [1 0 1 1 1 1]')
        prevRigid = rigid;
    end
    % rigid1 = scene.AddRigid([0 0 0 0 0 0]', rho, [1 1 1]', true, [0 0 0 0 0 0]');
    % rigid2 = scene.AddRigid([0 0 0 1 0 0]', rho, [1 1 1]');
    % E = [eye(3), [0 0 0]'; 0 0 0 1];
    % scene.AddJoint(rigid1, rigid2, E, [1 0 1 1 1 1]')
end

function RunScene(scene, drawHz, nTimesteps)
% Set export directory if export is intended
    scene.Init();
    for i=1:nTimesteps
        % Draw the scene if needed
        if ((drawHz ~= 0) && ((scene.t == 0) || ...
            ((scene.t - scene.tDrawLast) > 1/drawHz)))
            scene.Draw();
            % Check if exporting
            if scene.export
                scene.PrintImage();
            end
        end
        scene.Step();
    end
    scene.CleanUp();
end
