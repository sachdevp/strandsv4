classdef Rigid < handle
% RIGID Defines a handle class for rigid to allow reference passing. Important to get
% pointer-like bhavior.
    properties
        M = zeros(6)                    % Mass
        whd                             % [whd]'
        x                               % Position
        v                               % phi
        scripted = false                % Is it scripted?
        ind = [-1 -1 -1 -1 -1 -1]       % Indices in the mass matrix
        E
    end
    methods
        function success = Init(rigid, density)
            mass = density * prod(rigid.whd); % translational mass
            rigid.M(1, 1) = (1/12) * mass * rigid.whd([2,3])' * rigid.whd([2,3]);
            rigid.M(2, 2) = (1/12) * mass * rigid.whd([3,1])' * rigid.whd([3,1]);
            rigid.M(3, 3) = (1/12) * mass * rigid.whd([1,2])' * rigid.whd([1,2]);
            rigid.M(4, 4) = mass;
            rigid.M(5, 5) = mass;
            rigid.M(6, 6) = mass;
            % Initialize E
            rigid.E = rigidlib.integrate(eye(4,4), rigid.x, 1);
        end
        function success = UpdatePosition(rigid, h)
            rigid.x = rigid.x + h*rigid.v;
            rigid.E = rigidlib.integrate(rigid.E, rigid.v, h);
            %rigid.E = rigidlib.integrate(eye(4), rigid.x, 1);
            success = true;
        end
        function success = Draw(rigid)
            hsize = rigid.whd/2;
            verts = ones(4,8);
            verts(1:3,1) = [-hsize(1), -hsize(2), -hsize(3)]';
            verts(1:3,2) = [ hsize(1), -hsize(2), -hsize(3)]';
            verts(1:3,3) = [ hsize(1),  hsize(2), -hsize(3)]';
            verts(1:3,4) = [-hsize(1),  hsize(2), -hsize(3)]';
            verts(1:3,5) = [-hsize(1), -hsize(2),  hsize(3)]';
            verts(1:3,6) = [ hsize(1), -hsize(2),  hsize(3)]';
            verts(1:3,7) = [ hsize(1),  hsize(2),  hsize(3)]';
            verts(1:3,8) = [-hsize(1),  hsize(2),  hsize(3)]';
            lines{1} = verts(:,[1,2,3,4,1]);
            lines{2} = verts(:,[5,6,7,8,5]);
            lines{3} = verts(:,[1,5]);
            lines{4} = verts(:,[2,6]);
            lines{5} = verts(:,[3,7]);
            lines{6} = verts(:,[4,8]);
            for j = 1 : length(lines)
                line = lines{j};
                for k = 1 : size(line,2)
                    line(:,k) = rigid.E * line(:,k);
                end
                plot3(line(1,:), line(2,:), line(3,:), 'g');
            end
            success = true;
        end
    end
end