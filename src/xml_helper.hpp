/**
   @author: sachdevp
   @desc:   Header for XML help functions
*/
#pragma once
#include <include_common.hpp>
#include <pugixml.hpp>
#include <Eigen/Eigen>

// Note that column major matrix storage is assumed everywhere.
using namespace pugi;

namespace XML{

   //// String constants. Defined in xml_helper.cpp
   // Common properties
   extern const char* strId;
   extern const char* strName;
   extern const char* strParams;
   extern const char* strGravity;
   extern const char* strTransform;

   // For points
   extern const char* strPoints;
   extern const char* strPoint;
   extern const char* strPointType;
   extern const char* strGlobalPos;

   // For rigids
   extern const char* strRigids;
   extern const char* strRigid;

   // For joints
   extern const char* strJoints;
   extern const char* strJoint;
   extern const char* strRigid1;
   extern const char* strRigid2;
   extern const char* strConstrained;
   // For strands
   extern const char* strStrands;
   extern const char* strStrand;
   extern const char* strNode;
   extern const char* strObject;

   // For nodes
   extern const char* strNodeType;
   extern const char* strLorQ;
   extern const char* strNodeFixed;
   extern const char* strNodeFull;
   extern const char* strNodeCurve;

   // For Strand
   extern const char* strRadius;
   extern const char* strDensity;
   extern const char* strNodes;

   template<class T>
   T getChildVar(const xml_node& parent, const char* child){
      std::string str(parent.child_value(child));
      if(str.compare("") == 0){
         std::cerr << child << " not found in " << parent.name() << std::endl;
      }
      std::istringstream iBuffer(str);
      T val;
      iBuffer >> val;
      return val;
   }

   template<class T>
   RET getChildVarArr(T* arr, int n, const xml_node& parent, const char* child){
      std::string str(parent.child_value(child));
      if(str.compare("") == 0){
         std::cerr << child << " not found in " << parent.name() << std::endl;
      }
      std::istringstream iBuffer(str);
      for(int i = 0; i < n; i++){
         if(iBuffer.good()) iBuffer >> arr[i];
         else return RET::Failure;
      }
      return RET::Success;
   }


   template<class T>
   std::vector<T> getVarArr(const std::string& str){
      std::vector<T> arr;
      std::istringstream iBuffer(str);
      int i = 0;
      while(!iBuffer.eof()){
         T val;
         iBuffer >> val;
         arr.push_back(val);
         i++;
      }
      return arr;
   }

   void dataToEigenMatrix(Eigen::Ref<Eigen::Matrix4t> x);
   RET getMatrix(Eigen::MatrixXt& matrix, const xml_node& parent, const char* child, int m, int n);
   RET getMatrix4t(Eigen::Matrix4t& matrix, const xml_node& parent, const char* child);    // Special case of getMatrix
   RET getVector(Eigen::VectorXt& v, uint8_t sz, const xml_node& parent, const char* child);
   RET getVector3t(Eigen::Vector3t& v, const xml_node& parent, const char* child);
}
