/**
   @author: sachdevp
   @desc:   Generic object class which is derived for all the scene objects.
*/
#pragma once
#include <include_common.hpp>
#include <BaseObject.hpp>

class Object: public BaseObject{
protected:
   uint gid;
   Eigen::Matrix4t E;
   bool dirty;
   uint nDoF;                   // Number of physical degrees of freedom
   bool bAddedToScene, bRecord;

private:
   // Clear the object. Not yet initialized
   void clear();

public:
   // CHECK the use of dirty
   Object();
   Object(uint _gid, std::string _name, bool _dirty = true);
   // Set name and ID for object
   RET setInfo(uint _gid, std::string _name = "");
   // Set object as added to scene
   RET addedToScene();
   // Overriding BaseObject's Init
   RET Init();
   void setRecord(bool);
   bool toBeRecorded();

   /** Query functions **/
   // Is the object added to scene?
   RET isAddedToScene();

   /** Get functions **/
   uint getId(){return gid;}
   uint8_t getnDoF() const {return nDoF;}
   virtual Eigen::Matrix4t getTransform(){return E;}
   virtual RET clearForStep(ftype t){return R::Failure;};

   ftype setCoordinates(const Eigen::VectorXt& v);
};
