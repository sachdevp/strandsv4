import matplotlib.pylab as plt
import scipy.sparse as sps
import re
import numpy as np
import os

filename = '/home/sachdevp/Dropbox/SSL/AIST/New Tests/test.txt.d/mat1002.m'
outFilename = '/home/sachdevp/tmp/mat1002.m.proc'

try:
    os.remove(outFilename)
except OSError:
    pass
with open(filename) as inFile, open(outFilename, 'w+') as outFile:
    started = False
    matName = ''
    matrices = {}
    print filename, outFilename
    for line in inFile:
        if line.endswith('... \n'):
            matList = []
            matName = line.split('=')[0]
        if not started and line.startswith('['):
            row = line[1:-2]
            matList.append([float(x) for x in row.split(',')])
            started = True
        elif started:
            row = line[:-2]
            matList.append([float(x) for x in row.split(',')])
        if line.endswith(']\n'):
            print matName
            matrices[matName] = np.array(matList)
            M = sps.csr_matrix(matrices[matName])
            plt.spy(M)
            plt.grid(b=True, which='major', color='b', linestyle='-')
            plt.grid(b=True, which='minor', color='r', linestyle='--')
            plt.show()
            started = False
