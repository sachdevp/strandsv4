#include <DynObj/Rigid.hpp>
#include <EigenHelper.hpp>
#include <SE3.hpp>
#include <JointDamping.hpp>
using namespace std;
using namespace Eigen;
using namespace Strands;

#define CHECK_WRENCH_INITIALIZED(str) CHECK_INITIALIZED(str, JointDamping)
#define CHECK_WRENCH_NOT_INITIALIZED(str) CHECK_NOT_INITIALIZED(str, JointDamping)

JointDamping::JointDamping(SP<DynObj> ra, SP<DynObj> rb): Force(){
   mDynObjs.push_back(ra);
   mDynObjs.push_back(rb);
   E  = ra->getTransform();
   E0 = ra->getTransform();
   E1 = rb->getTransform();
   nBlocks = 2;
}

R JointDamping::Init() {
  CHECK_WRENCH_NOT_INITIALIZED("Already initialized.");
  bInit = true;
  mTorqueVec.setZero();
  return R::Success;
}

R JointDamping::fillForce() {
  CHECK_WRENCH_INITIALIZED("Cannot compute force.");

  // Set the force
  V3 localWrenchVec;
  // Apply positive torue on one, and negative on another.
  M3 rotE = SE3::getRotation(E0);
  localWrenchVec = rotE.transpose() * mTorqueVec;
  vForceBlocks[0] += localWrenchVec;
  rotE = SE3::getRotation(E1);
  localWrenchVec = rotE.transpose() * mTorqueVec;
  vForceBlocks[1] -= localWrenchVec;
  return R::Success;
}

R JointDamping::getIndices(std::vector<int> &vIdx, std::vector<int> &vLen) {
   Force::getIndices(vIdx, vLen);
   // Only setting torques
   vIdx.push_back(mDynObjs[0]->getIndex());
   vLen.push_back(3);
   vIdx.push_back(mDynObjs[1]->getIndex());
   vLen.push_back(3);
   return R::Success;
}

// Update the transform for the force. Note that the @Object transform matrix,
// @E, is in GLOBAL space, not local.
RET JointDamping::update() {
   CHECK_WRENCH_INITIALIZED("Cannot update.");
   E = mDynObjs[0]->getTransform();
   E0 = mDynObjs[0]->getTransform();
   E1 = mDynObjs[1]->getTransform();
   return R::Success;
}

RET JointDamping::clearForStep(ftype t){
   // V6 prevWrench;
   // prevWrench << 0,0,0,0,0,0;
   // for(pair<double, V6> wrenchPair: mWrenchMap){
   //    if(wrenchPair.first>t){break;}
   //    prevWrench = wrenchPair.second;
   // }
   // cout<<"Updating wrench: "<<t<<" "<<MS_T(mWrenchVec)<<endl;
   // mWrenchVec = prevWrench;
   return R::Success;
}

RET JointDamping::updateTorque(V3 _torque){
   mTorqueVec = _torque;
   return R::Success;
}
