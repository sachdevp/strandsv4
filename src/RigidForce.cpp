#include <RigidForce.hpp>
#include <DynObj/Rigid.hpp>
#include <SE3.hpp>
#include <DynObj/PointL.hpp>
#include <EigenHelper.hpp>
using namespace std;
using namespace Eigen;

#define CHECK_RIGIDFORCE_INITIALIZED(str)     CHECK_INITIALIZED(str, RigidForce)
#define CHECK_RIGIDFORCE_NOT_INITIALIZED(str) CHECK_NOT_INITIALIZED(str, RigidForce)

namespace Strands{
   RigidForce::RigidForce(SP<DynObj> _dynObj, ftype _mag, M4 _localE): Force(), mag(_mag), localE(_localE){
      mDynObjs.push_back(_dynObj);
      // NOTE Rigid assumed to be given in 6 DoFs
      auto dynObj = mDynObjs[0];
   }

   R RigidForce::Init(){
      CHECK_RIGIDFORCE_NOT_INITIALIZED("Already initialized.");
      auto dObj  = mDynObjs[0];
      auto rigid = dynamic_pointer_cast<Rigid>(dObj);
      auto point = dynamic_pointer_cast<PointL>(dObj);
      RCHECKERROR_B(!rigid && !point, "Force cannot be initialized. Cannot confirm Point or Rigid.");
      bInit = true;
      return R::Success;
   }

   R RigidForce::fillForce(){
      CHECK_RIGIDFORCE_INITIALIZED("Cannot compute force.");

      V3 x(mag, 0.0, 0.0);
      GET_Rp(rotE, p, localE);

      // Set the force
      // NOTE If point, the force gets overwritten by second statement.
      vForceBlocks[0].head<3>() += SE3::computeBracket(p)*rotE*x;
      vForceBlocks[0].tail<3>() += rotE*x;

      return R::Success;
   }

   R RigidForce::getIndices(std::vector<int> &vIdx, std::vector<int> &vLen){
      CHECK_RIGIDFORCE_INITIALIZED("Cannot get indices.");
      Force::getIndices(vIdx, vLen);
      vIdx.push_back(mDynObjs[0]->getIndex());
      vLen.push_back(mDynObjs[0]->getMatrixSize());
      return R::Success;
   }

   // Update the transform for the force. Note that the @Object transform matrix,
   // @E, is in GLOBAL space, not local.
   RET RigidForce::update(){
      CHECK_RIGIDFORCE_INITIALIZED("Cannot update.");
      E = localE*mDynObjs[0]->getTransform();
      return R::Success;
   }
}
