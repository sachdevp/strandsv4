#include <include_common.hpp>
#include <DynObj/ESegment.hpp>
#include <Segment.hpp>
#include <StrandsHelper.hpp>
#include <Node/Node.hpp>
#include <DynObj/PointL.hpp>
#include <vector>
#include <Joint.hpp>
#include <SE3.hpp>
#include <DoF/DoF.hpp>
#include <DoF/DoFFixed.hpp>
#include <EigenHelper.hpp>
#include <ctime>

using namespace Strands;
using namespace std;
ESegment::ESegment(){
   oSeg = nullptr;
   bOsegSet = false;
   rigidJoint = nullptr;
}

RET ESegment::pushNode0(Node* node0, SP<PointL> point){
   if(vNodes.size()!=0) return R::Failure;
   vNodes.push_back(node0);
   vPoints.push_back(point);
   return R::Success;
}

#define SET_FREE(cnst, axis) cnst[static_cast<int>(axis)] = 0;
RET ESegment::pushNodeSeg(Node* node, SP<PointL> point, Segment* seg){
   if(vNodes.size() == 0) return R::Failure;
   vNodes.push_back(node);
   vPoints.push_back(point);
   vSegs.push_back(seg);
   return R::Success;
}

RET ESegment::setJoint(SP<Strands::Joint> _joint){
   if(rigidJoint!=nullptr) return R::Failure;
   rigidJoint = _joint;
   return R::Success;
}

RET ESegment::recompute(VECV3 newPos){
   // Project points to new location
   auto dof1 = vPoints[1]->getDoFFixed();
   auto dof2 = vPoints[2]->getDoFFixed();
   RCHECK_NOTNULL(dof1);
   RCHECK_NOTNULL(dof2);
   // NOTE Don't use setx. Fixed DoFs used here
   RCHECKRET(dof1->updateOffset(newPos[0]));
   RCHECKRET(dof2->updateOffset(newPos[1]));
   // RCHECKRET(dof1->setTargetVelocity());
   // RCHECKRET(dof2->setTargetVelocity());
   RCHECKRET(dof1->setTargetCoordinates());
   RCHECKRET(dof2->setTargetCoordinates());
   return R::Success;
}

RET ESegment::setElementsInActive(){
   for(auto seg :vSegs)  seg->setInActive();
   for(auto cons:vCons)  cons->setInActive();
   for(auto node:vNodes) node->setInActive();
   return R::Success;
}

RET ESegment::setElementsActive(){
   for(auto seg :vSegs)  seg->setActive();
   for(auto cons:vCons)  cons->setActive();
   for(auto node:vNodes) node->setActive();
   return R::Success;
}

RET ESegment::setOrigActive(){
   if(!bOsegSet) return R::Failure;
   vNodes.front()->setActive();
   vNodes.back()->setActive();
   oSeg->setActive();
   return R::Success;
}

// Get endnodes' positions with all the necessary checks.
RET ESegment::getEndNodesPos(VECV3 &retVec){
   Node *n0, *n1;
   RCHECKRET(getEndNodes(n0, n1));
   retVec.push_back(n0->getx());
   retVec.push_back(n1->getx());
   return R::Success;
}

#define SQR_NORM2(p0, p1) ((p0[0]-p1[0])*(p0[0]-p1[0]) + (p0[1]-p1[1])*(p0[1]-p1[1]))
#define NORM2(p0, p1) sqrt(SQR_NORM2(p0, p1))

#define PROJECT_JOINT(x) (jointRot.transpose() * (x)).head<2>()
R jointClosestVec(V2 &ret, V3 jointPos, M3 jointRot, VECV3 points){
   V2 tPoints[2];
   tPoints[0] = PROJECT_JOINT(points[0]);
   tPoints[1] = PROJECT_JOINT(points[1]);
   // CHECK Probably not needed.
   V2 tJPos   = PROJECT_JOINT(jointPos);
   V2 P0P1    = (tPoints[1] - tPoints[0]);
   ftype l    = P0P1.norm() ;
   V2 P0P1dir = P0P1/l;
   V2 P0M     = ((tJPos - tPoints[0]).dot(P0P1dir))*P0P1dir;
   V2 proj    = tPoints[0] + P0M;
   ret = (proj - tJPos);
   return R::Success;
}

ftype jointDist(V2 &ret, V3 jointPos, M3 jointRot, VECV3 points){
   jointClosestVec(ret, jointPos, jointRot, points);
   return ret.norm() ;
}

RET ESegment::checkActive(bool &active){
   // If needs to be active, set them so (and the nodes). If not, set them inactive.
   if(!bOsegSet) return R::Failure;
   if(EQUALS(rigidJoint->getRad(),0)){
      log_err("Radius zero.");
      return R::Failure;
   }
   VECV3 endNodesPos, newPos;
   RCHECKRET(getEndNodesPos(endNodesPos));
   V3 segs[4], segsProj[4];
   GET_Rp(jointRot, jointPos, rigidJoint->getTransform());

   V2 closestVec;
   jointClosestVec(closestVec, jointPos, jointRot, endNodesPos);
   ftype dotp = (mRelInitDir.dot(closestVec));
   V3 zAxis = jointRot.col(Z);
   Node *n0, *n1;
   this->getEndNodes(n0, n1);
   if((closestVec.norm() > (rigidJoint->getRad() - SEG_THRESH) // Outside the cylinder
      && dotp > 0)                                    // and on the right side
      // OR if either point is too far along the Z axis
      || fabs<ftype>((n0->getx()-jointPos).dot(zAxis))>Z_ESEG_THRESH
      || fabs<ftype>((n1->getx()-jointPos).dot(zAxis))>Z_ESEG_THRESH
      ){
      active = false;
      RCHECKRET(setElementsInActive());
      RCHECKRET(setOrigActive());
      return R::Success;
   }
   active = true;
   int n0Idx, n1Idx;
   n0->getNodeId(n0Idx);
   n1->getNodeId(n1Idx);
   if(RFAIL(getNewPointsPos(newPos, endNodesPos, rigidJoint, sDir, inOrder))){
      const char* jName = rigidJoint->getCName();
      log_err("Could not get new points around %s", jName);
      log_warn("Error details: \n"
               "End Nodes Id: %d, %d\n"
               "End Nodes Pos: %s, %s\n"
               "Node distance: %3.2f, %3.2f\n"
               "Joint location: %s\n"
               "Joint radius: %3.2f\n"
               "sDir, inOrder: %d, %d\n",
               n0Idx, n1Idx,
               MS_T(n0->getx()), MS_T(n1->getx()),
               (n0->getx()-jointPos).norm() , (n1->getx()-jointPos).norm() ,
               MS_T(jointPos),
               rigidJoint->getRad(),
               sDir, inOrder);
      return R::Failure;
   }
   

   // RCHECKERROR(getNewPointsPos(newPos, endNodesPos, rigidJoint, sDir, inOrder),
   //             "Could not get new points around %s.", rigidJoint->getCName());
   setElementsActive();
   RCHECKERROR_NOTNULL(oSeg, "oSeg is null.");
   oSeg->setInActive();
   RCHECKRET(this->recompute(newPos));
   return R::Success;
}

RET ESegment::setOSeg(Segment* _seg){
   // If either oSeg is already initialized or _seg not initialized, fail.
   if(_seg == nullptr || oSeg != nullptr) return R::Failure;
   oSeg = _seg;
   bOsegSet = true;
   return R::Success;
}

RET ESegment::getEndNodes(Node* &node0, Node* &node1){
#ifdef DEBUG
   if(vNodes.size() < 2) return R::Failure;
#endif
   node0 = vNodes.front();
   node1 = vNodes.back();
   return R::Success;
}

RET ESegment::getLengths(int &nNodes, int &nPoints, int &nSegs, int &nCons){
   nNodes  = vNodes.size();
   nPoints = vPoints.size();
   nSegs   = vSegs.size();
   nCons   = vCons.size();
   return R::Success;
}

void ESegment::popCons(){
// vCons.pop_back();
}

SPVEC<Joint> ESegment::getvCons(){return vCons;}

RET ESegment::setComplete(){
   M4 E = rigidJoint->getTransform();
   VECV3 endPos;
   RCHECKERROR(getEndNodesPos(endPos), "Could not get end pos.");
   sDir = sign(endPos[0], SE3::getTranslation(E), SE3::getRotation(E).col(X));
   log_verbose("sDir is %d", sDir);
   rigidJoint->checkRigids(vNodes.front()->getSourceDynObj(), vNodes.back()->getSourceDynObj(), &(this->inOrder));
   log_verbose("Esegment over %s in order: %d", rigidJoint->getCName(), inOrder);
   RCHECKERROR(setRelInitDir(), "Could not initialize dir.");
   return R::Success;
}

RET ESegment::setRelInitDir(){
   GET_Rp(jointRot, jointPos, rigidJoint->getTransform());
   VECV3 endNodesPos;
   RCHECKRET(getEndNodesPos(endNodesPos));
   jointClosestVec(mRelInitDir, jointPos, jointRot, endNodesPos);
   mRelInitDir.normalize();
   return R::Success;
}
