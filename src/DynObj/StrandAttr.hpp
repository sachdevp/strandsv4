#pragma once
#include <include_common.hpp>
#include <Material.hpp>

class SceneAttr;

class StrandAttr{
   friend class Strands::Strand;
public:
   StrandAttr();
   StrandAttr(ftype _radius, ftype _density);
   void setRadius(ftype r) {radius = r;};
   void setDensity(ftype d) {density = d;};
   ftype getRadius() const {return radius;};
   ftype getDensity() const {return density;};
   ftype getLinearDensity() const {return density*radius*radius*M_PI;};
   RET setMaterial(SP<Strands::Material> mat);

   // TODO Make private
   /////////////////////
   // Model variables //
   /////////////////////
   ftype radius;
   // Volumetric density
   ftype density;
   SceneAttr* sceneAttr;
   SP<Strands::Material> material;
};
