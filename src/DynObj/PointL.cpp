/**
   PointL.cpp
   @author: sachdevp
   @desc:   Lagrangian point needed for any L or Q node.
*/

#include <DynObj/PointL.hpp>
#include <DoF/DoF.hpp>
#include <DoF/DoFFixed.hpp>
#include <DoF/DoFCurve.hpp>
#include <Object.hpp>

using namespace std;
using namespace Eigen;

void PointL::clear(){
   x.setZero(3);
   v.setZero(3);
   this->dirty = true;
}

MatrixXt PointL::getJacobian(){return M2::Identity();}
RET PointL::Init(){return DynObj::Init();}
int PointL::getPointId(){return pointId;}
M4 PointL::getDoFE(){return dof->getCenterE();};
M4 PointL::getDoFEExt(){return dof->getCenterEExt();};

void PointL::setDoF(DoF* _dof){
   dof     = _dof;
   bDoFSet = true;
   _dof->setTargetCoordinates();
}

// Increment the strand count for the point
RET PointL::addedToStrand(){nStrands++; return R::Success;}

SP<DynObj> PointL::getDoFSource(){return dof->getSource();}

RET PointL::getDebugOutput(string &s){
   s = "";
   s += getName() + "-x: " + MS_T(x) + "\n";
   s += getName() + "-v: " + MS_T(v) + "\n";
   s += getName() + "-iL: " + to_string(matIndex) + "\n";
   s += getName() + "-iL: " + to_string(matIndex) + "\n";
   return R::Success;
}
