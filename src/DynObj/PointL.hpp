/**
   PointL.hpp
   @author: sachdevp
   @desc:   Lagrangian point class. Uses DoF to implement things. 
            A point may be shared by multiple nodes, but there may be only one point per node.
*/
#pragma once
#include <include_common.hpp>
#include <Object.hpp>
#include <DynObj/DynObj.hpp>
#include <DoF/DoFFixed.hpp>

class DoF;

class PointL: public DynObj{
private:
   int pointId;
   // Number of strands that the point belongs to
   int nStrands;
   // Index based on reduced degrees of freedom.
   // Used for inextensibility constraint
   int strandMatIndex;

public:
   PointL():DynObj(){clear();};
   void clear();
   void setDoF(DoF* _dof);
   std::vector<uint8_t> getIndices();
   // Jacobian from RL to full
   Eigen::MatrixXt getJacobian();
   RET Init();
   void setPointId(int id){this->pointId = id;}
   int getPointId();
   // Increment the strand count for the point
   RET addedToStrand();
   bool isUsed(){return nStrands>0;};
   M4 getDoFE();
   M4 getDoFEExt();
   ~PointL(){};
   SP<DynObj> getDoFSource();
   DoFFixed* getDoFFixed(){return dynamic_cast<DoFFixed*>(getDoF());};
   virtual RET getDebugOutput(std::string &s) override final;
};
