/**
   @auth: Prashant Sachdeva
   @desc: Implementation of strand objects.
*/

#include <functional>

#include <DynObj/Strand.hpp>
#include <Segment.hpp>
#include <SegmentLine.hpp>
#include <Node/Node.hpp>
#include <Node/LNode.hpp>
#include <Node/QNode.hpp>
#include <DynObj/ESegment.hpp>
#include <DynObj/StrandAttr.hpp>
#include <Scene/SceneAttr.hpp>
#include <MaterialSpring.hpp>
#include <iterator>
#include <utility>
#include <set>
#include <SE3.hpp>
#include <StrandsHelper.hpp>

using namespace std;
using namespace Eigen;


#define GET_BLOCK4(A, n0, n1) getBlock##A(n0, n0), \
      getBlock##A(n0, n1),                         \
      getBlock##A(n1, n0),                         \
      getBlock##A(n1, n1)                          \
      
using namespace Strands;
/** Constructors **/
Strand::Strand(int _gid, string _name, ftype _radius, ftype _density,
               // Optional arguments
               SP<Material> mat, ftype _initStrain):
   DynObj(_gid, _name){
   initStrain = _initStrain;
   strandAttr = make_shared<StrandAttr>(_radius, _density);
   constImp        = 0.0;
   constImpSum     = 0.0;
   constStabImp    = 0.0;
   constStabImpSum = 0.0;
   constForce      = 0.0;
   bConstActiveAcc = false;
   if(!mat){
      bMaterialSet = false;
      log_err("No material provided for %s", _name.c_str());
   }
   else this->setMaterial(mat);
};

// FIXME Should call DynObj::Init(). Needs correction for that.
RET Strand::Init(){
   if(bMaterialSet == false) return R::Failure;
   uint nNodes = (uint)this->vNodes.size();
   this->nDoF = 4*nNodes;
   Mass.setZero(nDoF, nDoF);
   mMatK.setZero(nDoF, nDoF);
   Force.setZero(nDoF);
   bMatInit = true;
   // Initialize Nodes
   RCHECKRET(setSegmentMatBlocks());
   int idx = nNodes*3;
   for(auto node: vNodes){
      RCHECKRET(node->setStrandMatEIdx(idx));
      RCHECKRET(node->Init());
      idx++;
   }
   for(auto seg: vSegs) RCHECKRET(seg->Init());
   RCHECKRET(init_s());
   // Jacobian
   mMatJQ.setZero(nNodes, nNodes*3);
   matIndexE = -1;
   matIndexL = -1;
   return R::Success;
}

RET Strand::fillJSP(BlockMXt& JSP){
   int nodeMatRow = 0, nodeMatCol = -1;
   for(auto node: vNodes){
      nodeMatCol = node->getPointMatIndex();
      JSP.block<3,3>(nodeMatRow, nodeMatCol).setIdentity();
      nodeMatRow+= 3;
   }
   return R::Success;
}

RET Strand::fillJQ(BlockMXt& _JQ){
   int pointCol = 0, nodeCol = 0;
   uint sz = getnNodes();
   for(auto node: vNodes){
      pointCol = node->getPointMatIndex();
      if(mMatJQ.norm() > 1e10) log_err("Bad mMatJQ for strand %s.", getCName());
      // Filling the correct columns for point correspondence
      _JQ.block(0, pointCol, sz, 3) = this->mMatJQ.block(0, nodeCol, sz, 3);
      nodeCol+= 3;
   }
   return R::Success;
}

bool Strand::isTaut(){return bTaut;}

RET Strand::recordPrevStrain(){
   // log_warn("Recording prev strain for strand %s: %3.2e", getCName(), fStrain);
   fPrevStrain = fStrain;
   return R::Success;
}

RET Strand::setInexActive(int _idx, int _nIdx){
   RCHECKRET(setInexIndex(_idx, _nIdx));
   bInexActive = true;
   bConstActiveAcc = true;
   return R::Success;
}

bool Strand::isInexActive(){return bInexActive;}
bool Strand::isInexActiveAcc(){return bConstActiveAcc;}
RET Strand::setInexInActive(){bInexActive = false; return R::Success;}
RET Strand::getInexIndex(int &_idx, int &_nIdx){
   if(bInexActive){_idx  = inexIndex; _nIdx = nInex;}
   else return R::Failure;
   return R::Success;
};

RET Strand::setInexIndex(int _idx, int _nIdx){
   inexIndex = _idx;
   nInex = _nIdx;
   return R::Success;
};

RET Strand::setConstImp(ftype _constImp, ftype _constStabImp){
   RCHECKERROR_B(!bInexActive, "Setting constraint force but inex constraint not active.");
   // if(abs<ftype>(_constForce)>0.0)
   //    log_warn("Non zero constraint force %3.2e for strand %s.", _constForce, getCName());
   constImp         = _constImp;
   constImpSum     += _constImp;
   constStabImp     = _constStabImp;
   constStabImpSum += _constStabImp;
   return R::Success;
}

RET Strand::getConstForce(ftype &_constForce){
   RCHECKERROR_B(!bConstActiveAcc,
                 "Getting constraint force but inex constraint not active.");
   _constForce = constForce;
   return R::Success;
}

// Make sure the sign works for inextensibility
// TODO WIP
RET Strand::fillInexCons(BlockMXt& GX, BlockMXt& GS, BlockVXt &g, float h){
   Segment* seg = nullptr;
   Node *n0, *n1;
   GX.setZero();
   int cIdx = 0;
   bool first=true, second = false;
   for(const auto& seg: vSegs){
      // Not filling for inactive nodes.
      if(seg->isInActive()){continue;}
      auto ds = seg->getMatDiff();
      RCHECKRET(seg->getNodes(n0, n1));
      int il0, il1, is0, is1;
      // E indices are set later
      RCHECKERROR(n0->getStrandMatLIdx(il0), "Bad l index 0.");
      RCHECKERROR(n1->getStrandMatLIdx(il1), "Bad l index 1.");
      RCHECKERROR(n0->getNodeId(is0),        "Bad s index 0.");
      RCHECKERROR(n1->getNodeId(is1),        "Bad s index 1.");

      if(isTaut()){
         GX.block<1,3>(cIdx, il0) = -2*seg->getVec();
         GX.block<1,3>(cIdx, il1) =  2*seg->getVec();
         GS(cIdx, is0) =  2*ds;
         GS(cIdx, is1) = -2*ds;
      } else{
         GX.block<1,3>(cIdx, il0) = -seg->getVec()/seg->getLength();
         GX.block<1,3>(cIdx, il1) =  seg->getVec()/seg->getLength();
         GS(cIdx, is0) =  ds/abs(ds);
         GS(cIdx, is1) = -ds/abs(ds);
         g(cIdx) = (1.0/h)*(abs(ds)-seg->getLength());
      }

      // GX.block<1,3>(cIdx, il0) = 2*l_seg*seg->getLenDiff(first);
      cIdx++;
   }
   return R::Success;
}

// Helps set upper bound for velocity when too close to limit.
// RET Strand::fillInexVelUB(BlockVXt &g){
//    Segment* seg = nullptr;
//    Node *n0, *n1;
//    int cIdx;
//    if(isTaut()) g.setZero();
//    else{
//       for(const auto& seg: vSegs){
//          auto ds = seg->getMatDiff();
//          RCHECKRET(seg->getNodes(n0, n1));
//          int il0, il1, is0, is1;
//          // E indices are set later
//          RCHECKERROR(n0->getStrandMatLIdx(il0), "Bad l index 0.");
//          RCHECKERROR(n1->getStrandMatLIdx(il1), "Bad l index 1.");
//          RCHECKERROR(n0->getNodeId(is0),        "Bad s index 0.");
//          RCHECKERROR(n1->getNodeId(is1),        "Bad s index 1.");
//          // Not filling for inactive nodes.
//          if(seg->isInActive()){continue;}

//          if(isTaut()){
//             GX.block<1,3>(cIdx, il0) = -2*seg->getVec();
//             GX.block<1,3>(cIdx, il1) =  2*seg->getVec();
//             GS(cIdx, is0) =  2*ds;
//             GS(cIdx, is1) = -2*ds;
//          } else{
//             GX.block<1,3>(cIdx, il0) = -seg->getVec()/seg->getLength();
//             GX.block<1,3>(cIdx, il1) =  seg->getVec()/seg->getLength();
//             GS(cIdx, is0) =  ds/abs(ds);
//             GS(cIdx, is1) = -ds/abs(ds);
//          }

//          // GX.block<1,3>(cIdx, il0) = 2*l_seg*seg->getLenDiff(first);
//          // GX.block<1,3>(cIdx, il1) = 2*l_seg*seg->getLenDiff(second);
//          cIdx++;
//       }
      
//    }
//    return R::Success;
// }

RET Strand::fillInexDrift(BlockVXt &g){
   // g = max<ftype>(0, currLength - l0);
   int iSeg=0;
   for(const auto& seg: vSegs){
      if(seg->isInActive()) continue;
      auto l  = seg->getLength();
      auto ds = seg->getMatLength();
      g[iSeg] = l*l - ds*ds;
      iSeg++;
   }
   return R::Success;
}

// Loading strand nodes from a vector of points
// pointJoints: Maps points to particular joints.
// vPointCons: Return all constraints to be added to scene
int Strand::loadNodes
(SPVEC<Joint> &vPointCons, SPVEC<PointL> strandPoints, MAP<Joint> pointJoints){
   Node *prevNode = nullptr, *currNode;
   uint len = (uint)strandPoints.size();
   // Skip the first and the last one
   vESegs.clear();
   ESegment* currESeg = nullptr;
   // @iPrevPoint is the previous point over a joint. Initialized
   // to a stupidly high value.
   uint iPrevPoint = 1e5;
   SP<Joint> currJoint=nullptr, prevJoint = nullptr;
   Segment* seg;
   SP<PointL> currPoint;
   bool cleanup = false;
   bool prevOnJoint = false;
   // Create all nodes.

   // Figure out the ESegs
   map<int, int> eSegPairs;
   bool pairOn = false;
   int prevIdx= -1, currIdx = -1;
   int pairFirst;
   for(auto ptJtPair: pointJoints){
      // cout<<ptJtPair.first<<" "<<ptJtPair.second<<endl;
      auto currJoint = ptJtPair.second;
      // cout<<"currJoint is "<<currJoint<<endl;;
      currIdx = ptJtPair.first;
      if(currJoint==prevJoint && !pairOn){
         pairFirst = prevIdx;
         pairOn = true;
      }
      if(pairOn && currJoint!=prevJoint){
         // cout<<"eSeg pair "<<pairFirst<<" "<<prevIdx<<endl;
         eSegPairs.insert(make_pair(pairFirst, prevIdx));
         pairFirst = -1;
         pairOn = false;
      }

      prevJoint = currJoint;
      prevIdx = currIdx;
   }
   if(pairOn){
      // cout<<"eSeg pair "<<pairFirst<<" "<<prevIdx<<endl;
      eSegPairs.insert(make_pair(pairFirst, prevIdx));
   }

   // Now we know the ESegs, which can be completed later on.

   currJoint = nullptr;
   prevJoint = nullptr;
   for(uint iCurrPoint=0; iCurrPoint<len; iCurrPoint++){
      // Get current point
      currPoint = strandPoints[iCurrPoint];
      // Make the appropriate node and push
      if(iCurrPoint==0 || iCurrPoint==(len-1)) currNode = new LNode(currPoint);
      else currNode = new QNode(currPoint);
      this->pushNode(currNode);
      currPoint->addedToStrand();

      // Check if point is on joint.
      bool onJoint=false;
      if(pointJoints.size()>0){
         /** Segments over joints **/
         // See if current point is part of a segment over a joint.
         auto itJoint = pointJoints.find(iCurrPoint);
         // If found ...
         onJoint = itJoint != pointJoints.end();
         if(onJoint){
            currJoint = (*itJoint).second;
            CHECK_NOTNULL_R1(currJoint,
                             "Node cannot be mapped to null joint."
                             " Something went wrong.");
         }
      }

      // Are the points consecutive?
      bool consecutive = false;

      if(iCurrPoint > 0){
         CHECK_NOTNULL_R1(prevNode, "Previous node should be set.");
         // All but the first node have a segment corresponding to them.
         seg = setSegment(prevNode, currNode);

         if(!seg){
            log_err("Could not create segment.");
            return -1;
         }
         vSegs.push_back(seg);

         if(onJoint){
            // If iPrevPoint has been set before, then prevJoint has been set too.
            consecutive = (iPrevPoint==(iCurrPoint-1) && (currJoint==prevJoint));
            if(consecutive){
               // NOTE Continuing the previous ESegment, so make sure @currESeg is running.
               CHECK_NOTNULL_R1(currESeg, "Adding to null ESeg.");
               // Add node and segment to current ESeg (used when processing previous node too)
               currESeg->pushNodeSeg(currNode, currPoint, seg);
               currNode->markExtra(true);
            }
         }
      }
      // Point is on joint and either the first point, or start of a new ESeg (checked by nonconsecutive & point idx>0)
      if(onJoint){
         if(iCurrPoint==0 || (iCurrPoint>0 && !consecutive)){
            currESeg = new ESegment;
            currESeg->setJoint(currJoint);
            currESeg->pushNode0(currNode, currPoint);
            vESegs.push_back(currESeg);
            
            prevJoint   = currJoint;
            prevOnJoint = true;
         }
         iPrevPoint = iCurrPoint;
      }
      prevNode = currNode;
   }
   if(eSegPairs.size()!=vESegs.size()){
      log_err("Not the right number of ESegments.");
      return -1;
   }
   int iESeg = 0;
   for(auto eSegPair: eSegPairs){
      currESeg = vESegs[iESeg];

      Node *node0, *node1;
      RCHECKERROR_R1(currESeg->getEndNodes(node0, node1), "Could not get end nodes.");
      
      // Not an extra node. End of segment
      node1->markExtra(false);
      seg = setSegment(node0, node1);
      if(!seg){
         log_err("Could not create segment for eSeg.");
         return -1;
      }
      // Insert at the end of @currESeg's segments
      vSegs.insert(vSegs.begin()+eSegPair.second+iESeg, seg);

      // int id0, id1;
      // node0->getNodeId(id0);
      // node1->getNodeId(id1);

      RCHECKERROR_R1(currESeg->setOSeg(seg),  "Could not set oSeg.");
      RCHECKERROR_R1(currESeg->setComplete(), "Could not complete ESeg.");
      // Last segment ended, must clear last entry of vCons, cause it is invalid.
      currESeg->popCons();
      
      // Check integrity of currESeg
      int nNodes, nPoints, nSegs, nCons;
      currESeg->getLengths(nNodes, nPoints, nSegs, nCons);
      log_verbose("ESegment created with %d nodes,"
                  " %d points, %d segments, %d constraints",
                  nNodes, nPoints, nSegs, nCons);
      iESeg++;
   }
   // Collect all new constraints created for extra nodes.
   for(auto eSeg: vESegs){
      auto vCons = eSeg->getvCons();
      vPointCons.insert(vPointCons.end(), vCons.begin(), vCons.end());
   }
   // for(auto seg: vSegs){
   //    Node *node0, *node1;
   //    int id0, id1;
   //    seg->getNodes(node0, node1);
   //    node0->getNodeId(id0);
   //    node1->getNodeId(id1);
   // }
   
   bNodesLoaded = true;
   return len;
}

RET Strand::checkActiveSegments(){
   bool active;
   uint i = 0;
   for(auto seg : vSegs)  seg->setActive();
   for(auto node: vNodes) node->setActive();
   for(ESegment* eSeg: vESegs){
      RCHECKRET(eSeg->checkActive(active));
      if(active) log_verbose("%s: ESegment %u active.", getCName(), i);
      i++;
   }
   return R::Success;
}

RET Strand::addNode(Node* node){
   RCHECKERROR_NOTNULL(node, "Could not add node.");
   if(node->nodeId == vNodes.size()) this->vNodes.push_back(node);
   else log_err("Node being added out of order, "
                "i.e., node being added with bad nodeId.");
   return R::Success;
}

// Like addNode, but also sets nodeId
RET Strand::pushNode(Node* node){
   auto nNodes = this->getnNodes();
   node->setNodeId(nNodes);
   node->setStrandMatLIdx(nNodes*3);
   addNode(node);
   return R::Success;
}

void Strand::setSceneAttr(SceneAttr* _sceneAttr)
{strandAttr->sceneAttr = _sceneAttr;}

RET Strand::setMaterial(SP<Material> _mat){
   RCHECKERROR(strandAttr->setMaterial(_mat), "Could not set strand material.");
   bMaterialSet = true;
   return R::Success;
}


ostream& operator << ( ostream& out, const pair<uint, uint> pr)
{return out << pr.first << pr.second << '\n';}
/** Dynamics functions **/
RET Strand::computeJacobian(){
   MatrixXt DelS, DelX, L;
   int nE = (int)this->getnActiveENodes();
   if(nE>0){
      DelS.setZero(nE, nE+1);
      DelX.setZero(nE+1, 3*(nE+2));
      L.setZero(nE, nE);
      Node *prevNode=nullptr, *node0, *node1;
      int i=-1;
      ftype *l = new ftype[nE+1];
      for(auto seg: vSegs){
         // Inactive segments do not contribute to this.
         if(seg->isInActive()) continue;
         RCHECKRET(seg->getNodes(node0, node1));
         // If prevNode is not set, first valid segment. No mMatJQ processing.
         i++;
         VectorXt dxi = node1->getx() - node0->getx();
         dxi.normalize();
         DelX.block<1,3>(i, 3*  i  ) = -dxi.transpose() ;
         DelX.block<1,3>(i, 3*(i+1)) =  dxi.transpose() ;
         // TODO Confirm this works for all measures of length
         l[i] = seg->getLength();

         if(prevNode){
            ftype si  = prevNode->gets();
            ftype si1 = node0->gets();
            ftype si2 = node1->gets();

            ftype dsi1 = si2-si1;
            ftype dsi  = si1-si ;

            DelS(i-1,i-1) = -dsi1;
            DelS(i-1,i  ) =  dsi ;
         }
         prevNode = node0;
      }

      // L
      for(int k=0; k<nE-1; k++){
         L(k, k)   =  l[k] + l[k+1];
         L(k, k+1) = -l[k];
         L(k+1, k) = -l[k+2];
      }

      if(nE-1>=0) L(nE-1, nE-1) = l[nE-1] + l[nE];

      // Solve
      int iNode=0;
      FullPivLU<MatrixXt> Lsol = L.fullPivLu();
      MatrixXt sol = -Lsol.solve(DelS*DelX);
      MatrixXt JQ_active(sol.rows()+2, sol.cols());
      JQ_active<<MatrixXt::Zero(1, sol.cols()),
         sol,
         MatrixXt::Zero(1, sol.cols());
      log_verbose(MATRICES
                  HS("L")    "\n"
                  HS("DelS") "\n"
                  HS("DelX") "\n"
                  HS("JQ_active") "\n",
                  MS(L),
                  MS(DelS),
                  MS(DelX),
                  MS(JQ_active));
      // MatrixXt JQ_active = preJQ * DelX;
      // Replacing inverse
      // MatrixXt JQ_active = -L.inverse() *(DelS*DelX);
      vector<pair<uint, uint>> vActNodes;
      int offset=0, idx=-1, prevActIdx=-1, prevActStart =0, nodeIdx=-1;
      prevNode = nullptr;

      // List of indices
      for(auto node: vNodes){
         nodeIdx++;
         if(node->isActive()){
            if(prevNode && prevNode->isInActive()){
               vActNodes.push_back(make_pair(prevActStart, prevActIdx-prevActStart+1));
               prevActStart = nodeIdx;
            }
            prevActIdx = nodeIdx;
         }
         prevNode = node;
      }
      vActNodes.push_back(make_pair(prevActStart, nodeIdx-prevActStart+1));

      int sz, prev=0, oprev=0;

      int previ=0, prevj=0;
      for(auto rows: vActNodes){
         int szi = rows.second;
         prevj=0;
         for(auto cols: vActNodes){
            int szj = cols.second*3;
            mMatJQ.block(rows.first, cols.first*3, szi, szj) = JQ_active.block(previ, prevj, szi, szj);
            prevj += szj;
         }
         previ += szi;
      }
   } else mMatJQ.setZero();
   return R::Success;
}

RET Strand::checkTaut(){
   if(fStrain>(-EPSILON)) bTaut = true;
   else bTaut = false;
   // log_warn("Checking taut for %s to %d %7.7e",getCName(), bTaut, fStrain);
   return R::Success;
}

RET Strand::clearForStep(ftype t){
   Force.setZero();
   mMatJQ.setZero();
   constImp = 0;
   constStabImp= 0;
   for(auto node: vNodes){node->clearForStep(t);}
   for(auto seg: vSegs){seg->clearForStep();}
   // if(this->isInextensible() && fPrevStrain>-EPSILON && !bInexActive){
   //    log_err("%3.2e, Strand %s: Previous Strain %3.2e, new strain %3.2e, but inex not active.", t, getCName(), fPrevStrain, fStrain);
   // }
   // if(this->isInextensible() && fPrevStrain<-EPSILON &&  bInexActive){
   //    log_err("%3.2e, Strand %s: Previous Strain %3.2e, new strain %3.2e, but inex active.", t, getCName(), fPrevStrain, fStrain);
   // }
   // NOTE bInexActive will be set when creating inextensibility constraint in @Scene::fill
   setInexInActive();
   checkTaut();
   // NOTE updateAct functionality is moved to params. A map is no longer stored in @Strand instance
   // if(canBeActive()) updateAct(t);
   return R::Success;
}

RET Strand::computeForceVector(){
   int ind = 0;
   for(auto seg: vSegs){if(seg->isActive()) seg->fillNodeForces();}
   int idxL, idxE;

   if(IS_MATRIX_NULL(Force)) {
      log_err("Force for %s is null.", getCName());
   }
   for(auto node : vNodes) {
      if(node->isInActive())
         continue;
      RCHECKRET(node->getStrandMatLIdx(idxL));
      RCHECKRET(node->getStrandMatEIdx(idxE));
      Force.segment<3>(idxL) = node->Force.segment<3>(0);
      if(IS_MATRIX_NULL(Force)){
         log_err("Force 1 for %s is null.", getCName());
      }
      Force(idxE) = node->Force(3);
      if(IS_MATRIX_NULL(Force)){
         log_err("Force 2 for %s is null.", getCName());
      }

   }

   return R::Success;
}

RET Strand::computeMK(){
   // Compute mass for all segments
   // Set matrix to Zero
   Mass.setZero();
   mMatK.setZero();
   log_verbose("Computing strand mass for %s", getCName());
   // Can overlap the two loops in a staggered fashion to speed up a touch.
   int iSeg = 0;
   for(auto seg: vSegs){
      if(seg->isActive()){
         auto r = seg->computeSegmentMKf();
         if(RFAIL(r)){

            Node *node0, *node1;
            int id0, id1;
            seg->getNodes(node0, node1);
            node0->getNodeId(id0);
            node1->getNodeId(id1);
            int pid0 = node0->getPointId();
            int pid1 = node1->getPointId();
            log_err("Failed to compute mass for segment %d of %s between %d(%d) and %d(%d).",
                    iSeg, getCName(), id0, pid0, id1, pid1);
            return R::Failure;
         }
      }
      iSeg++;
   }

   /** Collect segment masses into the strand matrix **/
   for(auto seg: vSegs){
      if(seg->isActive()) seg->fillMK();
      else seg->fillInactiveMK();
   }
   log_random("Computed strand matrix: \n %s", MS(Mass));
   this->dirty = false;
   return RET::Success;
}

/** Getters and Setters **/
uint Strand::getnActiveSegments(){
   uint nSeg = 0;
   for(auto seg: vSegs) if(seg->isActive()) nSeg++;
   // log_warn("Number of active segments: %d", nSeg);
   return nSeg;
}

uint Strand::getnActiveENodes(){
   uint nENodes = 0;
   for(auto node: vNodes){if(node->isActive()) nENodes++;}
   // Front and back are always active
   return (nENodes - 2);
}

SP<PointL> Strand::getPointL(int pointId, SPVEC<PointL> points){
   for(auto point: points){
      if(point->getId() == pointId){
         log_random("%d", point->getId());
         return point;
      }
   }
   log_err("Point %d not found for strand %d", pointId, getId());
   return nullptr;
}

// TODO Replace with shared_ptr
Segment* Strand::setSegment(Node* n0, Node* n1){
   log_random("Setting segment between nodes %d and %d",
              n0->nodeId, n1->nodeId);
   CHECK_NOTNULL_RNULL(strandAttr, "StrandAttr is null.");
   if(n0 == n1 || n0->getPointId() == n1->getPointId()){
      log_err("Setting segment between same nodes(%p, %p) or point ids(%d, %d).",
              n0, n1, n0->getPointId(), n1->getPointId());
      return nullptr;
   }
   auto seg = new SegmentLine(n0, n1, strandAttr);
   return seg;
}

RET Strand::setSegmentMatBlocks(){
   Node *n0, *n1;
   for(auto seg: vSegs){
      RCHECKERROR(seg->getNodes(n0, n1), "Failed to get segment nodes.");
      MatBlocks *MBlocks = new MatBlocks(GET_BLOCK4(MLL,n0, n1),
                                         GET_BLOCK4(MLE,n0, n1),
                                         GET_BLOCK4(MEL,n0, n1),
                                         GET_BLOCK4(MEE,n0, n1));
      MatBlocks *KBlocks = new MatBlocks(GET_BLOCK4(KLL,n0, n1),
                                         GET_BLOCK4(KLE,n0, n1),
                                         GET_BLOCK4(KEL,n0, n1),
                                         GET_BLOCK4(KEE,n0, n1));
      seg->setMatrices(MBlocks, KBlocks);
   }
   return R::Success;
}

ftype Strand::getLinearDensity(){return strandAttr->getLinearDensity();}

RET Strand::getExtraNodes(ExtraNodes &extraNodes){
   // Number of non-extra nodes seen.
   int coreIndex=-1;
   int idx=-1;
   for(auto node: vNodes){
      idx++;
      if(node->isExtra()){
         if(node->isActive()){
            // Add to map
            extraNodes.push_back(make_pair(coreIndex, node->getx()));
         }
      } else{
         coreIndex++;
      }
   }
   return R::Success;
}

/** Matrix related functions **/
MatrixXt *Strand::getKorM(KorM KM){
   if(KM==KorM::Stiffness) return &(mMatK);
   return &(this->Mass);
}

// Defining 4 functions to get blocks of the matrix to Support discontinuous nodes.
#define BLOCK_FN_DEF(LLEE, szx, szy, c0, c1)                            \
   Eigen::BlockM ## szx ## szy ## t Strand::getBlock##LLEE (KorM KM, Node* n0, Node* n1){ \
      assert(bMatInit);                                                 \
      MatrixXt* A = getKorM(KM);                                        \
      int ind0 = c0?GET_EIND(n0->nodeId):GET_LIND(n0->nodeId);            \
      int ind1 = c1?GET_EIND(n1->nodeId):GET_LIND(n1->nodeId);            \
      return A->block<szx,szy>(ind0, ind1);                              \
   }

BLOCK_FN_DEF(LL, 3, 3, 0, 0); 
BLOCK_FN_DEF(LE, 3, 1, 0, 1); 
BLOCK_FN_DEF(EL, 1, 3, 1, 0); 
BLOCK_FN_DEF(EE, 1, 1, 1, 1); 

void Strand::setInitLength(ftype _l){
   l0 = _l;
   currLength = l0;
   log_verbose("Initial length of strand %s: %3.2f", getCName(), l0);
}

RET Strand::init_s(){
   V3 prev;
   if(initStrain<(-1+EPSILON)) return R::Failure;
   ftype denom = (1.0f + initStrain);
   // Make sure that the right nodes are active
   RCHECKERROR(checkActiveSegments(), "Failed to assess if segments are active.");
   ftype s = 0.0;
   firstNode()->sets(0.0);
   Node *tmp, *n;
   // While the segments don't have a necessary ordering, active segments are still ordered.
   for(auto seg: this->vSegs){
      if(seg->isInActive()) continue;
      RCHECKERROR(seg->getNodes(tmp, n), "Could not get nodes.");
      if(seg->getLength()<THRESH){
         log_err("Small segment.");
         return R::Failure;
      }
      s += seg->getLength();
      int id;
      id = n->getPointId();
      n->sets(s/denom);
   }
   lNode = n;
   setInitLength(s/denom);
   fStrain     = initStrain;
   fPrevStrain = initStrain;
   return checkTaut();
}

RET Strand::update(){
   // Activate or inactivate segments and nodes as needed
   bPrevTaut = bTaut;
   RCHECKERROR(checkActiveSegments(), "Failed to assess if segments are active.");
   RCHECKERROR(update_s(),            "Failed to update Eulerian coord for %s.", getCName());
   return R::Success;
}

RET Strand::update_s(){
   // ls is total material length
   int nSegs = getnActiveSegments();
   ftype *lSeg = new ftype[nSegs], lx=0.0;
   uint iSeg=0;
   Node *tmp, *n;

   // Get physical length of the segments
   for(auto seg: this->vSegs){
      if(seg->isInActive()) continue;
      lSeg[iSeg] = seg->getLength();
      // log_warn("New Length of strand segment %d of %s: %f", iSeg, getName().c_str(), lSeg[iSeg]);
      // log_warn("%s, %s", MS_T(seg->mMatInputs.node0->getx()), MS_T(seg->mMatInputs.node1->getx()));
      lx+=lSeg[iSeg];
      iSeg++;
   }
   // lc is the current material length
   ftype lc = 0;
   if(!EQUALS(l0, lastNode()->gets())){
      log_err("Original length and last node's s-value does not match: %3.2f, %3.2f", l0, lastNode()->gets());
      return R::Failure;
   }
   // Total material length is constant, so assign by proportion.
   iSeg=0;
   for(auto node: this->vNodes){
      if(node->isInActive()) continue;
      // Don't assign to front and end
      if(node != firstNode() && node != lastNode()){
         lc += lSeg[iSeg];
         auto sval = (lc/lx) * l0;
         // log_warn("%s s value: %f %f %f",getCName(), lc, lx, sval);
         node->sets(sval);
         iSeg++;
      }
   }
   fStrain = lx/l0 - 1.0f;
   // log_warn("Setting strain for %s to %3.2e, lx %3.2e, l0 %3.2e",getCName(), fStrain, lx, l0);
   currLength = lx;
   return R::Success;
}

Node* Strand::lastNode(){return lNode;}
MatrixXt Strand::getStiffnessMatrix(){return mMatK;}
int Strand::getStrandId(){return this->strandId;}
ftype Strand::getStrain(){return fStrain;}
ftype Strand::getPrevStrain(){return fPrevStrain;}
// Note that this includes 
uint Strand::getLSize(){return (uint)vNodes.size()*3;}
uint Strand::getESize(){return (uint)vNodes.size();}
int Strand::getLIndex(){return matIndexL;}
int Strand::getEIndex(){return matIndexE;}

RET Strand::setEIndex(uint eIndex){
   matIndexE = eIndex;
   return R::Success;
}

RET Strand::setLIndex(uint lIndex){
   matIndexL = lIndex;
   return R::Success;
}

RET Strand::setStrandId(uint _sid){
   this->strandId = _sid;
   return R::Success;
}

bool Strand::isInextensible(){return strandAttr->material->isInextensible();}

RET Strand::setInextensible(){
   log_verbose("Setting %s(%d) strand to inextensible", getCName(), getStrandId());
   return strandAttr->material->setInextensible();
}

bool Strand::canBeActive(){return strandAttr->material->canBeActive();}

RET Strand::setAct(float act){return strandAttr->material->setActivation(act);}
RET Strand::setAct(float act, float t){return strandAttr->material->setActivation(act);}

RET Strand::accumulate(ftype Dt){
   constForce = constImpSum/Dt;// + (constStabImpSum/Dt)/Dt;
   constImpSum = 0;
   constStabImpSum = 0;
   constImp = 0;
   constStabImp = 0;
   return R::Success;
}

RET Strand::cleanAcc(){
   bConstActiveAcc = false;
   return R::Success;
}
