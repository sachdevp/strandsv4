#include <include_common.hpp>

class Node;
class Segment;
class PointL;

class Strands::ESegment{
private:
   // End nodes + all points in between
   // Size: n
   std::vector<Node*> vNodes;
   // Points for end nodes + all nodes in between
   // Size: n
   SPVEC<PointL> vPoints;
   // Segments when extended segment is active
   // Size: n - 1
   std::vector<Segment*> vSegs;
   // Constraints corresponding to introduced points
   // Siz: n - 2
   SPVEC<Joint> vCons;
   // Original segment -> When extended segment is inactive
   Segment* oSeg;
   SP<Strands::Joint> rigidJoint;
   uint eStrandId;
   bool bOsegSet;
   RET getEndNodesOrder(bool &inOrder);
   RET getEndNodesPos(VECV3 &retVec);
   int sDir;
   bool inOrder;
   // Initial direction of closest point on segment.
   V2 mRelInitDir;
   
public:
   ESegment();
   RET setOrigActive();
   RET setElementsActive();
   RET setElementsInActive();
   RET pushNode0(Node*, SP<PointL>);
   RET pushNodeSeg(Node*, SP<PointL>, Segment*);
   RET setJoint(SP<Joint>);
   // Recompute the nodes/constraints of the esegment.
   RET recompute(VECV3 newPos);
   // Checks whether the segments and nodes are active or not, setting them active accordingly.
   RET checkActive(bool&);
   RET getEndNodes(Node*&, Node*&);
   RET setOSeg(Segment*);
   RET getLengths(int &nNodes, int &nPoints, int &nSegs, int &nCons);
   SPVEC<Joint> getvCons();
   void popCons();
   RET setComplete();
   RET setRelInitDir();
};
