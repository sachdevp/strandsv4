#include <DynObj/Strand.hpp>
#include <Node/Node.hpp>
#include <Segment.hpp>

/** Debug functions **/
// Log output when debugging.
using namespace Strands;
using namespace std;
using namespace Eigen;

RET Strand::getNodeOutput(string &dOut){
   for(auto node: vNodes)
      if(node->isActive()){
         int nIdx;
         node->getNodeId(nIdx);
         RCHECKWARN(node->appendDebugOutput(dOut),
                    "Could not debug node %u for strand %s",
                    nIdx, getCName());
      }
   return R::Success;
}

RET Strand::getSegOutput(string &dOut){
   int iSeg = 0;
   for(auto seg: vSegs){
      if(seg->isInActive()) continue;
      auto dx = seg->getLength();
      auto ds = seg->getMatLength();
      dOut += getName() + "-ds-dx #" + to_string(iSeg) + ": " +
         to_string(ds) + " " + to_string(dx) + "\n";
      iSeg++;
   }
   return R::Success;
}

RET Strand::getInexOutput(string &dOut){
   auto strName = getName();
   MatrixXt GX,GS;
   uint nS = getnActiveSegments();
   uint nN = getnNodes();
   GX.setZero(nS, nN*3);
   GS.setZero(nS, nN);
   BlockMXt GXb = GX.block(0, 0, nS, nN*3);
   BlockMXt GSb = GS.block(0, 0, nS, nN);

   VectorXt drift;
   drift.setZero(nS);
   BlockVXt g = drift.segment(0, nS);
   // g unused from following command.
   fillInexCons(GXb, GSb, g, 1.0);
   RCHECKERROR(fillInexDrift(g), "Could not get drift.");
   VectorXt v;
   v.setZero(vNodes.size()*3);
   int ii = 0;
   int idx;
   for(auto node: vNodes) {
      RCHECKERROR(node->getStrandMatLIdx(idx), "Bad index 0.");
      v.segment<3>(idx) = node->getv();
   }
   VectorXt inexVal = GX*v;
   dOut += strName + "-inexVal: "      + to_string(inexVal(0)) + "\n";
   dOut += strName + "-inex-stab-dx: " + MS_T(drift)           + "\n";
   dOut += strName + "-lambda: "       + to_string(constForce) + "\n";
   return R::Success;
}

RET Strand::getDebugOutput(string &dOut){
   auto strName = getName();
   auto cName   = getCName();
   dOut = strName + "-strain: " + to_string(getStrain()) + "\n";
   int iSeg = 0;
   RCHECKRET(getNodeOutput(dOut));
   RCHECKRET(getSegOutput(dOut));
   dOut += strName + "-force: " + MS_T(Force) + "\n";
   ExtraNodes extraNodes;
   RCHECKRET(getExtraNodes(extraNodes));

   if(extraNodes.size()>0) dOut += "Extra Nodes: \n";
   for(auto nodePair: extraNodes){
      dOut += to_string(nodePair.first) + ": " + MS_T(nodePair.second) + "\n";
   }
   dOut += strName + "-JQ: ...\n" + MS_T(mMatJQ) + "\n";
   if(isTaut()){
      dOut += strName + " is taut.\n";
      if(isInextensible()) RCHECKRET(getInexOutput(dOut));
   }
   return R::Success;
}

