#pragma once

#include <Object.hpp>
class DoF;
class Object;

class DynObj: public Object{
protected:
   DoF* dof;
   Eigen::MatrixXt Mass;
   Eigen::VectorXt Force;
   Eigen::VectorXt v;
   Eigen::VectorXt x, dx, dx_stab;

   int matIndex;
   bool bDoFSet;

public:
   DynObj();
   DynObj(uint _gid, std::string _name, bool _dirty = true);
   Eigen::VectorXt getCoordinates(){return x;};
   // Computes the mass matrix. Each object has it's own function.
   virtual RET computeMassMatrix(){return RET::Failure;};

   // Set functions
   RET setv(Eigen::VectorXt _v);
   virtual RET setx(Eigen::VectorXt _x);
   virtual RET setdx(Eigen::VectorXt _x);
   virtual RET setdx_stab(Eigen::VectorXt _x);
   RET setIndex(int ind);
   RET Init();

   int getIndex();
   virtual int getRedIndex();
   virtual int getnRed();
   // Scene needs to get the dof from a new constructed point
   DoF* getDoF();
   Eigen::VectorXt getv();
   inline Eigen::VectorXt getx() {return this->x;}
   // FIXME Confirm that mass matrix is computed
   Eigen::MatrixXt getMassMatrix() {return Mass;}
   Eigen::Index getMatrixSize() {return Mass.rows();}
   Eigen::VectorXt getForceVector() {return Force;}
   virtual RET setDebugOutput() override;
   virtual RET getDebugOutput(std::string &s) override{return R::Failure;};

   virtual M4 getTransformExt(){return E;};
};
