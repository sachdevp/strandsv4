#pragma once

#include <DynObj/DynObj.hpp>
#include <DoF/DoFFrame.hpp>

class Rigid : public DynObj{
public:
   Rigid();
   Rigid(std::string _OBJFile, Eigen::Matrix4t _E, ftype _density);

   virtual RET computeMassMatrix() override;
   RET Init();

   // NOTE Currently the dof must be a frame.
   RET setFrame(DoFFrame* _frame);

   RET update();

   RET setMassMatrix(ftype _M[4]);
   RET setMassMatrix(std::array<ftype, 4> _M);
   RET setMassMatrix(Eigen::Vector4t _M);
   RET setMassMatrix(ftype _mass, Eigen::Matrix3t _MI);
   RET setTransform(M4 comE, M4 objE);
   RET setTransform(M4 comE); // comE assumed to be same as objE

   void setDirty();
   RET setRigidId(int _rid);
   RET setMassMatrixBlock();

   int getRigidId();
   DoFFrame* getFrame() const{return dynamic_cast<DoFFrame*>(dof);};
   virtual M4 getTransformExt() override final;

   virtual RET setx(Eigen::VectorXt _x) override final;
   virtual RET getDebugOutput(std::string &) override final;
   virtual RET clearForStep(ftype t) override final;
   R setScVel(ftype t, V6 vel);
   R clearScVelMap();

   /** External interaction functions **/
   // void setExtWrench(ftype f0, ftype f1, ftype f2, ftype f3, ftype f4, ftype f5);
   // void setExtForceW(ftype f0, ftype f1, ftype f2, ftype r0, ftype r1, ftype r2);
   // void setInertia(const ftype* inertia); // inertia: [angX, angY, angZ, linear]
   // void setDamping(ftype d) {damping = d;};

private:
   RET clear();

   /////////////////////
   // State variables //
   /////////////////////
   Eigen::Vector6t fext;               // External force expressed in local, 6x1

   /////////////////////
   // Model variables //
   /////////////////////
   ftype mu;                    // friction coefficient
   ftype damping;               // Rayleigh mass damping
   ftype density;               // Density of the object

protected:
   M4 E0; // Initial and computed centre of mass and rotation
   // (from mesh verts)
   ftype mass[6];      // Mass matrix, 6x1
   ftype colour[3];
   // Unique to every rigid. Need this because the scene may not have consecutive rigid IDs.
   int rid;

   ///////////////////////
   // Working variables //
   ///////////////////////
   std::string OBJFile;
   bool bMassSet;
};
