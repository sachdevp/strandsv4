
#include <DynObj/StrandAttr.hpp>

using namespace std;

StrandAttr::StrandAttr(ftype _radius, ftype _density)
   :radius(_radius), density(_density){}

StrandAttr::StrandAttr(){
   radius = 0.1f;
   density = 1e3f;
   material = make_shared<Strands::Material>();
}

RET StrandAttr::setMaterial(SP<Strands::Material> mat){
   if(mat == nullptr) return R::Failure;
   this->material = mat;
   return R::Success;
}
