/**
   @author: sachdevp
   @desc:   Strand class definition.
   Defined as a string of Q nodes between two L nodes at the ends.
*/

#pragma once

#include <DynObj/DynObj.hpp>

class Node;
class StrandAttr;
class Segment;
class SceneAttr;
class PointL;

/** Macros for cleaner reading of code ahead **/
#define GET_LIND(x) (3*(x))
#define GET_EIND(x) (3*vNodes.size() + x)
// This macro aids pre-processed generation of function declarations for block
// matrix construction
#define BLOCK_FN_DECL(KM, MassStiffness, LLEE, szx, szy) \
   Eigen::BlockM ## szx ## szy ## t                                     \
   getBlock ## KM ## LLEE (Node* n0, Node* n1){                         \
      return getBlock ## LLEE (KorM::MassStiffness, n0, n1);            \
   };

class Strands::Strand: public DynObj {
   // Scene creator should be able to access privates of Strand
   friend class Strands::SceneCreator;
private:
   // Nodes and segments
   std::vector<Node*> vNodes;
   std::vector<Segment*> vSegs;
   std::vector<Strands::ESegment*> vESegs;
   ftype currLength;
   // Constraint force if inequality constraint is active
   ftype constForce;
   // Accumulation of constraint force over multiple steps
   ftype constImp, constImpSum, constStabImp, constStabImpSum;
   bool bConstActiveAcc;
   KeysMap<double> actsMap;

   // Strand's parameters should be part of strandAttr
   SP<StrandAttr> strandAttr;

   // State booleans
   bool bNodesLoaded;
   bool bMatInit;
   bool bMaterialSet;
   bool bInexActive;
   bool bTaut, bPrevTaut;

   // matIndexC is the constraint index
   int matIndexE, matIndexL;
   // Mat index in Ci_f
   int inexIndex, nInex;
   ///////////////////////////////////////////////
   // NOTE The matrix M and K is arranged as:   //
   // +--------+                                //
   // | LL  LE |                                //
   // | EL  EE |                                //
   // +--------+                                //
   ///////////////////////////////////////////////
   // mMatK - Stiffness matrix
   // mMatJQ - Quasistatic Jacobian
   Eigen::MatrixXt mMatJQ, mMatK;

   ftype l0, initStrain;
   ftype fStrain, fPrevStrain;
   // StrandId is not strictly necessary for the library However, to support
   // strand deletion it is sensible to not have to correct strand IDs for all
   // remaining strands.
   int strandId;

   // Check Id and return point.
   SP<PointL> getPointL(int pointId, SPVEC<PointL> points);
   enum class KorM{Stiffness, Mass};

   /** Gets matrix blocks for mass and stiffess **/
   Eigen::BlockM33t getBlockLL(KorM KM, Node* n0, Node* n1);
   Eigen::BlockM31t getBlockLE(KorM KM, Node* n0, Node* n1);
   Eigen::BlockM13t getBlockEL(KorM KM, Node* n0, Node* n1);
   Eigen::BlockM11t getBlockEE(KorM KM, Node* n0, Node* n1);
   Node* lastNode();
   Node *lNode;
   inline Node* firstNode(){return vNodes.front();}
   void setInitLength(ftype _l);
   Segment* setSegment(Node* n0, Node* n1);

public:
   // Core initialization function
   Strand(int _gid, std::string _name, ftype _radius, ftype _density,
          SP<Material> mat = nullptr, ftype _initStrain = 0.0);
   RET Init();

   /** Matrix/Vector computation functions **/
   RET computeMK();
   RET computeForceVector();
   RET computeJacobian();

   /** Node loading functions **/
   // Loading strand nodes from a vector of points
   // @pointJoints Maps points to particular joints.
   int loadNodes(SPVEC<Joint> &vPointCons, SPVEC<PointL> strandPoints, MAP<Joint> pointJoints);
   // // Loading nodes from a set of points with point IDs
   // int loadNodes(IVEC pointIds, MAP<PointL> allPoints, MAP<Joint> allJoints);

   // More manual methods of adding nodes.
   RET addNode(Node*);
   RET pushNode(Node*);

   // NOTE Deal with coincident nodes etc at the matrix level instead of the
   // Jacobian. In other words, let there be an easy matrix for block generation
   // at the strands for segments.


   /** Specific matrix block functions for **/
   Eigen::MatrixXt *getKorM(KorM KM);
   BLOCK_FN_DECL(M, Mass, LL, 3, 3)
   BLOCK_FN_DECL(M, Mass, LE, 3, 1)
   BLOCK_FN_DECL(M, Mass, EL, 1, 3)
   BLOCK_FN_DECL(M, Mass, EE, 1, 1)
   BLOCK_FN_DECL(K, Stiffness, LL, 3, 3)
   BLOCK_FN_DECL(K, Stiffness, LE, 3, 1)
   BLOCK_FN_DECL(K, Stiffness, EL, 1, 3)
   BLOCK_FN_DECL(K, Stiffness, EE, 1, 1)

   /** Eulerian coordinate functions **/
   // Initialize the Eulerian coordinates
   RET init_s();
   RET update_s();
   RET update();
   // Helper function to mark segments as active or inactive based on configuration.
   RET checkActiveSegments();
   virtual RET clearForStep(ftype t) final;

   /** Get functions **/
   int getEIndex();
   int getLIndex();
   // Is the strand taut?
   RET checkTaut();
   bool isTaut();
   Eigen::MatrixXt getStiffnessMatrix();
   int getStrandId();
   ftype getStrain();
   ftype getPrevStrain();
   bool canBeActive();
   uint getLSize();
   uint getESize();
   // Number of nodes in the strand
   inline uint getnNodes(){return (uint)vNodes.size();}
   inline uint getnLNodes(){return 2u;}
   inline uint getnENodes(){return getnNodes() - getnLNodes();}
   uint getnActiveENodes();
   uint getnSegments(){return (uint)vSegs.size();}
   uint getnActiveSegments();
   ftype getLinearDensity();
   Eigen::MatrixXt getJQ(){return mMatJQ;};
   RET getExtraNodes(ExtraNodes&);
   RET recordPrevStrain();

   RET setInexInActive();
   RET setInexActive(int _idx, int _nIdx);
   RET getInexIndex(int &_idx, int &_nIdx);
   RET setInexIndex(int _idx, int _nIdx);
   bool isInexActive();
   bool isInexTautActive();
   bool isInexActiveAcc();
   bool isInextensible();
   virtual RET getDebugOutput(std::string &s) final;
   RET getNodeOutput(std::string &dOut);
   RET getSegOutput(std::string &dOut);
   RET getInexOutput(std::string &dOut);

   /** Fill functions **/
   // Fill functions for JSP and JQ. Both have been implemented in a similar
   // block fashion to respect the location of points and strands in the
   // corresponding matrices
   RET prefill(){return R::Success;};
   RET fillJSP(Eigen::BlockMXt& JSP);
   RET fillJQ(Eigen::BlockMXt& _JQ);
   RET fillInexCons(Eigen::BlockMXt &GX, Eigen::BlockMXt &GS, Eigen::BlockVXt &g, float h);
   RET fillInexVelUB(Eigen::BlockVXt &g);
   RET fillInexDrift(Eigen::BlockVXt &g);

   /** Set functions**/

   RET setAct(float act);
   void setSceneAttr(SceneAttr* _sceneAttr);
   RET setMaterial(SP<Material> _mat);
   // Set the segments to be used for mass computation.
   RET setSegmentMatBlocks();
   // void setSegments(MAP<Joint> pointJoints = MAP<Joint>());
   RET setEIndex(uint eIndex);
   RET setLIndex(uint lIndex);
   RET setStrandId(uint _sid);
   RET setInextensible();
   RET setConstImp(ftype  _constImp, ftype  _constStabImp);
   RET getConstForce(ftype &_constForce);
   RET accumulate(ftype Dt);
   RET cleanAcc();
   R setAct(ftype act, ftype t);
};

