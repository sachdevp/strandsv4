/**
   @author: sachdevp
   @desc:   An object named world defined for easy handling of nodes and rigid bodies fixed to the world.
 */

#pragma once

#include <include_common.hpp>
#include <DynObj/DynObj.hpp>
#include <DynObj/WorldObject.hpp>


class WorldObject: public DynObj{

public:
   auto static creator() {
      return SP<WorldObject>(new WorldObject());
   }
   WorldObject():DynObj(static_cast<int>(RigidBodies::World), "world", false){
      E = Eigen::Matrix4t::Identity();
      nDoF = 6;
      v = Eigen::VectorXt::Zero(6);
      x = Eigen::VectorXt::Zero(6);
      matIndex = -1;
   }

   Eigen::VectorXt getv() {
      return this->v;
   }

   Eigen::VectorXt getCoordinates(){
      return x;
   };

   virtual int getRedIndex() override final{
      return -1;
   }
   virtual int getnRed() override final{
      return -1;
   }
};
