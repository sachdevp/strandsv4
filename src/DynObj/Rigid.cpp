#include <DynObj/Rigid.hpp>
#include <SE3.hpp>

using namespace std;

void Rigid::setDirty(){
   this->bMassSet = false;
}

Rigid::Rigid(std::string _OBJFile, M4 _E, ftype _density):
   density(_density),
   OBJFile(_OBJFile){
   auto r = setTransform(_E);
   RCHECKWARN(r, "Could not set transform");
   clear();
}

RET Rigid::setTransform(M4 comE){
   this->E = comE;
   this->E0 = EYE4;
   return R::Success;
}

RET Rigid::setTransform(M4 comE, M4 objE){
   this->E  = comE;
   this->E0 = comE.fullPivLu().solve(objE);
   log_random(HS("comE") "\n" HS("objE"), MS(comE), MS(objE));
   return RET::Success;
}

Rigid::Rigid(){clear();}

RET Rigid::clear(){
   this->nDoF = 6;
   this->v.setZero(6);
   this->x.setZero(6);
   this->setDirty();
   this->name = string("None");
   this->gid = -1;
   return R::Success;
}

RET Rigid::setMassMatrix(ftype _M[4]){
   this->Mass.setZero(6,6);
   for(int ii=0;ii<4;ii++){
      this->Mass(ii,ii) = _M[ii];
   }
   this->Mass(5,5) = this->Mass(3,3);
   this->Mass(4,4) = this->Mass(3,3);
   this->Force.setZero(6);
   this->bMassSet = true;
   log_random(HS("mass"), MS(Mass));
   return R::Success;
}

RET Rigid::setMassMatrix(std::array<ftype, 4> _M){
   return setMassMatrix(_M.data());
}

RET Rigid::setMassMatrix(ftype _mass, Eigen::Matrix3t _MI){
   this->Mass.setZero(6,6);

   this->Mass.block<3,3>(0,0) = _MI;

   this->Mass(5,5) = this->Mass(4,4) = this->Mass(3,3) = _mass;
   this->Force.setZero(6);
   this->bMassSet = true;
   return R::Success;
}

RET Rigid::setMassMatrix(Vector4t _M){
   ftype dblMass[4];
   EQUAL_ARRAY(dblMass, _M, 4);
   return setMassMatrix(dblMass);
}

RET Rigid::Init(){
   // Check whether it is added to scene.
   if(this->isAddedToScene()==R::Failure){
      log_info(1, "Rigid body %s not added to scene", ObjName(this));
   }
   this->computeMassMatrix();
   // this->x.segment<3>(3) = this->E.block<3, 1>(0, 3);
   return Object::Init();
}

RET Rigid::setFrame(DoFFrame* _frame){
   this->dof = _frame;
   this->x = _frame->getx();
   return R::Success;
}

RET Rigid::computeMassMatrix(){
   // TODO Requires using a function to generate mass matrix from OBJ file.
   if(this->bMassSet){return RET::Success;}
   else return RET::Failure;
};

RET Rigid::update(){
   return R::Success;
}

int Rigid::getRigidId(){
   return this->rid;
}

M4 Rigid::getTransformExt(){
   // E = COM -> World
   // E0 = Obj -> COM
   // E*E0 = Obj->World
   return E*E0;
}

RET Rigid::setRigidId(int _rid){
   this->rid = _rid;
   return R::Success;
}

RET Rigid::setx(Eigen::VectorXt _x) {
   VectorXt dx = _x - this->x;
   if(dx.norm() <EPSILON) return R::Success;
   // TODO Check whether should be on the right
   this->E = this->E * SE3::computeTransform(dx, 1.0);
   if(x.rows() == _x.rows()){this->x = _x; return R::Success;}
   log_err("Could not set position for %s. Wrong number of rows.", ObjName(this));
   return R::Failure;
}

RET Rigid::getDebugOutput(std::string &dOut){
   dOut = "";
   dOut += getName() + "-x: " + MS_T(this->x) + "\n";
   dOut += getName() + "-v: " + MS_T(this->v) + "\n";
   return R::Success;
}

RET Rigid::clearForStep(ftype t){
   return getFrame()->clearForStep(t);
}

R Rigid::setScVel(ftype t, V6 vel){
   return getFrame()->setScVel(t, vel);
}

R Rigid::clearScVelMap(){
   return getFrame()->clearScVelMap();
}
