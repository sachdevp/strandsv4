#include <DynObj/DynObj.hpp>
#include <DoF/DoF.hpp>

using namespace std;
using namespace Eigen;

DynObj::DynObj():Object(){
   bDoFSet = false;
   dof = nullptr;
}

DynObj::DynObj(uint _gid, string _name, bool _dirty): Object(_gid, _name, _dirty){
   bDoFSet = false;
   dof = nullptr;
}

VectorXt DynObj::getv() {
   if(this->dirty){
      dof->setTargetVelocity();
      this->dirty = false;
   }
   return this->v;
}

RET DynObj::setx(VectorXt _x) {
   if(x.rows() == _x.rows()){
      this->x = _x;
      return R::Success;
   }
   log_err("Could not set position for %s. Wrong number of rows.", ObjName(this));
   return R::Failure;
}

RET DynObj::setdx(VectorXt _x) {
   if(x.rows() == _x.rows()){
      this->dx = _x;
      return R::Success;
   }
   log_err("Could not set dx for %s. Wrong number of rows.", ObjName(this));
   return R::Failure;
}

RET DynObj::setdx_stab(VectorXt _x) {
   if(x.rows() == _x.rows()){
      this->dx_stab = _x;
      return R::Success;
   }
   log_err("Could not set stab dx for %s. Wrong number of rows.", ObjName(this));
   return R::Failure;
}

RET DynObj::setv(VectorXt _v){
   if(v.rows() == _v.rows()){
      this->v = _v;
      return R::Success;
   }
   log_err("Could not set velocity for %s. Wrong number of rows.", ObjName(this));
   return R::Failure;
}

RET DynObj::setIndex(int ind){
   this->matIndex = ind;
   return R::Success;
}

int DynObj::getIndex(){
   return this->matIndex;
}

int DynObj::getRedIndex(){
   return this->dof->getMatIndex();
}
int DynObj::getnRed(){
   return this->dof->getnReduced();
}

// Scene needs to get the dof from a new constructed dynamic object
DoF* DynObj::getDoF(){
   return dof;
}

RET DynObj::Init(){
   if(!this->bDoFSet){
      log_err("DoF not set");
      return R::Failure;
   }
   // DoF successfully set
   return Object::Init();
}

RET DynObj::setDebugOutput(){
   this->bDebug = true;
   // NOTE dof may not exist, e.g. for strand
   if(dof) dof->setDebugOutput();
   return R::Success;
}
