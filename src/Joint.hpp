#pragma once

#include <include_common.hpp>
#include <Object.hpp>
#include <SceneConstants.hpp>

class DofFrame;
class DynObj;

using namespace Eigen;

// NOTE For cylindrical joint, the assumed axis is Z axis, and joint has one
// radius used for trajectory computation of all strands passing over it.
typedef struct{
   ftype rad;
   ftype ang0;
   ftype k;
   std::array<ftype, 6> params;
   char type;
} JointShape;

class Strands::Joint: public Object{
public:
   Joint(SP<DynObj> ra, SP<DynObj> rb, std::array<bool, 6> cnstrn, M4 E);
   RET Init();
   RET update();
   RET computeConstraint();
   RET computeEqDrift(BlockVXt& drift);
   RET computeEqDrift6(V6 &drift);
   RET computeIneqDrift(BlockVXt &drift);
   RET computeIneqDrift(VectorXt &drift);

   MatrixXt getiCa(){return iCa;}
   MatrixXt getiCb(){return iCb;}
   MatrixXt getAdja(){return Adja;}
   MatrixXt getAdjb(){return Adjb;}
   MatrixXt getUnconstRowsA();
   // Get block for ith free row. 
   MatrixXt getuCa(){return uCa;}
   MatrixXt getuCb(){return uCb;}
   RET getCIndices(int &ia, int &ib);
   RET getEDebug(M4 &Ea, M4 &Eb);
   VectorXt getic() {return ic;}
   VectorXt getuc(){return uc;}
   bool isActive(){return bActive;}
   uint getJointId();
   uint getnC(){return nC;}
   // Only provides the maximum number possible
   uint getnCi(){return nCi;}
   // Count values>0 in exceeded
   uint getnExceeded(){int n = 0; for(auto e: exceeded){n+=(e>0);} return n;}
   RET getExceeded(std::array<int, 3> &outExceeded);
   void getCDoFs(int &cols_a, int &cols_b);
   M4 getTransform0_source(){return aEj0;};
   M4 getTransformA(){return Ea;}
   M4 getTransformB(){return Eb;}

   inline ftype      getRad(){return shape.rad;}
   inline SP<DynObj> getSource(){return ra;}

   RET setJointId(int);
   RET setLowerLimit(V6 lim);
   RET setUpperLimit(V6 lim);
   RET setLimitType(JointLimitType);
   // RET jointLimitExceeded(bool &result, int i, bool lOrULimit);
   bool checkRigids(SP<DynObj> r1, SP<DynObj> r2, bool *inOrder = nullptr);
   void setActive(bool a = true){bActive = a;}
   void setInActive(){bActive = false;}
   RET setTransforms();

   inline void setRad(ftype _rad){shape.rad = _rad;}
   inline void setAng0(ftype _ang){shape.ang0 = _ang;}
   inline void setLinStiffness(ftype _k){shape.type = 'l'; shape.k = _k;}
   inline void setExpStiffness(std::array<ftype, 6> _params){
      shape.type = 'e';
      shape.params = _params;
   }
   RET initStiffness(SP<JointStiffness> &_jStiff);
#ifdef EXPLICIT_JOINT_DAMPING
   RET initDamping(SP<JointDamping> &_jDamp);
#else
   RET initDamping();
#endif
   RET setDamping(ftype _d);
   VectorXt getDamping();

private:
   MatrixXt iCa, iCb, uCa, uCb;
   Matrix6t Adja, Adjb;
   VectorXt ic, uc;
   ftype fDamp;
   VectorXt dampVec;
   Matrix4t bEj0, aEj0, j0Ew;
   M4 Ea, Eb;
   std::array<ftype, 3> angles;
   std::array<ftype, 6> constImp, constImpSum, constForce; 
   std::array<ftype, 6> constStabImp, constStabImpSum; 
   // Position of j0 with respect to ra and rb
   V4 pa, pb;
   uint nC, nCi;
   int jointId;
   JointShape shape;
   SP<JointStiffness> mJStiff;

#ifdef EXPLICIT_JOINT_DAMPING
   SP<JointDamping> mJDamp;
#endif
   bool bInitStiffness, bInitDamping;

   // @exceeded mapping index to - 0 if not exceeded, 1 if lower limit exceeded, 2 if upper limit exceeded
   std::array<int, 3> exceeded;
   RET computeExceeded();
   
   SP<DynObj> ra, rb;
   char constrained;

   V6 upperLimit;
   V6 lowerLimit;
   JointLimitType mLimitType;
   bool bActive;

public:
   ///////////////////////
   // Working variables //
   ///////////////////////

   // Equality and inequality constraint indices.
   // NOTE that inequality constraints become active and inactive at different times, hence only
   // current value noted.
   int currEIdx;
   int currIIdx;

   RET getExceeded(std::array<int, 3> &outExceeded, std::array<bool, 3> &outConstrained);
   virtual RET clearForStep(float t) override final;
   RET getAngles(std::array<ftype, 3>&);
   RET getConstForce(std::array<ftype, 6>&);
   RET getConstIndex(int &_eqIdx, int &_ineqIdx);

   RET setEIdx(int idx){currEIdx = idx; return R::Success;};
   RET setIIdx(int idx){currIIdx = idx; return R::Success;};
   // Constraint force setting taken care of here.
   RET setConstImp(VectorXt eqImp, VectorXt ineqImp, VectorXt eqStabImp, VectorXt ineqStabImp);
   RET accumulate(ftype Dt);

   virtual RET getDebugOutput(std::string &s) override final;
   static bool needsUpper(JointLimitType);
   static bool needsLower(JointLimitType);
};
