#include <Material.hpp>

using namespace Eigen;
namespace Strands{
   RET Material::setInextensible(bool inex){
      this->bInextensible = inex;
      return R::Success;
   };

   RET Material::setLimits(ftype _limits[2]){
      if(_limits[0] >= _limits[1]) return R::Failure;
      fStrainLimits[0] = _limits[0];
      fStrainLimits[1] = _limits[1];
      return R::Success;
   }

   Material::Material(){
      clear();
   }

   RET Material::clear(){
      bGravity = true;
      bInextensible = false;
      bActivation = false;
      // Strain limits
      fStrainLimits[0] = -0.5;
      fStrainLimits[1] = 2;
      return R::Success;
   }
   RET Material::computeStiffness
   (Strands::MaterialStiffness &matStiffness, Strands::MaterialInputs matInputs){
      matStiffness.tKLL.setZero();
      matStiffness.tKLE.setZero();
      matStiffness.tKEE = 0;
      return R::Success;
   }
   bool Material::isMaterialActive(MatType matType){
      return (matType == MatType::MUSCLEHILL ||
              matType == MatType::MUSCLELINEAR);
   }

}
