#pragma once

#include <Force.hpp>

// Force defined at a position w.r.t rigid body using @localE. Force is applied
// along the X direction of @localE.
class Strands::RigidForce: public Strands::Force{
private:
   ftype mag;
   // Defined w.r.t rigid body
   M4 localE;
public:
   RigidForce(SP<DynObj> _dynObj, ftype mag, M4 localE = EYE4);
   virtual R fillForce() override final;
   // Update the direction based on the rigid body
   virtual R update() override final;
   virtual R Init() override final;
   virtual R getIndices(std::vector<int> &vIdx, std::vector<int> &vLen) override final;
   virtual RET clearForStep(ftype t) override final{return R::Success;};
};
