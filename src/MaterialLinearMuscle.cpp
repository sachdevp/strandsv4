#include <MaterialLinearMuscle.hpp>
#include <Node/Node.hpp>
#include <DynObj/StrandAttr.hpp>
using namespace Strands;

using MLM = MaterialLinearMuscle;

MLM::MaterialLinearMuscle(ftype _stiffness, ftype dl_act){
   fStiffness  = _stiffness;
   fDl_act     = dl_act;
   fAct        = 0.0;
   bActivation = true;
}

// NOTE Muscle-strands are made immune to gravity.
RET MLM::computeForces(Strands::MaterialForces &matForces,
                       Strands::MaterialInputs matInputs){
   matForces.fx[0].setZero();
   matForces.fx[1].setZero();
   matForces.fs[0] = 0.0;
   matForces.fs[1] = 0.0;
   if((matInputs.node0 == nullptr) ||
      (matInputs.node1 == nullptr)) return R::Failure;
   V3    x0 = matInputs.node0->getx();
   V3    x1 = matInputs.node1->getx();
   ftype s0 = matInputs.node0->gets();
   ftype s1 = matInputs.node1->gets();
   V3    dx = x1 - x0;
   ftype l = dx.norm() ;
   V3    dx_dir = dx/l;
   ftype ds = s1-s0;
   ftype l0_eff = ds*(1.0f-fDl_act*fAct);
   // effective strain if treated as a spring.
   ftype epsilon = l/l0_eff - 1;
   if(epsilon > 0.0){
      V3 fex = fStiffness * epsilon * dx_dir;
      matForces.fx[0] =  fex;
      matForces.fx[1] = -fex;
   }
   if (IS_MATRIX_NULL(matForces.fx[0])) {
     log_err("Matforces are null.");
   }

   return R::Success;
}

RET MLM::setActivation(ftype _a){
   RCHECKERROR_B(_a < 0.0 || _a > 1.0, "Trying to set activation to %3.2f.", _a);
   fAct = _a;
   return R::Success;
};
