#include <Force.hpp>

using namespace Strands;
using namespace std;
using namespace Eigen;

R Force::setForceBlocks(vector<BlockVXt> _blocks){
   RCHECKERROR_B(_blocks.size() != nBlocks,
                 "Incorrect # of incoming blocks(%zu), %zu expected.",
                 _blocks.size(), nBlocks);
   // Get all blocks.
   for (int i = 0; i<nBlocks; i++) vForceBlocks.push_back(_blocks[i]);
   bBlocksInit = true;
   return R::Success;
}

R Force::setForceId(int _fid){
   RCHECKERROR_B(_fid == -1, "Bad forceId.");
   this->forceId = _fid;
   return R::Success;
}

R Force::getIndices(IVec &vIdx, IVec &vLen){
   vIdx.clear(); vLen.clear(); return R::Success;
}
