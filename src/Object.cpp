/**
   @author: sachdevp
   @desc:
*/

#include <Object.hpp>
using namespace std;

Object::Object(uint _gid, string _name, bool _dirty){
   clear();
   gid = _gid;
   name = _name;
   dirty = _dirty;
}

RET Object::setInfo(uint _gid, string _name){
   this->gid = _gid;
   this->name = _name;
   return R::Success;
}

void Object::setRecord(bool _record){bRecord = _record;}
bool Object::toBeRecorded(){return bRecord;}

void Object::clear(){
   // Object uninitialized
   bAddedToScene = false;
   dirty         = true;
   name          = "NoNameObject";
   gid           = -1;
   bRecord       = true;
   BaseObject::clear();
}

Object::Object(){clear();}

RET Object::Init(){
   if(this->gid==-1){
      log_warn("ID not set for object: %s", ObjName(this));
   }
   return BaseObject::Init();
}

RET Object::addedToScene(){
   this->bAddedToScene = true;
   return R::Success;
}

RET Object::isAddedToScene(){
   if(this->bAddedToScene) return R::Success;
   return R::Failure;
}
