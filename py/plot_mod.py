import matplotlib.pyplot as plt
from matplotlib import pylab
from importlib import reload
import os
import strands_mod
from ctypes import *
import xml.etree.ElementTree as xmltree
import tempfile as tf
from matplotlib import cm
import numpy as np
import sys
import getopt

def read_dir(in_dir):
    in_files = []
    in_labels = []
    try:
        with open('%s/in.dat'%in_dir, 'r') as in_data_file:
            n_files = int(in_data_file.readline().strip())
            for line in in_data_file:
                if len(in_files)==n_files:
                    attr_labels = [item.strip() for item in line.strip().split(' ')]
                    break
                f_name, label = line.split(',')
                in_files.append('%s/%s'%(in_dir, f_name))
                in_labels.append(label.strip())
                # Do not read input data after number of files specified.
                # Necessary to avoid any extra lines
    except IOError:
        print('Could not find input data file \'in.dat\' in dir %s.'%in_dir)
        sys.exit(0)
    return in_files, in_labels, attr_labels

def read_state_files():
    opts, args = getopt.getopt(sys.argv[1:], "o:d:")
    in_dir = None
    out_file = None
    in_files = []
    in_labels = []

    for o, a in opts:
        if o=="-d":
            in_dir = a
            print('Input directory set to %s'%a)
        elif o=="-o":
            out_file = a

    if in_dir:
        in_files, in_labels, attr_labels = read_dir(in_dir)
    return out_file, in_files, in_labels, attr_labels

def read_state_files_multi(n):
    opts, args = getopt.getopt(sys.argv[1:], 'o:'+'d:'*n)
    in_dirs = []
    out_file = None
    in_files = []
    in_labels = []
    in_files_list = []
    in_labels_list = []
    attr_labels_list = []

    for o, a in opts:
        if o=="-d":
            in_dirs.append(a)
            print('Input directory set to %s'%a)
        elif o=="-o":
            out_file = a

    for in_dir in in_dirs:
        in_files, in_labels, attr_labels = read_dir(in_dir)
        in_files_list.append(in_files)
        in_labels_list.append(in_labels)
        attr_labels_list.append(attr_labels)

    return out_file, in_files_list, in_labels_list, attr_labels_list

def read_state_files():
    opts, args = getopt.getopt(sys.argv[1:], "o:d:")
    in_dir = None
    out_file = None
    in_files = []
    in_labels = []

    for o, a in opts:
        if o=="-d":
            in_dir = a
            print('Input directory set to %s'%a)
        elif o=="-o":
            out_file = a

    if in_dir:
        in_files, in_labels, attr_labels = read_dir(in_dir)
    return out_file, in_files, in_labels, attr_labels

def read_state_map(in_file, label, attr_labels, timestep):
    try:
        with open(in_file, 'r') as in_data_file:
            mode = 'vals'
            result = lambda: 0
            # Label for result, to be used in plotting
            result.label = label
            result.filename = in_file
            result.timestep = timestep
            result.l_attr_dicts = []
            for line in in_data_file:
                if mode=='vals':
                    # Reading attr dictionary
                    attr_vals = [x for x in line[:-1].split(' ') if x!='']
                    attr_dict = {}
                    idx2 = 0
                    for idx, label in enumerate(attr_labels):
                        attr_label = attr_labels[idx]
                        attr_right = attr_label.split('.')[1]
                        attr_len = strands_mod.attr_lens[attr_right]
                        
                        attr_dict[attr_label] =  [float(x) for x in attr_vals[idx2:(idx2+attr_len)]]
                        idx2 = idx2+attr_len
                    result.l_attr_dicts.append(attr_dict)
            return result
    except IOError:
        print('Could not open input file %s'%in_file)
        sys.exit(0)

def plot_pip_mcp(f, axis, results, in_dict):
    cmap = plt.cm.get_cmap('jet')

    for i_result, result in enumerate(results):
        mcp_mid = in_dict['mcp_mid']
        pip_mid = in_dict['pip_mid']
        dip_mid = in_dict['dip_mid']
        timestep = in_dict['timestep']
        time_idxs = [int(x/timestep) for x in [0, 5, 10, 15]]
        steps = len(result.l_attr_dicts)
        mcp_vals = [x[0]*180/3.1415 for x in [y['MCP.ang'] for y in result.l_attr_dicts]]
        mcp_mid_idx = min(range(len(mcp_vals)), key=lambda i: abs(mcp_vals[i]-mcp_mid))
        pip_vals = [x[0]*180/3.1415 for x in [y['PIP.ang'] for y in result.l_attr_dicts]]
        pip_mid_idx = min(range(len(pip_vals)), key=lambda i: abs(pip_vals[i]-pip_mid))
        
        dip_vals = [x[0]*180/3.1415 for x in [y['DIP.ang'] for y in result.l_attr_dicts]]
        dip_mid_idx = min(range(len(dip_vals)), key=lambda i: abs(dip_vals[i]-dip_mid))
        
        rgba = cmap((i_result)/len(results))
        time_range = np.arange(0, steps, 1)*timestep
        line = plt.plot(mcp_vals[0:steps], pip_vals[0:steps], label=result.label, c=rgba)[0]
        for time_idx in time_idxs:
            plt.plot(mcp_vals[time_idx], pip_vals[time_idx], marker='.', markersize=15, c=rgba)
    ytics = [0, 20, 40, 60, 80, 100]
    xtics = [-20, 0, 20, 40, 60, 80, 100]
    plt.plot([20, 35], [35, 60], c='k')
    plt.plot(35, 60, marker=(3, 0, 90),markersize=15,c='k')
    plt.text(23, 49, 'Time', ha='center', rotation=60, va='center')
    plt.text(-10, 5, '0 s', ha='center', va='center')
    plt.text(20, 25, '5 s', ha='center', va='center')
    plt.text(43, 67, '10 s', ha='center', va='center')
    plt.text(85, 100, '15 s', ha='center', va='center')
    axis.grid(color=[0.8,0.8,0.8], linestyle='-.', linewidth=0.2)

    axis.set_xticks(xtics)
    axis.set_yticks(ytics)
    axis.set_xlabel('MCP')
    axis.set_ylabel('PIP')
    return f, axis

def plot_angles_helper(f, axes, results, in_dict):
    cmap = plt.cm.get_cmap('jet')
    for i_result, result in enumerate(results):
        mcp_mid = in_dict['mcp_mid']
        pip_mid = in_dict['pip_mid']
        dip_mid = in_dict['dip_mid']
        timestep = in_dict['timestep']
        steps = len(result.l_attr_dicts)
        mcp_vals = [x[0]*180/3.1415 for x in [y['MCP.ang'] for y in result.l_attr_dicts]]
        mcp_mid_idx = min(range(len(mcp_vals)), key=lambda i: abs(mcp_vals[i]-mcp_mid))
        pip_vals = [x[0]*180/3.1415 for x in [y['PIP.ang'] for y in result.l_attr_dicts]]
        pip_mid_idx = min(range(len(pip_vals)), key=lambda i: abs(pip_vals[i]-pip_mid))
        dip_vals = [x[0]*180/3.1415 for x in [y['DIP.ang'] for y in result.l_attr_dicts]]
        dip_mid_idx = min(range(len(dip_vals)), key=lambda i: abs(dip_vals[i]-dip_mid))
        # orl_taut_vals = np.array([x[0] for x in [y['ORL.taut'] for y in result.l_attr_dicts]])
        # print (orl_taut_vals)
        
        rgba = cmap((i_result)/len(results))
        time_range = np.arange(0, steps, 1)*timestep
        axes[0].plot(time_range, dip_vals[0:steps], label=result.label+' ', c=rgba)
        loc = dip_mid_idx/100.0
        axes[0].plot(loc, -40, marker='D', c=rgba)
        axes[0].plot([loc, loc], [-40, 120], linestyle='--', linewidth=1, c=rgba)
        axes[0].grid(color=[0.8,0.8,0.8], linestyle='-.', linewidth=0.2)
        # axes[0].fill(time_range, 160/len(results)*(i_result+1)*orl_taut_vals, facecolor=rgba, alpha=0.3)
        axes[1].plot(time_range, pip_vals[0:steps], label=result.label+' ', c=rgba)
        loc = pip_mid_idx/100.0
        axes[1].plot(loc, -40, marker='D', c=rgba)
        axes[1].plot([loc, loc], [-40, 120], linestyle='--', linewidth=1, c=rgba)
        axes[1].grid(color=[0.8,0.8,0.8], linestyle='-.', linewidth=0.2)
        axes[2].plot(time_range, mcp_vals[0:steps], label=result.label+' ', c=rgba)
        loc = mcp_mid_idx/100.0
        axes[2].plot(loc, -40, marker='D', c=rgba)
        axes[2].plot([loc, loc], [-40, 120], linestyle='--', linewidth=1, c=rgba)
        xtics = [  0, 5, 10, 15]
        ytics = [-40, 0, 40, 80, 120]
        row_labels = ['DIP', 'PIP', 'MCP']
        axes[2].grid(color=[0.8,0.8,0.8], linestyle='-.', linewidth=0.2)
        for i_row, row_axis in enumerate(axes):
            row_axis.set_ylabel(row_labels[i_row], labelpad=-5)
            row_axis.set_yticks(ytics)
            row_axis.set_xticks(xtics)
            row_axis.set_ylim([ytics[0], ytics[-1]])
    f.tight_layout()

def plot_angles(results, in_dict):
    f, axes = plt.subplots(nrows=3, ncols=1, figsize=(6, 8), sharex=True)
    plot_angles_helper(f, axes, results, in_dict)
    plt.legend(ncol=3)
    return f, axes
