import plot_mod
import matplotlib as mp
import matplotlib.pyplot as plt

# Plot weighted lum results with and without ORL

out_file, in_files_list, in_labels_list, attr_labels_list = plot_mod.read_state_files_multi(2)

ndirs = 2
mp.use('pgf')

preamble = [
    r'\usepackage{fontspec}',
    r'\setmainfont{Linux Libertine O}',
]

params = {
    'font.family': 'sans-serif',
    'font.size': 18,
    'text.usetex': True,
    'pgf.rcfonts': False,
    'pgf.texsystem': 'xelatex',
    'pgf.preamble': preamble,
}

mp.rcParams.update(params)

timestep = 0.01
in_dict = dict()
in_dict['pip_mid'] = 54
in_dict['dip_mid'] = 22.5
in_dict['mcp_mid'] = 44.8
in_dict['timestep'] = 0.01
results = []

f, axes = plt.subplots(nrows=3, ncols=2, figsize=(12, 8), sharex=True)
axes = [[ axes[row][col] for row in range(0,3) ] for col in range(0,2) ]

idir=0
for in_files, in_labels, attr_labels in zip(in_files_list, in_labels_list, attr_labels_list):
    results = []
    for in_file, in_label in zip(in_files, in_labels):
        results.append(plot_mod.read_state_map(in_file, in_label, attr_labels, timestep))
    plot_mod.plot_angles_helper(f, axes[idir], results, in_dict)
    idir=idir+1
plt.subplots_adjust(left=0.09, bottom=.20, right=None, top=None, wspace=0.20, hspace=0.10)
f.text(0.28, 0.985, '(a) Without OL', ha='center', va='center')
f.text(0.765, 0.985, '(b) With OL', ha='center', va='center')
f.text(0.28, 0.15, 'Time (s)', ha='center', va='center')
f.text(0.01, 0.55, "Flexion-Extension ($^\circ$)", ha='center', va='center', rotation=90, fontsize=18)
plt.legend(bbox_to_anchor=(-1.2, -.8, 2.2, .102), loc=3, title='Lumbrical Load',
           ncol=8, mode="expand", borderaxespad=0.)
plt.savefig(out_file)
        
    
