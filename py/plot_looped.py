import plot_mod
import matplotlib.pyplot as plt
import matplotlib as mp
import matplotlib.gridspec as gridspec

out_file, in_files, in_labels, attr_labels = plot_mod.read_state_files()

mp.use('pgf')

preamble = [
    r'\usepackage{fontspec}',
    r'\setmainfont{Linux Libertine O}',
]

params = {
    'font.family': 'sans-serif',
    'font.size': 18,
    'text.usetex': True,
    'pgf.rcfonts': False,
    'pgf.texsystem': 'xelatex',
    'pgf.preamble': preamble,
}

mp.rcParams.update(params)

timestep = 0.01
in_dict = dict()
in_dict['pip_mid'] = 54
in_dict['dip_mid'] = 22.5
in_dict['mcp_mid'] = 44.8
in_dict['timestep'] = 0.01
results = []

plt.figure(figsize=(12,8.5))
gs = gridspec.GridSpec(3, 5)
s = 2
axes0 = plt.subplot(gs[0, 0:s])
axes = [axes0, plt.subplot(gs[1, 0:s], sharex=axes0), plt.subplot(gs[2, 0:s], sharex=axes0), plt.subplot(gs[1:3, s:])]
f = plt.gcf()
plt.setp(axes[0].get_xticklabels(), visible=False)
plt.setp(axes[1].get_xticklabels(), visible=False)
axes[3].set_aspect('equal')

for in_file, in_label in zip(in_files, in_labels):
    results.append(plot_mod.read_state_map(in_file, in_label, attr_labels, timestep))
plot_mod.plot_angles_helper(f, axes[:-1], results, in_dict)
plot_mod.plot_pip_mcp(f, axes[3], results, in_dict)
plt.subplots_adjust(left=0.09, bottom=.20, right=None, top=None, wspace=0.10, hspace=0.10)
f.text(0.26, 0.15, 'Time (s)', ha='center', va='center')
f.text(0.01, 0.575, 'Flexion-Extension ($^\circ$)', ha='center', va='center', rotation=90, fontsize=18)
plt.legend(
    bbox_to_anchor=(-1.25, -0.35,  2.2, .102),
    loc=3, title='Activation',
           ncol=8, mode="expand", borderaxespad=0.)

print (out_file)
plt.savefig(out_file)

        
    
