import matplotlib.pyplot as plt
from matplotlib import pylab
from importlib import reload
import os
import strands_mod
from ctypes import *
import xml.etree.ElementTree as xmltree
import tempfile as tf
from matplotlib import cm
import numpy as np
import sys
import getopt

strands_dll = strands_mod.load_strands_mod()
opts, args = getopt.getopt(sys.argv[1:], "d:")

str_fname = '/home/sachdevp/Projects/Strandsv4/data/xml/Lumbrical.Temp.xml'
str_fname_c = str_fname.encode('utf-8')
et = xmltree.parse(str_fname)

nSteps1 = 100
nSteps2 = 100
nRuns = 5

base_fname = 'data'

scene_p1 = c_void_p()
scene_p1 = strands_dll.load_scene(str_fname_c)
scene_p2 = c_void_p()
scene_p2 = strands_dll.load_scene(str_fname_c)
if not scene_p1 or not scene_p2:
    raise ValueError('Could not load scene.')
state_attrs_str = strands_dll.get_state_attrs(scene_p1).decode('utf-8')
state_attrs = [x for x in str(state_attrs_str).split(' ') if x!='']
print(state_attrs)
dt = strands_dll.get_dt(scene_p1)

init_x = POINTER(c_float)()
init_v = POINTER(c_float)()
init_len = c_int()
init_x2 = POINTER(c_float)()
init_v2 = POINTER(c_float)()
init_len2 = c_int()
m1 = POINTER(c_float)()
m2 = POINTER(c_float)()
r1 = POINTER(c_float)()
r2 = POINTER(c_float)()
sz1 = c_int()
sz2 = c_int()

for iStep in range(0, nSteps1):
    strands_dll.step(scene_p1)
    if(strands_dll.failed_check(scene_p1)): break
    success_steps = iStep

max_dm = 0
max_dx = 0
max_dv = 0
for i in range(nRuns):
    strands_dll.get_internal_state(scene_p1, byref(init_x), byref(init_v), byref(init_len))
    strands_dll.set_initial_state(scene_p2, init_x, init_v, init_len, 1)
    print('----------------------------------------------\n\n\n\n')
    for iStep in range(0, nSteps2):
        s1 = strands_dll.get_internal_state(scene_p1, byref(init_x), byref(init_v), byref(init_len))
        s2 = strands_dll.get_internal_state(scene_p2, byref(init_x2), byref(init_v2), byref(init_len2))

        x1 = [init_x[i]  for i in range(init_len.value)]
        x2 = [init_x2[i] for i in range(init_len.value)]
        v1 = [init_v[i]  for i in range(init_len.value)]
        v2 = [init_v2[i] for i in range(init_len.value)]
        # print([a-b for a, b in zip(v1, v2)])
        vnorm = sum([min(a*a, b*b) for a, b in zip(v1, v2)])
        dx = (sum([(a-b)*(a-b) for a, b in zip(x1, x2)])/ sum([max(a*a, b*b) for a, b in zip(x1, x2)]))
        dv = (sum([(a-b)*(a-b) for a, b in zip(v1, v2)])/ vnorm)
        if dv>1.0:
           print([abs(a-b) for a, b in zip(v1, v2)])
        state1 = strands_dll.get_state(scene_p1)
        state2 = strands_dll.get_state(scene_p2)

        # state1_vals = [float(x) for x in state1.split(' ') if x]
        # state2_vals = [float(x) for x in state2.split(' ') if x]
        state1_p = strands_mod.process_state(state_attrs, state1)
        state2_p = strands_mod.process_state(state_attrs, state2)
        # for key in state1_p.keys():
        #     print(key, state1_p[key], state2_p[key])
        strands_dll.step(scene_p1)
        strands_dll.step(scene_p2)
        if(strands_dll.failed_check(scene_p1)): break
        if(strands_dll.failed_check(scene_p2)): break

        strands_dll.get_red_mat(scene_p1, byref(m1), byref(r1), byref(sz1))
        strands_dll.get_red_mat(scene_p2, byref(m2), byref(r2), byref(sz2))
        # print(m1, m2)

        # print(['%4.2e'%(r1[i]-r2[i]) for i in range(sz1.value)])

        if sz1.value!=sz2.value:
            print('Error: Bad matrix sizes %d, %d'%(sz1.value, sz2.value))
            break;

        val = sz1.value
        val2 = val*val
        dm = sum([pow(m1[i]-m2[i],2) for i in range(val2)])/ sum([pow(max(m1[i],m2[i]),2) for i in range(val2)])
        dr = sum([pow(r1[i]-r2[i],2) for i in range(val)]) / sum([pow(max(r1[i],r2[i]),2) for i in range(val) ])
        max_dm = max(dm, max_dm)
        max_dx = max(dx, max_dx)
        max_dv = max(dv, max_dv)
        print('%d %d vnorm %3.2e, dv is %3.2e (%3.2e, %3.2e)'%(init_len.value, init_len2.value, vnorm, max_dv, dm, dr))

        # for i in range(val):
        #     print('%f, %f'%(m1[i], m2[i]))

        success_steps = success_steps+1
print(success_steps)
print('dm is %3.2e'%max_dm)
print('dx is %3.2e'%max_dx)
print('dv is %3.2e'%max_dv)
