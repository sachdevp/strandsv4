import matplotlib.pyplot as plt
from matplotlib import pylab
from importlib import reload
import os
import strands_mod
from ctypes import *
import xml.etree.ElementTree as xmltree
import tempfile as tf
from matplotlib import cm
import numpy as np
import sys
import getopt

strands_dll = strands_mod.load_strands_mod()
opts, args = getopt.getopt(sys.argv[1:], "d:")
out_dir = None
for o, a in opts:
    if o=="-d":
        print('-d detected')
        try:
            if not os.path.exists(a):
                os.makedirs(a)
            out_dir = a
            print('Output directory set to %s'%a)
        except:
            print('Could not create directory %s'%a)
    else:
        print('Input poorly formatted.')
        sys.exit(0)
if not out_dir:
    print('Directory not set.')
    sys.exit(0)

str_fname = '../data/xml/Lumbrical.Looped.xml'
str_fname_c = str_fname.encode('utf-8')
et = xmltree.parse(str_fname)

cmap = plt.cm.get_cmap('jet')

# act_list = [0, 0.5, 1.0]
act_list = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.75, 1.0]
enable_wrench_list = [True, True, True, True, True, False, False, False]
str_LUMMuscle_c = c_char_p('LUMMuscle'.encode('utf-8'))
str_FlxAnchor_c = c_char_p('pCube2'.encode('utf-8'))
str_MCPWrench_c = c_char_p('MCPWrench'.encode('utf-8'))
str_PIPWrench_c = c_char_p('PIPWrench'.encode('utf-8'))

# 18 seconds 
nSteps  = 1800
nInitSteps = 100
# pip_mid = 54
# dip_mid = 22.5
# mcp_mid = 44.8

base_fname = 'data'

try:
    indat_file = '%s/in.dat'%out_dir
    print(indat_file)
    with open(indat_file, 'w') as fout:
        print(len(act_list), file=fout)
except:
    print('Could not create new in.dat file.')

for i_act, act_val in enumerate(act_list):
    scene_p = c_void_p()
    scene_p = strands_dll.load_scene(str_fname_c)
    if not scene_p:
        raise ValueError('Could not load scene.')
    state_attrs = None
    if i_act==0:
        state_attrs_str = strands_dll.get_state_attrs(scene_p).decode('utf-8')
        state_attrs = [x for x in str(state_attrs_str).split(' ') if x!='']
    # print(state_attrs, file=out_file)
    dt = strands_dll.get_dt(scene_p)
    enable_wrench = enable_wrench_list[i_act]

    state_maps = []
    success_steps = 0
    results_fname = '%s.%1.1f.csv'%(base_fname, act_val)
    if enable_wrench:
        strands_dll.disable_wrench(scene_p, str_PIPWrench_c)
        vel_sc = [0,0,0]
        strands_dll.script_rigid(scene_p, str_FlxAnchor_c, strands_mod.convert_float_list(vel_sc))
        for iStep in range(0, nInitSteps):
            strands_dll.step(scene_p)
            if(strands_dll.failed_check(scene_p)): break
            success_steps = iStep
        vel_sc[2] = -0.2
        strands_dll.script_rigid(scene_p, str_FlxAnchor_c, strands_mod.convert_float_list(vel_sc))
    else:
        strands_dll.disable_wrench(scene_p, str_MCPWrench_c)
        vel_sc = [0,0,0]
        strands_dll.script_rigid(scene_p, str_FlxAnchor_c, strands_mod.convert_float_list(vel_sc))
        for iStep in range(0, nInitSteps):
            strands_dll.step(scene_p)
            if(strands_dll.failed_check(scene_p)): break
            success_steps = iStep
        vel_sc[2] = -0.2
        strands_dll.script_rigid(scene_p, str_FlxAnchor_c, strands_mod.convert_float_list(vel_sc))
    strands_dll.disable_wrench(scene_p, str_MCPWrench_c)
    strands_dll.disable_wrench(scene_p, str_PIPWrench_c)
    strands_dll.set_act(scene_p, str_LUMMuscle_c, act_val)

    try:
        with open('%s/in.dat'%out_dir, 'a+') as fout:
            print('%s, %s'%(results_fname, str(act_val)), file=fout)
            if i_act==(len(act_list)-1):
                state_attrs_str = strands_dll.get_state_attrs(scene_p).decode('utf-8')
                print(state_attrs_str, file=fout)
                state_attrs = [x for x in str(state_attrs_str).split(' ') if x!='']
    except IOError:
        print('Could not write in.dat in dir %s'%out_dir)

    try:
        with open('%s/%s'%(out_dir, results_fname), 'w') as fout:
            state_attrs_str = strands_dll.get_state_attrs(scene_p).decode('utf-8')
            state_attrs = [x for x in str(state_attrs_str).split(' ') if x!='']
            # print(state_attrs_str, file=fout)
            success_steps=0
            for iStep in range(0, nSteps):
                strands_dll.step(scene_p)
                if(strands_dll.failed_check(scene_p)): break
                print(strands_dll.get_state(scene_p).decode('utf-8'), file=fout)
                success_steps = iStep
            print('Completed %d steps successfully.'%success_steps)
    except IOError:
        print('Could not write results for %d in %s'%(act_val, out_dir))

    # for iStep in range(0, nSteps):
    #     strands_dll.step(scene_p)
    #     if(strands_dll.failed_check(scene_p)):
    #         break
    #     state_map = dict()
    #     state_map = strands_mod.process_state(state_attrs, strands_dll.get_state(scene_p))
    #     state_maps.append(state_map)
    #     print(state_attrs.values(), file=out_file)
    #     success_steps = iStep
    # if success_steps==0: continue
    # mcp_vals = [x[0]*180/3.1415 for x in [y['MCP.ang'] for y in state_maps]]
    # mcp_mid_idx = min(range(len(mcp_vals)), key=lambda i: abs(mcp_vals[i]-mcp_mid))
    # pip_vals = [x[0]*180/3.1415 for x in [y['PIP.ang'] for y in state_maps]]
    # pip_mid_idx = min(range(len(pip_vals)), key=lambda i: abs(pip_vals[i]-pip_mid))
    # dip_vals = [x[0]*180/3.1415 for x in [y['DIP.ang'] for y in state_maps]]
    # dip_mid_idx = min(range(len(dip_vals)), key=lambda i: abs(dip_vals[i]-dip_mid))
    # orl_taut_vals = [x[0] for x in [y['ORL.taut'] for y in state_maps]]
    # print(orl_taut_vals)
    # rgba = cmap((i_act)/len(act_list))
    # time_range = np.arange(0, success_steps, 1)/100.0
    # axes[0].plot(time_range, dip_vals[0:success_steps], label=act_val, c=rgba)
    # axes[0].plot(dip_mid_idx/100.0, -40, marker='D', c=rgba)
    # axes[1].plot(time_range, pip_vals[0:success_steps], label=act_val, c=rgba)
    # axes[1].plot(pip_mid_idx/100.0, -40, marker='D', c=rgba)
    # axes[2].plot(time_range, mcp_vals[0:success_steps], label=act_val, c=rgba)
    # axes[2].plot(mcp_mid_idx/100.0, -40, marker='D', c=rgba)
    # axes[3].plot(time_range, orl_taut_vals[0:success_steps], label=act_val, c=rgba)

# xtics = [  0, 5, 10, 15,  20]
# ytics = [-40, 0, 40, 80, 120]
# row_labels = ['DIP Extension-Flexion', 'PIP Extension-Flexion', 'MCP Extension-Flexion', 'ORL strain']
# for i_row, row_axis in enumerate(axes):
#     if i_row>2: continue
#     row_axis.set_ylabel(row_labels[i_row])
#     row_axis.set_yticks(ytics)
#     row_axis.set_xticks(xtics)
# f.tight_layout()
# plt.legend(ncol=3)
# plt.show()
