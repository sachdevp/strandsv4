import matplotlib.pyplot as plt
from matplotlib import pylab
from importlib import reload
import os
import strands_mod
from ctypes import *
import xml.etree.ElementTree as xmltree
import tempfile as tf
from matplotlib import cm
import numpy as np
import getopt
import sys

# Usage: python <scriptname> -o out_dir

strands_dll = strands_mod.load_strands_mod()
opts, args = getopt.getopt(sys.argv[1:], "d:")
out_dir = None
for o, a in opts:
    if o=="-d":
        print('-d detected')
        try:
            if not os.path.exists(a):
                os.makedirs(a)
            out_dir = a
            print('Output directory set to %s'%a)
        except:
            print('Could not create directory %s'%a)
    else:
        print('Input poorly formatted.')
        sys.exit(0)
if not out_dir:
    print('Directory not set.')
    sys.exit(0)

fname = '../data/xml/Lumbrical.Weighted.NoORL.xml'
et = xmltree.parse(fname)
load_mass_tag  = [x for x in et.findall("./rigid[@name='LUMLoad']/") if x.tag=='mass'][0]
load_mi_tag    = [x for x in et.findall("./rigid[@name='LUMLoad']/") if x.tag=='MI'][0]
wrench_tag     = [x for x in et.findall("./force[@name='LUMLoadForce']/scripted/") if x.tag=='wrench'][0]
mcp_wrench_tag = [x for x in et.findall("./force[@name='MCPWrench']/scripted/") if x.tag=='wrench'][0]
pip_stiff_tag  = [x for x in et.findall("./joint[@name='PIP']/shape/") if x.tag=='stiffness'][0]
pip_damp_tag   = [x for x in et.findall("./joint[@name='PIP']/shape/") if x.tag=='damping'][0]
dip_stiff_tag  = [x for x in et.findall("./joint[@name='DIP']/shape/") if x.tag=='stiffness'][0]
dip_damp_tag   = [x for x in et.findall("./joint[@name='DIP']/shape/") if x.tag=='damping'][0]
mcp_stiff_tag  = [x for x in et.findall("./joint[@name='MCP']/shape/") if x.tag=='stiffness'][0]
mcp_damp_tag   = [x for x in et.findall("./joint[@name='MCP']/shape/") if x.tag=='damping'][0]

nSteps     = 1800
nInitSteps = 100
cmap = plt.cm.get_cmap('jet')
mass_list = [10, 20, 30, 40, 50, 100, 150, 200]
enable_wrench_list = [True, True, True, True, True, False, False, False]
# mass_list = [10, 200]
# enable_wrench_list = [True, False]
pip_stiffness_val = ['e', [0, 0.05, 0, 0.05, 30, 0]]
dip_stiffness_val = ['e', [0, 0.05, 0, 0.05, 30, 0]]
pip_damping = 1e5
dip_damping = 5e4
strFlexAnchor = c_char_p('pCube2'.encode('utf-8'))
strMCPWrench  = c_char_p('MCPWrench'.encode('utf-8'))
strPIPWrench  = c_char_p('PIPWrench'.encode('utf-8'))

current_mass   = strands_mod.tag_to_mass(load_mass_tag)
current_mi     = strands_mod.tag_to_mi(load_mi_tag)
current_wrench = strands_mod.tag_to_wrench(wrench_tag)


# 18 seconds 
f, axes = plt.subplots(nrows=3, ncols=1, figsize=(6, 8))
# Should the wrench be enabled to modify initial position
strands_mod.set_stiffness(pip_stiff_tag, pip_stiffness_val[0], pip_stiffness_val[1])
strands_mod.set_stiffness(dip_stiff_tag, dip_stiffness_val[0], dip_stiffness_val[1])
pip_damp_tag.text = str(pip_damping)
dip_damp_tag.text = str(dip_damping)

base_fname = 'data'

try:
    with open('%s/in.dat'%out_dir, 'w') as fout:
        print(len(mass_list), file=fout)
except:
    print('Could not create new in.dat file.')
for i_mass, mass_val in enumerate(mass_list):
    load_mass_tag.text = str(mass_val)
    strands_mod.set_text(load_mi_tag, [val*mass_val/current_mass for val in current_mi])
    strands_mod.set_text(wrench_tag,  [val*mass_val/current_mass for val in current_wrench])
    enable_wrench = enable_wrench_list[i_mass]
    tfile = tf.NamedTemporaryFile()
    tfile_name = tfile.name
    et.write(tfile_name)

    scene_p = c_void_p()
    scene_p = strands_dll.load_scene(c_char_p(tfile_name.encode('utf-8')))
    if not scene_p:
        raise ValueError('Could not load scene.')
    dt = strands_dll.get_dt(scene_p)

    results_fname = '%s.%d.csv'%(base_fname, mass_val)
    success_steps=0
    if enable_wrench:
        strands_dll.disable_wrench(scene_p, strPIPWrench)
        vel_sc = [0,0,0]
        strands_dll.script_rigid(scene_p, strFlexAnchor, strands_mod.convert_float_list(vel_sc))
        broken=False
        for iStep in range(0, nInitSteps):
            strands_dll.step(scene_p)
            if(strands_dll.failed_check(scene_p)):
                broken=True
                input('Broken: ')
                break
            success_steps = iStep
        if broken and success_steps==0:
            sys.exit(0)
        vel_sc[2] = -0.2
        strands_dll.script_rigid(scene_p, strFlexAnchor, strands_mod.convert_float_list(vel_sc))
    else:
        strands_dll.disable_wrench(scene_p, strMCPWrench)
        vel_sc = [0,0,0]
        strands_dll.script_rigid(scene_p, strFlexAnchor, strands_mod.convert_float_list(vel_sc))
        for iStep in range(0, nInitSteps):
            strands_dll.step(scene_p)
            if(strands_dll.failed_check(scene_p)): break
            success_steps = iStep
        vel_sc[2] = -0.2
        strands_dll.script_rigid(scene_p, strFlexAnchor, strands_mod.convert_float_list(vel_sc))
    strands_dll.disable_wrench(scene_p, strMCPWrench)
    strands_dll.disable_wrench(scene_p, strPIPWrench)

    try:
        with open('%s/in.dat'%out_dir, 'a+') as fout:
            print('%s, %s'%(results_fname, str(mass_val)+'g'), file=fout)
            if i_mass==(len(mass_list)-1):
                state_attrs_str = strands_dll.get_state_attrs(scene_p).decode('utf-8')
                print(state_attrs_str, file=fout)
                state_attrs = [x for x in str(state_attrs_str).split(' ') if x!='']
    except IOError:
        print('Could not write in.dat in dir %s'%out_dir)

    try:
        with open('%s/%s'%(out_dir, results_fname), 'w') as fout:
            state_attrs_str = strands_dll.get_state_attrs(scene_p).decode('utf-8')
            state_attrs = [x for x in str(state_attrs_str).split(' ') if x!='']
            # print(state_attrs_str, file=fout)
            success_steps=0
            for iStep in range(0, nSteps):
                strands_dll.step(scene_p)
                if(strands_dll.failed_check(scene_p)): break
                print(strands_dll.get_state(scene_p).decode('utf-8'), file=fout)
                success_steps = iStep
            print('Completed %d steps successfully.'%success_steps)
    except IOError:
        print('Could not write results for %d in %s'%(mass_val, out_dir))
