from ctypes import *
import os
import sys

def load_strands_mod():
    t1=-1
    t2=-1
    f1 = "../lib/libStrandsDynamicDebug.so"
    f2 = "../lib/libStrandsDynamicRelease.so"
    fname = ""
    try: t1 = os.path.getmtime(f1)
    except: pass 
    try: t2 = os.path.getmtime(f2)
    except: pass
    if t1>0 and t2>0:
        if t1>t2:
            fname = f1
        else:
            fname = f2
    elif t1<0:
        fname = "../lib/libStrandsDynamicRelease.so"
    elif t2<0:
        fname = "../lib/libStrandsDynamicDebug.so"
    else:
        raise ValueError('Could not load any library: neither release nor debug.')
    strands_dll = cdll.LoadLibrary(fname)

    strands_dll.load_scene.argtypes         = [c_char_p]
    strands_dll.load_scene.restype          =  c_void_p
    strands_dll.step.argtypes               = [c_void_p]
    strands_dll.get_label.argtypes          = [c_void_p]
    strands_dll.get_label.restype           =  c_char_p
    strands_dll.get_state.argtypes          = [c_void_p]
    strands_dll.get_state.restype           =  c_char_p
    strands_dll.get_state_attrs.argtypes    = [c_void_p]
    strands_dll.get_state_attrs.restype     =  c_char_p
    strands_dll.get_dt.argtypes             = [c_void_p]
    strands_dll.get_dt.restype              =  c_float
    strands_dll.set_act.argtypes            = [c_void_p, c_char_p, c_float]
    strands_dll.set_act.restype             =  c_bool
    strands_dll.set_act_vec.argtypes        = [c_void_p, POINTER(c_char_p), POINTER(c_float), c_int]
    strands_dll.set_act_vec.restype         =  c_bool
    strands_dll.get_internal_state.argtypes = [c_void_p, POINTER(POINTER(c_float)), POINTER(POINTER(c_float)), POINTER(c_int)];
    strands_dll.get_internal_state.restype  =  c_bool
    strands_dll.set_initial_state.argtypes  = [c_void_p, POINTER(c_float), POINTER(c_float), c_int, c_float];
    strands_dll.set_initial_state.restype   =  c_bool
    strands_dll.get_red_mat.argtypes        = [c_void_p, POINTER(POINTER(c_float)), POINTER(POINTER(c_float)), POINTER(c_int)];
    # Wrench functions
    strands_dll.disable_wrench.argtypes  = [c_void_p, c_char_p]
    # strands_dll.disable_wrench.restype = c_void
    strands_dll.script_wrench.argtypes   = [c_void_p, c_char_p, POINTER(c_float)]
    # strands_dll.script_wrench.restype  = c_void
    # Rigid script functiopns
    strands_dll.script_rigid.argtypes    = [c_void_p, c_char_p, POINTER(c_float)]
    # strands_dll.script_rigid.restype   = c_void
    
    return strands_dll

def convert_string_list(lst):
    arr = (c_char_p * len(lst))()
    arr[:] = lst
    return arr

def convert_float_list(lst):
    arr = (c_float * len(lst))()
    arr[:] = lst
    return arr 

# def get_attr_lens():
attr_lens = dict()
attr_lens['pos']=6
attr_lens['ang']=1
attr_lens['exceeded_x']=1
attr_lens['exceeded_y']=1
attr_lens['exceeded_z']=1
attr_lens['strain']=1
attr_lens['taut']=1

def process_state(state_attrs, state_vals):
    state_vals = [float(x) for x in state_vals.decode('utf-8').split(' ') if x]
    state_map = dict()
    idx=0
    for attr in state_attrs:
        attr_name = attr.split('.')[1]
        # print attr_name
        if attr_name in attr_lens:
            # print 'attr_name in attr_lens'
            attr_len = attr_lens[attr_name]
            if (idx+attr_len)>len(state_vals):
                print('We got a problem in state.')
                return
            # print 'attr_name in attr_lens 2'
            state_map[attr] = state_vals[idx:(idx+attr_len)]
            # print 'state_map attr appended'
            idx = idx+attr_len
        else:
            print('Don\'t know length for attr %s'%attr_name)
    if idx!=len(state_vals):
        print('Too many state vals.')
    return state_map

def tag_to_mass(mass_tag):
    if mass_tag.tag!='mass':
        print('Not a mass tag.')
    try: return float(mass_tag.text)
    except ValueError:
        print('Could not convert mass tag to float.')
        raise ValueError('Could not convert mass tag.')

def tag_to_damp(damp_tag):
    if damp_tag.tag!='damping':
        print('Not a damping tag.')
    try: return float(damp_tag.text)
    except ValueError:
        print('Could not convert damp tag to float.')
        raise ValueError('Could not convert damp tag.')

def tag_to_stiff(stiff_tag):
    if stiff_tag.tag!='stiffness':
        print('Not a stiffness tag.')
    try: return stiff_tag.attrib['type'], [float(x) for x in stiff_tag.text.split(' ') if x!='']
    except ValueError:
        print('Could not convert stiffness tag to type and float list.')
        return None, None

def tag_to_wrench(wrench_tag):
    if wrench_tag.tag!='wrench':
        print('Not a wrench tag.')
    try: return [float(x) for x in wrench_tag.text.split(' ') if x!='']
    except ValueError:
        print('Could not convert wrench tag float list.')
        return None

def tag_to_mi(mi_tag):
    if mi_tag.tag!='MI':
        print('Not an MI tag.')
    try: return [float(x) for x in mi_tag.text.split(' ') if x!='']
    except ValueError:
        print('Could not convert MI tag to float list.')
        return None

def set_text(tag, params):
    tag.text = ' '.join([str(x) for x in params])

def set_stiffness(tag, typ, params):
    tag.attrib['type']=typ
    set_text(tag, params)

def get_out_file():
    '''Get output file, failing which, exit.'''
    out_file = None
    if len(sys.argv)==2:
        if os.path.isfile(sys.argv[1]):
            yorn = input('File already exists. Override? y/n')
            if not yorn=='y':
                sys.exit(0)
        try:
            out_file = open(sys.argv[1], 'w')
        except IOError:
            print ('Could not open output file')
            sys.exit(0)
    else:
        print('Argument for output file missing.')
        sys.exit(0)
    return out_file
        
def get_in_file():
    '''Get output file, failing which, exit.'''
    in_file = None
    if len(sys.argv)==2:
        if not os.path.isfile(sys.argv[1]):
            yorn = input('File does not exist.')
            sys.exit(0)
        try:
            in_file = open(sys.argv[1], 'r')
        except IOError:
            print ('Could not open input file.')
            sys.exit(0)
    else:
        print('Argument for input file missing.')
        sys.exit(0)
    return in_file
