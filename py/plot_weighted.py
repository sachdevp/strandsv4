import plot_mod
import matplotlib as mp
import matplotlib.pyplot as plt

out_file, in_files, in_labels, attr_labels = plot_mod.read_state_files()

mp.rcParams.update({'font.size': 12})
timestep = 0.01
in_dict = dict()
in_dict['pip_mid'] = 54
in_dict['dip_mid'] = 22.5
in_dict['mcp_mid'] = 44.8
in_dict['timestep'] = 0.01
results = []
for in_file, in_label in zip(in_files, in_labels):
    results.append(plot_mod.read_state_map(in_file, in_label, attr_labels, timestep))
f, axes = plot_mod.plot_angles(results, in_dict)

plt.grid()
plt.savefig(out_file)
        
    
